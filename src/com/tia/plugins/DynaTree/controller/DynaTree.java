package com.tia.plugins.DynaTree.controller;

import java.sql.Connection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Text;

import com.tia.plugins.DynaTree.model.Leaf;
import com.tia.plugins.DynaTree.model.Model;
import com.tia.plugins.DynaTree.model.TreeController;
import com.tia.plugins.DynaTree.model.TreeModel;
import com.tia.plugins.DynaTree.model.TreeNode;
import com.tia.plugins.DynaTree.ui.TreeNodeContentProvider;
import com.tia.plugins.DynaTree.ui.TreeNodeLabelProvider;

public class DynaTree {
	protected TreeViewer treeViewer;
	protected Text text;
	protected TreeNodeLabelProvider labelProvider;

	private List<TreeModel> treeModel;

	private TreeController objTest = new TreeController();
	protected TreeNode root;

	private Connection con;
	private String strQuery;
	private String strWhere;
	private String strOrder;
	private String strCustmQry;

	public DynaTree(Connection con, String strQry) {
		this(con, strQry, "", "");
	}

	public DynaTree(Connection con, String strQry, String strWhere,
			String strOrder) {
		this.con = con;
		this.strQuery = strQry;
		System.out.println("this is the query:"+strQry+strWhere);
		this.strWhere = strWhere;
		this.strOrder = strOrder;
		this.strCustmQry = "";
	}

	public DynaTree(Connection con, String strQry, String strWhere,
			String strOrder, String strCustomField) {
		this.con = con;
		this.strQuery = strQry;
		this.strWhere = strWhere;
		this.strOrder = strOrder;
		this.strCustmQry = strCustomField;
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public TreeViewer createTree(TreeViewer tv) {
		treeViewer = tv;
		treeViewer.setContentProvider(new TreeNodeContentProvider());
		labelProvider = new TreeNodeLabelProvider();
		treeViewer.setLabelProvider(labelProvider);
		treeViewer.setUseHashlookup(true);
		FormData layoutData = new FormData();
		treeViewer.getControl().setLayoutData(layoutData);

		if (this.strWhere.equals("")) {
			treeViewer.setInput(getInitalInput());

		} else {
			root = new TreeNode();
			if (!this.strCustmQry.equals("")) {
				treeViewer.setInput(getInitalInputUnlimited(root,
						this.strQuery, strWhere, this.strCustmQry, strOrder));
			} else {
				treeViewer.setInput(getInitalInputUnlimited(root,
						this.strQuery, strWhere, "0", strOrder));
			}
		}

		treeViewer.expandToLevel(1);
		return treeViewer;
	}

	/**
	 * Add a new book to the selected moving box. If a moving box is not
	 * selected, use the selected obect's moving box.
	 * 
	 * If nothing is selected add to the root.
	 */
	public void addNewLeaf(String id, String Label, String code) {
		TreeNode receivingBox;
		if (treeViewer.getSelection().isEmpty()) {
			receivingBox = root;
		} else {
			IStructuredSelection selection = (IStructuredSelection) treeViewer
					.getSelection();
			Model selectedDomainObject = (Model) selection.getFirstElement();
			if (!(selectedDomainObject instanceof TreeNode)) {
				receivingBox = selectedDomainObject.getParent();
			} else {
				receivingBox = (TreeNode) selectedDomainObject;
			}
		}
		// System.out.println("This is parent of selection " +
		// receivingBox.getLabel());
		Leaf lf = new Leaf(id, Label, code);
		receivingBox.add(lf);
		treeViewer.refresh();
		// treeViewer.expandAll();
	}

	public void addNewTreeNode(String id, String Label, String code) {
		TreeNode receivingBox;
		if (treeViewer.getSelection().isEmpty()) {
			receivingBox = root;
		} else {
			IStructuredSelection selection = (IStructuredSelection) treeViewer
					.getSelection();
			Model selectedDomainObject = (Model) selection.getFirstElement();
			if (!(selectedDomainObject instanceof TreeNode)) {
				// receivingBox = (MovingBox) selectedDomainObject;
				receivingBox = selectedDomainObject.getParent();
			} else {
				receivingBox = (TreeNode) selectedDomainObject;
			}
		}
		System.out.println("This is parent of selection "
				+ receivingBox.getLabel());
		TreeNode tn = new TreeNode(id, Label, code);
		receivingBox.add(tn);
		treeViewer.refresh();
		// treeViewer.expandAll();
	}

	/**
	 * Remove the selected domain object(s). If multiple objects are selected
	 * remove all of them.
	 * 
	 * If nothing is selected do nothing.
	 */
	public void removeSelected() {
		if (treeViewer.getSelection().isEmpty()) {
			return;
		}
		IStructuredSelection selection = (IStructuredSelection) treeViewer
				.getSelection();
		/*
		 * Tell the tree to not redraw until we finish removing all the selected
		 * children.
		 */
		treeViewer.getTree().setRedraw(false);
		for (Iterator iterator = selection.iterator(); iterator.hasNext();) {
			Model model = (Model) iterator.next();
			TreeNode parent = model.getParent();
			parent.remove(model);
		}
		treeViewer.getTree().setRedraw(true);
	}

	public TreeNode getInitalInput() {
		treeModel = objTest.getTreeNodes(this.con, this.strQuery);
		TreeNode Tree = null;
		root = new TreeNode();
		for (Iterator<TreeModel> iterator = treeModel.iterator(); iterator
				.hasNext();) {
			TreeModel listTreeModel = iterator.next();
			int Parent = Integer.parseInt(listTreeModel.getParent());
			if (Parent == 0) {
				Tree = new TreeNode(listTreeModel.getId(),
						listTreeModel.getLabel(), listTreeModel.getCode());
				root.add(Tree);
			} else {
				if (Parent > 0) {
					// if(root.getCvalue().equals(listTreeModel.getParent()))
					Tree.add(new Leaf(listTreeModel.getId(), listTreeModel
							.getLabel(), listTreeModel.getCode()));
				}

			}
		}

		return root;
	}

	public TreeNode getInitalInputUnlimited(TreeNode parent, String strParaQry,
			String strParaWhere, String strParaValue, String strParaOrder) {
		String strQuery = strParaQry + " WHERE " + strParaWhere + " = "
				+ strParaValue + " ORDER BY " + strParaOrder;
		System.out.println("This is the strQuery:"+strQuery);

		treeModel = objTest.getTreeNodes(this.con, strQuery);
		TreeNode Tree = null;
		TreeNode root = parent;
		for (Iterator<TreeModel> iterator = treeModel.iterator(); iterator
				.hasNext();) {
			TreeModel listTreeModel = iterator.next();
			Tree = new TreeNode(listTreeModel.getId(),
					listTreeModel.getLabel(), listTreeModel.getCode());
			root.add(Tree);
			getInitalInputUnlimited(Tree, strParaQry, strParaWhere,
					listTreeModel.getId(), strParaOrder);
		}

		return root;
	}

}
