package com.tia.plugins.DynaTree.ui;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;

import com.tia.plugins.DynaTree.model.Leaf;
import com.tia.plugins.DynaTree.model.TreeNode;

public class TreeNodeLabelProvider extends LabelProvider {
	private Map imageCache = new HashMap(11);

	/*
	 * @see ILabelProvider#getImage(Object)
	 */
	@Override
	public Image getImage(Object element) {
		ImageDescriptor descriptor = null;
		if (element instanceof TreeNode) {
			descriptor = new ImageDescriptor() {

				@Override
				public ImageData getImageData() {
					// ImageData imageData=new ImageData("icons/treenode.gif");
					// URL iconURL =
					// getClass().getResource("/com/toedter/calendar/images/JDateChooserIcon.gif");
					ClassLoader cl = this.getClass().getClassLoader();
					ImageData imageDate = new ImageData(
							cl.getResourceAsStream("packages.gif"));
					return imageDate;
				}
			};

		} else if (element instanceof Leaf) {
			descriptor = new ImageDescriptor() {

				@Override
				public ImageData getImageData() {
					// ImageData imageData=new ImageData("icons/leaf.gif");
					ClassLoader cl = this.getClass().getClassLoader();
					ImageData imageDate = new ImageData(
							cl.getResourceAsStream("package.gif"));
					return imageDate;
				}
			};
		} else {
			throw unknownElement(element);
		}

		// obtain the cached image corresponding to the descriptor
		Image image = (Image) imageCache.get(descriptor);
		if (image == null) {
			image = descriptor.createImage();
			imageCache.put(descriptor, image);
		}
		return image;
	}

	/*
	 * @see ILabelProvider#getText(Object)
	 */
	@Override
	public String getText(Object element) {
		if (element instanceof TreeNode) {
			if (((TreeNode) element).getLabel() == null) {
				return "Box";
			} else {
				return ((TreeNode) element).getLabel();
			}
		} else if (element instanceof Leaf) {
			return ((Leaf) element).getLabel();
		} else {
			throw unknownElement(element);
		}
	}

	public String getCode(Object element) {
		if (element instanceof TreeNode) {
			if (((TreeNode) element).getCvalue() == null) {
				return "Box";
			} else {
				return ((TreeNode) element).getCvalue();
			}
		} else if (element instanceof Leaf) {
			return ((Leaf) element).getCvalue();
		} else {
			throw unknownElement(element);
		}
	}

	public String getId(Object element) {
		if (element instanceof TreeNode) {
			if (((TreeNode) element).getCvalue() == null) {
				return "Box";
			} else {
				return ((TreeNode) element).getNodeId();
			}
		} else if (element instanceof Leaf) {
			return ((Leaf) element).getNodeId();
		} else {
			throw unknownElement(element);
		}
	}

	@Override
	public void dispose() {
		for (Iterator i = imageCache.values().iterator(); i.hasNext();) {
			((Image) i.next()).dispose();
		}
		imageCache.clear();
	}

	protected RuntimeException unknownElement(Object element) {
		return new RuntimeException("Unknown type of element in tree of type "
				+ element.getClass().getName());
	}

}
