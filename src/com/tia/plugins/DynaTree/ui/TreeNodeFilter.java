package com.tia.plugins.DynaTree.ui;
import com.tia.plugins.DynaTree.model.*;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

public class TreeNodeFilter extends ViewerFilter {

	/*
	 * @see ViewerFilter#select(Viewer, Object, Object)
	 */
	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		return element instanceof TreeNode;
	}

}
