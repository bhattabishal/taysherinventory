package com.tia.plugins.DynaTree.ui;




import org.eclipse.jface.viewers.ViewerSorter;

import com.tia.plugins.DynaTree.model.Leaf;
import com.tia.plugins.DynaTree.model.TreeNode;

public class LeafNodeSorter extends ViewerSorter {
	
	/*
	 * @see ViewerSorter#category(Object)
	 */
	/** Orders the items in such a way that books appear 
	 * before moving boxes, which appear before board games. */
	@Override
	public int category(Object element) {
		if(element instanceof Leaf) return 1;
		if(element instanceof TreeNode) return 2;
		return 3;
	}

}
