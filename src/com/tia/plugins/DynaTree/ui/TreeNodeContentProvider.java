package com.tia.plugins.DynaTree.ui;

import java.util.Iterator;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;

import com.tia.plugins.DynaTree.model.*;



public class TreeNodeContentProvider implements ITreeContentProvider, IDeltaListener {
	private static Object[] EMPTY_ARRAY = new Object[0];
	protected TreeViewer viewer;
	
	/*
	 * @see IContentProvider#dispose()
	 */
	@Override
	public void dispose() {}

	/*
	 * @see IContentProvider#inputChanged(Viewer, Object, Object)
	 */
	/**
	* Notifies this content provider that the given viewer's input
	* has been switched to a different element.
	* <p>
	* A typical use for this method is registering the content provider as a listener
	* to changes on the new input (using model-specific means), and deregistering the viewer 
	* from the old input. In response to these change notifications, the content provider
	* propagates the changes to the viewer.
	* </p>
	*
	* @param viewer the viewer
	* @param oldInput the old input element, or <code>null</code> if the viewer
	*   did not previously have an input
	* @param newInput the new input element, or <code>null</code> if the viewer
	*   does not have an input
	*/
	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		this.viewer = (TreeViewer)viewer;
		if(oldInput != null) {
			removeListenerFrom((TreeNode)oldInput);
		}
		if(newInput != null) {
			addListenerTo((TreeNode)newInput);
		}
	}
	
	/** Because the domain model does not have a richer
	 * listener model, recursively remove this listener
	 * from each child treeNode of the given treeNode. */
	protected void removeListenerFrom(TreeNode treeNode) {
		treeNode.removeListener(this);
		for (Iterator iterator = treeNode.getTreeNodes().iterator(); iterator.hasNext();) {
			TreeNode aBox = (TreeNode) iterator.next();
			removeListenerFrom(aBox);
		}
	}
	
	/** Because the domain model does not have a richer
	 * listener model, recursively add this listener
	 * to each child treeNode of the given treeNode. */
	protected void addListenerTo(TreeNode treeNode) {
		treeNode.addListener(this);
		for (Iterator iterator = treeNode.getTreeNodes().iterator(); iterator.hasNext();) {
			TreeNode aBox = (TreeNode) iterator.next();
			addListenerTo(aBox);
		}
	}
	
	
	

	/*
	 * @see ITreeContentProvider#getChildren(Object)
	 */
	@Override
	public Object[] getChildren(Object parentElement) {
		if(parentElement instanceof TreeNode) {
			TreeNode treeNode = (TreeNode)parentElement;
			return concat(treeNode.getTreeNodes().toArray(), 
				treeNode.getLeafs().toArray());
		}
		return EMPTY_ARRAY;
	}
	
	protected Object[] concat(Object[] object, Object[] more) {
		Object[] both = new Object[object.length + more.length];
		System.arraycopy(object, 0, both, 0, object.length);
		System.arraycopy(more, 0, both, object.length, more.length);
		return both;
	}

	/*
	 * @see ITreeContentProvider#getParent(Object)
	 */
	@Override
	public Object getParent(Object element) {
		if(element instanceof Model) {
			return ((Model)element).getParent();
		}
		return null;
	}

	/*
	 * @see ITreeContentProvider#hasChildren(Object)
	 */
	@Override
	public boolean hasChildren(Object element) {
		return getChildren(element).length > 0;
	}

	/*
	 * @see IStructuredContentProvider#getElements(Object)
	 */
	@Override
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	/*
	 * @see IDeltaListener#add(DeltaEvent)
	 */
	@Override
	public void add(DeltaEvent event) {
		Object treeNode = ((Model)event.receiver()).getParent();
		viewer.refresh(treeNode, false);
	}

	/*
	 * @see IDeltaListener#remove(DeltaEvent)
	 */
	@Override
	public void remove(DeltaEvent event) {
		add(event);
	}

}
