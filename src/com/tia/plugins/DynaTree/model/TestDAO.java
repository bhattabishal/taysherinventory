package com.tia.plugins.DynaTree.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tia.common.db.DbConnection;



public class TestDAO {
	private List<TreeModel> newObjects;

	public List<TreeModel> getParent(Connection cnn,String strQry) {
		newObjects = new ArrayList<TreeModel>();
		//Connection cnn = dbconnection.getConnection();
		TreeModel treeModel;
		ResultSet rs = null;
		//String strQry = "";
		//strQry = "SELECT Pid, port_name,parent FROM loan_portfolio_mcg order by sub_port_id";
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			rs = st.executeQuery(strQry);
			System.out.println("The getAllMember " + strQry);
			if (rs != null)
				while (rs.next()) {
					treeModel = new TreeModel();
					treeModel.setCode(rs.getString("code"));
					treeModel.setLabel(rs.getString("label"));
					treeModel.setParent(rs.getString("parent"));
					newObjects.add(treeModel);
				}
			else {
				return null;
			}
			return newObjects;

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return newObjects;
	}

	public List<TreeModel> getChildren() {
		newObjects = new ArrayList<TreeModel>();
		Connection cnn = DbConnection.getConnection();
		TreeModel treeModel;
		ResultSet rs = null;
		String strQry = "";
		strQry = "SELECT Pid, port_name,Parent FROM loan_portfolio_mcg WHERE Parent>0";
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			rs = st.executeQuery(strQry);
			System.out.println("The getAllMember " + strQry);
			if (rs != null)
				while (rs.next()) {
					treeModel = new TreeModel();
					treeModel.setCode(rs.getString("Pid"));
					treeModel.setLabel(rs.getString("port_name"));
					treeModel.setParent(rs.getString("parent"));
					newObjects.add(treeModel);
				}
			else {
				return null;
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return newObjects;
	}
}
