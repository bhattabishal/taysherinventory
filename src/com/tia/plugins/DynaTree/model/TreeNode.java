package com.tia.plugins.DynaTree.model;

import java.util.ArrayList;
import java.util.List;

public class TreeNode extends Model {
	protected List treeNodes;
	protected List leafs;
	
	private static IModelVisitor adder = new Adder();
	private static IModelVisitor remover = new Remover();
	
	public TreeNode() {
		treeNodes = new ArrayList();
		leafs = new ArrayList();
	}
	
	private static class Adder implements IModelVisitor {

		/*
		 * @see ModelVisitorI#visitLeaf(MovingBox, Object)
		 */
		@Override
		public void visitLeaf(Leaf leaf, Object argument) {
			((TreeNode) argument).addLeaf(leaf);
		}

		/*
		 * @see ModelVisitorI#visitTreeNode(MovingBox, Object)
		 */
		@Override
		public void visitTreeNode(TreeNode treeNode, Object argument) {
			((TreeNode) argument).addTreeNode(treeNode);
		}

	}

	private static class Remover implements IModelVisitor {
		/*
		 * @see ModelVisitorI#visitLeaf(MovingBox, Object)
		 */
		@Override
		public void visitLeaf(Leaf leaf, Object argument) {
			((TreeNode) argument).removeLeaf(leaf);
		}

		/*
		 * @see ModelVisitorI#visitTreeNode(MovingBox, Object)
		 */
		@Override
		public void visitTreeNode(TreeNode treeNode, Object argument) {
			((TreeNode) argument).removeBox(treeNode);
			treeNode.addListener(NullDeltaListener.getSoleInstance());
		}

	}
	
	public TreeNode(String id,String label, String cValue) {
		this();
		this.nodeId = id;
		this.label = label;
		this.cValue = cValue;
	}
	
	public List getTreeNodes() {
		return treeNodes;
	}
	
	protected void addTreeNode(TreeNode treeNode) {
		treeNodes.add(treeNode);
		treeNode.parent = this;
		fireAdd(treeNode);
	}
	
	protected void addLeaf(Leaf leaf) {
		leafs.add(leaf);
		leaf.parent = this;
		fireAdd(leaf);
	}
	
	public List getLeafs() {
		return leafs;
	}
	
	public void remove(Model toRemove) {
		toRemove.accept(remover, this);
	}
	
	protected void removeLeaf(Leaf leaf) {
		leafs.remove(leaf);
		leaf.addListener(NullDeltaListener.getSoleInstance());
		fireRemove(leaf);
	}
	
	protected void removeBox(TreeNode treeNode) {
		treeNodes.remove(treeNode);
		treeNode.addListener(NullDeltaListener.getSoleInstance());
		fireRemove(treeNode);	
	}

	public void add(Model toAdd) {
		toAdd.accept(adder, this);
	}
	
	/** Answer the total number of items the
	 * receiver contains. */
	public int size() {
		return getLeafs().size() + getTreeNodes().size();
	}
	/*
	 * @see Model#accept(ModelVisitorI, Object)
	 */
	@Override
	public void accept(IModelVisitor visitor, Object passAlongArgument) {
		visitor.visitTreeNode(this, passAlongArgument);
	}

}
