package com.tia.plugins.DynaTree.model;

public class TreeModel {
	
	private String parent,label,code,id;

	public String getParent() {
		return this.parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getId(){
		return this.id;
	}

	public void setId(String id){
		this.id = id;
	}
}
