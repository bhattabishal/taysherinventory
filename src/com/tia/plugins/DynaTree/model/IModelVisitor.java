package com.tia.plugins.DynaTree.model;



public interface IModelVisitor {
	public void visitTreeNode(TreeNode treeNode, Object passAlongArgument);
	public void visitLeaf(Leaf leaf, Object passAlongArgument);
}
