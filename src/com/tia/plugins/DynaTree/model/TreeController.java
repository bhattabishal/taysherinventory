package com.tia.plugins.DynaTree.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tia.common.db.DbConnection;


public class TreeController {
	private List<TreeModel> newObjects;

	public List<TreeModel> getTreeNodes(Connection cnn, String strQry) {
		newObjects = new ArrayList<TreeModel>();
		TreeModel treeModel;
		ResultSet rs = null;
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			rs = st.executeQuery(strQry);
			//System.out.println("The getAllMember " + strQry);
			if (rs != null)
				while (rs.next()) {
					treeModel = new TreeModel();
					treeModel.setId(rs.getString("id"));
					treeModel.setCode(rs.getString("code"));
					treeModel.setLabel(rs.getString("label"));
					// if(strQry.contains("parent"))
					// treeModel.setParent(rs.getString("parent"));
					treeModel.setParent(rs.getString("parent"));
					newObjects.add(treeModel);
				}
			else {
				return null;
			}
			return newObjects;

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return newObjects;
	}

	public List<TreeModel> getChildren() {
		newObjects = new ArrayList<TreeModel>();
		Connection cnn = DbConnection.getConnection();
		TreeModel treeModel;
		ResultSet rs = null;
		String strQry = "";
		strQry = "SELECT Pid, port_name,Parent FROM loan_portfolio_mcg WHERE Parent>0";
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			rs = st.executeQuery(strQry);
			System.out.println("The getAllMember " + strQry);
			if (rs != null)
				while (rs.next()) {
					treeModel = new TreeModel();
					treeModel.setCode(rs.getString("Pid"));
					treeModel.setLabel(rs.getString("port_name"));
					treeModel.setParent(rs.getString("parent"));
					newObjects.add(treeModel);
				}
			else {
				return null;
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return newObjects;
	}
}
