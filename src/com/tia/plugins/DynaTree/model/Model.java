package com.tia.plugins.DynaTree.model;
public abstract class Model {
	protected TreeNode parent;
	protected String label;
	protected String cValue;
	protected String nodeId;
	
	protected IDeltaListener listener = NullDeltaListener.getSoleInstance();
	
	protected void fireAdd(Object added) {
		listener.add(new DeltaEvent(added));
	}

	protected void fireRemove(Object removed) {
		listener.remove(new DeltaEvent(removed));
	}
	
	/* The receiver should visit the toVisit object and
	 * pass along the argument. */
	public abstract void accept(IModelVisitor visitor, Object passAlongArgument);
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
	
	public TreeNode getParent() {
		return parent;
	}
	
	public String getCvalue() {
		return cValue;
	}
	
	public String getNodeId(){
		return nodeId;
	}

	public Model(String id, String title, String cValue) {
		this.label = title;
		this.cValue = cValue;
		this.nodeId = id;
	}
	
	public Model() {
	}	
	
	public void removeListener(IDeltaListener listener) {
		if(this.listener.equals(listener)) {
			this.listener = NullDeltaListener.getSoleInstance();
		}
	}

	public void addListener(IDeltaListener listener) {
		this.listener = listener;
	}
	
}
