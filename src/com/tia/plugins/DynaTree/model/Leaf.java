package com.tia.plugins.DynaTree.model;

public class Leaf extends Model {
//	protected static List newLeafs = buildLeafList();
	protected static int cursor = 0;
	
	public Leaf(String id, String label, String cValue) {
		super(id, label, cValue);
	}
	
//	public static Leaf newLeaf() {
//		Leaf newLeaf = (Leaf)newLeafs.get(cursor);
//		cursor = ((cursor + 1) % newLeafs.size());
//		return newLeaf;
//	}
	
//	protected static List buildLeafList() {
//		newLeafs = new ArrayList();
//		Leaf[] leafs = new Leaf[] {
//			new Leaf("Advanced Java: Idioms, Pitfalls, Styles and Programming Tips", "Chris"),
//			new Leaf("Programming Ruby: A Pragmatic Programmer's Guide", "David")
//		};
//		
//		for (int i = 0; i < leafs.length; i++) {
//			newLeafs.add(leafs[i]);		
//		}
//		return newLeafs;
//	}
	
	/*
	 * @see Model#accept(ModelVisitorI, Object)
	 */
	@Override
	public void accept(IModelVisitor visitor, Object passAlongArgument) {
		visitor.visitLeaf(this, passAlongArgument);
	}

}
