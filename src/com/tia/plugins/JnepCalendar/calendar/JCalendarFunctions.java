package com.tia.plugins.JnepCalendar.calendar;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.regex.Pattern;







import com.csvreader.CsvReader;
import com.tia.common.util.UtilFxn;

public class JCalendarFunctions {
	CsvReader cvsDate;
	CsvReader cvsIntializeDate;
	public static String strSeprator = "/";// �";

	public enum Diff {
		YEAR, MONTH, DAY;
	}

	public JCalendarFunctions() {
		
	}

	public boolean isDate(CharSequence date) {

		// some regular expression
		String time = "(\\s(([01]?\\d)|(2[0123]))[:](([012345]\\d)|(60))"
				+ "[:](([012345]\\d)|(60)))?"; // with a space before, zero or
												// one time

		// no check for leap years (Schaltjahr)
		// and 31.02.2006 will also be correct
		String day = "(([12]\\d)|(3[01])|(0?[1-9]))"; // 01 up to 31
		String month = "((1[012])|(0\\d))"; // 01 up to 12
		String year = "\\d{4}";

		// define here all date format
		ArrayList patterns = new ArrayList();
		patterns.add(day + "[-.]" + month + "[-.]" + year + time);
		patterns.add(year + "-" + month + "-" + day + time);
		patterns.add(year + "/" + month + "/" + day + time);
		// here you can add more date formats if you want

		// check dates
		for (Object objPa : patterns) {
			String strPa = objPa.toString();
			Pattern p = Pattern.compile(strPa);
			if (p.matcher(date).matches())
				return true;
		}
		return false;

	}

	public int getDaysBetween(java.util.Calendar d1, java.util.Calendar d2) {
		if (d1.after(d2)) { // swap dates so that d1 is start and d2 is end
			java.util.Calendar swap = d1;
			d1 = d2;
			d2 = swap;
		}
		int days = d2.get(java.util.Calendar.DAY_OF_YEAR)
				- d1.get(java.util.Calendar.DAY_OF_YEAR);
		int y2 = d2.get(java.util.Calendar.YEAR);
		if (d1.get(java.util.Calendar.YEAR) != y2) {
			d1 = (java.util.Calendar) d1.clone();
			do {
				days += d1.getActualMaximum(java.util.Calendar.DAY_OF_YEAR);
				d1.add(java.util.Calendar.YEAR, 1);
			} while (d1.get(java.util.Calendar.YEAR) != y2);
		}
		return days;
	} // getDaysBetween()

	public int getMonthsBetween(Date BigEngdate, Date SmallEngdate) {
		Calendar BigCal = Calendar.getInstance();
		Calendar SmallCal = Calendar.getInstance();
		int month1, month2;

		BigCal.setTime(BigEngdate);
		SmallCal.setTime(SmallEngdate);

		month1 = BigCal.get(Calendar.YEAR) * 12 + BigCal.get(Calendar.MONTH);
		month2 = SmallCal.get(Calendar.YEAR) * 12
				+ SmallCal.get(Calendar.MONTH);

		return month1 - month2;
	}

	public int getYearBetween(Date BigEngdate, Date SmallEngdate) {
		Calendar BigCal = Calendar.getInstance();
		Calendar SmallCal = Calendar.getInstance();

		BigCal.setTime(BigEngdate);
		SmallCal.setTime(SmallEngdate);
		return BigCal.get(Calendar.YEAR) - SmallCal.get(Calendar.YEAR);
	}

	public int GetDateDiff() {
		return 1;
	}

	public String getSeperator() {
		return strSeprator;
	}

	public void setSeperator(String seperator) {
		strSeprator = seperator;// "�";
	}

	public Date GetEnglishDate(String strNepaliDate) {
		// BufferedReader readDateNepali = new BufferedReader(new
		// InputStreamReader(
		// this.getClass().getClassLoader().getResourceAsStream("DateNepali.csv")));
		// BufferedReader readInitializeDate = new BufferedReader(new
		// InputStreamReader(
		// this.getClass().getClassLoader().getResourceAsStream("initializedate.csv")));
		ClassLoader CLDR = this.getClass().getClassLoader();

		BufferedReader readDateNepali = new BufferedReader(
				new InputStreamReader(
						CLDR.getResourceAsStream("DateNepali.csv")));
		BufferedReader readInitializeDate = new BufferedReader(
				new InputStreamReader(
						CLDR.getResourceAsStream("initializedate.csv")));

		/*
		 * System.out.println(JcalendarFunctionCvs.class.getClassLoader().
		 * getResource("DateNepali.csv").toString().replace("DateNepali.csv",
		 * "")); System.out.println(JcalendarFunctionCvs.class.getClassLoader().
		 * getResource("DateNepali.csv").getFile()); URI fileUrl = null; try {
		 * fileUrl =
		 * JcalendarFunctionCvs.class.getClassLoader().getResource("DateNepali.csv"
		 * ).toURI();
		 * 
		 * } catch (URISyntaxException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } try { URL filePath=fileUrl.toURL(); } catch
		 * (MalformedURLException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */

		cvsDate = new CsvReader(readDateNepali);
		cvsIntializeDate = new CsvReader(readInitializeDate);
		// cvsIntializeDate= new CsvReader(FileCvsInitializeDate);
		long daysDiff = 0;
		int countRows = 0;
		String[] splitDate = null;
		int intcounter, intcounter1;
		Date EngDate;
		String strEngDate = null;
		strNepaliDate = strNepaliDate.replaceAll("/", getSeperator());
		if (strNepaliDate == "" || strNepaliDate == null) {
			return null;
		}
		if (strNepaliDate.contains(this.getSeperator()) == false) {
			return null;
		}

		splitDate = strNepaliDate.split(this.getSeperator());

		if (splitDate[0] == "" || splitDate[1] == "" || splitDate[2] == "") {
			return null;
		}

		// cnn = jconn.getconnection();
		try {
			cvsDate.readHeaders();
			while (cvsDate.readRecord()) {// year,baishak,jestha,ashad,shrawan,bhadra,aaswin,kartik,mangshir,poush,magh,falgun,chaitra,
				if (Integer.parseInt(cvsDate.get("year")) < Integer
						.parseInt(splitDate[0])) {
					String day1 = cvsDate.get("baishak");
					String day2 = cvsDate.get("jestha");
					String day3 = cvsDate.get("ashad");
					String day4 = cvsDate.get("shrawan");
					String day5 = cvsDate.get("bhadra");
					String day6 = cvsDate.get("aaswin");
					String day7 = cvsDate.get("kartik");
					String day8 = cvsDate.get("mangshir");
					String day9 = cvsDate.get("poush");
					String day10 = cvsDate.get("magh");
					String day11 = cvsDate.get("falgun");
					String day12 = cvsDate.get("chaitra");
					daysDiff = daysDiff + Long.parseLong(day1)
							+ Long.parseLong(day2) + Long.parseLong(day3)
							+ Long.parseLong(day4) + Long.parseLong(day5)
							+ Long.parseLong(day6) + Long.parseLong(day7)
							+ Long.parseLong(day8) + Long.parseLong(day9)
							+ Long.parseLong(day10) + Long.parseLong(day11)
							+ Long.parseLong(day12);
				} else if (Integer.parseInt(cvsDate.get("year")) == Integer
						.parseInt(splitDate[0])) {
					for (int i = 1; i < Integer.parseInt(splitDate[1]); i++) {
						// System.out.println("please check this date"+cvsDate.get(i));
						daysDiff = daysDiff + Long.parseLong(cvsDate.get(i));
					}
				} else
					break;
				// cvsDate.close();
			}
		} catch (IOException e1) {
			cvsDate.close();
			cvsIntializeDate.close();
			try {
				readDateNepali.close();
				readInitializeDate.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			e1.printStackTrace();
		}
		daysDiff = (daysDiff + Long.parseLong(splitDate[2])) - 1;

		try {
			cvsIntializeDate.readHeaders();
			while (cvsIntializeDate.readRecord()) {
				strEngDate = cvsIntializeDate.get("startengdate");
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			cvsDate.close();
			cvsIntializeDate.close();
			try {
				readDateNepali.close();
				readInitializeDate.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		try {
			EngDate = df.parse(strEngDate);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(EngDate);
		// System.out.println("Date is : " + df.format(cal.getTime()));
		cal.add(Calendar.DATE, (int) daysDiff);
		// System.out.println("Date is : " + df.format(cal.getTime()));

		// try {
		// return df.parse(cal.getTime().toString());
		// } catch (ParseException e) {
		return cal.getTime();
		// }
	}

	public String getEndOfMonthDate(String NepDate) {
		ResultSet rs = null;
		int lstmnth = 1;
		String sDates[] = NepDate.split(getSeperator());
		int a = Integer.parseInt(sDates[1]);
		String mnthName = getMonthNameNepali(a);
		String query = "select " + mnthName + " from detaildate where year='"
				+ sDates[0] + "'";
		// System.out.println("mnth query--"+query);
		try {

			rs = UtilFxn.getData(query);
			while (rs.next()) {
				lstmnth = rs.getInt(mnthName);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sDates[1] = sDates[1].length() < 2 ? "0" + sDates[1] : sDates[1];
		System.out.println("End Of Month============" + sDates[0]
				+ getSeperator() + sDates[1] + getSeperator() + lstmnth);
		return sDates[0] + getSeperator() + sDates[1] + getSeperator()
				+ lstmnth;
	}

	public static String getMonthNameNepali(int month) {
		switch (month) {
		case 1:
			return "baishak";
		case 2:
			return "jestha";

		case 3:
			return "ashad";

		case 4:
			return "shrawan";

		case 5:
			return "bhadra";

		case 6:
			return "aaswin";

		case 7:
			return "kartik";

		case 8:
			return "mangshir";

		case 9:
			return "poush";

		case 10:
			return "magh";

		case 11:
			return "falgun";

		case 12:
			return "chaitra";

		default:
			break;
		}

		return "";
	}

	public Date GetFormattedEnglishDate(String strNepaliDate) {
		Date dtFormattedDate = new Date();
		// Calendar clnd = Calendar.getInstance();
		// clnd.setTime(this.GetEnglishDate(strNepaliDate));

		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

		try {
			dtFormattedDate = df.parse(df.format(this
					.GetEnglishDate(strNepaliDate)));

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dtFormattedDate;

	}

	public String replaceSeparator(String date, String separator) {
		return date.replaceAll(separator, getSeperator());
	}

	/*
	 * public String GetNepaliDate(String strEnglishDate) { long lngDaysDiff,
	 * tmpDaysDiff; int intcounter; Date dtEngDate, dtInitDate;
	 * 
	 * DateFormat df = new SimpleDateFormat("yyyy/MM/dd"); NumberFormat
	 * formatter = new DecimalFormat("##00");
	 * 
	 * if (strEnglishDate == "" || strEnglishDate == null) { return null; }
	 * 
	 * if (isDate(strEnglishDate) == false) { return null; }
	 * 
	 * cnn = jconn.getconnection();
	 * 
	 * Qry = "SELECT * FROM InitializeDate";
	 * 
	 * Recordset = jconn.GetSelect(cnn, Qry);
	 * 
	 * try { Recordset.first(); dtEngDate = df.parse(strEnglishDate); dtInitDate
	 * = df.parse(Recordset.getString("startengdate")); Calendar EngDate =
	 * Calendar.getInstance(); Calendar InitDate = Calendar.getInstance();
	 * EngDate.setTime(dtEngDate); InitDate.setTime(dtInitDate);
	 * 
	 * lngDaysDiff = getDaysBetween(EngDate, InitDate); if (lngDaysDiff == 0) {
	 * return Recordset.getString("startnepalidate").replace("/",
	 * this.getSeperator()); } else { Qry =
	 * "SELECT * FROM detaildate ORDER BY year"; Recordset =
	 * jconn.GetSelect(cnn, Qry); Recordset.first();
	 * 
	 * tmpDaysDiff = lngDaysDiff; while (tmpDaysDiff > 0) { for (intcounter = 2;
	 * intcounter <= 13; intcounter++) { if (tmpDaysDiff >
	 * Recordset.getLong(intcounter)) { tmpDaysDiff = tmpDaysDiff -
	 * Recordset.getLong(intcounter); } else if (tmpDaysDiff ==
	 * Recordset.getLong(intcounter)) { if (intcounter >= 12) {
	 * Recordset.next(); return Recordset.getLong(1) + this.getSeperator() +
	 * "01" + this.getSeperator() + "01";
	 * 
	 * } else {
	 * 
	 * return Recordset.getLong(1) + this.getSeperator() +
	 * formatter.format(intcounter) + this.getSeperator() + "01"; } } else {
	 * return Recordset.getLong(1) + this.getSeperator() +
	 * formatter.format(intcounter - 1) + this.getSeperator() +
	 * formatter.format(tmpDaysDiff + 1); } } Recordset.next(); } } return null;
	 * } catch (ParseException e) { return null; } catch (SQLException e) {
	 * return null; } }
	 */
	public String GetNepaliDate(String strEnglishDate) {
		// BufferedReader readDateNepali = new BufferedReader(new
		// InputStreamReader(
		// this.getClass().getClassLoader().getResourceAsStream("DateNepali.csv")));
		// BufferedReader readInitializeDate = new BufferedReader(new
		// InputStreamReader(
		// this.getClass().getClassLoader().getResourceAsStream("initializedate.csv")));
		ClassLoader CLDR = this.getClass().getClassLoader();

		BufferedReader readDateNepali = new BufferedReader(
				new InputStreamReader(
						CLDR.getResourceAsStream("DateNepali.csv")));
		BufferedReader readInitializeDate = new BufferedReader(
				new InputStreamReader(
						CLDR.getResourceAsStream("initializedate.csv")));

		cvsDate = new CsvReader(readDateNepali);
		cvsIntializeDate = new CsvReader(readInitializeDate);

		strEnglishDate = strEnglishDate.replace("-", "/").replace("�", "/");
		long lngDaysDiff, tmpDaysDiff;
		int intcounter;
		Date dtEngDate, dtInitDate;
		// String[] splitdate;
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		NumberFormat formatter = new DecimalFormat("##00");

		// System.out.println("calFxn : GetNepaliDate : para "+strEnglishDate);

		if (strEnglishDate == "" || strEnglishDate == null) {
			return null;
		}

		// System.out.println("calFxn : GetNepaliDate : after null check "+strEnglishDate);

		if (isDate(strEnglishDate) == false) {
			return null;
		}
		// System.out.println("calFxn : GetNepaliDate : after date check "+strEnglishDate);

		/*
		 * cnn = jconn.getconnection();
		 * 
		 * Qry = "SELECT * FROM InitializeDate";
		 * 
		 * Recordset = jconn.GetSelect(cnn, Qry);
		 */
		try {
			cvsIntializeDate.readHeaders();
			while (cvsIntializeDate.readRecord()) {
				try {
					dtEngDate = df.parse(strEnglishDate);
					dtInitDate = df.parse(cvsIntializeDate.get("startengdate"));
					Calendar EngDate = Calendar.getInstance();
					Calendar InitDate = Calendar.getInstance();
					EngDate.setTime(dtEngDate);
					InitDate.setTime(dtInitDate);
					lngDaysDiff = getDaysBetween(EngDate, InitDate);
					if (lngDaysDiff == 0) {
						return cvsIntializeDate.get("startengdate").replace(
								"/", getSeperator());
					} else {
						tmpDaysDiff = lngDaysDiff;
						while (tmpDaysDiff > 0) {
							cvsDate.readHeaders();
							while (cvsDate.readRecord()) {
								/*
								 * String day1=cvsDate.get("baishak"); String
								 * day2=cvsDate.get("jestha"); String
								 * day3=cvsDate.get("ashad"); String
								 * day4=cvsDate.get("shrawan"); String
								 * day5=cvsDate.get("bhadra"); String
								 * day6=cvsDate.get("aaswin"); String
								 * day7=cvsDate.get("kartik"); String
								 * day8=cvsDate.get("mangshir"); String
								 * day9=cvsDate.get("poush"); String
								 * day10=cvsDate.get("magh"); String
								 * day11=cvsDate.get("falgun"); String
								 * day12=cvsDate.get("chaitra");
								 */

								for (intcounter = 1; intcounter <= 12; intcounter++) {
									if (tmpDaysDiff > Long.parseLong(cvsDate
											.get(intcounter))) {
										tmpDaysDiff = tmpDaysDiff
												- Long.parseLong(cvsDate
														.get(intcounter));
									} else if (tmpDaysDiff == Long
											.parseLong(cvsDate.get(intcounter))) {
										if (intcounter > 11) {
											cvsDate.readRecord();
											return cvsDate.get(0)
													+ getSeperator() + "01"
													+ getSeperator() + "01";

										} else {
											return cvsDate.get(0)
													+ getSeperator()
													+ formatter
															.format(intcounter + 1)
													+ getSeperator() + "01";
										}
									} else {
										return cvsDate.get(0)
												+ getSeperator()
												+ formatter.format(intcounter)
												+ getSeperator()
												+ formatter
														.format(tmpDaysDiff + 1);
									}
								}
							}
						}
					}
				} catch (ParseException e) {
					cvsDate.close();
					cvsIntializeDate.close();
					try {
						readDateNepali.close();
						readInitializeDate.close();
					} catch (IOException y) {
						// TODO Auto-generated catch block
						y.printStackTrace();
					}
					e.printStackTrace();
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			cvsDate.close();
			cvsIntializeDate.close();
			try {
				readDateNepali.close();
				readInitializeDate.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;

	}

	public String nepaliDateAdd(long intervalue, String nepaliDateValue) {
		Date engDateValue, tmpDateValue;
		String resultDate;
		if (nepaliDateValue.isEmpty()) {
			return "";
		}
		Format formatter = new SimpleDateFormat("yyyy/MM/dd");

		Calendar cal = Calendar.getInstance();

		engDateValue = GetEnglishDate(nepaliDateValue);
		cal.setTime(engDateValue);
		cal.add(Calendar.DATE, (int) intervalue);
		tmpDateValue = cal.getTime();
		resultDate = GetNepaliDate(formatter.format(tmpDateValue));
		return resultDate;
	}

	public boolean CompareDate(String BigNepDate, String SmallNepDate) {
		Date NewDate1, NewDate2;
		NewDate1 = GetEnglishDate(BigNepDate.trim());
		NewDate2 = GetEnglishDate(SmallNepDate.trim());

		if (NewDate1.equals(NewDate2)) {
			return false;
		} else if (NewDate1.after(NewDate2)) {
			return true;
		} else {
			return false;
		}
	}

	public int GetWeekDay(String NepaliDate) {
		Date newDate;
		Calendar cal = Calendar.getInstance();

		newDate = GetEnglishDate(NepaliDate);
		cal.setTime(newDate);
		return cal.get(Calendar.DAY_OF_WEEK);
	}

	public int GetFirstDayOfWeek(int NepYear, int NepMonth) {
		String NepDate;
		NepDate = Integer.toString(NepYear) + this.getSeperator()
				+ Integer.toString(NepMonth) + this.getSeperator() + "01";
		return GetWeekDay(NepDate);
	}

	public String Today() {
		Date DtDate;
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		Calendar cal = Calendar.getInstance(Locale.ENGLISH);
		DtDate = cal.getTime();
		return GetNepaliDate(df.format(DtDate));
	}

	public int CurrentMonth() {
		String month;
		System.out.println("jcalendarFunction " + Today().toString());
		month = Today().substring(5, 7);
		System.out.println("jcalendarFunction " + month);
		return Integer.parseInt(month);
	}

	public int CurrentYear() {
		String year;
		year = Today().substring(0, 4);
		return Integer.parseInt(year);
	}

	public int CurrentDay() {
		String day;
		day = Today().substring(8, 10);
		return Integer.parseInt(day);
	}

	public String DateSeperator() {
		return this.strSeprator;
	}

	public boolean IsSaturday(String NepaliDate) {
		if (GetWeekDay(NepaliDate) == 7) {
			return true;
		} else {
			return false;
		}
	}

	public int DateDifference(String BigNepDate, String SmallNepDate,
			Diff difference) {
		int retValue = 0;
		Date BigDate, SmallDate;
		BigDate = GetEnglishDate(BigNepDate.trim());
		SmallDate = GetEnglishDate(SmallNepDate.trim());
		Calendar BC = Calendar.getInstance();
		Calendar SC = Calendar.getInstance();

		BC.setTime(BigDate);
		SC.setTime(SmallDate);

		if (difference == JCalendarFunctions.Diff.YEAR) {
			retValue = getYearBetween(BigDate, SmallDate);
		} else if (difference == JCalendarFunctions.Diff.MONTH) {
			retValue = getMonthsBetween(BigDate, SmallDate);
		} else if (difference == JCalendarFunctions.Diff.DAY) {
			retValue = getDaysBetween(BC, SC);
		}

		return retValue;
	}

	public int NoOfDaysInMonth(int NepaliYear, int NepaliMonth) {
		// BufferedReader readDateNepali = new BufferedReader(new
		// InputStreamReader(
		// this.getClass().getClassLoader().getResourceAsStream("DateNepali.csv")));
		ClassLoader CLDR = this.getClass().getClassLoader();

		BufferedReader readDateNepali = new BufferedReader(
				new InputStreamReader(
						CLDR.getResourceAsStream("DateNepali.csv")));
		cvsDate = new CsvReader(readDateNepali);

		int DaysInMonth;
		if (NepaliMonth > 0 && NepaliMonth < 13) {
			try {
				cvsDate.readHeaders();
				while (cvsDate.readRecord()) {// year,baishak,jestha,ashad,shrawan,bhadra,aaswin,kartik,mangshir,poush,magh,falgun,chaitra,
					if (Integer.parseInt(cvsDate.get("year")) == NepaliYear) {
						DaysInMonth = Integer
								.parseInt(cvsDate.get(NepaliMonth));
						return DaysInMonth;

					}
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return 0;
			} finally {
				cvsDate.close();
				try {
					readDateNepali.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} else {
			cvsDate.close();
			try {
				readDateNepali.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 0;

		}
		return 0;
	}

	public int getNepYear(String sDate) {
		String sDates[] = sDate.split(getSeperator());
		return Integer.parseInt(sDates[0]);
	}

	public int getNepMonth(String sDate) {
		String sDates[] = sDate.split(getSeperator());
		return Integer.parseInt(sDates[1]);
	}

	public int getNepDay(String sDate) {
		String sDates[] = sDate.split(getSeperator());
		return Integer.parseInt(sDates[2]);
	}

	public String formatEngDate(Calendar eCal) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		System.out.println("this formatEngDate "
				+ dateFormat.format(eCal.getTime()));
		return dateFormat.format(eCal.getTime());
	}

	public static void main(String[] args) {
		JCalendarFunctions jcalfunction = new JCalendarFunctions();
		// System.out.println("one "+jcalfunction.GetEnglishDate("2009�12�30"));
		jcalfunction.setSeperator("�");
		String strDate = "2069�01�31";
		Calendar clnd = Calendar.getInstance();
		clnd.setTime(jcalfunction.GetEnglishDate(strDate));
		System.out.println("English date for " + strDate + " is "
				+ jcalfunction.formatEngDate(clnd));
		System.out.println("English date from new fxn for " + strDate + " is "
				+ jcalfunction.GetFormattedEnglishDate(strDate));
		// System.out.println("result is "+jcalfunction.GetNepaliDate("2009/12/11"));
		// System.out.println(jcalfunction.GetNepaliDate("1986/01/01"));
		// System.out.println(jcalfunction.nepaliDateAdd(11,"2039�05�11"));
		// System.out.println(jcalfunction.CompareDate("2038�05�11","2037�05�11"));
		// System.out.println(jcalfunction.GetWeekDay("2038�08�26"));
		// System.out.println(jcalfunction.Today());
		// System.out.println(jcalfunction.CurrentMonth());
		// System.out.println(jcalfunction.CurrentYear());
		// System.out.println(jcalfunction.CurrentDay());
		// System.out.println(jcalfunction.DateDifference("2066�05�11","2039�05�11",Diff.MONTH));
	}

	public String addSubractDaysFrmEngDate(Date engDate, String operator,
			int days, String format) {
		Date dd = new Date();
		dd = engDate;
		String reqDate = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		int MILLIS_IN_DAY = 1000 * 60 * 60 * 24 * days;
		if (operator == "minus") {
			reqDate = dateFormat.format(dd.getTime() - MILLIS_IN_DAY);

		}
		if (operator == "plus") {
			reqDate = dateFormat.format(dd.getTime() + MILLIS_IN_DAY);
		}
		return reqDate;
	}

	public String getCurrentNepaliDate() {
		String strEnDt = "";
		String dt = "";
		try {
			SimpleDateFormat sdfSource = new SimpleDateFormat(
					"EEE MMM dd hh:mm:ss z yyyy");
			Date curRawEngDate = new Date();
			strEnDt = curRawEngDate.toString();

			System.out.println("Raw current Date:" + curRawEngDate);

			Date Fromdate = sdfSource.parse(strEnDt);

			SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd");
			strEnDt = sdfDestination.format(Fromdate);

			dt = this.GetNepaliDate(strEnDt);

			// return strDt;

		} catch (Exception e1) {

			e1.printStackTrace();

		}

		return dt;
	}

	public String getCurrentEnglishDateTime(String format) {
		if (format == "") {
			format = "yyyy-MM-dd HH:mm:ss";
		}
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = new Date();
		return dateFormat.format(date);
	}

	public boolean IsEnSaturday(Date EnDate) {
		if (GetEnWeekDay(EnDate) == 7) {
			return true;
		} else {
			return false;
		}
	}

	public int GetEnWeekDay(Date EnDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(EnDate);
		return cal.get(Calendar.DAY_OF_WEEK);
	}

	public String npDtToEnDt(String npClosingDt) {
		String strDt = "";
		try {

			SimpleDateFormat sdfSource = new SimpleDateFormat(
					"EEE MMM dd hh:mm:ss z yyyy");
			Date curRawEngDate = GetEnglishDate(npClosingDt);
			strDt = curRawEngDate.toString();

			Date Fromdate = sdfSource.parse(strDt);
			System.out.println("From Date:" + Fromdate);

			SimpleDateFormat sdfDestination = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			strDt = sdfDestination.format(Fromdate);

			// return strDt;
			System.out.println("StrDt:" + strDt);

		} catch (Exception e1) {

			e1.printStackTrace();

		}
		return strDt;

	}

	public int getDaysBetween(String baseEnDate, String nextEnDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		int days = 0;

		try {
			Calendar baseC = Calendar.getInstance();
			Calendar nextC = Calendar.getInstance();

			baseC.setTime(sdf.parse(baseEnDate));
			nextC.setTime(sdf.parse(nextEnDate));

			days = baseC.get(java.util.Calendar.DAY_OF_YEAR)
					- nextC.get(java.util.Calendar.DAY_OF_YEAR);
			int y2 = baseC.get(java.util.Calendar.YEAR);
			if (nextC.get(java.util.Calendar.YEAR) != y2) {
				nextC = (java.util.Calendar) nextC.clone();
				do {
					days += nextC
							.getActualMaximum(java.util.Calendar.DAY_OF_YEAR);
					nextC.add(java.util.Calendar.YEAR, 1);
				} while (nextC.get(java.util.Calendar.YEAR) != y2);
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return days;
	}

	public boolean CompareDateEqual(String BigNepDate, String SmallNepDate) {
		Date NewDate1, NewDate2;
		NewDate1 = GetEnglishDate(BigNepDate);
		NewDate2 = GetEnglishDate(SmallNepDate);

		if (NewDate1.equals(NewDate2)) {
			return true;
		} else {
			return false;
		}
	}

	public int CurrentYearFor(String Date) {
		String year;
		year = Date.substring(0, 4);
		return Integer.parseInt(year);
	}

	// this functions returns the desired date in either nepali or english by
	// simpling passing the date parameter that we get from the date picker
	public String getParsedDateOfDatePicker(Date date, boolean rtnNep) {
		String reqdate = "";
		SimpleDateFormat sdfSource = new SimpleDateFormat(
				"EEE MMM dd hh:mm:ss z yyyy");
		String strDate = date.toString();
		Date Date = null;
		try {
			Date = sdfSource.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd");
		strDate = sdfDestination.format(Date);

		if (rtnNep) {

			reqdate = this.GetNepaliDate(strDate);
		} else {
			reqdate = strDate;
		}

		return reqdate;
	}

	public static String currentDate() {
		Calendar cal = new GregorianCalendar();

		// Get the components of the date
		// int era = cal.get(Calendar.ERA); // 0=BC, 1=AD
		int year = cal.get(Calendar.YEAR); // 2002
		int month = cal.get(Calendar.MONTH) + 1; // 0=Jan, 1=Feb, ...
		int day = cal.get(Calendar.DAY_OF_MONTH); // 1...
		// int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK); // 1=Sunday, 2=Monday,

		String mn = String.valueOf(month);
		String dy = String.valueOf(day);
		if (month < 10) {
			mn = "0" + month;
		}
		if (day < 10) {
			dy = "0" + day;
		}
		// ...
		String curDate = year + "-" + mn + "-" + dy;
		return curDate;
	}

	public int CurrentMonthFor(String Date) {
		String month;
		month = Date.substring(5, 7);
		return Integer.parseInt(month);
	}

	public int CurrentDayFor(String Date) {
		String day;
		day = Date.substring(8, 10);
		return Integer.parseInt(day);
	}

	/**
	 * This function takes english unformatted date (EEE MMM dd hh:mm:ss z yyyy)
	 * and return formatted date.
	 * 
	 * @param engLishDate
	 *            (Unformatted)
	 * @return Formatted English date in this (yyyy/MM/dd) format.
	 * @author Ganesh Joshi
	 */
	public String getFormattedEnglishDate(String engLishDate) {
		Date frmtdDate = null;
		SimpleDateFormat sdfSource = new SimpleDateFormat(
				"EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);

		SimpleDateFormat dt = new SimpleDateFormat("yyyy" + getSeperator()
				+ "MM" + getSeperator() + "dd", Locale.ENGLISH);
		if (engLishDate.equals("")) {
			return "";
		}
		try {
			frmtdDate = sdfSource.parse(engLishDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return dt.format(frmtdDate);
	}

	/**
	 * This function return only system date in English
	 * 
	 * @return
	 */
	public String getSystemDate() {
		JCalendarFunctions jcfx = new JCalendarFunctions();
		Date d = new Date();
		return jcfx.getFormattedEnglishDate(d.toString());
	}

	/**
	 * @author Ganesh Joshi This function return system date and time in
	 *         english.
	 * @return 2060/01/01 6:00:45 pm
	 */
	public static String getSystemDateAndTime() {
		String date = "";
		JCalendarFunctions jcfx = new JCalendarFunctions();
		Date d = new Date();
		Calendar cal = Calendar.getInstance();
		cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

		date = jcfx.getFormattedEnglishDate(d.toString()) + "  "
				+ sdf.format(cal.getTime());

		return date;
	}
}
