package com.tia.plugins.JnepCalendar.calendar;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.tia.common.db.DbConnection;

public class JConnection {
	/**
	 * @return
	 */

	Connection connection;

	public JConnection() {

	}

	public Connection getconnection() {

		// this.connection = DbConnection.getConnection();
		//
		// if (connection != null) {
		// System.out.println("Connected ...");
		// return connection;
		// } else {
		// System.out.println("Trying Local Connection ...");
		// connection = this.getLocalConnection();
		// return connection;
		// }

		return DbConnection.getConnection();
	}

	/*
	 * public static void main(String[] args) { JConnection jconn = new
	 * JConnection();
	 * 
	 * jconn.getresult(jconn.getconnection()); }
	 */

	// public Connection getconnection() {
	//
	// try {
	// Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	// } catch (ClassNotFoundException e) {
	// System.out.println("Where is your PostgreSQL JDBC Driver? Include in your library path!");
	// e.printStackTrace();
	// return null;
	// }
	// Connection connection = null;
	//
	// try {
	//
	// connection =
	// DriverManager.getConnection("jdbc:sqlserver://192.168.0.23;database=testSkbbl",
	// "sa", "laxman");
	//
	// } catch (SQLException e) {
	// JOptionPane.showMessageDialog(null,"Unable to connect to server : "+e.toString(),"Connection Error",JOptionPane.ERROR_MESSAGE);
	// return null;
	// }
	//
	// if (connection != null) {
	// System.out.println("Connected ...");
	// return connection;
	// } else {
	// System.out.println("Failed to make connection!");
	// return null;
	// }
	// }

	public void getresult(Connection c) {
		Statement s = null;

		try {
			s = c.createStatement();
		} catch (SQLException se) {
			// System.out.println("Statement is not created ...");
			se.printStackTrace();
			System.exit(1);
		}
		ResultSet rs = null;
		try {
			rs = s.executeQuery("SELECT * FROM detaildate");
		} catch (SQLException se) {
			// System.out.println("Query is not executed ...");
			se.printStackTrace();
			System.exit(1);
		}

		int index = 0;

		try {
			while (rs.next()) {
				// System.out.println("Here's the result of row " + index++ +
				// ":");
				// System.out.println(rs.getString("year")+" "+
				// rs.getString("ashad"));
			}
		} catch (SQLException se) {
			// System.out.println("We got an exception while getting a result:this "
			// +
			// "shouldn't happen: we've done something really bad.");
			se.printStackTrace();
			System.exit(1);
		}
	}

	public ResultSet GetSelect(Connection c, String Query) {
		Statement s = null;

		try {
			s = c.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException se) {
			// System.out.println("Statement is not created ...");
			se.printStackTrace();
			return null;
		}
		ResultSet rs = null;
		try {
			rs = s.executeQuery(Query);
			return rs;
		} catch (SQLException se) {
			// System.out.println("Query is not executed ...");
			se.printStackTrace();
			return null;
		}

		/*
		 * int index = 0;
		 * 
		 * try { while (rs.next()) {
		 * System.out.println("Here's the result of row " + index++ + ":");
		 * System.out.println(rs.getString("year")+" "+ rs.getString("ashad"));
		 * } } catch (SQLException se) {
		 * System.out.println("We got an exception while getting a result:this "
		 * + "shouldn't happen: we've done something really bad.");
		 * se.printStackTrace(); return null; }
		 */
	}

}
