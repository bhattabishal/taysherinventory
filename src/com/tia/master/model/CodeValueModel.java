	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : CodeValueModel.
	/// </summary>
package com.tia.master.model;
	public class CodeValueModel
	{
		private String m_cv_type;
		private String m_cv_code;
		private String m_cv_lbl;
		private int m_cv_id;
		private CodeValueModel_Criteria z_WhereClause;

		private IsDirty_CodeValueModel z_bool;

		public CodeValueModel()
		{
			z_WhereClause = new CodeValueModel_Criteria();
			z_bool = new IsDirty_CodeValueModel();
		}
		public class IsDirty_CodeValueModel
		{
			public boolean  m_cv_type;
			public boolean  m_cv_code;
			public boolean  m_cv_lbl;
			public boolean  m_cv_id;
		}
		public String getCv_type()
		{
			return m_cv_type;
		}
		public void setCv_type(String value)
		{
			z_bool.m_cv_type = true;
			m_cv_type = value;
		}
		public String getCv_code()
		{
			return m_cv_code;
		}
		public void setCv_code(String value)
		{
			z_bool.m_cv_code = true;
			m_cv_code = value;
		}
		public String getCv_lbl()
		{
			return m_cv_lbl;
		}
		public void setCv_lbl(String value)
		{
			z_bool.m_cv_lbl = true;
			m_cv_lbl = value;
		}
		public int getCv_id()
		{
			return m_cv_id;
		}
		public void setCv_id(int value)
		{
			z_bool.m_cv_id = true;
			m_cv_id = value;
		}
		/*
		public CodeValueModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public CodeValueModel_Criteria Where(CodeValueModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO code_value_mcg ( ";
			if (z_bool.m_cv_type)
			{
				SQL+= z_sep +"cv_type";
				z_sep=" , ";
				}
			if (z_bool.m_cv_code)
			{
				SQL+= z_sep +"cv_code";
				z_sep=" , ";
				}
			if (z_bool.m_cv_lbl)
			{
				SQL+= z_sep +"cv_lbl";
				z_sep=" , ";
				}
			if (z_bool.m_cv_id)
			{
				SQL+= z_sep +"cv_id";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_cv_type)
			{
				SQL+= z_sep +"'" + m_cv_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cv_code)
			{
				SQL+= z_sep +"'" + m_cv_code + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cv_lbl)
			{
				SQL+= z_sep +"'" + m_cv_lbl + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cv_id)
			{
				SQL+= z_sep +"'" + m_cv_id + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE code_value_mcg SET ";
			if (z_bool.m_cv_type)
			{
				SQL+= z_sep +"cv_type='" + m_cv_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cv_code)
			{
				SQL+= z_sep +"cv_code='" + m_cv_code + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cv_lbl)
			{
				SQL+= z_sep +"cv_lbl='" + m_cv_lbl + "'";
				z_sep=" , ";
				}
//			if (z_bool.m_cv_id)
//			{
//				SQL+= z_sep +"cv_id='" + m_cv_id + "'";
//				z_sep=" , ";
//				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_cv_type)
			{
				SQL+= z_sep +"cv_type='" + z_WhereClause.cv_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cv_code)
			{
				SQL+= z_sep +"cv_code='" + z_WhereClause.cv_code() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cv_lbl)
			{
				SQL+= z_sep +"cv_lbl='" + z_WhereClause.cv_lbl() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cv_id)
			{
				SQL+= z_sep +"cv_id='" + z_WhereClause.cv_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM code_value_mcg";
			if (z_WhereClause.z_bool.m_cv_type)
			{
				SQL+= z_sep +"cv_type='" + z_WhereClause.cv_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cv_code)
			{
				SQL+= z_sep +"cv_code='" + z_WhereClause.cv_code() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cv_lbl)
			{
				SQL+= z_sep +"cv_lbl='" + z_WhereClause.cv_lbl() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cv_id)
			{
				SQL+= z_sep +"cv_id='" + z_WhereClause.cv_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM code_value_mcg";
			if (z_WhereClause.z_bool.m_cv_type)
			{
				SQL+= z_sep +"cv_type='" + z_WhereClause.cv_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cv_code)
			{
				SQL+= z_sep +"cv_code='" + z_WhereClause.cv_code() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cv_lbl)
			{
				SQL+= z_sep +"cv_lbl='" + z_WhereClause.cv_lbl() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cv_id)
			{
				SQL+= z_sep +"cv_id='" + z_WhereClause.cv_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : CodeValueModel_Criteria.
	/// </summary>
	public class CodeValueModel_Criteria 
	{
		private String m_cv_type;
		private String m_cv_code;
		private String m_cv_lbl;
		private int m_cv_id;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_CodeValueModel_Criteria  z_bool;

		public CodeValueModel_Criteria()

		{
		z_bool=new IsDirty_CodeValueModel_Criteria();  
		}
		public class IsDirty_CodeValueModel_Criteria 
		{
			public boolean  m_cv_type;
			public boolean  m_cv_code;
			public boolean  m_cv_lbl;
			public boolean  m_cv_id;
			public boolean  MyWhere;

		}
		public String cv_type()
		{
			return m_cv_type;
		}
		public void cv_type(String value)
		{
			z_bool.m_cv_type = true;
			m_cv_type = value;
			if (z_bool.m_cv_type)
			{
				_zWhereClause += z_sep+"cv_type='"+cv_type() + "'";
				z_sep=" AND ";
			}
		}
		public String cv_code()
		{
			return m_cv_code;
		}
		public void cv_code(String value)
		{
			z_bool.m_cv_code = true;
			m_cv_code = value;
			if (z_bool.m_cv_code)
			{
				_zWhereClause += z_sep+"cv_code='"+cv_code() + "'";
				z_sep=" AND ";
			}
		}
		public String cv_lbl()
		{
			return m_cv_lbl;
		}
		public void cv_lbl(String value)
		{
			z_bool.m_cv_lbl = true;
			m_cv_lbl = value;
			if (z_bool.m_cv_lbl)
			{
				_zWhereClause += z_sep+"cv_lbl='"+cv_lbl() + "'";
				z_sep=" AND ";
			}
		}
		public int cv_id()
		{
			return m_cv_id;
		}
		public void cv_id(int value)
		{
			z_bool.m_cv_id = true;
			m_cv_id = value;
			if (z_bool.m_cv_id)
			{
				_zWhereClause += z_sep+"cv_id='"+cv_id() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}