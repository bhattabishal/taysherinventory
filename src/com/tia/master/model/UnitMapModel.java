	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : UnitMapModel.
	/// </summary>
package com.tia.master.model;
	public class UnitMapModel
	{
		private String m_unit_name;
		private String m_conversion_ration;
		private int m_unit_id;
		private int m_base_unit;
		private int m_status;
		public UnitMapModel_Criteria z_WhereClause;

		private IsDirty_UnitMapModel z_bool;

		public UnitMapModel()
		{
			z_WhereClause = new UnitMapModel_Criteria();
			z_bool = new IsDirty_UnitMapModel();
		}
		public class IsDirty_UnitMapModel
		{
			public boolean  m_unit_name;
			public boolean  m_conversion_ration;
			public boolean  m_unit_id;
			public boolean  m_base_unit;
			public boolean  m_status;
		}
		public String getUnit_name()
		{
			return m_unit_name;
		}
		public void setUnit_name(String value)
		{
			z_bool.m_unit_name = true;
			m_unit_name = value;
		}
		public String getConversion_ration()
		{
			return m_conversion_ration;
		}
		public void setConversion_ration(String value)
		{
			z_bool.m_conversion_ration = true;
			m_conversion_ration = value;
		}
		public int getUnit_id()
		{
			return m_unit_id;
		}
		public void setUnit_id(int value)
		{
			z_bool.m_unit_id = true;
			m_unit_id = value;
		}
		public int getBase_unit()
		{
			return m_base_unit;
		}
		public void setBase_unit(int value)
		{
			z_bool.m_base_unit = true;
			m_base_unit = value;
		}
		public int getStatus()
		{
			return m_status;
		}
		public void setStatus(int value)
		{
			z_bool.m_status = true;
			m_status = value;
		}
		/*
		public UnitMapModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public UnitMapModel_Criteria Where(UnitMapModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_unit ( ";
			if (z_bool.m_unit_name)
			{
				SQL+= z_sep +"unit_name";
				z_sep=" , ";
				}
			if (z_bool.m_conversion_ration)
			{
				SQL+= z_sep +"conversion_ration";
				z_sep=" , ";
				}
			if (z_bool.m_unit_id)
			{
				SQL+= z_sep +"unit_id";
				z_sep=" , ";
				}
			if (z_bool.m_base_unit)
			{
				SQL+= z_sep +"base_unit";
				z_sep=" , ";
				}
			if (z_bool.m_status)
			{
				SQL+= z_sep +"status";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_unit_name)
			{
				SQL+= z_sep +"'" + m_unit_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_conversion_ration)
			{
				SQL+= z_sep +"'" + m_conversion_ration + "'";
				z_sep=" , ";
				}
			if (z_bool.m_unit_id)
			{
				SQL+= z_sep +"'" + m_unit_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_base_unit)
			{
				SQL+= z_sep +"'" + m_base_unit + "'";
				z_sep=" , ";
				}
			if (z_bool.m_status)
			{
				SQL+= z_sep +"'" + m_status + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_unit SET ";
			if (z_bool.m_unit_name)
			{
				SQL+= z_sep +"unit_name='" + m_unit_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_conversion_ration)
			{
				SQL+= z_sep +"conversion_ration='" + m_conversion_ration + "'";
				z_sep=" , ";
				}
			if (z_bool.m_unit_id)
			{
				SQL+= z_sep +"unit_id='" + m_unit_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_base_unit)
			{
				SQL+= z_sep +"base_unit='" + m_base_unit + "'";
				z_sep=" , ";
				}
			if (z_bool.m_status)
			{
				SQL+= z_sep +"status='" + m_status + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_unit_name)
			{
				SQL+= z_sep +"unit_name='" + z_WhereClause.unit_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_conversion_ration)
			{
				SQL+= z_sep +"conversion_ration='" + z_WhereClause.conversion_ration() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit_id)
			{
				SQL+= z_sep +"unit_id='" + z_WhereClause.unit_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_base_unit)
			{
				SQL+= z_sep +"base_unit='" + z_WhereClause.base_unit() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_status)
			{
				SQL+= z_sep +"status='" + z_WhereClause.status() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_unit";
			if (z_WhereClause.z_bool.m_unit_name)
			{
				SQL+= z_sep +"unit_name='" + z_WhereClause.unit_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_conversion_ration)
			{
				SQL+= z_sep +"conversion_ration='" + z_WhereClause.conversion_ration() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit_id)
			{
				SQL+= z_sep +"unit_id='" + z_WhereClause.unit_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_base_unit)
			{
				SQL+= z_sep +"base_unit='" + z_WhereClause.base_unit() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_status)
			{
				SQL+= z_sep +"status='" + z_WhereClause.status() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_unit";
			if (z_WhereClause.z_bool.m_unit_name)
			{
				SQL+= z_sep +"unit_name='" + z_WhereClause.unit_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_conversion_ration)
			{
				SQL+= z_sep +"conversion_ration='" + z_WhereClause.conversion_ration() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit_id)
			{
				SQL+= z_sep +"unit_id='" + z_WhereClause.unit_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_base_unit)
			{
				SQL+= z_sep +"base_unit='" + z_WhereClause.base_unit() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_status)
			{
				SQL+= z_sep +"status='" + z_WhereClause.status() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : UnitMapModel_Criteria.
	/// </summary>
	public class UnitMapModel_Criteria 
	{
		private String m_unit_name;
		private int m_conversion_ration;
		private int m_unit_id;
		private int m_base_unit;
		private int m_status;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_UnitMapModel_Criteria  z_bool;

		public UnitMapModel_Criteria()

		{
		z_bool=new IsDirty_UnitMapModel_Criteria();  
		}
		public class IsDirty_UnitMapModel_Criteria 
		{
			public boolean  m_unit_name;
			public boolean  m_conversion_ration;
			public boolean  m_unit_id;
			public boolean  m_base_unit;
			public boolean  m_status;
			public boolean  MyWhere;

		}
		public String unit_name()
		{
			return m_unit_name;
		}
		public void unit_name(String value)
		{
			z_bool.m_unit_name = true;
			m_unit_name = value;
			if (z_bool.m_unit_name)
			{
				_zWhereClause += z_sep+"unit_name='"+unit_name() + "'";
				z_sep=" AND ";
			}
		}
		public int conversion_ration()
		{
			return m_conversion_ration;
		}
		public void conversion_ration(int value)
		{
			z_bool.m_conversion_ration = true;
			m_conversion_ration = value;
			if (z_bool.m_conversion_ration)
			{
				_zWhereClause += z_sep+"conversion_ration='"+conversion_ration() + "'";
				z_sep=" AND ";
			}
		}
		public int unit_id()
		{
			return m_unit_id;
		}
		public void unit_id(int value)
		{
			z_bool.m_unit_id = true;
			m_unit_id = value;
			if (z_bool.m_unit_id)
			{
				_zWhereClause += z_sep+"unit_id='"+unit_id() + "'";
				z_sep=" AND ";
			}
		}
		public int base_unit()
		{
			return m_base_unit;
		}
		public void base_unit(int value)
		{
			z_bool.m_base_unit = true;
			m_base_unit = value;
			if (z_bool.m_base_unit)
			{
				_zWhereClause += z_sep+"base_unit='"+base_unit() + "'";
				z_sep=" AND ";
			}
		}
		public int status()
		{
			return m_status;
		}
		public void status(int value)
		{
			z_bool.m_status = true;
			m_status = value;
			if (z_bool.m_status)
			{
				_zWhereClause += z_sep+"status='"+status() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}