	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : KeySettingModel.
	/// </summary>
package com.tia.master.model;
	public class KeySettingModel
	{
		private String m_key_value;
		private String m_key_for;
		private int m_updt_by;
		private int m_updt_cnt;
		private int m_key_id;
		private KeySettingModel_Criteria z_WhereClause;

		private IsDirty_KeySettingModel z_bool;

		public KeySettingModel()
		{
			z_WhereClause = new KeySettingModel_Criteria();
			z_bool = new IsDirty_KeySettingModel();
		}
		public class IsDirty_KeySettingModel
		{
			public boolean  m_key_value;
			public boolean  m_key_for;
			public boolean  m_updt_by;
			public boolean  m_updt_cnt;
			public boolean  m_key_id;
		}
		public String getKey_value()
		{
			return m_key_value;
		}
		public void setKey_value(String value)
		{
			z_bool.m_key_value = true;
			m_key_value = value;
		}
		public String getKey_for()
		{
			return m_key_for;
		}
		public void setKey_for(String value)
		{
			z_bool.m_key_for = true;
			m_key_for = value;
		}
		public int getUpdt_by()
		{
			return m_updt_by;
		}
		public void setUpdt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
		}
		public int getUpdt_cnt()
		{
			return m_updt_cnt;
		}
		public void setUpdt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
		}
		public int getKey_id()
		{
			return m_key_id;
		}
		public void setKey_id(int value)
		{
			z_bool.m_key_id = true;
			m_key_id = value;
		}
		/*
		public KeySettingModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public KeySettingModel_Criteria Where(KeySettingModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_key_setting ( ";
			if (z_bool.m_key_value)
			{
				SQL+= z_sep +"key_value";
				z_sep=" , ";
				}
			if (z_bool.m_key_for)
			{
				SQL+= z_sep +"key_for";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt";
				z_sep=" , ";
				}
			if (z_bool.m_key_id)
			{
				SQL+= z_sep +"key_id";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_key_value)
			{
				SQL+= z_sep +"'" + m_key_value + "'";
				z_sep=" , ";
				}
			if (z_bool.m_key_for)
			{
				SQL+= z_sep +"'" + m_key_for + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"'" + m_updt_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"'" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_key_id)
			{
				SQL+= z_sep +"'" + m_key_id + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_key_setting SET ";
			if (z_bool.m_key_value)
			{
				SQL+= z_sep +"key_value='" + m_key_value + "'";
				z_sep=" , ";
				}
			if (z_bool.m_key_for)
			{
				SQL+= z_sep +"key_for='" + m_key_for + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + m_updt_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_key_id)
			{
				SQL+= z_sep +"key_id='" + m_key_id + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_key_value)
			{
				SQL+= z_sep +"key_value='" + z_WhereClause.key_value() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_key_for)
			{
				SQL+= z_sep +"key_for='" + z_WhereClause.key_for() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_key_id)
			{
				SQL+= z_sep +"key_id='" + z_WhereClause.key_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_key_setting";
			if (z_WhereClause.z_bool.m_key_value)
			{
				SQL+= z_sep +"key_value='" + z_WhereClause.key_value() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_key_for)
			{
				SQL+= z_sep +"key_for='" + z_WhereClause.key_for() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_key_id)
			{
				SQL+= z_sep +"key_id='" + z_WhereClause.key_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_key_setting";
			if (z_WhereClause.z_bool.m_key_value)
			{
				SQL+= z_sep +"key_value='" + z_WhereClause.key_value() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_key_for)
			{
				SQL+= z_sep +"key_for='" + z_WhereClause.key_for() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_key_id)
			{
				SQL+= z_sep +"key_id='" + z_WhereClause.key_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : KeySettingModel_Criteria.
	/// </summary>
	public class KeySettingModel_Criteria 
	{
		private String m_key_value;
		private String m_key_for;
		private int m_updt_by;
		private int m_updt_cnt;
		private int m_key_id;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_KeySettingModel_Criteria  z_bool;

		public KeySettingModel_Criteria()

		{
		z_bool=new IsDirty_KeySettingModel_Criteria();  
		}
		public class IsDirty_KeySettingModel_Criteria 
		{
			public boolean  m_key_value;
			public boolean  m_key_for;
			public boolean  m_updt_by;
			public boolean  m_updt_cnt;
			public boolean  m_key_id;
			public boolean  MyWhere;

		}
		public String key_value()
		{
			return m_key_value;
		}
		public void key_value(String value)
		{
			z_bool.m_key_value = true;
			m_key_value = value;
			if (z_bool.m_key_value)
			{
				_zWhereClause += z_sep+"key_value='"+key_value() + "'";
				z_sep=" AND ";
			}
		}
		public String key_for()
		{
			return m_key_for;
		}
		public void key_for(String value)
		{
			z_bool.m_key_for = true;
			m_key_for = value;
			if (z_bool.m_key_for)
			{
				_zWhereClause += z_sep+"key_for='"+key_for() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_by()
		{
			return m_updt_by;
		}
		public void updt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
			if (z_bool.m_updt_by)
			{
				_zWhereClause += z_sep+"updt_by='"+updt_by() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_cnt()
		{
			return m_updt_cnt;
		}
		public void updt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
			if (z_bool.m_updt_cnt)
			{
				_zWhereClause += z_sep+"updt_cnt='"+updt_cnt() + "'";
				z_sep=" AND ";
			}
		}
		public int key_id()
		{
			return m_key_id;
		}
		public void key_id(int value)
		{
			z_bool.m_key_id = true;
			m_key_id = value;
			if (z_bool.m_key_id)
			{
				_zWhereClause += z_sep+"key_id='"+key_id() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}