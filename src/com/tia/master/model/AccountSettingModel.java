	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : AccountSettingModel.
	/// </summary>
package com.tia.master.model;
	public class AccountSettingModel
	{
		private String m_service_expense_cr;
		private int m_acc_set_id;
		private String m_item_sold_vat_dr;
		private String m_service_expense_dr;
		private String m_item_purchased_expense_dr;
		private String m_item_sold_credit_dr;
		private String m_item_sold_cr;
		private String m_service_earned_cr;
		private String m_item_purchased_dr;
		private String m_item_purchased_credit_cr;
		private String m_service_earned_dr;
		private String m_item_sold_discount_dr;
		private String m_item_sold_vat_cr;
		private String m_item_sold_discount_cr;
		private String m_item_purchased_expense_cr;
		public AccountSettingModel_Criteria z_WhereClause;

		private IsDirty_AccountSettingModel z_bool;

		public AccountSettingModel()
		{
			z_WhereClause = new AccountSettingModel_Criteria();
			z_bool = new IsDirty_AccountSettingModel();
		}
		public class IsDirty_AccountSettingModel
		{
			public boolean  m_service_expense_cr;
			public boolean  m_acc_set_id;
			public boolean  m_item_sold_vat_dr;
			public boolean  m_service_expense_dr;
			public boolean  m_item_purchased_expense_dr;
			public boolean  m_item_sold_credit_dr;
			public boolean  m_item_sold_cr;
			public boolean  m_service_earned_cr;
			public boolean  m_item_purchased_dr;
			public boolean  m_item_purchased_credit_cr;
			public boolean  m_service_earned_dr;
			public boolean  m_item_sold_discount_dr;
			public boolean  m_item_sold_vat_cr;
			public boolean  m_item_sold_discount_cr;
			public boolean  m_item_purchased_expense_cr;
		}
		public String getService_expense_cr()
		{
			return m_service_expense_cr;
		}
		public void setService_expense_cr(String value)
		{
			z_bool.m_service_expense_cr = true;
			m_service_expense_cr = value;
		}
		public int getAcc_set_id()
		{
			return m_acc_set_id;
		}
		public void setAcc_set_id(int value)
		{
			z_bool.m_acc_set_id = true;
			m_acc_set_id = value;
		}
		public String getItem_sold_vat_dr()
		{
			return m_item_sold_vat_dr;
		}
		public void setItem_sold_vat_dr(String value)
		{
			z_bool.m_item_sold_vat_dr = true;
			m_item_sold_vat_dr = value;
		}
		public String getService_expense_dr()
		{
			return m_service_expense_dr;
		}
		public void setService_expense_dr(String value)
		{
			z_bool.m_service_expense_dr = true;
			m_service_expense_dr = value;
		}
		public String getItem_purchased_expense_dr()
		{
			return m_item_purchased_expense_dr;
		}
		public void setItem_purchased_expense_dr(String value)
		{
			z_bool.m_item_purchased_expense_dr = true;
			m_item_purchased_expense_dr = value;
		}
		public String getItem_sold_credit_dr()
		{
			return m_item_sold_credit_dr;
		}
		public void setItem_sold_credit_dr(String value)
		{
			z_bool.m_item_sold_credit_dr = true;
			m_item_sold_credit_dr = value;
		}
		public String getItem_sold_cr()
		{
			return m_item_sold_cr;
		}
		public void setItem_sold_cr(String value)
		{
			z_bool.m_item_sold_cr = true;
			m_item_sold_cr = value;
		}
		public String getService_earned_cr()
		{
			return m_service_earned_cr;
		}
		public void setService_earned_cr(String value)
		{
			z_bool.m_service_earned_cr = true;
			m_service_earned_cr = value;
		}
		public String getItem_purchased_dr()
		{
			return m_item_purchased_dr;
		}
		public void setItem_purchased_dr(String value)
		{
			z_bool.m_item_purchased_dr = true;
			m_item_purchased_dr = value;
		}
		public String getItem_purchased_credit_cr()
		{
			return m_item_purchased_credit_cr;
		}
		public void setItem_purchased_credit_cr(String value)
		{
			z_bool.m_item_purchased_credit_cr = true;
			m_item_purchased_credit_cr = value;
		}
		public String getService_earned_dr()
		{
			return m_service_earned_dr;
		}
		public void setService_earned_dr(String value)
		{
			z_bool.m_service_earned_dr = true;
			m_service_earned_dr = value;
		}
		public String getItem_sold_discount_dr()
		{
			return m_item_sold_discount_dr;
		}
		public void setItem_sold_discount_dr(String value)
		{
			z_bool.m_item_sold_discount_dr = true;
			m_item_sold_discount_dr = value;
		}
		public String getItem_sold_vat_cr()
		{
			return m_item_sold_vat_cr;
		}
		public void setItem_sold_vat_cr(String value)
		{
			z_bool.m_item_sold_vat_cr = true;
			m_item_sold_vat_cr = value;
		}
		public String getItem_sold_discount_cr()
		{
			return m_item_sold_discount_cr;
		}
		public void setItem_sold_discount_cr(String value)
		{
			z_bool.m_item_sold_discount_cr = true;
			m_item_sold_discount_cr = value;
		}
		public String getItem_purchased_expense_cr()
		{
			return m_item_purchased_expense_cr;
		}
		public void setItem_purchased_expense_cr(String value)
		{
			z_bool.m_item_purchased_expense_cr = true;
			m_item_purchased_expense_cr = value;
		}
		/*
		public AccountSettingModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public AccountSettingModel_Criteria Where(AccountSettingModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_acc_setting ( ";
			if (z_bool.m_service_expense_cr)
			{
				SQL+= z_sep +"service_expense_cr";
				z_sep=" , ";
				}
//			if (z_bool.m_acc_set_id)
//			{
//				SQL+= z_sep +"acc_set_id";
//				z_sep=" , ";
//				}
			if (z_bool.m_item_sold_vat_dr)
			{
				SQL+= z_sep +"item_sold_vat_dr";
				z_sep=" , ";
				}
			if (z_bool.m_service_expense_dr)
			{
				SQL+= z_sep +"service_expense_dr";
				z_sep=" , ";
				}
			if (z_bool.m_item_purchased_expense_dr)
			{
				SQL+= z_sep +"item_purchased_expense_dr";
				z_sep=" , ";
				}
			if (z_bool.m_item_sold_credit_dr)
			{
				SQL+= z_sep +"item_sold_credit_dr";
				z_sep=" , ";
				}
			if (z_bool.m_item_sold_cr)
			{
				SQL+= z_sep +"item_sold_cr";
				z_sep=" , ";
				}
			if (z_bool.m_service_earned_cr)
			{
				SQL+= z_sep +"service_earned_cr";
				z_sep=" , ";
				}
			if (z_bool.m_item_purchased_dr)
			{
				SQL+= z_sep +"item_purchased_dr";
				z_sep=" , ";
				}
			if (z_bool.m_item_purchased_credit_cr)
			{
				SQL+= z_sep +"item_purchased_credit_cr";
				z_sep=" , ";
				}
			if (z_bool.m_service_earned_dr)
			{
				SQL+= z_sep +"service_earned_dr";
				z_sep=" , ";
				}
			if (z_bool.m_item_sold_discount_dr)
			{
				SQL+= z_sep +"item_sold_discount_dr";
				z_sep=" , ";
				}
			if (z_bool.m_item_sold_vat_cr)
			{
				SQL+= z_sep +"item_sold_vat_cr";
				z_sep=" , ";
				}
			if (z_bool.m_item_sold_discount_cr)
			{
				SQL+= z_sep +"item_sold_discount_cr";
				z_sep=" , ";
				}
			if (z_bool.m_item_purchased_expense_cr)
			{
				SQL+= z_sep +"item_purchased_expense_cr";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_service_expense_cr)
			{
				SQL+= z_sep +"'" + m_service_expense_cr + "'";
				z_sep=" , ";
				}
//			if (z_bool.m_acc_set_id)
//			{
//				SQL+= z_sep +"'" + m_acc_set_id + "'";
//				z_sep=" , ";
//				}
			if (z_bool.m_item_sold_vat_dr)
			{
				SQL+= z_sep +"'" + m_item_sold_vat_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_expense_dr)
			{
				SQL+= z_sep +"'" + m_service_expense_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_purchased_expense_dr)
			{
				SQL+= z_sep +"'" + m_item_purchased_expense_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_sold_credit_dr)
			{
				SQL+= z_sep +"'" + m_item_sold_credit_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_sold_cr)
			{
				SQL+= z_sep +"'" + m_item_sold_cr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_earned_cr)
			{
				SQL+= z_sep +"'" + m_service_earned_cr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_purchased_dr)
			{
				SQL+= z_sep +"'" + m_item_purchased_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_purchased_credit_cr)
			{
				SQL+= z_sep +"'" + m_item_purchased_credit_cr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_earned_dr)
			{
				SQL+= z_sep +"'" + m_service_earned_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_sold_discount_dr)
			{
				SQL+= z_sep +"'" + m_item_sold_discount_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_sold_vat_cr)
			{
				SQL+= z_sep +"'" + m_item_sold_vat_cr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_sold_discount_cr)
			{
				SQL+= z_sep +"'" + m_item_sold_discount_cr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_purchased_expense_cr)
			{
				SQL+= z_sep +"'" + m_item_purchased_expense_cr + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_acc_setting SET ";
			if (z_bool.m_service_expense_cr)
			{
				SQL+= z_sep +"service_expense_cr='" + m_service_expense_cr + "'";
				z_sep=" , ";
				}
//			if (z_bool.m_acc_set_id)
//			{
//				SQL+= z_sep +"acc_set_id='" + m_acc_set_id + "'";
//				z_sep=" , ";
//				}
			if (z_bool.m_item_sold_vat_dr)
			{
				SQL+= z_sep +"item_sold_vat_dr='" + m_item_sold_vat_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_expense_dr)
			{
				SQL+= z_sep +"service_expense_dr='" + m_service_expense_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_purchased_expense_dr)
			{
				SQL+= z_sep +"item_purchased_expense_dr='" + m_item_purchased_expense_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_sold_credit_dr)
			{
				SQL+= z_sep +"item_sold_credit_dr='" + m_item_sold_credit_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_sold_cr)
			{
				SQL+= z_sep +"item_sold_cr='" + m_item_sold_cr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_earned_cr)
			{
				SQL+= z_sep +"service_earned_cr='" + m_service_earned_cr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_purchased_dr)
			{
				SQL+= z_sep +"item_purchased_dr='" + m_item_purchased_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_purchased_credit_cr)
			{
				SQL+= z_sep +"item_purchased_credit_cr='" + m_item_purchased_credit_cr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_earned_dr)
			{
				SQL+= z_sep +"service_earned_dr='" + m_service_earned_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_sold_discount_dr)
			{
				SQL+= z_sep +"item_sold_discount_dr='" + m_item_sold_discount_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_sold_vat_cr)
			{
				SQL+= z_sep +"item_sold_vat_cr='" + m_item_sold_vat_cr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_sold_discount_cr)
			{
				SQL+= z_sep +"item_sold_discount_cr='" + m_item_sold_discount_cr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_purchased_expense_cr)
			{
				SQL+= z_sep +"item_purchased_expense_cr='" + m_item_purchased_expense_cr + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_service_expense_cr)
			{
				SQL+= z_sep +"service_expense_cr='" + z_WhereClause.service_expense_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_acc_set_id)
			{
				SQL+= z_sep +"acc_set_id='" + z_WhereClause.acc_set_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_vat_dr)
			{
				SQL+= z_sep +"item_sold_vat_dr='" + z_WhereClause.item_sold_vat_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_expense_dr)
			{
				SQL+= z_sep +"service_expense_dr='" + z_WhereClause.service_expense_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_purchased_expense_dr)
			{
				SQL+= z_sep +"item_purchased_expense_dr='" + z_WhereClause.item_purchased_expense_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_credit_dr)
			{
				SQL+= z_sep +"item_sold_credit_dr='" + z_WhereClause.item_sold_credit_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_cr)
			{
				SQL+= z_sep +"item_sold_cr='" + z_WhereClause.item_sold_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_earned_cr)
			{
				SQL+= z_sep +"service_earned_cr='" + z_WhereClause.service_earned_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_purchased_dr)
			{
				SQL+= z_sep +"item_purchased_dr='" + z_WhereClause.item_purchased_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_purchased_credit_cr)
			{
				SQL+= z_sep +"item_purchased_credit_cr='" + z_WhereClause.item_purchased_credit_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_earned_dr)
			{
				SQL+= z_sep +"service_earned_dr='" + z_WhereClause.service_earned_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_discount_dr)
			{
				SQL+= z_sep +"item_sold_discount_dr='" + z_WhereClause.item_sold_discount_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_vat_cr)
			{
				SQL+= z_sep +"item_sold_vat_cr='" + z_WhereClause.item_sold_vat_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_discount_cr)
			{
				SQL+= z_sep +"item_sold_discount_cr='" + z_WhereClause.item_sold_discount_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_purchased_expense_cr)
			{
				SQL+= z_sep +"item_purchased_expense_cr='" + z_WhereClause.item_purchased_expense_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_acc_setting";
			if (z_WhereClause.z_bool.m_service_expense_cr)
			{
				SQL+= z_sep +"service_expense_cr='" + z_WhereClause.service_expense_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_acc_set_id)
			{
				SQL+= z_sep +"acc_set_id='" + z_WhereClause.acc_set_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_vat_dr)
			{
				SQL+= z_sep +"item_sold_vat_dr='" + z_WhereClause.item_sold_vat_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_expense_dr)
			{
				SQL+= z_sep +"service_expense_dr='" + z_WhereClause.service_expense_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_purchased_expense_dr)
			{
				SQL+= z_sep +"item_purchased_expense_dr='" + z_WhereClause.item_purchased_expense_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_credit_dr)
			{
				SQL+= z_sep +"item_sold_credit_dr='" + z_WhereClause.item_sold_credit_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_cr)
			{
				SQL+= z_sep +"item_sold_cr='" + z_WhereClause.item_sold_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_earned_cr)
			{
				SQL+= z_sep +"service_earned_cr='" + z_WhereClause.service_earned_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_purchased_dr)
			{
				SQL+= z_sep +"item_purchased_dr='" + z_WhereClause.item_purchased_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_purchased_credit_cr)
			{
				SQL+= z_sep +"item_purchased_credit_cr='" + z_WhereClause.item_purchased_credit_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_earned_dr)
			{
				SQL+= z_sep +"service_earned_dr='" + z_WhereClause.service_earned_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_discount_dr)
			{
				SQL+= z_sep +"item_sold_discount_dr='" + z_WhereClause.item_sold_discount_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_vat_cr)
			{
				SQL+= z_sep +"item_sold_vat_cr='" + z_WhereClause.item_sold_vat_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_discount_cr)
			{
				SQL+= z_sep +"item_sold_discount_cr='" + z_WhereClause.item_sold_discount_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_purchased_expense_cr)
			{
				SQL+= z_sep +"item_purchased_expense_cr='" + z_WhereClause.item_purchased_expense_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_acc_setting";
			if (z_WhereClause.z_bool.m_service_expense_cr)
			{
				SQL+= z_sep +"service_expense_cr='" + z_WhereClause.service_expense_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_acc_set_id)
			{
				SQL+= z_sep +"acc_set_id='" + z_WhereClause.acc_set_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_vat_dr)
			{
				SQL+= z_sep +"item_sold_vat_dr='" + z_WhereClause.item_sold_vat_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_expense_dr)
			{
				SQL+= z_sep +"service_expense_dr='" + z_WhereClause.service_expense_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_purchased_expense_dr)
			{
				SQL+= z_sep +"item_purchased_expense_dr='" + z_WhereClause.item_purchased_expense_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_credit_dr)
			{
				SQL+= z_sep +"item_sold_credit_dr='" + z_WhereClause.item_sold_credit_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_cr)
			{
				SQL+= z_sep +"item_sold_cr='" + z_WhereClause.item_sold_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_earned_cr)
			{
				SQL+= z_sep +"service_earned_cr='" + z_WhereClause.service_earned_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_purchased_dr)
			{
				SQL+= z_sep +"item_purchased_dr='" + z_WhereClause.item_purchased_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_purchased_credit_cr)
			{
				SQL+= z_sep +"item_purchased_credit_cr='" + z_WhereClause.item_purchased_credit_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_earned_dr)
			{
				SQL+= z_sep +"service_earned_dr='" + z_WhereClause.service_earned_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_discount_dr)
			{
				SQL+= z_sep +"item_sold_discount_dr='" + z_WhereClause.item_sold_discount_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_vat_cr)
			{
				SQL+= z_sep +"item_sold_vat_cr='" + z_WhereClause.item_sold_vat_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sold_discount_cr)
			{
				SQL+= z_sep +"item_sold_discount_cr='" + z_WhereClause.item_sold_discount_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_purchased_expense_cr)
			{
				SQL+= z_sep +"item_purchased_expense_cr='" + z_WhereClause.item_purchased_expense_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : AccountSettingModel_Criteria.
	/// </summary>
	public class AccountSettingModel_Criteria 
	{
		private String m_service_expense_cr;
		private int m_acc_set_id;
		private String m_item_sold_vat_dr;
		private String m_service_expense_dr;
		private String m_item_purchased_expense_dr;
		private String m_item_sold_credit_dr;
		private String m_item_sold_cr;
		private String m_service_earned_cr;
		private String m_item_purchased_dr;
		private String m_item_purchased_credit_cr;
		private String m_service_earned_dr;
		private String m_item_sold_discount_dr;
		private String m_item_sold_vat_cr;
		private String m_item_sold_discount_cr;
		private String m_item_purchased_expense_cr;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_AccountSettingModel_Criteria  z_bool;

		public AccountSettingModel_Criteria()

		{
		z_bool=new IsDirty_AccountSettingModel_Criteria();  
		}
		public class IsDirty_AccountSettingModel_Criteria 
		{
			public boolean  m_service_expense_cr;
			public boolean  m_acc_set_id;
			public boolean  m_item_sold_vat_dr;
			public boolean  m_service_expense_dr;
			public boolean  m_item_purchased_expense_dr;
			public boolean  m_item_sold_credit_dr;
			public boolean  m_item_sold_cr;
			public boolean  m_service_earned_cr;
			public boolean  m_item_purchased_dr;
			public boolean  m_item_purchased_credit_cr;
			public boolean  m_service_earned_dr;
			public boolean  m_item_sold_discount_dr;
			public boolean  m_item_sold_vat_cr;
			public boolean  m_item_sold_discount_cr;
			public boolean  m_item_purchased_expense_cr;
			public boolean  MyWhere;

		}
		public String service_expense_cr()
		{
			return m_service_expense_cr;
		}
		public void service_expense_cr(String value)
		{
			z_bool.m_service_expense_cr = true;
			m_service_expense_cr = value;
			if (z_bool.m_service_expense_cr)
			{
				_zWhereClause += z_sep+"service_expense_cr='"+service_expense_cr() + "'";
				z_sep=" AND ";
			}
		}
		public int acc_set_id()
		{
			return m_acc_set_id;
		}
		public void acc_set_id(int value)
		{
			z_bool.m_acc_set_id = true;
			m_acc_set_id = value;
			if (z_bool.m_acc_set_id)
			{
				_zWhereClause += z_sep+"acc_set_id='"+acc_set_id() + "'";
				z_sep=" AND ";
			}
		}
		public String item_sold_vat_dr()
		{
			return m_item_sold_vat_dr;
		}
		public void item_sold_vat_dr(String value)
		{
			z_bool.m_item_sold_vat_dr = true;
			m_item_sold_vat_dr = value;
			if (z_bool.m_item_sold_vat_dr)
			{
				_zWhereClause += z_sep+"item_sold_vat_dr='"+item_sold_vat_dr() + "'";
				z_sep=" AND ";
			}
		}
		public String service_expense_dr()
		{
			return m_service_expense_dr;
		}
		public void service_expense_dr(String value)
		{
			z_bool.m_service_expense_dr = true;
			m_service_expense_dr = value;
			if (z_bool.m_service_expense_dr)
			{
				_zWhereClause += z_sep+"service_expense_dr='"+service_expense_dr() + "'";
				z_sep=" AND ";
			}
		}
		public String item_purchased_expense_dr()
		{
			return m_item_purchased_expense_dr;
		}
		public void item_purchased_expense_dr(String value)
		{
			z_bool.m_item_purchased_expense_dr = true;
			m_item_purchased_expense_dr = value;
			if (z_bool.m_item_purchased_expense_dr)
			{
				_zWhereClause += z_sep+"item_purchased_expense_dr='"+item_purchased_expense_dr() + "'";
				z_sep=" AND ";
			}
		}
		public String item_sold_credit_dr()
		{
			return m_item_sold_credit_dr;
		}
		public void item_sold_credit_dr(String value)
		{
			z_bool.m_item_sold_credit_dr = true;
			m_item_sold_credit_dr = value;
			if (z_bool.m_item_sold_credit_dr)
			{
				_zWhereClause += z_sep+"item_sold_credit_dr='"+item_sold_credit_dr() + "'";
				z_sep=" AND ";
			}
		}
		public String item_sold_cr()
		{
			return m_item_sold_cr;
		}
		public void item_sold_cr(String value)
		{
			z_bool.m_item_sold_cr = true;
			m_item_sold_cr = value;
			if (z_bool.m_item_sold_cr)
			{
				_zWhereClause += z_sep+"item_sold_cr='"+item_sold_cr() + "'";
				z_sep=" AND ";
			}
		}
		public String service_earned_cr()
		{
			return m_service_earned_cr;
		}
		public void service_earned_cr(String value)
		{
			z_bool.m_service_earned_cr = true;
			m_service_earned_cr = value;
			if (z_bool.m_service_earned_cr)
			{
				_zWhereClause += z_sep+"service_earned_cr='"+service_earned_cr() + "'";
				z_sep=" AND ";
			}
		}
		public String item_purchased_dr()
		{
			return m_item_purchased_dr;
		}
		public void item_purchased_dr(String value)
		{
			z_bool.m_item_purchased_dr = true;
			m_item_purchased_dr = value;
			if (z_bool.m_item_purchased_dr)
			{
				_zWhereClause += z_sep+"item_purchased_dr='"+item_purchased_dr() + "'";
				z_sep=" AND ";
			}
		}
		public String item_purchased_credit_cr()
		{
			return m_item_purchased_credit_cr;
		}
		public void item_purchased_credit_cr(String value)
		{
			z_bool.m_item_purchased_credit_cr = true;
			m_item_purchased_credit_cr = value;
			if (z_bool.m_item_purchased_credit_cr)
			{
				_zWhereClause += z_sep+"item_purchased_credit_cr='"+item_purchased_credit_cr() + "'";
				z_sep=" AND ";
			}
		}
		public String service_earned_dr()
		{
			return m_service_earned_dr;
		}
		public void service_earned_dr(String value)
		{
			z_bool.m_service_earned_dr = true;
			m_service_earned_dr = value;
			if (z_bool.m_service_earned_dr)
			{
				_zWhereClause += z_sep+"service_earned_dr='"+service_earned_dr() + "'";
				z_sep=" AND ";
			}
		}
		public String item_sold_discount_dr()
		{
			return m_item_sold_discount_dr;
		}
		public void item_sold_discount_dr(String value)
		{
			z_bool.m_item_sold_discount_dr = true;
			m_item_sold_discount_dr = value;
			if (z_bool.m_item_sold_discount_dr)
			{
				_zWhereClause += z_sep+"item_sold_discount_dr='"+item_sold_discount_dr() + "'";
				z_sep=" AND ";
			}
		}
		public String item_sold_vat_cr()
		{
			return m_item_sold_vat_cr;
		}
		public void item_sold_vat_cr(String value)
		{
			z_bool.m_item_sold_vat_cr = true;
			m_item_sold_vat_cr = value;
			if (z_bool.m_item_sold_vat_cr)
			{
				_zWhereClause += z_sep+"item_sold_vat_cr='"+item_sold_vat_cr() + "'";
				z_sep=" AND ";
			}
		}
		public String item_sold_discount_cr()
		{
			return m_item_sold_discount_cr;
		}
		public void item_sold_discount_cr(String value)
		{
			z_bool.m_item_sold_discount_cr = true;
			m_item_sold_discount_cr = value;
			if (z_bool.m_item_sold_discount_cr)
			{
				_zWhereClause += z_sep+"item_sold_discount_cr='"+item_sold_discount_cr() + "'";
				z_sep=" AND ";
			}
		}
		public String item_purchased_expense_cr()
		{
			return m_item_purchased_expense_cr;
		}
		public void item_purchased_expense_cr(String value)
		{
			z_bool.m_item_purchased_expense_cr = true;
			m_item_purchased_expense_cr = value;
			if (z_bool.m_item_purchased_expense_cr)
			{
				_zWhereClause += z_sep+"item_purchased_expense_cr='"+item_purchased_expense_cr() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}