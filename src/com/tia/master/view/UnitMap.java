package com.tia.master.view;


import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.GridLayout;

import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.BeforeSaveMessages;
import com.tia.common.util.CmbQry;
import com.tia.common.util.DecimalTextPositiveOnly;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.common.util.Validator;
import com.tia.master.controller.UnitMapController;
import com.tia.master.model.UnitMapModel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;

public class UnitMap extends ViewPart {
	private Text txtUnitName;
	private SwtBndCombo bcmbStatus;
	private Table tblDetails;
	private Text txtConversionRatio;
	private Button cmdNew;
	private Button cmdSave;
	private Button cmdCancel;
	private Button cmdEdit;
	private Button cmdDelete;
	private Composite comParent;
	Shell sh = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	UnitMapModel objUnitModel = new UnitMapModel();
	BeforeSaveMessages msg=new BeforeSaveMessages();
	static Connection cnn = DbConnection.getConnection();
	Validator validator = new Validator();
	UnitMapController unitMapCntrl = new UnitMapController();
	int unitId=0;
	private SwtBndCombo bcmbBaseUnit;

	public UnitMap() {
		setTitleImage(SWTResourceManager.getImage(UnitMap.class, "/unit16.png"));

	}

	@Override
	public void createPartControl(Composite parent) {

		comParent = new Composite(parent, SWT.NONE);
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("Unit Management Form", comParent, UnitMap.class);

		Composite cmpMain = new Composite(comParent, SWT.NONE);
		GridData gd_cmpMain = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmpMain.heightHint = 502;
		cmpMain.setLayoutData(gd_cmpMain);
		Group grpPersonalInfo = new Group(cmpMain, SWT.NONE);
		grpPersonalInfo.setText("Unit Info");
		grpPersonalInfo.setBounds(10, 10, 972, 95);

		Label lblFullName = new Label(grpPersonalInfo, SWT.NONE);
		lblFullName.setAlignment(SWT.RIGHT);
		lblFullName.setBounds(346, 30, 100, 15);
		lblFullName.setText("Unit Name :");
		lblFullName.setForeground(UtilMethods.getFontForegroundColorRed());

		txtUnitName = new Text(grpPersonalInfo, SWT.BORDER);
		txtUnitName.setBounds(452, 27, 199, 21);
		txtUnitName.setEnabled(false);

		Label lblStatus = new Label(grpPersonalInfo, SWT.NONE);
		lblStatus.setText("Status :");
		lblStatus.setAlignment(SWT.RIGHT);
		lblStatus.setBounds(37, 59, 85, 15);

		bcmbStatus = new SwtBndCombo(grpPersonalInfo, SWT.READ_ONLY);
		bcmbStatus.setBounds(128, 57, 199, 21);
		bcmbStatus.setEnabled(false);
		bcmbStatus.fillData(CmbQry.getStatus(), CmbQry.getLabel(),
 				CmbQry.getId(), DbConnection.getConnection(),false);

		Label lblEmail = new Label(grpPersonalInfo, SWT.NONE);
		lblEmail.setText("Conversion Ratio :");
		lblEmail.setAlignment(SWT.RIGHT);
		lblEmail.setBounds(657, 30, 100, 15);
		lblEmail.setForeground(UtilMethods.getFontForegroundColorRed());

		txtConversionRatio = new Text(grpPersonalInfo, SWT.BORDER);
		txtConversionRatio.setBounds(763, 27, 199, 21);
		txtConversionRatio.setEnabled(false);
		txtConversionRatio.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtConversionRatio, 20));
		
		bcmbBaseUnit = new SwtBndCombo(grpPersonalInfo, SWT.READ_ONLY);
		
		bcmbBaseUnit.setBounds(128, 24, 199, 23);
		bcmbBaseUnit
		.fillData(CmbQry.getBaseUnit(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection());
		
		Label lblType = new Label(grpPersonalInfo, SWT.NONE);
		lblType.setAlignment(SWT.RIGHT);
		lblType.setBounds(28, 29, 94, 15);
		lblType.setText("Base Unit");
		lblType.setForeground(UtilMethods.getFontForegroundColorRed());
		
		grpPersonalInfo.setTabList(new Control[]{txtUnitName, txtConversionRatio, bcmbStatus});

		Group grpControls = new Group(cmpMain, SWT.NONE);
		grpControls.setBounds(10, 111, 517, 68);

		cmdNew = new Button(grpControls, SWT.NONE);
		cmdNew.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				if(ApplicationWorkbenchWindowAdvisor
						.hasAuthOr(new String[] { "unit_add" })) {
					SetFieldDisable();
					SetFieldEnabled();
					unitId=0;
					cmdNew.setEnabled(false);
					cmdDelete.setEnabled(false);
					//txtUnitName.forceFocus();
				}
			}
		});
		cmdNew.setBounds(23, 25, 75, 25);
		cmdNew.setText("New");

		cmdEdit = new Button(grpControls, SWT.NONE);
		cmdEdit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String currentUser = ApplicationWorkbenchWindowAdvisor
						.getCurrentUser();
				System.out.println(currentUser);
			
				if(ApplicationWorkbenchWindowAdvisor
						.hasAuthOr(new String[] { "unit_update" })){
					SetFieldEnabled();
					cmdEdit.setEnabled(false);
					cmdNew.setEnabled(false);
				} 
			}
		});
		cmdEdit.setEnabled(false);
		cmdEdit.setText("Edit");
		cmdEdit.setBounds(110, 25, 75, 25);

		cmdDelete = new Button(grpControls, SWT.NONE);
		cmdDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(ApplicationWorkbenchWindowAdvisor
						.hasAuthOr(new String[] { "unit_update" })){
					if(tblDetails.getSelectionIndex() >=0){
					
					TableItem selection[] = tblDetails.getSelection();
					TableItem selectedRow = selection[0];
					String rowValue = selectedRow.getText(0);
					int unitid=Integer.valueOf(rowValue);

				MessageBox quesBox = new MessageBox(sh, SWT.YES | SWT.NO);
				quesBox.setText("Confirm Delete");
				quesBox.setMessage("Are u sure u want to Delete?");

				if (quesBox.open() == SWT.YES) {

				
					if (unitMapCntrl.DeleteUnit(unitid))
					{
						String Message = "Sucessfully Deleted";
						MessageBoxInformation(Message);
						tblDetails.removeAll();
						FillTableDetails();
						SetFieldDisable();
						cmdNew.setEnabled(true);
					}
				}
				}
					else
					{
						msg.showIndividualMessage("Please select the row to delete", 0);
					}
			}
			}
		});
		cmdDelete.setEnabled(false);
		cmdDelete.setText("Delete");
		cmdDelete.setBounds(194, 25, 75, 25);

		cmdCancel = new Button(grpControls, SWT.NONE);
		cmdCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				SetFieldDisable();
				cmdNew.setEnabled(true);
			}
		});
		cmdCancel.setText("Cancel");
		cmdCancel.setBounds(416, 25, 75, 25);
		cmdSave = new Button(grpControls, SWT.NONE);
		cmdSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				checkSave();
				if (msg.returnMessages()) {
					
					MessageBox quesBox = new MessageBox(sh, SWT.YES | SWT.NO);
					quesBox.setMessage("Confirm Save");
					quesBox.setMessage("Are u sure u want to Save?");

					objUnitModel.setUnit_name(txtUnitName.getText());
					
					objUnitModel.setBase_unit(Integer.valueOf(bcmbBaseUnit.getSelectedBoundValue().toString()));
					objUnitModel.setConversion_ration(txtConversionRatio.getText());
					objUnitModel.setStatus(Integer.valueOf(bcmbStatus.getSelectedBoundValue().toString()));
					try {
						String strQry = "";
						java.sql.Statement st = cnn.createStatement(
								ResultSet.TYPE_SCROLL_SENSITIVE,
								ResultSet.CONCUR_UPDATABLE);
						if (unitId > 0) {
							
							objUnitModel.z_WhereClause
									.unit_id(unitId);
							strQry = objUnitModel.Update();
						} else {
							
							strQry = objUnitModel.Insert();
						}
						System.out.println(strQry);
						st.executeUpdate(strQry);
						String message = "sucessfully Saved/Updated";
						MessageBoxInformation(message);
						tblDetails.removeAll();
						FillTableDetails();
						SetFieldDisable();
						cmdNew.setEnabled(true);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			}
		});
		cmdSave.setEnabled(false);
		cmdSave.setBounds(329, 25, 75, 25);
		cmdSave.setText("Save");

		tblDetails = new Table(cmpMain, SWT.BORDER | SWT.FULL_SELECTION);
		tblDetails.setLocation(10, 192);
		tblDetails.setSize(972, 300);
		tblDetails.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				cmdSave.setEnabled(false);
				SetFieldDisable();
				unitId=Integer.valueOf(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(0));
				txtUnitName.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(2));
				
				txtConversionRatio.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(4));
				bcmbStatus.setSelectedBoundValue(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(6));
				bcmbBaseUnit.setSelectedBoundValue(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(7));
				
				// String password = UtilFxn.GetValueFromTable("ntb_user",
				// "login_pwd",
				// "user_id = '"+Integer.parseInt(txtUserId.getText())+"'");
				cmdEdit.setEnabled(true);
			}
		});
		tblDetails.setHeaderVisible(true);
		tblDetails.setLinesVisible(true);

		TableColumn tblclmnId = new TableColumn(tblDetails, SWT.NONE);
		tblclmnId.setWidth(0);
		tblclmnId.setText("id");
		
		TableColumn tblclmnUserId = new TableColumn(tblDetails, SWT.NONE);
		tblclmnUserId.setWidth(51);
		tblclmnUserId.setText("S. No.");

		TableColumn tblclmnLoginName = new TableColumn(tblDetails, SWT.NONE);
		tblclmnLoginName.setWidth(225);
		tblclmnLoginName.setText("Unit Name");

		TableColumn tblclmnFullName = new TableColumn(tblDetails, SWT.NONE);
		tblclmnFullName.setWidth(207);
		tblclmnFullName.setText("Base Unit");

		TableColumn tblclmnAdress = new TableColumn(tblDetails, SWT.NONE);
		tblclmnAdress.setWidth(139);
		tblclmnAdress.setText("Conversion Ratio");

		TableColumn tblclmnEmail = new TableColumn(tblDetails, SWT.NONE);
		tblclmnEmail.setWidth(126);
		tblclmnEmail.setText("Status");
		
		TableColumn tblStatusId = new TableColumn(tblDetails, SWT.NONE);
		tblStatusId.setWidth(0);
		tblStatusId.setText("Status");
		
		TableColumn tblTypeId = new TableColumn(tblDetails, SWT.NONE);
		tblTypeId.setWidth(0);
		tblTypeId.setText("Status");

		
		// TODO Auto-generated method stub
		FillTableDetails();

	}

	protected void MessageBoxInformation(String message) {
		MessageBox mbInfo = new MessageBox(sh, SWT.ICON_INFORMATION);
		mbInfo.setText("Sucess");
		mbInfo.setMessage(message);
		mbInfo.open();

	}

	protected void checkSave() {
		
		if (bcmbBaseUnit.getSelectedBoundValue().equals("0")) {
			msg.setMandatoryMessages("Base Unit is required");
		}
		if (txtUnitName.getText().equals("")) {
			msg.setMandatoryMessages("Unit name is required");
		}
		else
		{
			
		}
		

		if(txtConversionRatio.getText().equals(""))
		{
			msg.setMandatoryMessages("Conversion ratio is required");
		}
		else if(Double.parseDouble(txtConversionRatio.getText()) <= 0)
		{
			msg.setMandatoryMessages("Conversion ratio must be greater than zero");
		}

		if (bcmbStatus.getSelectedBoundValue().equals("0")) {
			msg.setMandatoryMessages("Status is required");
		}
		
	}

	protected void SetFieldEnabled() {
		
		bcmbStatus.setEnabled(true);
		bcmbBaseUnit.setEnabled(true);
		txtConversionRatio.setEnabled(true);
		txtUnitName.setEnabled(true);
		
		//txtBaseUnit.setEnabled(true);
		
		cmdSave.setEnabled(true);
		cmdDelete.setEnabled(true);
		
	}

	protected void SetFieldDisable() {
		
		bcmbStatus.setSelectedBoundValue("0");
		bcmbBaseUnit.setSelectedBoundValue("0");
		txtConversionRatio.setText("");
		txtUnitName.setText("");
		
	
		
		bcmbStatus.setEnabled(false);
		bcmbBaseUnit.setEnabled(false);
		txtConversionRatio.setEnabled(false);
		txtUnitName.setEnabled(false);
		
		
	
		cmdSave.setEnabled(false);
		cmdDelete.setEnabled(false);
		cmdEdit.setEnabled(false);
		

	}

	private void FillTableDetails() {
		tblDetails.removeAll();
		ResultSet rsUnitDetail = unitMapCntrl.GetUnitDetail(0);
		try {
			if (rsUnitDetail == null) {
				TableItem item = new TableItem(tblDetails, SWT.NONE);
				item.setText(0, "");
				item.setText(1, "");
				item.setText(2, "");
				item.setText(3, "");
				item.setText(4, "");
				item.setText(5, "");
				item.setText(6, "");
				item.setText(7, "");

			} else
				do {
					TableItem item = new TableItem(tblDetails, SWT.NONE);
					item.setText(0, String.valueOf(rsUnitDetail.getInt("unit_id")));

					item.setText(1, String.valueOf(tblDetails.getItemCount()));
					
					item.setText(2, rsUnitDetail.getString("unit_name"));
					
					item.setText(3, rsUnitDetail.getString("base_unit_label"));
					item.setText(4, String.valueOf(rsUnitDetail.getBigDecimal("conversion_ration")));
					item.setText(5, rsUnitDetail.getInt("status")==1?"Active":"Inactive");
					item.setText(6, String.valueOf(rsUnitDetail.getInt("status")));
					item.setText(7, String.valueOf(rsUnitDetail.getInt("base_unit")));

				} while (rsUnitDetail.next());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// public static String encryptPassword( String password )
	// {
	// String encrypted = "";
	// try
	// {
	// MessageDigest digest = MessageDigest.getInstance( "MD5" );
	// byte[] passwordBytes = password.getBytes( );
	// digest.reset( );
	// digest.update( passwordBytes );
	// byte[] message = digest.digest( );
	//
	// StringBuffer hexString = new StringBuffer();
	// for ( int i=0; i < message.length; i++)
	// {
	// hexString.append( Integer.toHexString(
	// 0xFF & message[ i ] ) );
	// }
	// encrypted = hexString.toString();
	// }
	// catch( Exception e ) { }
	// return encrypted;
	// }

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}
}

