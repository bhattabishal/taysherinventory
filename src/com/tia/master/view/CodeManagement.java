package com.tia.master.view;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.swtdesigner.SWTResourceManager;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.BeforeSaveMessages;
import com.tia.common.util.CmbQry;
import com.tia.common.util.DecimalTextPositiveOnly;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.master.controller.AccountSettingController;
import com.tia.master.controller.CodeValueController;
import com.tia.master.model.AccountSettingModel;
import com.tia.master.model.CodeValueModel;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Combo;

public class CodeManagement extends ViewPart {
	private final Shell sh;
	private Table tblAccCode;
	private Table tblMember;
	private Table tblLoan;
	private Table tblAccount;
	private Table tblOther;
	private Text txtAccCode;
	private Text txtMember;
	private Table tblSaving;
	private CTabFolder tabFolder;
	private CTabItem tbtmAccountCode;
	private CTabItem tbtmMember;
	private CTabItem tbtmAccount;
	private CTabItem tbtmLoan;
	private CTabItem tbtmSaving;
	private CTabItem tbtmOthers;
	private Button btnACAdd;
	private Button btnMAdd;
	private Button btnMEdit;
	private Button btnACDelete;
	private Button btnMSave;
	private Button btnMClear;
	private Button btnSSave;
	private Button btnSClear;
	private Button btnOAdd;
	private Button btnOEdit;
	private Button btnOSave;
	private Button btnOClear;
	private Button btnLAdd;
	private Button btnLEdit;
	private Button btnLSave;
	private Button btnLClear;
	private Button btnAAdd;
	private Button btnASave;
	private Button btnAEdit;
	private Button btnAClear;
	private Text txtCodeLabel; 
	private Text txtLCodeLabel;
	private Text txtACodeLabel;
	private Text txtSCodeLabel;
	private Text txtOCodeLabel;
	private String cv_type; 
	private String cv_id;
	private String cv_code;
	private String cv_lbl;
	//for acc code tab1 work
	String accCode="";
	String member="";
	private SwtBndCombo bcmbTrxnType;
	private final CodeValueController objCodeValueController=new CodeValueController();
	private final CodeValueModel objCodeModel=new CodeValueModel();
	private final JCalendarFunctions jfxn=new JCalendarFunctions();
	private BeforeSaveMessages msg=new BeforeSaveMessages();
	private int accId=0;
	private CTabItem tbtmAccSetting;
	private SwtBndCombo bcmbItemPurchased;
	private SwtBndCombo bcmbItemPurchaseCredit;
	private Group grpItem;
	private Group grpItemPurchase;
	private SwtBndCombo bcmbItemPurchasedExpensesCr;
	private SwtBndCombo bcmbItemSold;
	private SwtBndCombo bcmbItemSoldVatDr;
	private SwtBndCombo bcmbItemSoldDiscountDr;
	private SwtBndCombo bcmbItemSoldCredit;
	private SwtBndCombo bcmbServiceEarnedDr;
	private SwtBndCombo bcmbServicePaidDr;
	private SwtBndCombo bcmbItemSoldDiscountCr;
	private SwtBndCombo bcmbServiceEarnedCr;
	private SwtBndCombo bcmbServicePaidCr;
	private SwtBndCombo bcmbItemSoldVatCr;
	private SwtBndCombo bcmbItemPurchasedExpensesDr;
	//end of tab1 variables
	/**
	  * The constructor.
	  */
	 public CodeManagement() {
		 setTitleImage(SWTResourceManager.getImage(CodeManagement.class,
			"/keysetting16.png"));
		 sh = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		// image = new Image(sh.getDisplay(), "/list-information.png");
		 cv_id = "";
		 cv_type = "";
	 }
	 
	 /**
	  * This is a callback that will allow us to create the viewer and
	  * initialize it.
	  */
	 @Override
	public void createPartControl(Composite parent) {
		// set the Gridlayout to the main form
			GridLayout gl_parent1 = new GridLayout(1, false);
			gl_parent1.marginHeight = 1;
			parent.setLayout(gl_parent1);
			Banner.getTitle("Key Setting Management", parent, CodeManagement.class);
	 	Composite composite = new Composite(parent, SWT.NONE);
	 	
	 	Composite Composite = new Composite(composite, SWT.NONE);
	 	Composite.setBounds(0, 0, 970, 575);
	 	
	 	tabFolder = new CTabFolder(Composite, SWT.BORDER);
	 	tabFolder.setBounds(16, 0, 951, 521);
	 	tabFolder.setSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));
	 	tabFolder.setTabHeight(20);
	 	tabFolder.setSimple(false);
	 	tabFolder.setBorderVisible(false);
				
		
		
		tbtmSaving = new CTabItem(tabFolder, SWT.NONE);
		tbtmSaving.setText("Vat/Discount");
		
		
		tbtmOthers = new CTabItem(tabFolder, SWT.NONE);
		tbtmOthers.setText("Code Values");
		
		//tbtmAccSetting = new CTabItem(tabFolder, SWT.NONE);
		//tbtmAccSetting.setText("Account Setting");
		
		
		
		//load tab1 contents
		loadOtherTab();
	 	loadSavingTab();
	 	//loadAccSettingTab();
	 	
	 	tabFolder.setSelection(tbtmSaving);
	 
	 }
	 
	 private void loadSavingTab()
	 {
		    Composite comSaving = new Composite(tabFolder, SWT.NONE);
			tbtmSaving.setControl(comSaving);
			
			FormData fd_tree1 = new FormData();
			fd_tree1.top = new FormAttachment(0, 10);
			fd_tree1.bottom = new FormAttachment(100, -10);
			fd_tree1.right = new FormAttachment(100, -10);
			fd_tree1.left = new FormAttachment(0, 10);
			comSaving.setLayout(null);
		
			Composite comSavTree = new Composite(comSaving, SWT.NONE);
			comSavTree.setBounds(5, 5, 254, 491);
			comSavTree.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 3));
			
			// tree displaying the account heads
			final Tree tree = new Tree(comSavTree, SWT.SINGLE | SWT.BORDER);
			tree.setBounds(10, 5, 234, 476);
			
			
			
			TreeItem trtmStatus = new TreeItem(tree, SWT.NONE);
			trtmStatus.setText("Sales");
			
			
			
			tree.addListener(SWT.Selection, new Listener() {
			      @Override
				public void handleEvent(Event e1) {
			    	
			    	btnSClear.setEnabled(false);
			    	btnSSave.setEnabled(false);
			    	String strSelected = "";
			        TreeItem[] selection = tree.getSelection();
			        for (int i = 0; i < selection.length; i++)
			        	strSelected += selection[i].getText() + " ";
			          System.out.println("Selection={" + strSelected + "}");
			          String ctype = strSelected.trim();
			          if(ctype.equals("Sales")){
			    		  cv_type = "Sales";			        	  
			          }
			          
			          fillTableSaving();			          
			      }
			    });

			Composite comSav1 = new Composite(comSaving, SWT.NONE);
			comSav1.setBounds(264, 5, 248, 481);
			
			
			btnSSave = new Button(comSav1, SWT.NONE);
			btnSSave.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if(txtSCodeLabel.getText().equals(""))
					{
						msg.showIndividualMessage("Please enter value to save", 0);
					
						
					}
					else if(tblSaving.getSelectionIndex() < 0)
					{
						msg.showIndividualMessage("Please select table row to edit and save", 0);
					}
					else
					{
						saveSavingCodeValue();	
					}
				}
			});
			btnSSave.setEnabled(false);
			btnSSave.setBounds(86, 34, 75, 25);
			btnSSave.setText("Save");
			
			
			btnSClear = new Button(comSav1, SWT.NONE);
			btnSClear.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					txtSCodeLabel.setText("");
				}
			});
			btnSClear.setEnabled(false);
			btnSClear.setBounds(86, 65, 75, 25);
			btnSClear.setText("Clear");
			
			
			txtSCodeLabel = new Text(comSav1, SWT.BORDER);
			txtSCodeLabel.addListener(SWT.Verify, new DecimalTextPositiveOnly(
					txtSCodeLabel, 20));
			
			txtSCodeLabel.setEnabled(false);
			txtSCodeLabel.setBounds(72, 5, 157, 23);
			
			Label lblCodeLabel = new Label(comSav1, SWT.NONE);
			lblCodeLabel.setAlignment(SWT.RIGHT);
			lblCodeLabel.setBounds(12, 7, 54, 18);
			lblCodeLabel.setText("Values");
			
			
//			Label lblCodeValue = new Label(comMem1, SWT.NONE);
//			lblCodeValue.setBounds(26, 41, 67, 15);
//			lblCodeValue.setText("Code Value:");
			
//			txtMember = new Text(comMem1, SWT.BORDER);
//			txtMember.setEnabled(false);
//			txtMember.setBounds(100, 38, 157, 21);
			
			Composite comSav = new Composite(comSaving, SWT.NONE);
			comSav.setBounds(518, 5, 417, 481);
			comSav.setLayout(null);
			
			tblSaving = new Table(comSav, SWT.BORDER | SWT.FULL_SELECTION);
			tblSaving.setBounds(7, 5, 405, 476);
			
			tblSaving.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
				//	btnACDelete.setEnabled(true);
					txtSCodeLabel.setText(tblSaving.getItem(tblSaving.getSelectionIndex()).getText(2));
					//txtMember.setText(tblMember.getItem(tblMember.getSelectionIndex()).getText(2));
				
					cv_code = tblSaving.getItem(tblSaving.getSelectionIndex()).getText(2);
					cv_id = tblSaving.getItem(tblSaving.getSelectionIndex()).getText(3);
					cv_type = tblSaving.getItem(tblSaving.getSelectionIndex()).getText(4);
					cv_lbl=tblSaving.getItem(tblSaving.getSelectionIndex()).getText(1);
					//System.out.println("this ti sthe sdakljfkl"+cv_type);
					btnSSave.setEnabled(true);
					btnSClear.setEnabled(true);
					txtSCodeLabel.setEnabled(true);
				}
			});
			tblSaving.setHeaderVisible(true);
			tblSaving.setLinesVisible(true);
			tblSaving.removeAll();
			
			TableColumn tblclmnSn = new TableColumn(tblSaving, SWT.NONE);
			tblclmnSn.setWidth(39);
			tblclmnSn.setText("S.No");
			
			TableColumn tblclmnTransactionType = new TableColumn(tblSaving, SWT.NONE);
			tblclmnTransactionType.setWidth(145);
			tblclmnTransactionType.setText("CodeLabel");
			
			TableColumn tblclmnAccountCode = new TableColumn(tblSaving, SWT.NONE);
			tblclmnAccountCode.setWidth(216);
			tblclmnAccountCode.setText("CodeValue");
			
			TableColumn id = new TableColumn(tblSaving, SWT.NONE);
			id.setWidth(1);
			id.setText("id");
			
			TableColumn type = new TableColumn(tblSaving, SWT.NONE);
			id.setWidth(1);
			id.setText("type");
			
			cv_type="Sales";
			fillTableSaving();
	 }
	 
	 private void loadOtherTab()
	 {
		    Composite comOther = new Composite(tabFolder, SWT.NONE);
		    tbtmOthers.setControl(comOther);
			
			FormData fd_tree1 = new FormData();
			fd_tree1.top = new FormAttachment(0, 10);
			fd_tree1.bottom = new FormAttachment(100, -10);
			fd_tree1.right = new FormAttachment(100, -10);
			fd_tree1.left = new FormAttachment(0, 10);
			comOther.setLayout(null);
		
			Composite comOthTree = new Composite(comOther, SWT.NONE);
			comOthTree.setBounds(5, 5, 254, 481);
			comOthTree.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 3));
			
			// tree displaying the account heads
			final Tree tree = new Tree(comOthTree, SWT.SINGLE | SWT.BORDER);
			tree.setBounds(10, 5, 234, 476);
			
			
			
			TreeItem trtmEducation = new TreeItem(tree, SWT.NONE);
			trtmEducation.setText("Item Type");
			
			TreeItem trtmOccupation = new TreeItem(tree, SWT.NONE);
			trtmOccupation.setText("Item Brand");
			trtmOccupation.setExpanded(true);
			
			TreeItem trtmPurchaseExpenses = new TreeItem(tree, SWT.NONE);
			trtmPurchaseExpenses.setText("Item Purchase Expenses");
			
			TreeItem trtmCash = new TreeItem(tree, SWT.NONE);
			trtmCash.setText("Cash");
			
			TreeItem trtmBank = new TreeItem(tree, SWT.NONE);
			trtmBank.setText("Bank");
			
			TreeItem trtmOterExpenses = new TreeItem(tree, SWT.NONE);
			trtmOterExpenses.setText("Other Trxn Types");
			
			
			
			tree.addListener(SWT.Selection, new Listener() {
			      @Override
				public void handleEvent(Event e1) {
			    	btnOEdit.setEnabled(false);
			    	btnOAdd.setEnabled(false);
			    	btnOClear.setEnabled(false);
			    	btnOSave.setEnabled(false);
			    	String strSelected = "";
			        TreeItem[] selection = tree.getSelection();
			        for (int i = 0; i < selection.length; i++)
			        	strSelected += selection[i].getText() + " ";
			          System.out.println("Selection={" + strSelected + "}");
			          String ctype = strSelected.trim();
			          if(ctype.equals("Item Type")){
			    		  cv_type = "Item Type";			        	  
			          }
			          else if(ctype.equals("Item Brand")){
			        	  cv_type = "Item Brand";			        	  
			          }
			          else if(ctype.equals("Item Purchase Expenses")){
			        	  cv_type = "purexpense";			        	  
			          }
			          else if(ctype.equals("Other Trxn Types")){
			        	  cv_type = "other_trxn";			        	  
			          }
			          else if(ctype.equals("Cash")){
			        	  cv_type = "cash";			        	  
			          }
			          else if(ctype.equals("Bank")){
			        	  cv_type = "bank";			        	  
			          }
			          
			          
			          fillTableOther();			          
			      }
			    });

			Composite comother2 = new Composite(comOther, SWT.NONE);
			comother2.setBounds(264, 5, 248, 481);
						
			
			btnOAdd = new Button(comother2, SWT.NONE);
			btnOAdd.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
//					//first check if condition meets to save
					clear();
					//txtMember.setEnabled(true);
					txtOCodeLabel.setEnabled(true);
					btnOSave.setEnabled(true);
					btnOClear.setEnabled(true);
					btnOAdd.setEnabled(false);
					tblOther.setSelection(-1);		
					btnOEdit.setEnabled(false);
				}
			});
			btnOAdd.setEnabled(false);
			btnOAdd.setBounds(86, 34, 75, 25);
			btnOAdd.setText("Add");
			
			
			btnOSave = new Button(comother2, SWT.NONE);
			btnOSave.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if(txtOCodeLabel.getText().equals(""))
					{
						msg.showIndividualMessage("Please enter label value to save", 0);		
					}
					else
						saveOtherCodeValue();
				}
			});
			btnOSave.setEnabled(false);
			btnOSave.setBounds(86, 65, 75, 25);
			btnOSave.setText("Save");
			
			
			btnOEdit = new Button(comother2, SWT.NONE);
			btnOEdit.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if(tblOther.getSelectionIndex() >= 0)
					{
					txtOCodeLabel.setEnabled(true);
					btnOSave.setEnabled(true);
					btnOAdd.setEnabled(false);
					}
					else
					{
						msg.showIndividualMessage("Please select table row to edit.", 0);
					}
				}
			});
			btnOEdit.setEnabled(false);
			btnOEdit.setBounds(86, 96, 75, 25);
			btnOEdit.setText("Edit");
			
			
			btnOClear = new Button(comother2, SWT.NONE);
			btnOClear.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					clear();
				}
			});
			btnOClear.setEnabled(false);
			btnOClear.setBounds(86, 129, 75, 25);
			btnOClear.setText("Clear");
			
			
			txtOCodeLabel = new Text(comother2, SWT.BORDER);
			txtOCodeLabel.setEnabled(false);
			txtOCodeLabel.setBounds(72, 5, 157, 23);
			
			Label lblCodeLabel = new Label(comother2, SWT.NONE);
			lblCodeLabel.setAlignment(SWT.RIGHT);
			lblCodeLabel.setBounds(3, 7, 66, 18);
			lblCodeLabel.setText("CodeLabel");
			
			
			Composite gd_tblOther = new Composite(comOther, SWT.NONE);
			gd_tblOther.setBounds(518, 5, 417, 481);
			gd_tblOther.setLayout(null);
			
			tblOther = new Table(gd_tblOther, SWT.BORDER | SWT.FULL_SELECTION);
			tblOther.setBounds(7, 5, 405, 476);
			tblOther.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
				//	btnACDelete.setEnabled(true);
					txtOCodeLabel.setText(tblOther.getItem(tblOther.getSelectionIndex()).getText(1));
					//txtMember.setText(tblMember.getItem(tblMember.getSelectionIndex()).getText(2));
					btnOEdit.setEnabled(true);
					btnOAdd.setEnabled(true);
					cv_code = tblOther.getItem(tblOther.getSelectionIndex()).getText(2);
					cv_id = tblOther.getItem(tblOther.getSelectionIndex()).getText(3);
					cv_type = tblOther.getItem(tblOther.getSelectionIndex()).getText(4);
					//System.out.println("this ti sthe sdakljfkl"+cv_type);
					btnOSave.setEnabled(false);
					txtOCodeLabel.setEnabled(false);
				}
			});
			tblOther.setHeaderVisible(true);
			tblOther.setLinesVisible(true);
			tblOther.removeAll();
			
			TableColumn tblclmnSn = new TableColumn(tblOther, SWT.NONE);
			tblclmnSn.setWidth(39);
			tblclmnSn.setText("S.No");
			
			TableColumn tblclmnTransactionType = new TableColumn(tblOther, SWT.NONE);
			tblclmnTransactionType.setWidth(145);
			tblclmnTransactionType.setText("CodeLabel");
			
			TableColumn tblclmnAccountCode = new TableColumn(tblOther, SWT.NONE);
			tblclmnAccountCode.setWidth(216);
			tblclmnAccountCode.setText("CodeValue");
			
			TableColumn id = new TableColumn(tblOther, SWT.NONE);
			id.setWidth(1);
			id.setText("id");
			
			TableColumn type = new TableColumn(tblOther, SWT.NONE);
			id.setWidth(1);
			id.setText("type");
	 }
	 
	 
	 private void loadAccSettingTab()
	 {
		 Composite comAcc = new Composite(tabFolder, SWT.NONE);
		    tbtmAccSetting.setControl(comAcc);
			
			FormData fd_tree1 = new FormData();
			fd_tree1.top = new FormAttachment(0, 10);
			fd_tree1.bottom = new FormAttachment(100, -10);
			fd_tree1.right = new FormAttachment(100, -10);
			fd_tree1.left = new FormAttachment(0, 10);
			comAcc.setLayout(null);
			
			grpItemPurchase = new Group(comAcc, SWT.NONE);
			grpItemPurchase.setText("Item Purchase");
			grpItemPurchase.setBounds(10, 10, 927, 129);
			
			grpItem = new Group(grpItemPurchase, SWT.NONE);
			grpItem.setText("Item");
			grpItem.setBounds(10, 32, 186, 74);
			
			Label lblDr = new Label(grpItem, SWT.NONE);
			lblDr.setAlignment(SWT.RIGHT);
			lblDr.setBounds(10, 36, 32, 15);
			lblDr.setText("Dr.: ");
			
			bcmbItemPurchased = new SwtBndCombo(grpItem, SWT.READ_ONLY);
			bcmbItemPurchased.setBounds(42, 33, 134, 23);
			bcmbItemPurchased.fillData(CmbQry.getAccountByAccountName(),
					CmbQry.getLabel(), CmbQry.getId(),
					DbConnection.getConnection(), true);
			
			Group grpExpenses = new Group(grpItemPurchase, SWT.NONE);
			grpExpenses.setText("Expenses");
			grpExpenses.setBounds(202, 32, 191, 74);
			
			Label lblCr = new Label(grpExpenses, SWT.NONE);
			lblCr.setAlignment(SWT.RIGHT);
			lblCr.setBounds(10, 49, 27, 15);
			lblCr.setText("Cr.: ");
			
			bcmbItemPurchasedExpensesCr = new SwtBndCombo(grpExpenses, SWT.READ_ONLY);
			bcmbItemPurchasedExpensesCr.setBounds(43, 45, 134, 23);
			
			Label lblDr_6 = new Label(grpExpenses, SWT.NONE);
			lblDr_6.setText("Dr.: ");
			lblDr_6.setAlignment(SWT.RIGHT);
			lblDr_6.setBounds(10, 18, 27, 15);
			
			bcmbItemPurchasedExpensesDr = new SwtBndCombo(grpExpenses, SWT.READ_ONLY);
			bcmbItemPurchasedExpensesDr.setBounds(43, 15, 134, 23);
			bcmbItemPurchasedExpensesDr.fillData(CmbQry.getAccountByAccountName(),
					CmbQry.getLabel(), CmbQry.getId(),
					DbConnection.getConnection(), true);
			
			bcmbItemPurchasedExpensesCr.fillData(CmbQry.getAccountByAccountName(),
					CmbQry.getLabel(), CmbQry.getId(),
					DbConnection.getConnection(), true);
			
			Group grpCredit = new Group(grpItemPurchase, SWT.NONE);
			grpCredit.setText("Credit");
			grpCredit.setBounds(406, 32, 191, 74);
			
			Label label = new Label(grpCredit, SWT.NONE);
			label.setText("Cr.: ");
			label.setAlignment(SWT.RIGHT);
			label.setBounds(10, 36, 27, 15);
			
			bcmbItemPurchaseCredit = new SwtBndCombo(grpCredit, SWT.READ_ONLY);
			bcmbItemPurchaseCredit.setBounds(43, 33, 134, 23);
			bcmbItemPurchaseCredit.fillData(CmbQry.getAccountByAccountName(),
					CmbQry.getLabel(), CmbQry.getId(),
					DbConnection.getConnection(), true);
			
			Group grpItemSold = new Group(comAcc, SWT.NONE);
			grpItemSold.setText("Item Sold");
			grpItemSold.setBounds(10, 178, 927, 222);
			
			Group group = new Group(grpItemSold, SWT.NONE);
			group.setText("Item");
			group.setBounds(10, 32, 186, 74);
			
			Label lblCr_1 = new Label(group, SWT.NONE);
			lblCr_1.setText("Cr.: ");
			lblCr_1.setAlignment(SWT.RIGHT);
			lblCr_1.setBounds(10, 36, 32, 15);
			
			bcmbItemSold = new SwtBndCombo(group, SWT.READ_ONLY);
			bcmbItemSold.setBounds(42, 33, 134, 23);
			bcmbItemSold.fillData(CmbQry.getAccountByAccountName(),
					CmbQry.getLabel(), CmbQry.getId(),
					DbConnection.getConnection(), true);
			
			Group grpVat = new Group(grpItemSold, SWT.NONE);
			grpVat.setText("Vat");
			grpVat.setBounds(220, 32, 186, 74);
			
			Label lblDr_1 = new Label(grpVat, SWT.NONE);
			lblDr_1.setText("Dr.: ");
			lblDr_1.setAlignment(SWT.RIGHT);
			lblDr_1.setBounds(10, 19, 32, 15);
			
			bcmbItemSoldVatDr = new SwtBndCombo(grpVat, SWT.READ_ONLY);
			bcmbItemSoldVatDr.setBounds(42, 15, 134, 23);
			bcmbItemSoldVatDr.fillData(CmbQry.getAccountByAccountName(),
					CmbQry.getLabel(), CmbQry.getId(),
					DbConnection.getConnection(), true);
			
			Label lblCr_2 = new Label(grpVat, SWT.NONE);
			lblCr_2.setText("Cr.: ");
			lblCr_2.setAlignment(SWT.RIGHT);
			lblCr_2.setBounds(10, 47, 32, 15);
			
			bcmbItemSoldVatCr = new SwtBndCombo(grpVat, SWT.READ_ONLY);
			bcmbItemSoldVatCr.setBounds(42, 44, 134, 23);
			bcmbItemSoldVatCr.fillData(CmbQry.getAccountByAccountName(),
					CmbQry.getLabel(), CmbQry.getId(),
					DbConnection.getConnection(), true);
			
			Group grpDiscount = new Group(grpItemSold, SWT.NONE);
			grpDiscount.setText("Discount");
			grpDiscount.setBounds(422, 32, 186, 74);
			
			Label lblDr_2 = new Label(grpDiscount, SWT.NONE);
			lblDr_2.setText("Dr.: ");
			lblDr_2.setAlignment(SWT.RIGHT);
			lblDr_2.setBounds(10, 20, 32, 15);
			
			bcmbItemSoldDiscountDr = new SwtBndCombo(grpDiscount, SWT.READ_ONLY);
			bcmbItemSoldDiscountDr.setBounds(42, 15, 134, 23);
			bcmbItemSoldDiscountDr.fillData(CmbQry.getAccountByAccountName(),
					CmbQry.getLabel(), CmbQry.getId(),
					DbConnection.getConnection(), true);
			
			Label lblCr_3 = new Label(grpDiscount, SWT.NONE);
			lblCr_3.setText("Cr.: ");
			lblCr_3.setAlignment(SWT.RIGHT);
			lblCr_3.setBounds(10, 47, 32, 15);
			
			 bcmbItemSoldDiscountCr = new SwtBndCombo(grpDiscount, SWT.READ_ONLY);
			bcmbItemSoldDiscountCr.setBounds(42, 43, 134, 23);
			bcmbItemSoldDiscountCr.fillData(CmbQry.getAccountByAccountName(),
					CmbQry.getLabel(), CmbQry.getId(),
					DbConnection.getConnection(), true);
			
			Group grpCredit_1 = new Group(grpItemSold, SWT.NONE);
			grpCredit_1.setText("Credit");
			grpCredit_1.setBounds(623, 32, 186, 74);
			
			Label lblDr_3 = new Label(grpCredit_1, SWT.NONE);
			lblDr_3.setText("Dr.: ");
			lblDr_3.setAlignment(SWT.RIGHT);
			lblDr_3.setBounds(10, 36, 32, 15);
			
			bcmbItemSoldCredit = new SwtBndCombo(grpCredit_1, SWT.READ_ONLY);
			bcmbItemSoldCredit.setBounds(42, 33, 134, 23);
			
			Group grpService = new Group(grpItemSold, SWT.NONE);
			grpService.setText("Service Earned");
			grpService.setBounds(10, 121, 186, 74);
			
			Label lblDr_4 = new Label(grpService, SWT.NONE);
			lblDr_4.setText("Dr.: ");
			lblDr_4.setAlignment(SWT.RIGHT);
			lblDr_4.setBounds(10, 20, 32, 15);
			
			bcmbServiceEarnedDr = new SwtBndCombo(grpService, SWT.READ_ONLY);
			bcmbServiceEarnedDr.setBounds(42, 16, 134, 23);
			
			Label lblCr_4 = new Label(grpService, SWT.NONE);
			lblCr_4.setText("Cr.: ");
			lblCr_4.setAlignment(SWT.RIGHT);
			lblCr_4.setBounds(10, 47, 32, 15);
			
			bcmbServiceEarnedCr = new SwtBndCombo(grpService, SWT.READ_ONLY);
			bcmbServiceEarnedCr.setBounds(42, 44, 134, 23);
			bcmbServiceEarnedCr.fillData(CmbQry.getAccountByAccountName(),
					CmbQry.getLabel(), CmbQry.getId(),
					DbConnection.getConnection(), true);
			bcmbServiceEarnedDr.fillData(CmbQry.getAccountByAccountName(),
					CmbQry.getLabel(), CmbQry.getId(),
					DbConnection.getConnection(), true);
			
			Group grpThirdPartyPaid = new Group(grpItemSold, SWT.NONE);
			grpThirdPartyPaid.setText("Service Paid");
			grpThirdPartyPaid.setBounds(220, 121, 186, 74);
			
			Label lblDr_5 = new Label(grpThirdPartyPaid, SWT.NONE);
			lblDr_5.setText("Dr.: ");
			lblDr_5.setAlignment(SWT.RIGHT);
			lblDr_5.setBounds(10, 22, 32, 15);
			
			bcmbServicePaidDr = new SwtBndCombo(grpThirdPartyPaid, SWT.READ_ONLY);
			bcmbServicePaidDr.setBounds(42, 16, 134, 23);
			
			Label label_1 = new Label(grpThirdPartyPaid, SWT.NONE);
			label_1.setText("Cr.: ");
			label_1.setAlignment(SWT.RIGHT);
			label_1.setBounds(10, 48, 32, 15);
			
			bcmbServicePaidCr = new SwtBndCombo(grpThirdPartyPaid, SWT.READ_ONLY);
			bcmbServicePaidCr.setBounds(42, 45, 134, 23);
			bcmbServicePaidCr.fillData(CmbQry.getAccountByAccountName(),
					CmbQry.getLabel(), CmbQry.getId(),
					DbConnection.getConnection(), true);
			bcmbServicePaidDr.fillData(CmbQry.getAccountByAccountName(),
					CmbQry.getLabel(), CmbQry.getId(),
					DbConnection.getConnection(), true);
			
			Button btnSave = new Button(comAcc, SWT.NONE);
			btnSave.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					msg=new BeforeSaveMessages();
					AccountSettingController control=new AccountSettingController();
					beforSaveAccCode();
					if(msg.returnMessages())
					{
						AccountSettingModel model=new AccountSettingModel();
						model.setAcc_set_id(accId);
						model.setItem_purchased_credit_cr(bcmbItemPurchaseCredit.getSelectedBoundValue().toString());
						model.setItem_purchased_dr(bcmbItemPurchased.getSelectedBoundValue().toString());
						model.setItem_purchased_expense_cr(bcmbItemPurchasedExpensesCr.getSelectedBoundValue().toString());
						model.setItem_purchased_expense_dr(bcmbItemPurchasedExpensesDr.getSelectedBoundValue().toString());
						model.setItem_sold_cr(bcmbItemSold.getSelectedBoundValue().toString());
						model.setItem_sold_credit_dr(bcmbItemSoldCredit.getSelectedBoundValue().toString());
						model.setItem_sold_discount_dr(bcmbItemSoldDiscountDr.getSelectedBoundValue().toString());
						model.setItem_sold_discount_cr(bcmbItemSoldDiscountCr.getSelectedBoundValue().toString());
						model.setItem_sold_vat_dr(bcmbItemSoldVatDr.getSelectedBoundValue().toString());
						model.setItem_sold_vat_cr(bcmbItemSoldVatCr.getSelectedBoundValue().toString());
						model.setService_earned_dr(bcmbServiceEarnedDr.getSelectedBoundValue().toString());
						model.setService_earned_cr(bcmbServiceEarnedCr.getSelectedBoundValue().toString());
						model.setService_expense_cr(bcmbServicePaidCr.getSelectedBoundValue().toString());
						model.setService_expense_dr(bcmbServicePaidDr.getSelectedBoundValue().toString());
						
						Connection cnn=DbConnection.getConnection();
						try {
							cnn.setAutoCommit(false);
							if(control.saveAccountSetting(model))
							{
								msg.showIndividualMessage("Account Setting successfully updated/inserted.", 0);
								cnn.setAutoCommit(true);
							}
							else
							{
								msg.showIndividualMessage("Some error occured.Please try again.", 0);
								cnn.rollback();
								cnn.setAutoCommit(true);
							}
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							try {
								cnn.rollback();
								cnn.setAutoCommit(true);
							} catch (SQLException e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}
						}
					}
					
				}
			});
			btnSave.setBounds(470, 421, 102, 50);
			btnSave.setText("Save");
			bcmbItemSoldCredit.fillData(CmbQry.getAccountByAccountName(),
					CmbQry.getLabel(), CmbQry.getId(),
					DbConnection.getConnection(), true);
			fillAccCodeCombo();
	 }
	
	 
	
	 
	
	 
	 
	 
	 private void fillTableOther(){
		 ResultSet rstype = objCodeValueController.getCodeValueDetails(cv_type);
         try {
        	 tblOther.removeAll();  
        	 clear();
			 if(rstype.next()){
				rstype.beforeFirst();//restore back the cursor to default
				while(rstype.next()){	
					TableItem item = new TableItem(tblOther, SWT.NONE);
					
					//int i = 0;
					item.setText(0, String.valueOf(tblOther.getItemCount()));
					item.setText(1, rstype.getString("cv_lbl"));
					item.setText(2, rstype.getString("cv_code"));
					item.setText(3, rstype.getString("cv_id"));
					//System.out.println(rstype.getString("cv_type"));
					item.setText(4, rstype.getString("cv_type"));
					//i++;
				}
			}
			btnOAdd.setEnabled(true);
		  }catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		  }
	 }
	 
	 private void fillAccCodeCombo()
	 {
		 AccountSettingController control=new AccountSettingController();
		 AccountSettingModel model=control.GetAccountDetail();
		 
		 if(model!=null)
		 {
			 bcmbItemPurchaseCredit.setSelectedBoundValue(model.getItem_purchased_credit_cr());
			 bcmbItemPurchased.setSelectedBoundValue(model.getItem_purchased_dr());
			 bcmbItemPurchasedExpensesCr.setSelectedBoundValue(model.getItem_purchased_expense_cr());
			 bcmbItemPurchasedExpensesDr.setSelectedBoundValue(model.getItem_purchased_expense_dr());
			 bcmbItemSold.setSelectedBoundValue(model.getItem_sold_cr());
			 bcmbItemSoldCredit.setSelectedBoundValue(model.getItem_sold_credit_dr());
			 bcmbItemSoldDiscountDr.setSelectedBoundValue(model.getItem_sold_discount_dr());
			 bcmbItemSoldDiscountCr.setSelectedBoundValue(model.getItem_sold_discount_cr());
			 bcmbItemSoldVatDr.setSelectedBoundValue(model.getItem_sold_vat_dr());
			 bcmbItemSoldVatCr.setSelectedBoundValue(model.getItem_sold_vat_cr());
			 bcmbServiceEarnedCr.setSelectedBoundValue(model.getService_earned_cr());
			 bcmbServiceEarnedDr.setSelectedBoundValue(model.getService_earned_dr());
			 bcmbServicePaidCr.setSelectedBoundValue(model.getService_expense_cr());
			 bcmbServicePaidDr.setSelectedBoundValue(model.getService_expense_dr());
			 accId=model.getAcc_set_id();
		 }
		
		 
	 }
	 
	 private void fillTableSaving(){
		 ResultSet rstype = objCodeValueController.getCodeValueDetails(cv_type);
         try {
        	 tblSaving.removeAll();  
        	 clear();
			 if(rstype.next()){
				rstype.beforeFirst();//restore back the cursor to default
				while(rstype.next()){	
					TableItem item = new TableItem(tblSaving, SWT.NONE);
					
					//int i = 0;
					item.setText(0, String.valueOf(tblSaving.getItemCount()));
					item.setText(1, rstype.getString("cv_lbl"));
					item.setText(2, rstype.getString("cv_code"));
					item.setText(3, rstype.getString("cv_id"));
					//System.out.println(rstype.getString("cv_type"));
					item.setText(4, rstype.getString("cv_type"));
					//i++;
				}
			}
		  }catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		  }
	 }
	 
	 
	 
	 
	
	 private void saveSavingCodeValue()
	 {
		 
		
			//set values to be saved
			 objCodeModel.setCv_lbl(cv_lbl.replaceAll("'", "''"));
			 objCodeModel.setCv_type(cv_type);
			 objCodeModel.setCv_code(txtSCodeLabel.getText());
			 objCodeModel.setCv_id(Integer.valueOf(cv_id));
			 
			 //now call insert
			 if(objCodeValueController.Update(objCodeModel))
			 {
				 MessageBox mb=new MessageBox(sh,SWT.NONE | SWT.ICON_INFORMATION);
				 mb.setMessage("The Value Has Been Successfully Updated");
				 mb.setText("Success");
				 mb.open();
				 //reload the table
				 fillTableSaving();
				 clear();
				
				 btnSSave.setEnabled(false);
				 btnSClear.setEnabled(false);
				 txtSCodeLabel.setEnabled(false);
				 //clear fields
				 //txtAccCode.setText("");				 
			 }
			 else
			 {
				 MessageBox mb=new MessageBox(sh,SWT.ERROR | SWT.ICON_ERROR);
				 mb.setMessage("Some Error Occurred While Updating. Please Try Again");
				 mb.setText("Error");
				 mb.open(); 
			 }
		 
	 }
	 private void saveOtherCodeValue()
	 {
		 if(cv_id.equals("")){
			
			 //set values to be saved
			 objCodeModel.setCv_lbl(txtOCodeLabel.getText().replaceAll("'", "''"));
			 //objCodeModel.setCv_code(txtMember.getText());
			 cv_code = String.valueOf(getMaxCodeValue("code_value_mcg", "cv_code", cv_type));
			 System.out.println("This is the cv_code"+cv_code);
			 objCodeModel.setCv_type(cv_type);
			 objCodeModel.setCv_code(cv_code);
			 
			 //now call insert
			 if(objCodeValueController.Insert(objCodeModel))
			 {
				 MessageBox mb=new MessageBox(sh,SWT.NONE | SWT.ICON_INFORMATION);
				 mb.setMessage("The Value Has Been Successfully Inserted");
				 mb.setText("Success");
				 mb.open();
				 //reload the table
				 fillTableOther();
				 clear();
				 //clear fields
				 //txtAccCode.setText("");				 
			 }
			 else
			 {
				 MessageBox mb=new MessageBox(sh,SWT.ERROR | SWT.ICON_ERROR);
				 mb.setMessage("Some Error Occurred While Saving.Please Try Again");
				 mb.setText("Error");
				 mb.open(); 
			 }
		 }
		 else{
			 if(tblOther.getSelectionIndex() >= 0)
				{
					
				
			 
			//set values to be saved
			 objCodeModel.setCv_lbl(txtOCodeLabel.getText().replaceAll("'", "''"));
			 objCodeModel.setCv_type(cv_type);
			 objCodeModel.setCv_code(cv_code);
			 objCodeModel.setCv_id(Integer.valueOf(cv_id));
			 
			 //now call insert
			 if(objCodeValueController.Update(objCodeModel))
			 {
				 MessageBox mb=new MessageBox(sh,SWT.NONE | SWT.ICON_INFORMATION);
				 mb.setMessage("The Value Has Been Successfully Updated");
				 mb.setText("Success");
				 mb.open();
				 //reload the table
				 fillTableOther();
				 clear();
				 btnOEdit.setEnabled(false);
				 btnOSave.setEnabled(false);
				 //clear fields
				 //txtAccCode.setText("");				 
			 }
			 else
			 {
				 MessageBox mb=new MessageBox(sh,SWT.ERROR | SWT.ICON_ERROR);
				 mb.setMessage("Some Error Occurred While Updating.Please Try Again");
				 mb.setText("Error");
				 mb.open(); 
			 }
				}
			 else
			 {
				 msg.showIndividualMessage("Please Select table row to edit", 0);
			 }
		 }
	 }
	 
	 private void beforSaveAccCode()
	 {
		 if(bcmbItemPurchaseCredit.getSelectedBoundValue().toString().equals("0"))
		 {
			 msg.setMandatoryMessages("Item Purchased Credit is required");
		 }
		 if(bcmbItemPurchased.getSelectedBoundValue().toString().equals("0"))
		 {
			 msg.setMandatoryMessages("Item Purchased Debit is required");
		 }
		 if(bcmbItemPurchasedExpensesCr.getSelectedBoundValue().toString().equals("0"))
		 {
			 msg.setMandatoryMessages("Item Purchased Exense is required");
		 }
		 if(bcmbItemSold.getSelectedBoundValue().toString().equals("0"))
		 {
			 msg.setMandatoryMessages("Item Sold Credit is required");
		 }
		 if(bcmbItemSoldCredit.getSelectedBoundValue().toString().equals("0"))
		 {
			 msg.setMandatoryMessages("Item Sold Credit is required");
		 }
		 if(bcmbItemSoldDiscountDr.getSelectedBoundValue().toString().equals("0"))
		 {
			 msg.setMandatoryMessages("Item Sold Discount is required");
		 }
		 if(bcmbItemSoldVatDr.getSelectedBoundValue().toString().equals("0"))
		 {
			 msg.setMandatoryMessages("Item Sold Vat is required");
		 }
		 if(bcmbServiceEarnedDr.getSelectedBoundValue().toString().equals("0"))
		 {
			 msg.setMandatoryMessages("Service earned is required");
		 }
		 if(bcmbServiceEarnedDr.getSelectedBoundValue().toString().equals("0"))
		 {
			 msg.setMandatoryMessages(" service earned cr is required");
		 }
		 if(bcmbServicePaidDr.getSelectedBoundValue().toString().equals("0"))
		 {
			 msg.setMandatoryMessages("Third party service paid is required");
		 }
		 
	 }
	 
	 public static int getMaxCodeValue(String TableName, String ColumnName, String cv_type) {
			int id = 0;
			Connection cnn = DbConnection.getConnection();
			String strQry = "Select isnull(max(cast(" + ColumnName
				+ " as numeric(20,0))),0) as cv_code from " + TableName + " where cv_type = '"+cv_type+"'";
			//System.out.println("The query is " + strQry);
			try {
				System.out.println("zzz... the getMaxId QRY here = "+strQry);
				Statement st = cnn.createStatement(
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_READ_ONLY);
				ResultSet rs = st.executeQuery(strQry);
				if (rs != null) {
					rs.next();
					if (rs.getString("cv_code") != null
							&& !rs.getString("cv_code").equals(""))
						id = Integer.parseInt(rs.getString("cv_code").toString());
				}
			} catch (SQLException ex) {
				System.out.println(ex.toString());
			}
			//System.out.println("Max Id:" + id);
			id++;
			return id;
		}
	 
	 private void clear() {
			
			txtSCodeLabel.setText("");
			txtOCodeLabel.setText("");
			tblSaving.deselectAll();
			tblOther.deselectAll();
			this.cv_id = "";
			this.cv_code = "";
			this.cv_lbl="";
		}

	 //end of tab1 functions
	 
	 /**
	  * Passing the focus request to the form.
	  */
	 @Override
	public void setFocus() {

	 }
}