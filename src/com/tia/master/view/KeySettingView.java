package com.tia.master.view;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.nebula.widgets.grid.Grid;
import org.eclipse.nebula.widgets.grid.GridColumn;
import org.eclipse.nebula.widgets.grid.GridColumnGroup;
import org.eclipse.nebula.widgets.grid.GridItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.Validator;


public class KeySettingView extends ViewPart {
	private Text txtName;
	private Text txtCode;
	static Connection cnn = DbConnection.getConnection();
	
	static Validator objtime = new Validator();
	Shell sh = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	static Button cmdSave;
	private Text txtHallId;
	private Composite comParent;

	public KeySettingView() {
		setTitleImage(SWTResourceManager.getImage(KeySettingView.class, "/hall-16-16.PNG"));
		
	}

	@Override
	public void createPartControl(Composite parent) {

		comParent = new Composite(parent, SWT.NONE);
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("Hall Mangement Form", comParent, KeySettingView.class);

		Composite composite = new Composite(comParent, SWT.NONE);
		GridData gd_composite = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_composite.heightHint = 466;
		gd_composite.widthHint = 975;
		composite.setLayoutData(gd_composite);

		Group grpFields = new Group(composite, SWT.NONE);
		grpFields.setText("Sales Key Values");
		grpFields.setBounds(7, 10, 656, 58);

		Label lblName = new Label(grpFields, SWT.NONE);
		lblName.setAlignment(SWT.RIGHT);
		lblName.setBounds(21, 23, 50, 15);
		lblName.setText("Vat(%) :");

		Label lblCode = new Label(grpFields, SWT.NONE);
		lblCode.setAlignment(SWT.RIGHT);
		lblCode.setText("Discount(%) :");
		lblCode.setBounds(336, 23, 71, 25);

		txtName = new Text(grpFields, SWT.BORDER);
		txtName.setEnabled(false);
		txtName.setBounds(76, 23, 239, 21);

		txtCode = new Text(grpFields, SWT.BORDER);
		txtCode.setEnabled(false);
		txtCode.setBounds(413, 20, 107, 21);

		txtHallId = new Text(grpFields, SWT.BORDER);
		txtHallId.setEnabled(false);
		txtHallId.setEditable(false);
		txtHallId.setVisible(false);
		txtHallId.setBounds(287, 75, 76, 21);
		grpFields.setTabList(new Control[]{txtName, txtCode});
		
				cmdSave = new Button(composite, SWT.NONE);
				cmdSave.setBounds(39, 86, 88, 25);
				cmdSave.setEnabled(false);
				
						// saving the data into the database
						cmdSave.addSelectionListener(new SelectionAdapter() {
							@Override
							public void widgetSelected(SelectionEvent e) {
				
							
							}
						});
						cmdSave.setText("Save");

		

	}

	

	

	


	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}
}
