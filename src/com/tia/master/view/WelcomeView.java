package com.tia.master.view;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.part.ViewPart;

import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.controller.JournalEntryController;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.inventory.controller.DayBookController;
import com.tia.inventory.controller.StockItemController;
import com.tia.inventory.view.StockEntryView;
import com.tia.inventory.view.StockPurchasedEntryView;
import com.tia.inventory.view.StockSalesEntryView;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import com.tia.user.view.ChangePasswordDialog;

import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class WelcomeView extends ViewPart {
	private final FormToolkit formToolkit = new FormToolkit(
			Display.getDefault());
	private Text txtUsername;
	private Table table;
	private Composite compStockOut;
	

	public WelcomeView() {
		setTitleImage(SWTResourceManager.getImage(WelcomeView.class,
				"/status.png"));
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createPartControl(Composite parent) {

		ScrolledComposite scrolledComposite = new ScrolledComposite(parent,
				SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);
		scrolledComposite.setBackground(SWTResourceManager
				.getColor(SWT.COLOR_TITLE_BACKGROUND));
		formToolkit.adapt(scrolledComposite);
		formToolkit.paintBordersFor(scrolledComposite);

		Composite composite = new Composite(scrolledComposite, SWT.NONE);
		composite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		formToolkit.adapt(composite);
		formToolkit.paintBordersFor(composite);
		GridLayout gl_composite = new GridLayout(1, false);
		gl_composite.marginWidth = 0;
		gl_composite.verticalSpacing = 0;
		gl_composite.marginHeight = 0;
		gl_composite.horizontalSpacing = 0;
		composite.setLayout(gl_composite);

		// Label label_8 = new Label(composite, SWT.NONE);
		// GridData gd_label_8 = new GridData(SWT.LEFT, SWT.CENTER, false,
		// false,
		// 1, 1);
		// gd_label_8.widthHint = 968;
		// label_8.setLayoutData(gd_label_8);
		// label_8.setImage(SWTResourceManager.getImage(WelcomeNew2.class,
		// / "/skbblwelcome.jpg"));
		// formToolkit.adapt(label_8, true, true);

		Composite bannerComposite = new Composite(composite, SWT.NONE);

		GridData gd_bannerComposite = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_bannerComposite.widthHint = 955;
		bannerComposite.setLayoutData(gd_bannerComposite);
		// formToolkit.adapt(bannerComposite, true, true);
		bannerComposite.setBackground(SWTResourceManager
				.getColor(253, 254, 255));

		Label lblImage = new Label(bannerComposite, SWT.NONE);
		lblImage.setBounds(0, 0, 580, 146);

		formToolkit.adapt(lblImage, true, true);
		lblImage.setImage(SWTResourceManager.getImage(WelcomeView.class,
				"/welcome.jpg"));

		Label lblfgfLsfgxsfl = new Label(bannerComposite, SWT.NONE);
		lblfgfLsfgxsfl.setForeground(SWTResourceManager.getColor(27, 74, 3));
		lblfgfLsfgxsfl
				.setBackground(SWTResourceManager.getColor(253, 254, 255));
			lblfgfLsfgxsfl.setFont(SWTResourceManager.getFont("Monotype Corsiva", 18,SWT.NORMAL));

		lblfgfLsfgxsfl.setAlignment(SWT.RIGHT);
		lblfgfLsfgxsfl.setBounds(526, 15, 414, 25);
		// formToolkit.adapt(lblfgfLsfgxsfl, true, true);
		lblfgfLsfgxsfl.setText("TEYSEER AUTO SERVICES");

		Label address = new Label(bannerComposite, SWT.NONE);
		address.setText("Sitapaila -1, Kathmandu");
		address.setFont(SWTResourceManager.getFont("Suryodaya", 10, SWT.NORMAL));
			address.setFont(SWTResourceManager.getFont("Monotype Corsiva", 14,SWT.NORMAL));
		address.setAlignment(SWT.RIGHT);
		address.setBounds(590, 45, 350, 25);
		address.setForeground(SWTResourceManager.getColor(27, 74, 3));
		address.setBackground(SWTResourceManager.getColor(253, 254, 255));

		

		Label lblSysDate = new Label(bannerComposite, SWT.NONE);
		JCalendarFunctions jcal = new JCalendarFunctions();
		lblSysDate.setText("Date :- " + jcal.currentDate());
		lblSysDate.setAlignment(SWT.RIGHT);
		lblSysDate.setBounds(698, 76, 188, 15);
		lblSysDate.setForeground(SWTResourceManager.getColor(27, 74, 3));
		lblSysDate.setBackground(SWTResourceManager.getColor(253, 254, 255));
		

		Label lblSysUser = new Label(bannerComposite, SWT.NONE);
		lblSysUser.setText("User :- ");
		// + ApplicationWorkbenchWindowAdvisor.getUserNameLong());
		// System.out.println("the username is----------"
		// + ApplicationWorkbenchWindowAdvisor.getUserNameLong());
		lblSysUser.setAlignment(SWT.RIGHT);
		lblSysUser.setBounds(688, 97, 138, 21);
		lblSysUser.setForeground(SWTResourceManager.getColor(27, 74, 3));
		lblSysUser.setBackground(SWTResourceManager.getColor(253, 254, 255));
		

		txtUsername = new Text(bannerComposite, SWT.NONE);
		txtUsername.setBounds(831, 97, 124, 21);
		txtUsername
				.setText(ApplicationWorkbenchWindowAdvisor.getCurrentUser());
		txtUsername.setForeground(SWTResourceManager.getColor(27, 74, 3));
		txtUsername.setBackground(SWTResourceManager.getColor(253, 254, 255));
		txtUsername.setEditable(false);
		//txtUsername.setFont(UtilMethods.getSmallFont());
		//formToolkit.adapt(txtUsername, true, true);

		Composite composite_2 = new Composite(composite, SWT.NONE);
		composite_2.setLayout(new GridLayout(4, false));
		GridData gd_composite_2 = new GridData(SWT.CENTER, SWT.BOTTOM, false,
				false, 1, 1);
		gd_composite_2.widthHint = 918;
		gd_composite_2.heightHint = 461;
		composite_2.setLayoutData(gd_composite_2);
		formToolkit.adapt(composite_2);
		formToolkit.paintBordersFor(composite_2);
		
		
		
		
		
				// member section starts
				Composite comFinancialInfo = new Composite(composite_2, SWT.NONE);
				comFinancialInfo.setBackground(SWTResourceManager.getColor(250, 247, 229));
				formToolkit.adapt(comFinancialInfo);
				formToolkit.paintBordersFor(comFinancialInfo);
				GridData gd_comFinancialInfo = new GridData(SWT.FILL, SWT.TOP, false,
						false, 1, 1);
				gd_comFinancialInfo.heightHint = 243;
				gd_comFinancialInfo.widthHint = 291;
				comFinancialInfo.setLayoutData(gd_comFinancialInfo);
				comFinancialInfo.setLayout(new GridLayout(1, false));
				Section sctnMember = formToolkit.createSection(comFinancialInfo,
						Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
				GridData gd_sctnMember = new GridData(SWT.FILL, SWT.TOP, false, false,
						1, 1);
				gd_sctnMember.heightHint = 201;
				gd_sctnMember.widthHint = 266;
				sctnMember.setLayoutData(gd_sctnMember);
				formToolkit.paintBordersFor(sctnMember);
				sctnMember.setText("Financial Informations");
				

				Composite compQ = new Composite(sctnMember, SWT.NONE);
				compQ.setBackground(SWTResourceManager.getColor(250, 247, 229));
				formToolkit.adapt(compQ);
				formToolkit.paintBordersFor(compQ);
				sctnMember.setClient(compQ);
				
				DayBookController control=new DayBookController();
				
				ResultSet rs=control.getTotalIncome();
				double cashInHand=0;
				double pittyCash=0;
				
				try {
					if(rs.next())
					{
						cashInHand=rs.getDouble("income");
						pittyCash=rs.getDouble("expense");
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				double itemStock=0;
				
				
				ResultSet rs1=control.getTotalStockItem();
				try {
					if(rs.next())
					{
						itemStock=rs.getDouble("item");
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				double salesIncome=0;
				ResultSet rs2=control.getTotalStockOut();
				try {
					if(rs.next())
					{
						salesIncome=rs.getDouble("item");
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
				Label lblCashInHand = new Label(compQ, SWT.NONE);
				lblCashInHand.setBounds(0, 10, 149, 15);
				formToolkit.adapt(lblCashInHand, true, true);
				lblCashInHand.setText("Total Income: ");
				
				Label lblRs = new Label(compQ, SWT.NONE);
				lblRs.setBounds(155, 10, 89, 15);
				formToolkit.adapt(lblRs, true, true);
				lblRs.setText("Rs "+UtilFxn.getFormatedAmount(cashInHand));
				
				Label lblPittyCash = new Label(compQ, SWT.NONE);
				lblPittyCash.setBounds(0, 38, 149, 15);
				formToolkit.adapt(lblPittyCash, true, true);
				
				lblPittyCash.setText("Total Expense: ");
				
				
				Label lblRs_1 = new Label(compQ, SWT.NONE);
				lblRs_1.setBounds(155, 38, 89, 15);
				formToolkit.adapt(lblRs_1, true, true);
				lblRs_1.setText("Rs "+UtilFxn.getFormatedAmount(pittyCash));
				
				Label lblItemPurchased = new Label(compQ, SWT.NONE);
				lblItemPurchased.setBounds(0, 67, 149, 15);
				formToolkit.adapt(lblItemPurchased, true, true);
				lblItemPurchased.setText("Total Item In Stock: ");
				
				Label lblRs_2 = new Label(compQ, SWT.NONE);
				lblRs_2.setBounds(155, 67, 89, 15);
				formToolkit.adapt(lblRs_2, true, true);
				lblRs_2.setText("Rs "+UtilFxn.getFormatedAmount(itemStock));
				
				
				Label lblItemSold = new Label(compQ, SWT.NONE);
				lblItemSold.setBounds(0, 96, 149, 15);
				formToolkit.adapt(lblItemSold, true, true);
				lblItemSold.setText("Total Item Sold: ");
				
				Label lblRs_3 = new Label(compQ, SWT.NONE);
				lblRs_3.setBounds(155, 96, 89, 15);
				formToolkit.adapt(lblRs_3, true, true);
				lblRs_3.setText("Rs "+UtilFxn.getFormatedAmount(salesIncome));
				
				
				Label lblAccountReceiable = new Label(compQ, SWT.NONE);
				lblAccountReceiable.setBounds(0, 125, 149, 15);
				formToolkit.adapt(lblAccountReceiable, true, true);
				lblAccountReceiable.setText("Total Item Sold Today: ");
				
				Label lblRs_4 = new Label(compQ, SWT.NONE);
				lblRs_4.setBounds(155, 125, 89, 15);
				formToolkit.adapt(lblRs_4, true, true);
				lblRs_4.setText("Rs "+UtilFxn.getFormatedAmount(salesIncome));
				
				Label lblAccountPayable = new Label(compQ, SWT.NONE);
				lblAccountPayable.setBounds(0, 157, 149, 15);
				formToolkit.adapt(lblAccountPayable, true, true);
				lblAccountPayable.setText("Total Item Sold Last Month: ");
				
				Label lblRs_5 = new Label(compQ, SWT.NONE);
				lblRs_5.setBounds(155, 157, 89, 15);
				formToolkit.adapt(lblRs_5, true, true);
				lblRs_5.setText("Rs "+UtilFxn.getFormatedAmount(salesIncome));
				
				Label label = new Label(compQ, SWT.SEPARATOR | SWT.HORIZONTAL);
				label.setBounds(0, 31, 254, 2);
				formToolkit.adapt(label, true, true);
				
				Label label_1 = new Label(compQ, SWT.SEPARATOR | SWT.HORIZONTAL);
				label_1.setBounds(0, 59, 254, 2);
				formToolkit.adapt(label_1, true, true);
				
				Label label_2 = new Label(compQ, SWT.SEPARATOR | SWT.HORIZONTAL);
				label_2.setBounds(0, 88, 254, 2);
				formToolkit.adapt(label_2, true, true);
				
				Label label_3 = new Label(compQ, SWT.SEPARATOR | SWT.HORIZONTAL);
				label_3.setBounds(0, 117, 254, 2);
				formToolkit.adapt(label_3, true, true);
				
				Label label_4 = new Label(compQ, SWT.SEPARATOR | SWT.HORIZONTAL);
				label_4.setBounds(0, 147, 254, 2);
				formToolkit.adapt(label_4, true, true);
		new Label(composite_2, SWT.NONE);
		
		// member section ends

		// loan section starts
		Composite compOutOfStock = new Composite(composite_2, SWT.NONE);
		compOutOfStock.setBackground(SWTResourceManager.getColor(250, 247, 229));
		formToolkit.adapt(compOutOfStock);
		formToolkit.paintBordersFor(compOutOfStock);
		compOutOfStock.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false,
				false, 1, 1));
		
		compOutOfStock.setLayout(new GridLayout(1, false));
		Section sctnLoan = formToolkit.createSection(compOutOfStock,
				Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		GridData gd_sctnLoan = new GridData(SWT.FILL, SWT.TOP, false, false, 1,
				1);
		gd_sctnLoan.heightHint = 234;
		gd_sctnLoan.widthHint = 381;
		sctnLoan.setLayoutData(gd_sctnLoan);
		formToolkit.paintBordersFor(sctnLoan);
		sctnLoan.setText("Item Out Of Stock");
		sctnLoan.setExpanded(true);

		compStockOut = new Composite(sctnLoan, SWT.NONE);
		compStockOut.setBackground(SWTResourceManager.getColor(250, 247, 229));
		formToolkit.adapt(compStockOut);
		formToolkit.paintBordersFor(compStockOut);
		sctnLoan.setClient(compStockOut);
		
		//end out of stock
		
		Composite compQuickLinks = new Composite(composite_2, SWT.NONE);
		compQuickLinks.setBackground(SWTResourceManager.getColor(250, 247, 229));
		formToolkit.adapt(compQuickLinks);
		formToolkit.paintBordersFor(compQuickLinks);
		GridData gd_comQuickLinks = new GridData(SWT.FILL, SWT.TOP, false,
				false, 1, 1);
		gd_comQuickLinks.heightHint = 243;
		gd_comQuickLinks.widthHint = 218;
		compQuickLinks.setLayoutData(gd_comQuickLinks);
		compQuickLinks.setLayout(new GridLayout(1, false));
		Section sctnLinks = formToolkit.createSection(compQuickLinks,
				Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		GridData gd_sctnLinks = new GridData(SWT.FILL, SWT.TOP, false, false,
				1, 1);
		gd_sctnLinks.heightHint = 235;
		gd_sctnLinks.widthHint = 193;
		sctnLinks.setLayoutData(gd_sctnLinks);
		formToolkit.paintBordersFor(sctnLinks);
		sctnLinks.setText("Quick Links");
		

		Composite composite_1 = new Composite(sctnLinks, SWT.NONE);
		composite_1.setBackground(SWTResourceManager.getColor(250, 247, 229));
		formToolkit.adapt(composite_1);
		formToolkit.paintBordersFor(composite_1);
		sctnLinks.setClient(composite_1);
		
		Link link = new Link(composite_1, SWT.NONE);
		link.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				StockEntryView hms = new StockEntryView(
						PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getShell(),0,"");
				hms.open();
			}
		});
		link.setBounds(0, 7, 171, 15);
		formToolkit.adapt(link, true, true);
		link.setText("<a>Register new item</a>");
		
		Link link_1 = new Link(composite_1, SWT.NONE);
		link_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				StockPurchasedEntryView cngpsword = new StockPurchasedEntryView(
						PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getShell());
				cngpsword.open();
			}
		});
		link_1.setBounds(0, 29, 171, 15);
		formToolkit.adapt(link_1, true, true);
		link_1.setText("<a>Stock Purchase Entry</a>");
		
		Link link_2 = new Link(composite_1, SWT.NONE);
		link_2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				StockSalesEntryView cngpsword = new StockSalesEntryView(
						PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getShell());
				cngpsword.open();
			}
		});
		link_2.setBounds(0, 50, 171, 15);
		formToolkit.adapt(link_2, true, true);
		link_2.setText("<a>Stock Sales Entry</a>");
		
		Link link_3 = new Link(composite_1, SWT.NONE);
		link_3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				openView("com.tia.account.journalVoucher");
			}
		});
		link_3.setBounds(0, 71, 171, 15);
		formToolkit.adapt(link_3, true, true);
		link_3.setText("<a>Day Book Entry</a>");
		
		Link link_4 = new Link(composite_1, SWT.NONE);
		link_4.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				openView("com.tia.inven.serviceRecord");
			}
		});
		link_4.setBounds(0, 92, 171, 15);
		formToolkit.adapt(link_4, true, true);
		link_4.setText("<a>Service Record Entry</a>");
		
		Link link_5 = new Link(composite_1, SWT.NONE);
		link_5.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ChangePasswordDialog cngpsword = new ChangePasswordDialog(
						PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getShell());
				cngpsword.open();
			}
		});
		link_5.setBounds(0, 113, 171, 15);
		formToolkit.adapt(link_5, true, true);
		link_5.setText("<a>Change Login Password</a>");
				

		sctnMember.setExpanded(true);
		sctnLinks.setExpanded(true);
		sctnLoan.setExpanded(true);
		// saving section ends

		scrolledComposite.setContent(composite);
		scrolledComposite.setMinSize(new Point(970, 670));

		//testing
		String np = jcal.GetNepaliDate("2013-04-15");
		System.err.println(np);
		System.err.println(jcal.GetEnglishDate(np));
		
		fillStockOutTable();
	}

	// method for opening the view
	public void openView(String viewId) {
		IWorkbenchPage wbp = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		try {
			wbp.showView(viewId);
		} catch (PartInitException e1) {
			e1.printStackTrace();
		}
	}
	
	private void fillStockOutTable()
	{
		StockItemController control=new StockItemController();
		ResultSet rs=control.fetchItemOutOfStock();
		try {
			if(UtilFxn.getRowCount(rs) > 0)
			{
				table = formToolkit.createTable(compStockOut, SWT.NONE | SWT.BORDER_DASH);
				table.setBounds(0, 10, 369, 164);
				formToolkit.paintBordersFor(table);
				table.setHeaderVisible(true);
				table.setLinesVisible(true);
				
				
				TableColumn tblclmnSn = new TableColumn(table, SWT.NONE);
				tblclmnSn.setWidth(48);
				tblclmnSn.setText("S.N.");
				
				TableColumn tblclmnCode = new TableColumn(table, SWT.NONE);
				tblclmnCode.setWidth(117);
				tblclmnCode.setText("Code");
				
				TableColumn tblclmnSold = new TableColumn(table, SWT.NONE);
				tblclmnSold.setWidth(100);
				tblclmnSold.setText("Sold");
				
				TableColumn tblclmnRemaining = new TableColumn(table, SWT.NONE);
				tblclmnRemaining.setWidth(100);
				tblclmnRemaining.setText("Remaining");
				int i=1;
				while(rs.next())
				{
					TableItem item=new TableItem(table, SWT.NONE);
					item.setText(0,String.valueOf(i));
					item.setText(1,rs.getString("item_code"));
					item.setText(2,rs.getString("sel_unit_total"));
					item.setText(3,rs.getString("remaining_unit_total"));
					i++;
				}
			}
			else
			{
				Label lblCurrentlyNoItems = new Label(compStockOut, SWT.NONE);
				lblCurrentlyNoItems.setFont(SWTResourceManager.getFont("Segoe UI", 11, SWT.NORMAL));
				lblCurrentlyNoItems.setBounds(10, 10, 349, 59);
				formToolkit.adapt(lblCurrentlyNoItems, true, true);
				lblCurrentlyNoItems.setText("Currently, no items are out of stock.");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}
}

