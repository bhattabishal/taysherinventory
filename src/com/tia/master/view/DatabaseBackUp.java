/**
 * 
 */
package com.tia.master.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;



import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.util.Banner;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.master.controller.KeySettingController;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import com.tia.user.controller.LoginController;

/**
 * @author magnus
 * 
 */
public class DatabaseBackUp extends ViewPart {

	/**
	 * 
	 */
	private Composite comTitle;
	private Text txtBackUpFileName;
	private JCalendarFunctions jCal;

	static Shell sh = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
			.getShell();

	// To commonize these variablies related to dbSchema, we can put them into
	// loginSession class

	public DatabaseBackUp() {
		// TODO Auto-generated constructor stub
		jCal= new JCalendarFunctions();
		setTitleImage(SWTResourceManager.getImage(DatabaseBackUp.class,
				"/logo.png"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		// TODO Auto-generated method stub
		Composite comParent = new Composite(parent, SWT.NONE);
		comParent.setLayout(new GridLayout(1, false));

		comTitle = Banner.getTitle("Database Backup",
				comParent, DatabaseBackUp.class);
		// comParent.setLayout(new GridLayout(1, false));

		Composite comMain = new Composite(comParent, SWT.NONE);
		GridData gd_comMain = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_comMain.heightHint = 150;
		gd_comMain.widthHint = 500;
		comMain.setLayoutData(gd_comMain);

		Group grpDatabaseBackup = new Group(comMain, SWT.NONE);
		grpDatabaseBackup.setText("DatabaseBackup");
		grpDatabaseBackup.setFont(UtilMethods.getFontBold());
		grpDatabaseBackup.setBounds(10, 10, 464, 58);

		Label lblFilename = new Label(grpDatabaseBackup, SWT.NONE);
		lblFilename.setAlignment(SWT.RIGHT);
		lblFilename.setBounds(10, 26, 100, 18);
		lblFilename.setText("BackupFileName");
		

		txtBackUpFileName = new Text(grpDatabaseBackup, SWT.BORDER);
		txtBackUpFileName.setBounds(114, 25, 260, 20);
		// txtBackUpFileName.setFont(CommonFunctions.getFont());

		Button cmdBackup = new Button(grpDatabaseBackup, SWT.NONE);
		cmdBackup.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Little stuff for validation
				if (txtBackUpFileName.getText().equals("")) {
					MessageBox msg = new MessageBox(sh, SWT.ICON_ERROR);
					msg.setMessage("Please,give a File Name to backup");
					msg.setText("Invalid Text");
					msg.open();
				}// if ends
				else {
					backupDatabase();
					// //Modified as of May 4, 2012
					// JCalendarFunctions jcf = new JCalendarFunctions();
					// String dateNow = jcf.CurrentYear() + "-"
					// + jcf.CurrentMonth() + "-" + jcf.CurrentDay();
					// // getting the name and backup URL of database currently
					// // skbbl is connected to
					// String curDbName = ApplicationWorkbenchWindowAdvisor
					// .getCurrentDbName();
					// String backupURL = ApplicationWorkbenchWindowAdvisor
					// .getBackUpURL();
					// String backUpFileName = backupURL + curDbName + "_"
					// + txtBackUpFileName.getText() + "_" + dateNow;
					// System.out
					// .println("zzz.. from backUP utility view.. The SKBBL is currently connected to DATABASE ===> "
					// + curDbName);
					//
					// // LOC to create and save the Backup (*.bak) file for the
					// // selected database
					// BackUpController.createBackup(backUpFileName, curDbName,
					// dateNow);
					// // LOC to backup all files related to the SKBBL
					// try {
					// copyDirectory(
					// new File(ApplicationWorkbenchWindowAdvisor
					// .getDocUploadUrl()), new File(backupURL
					// + "\\SKBBLDocsTillBackUp"));
					// } catch (IOException ex) {
					// // TODO Auto-generated catch block
					// ex.printStackTrace();
					// }
					// //Modified as of May 4, 2012
				}
			}
		});
		cmdBackup.setBounds(380, 24, 68, 24);
		cmdBackup.setText("BackUp");
		

		jCal = new JCalendarFunctions();
	}

	private void backupDatabase() {

		boolean test = backupDatabase(txtBackUpFileName.getText());
	}

	// Database backup modification by Rabindra Rai May 4, 2012
	// private void backupDatabase() {
	// DayInOut dio = new DayInOut();
	// LoginController lic = new LoginController();
	// String today = ApplicationWorkbenchWindowAdvisor.getDayInNepDate();
	// String dateTime = jCal.getCurrentEnglishDateTime("");
	//
	// if (dio.IsUserLoggedIn()) {
	// UtilFxn.messageBox(
	// SWT.ICON_WARNING,
	// "Warning",
	// "User(s) are still logged in.\nLog off following users before performing backup.\n"
	// + dio.LoggedInUsersList()).open();
	// } else {
	// String dbName = ApplicationWorkbenchWindowAdvisor.getBackUpURL()
	// + txtBackUpFileName.getText() + "_"
	// + ApplicationWorkbenchWindowAdvisor.getCurrentDbName()
	// + "_" + today.replace("/", "-") + "_"
	// + dateTime.substring(11, 19).replace(":", "");
	//
	// if (lic.LogOutUser()) {
	// if (BackUpController.createBackupDayOut(dbName,
	// ApplicationWorkbenchWindowAdvisor.getCurrentDbName(),
	// today + dateTime.substring(11, 19))) {
	// lic.UndoLogout();
	// UtilFxn.messageBox(SWT.ICON_INFORMATION, "Information",
	// "Database backup successfully performed.").open();
	// } else {
	// lic.UndoLogout();
	// UtilFxn.messageBox(SWT.ICON_INFORMATION, "Information",
	// "Unable to performe database backup.").open();
	// }
	// }
	// }
	// }
	public boolean backupDatabase(String fileName) {
		boolean done = false;
		KeySettingController dio = new KeySettingController();
		LoginController lic = new LoginController();
		String today = jCal.currentDate();
		String dateTime = jCal.getCurrentEnglishDateTime("");

		if (dio.IsUserLoggedIn()) {
			UtilMethods.messageBox(
					SWT.ICON_WARNING,
					"Warning",
					"User(s) are still logged in.\nLog off following users before performing backup.\n"
							+ dio.LoggedInUsersList()).open();
		} else {
			String dbName = ApplicationWorkbenchWindowAdvisor.getBackUpURL()
					+ fileName + "_"
					+ ApplicationWorkbenchWindowAdvisor.getCurrentDbName()
					+ "_" + today.replace("/", "-") + "_"
					+ dateTime.substring(11, 19).replace(":", "");

			if (lic.LogOutUser()) {
				if (dio.createBackupDayOut(dbName,
						ApplicationWorkbenchWindowAdvisor.getCurrentDbName(),
						today + dateTime.substring(11, 19))) {
					lic.UndoLogout();
					// UtilFxn.messageBox(SWT.ICON_INFORMATION, "Information",
					// "Database backup successfully performed.").open();
					done = true;
				} else {
					lic.UndoLogout();
					UtilMethods.messageBox(SWT.ICON_INFORMATION, "Information",
							"Unable to performe database backup.").open();
				}
			}
		}
		return done;
	}

	// Database backup modification by Rabindra Rai May 4, 2012

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

	public static void copyDirectory(File sourceLocation, File targetLocation)
			throws IOException {
		if (sourceLocation.isDirectory()) {
			if (!targetLocation.exists()) {
				targetLocation.mkdir();
			}

			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++) {
				copyDirectory(new File(sourceLocation, children[i]), new File(
						targetLocation, children[i]));
			}
		} else {

			InputStream in = new FileInputStream(sourceLocation);
			OutputStream out = new FileOutputStream(targetLocation);

			// Copy the bits from instream to outstream
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		}
	}// end of method
}// end of class
