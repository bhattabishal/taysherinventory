package com.tia.master.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.tia.common.db.DbConnection;

public class UnitMapController {
	Connection cnn= DbConnection.getConnection();
	
	
	public ResultSet getUnitDetailsById(int id)
	{
		String strQry ="";
		
			strQry ="SELECT\n" +
"un.unit_id,\n" +
"un.conversion_ration,\n" +
"un.status,\n" +
"un.unit_name,\n" +
"un.base_unit,\n" +
"unt.cv_lbl as base_unit_label\n" +

"FROM\n" +
"dbo.inven_unit AS un\n" +
"INNER JOIN dbo.code_value_mcg AS unt ON un.base_unit = unt.cv_code WHERE unt.cv_type='base_unit' AND un.unit_id='"+id+"'";
		
		
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet rsUserDetail = st.executeQuery(strQry);
			if(rsUserDetail.next()){
			return rsUserDetail;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			return null;
	}
	
	public ResultSet GetUnitDetail(int status) {
		String strQry ="";
		if(status <= 0)
		{
			strQry ="SELECT\n" +
"un.unit_id,\n" +
"un.conversion_ration,\n" +
"un.status,\n" +
"un.unit_name,\n" +
"un.base_unit,\n" +
"unt.cv_lbl as base_unit_label\n" +

"FROM\n" +
"dbo.inven_unit AS un\n" +
"INNER JOIN dbo.code_value_mcg AS unt ON un.base_unit = unt.cv_code WHERE unt.cv_type='base_unit'";
		}
		else
		{
			strQry ="SELECT\n" +
			"un.unit_id,\n" +
			"un.conversion_ration,\n" +
			"un.status,\n" +
			"un.unit_name,\n" +
			"un.base_unit,\n" +
			"unt.cv_lbl as base_unit_label\n" +

			"FROM\n" +
			"dbo.inven_unit AS un\n" +
			"INNER JOIN dbo.code_value_mcg AS unt ON un.base_unit = unt.cv_code WHERE unt.cv_type='base_unit' AND un.status'"+status+"'";

		}	
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet rsUserDetail = st.executeQuery(strQry);
			if(rsUserDetail.next()){
			return rsUserDetail;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			return null;
		}
	public ResultSet GetUnitTypeDetail(int id) {
		String strQry ="";
		
			strQry ="select * from inven_unit_type where unit_type_id='"+id+"'";
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet rsUserDetail = st.executeQuery(strQry);
			if(rsUserDetail.next()){
			return rsUserDetail;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
			return null;
		}
	
	
	public boolean DeleteUnit(int unitId){
		String strQry ="Delete  from inven_unit where unit_id='"+unitId+"'";
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			st.executeUpdate(strQry);				
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
		
	}
}
