package com.tia.master.controller;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.db.DbConnection;

public class KeySettingController {
	private static Connection cnn = DbConnection.getConnection();
	private static Statement st = null;
	static Shell sh = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
			.getShell();
	
	public boolean IsUserLoggedIn() {

		boolean flag = false;
		Connection cnn = DbConnection.getConnection();
		String strQry = "SELECT COUNT(*) cnt FROM log_in_info_mcg WHERE "
				+ "ISNULL(log_out_time, 0)=0 " + "AND br_id = '"
				+ 1
				+ "' AND user_id<>'"
				+ ApplicationWorkbenchWindowAdvisor.getLoginId() + "';";

		try {
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry);

			if (rs.next()) {
				if (rs.getInt("cnt") > 0) {
					flag = true;
				}
			}
		} catch (SQLException ex) {
			System.out.println(ex.toString());
		}

		return flag;
	}
	
	public String LoggedInUsersList() {

		String names = "";
		Connection cnn = DbConnection.getConnection();
		String strQry = "SELECT (SELECT full_name FROM user_accounts WHERE "
				+ "user_id = log.user_id) user_name FROM log_in_info_mcg log WHERE br_id = '"
				+ 1
				+ "' AND ISNULL(log_out_time, 0) = 0 AND user_id <> '"
				+ ApplicationWorkbenchWindowAdvisor.getLoginId() + "'";

		try {
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry);

			while (rs.next()) {
				names += "  -  " + rs.getString("user_name") + "\n";
			}
		} catch (SQLException ex) {
			System.out.println(ex.toString());
		}

		return names;
	}
	public static void createBackup(String backUpFileName, String dbSource,
			String dateNow) {
		String sqlQry = " BACKUP DATABASE "
				+ dbSource
				+ " TO  "
				+ " DISK = N'"
				+ backUpFileName
				+ ".bak' WITH NOFORMAT, NOINIT,  "
				+ " NAME = N'skbblDUMMY-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10 ";

		System.out.println("zzz....The Backup is being created.....!");

		try {
			System.out.println("zzz....The Backup QRY= " + sqlQry);
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			st.executeUpdate(sqlQry);
			System.out.println("kkk... The has been created successfully !");
			//SaveQueryController.saveQuery(cnn, sqlQry, "BackUp of " + dbSource
				//	+ " on " + dateNow + "");
			System.out
					.println("kkk... The backup query has been created successfully !");

			// Messaging
			MessageBox msg = new MessageBox(sh, SWT.ICON_INFORMATION);
			msg.setMessage("The Backup of Database '" + dbSource
					+ "' with name " + backUpFileName
					+ " has been created successfully !");
			msg.setText("Backup created");
			msg.open();
		} catch (SQLException e) {
			MessageBox msg = new MessageBox(sh, SWT.ICON_ERROR);
			msg.setMessage("An ERROR occured while trying to create backup for database "
					+ dbSource + " ! The error code is :" + e.toString());
			msg.setText("Backup Creation Failed");
			msg.open();
			System.out
					.println("kkz... An ERROR occured while trying to create backup for database "
							+ dbSource
							+ " ! The error code is :"
							+ e.toString());

		}
	}// end of method createBackup()

	public static boolean createBackupDayOut(String backUpFileName,
			String dbSource, String dateNow) {
		boolean flag = false;
		/*
		 * check whether the path exists or not.
		 * if the path does not exist, system creates it first
		 * and after creation of path the backup will be created.
		 * if the path already exists, system simply creates the backup in it.
		 */
		String x =backUpFileName.substring(0, backUpFileName.lastIndexOf("\\"));
		if(!(new File(x).isDirectory()))
		{
			new File(x).mkdirs();
		}
		String sqlQry = " BACKUP DATABASE "
				+ dbSource
				+ " TO  "
				+ " DISK = N'"
				+ backUpFileName
				+ ".bak' WITH NOFORMAT, NOINIT,  "
				+ " NAME = N'skbblDUMMY-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10 ";

		System.out.println("zzz....The Backup is being created.....!");

		try {
			System.out.println("zzz....The Backup QRY= " + sqlQry);
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			st.executeUpdate(sqlQry);
			System.out.println("kkk... The has been created successfully !");
			//SaveQueryController.saveQuery(cnn, sqlQry, "BackUp of " + dbSource
					//+ " on " + dateNow + "");
			System.out
					.println("kkk... The backup query has been created successfully !");

			boolean isClosing = false;
			
			if (!isClosing)
			{
				// Messaging
				MessageBox msg = new MessageBox(sh, SWT.ICON_INFORMATION);
				msg.setMessage("The Backup of Database '" + dbSource
					+ "' with name " + backUpFileName
					+ " has been created successfully !");
				msg.setText("Backup created");
				msg.open();
			}
			flag = true;
		} catch (SQLException e) {
			MessageBox msg = new MessageBox(sh, SWT.ICON_ERROR);
			msg.setMessage("An ERROR occured while trying to create backup for database "
					+ dbSource + " ! The error code is :" + e.toString());
			msg.setText("Backup Creation Failed");
			msg.open();
			System.out
					.println("kkz... An ERROR occured while trying to create backup for database "
							+ dbSource
							+ " ! The error code is :"
							+ e.toString());

		}

		return flag;

	}


}
