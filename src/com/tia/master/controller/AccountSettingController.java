package com.tia.master.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.tia.common.db.DbConnection;

import com.tia.master.model.AccountSettingModel;

public class AccountSettingController {
	Connection cnn= DbConnection.getConnection();
	
	public AccountSettingModel GetAccountDetail() {
		AccountSettingModel model=new AccountSettingModel();
		String strQry ="";
		
			strQry ="SELECT * from inven_acc_setting";

			
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry);
			if(rs.next()){
			model.setAcc_set_id(rs.getInt("acc_set_id"));
			model.setItem_purchased_credit_cr(rs.getString("item_purchased_credit_cr"));
			model.setItem_purchased_dr(rs.getString("item_purchased_dr"));
			model.setItem_purchased_expense_cr(rs.getString("item_purchased_expense_cr"));
			model.setItem_purchased_expense_dr(rs.getString("item_purchased_expense_dr"));
			model.setItem_sold_cr(rs.getString("item_sold_cr"));
			model.setItem_sold_discount_dr(rs.getString("item_sold_discount_dr"));
			model.setItem_sold_discount_cr(rs.getString("item_sold_discount_cr"));
			model.setItem_sold_vat_dr(rs.getString("item_sold_vat_dr"));
			model.setItem_sold_vat_cr(rs.getString("item_sold_vat_cr"));
			model.setItem_sold_credit_dr(rs.getString("item_sold_credit_dr"));
			model.setService_earned_cr(rs.getString("service_earned_cr"));
			model.setService_earned_dr(rs.getString("service_earned_dr"));
			model.setService_expense_cr(rs.getString("service_expense_cr"));
			model.setService_expense_dr(rs.getString("service_expense_dr"));
			}
			else
			{
				return null;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
			return model;
		}
	public boolean saveAccountSetting(AccountSettingModel model) {

		boolean retFlag = true;
		String sql = "";

		if (model.getAcc_set_id() == 0) {
			// its the insert operation
			sql = model.Insert();
		} else {
			// its the update operation
			model.z_WhereClause.acc_set_id(model
					.getAcc_set_id());
			// may need to be added
			/*
			 * objCustomerModel.z_WhereClause.br_id(
			 * ApplicationWorkbenchWindowAdvisor .getUserId());
			 */
			sql = model.Update();
		}

		try {

			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			// if the single user is inserted/update, its successful
			if (st.executeUpdate(sql) == 1)
				retFlag = true;
			else
				retFlag = false;

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return retFlag;

	}
}
