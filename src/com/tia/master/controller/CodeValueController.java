package com.tia.master.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilMethods;
import com.tia.master.model.CodeValueModel;



public class CodeValueController {
	private Connection cnn = DbConnection.getConnection();
	private Statement st;
	private ResultSet rs;
	private String cv_type;
	
	public CodeValueController() {
		// db connection
		this.cnn = DbConnection.getConnection();
		cv_type = "";

	}
	
	//inserts record into the database
	public boolean Insert(CodeValueModel objModel)
	{
		boolean value=false;
		int id=UtilMethods.getMaxId("code_value_mcg", "cv_id");
		//objModel.setCv_id(id);
		String strQry=objModel.Insert();
		try {
			this.st=cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			if ((st.executeUpdate(strQry) == 1))
			{
				value=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return value;
	}
	public boolean Update(CodeValueModel objModel)
	{
		boolean value=false;
		//int id=UtilFxn.getMaxId("code_value_mcg", "cv_id");
		//objModel.setCv_id(id);
		String strQry= "Update code_value_mcg set cv_type = '"+objModel.getCv_type()+"', cv_code = '"+objModel.getCv_code()+"', cv_lbl = '"+objModel.getCv_lbl()+"' where cv_id = '"+objModel.getCv_id()+"'";
		System.out.println("This is the query for update of the code value:"+strQry);
		try {
			this.st=cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			if ((st.executeUpdate(strQry) == 1))
			{
				value=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return value;
	}
	
	public ResultSet getCodeValueDetails(String code_type){
		System.out.println(code_type);
		String strQry = "select * from code_value_mcg where cv_type = '"+code_type+"'";
		try {
			System.out.println(strQry);
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);

			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	

}
