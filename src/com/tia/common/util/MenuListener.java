package com.tia.common.util;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.user.view.ChangePasswordDialog;

//import com.ntb.ApplicationWorkbenchWindowAdvisor;

public class MenuListener implements IWorkbenchWindowActionDelegate {

	@Override
	public void run(IAction action) {
		IWorkbenchPage wbp = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		UtilMethods objUtilMethods = new UtilMethods();
		
		try {

			if (action.getId().toString().equals("com.ntb.hall.hallBooking")) {
				wbp.showView("com.ntb.hall.hallBookingView");

			} else if (action.getId().equalsIgnoreCase(
					"com.ntb.hall.customerMgmt")) {
				if(ApplicationWorkbenchWindowAdvisor.getStatus() < 3){
					wbp.showView("com.ntb.hall.customerMgmtView");
				}else {
					UtilMethods.messageBox(SWT.ICON_WARNING, "Information",
					"You do not have Privilege. Please contact Administrator.").open();
				}
			} else if (action.getId().equalsIgnoreCase(
					"com.ntb.hall.hallBookingMgmt")) {
				wbp.showView("com.ntb.hall.hallBookingMgmtView");
			} else if (action.getId().toString().equals("mnuHallMgmtSys")) {
				if(ApplicationWorkbenchWindowAdvisor.getStatus() == 1){
				wbp.showView("com.ntb.hall.hallMgmt");
				}else {
					UtilMethods.messageBox(SWT.ICON_WARNING, "Information",
					"You do not have Privilege. Please contact Administrator.").open();
				}
			} else if (action.getId().toString().equals("mnuBookScheme")) {
				if(ApplicationWorkbenchWindowAdvisor.getStatus() == 1){
				wbp.showView("com.ntb.bookingscheme.bookScheme");
				}else {
					UtilMethods.messageBox(SWT.ICON_WARNING, "Information",
					"You do not have Privilege. Please contact Administrator.").open();
				}
			} else if (action.getId().toString().equals("mnuUserManagement")) {
				if(ApplicationWorkbenchWindowAdvisor.getStatus() == 1){
				wbp.showView("com.ntb.user.userManagement");
				}else {
					UtilMethods.messageBox(SWT.ICON_WARNING, "Information",
					"You do not have Privilege. Please contact Administrator.").open();
				}
			}else if (action.getId().toString().equals("mnuRoleManagment")&& ApplicationWorkbenchWindowAdvisor
					.hasAuthOr(new String[] { "role_view" })) {
				
					wbp.showView("com.tia.user.roleManage");
					
				}
			else if (action.getId().toString().equals("mnuAccHead")&& ApplicationWorkbenchWindowAdvisor
					.hasAuthOr(new String[] { "account_head_view" })) {
				
					wbp.showView("com.tia.account.accountHead");
					
				}
			else if (action.getId().toString().equals("mnuJournalVoucher")&& ApplicationWorkbenchWindowAdvisor
					.hasAuthOr(new String[] { "jv_view" })) {
				
					wbp.showView("com.tia.account.journalVoucher");
					
				}
			else if (action.getId().toString().equals("mnuTrialBalance")&& ApplicationWorkbenchWindowAdvisor
					.hasAuthOr(new String[] { "trial_balance_view" })) {
				
					wbp.showView("com.tia.account.trialBalance");
					
				}
			else if (action.getId().toString().equals("mnuJournalVoucherReport")&& ApplicationWorkbenchWindowAdvisor
					.hasAuthOr(new String[] { "jv_report_view" })) {
				
					wbp.showView("com.tia.account.journalvoucher");
					
				}
			else if (action.getId().toString().equals("mnuBalanceSheet")&& ApplicationWorkbenchWindowAdvisor
					.hasAuthOr(new String[] { "bs_view" })) {
				
					wbp.showView("com.tia.account.balanceSheet");
					
				}
			else if (action.getId().toString().equals("mnuLedger")&& ApplicationWorkbenchWindowAdvisor
					.hasAuthOr(new String[] { "ledger_view" })) {
				
					wbp.showView("com.tia.account.ledger");
					
				}
			else if (action.getId().toString().equals("mnuProfitLoss")&& ApplicationWorkbenchWindowAdvisor
					.hasAuthOr(new String[] { "profitloss_view" })) {
				
					wbp.showView("com.tia.account.profitloss");
					
				}
			else if (action.getId().toString().equals("mnuUnitMap")&& ApplicationWorkbenchWindowAdvisor
					.hasAuthOr(new String[] { "unit_view" })) {
				
					wbp.showView("com.tia.master.unitMap");
					
				}
			else if (action.getId().toString().equals("mnuDbBackup")) {
				
					wbp.showView("com.tia.view.dbBackup");
					
				}
			else if (action.getId().toString().equals("mnuCodeValue")&& ApplicationWorkbenchWindowAdvisor
					.hasAuthOr(new String[] { "code_value_view" })) {
				
					wbp.showView("com.tia.master.codeValue");
					
				}
			else if (action.getId().toString().equals("mnuStockCat")&& ApplicationWorkbenchWindowAdvisor
					.hasAuthOr(new String[] { "cat_stock_view" })) {
				
					wbp.showView("com.tia.inven.stockCat");
					
				}
			else if (action.getId().toString().equals("mnuStockItem")&& ApplicationWorkbenchWindowAdvisor
					.hasAuthOr(new String[] { "item_stock_view" })) {
				
					wbp.showView("com.tia.inven.stockList");
					
				}
			else if (action.getId().toString().equals("mnuStockPurchasedList")) {
				
					wbp.showView("com.tia.inven.stockPurchased");
					
				}
			else if (action.getId().toString().equals("mnuStockSalesList")) {
				
				wbp.showView("com.tia.inven.salesList");
				
			}
			else if (action.getId().toString().equals("mnuCustSupplier")) {				
				wbp.showView("com.tia.inven.cusSuppList");			
			}
			else if (action.getId().toString().equals("mnuBookingListReport")) {
				wbp.showView("com.ntb.hall.bookingListReportView");
			} else if (action.getId().toString().equals("mnuHallBookingReport")) {
				wbp.showView("com.ntb.hall.hallBookingReport");
			}else if (action.getId().toString().equals("mnuChangePassword")) {
				ChangePasswordDialog cngpsword = new ChangePasswordDialog(
						PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getShell());
				cngpsword.open();
			}

		} catch (PartInitException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// System.out.println("this menu is Working");
	}

	@Override
	public void dispose() {

	}

	@Override
	public void init(IWorkbenchWindow window) {
		// System.out.println("this menu is Working");

	}

}
