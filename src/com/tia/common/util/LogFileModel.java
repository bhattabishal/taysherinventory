package com.tia.common.util;

public class LogFileModel {

	private String m_details;
	private String m_system_date;
	private String m_sub_module;
	private int m_log_id;
	private String m_user_id;
	private String m_log_date;
	private String m_module;
	private String m_activity;
	private _Criteria z_WhereClause;

	private IsDirty_ z_bool;

	public LogFileModel() {
		z_WhereClause = new _Criteria();
		z_bool = new IsDirty_();
	}

	public class IsDirty_ {
		public boolean m_details;
		public boolean m_system_date;
		public boolean m_sub_module;
		public boolean m_log_id;
		public boolean m_user_id;
		public boolean m_log_date;
		public boolean m_module;
		public boolean m_activity;
	}

	public String getDetails() {
		return m_details;
	}

	public void setDetails(String value) {
		z_bool.m_details = true;
		m_details = value;
	}

	public String getSystem_date() {
		return m_system_date;
	}

	public void setSystem_date(String value) {
		z_bool.m_system_date = true;
		m_system_date = value;
	}

	public String getSub_module() {
		return m_sub_module;
	}

	public void setSub_module(String value) {
		z_bool.m_sub_module = true;
		m_sub_module = value;
	}

	public int getLog_id() {
		return m_log_id;
	}

	public void setLog_id(int value) {
		z_bool.m_log_id = true;
		m_log_id = value;
	}

	public String getUser_id() {
		return m_user_id;
	}

	public void setUser_id(String value) {
		z_bool.m_user_id = true;
		m_user_id = value;
	}

	public String getLog_date() {
		return m_log_date;
	}

	public void setLog_date(String value) {
		z_bool.m_log_date = true;
		m_log_date = value;
	}

	public String getModule() {
		return m_module;
	}

	public void setModule(String value) {
		z_bool.m_module = true;
		m_module = value;
	}

	public String getActivity() {
		return m_activity;
	}

	public void setActivity(String value) {
		z_bool.m_activity = true;
		m_activity = value;
	}

	/*
	 * public _Criteria Where() { return z_WhereClause; } public _Criteria
	 * Where(_Criteria value) { z_WhereClause = value; } }
	 */
	public String Insert() {

		String z_sep = "";
		String SQL = "INSERT INTO log_table_mcg ( ";
		if (z_bool.m_details) {
			SQL += z_sep + "details";
			z_sep = " , ";
		}
		if (z_bool.m_system_date) {
			SQL += z_sep + "system_date";
			z_sep = " , ";
		}
		if (z_bool.m_sub_module) {
			SQL += z_sep + "sub_module";
			z_sep = " , ";
		}
		if (z_bool.m_log_id) {
			SQL += z_sep + "log_id";
			z_sep = " , ";
		}
		if (z_bool.m_user_id) {
			SQL += z_sep + "user_id";
			z_sep = " , ";
		}
		if (z_bool.m_log_date) {
			SQL += z_sep + "log_date";
			z_sep = " , ";
		}
		if (z_bool.m_module) {
			SQL += z_sep + "module";
			z_sep = " , ";
		}
		if (z_bool.m_activity) {
			SQL += z_sep + "activity";
			z_sep = " , ";
		}
		SQL += ") VALUES (";
		z_sep = "";
		if (z_bool.m_details) {
			SQL += z_sep + "'" + m_details + "'";
			z_sep = " , ";
		}
		if (z_bool.m_system_date) {
			SQL += z_sep + "'" + m_system_date + "'";
			z_sep = " , ";
		}
		if (z_bool.m_sub_module) {
			SQL += z_sep + "'" + m_sub_module + "'";
			z_sep = " , ";
		}
		if (z_bool.m_log_id) {
			SQL += z_sep + "'" + m_log_id + "'";
			z_sep = " , ";
		}
		if (z_bool.m_user_id) {
			SQL += z_sep + "'" + m_user_id + "'";
			z_sep = " , ";
		}
		if (z_bool.m_log_date) {
			SQL += z_sep + "'" + m_log_date + "'";
			z_sep = " , ";
		}
		if (z_bool.m_module) {
			SQL += z_sep + "'" + m_module + "'";
			z_sep = " , ";
		}
		if (z_bool.m_activity) {
			SQL += z_sep + "'" + m_activity + "'";
			z_sep = " , ";
		}
		SQL += ")";
		return SQL;
	}

	public String Update() {
		String z_sep = "";
		String SQL = "UPDATE log_table_mcg SET ";
		if (z_bool.m_details) {
			SQL += z_sep + "details='" + m_details + "'";
			z_sep = " , ";
		}
		if (z_bool.m_system_date) {
			SQL += z_sep + "system_date='" + m_system_date + "'";
			z_sep = " , ";
		}
		if (z_bool.m_sub_module) {
			SQL += z_sep + "sub_module='" + m_sub_module + "'";
			z_sep = " , ";
		}
		if (z_bool.m_log_id) {
			SQL += z_sep + "log_id='" + m_log_id + "'";
			z_sep = " , ";
		}
		if (z_bool.m_user_id) {
			SQL += z_sep + "user_id='" + m_user_id + "'";
			z_sep = " , ";
		}
		if (z_bool.m_log_date) {
			SQL += z_sep + "log_date='" + m_log_date + "'";
			z_sep = " , ";
		}
		if (z_bool.m_module) {
			SQL += z_sep + "module='" + m_module + "'";
			z_sep = " , ";
		}
		if (z_bool.m_activity) {
			SQL += z_sep + "activity='" + m_activity + "'";
			z_sep = " , ";
		}
		z_sep = " WHERE ";
		if (z_WhereClause.z_bool.m_details) {
			SQL += z_sep + "details='" + z_WhereClause.details() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_system_date) {
			SQL += z_sep + "system_date='" + z_WhereClause.system_date() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_sub_module) {
			SQL += z_sep + "sub_module='" + z_WhereClause.sub_module() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_log_id) {
			SQL += z_sep + "log_id='" + z_WhereClause.log_id() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_user_id) {
			SQL += z_sep + "user_id='" + z_WhereClause.user_id() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_log_date) {
			SQL += z_sep + "log_date='" + z_WhereClause.log_date() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_module) {
			SQL += z_sep + "module='" + z_WhereClause.module() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_activity) {
			SQL += z_sep + "activity='" + z_WhereClause.activity() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.MyWhere) {
			SQL += z_sep + z_WhereClause.MyWhere();
			z_sep = " AND ";
		}
		return SQL;
	}

	public String Delete() {
		String z_sep = " WHERE ";
		String SQL = "DELETE FROM log_table_mcg";
		if (z_WhereClause.z_bool.m_details) {
			SQL += z_sep + "details='" + z_WhereClause.details() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_system_date) {
			SQL += z_sep + "system_date='" + z_WhereClause.system_date() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_sub_module) {
			SQL += z_sep + "sub_module='" + z_WhereClause.sub_module() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_log_id) {
			SQL += z_sep + "log_id='" + z_WhereClause.log_id() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_user_id) {
			SQL += z_sep + "user_id='" + z_WhereClause.user_id() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_log_date) {
			SQL += z_sep + "log_date='" + z_WhereClause.log_date() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_module) {
			SQL += z_sep + "module='" + z_WhereClause.module() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_activity) {
			SQL += z_sep + "activity='" + z_WhereClause.activity() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.MyWhere) {
			SQL += z_sep + z_WhereClause.MyWhere();
			z_sep = " AND ";
		}
		return SQL;
	}

	public String SearchSQL() {
		String z_sep = " WHERE ";
		String SQL = "SELECT * FROM log_table_mcg";
		if (z_WhereClause.z_bool.m_details) {
			SQL += z_sep + "details='" + z_WhereClause.details() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_system_date) {
			SQL += z_sep + "system_date='" + z_WhereClause.system_date() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_sub_module) {
			SQL += z_sep + "sub_module='" + z_WhereClause.sub_module() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_log_id) {
			SQL += z_sep + "log_id='" + z_WhereClause.log_id() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_user_id) {
			SQL += z_sep + "user_id='" + z_WhereClause.user_id() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_log_date) {
			SQL += z_sep + "log_date='" + z_WhereClause.log_date() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_module) {
			SQL += z_sep + "module='" + z_WhereClause.module() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.m_activity) {
			SQL += z_sep + "activity='" + z_WhereClause.activity() + "'";
			z_sep = " AND ";
		}
		if (z_WhereClause.z_bool.MyWhere) {
			SQL += z_sep + z_WhereClause.MyWhere();
			z_sep = " AND ";
		}
		return SQL;
	}

	// / <summary>
	// / Generated Class for Table : _Criteria.
	// / </summary>
	public class _Criteria {
		private String m_details;
		private String m_system_date;
		private String m_sub_module;
		private int m_log_id;
		private String m_user_id;
		private String m_log_date;
		private String m_module;
		private String m_activity;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep = " ";

		public IsDirty__Criteria z_bool;

		public _Criteria()

		{
			z_bool = new IsDirty__Criteria();
		}

		public class IsDirty__Criteria {
			public boolean m_details;
			public boolean m_system_date;
			public boolean m_sub_module;
			public boolean m_log_id;
			public boolean m_user_id;
			public boolean m_log_date;
			public boolean m_module;
			public boolean m_activity;
			public boolean MyWhere;

		}

		public String details() {
			return m_details;
		}

		public void details(String value) {
			z_bool.m_details = true;
			m_details = value;
			if (z_bool.m_details) {
				_zWhereClause += z_sep + "details='" + details() + "'";
				z_sep = " AND ";
			}
		}

		public String system_date() {
			return m_system_date;
		}

		public void system_date(String value) {
			z_bool.m_system_date = true;
			m_system_date = value;
			if (z_bool.m_system_date) {
				_zWhereClause += z_sep + "system_date='" + system_date() + "'";
				z_sep = " AND ";
			}
		}

		public String sub_module() {
			return m_sub_module;
		}

		public void sub_module(String value) {
			z_bool.m_sub_module = true;
			m_sub_module = value;
			if (z_bool.m_sub_module) {
				_zWhereClause += z_sep + "sub_module='" + sub_module() + "'";
				z_sep = " AND ";
			}
		}

		public int log_id() {
			return m_log_id;
		}

		public void log_id(int value) {
			z_bool.m_log_id = true;
			m_log_id = value;
			if (z_bool.m_log_id) {
				_zWhereClause += z_sep + "log_id='" + log_id() + "'";
				z_sep = " AND ";
			}
		}

		public String user_id() {
			return m_user_id;
		}

		public void user_id(String value) {
			z_bool.m_user_id = true;
			m_user_id = value;
			if (z_bool.m_user_id) {
				_zWhereClause += z_sep + "user_id='" + user_id() + "'";
				z_sep = " AND ";
			}
		}

		public String log_date() {
			return m_log_date;
		}

		public void log_date(String value) {
			z_bool.m_log_date = true;
			m_log_date = value;
			if (z_bool.m_log_date) {
				_zWhereClause += z_sep + "log_date='" + log_date() + "'";
				z_sep = " AND ";
			}
		}

		public String module() {
			return m_module;
		}

		public void module(String value) {
			z_bool.m_module = true;
			m_module = value;
			if (z_bool.m_module) {
				_zWhereClause += z_sep + "module='" + module() + "'";
				z_sep = " AND ";
			}
		}

		public String activity() {
			return m_activity;
		}

		public void activity(String value) {
			z_bool.m_activity = true;
			m_activity = value;
			if (z_bool.m_activity) {
				_zWhereClause += z_sep + "activity='" + activity() + "'";
				z_sep = " AND ";
			}
		}

		public String MyWhere() {
			return z_MyWhere;
		}

		public void MyWhere(String value) {
			z_bool.MyWhere = true;
			z_MyWhere = value;
			if (z_bool.MyWhere) {
				_zWhereClause += z_sep + z_MyWhere;
				z_sep = " AND ";
			}
		}

		public String WhereClause() {
			return _zWhereClause;
		}
	}
}