package com.tia.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator{
	 
	  private Pattern pattern;
	  private Matcher matcher;

	  private static final String TIME24HOURS_PATTERN ="([01]?[0-9]|2[0-3]):[0-5][0-9]";

	  public Validator(){
		  pattern = Pattern.compile(TIME24HOURS_PATTERN);
	  }

	  /**
	   * Validate time in 24 hours format with regular expression
	   * @param time time address for validation
	   * @return true valid time format, false invalid time format
	   */
	  public boolean validateTime(final String time){

		  matcher = pattern.matcher(time);
		  return matcher.matches();

	  }
	    public boolean validateEmail(String email)
	    {
	        boolean validEmail;
	        String Email=email;
	        Pattern pattern = Pattern.compile("^[\\w\\.-]{1,}\\@([\\da-zA-Z-]{1,}\\.){1,}[\\da-zA-Z-]+$"); //represents all valid EmailIDs        
	        Matcher matcher = pattern.matcher(Email);         
	              if (!matcher.matches()) 
	              {
	                  validEmail=false;
	                  //System.out.println("Email is not a Valid one !");
	              }
	              else
	                {
	                  validEmail=true;
	                }
	              return validEmail;         
	    }
}