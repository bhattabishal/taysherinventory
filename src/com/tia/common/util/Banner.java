package com.tia.common.util;

//import org.eclipse.jface.viewers.CellEditor.LayoutData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
//import org.eclipse.ui.part.ViewPart;

import com.swtdesigner.SWTResourceManager;

public class Banner {

	public static Composite getTitle(String strTitle, Composite mainComposite,
			@SuppressWarnings("rawtypes") Class classOfViewPart) {
		// Composite for Title
		Composite comTitle = new Composite(mainComposite, SWT.NONE);
		GridData grdDtcomTitle = new GridData(SWT.LEFT, SWT.CENTER, true,
				false, 0, 0);
		grdDtcomTitle.heightHint = 35;
		grdDtcomTitle.widthHint = 990;
		comTitle.setLayoutData(grdDtcomTitle);
		// comTitle.setLocation(, y)
		GridLayout grdLyotComTtl = new GridLayout(2, false);
		grdLyotComTtl.marginWidth = 0;
		grdLyotComTtl.marginHeight = 0;
		comTitle.setLayout(grdLyotComTtl);
//		comTitle.setBackground(SWTResourceManager.getColor(51, 102, 0));
//		comTitle.setBackground(SWTResourceManager.getColor(50, 62, 113));
		comTitle.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));

		Label lblTitle = new Label(comTitle, SWT.NONE);
		GridData grdDtlblTitle = new GridData(SWT.LEFT, SWT.CENTER, false,false, 0, 0);
		grdDtlblTitle.horizontalIndent = 10;
		grdDtlblTitle.verticalIndent = 5;
		grdDtlblTitle.heightHint = 35;
		grdDtlblTitle.widthHint = 880;
		lblTitle.setLayoutData(grdDtlblTitle);
		lblTitle.setBounds(0, 0, 880, 35);
		lblTitle.setFont(SWTResourceManager.getFont("Arial", 16, SWT.BOLD));
//		lblTitle.setBackground(SWTResourceManager.getColor(51, 102, 0));
		lblTitle.setForeground(SWTResourceManager.getColor(50, 62, 113));
		lblTitle.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblTitle.setText(strTitle);

		Label lblImage = new Label(comTitle, SWT.NONE);
		GridData grdDtlblImage = new GridData(SWT.LEFT, SWT.CENTER, false,false, 0, 0);
		grdDtlblImage.widthHint = 100;
		grdDtlblImage.heightHint = 35;
		lblImage.setLayoutData(grdDtlblImage);
		lblImage.setAlignment(SWT.RIGHT);
		lblImage.setBounds(0, 0, 100, 35);
		lblImage.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblImage.setImage(SWTResourceManager.getImage(classOfViewPart,"/back.png"));

		return comTitle;
	}

	public static Composite getLabel(String strTitle, Composite mainComposite,
			@SuppressWarnings("rawtypes") Class classOfViewPart,int intWidth) {
		// Composite for Title
		Composite comTitle = new Composite(mainComposite, SWT.NONE);
		GridData grdDtcomTitle = new GridData(SWT.LEFT, SWT.CENTER, true,
				false, 0, 0);
		grdDtcomTitle.heightHint = 30;
		grdDtcomTitle.widthHint = intWidth;
		comTitle.setLayoutData(grdDtcomTitle);
		GridLayout grdLyotComTtl = new GridLayout(1, false);
		grdLyotComTtl.marginWidth = 0;
		grdLyotComTtl.marginHeight = 0;
		comTitle.setLayout(grdLyotComTtl);
		comTitle.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));

		Label lblTitle = new Label(comTitle, SWT.NONE);
		GridData grdDtlblTitle = new GridData(SWT.LEFT, SWT.CENTER, false,false, 0, 0);
		grdDtlblTitle.horizontalIndent = 10;
		grdDtlblTitle.verticalIndent = 5;
		grdDtlblTitle.heightHint = 20;
		grdDtlblTitle.widthHint = intWidth-20;
		int width = intWidth-20;
		lblTitle.setLayoutData(grdDtlblTitle);
		lblTitle.setBounds(0, 0, width, 20);
		lblTitle.setFont(SWTResourceManager.getFont("Arial", 12, SWT.BOLD));
		lblTitle.setForeground(SWTResourceManager.getColor(50, 62, 113));
		lblTitle.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblTitle.setText(strTitle);

		return comTitle;
	}

	public static Label getLabel(String strTitle, Composite comParent,
			int intWidth) {
		Label lblTitle = new Label(comParent, SWT.NONE);
		GridData grdDtlblTitle = new GridData(SWT.LEFT, SWT.CENTER, false,false, 0, 0);
//		grdDtlblTitle.horizontalIndent = 10;
//		grdDtlblTitle.verticalIndent = 5;
		lblTitle.setLayoutData(grdDtlblTitle);
		lblTitle.setBounds(0, 0, intWidth, 30);
//		lblTitle.setBackground(SWTResourceManager.getColor(51, 102, 0));
		lblTitle.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
//		lblTitle.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblTitle.setForeground(SWTResourceManager.getColor(50, 62, 113));
		lblTitle.setFont(SWTResourceManager.getFont("Arial", 12, SWT.BOLD));
		lblTitle.setText(strTitle);

		return lblTitle;
	}
}
