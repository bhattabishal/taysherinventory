package com.tia.common.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.db.DbConnection;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;



public class LogFileCreate {
	private static JCalendarFunctions jcal=new JCalendarFunctions();

	public static void logFileCreater(String module_name,
			String module_sub_name, String activity, String summary) {
		Connection cnn = DbConnection.getConnection();
		LogFileModel log = new LogFileModel();
		log.setModule(module_name);
		log.setSub_module(module_sub_name);
		log.setActivity(activity);
		log.setUser_id(Integer.toString(ApplicationWorkbenchWindowAdvisor
				.getLoginId()));
		log.setLog_date(jcal.getCurrentNepaliDate());
		log.setDetails(summary);

		log.setSystem_date(JCalendarFunctions.getSystemDateAndTime());

		String sumry = "\t(Module:) " + module_name + "  (SubModule:) "
				+ module_sub_name + "  (Activity:) " + activity + " (Time:)  "
				+ JCalendarFunctions.getSystemDateAndTime()
				+ "  \n\t\t(Details:)\n\t\t" + summary + "\n";
		LogFileCreate aa = new LogFileCreate();
		aa.write(cnn, "", sumry, log);
	}

	public static void logFileCreater(Connection cnn, String module_name,
			String module_sub_name, String activity, ArrayList<String> log1) {

		String summary = "";
		for (int i = 0; i < log1.size(); i++) {
			System.out.println("Summary-> " + log1.get(i));
			summary += log1.get(i);
		}

		LogFileModel log = new LogFileModel();
		log.setModule(module_name);
		log.setSub_module(module_sub_name);
		log.setActivity(activity);
		log.setUser_id(Integer.toString(ApplicationWorkbenchWindowAdvisor
				.getLoginId()));
		log.setLog_date(jcal.getCurrentNepaliDate());
		log.setDetails(summary);
		log.setSystem_date(JCalendarFunctions.getSystemDateAndTime());

		String sumry = "\t(Module:) " + module_name + "  (SubModule:) "
				+ module_sub_name + "  (Activity:) " + activity + " (Time:)  "
				+ JCalendarFunctions.getSystemDateAndTime()
				+ "  \n\t\t(Details:)\n\t\t" + summary + "\n";
		LogFileCreate aa = new LogFileCreate();
		aa.write(cnn, "", sumry, log);
	}

	public static void logAtLogin() {
		Connection cnn = DbConnection.getConnection();

		String ab = "\n**************************************************************************************\n\n(Login Date:) "
				+ jcal.getCurrentNepaliDate()
				+ " (System Date:) "
				+ JCalendarFunctions.getSystemDateAndTime()
				+ "  (User Name:) "
				+ ApplicationWorkbenchWindowAdvisor.getCurrentUser()
				+ " (User Id:) "
				+ ApplicationWorkbenchWindowAdvisor.getLoginId() + " : \n";
		LogFileCreate aa = new LogFileCreate();
		LogFileModel log = new LogFileModel();
		log.setLog_date(jcal.getCurrentNepaliDate());
		log.setSystem_date(JCalendarFunctions.getSystemDateAndTime());
		log.setUser_id(Integer.toString(ApplicationWorkbenchWindowAdvisor
				.getLoginId()));
		log.setActivity("Login");
		aa.write(cnn, ab, "", log);
	}

	public static void logAtLogout() {
		Connection cnn = DbConnection.getConnection();

		String ab = "\n(Logout Date:) "
				+ jcal.getCurrentNepaliDate()
				+ " (System Date:) " + JCalendarFunctions.getSystemDateAndTime()
				+ "  (User NAme:) "
				+ ApplicationWorkbenchWindowAdvisor.getCurrentUser()
				+ " (User Id:) "
				+ ApplicationWorkbenchWindowAdvisor.getLoginId() + " : \n";
		LogFileCreate aa = new LogFileCreate();
		LogFileModel log = new LogFileModel();
		log.setLog_date(jcal.getCurrentNepaliDate());
		log.setSystem_date(JCalendarFunctions.getSystemDateAndTime());
		log.setUser_id(Integer.toString(ApplicationWorkbenchWindowAdvisor
				.getLoginId()));
		log.setActivity("Logout");
		aa.write(cnn, ab, "", log);
	}

	private void write(Connection cnn, String ab, String summary,
			LogFileModel log) {

		JCalendarFunctions j = new JCalendarFunctions();
		String date = j.getEndOfMonthDate(
				jcal.getCurrentNepaliDate())
				.replaceAll("/", "-");
		String path = ApplicationWorkbenchWindowAdvisor.getBackUpURL() + "log";
		File file = new File(path);
		if (file.isDirectory()) {
			path += "\\logfile" + date.replaceAll(j.getSeperator(), "");
		} else {
			file.mkdir();
			path += "\\logfile" + date.replaceAll(j.getSeperator(), "");
			try {
				Runtime.getRuntime().exec(
						"attrib +H +R +S +A " + file.getPath());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		File files = new File(path);
		try {
			Statement st = cnn.createStatement();
			String query = log.Insert();
			int i = st.executeUpdate(query);
			if (i > 0) {
				try {
					if (files.createNewFile()) {
						Runtime.getRuntime().exec(
								"attrib +H " + files.getPath());
					
					} 
					FileWriter fw = new FileWriter(files, true);
					fw.write(ab);
					fw.write(summary);
					fw.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
