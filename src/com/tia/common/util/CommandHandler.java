package com.tia.common.util;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.user.controller.LoginController;

public class CommandHandler extends AbstractHandler {
	
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchPage wbp = PlatformUI.getWorkbench()
		.getActiveWorkbenchWindow().getActivePage();
		Shell sh = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		UtilMethods objUtilMethods = new UtilMethods();
		LoginController lic = new LoginController();
//		else if (event.getCommand().getId().equals("ntb.cmd.user")) {
//			if(ApplicationWorkbenchWindowAdvisor.getStatus() == 1){
//				wbp.showView("com.ntb.user.userManagement");
//			}else {
//				UtilMethods.messageBox(SWT.ICON_WARNING, "Information",
//				"You do not have Privilege. Please contact Administrator.").open();
//			}
//		} 
		try {
		if (event.getCommand().getId().equals("ntb.cmd.servicerecord")) {
			
				wbp.showView("com.tia.inven.serviceRecord");	
			
		} else if (event.getCommand().getId().equals("tia.cmd.customer")) {
			
				wbp.showView("com.tia.inven.cusSuppList");
			
		} else if (event.getCommand().getId().equals("tia.cmd.stock")) {
			
				wbp.showView("com.tia.inven.stockPurchased");
		} else if (event.getCommand().getId().equals("tia.cmd.user")) {
			wbp.showView("com.ntb.user.userManagement");
		} else if (event.getCommand().getId().equals("tia.cmd.sales")) {
			
				wbp.showView("com.tia.inven.salesList");
		}
		else if (event.getCommand().getId().equals("tia.manual.pdf")) {
			
			if (Desktop.isDesktopSupported()) {
				Desktop desktop = Desktop.getDesktop();
				String currentDir = new File(".").getAbsolutePath();
				String dir = System.getProperty("user.dir");
				try {
					File file = new File(dir
							+ "/User Manual Teyseer.pdf");
					if (file.isFile()) {
						desktop.open(file);
					} else {
						MessageBox mb = new MessageBox(sh, SWT.ICON_ERROR);
						mb.setMessage("File User Manual Teyseer.pdf not found!");
						mb.setText("File Not Found");
						mb.open();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
	}
		else if (event.getCommand().getId().equals("ntb.cmd.logout")) {
						
			MessageBox quesBox = new MessageBox(sh, SWT.YES | SWT.NO);
			quesBox.setText("Confirm Logout");
			quesBox.setMessage("Are you really want to logout the system?");			
			if (quesBox.open() == SWT.YES) {						
				//
				if (lic.LogOutUser()) {
					String[] strButton = {"ok"};
					UtilMethods.messageBox(
							SWT.ICON_INFORMATION,
							"Exit",
							"GoodBye"
									+ " "
									+ ApplicationWorkbenchWindowAdvisor
											.getCurrentUser() + " .",
							strButton).open();
					wbp.getWorkbenchWindow().getWorkbench().restart();
				} else {
					UtilMethods.messageBox(SWT.ICON_WARNING,
							"LogOut",
							"Unable to log out at this time.")
							.open();
				}
			}
		} else if (event.getCommand().getId().equals("ntb.cmd.exit")) {
			MessageBox quesBox = new MessageBox(sh, SWT.YES | SWT.NO);
			quesBox.setText("Confirm System Exit");
			quesBox.setMessage("Are you really want to exit the system?");			
			if (quesBox.open() == SWT.YES) {
				if (lic.LogOutUser()) {
					String[] strButton = {"ok"};
					UtilMethods.messageBox(
							SWT.ICON_INFORMATION,
							"Exit",
							"GoodBye"
									+ " "
									+ ApplicationWorkbenchWindowAdvisor
											.getCurrentUser() + " .",
							strButton).open();
					HandlerUtil.getActiveWorkbenchWindow(event).close();
				} else {
					UtilMethods.messageBox(SWT.ICON_WARNING,
							"LogOut",
							"Unable to log out at this time.")
							.open();
				}
			//
			}
		} else if (event.getCommand().getId().equals("tia.voucher.report")) {
			wbp.showView("com.tia.account.journalVoucher");
		}
		else if (event.getCommand().getId().equals("tia.acc.report")) {
			wbp.showView("tia.Inven.Acc.Report");
		}
		
		} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		System.out.println("From command ..."+event.getCommand().getId());
		//HandlerUtil.getActiveWorkbenchWindow(event).close();
		return null;
	}
/*	HallBookingMgmtView hms = new HallBookingMgmtView(
			PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getShell(),2,"2011-07-25","13:00:00" );
	hms.open();
*/
}
