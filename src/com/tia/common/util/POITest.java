package com.tia.common.util;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;



/**
 * @author suresh
 * @supervisor loojah bajracharya
 * @for Magnus Consulting Grp.
 * 
 *      This is a utility class to handle Data Exporting to Excel sheet from
 *      Grid/table of GUI as data-sources. <br>
 *      For further assistance visit our official site :<br>
 * @see <a href="http://www.magnus.com.np">Magnus Consulting Grp.</a> For
 *      assistance on further development of this Utility visit us at:<br>
 * @see <a href="http://www.hamroblog-sristi.blogspot.com" >Hamro-Blog</a>
 * 
 * @revisedby bishal,suresh
 * 
 */
public class POITest {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {

	}// main() ends

	/**
	 * @param object
	 * @param targetFileName
	 * @param title
	 * @param subTitle
	 * @param frozenCol
	 * @param frozenRow
	 * @return boolean
	 * 
	 *         Here, first @param of object type is actually an explicitly
	 *         type-casted instance of the GUI data-presentation control (which
	 *         we'll call List Data Source, onwards). Here, for our context we
	 *         have considered that this object should hold either a
	 *         org.eclipse.nebula.widgets.grid.Grid or a
	 *         org.eclipse.swt.widgets.Table type of GUI controls in the
	 *         initialized(data-filled) state.
	 * 
	 *         Significance of using such an Object type of @param is that the
	 *         burden to write two methods doing functionally identical tasks
	 *         differing only in their
	 * @params (i.e. either Table or Grid as @params) is solved. Second @param
	 *         targetURL is the location in file system. eg. C:
	 */
	public static boolean exportToExcel(Object object, String targetFileName,
			String title, String subTitle, int frozenCol, int frozenRow) {
		// boolean variable to check if the List data source is a Table or a
		// Grid
		boolean isTable = false;
		System.out.println("\nkkkk... this Object is a "
				+ object.getClass().getName() + "!\n");
		if (object.getClass().getName()
				.equals("org.eclipse.nebula.widgets.grid.Grid")) {
			// initialize isTable to false if List Data source is nebula Grid
			isTable = false;
			System.out
					.println("\nkkkk..checking data source.... this is a Grid not table !\n");
			System.out.println("\nzzkk.. this "
					+ object
					+ " has columnGroups ="
					+ ((org.eclipse.nebula.widgets.grid.Grid) object)
							.getColumnGroupCount());

		}
		if (object.getClass().getName().equals("org.eclipse.swt.widgets.Table")) {
			// initialize isTable to true if List Data source is SWT Table
			isTable = true;
			// org.eclipse.swt.widgets.Table
			// tmpTable=(org.eclipse.swt.widgets.Table)object;//it doesn't work
			// however
			System.out
					.println("\nkkkk..checking data source... this is a Table not grid !\n");
		}
		// initialize counter for those columns which are visible in GUI of List
		// Data Source

		// Declare a list holding an arraylist of Array of Strings
		// NOTE: this list in fact holds the target data to be written to excel
		// file

		// initialize counter for those columns which are visible in GUI of List
		// Data Source
		int j = 0;
		int colNo = (isTable ? ((org.eclipse.swt.widgets.Table) object)
				.getColumnCount()
				: ((org.eclipse.nebula.widgets.grid.Grid) object)
						.getColumnCount());
		int rowNo = (isTable ? ((org.eclipse.swt.widgets.Table) object)
				.getItemCount()
				: ((org.eclipse.nebula.widgets.grid.Grid) object)
						.getItemCount());
		// Now, calculate total columns in target file
		for (int i = 0; i < colNo; i++) {
			if ((isTable ? ((org.eclipse.swt.widgets.Table) object)
					.getColumn(i).getWidth() > 0
					: ((org.eclipse.nebula.widgets.grid.Grid) object)
							.getColumn(i).getWidth() > 0)) {
				j++;
			}
		}
		String[] colArray = new String[j];
		if (isTable) {
			// LOC to dynamically initialize this array of column headers
			int k = 0;
			for (int i = 0; i < colNo; i++) {
				if ((isTable ? ((org.eclipse.swt.widgets.Table) object)
						.getColumn(i).getWidth() > 0
						: ((org.eclipse.nebula.widgets.grid.Grid) object)
								.getColumn(i).getWidth() > 0)) {
					colArray[k] = (isTable ? ((org.eclipse.swt.widgets.Table) object)
							.getColumn(i).getText()
							: ((org.eclipse.nebula.widgets.grid.Grid) object)
									.getColumn(i).getText());
					// cols[i].getText();
					k++;
				}
				System.out
						.println("zzz....in original table the column with col no. "
								+ i
								+ " has name ="
								+ (isTable ? ((org.eclipse.swt.widgets.Table) object)
										.getColumn(i).getText()
										: ((org.eclipse.nebula.widgets.grid.Grid) object)
												.getColumn(i).getText()));
			}// for ends
		}

		List<String[]> data = new ArrayList<String[]>();// Arrays.asList(colList);
		for (int rows = 0; rows < rowNo; rows++) {
			/*
			 * Declare an array(say row of target excel table sheet) to hold the
			 * data of entire a single row of GUI list Data Source
			 */
			String[] tmpRow = new String[j];
			int m = 0;
			for (int l = 0; l < colNo; l++) { // LOC to initialize the row-Array
				if ((isTable ? ((org.eclipse.swt.widgets.Table) object)
						.getColumn(l).getWidth() > 0
						: ((org.eclipse.nebula.widgets.grid.Grid) object)
								.getColumn(l).getWidth() > 0)) {
					tmpRow[m] = (isTable ? ((org.eclipse.swt.widgets.Table) object)
							.getItem(rows).getText(l)
							: ((org.eclipse.nebula.widgets.grid.Grid) object)
									.getItem(rows).getText(l));
					m++;
				}

			}// inner for ends
				// Now, add the row-Array to the list as list-item
			data.add(tmpRow);
		}// for ends
			// LOC to create excel file and write it to file system
		try {
			org.eclipse.nebula.widgets.grid.GridColumn[] gcAll = new org.eclipse.nebula.widgets.grid.GridColumn[] {};
			org.eclipse.nebula.widgets.grid.GridColumn[] gcNoCG = new org.eclipse.nebula.widgets.grid.GridColumn[] {};
			org.eclipse.nebula.widgets.grid.GridColumnGroup[] gcgAll = new org.eclipse.nebula.widgets.grid.GridColumnGroup[] {};
			if (!isTable) {
				gcAll = eligibleGridCols(((org.eclipse.nebula.widgets.grid.Grid) object)
						.getColumns());
				gcNoCG = eligibleGridColsNoCG(((org.eclipse.nebula.widgets.grid.Grid) object)
						.getColumns());
				gcgAll = ((org.eclipse.nebula.widgets.grid.Grid) object)
						.getColumnGroups();
			}
			// Define a xl WorkBook to be built into the target file
			Workbook hwb = new SXSSFWorkbook();
			// And a xl sheet to work into
			SXSSFSheet sheetFirst = (SXSSFSheet) hwb
					.createSheet(targetFileName);
			sheetFirst.setColumnWidth(31, 200);
			// set the Freeze-pane locations
			sheetFirst.createFreezePane(frozenCol, frozenRow);

			// Define a font of type org.apache.poi.ss.usermodel.Font
			Font font = hwb.createFont();
			Font fontCG = hwb.createFont();
			Font fontCell = hwb.createFont();
			// make this font Bold
			font.setBoldweight(Font.BOLDWEIGHT_BOLD);
			font.setFontHeightInPoints((short) 14);
			
			fontCG.setBoldweight(Font.BOLDWEIGHT_BOLD);
			fontCG.setFontHeightInPoints((short) 12);

			// define a CellStyle
			CellStyle cssMergedHeader = hwb.createCellStyle();
			CellStyle cssMergedHeaderCG = hwb.createCellStyle();
			cssMergedHeaderCG.setFont(fontCG);
			// set the cell value's default alignment to center
			cssMergedHeaderCG.setAlignment((short) 2);
			// set the font of cell style
			cssMergedHeader.setFont(font);
			// Alignment to the center of cell
			cssMergedHeader.setAlignment((short) 2);
			Row rowheadCG = sheetFirst.createRow((gcgAll.length == 0 ? 0 : 1));
			// title row
			Row titleRow = sheetFirst.createRow(0);
			titleRow.setHeightInPoints(28);
			Cell titleCell = titleRow.createCell(0);
			titleCell.setCellValue(title
					+ (subTitle.equals("") ? "" : "  " + subTitle + " "));
			titleCell.setCellStyle(cssMergedHeader);
			sheetFirst
					.addMergedRegion(new CellRangeAddress(0, (short) 0, 0,
							(short) (isTable ? colArray.length - 1
									: gcAll.length - 1)));
			// Now, create the fonts available in Excel
			Font fontHeader = hwb.createFont();
			fontHeader.setBoldweight(Font.BOLDWEIGHT_BOLD);
			// set the default color of font to be blue color
			fontHeader.setColor((short) 0xc);// setColor((short)Font.COLOR_RED);
			fontHeader.setFontHeightInPoints((short) 10);
			// define a CellStyle
			CellStyle css = hwb.createCellStyle();
			css.setFont(fontHeader);
			// set the default alignment of cell-value to Center
			css.setAlignment((short) 2);

			/*
			 * Create a row for the work sheet i.e. in particular we are
			 * creating the title row of the xl sheet, i.e. row no. 0 in work
			 * sheet.so, we name it to be rowhead as follows :
			 */
			Row rowhead = sheetFirst.createRow((gcgAll.length == 0 ? 1 : 2));
			/*
			 * Now, set these column names to respective columns in the work
			 * sheet as follows: LOC to handle rendering Columns of Table
			 */
			if (isTable) {
				for (int ii = 0; ii < colArray.length; ii++) {
					rowhead.createCell(ii).setCellValue(colArray[ii]);
					rowhead.getCell(ii).setCellStyle(css);
				}
			}
			// LOC to handle rendering ColumnGroups and Columns of Grid
			if (!isTable) {
				for (int ii = 0; ii < gcAll.length; ii++) {
					rowhead.createCell(ii).setCellValue(gcAll[ii].getText());
					rowhead.getCell(ii).setCellStyle(css);
				}

				List<String[]> listCG = new ArrayList<String[]>();
				List<String[]> listNoCG = new ArrayList<String[]>();
				int noCG = 0;
				int cgcCount = 0;
				for (int m = 0; m < gcAll.length;) {
					if (gcAll[m].getColumnGroup() == null) {
						// LOC to load names all GridColumns having no
						// ColumnGroups
						if (gcAll[m].equals(gcNoCG[noCG])) {
							String[] listCGItem = new String[3];
							listCGItem[0] = String.valueOf(m);
							listCGItem[1] = String.valueOf(m);
							listCGItem[2] = String.valueOf(gcAll[m].getText()
									.trim());
							noCG++;
							m++;
							listNoCG.add(listCGItem);
						}// end of innermost if
					}// end of if
					else {
						// LOC to load names of all GridColumns having
						// ColumnGroups
						org.eclipse.nebula.widgets.grid.GridColumn[] gcTmp = ((org.eclipse.nebula.widgets.grid.Grid) object)
								.getColumnGroup(cgcCount).getColumns();
						String[] targetGC = eligibleCols(gcTmp);
						for (int l = 0; l < gcAll.length; l++) {

							if (gcAll[m].getText().equals(targetGC[0])) {
								String[] listCGItem = new String[3];
								listCGItem[0] = String.valueOf(m);// cgcCount>0?nextIndexToStart:
								listCGItem[1] = String.valueOf(m
										+ targetGC.length - 1);// (cgcCount>0?nextIndexToStart+targetGC.length-1:m+targetGC.length-1)
								listCGItem[2] = String.valueOf(gcgAll[cgcCount]
										.getText().trim());
								listCG.add(listCGItem);
								cgcCount++;
								m = m + targetGC.length;
								break;
							}// if ends
						}// for ends
					}// end of else
				}// end of for
				noCG = 0;
				cgcCount = 0;
				for (int m = 0; m < gcAll.length; m++) {
					if (gcAll[m].getColumnGroup() == null) {
						// LOC to handle rendering of column headings having no
						// ColumnGroup as their containers
						if (gcAll[m].equals(gcNoCG[noCG])) {
							sheetFirst.addMergedRegion(new CellRangeAddress(1,
									(short) 2, m, m));
							Cell headerCGCell = rowheadCG.createCell(m);
							headerCGCell.setCellStyle(css);
							headerCGCell.setCellValue(Arrays.asList(
									listNoCG.get(noCG)).get(2));
							noCG++;
						}// innermost if() ends
					}// if() ends
					else {
						// LOC to handle rendering of columns headings having
						// ColumnGroups as their containers
						for (; cgcCount < gcgAll.length;) {
							org.eclipse.nebula.widgets.grid.GridColumn[] gcTmp = ((org.eclipse.nebula.widgets.grid.Grid) object)
									.getColumnGroup(cgcCount).getColumns();
							String[] targetGC = eligibleCols(gcTmp);
							if (gcAll[m].getText().equals(targetGC[0])) {
								int startAt = Integer.valueOf(Arrays.asList(
										listCG.get(cgcCount)).get(0));
								int endAt = Integer.valueOf(Arrays.asList(
										listCG.get(cgcCount)).get(1));
								sheetFirst
										.addMergedRegion(new CellRangeAddress(
												1, 1, (short) startAt,
												(short) endAt));

								Cell headerCGCell = rowheadCG
										.createCell((short) startAt);
								headerCGCell.setCellStyle(cssMergedHeaderCG);
								headerCGCell.setCellValue(Arrays.asList(
										listCG.get(cgcCount)).get(2));

								cgcCount++;
								m = endAt;
								break;
							}// inner if() ends
							break;
						}// for() ends
					}// else ends
				}// for ends
			}// if(isTable) ends

			// Define another cell style to address styling of Data to be
			// rendered into the data-cells
			CellStyle cs = hwb.createCellStyle();
			CellStyle csText = hwb.createCellStyle();
			CellStyle csInt = hwb.createCellStyle();
			cs.setFont(fontCell);
			csText.setFont(fontCell);
			csInt.setFont(fontCell);
			// Define a data format to manipulate presentation of data in cells
			DataFormat format = hwb.createDataFormat();
			// set the data format of data cells as defined
			cs.setDataFormat(format.getFormat("##,###.00"));
			csInt.setDataFormat(format.getFormat("#####"));

			// Data filling process into work-sheet data-cells
			int i;
			i = (gcgAll.length == 0 ? 2 : 3);
			for (int rows = 0; rows < data.size(); rows++) {
				Row row = sheetFirst.createRow(i);
				for (int m = 0; m < (isTable ? colArray.length : gcAll.length); m++) {
					if (Arrays.asList(data.get(rows)).get(m)
							.replaceAll(",", "").replaceAll("-", "")
							.matches("(\\d){1,23}\\.(\\d){1,23}")) {
						row.createCell(m).setCellValue(
								(Double.valueOf(Arrays.asList(data.get(rows))
										.get(m).replaceAll(",", ""))));
						// format the data in this cell
						row.getCell(m).setCellStyle(cs);
					} else if (Arrays.asList(data.get(rows)).get(m)
							.matches("(\\d){1,22}")) {
						/*String value=Arrays.asList(data.get(rows))
								.get(m).replaceAll(",", "");
						if(Double.valueOf(value)<=2147483647.00)
						{*/
						row.createCell(m).setCellValue(Double.valueOf(Arrays.asList(data.get(rows))
								.get(m).replaceAll(",", "")));
						/*}
						else
						{
							row.createCell(m).setCellValue(Double.valueOf(value));
						}*/
						row.getCell(m).setCellStyle(csInt);
					} else {
						row.createCell(m).setCellValue(
								(Arrays.asList(data.get(rows)).get(m)));
						row.getCell(m).setCellStyle(csText);
					}
				}// inner for ends
					// increment the value of i to point to next row in target
					// XL work-sheet
				i++;
			}// for ends
				// auto-size the column headers as per the size of text on them
			for (int cols = 0; cols < (isTable ? colArray.length : gcAll.length); cols++) {
				sheetFirst.autoSizeColumn(cols, true);
			}
			// Now, write the output stream contents as java file I/O operation
			// into browsed location
			Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getShell();
			FileDialog dialog = new FileDialog(shell, SWT.SAVE);
			dialog.setFilterExtensions(new String[] { "*.xlsx", "*.xls" });
			dialog.setFileName(targetFileName);
			targetFileName = dialog.open();
			FileOutputStream fileOut = new FileOutputStream(targetFileName);
			// And write the stream-data into workbook
			hwb.write(fileOut);
			// close the File I/O stream
			fileOut.close();
			System.out.println("Your excel file has been generated!");
			return true;

		} // try ends
		catch (Exception ex) {
			System.out.println("zzz... the Exception is : " + ex);
			return false;
		}

	}// end of method exportToExcel()

	/**
	 * @param gcTmp
	 * @return number of total eligible columns in a grid
	 */
	private static int findEligibleCols(
			org.eclipse.nebula.widgets.grid.GridColumn[] gcTmp) {
		int colCount = 0;
		for (int y = 0; y < gcTmp.length; y++) {
			if (gcTmp[y].getWidth() > 0) {
				colCount++;
			}
		}
		return colCount;
	}// end of method findEligibleCols()

	/**
	 * @param gcTmp
	 * @return number of eligible columns having no parent ColumnGroups
	 */
	private static int findEligibleColsNoCG(
			org.eclipse.nebula.widgets.grid.GridColumn[] gcTmp) {
		int colCount = 0;
		for (int y = 0; y < gcTmp.length; y++) {
			if (gcTmp[y].getWidth() > 0 && gcTmp[y].getColumnGroup() == null) {
				colCount++;
			}
		}
		return colCount;
	}// end of method findEligibleColsNoCG()

	/**
	 * @param gcTmp
	 * @return string array of name of all eligible gridcolumns
	 */
	private static String[] eligibleCols(
			org.eclipse.nebula.widgets.grid.GridColumn[] gcTmp) {
		String[] eCols = new String[findEligibleCols(gcTmp)];
		int targetIndex = 0;
		for (int i = 0; i < gcTmp.length; i++) {
			if (gcTmp[i].getWidth() > 0) {
				eCols[targetIndex] = gcTmp[i].getText();
				targetIndex++;
			}
		}

		return eCols;
	}// end of method eligibleCols()

	/**
	 * @param gcTmp
	 * @return all GridColumns being visually rendered in GUI
	 */
	private static org.eclipse.nebula.widgets.grid.GridColumn[] eligibleGridCols(
			org.eclipse.nebula.widgets.grid.GridColumn[] gcTmp) {
		org.eclipse.nebula.widgets.grid.GridColumn[] eCols = new org.eclipse.nebula.widgets.grid.GridColumn[findEligibleCols(gcTmp)];
		int targetIndex = 0;
		for (int i = 0; i < gcTmp.length; i++) {
			if (gcTmp[i].getWidth() > 0) {
				eCols[targetIndex] = gcTmp[i];
				targetIndex++;
			}
		}

		return eCols;
	}// end of method eligibleGridCols()

	/**
	 * @param gcTmp
	 * @return array of GridColumn having GUI width=0 and no ColumnGroup as its
	 *         container
	 */
	private static org.eclipse.nebula.widgets.grid.GridColumn[] eligibleGridColsNoCG(
			org.eclipse.nebula.widgets.grid.GridColumn[] gcTmp) {
		org.eclipse.nebula.widgets.grid.GridColumn[] eCols = new org.eclipse.nebula.widgets.grid.GridColumn[findEligibleColsNoCG(gcTmp)];
		int targetIndex = 0;
		for (int i = 0; i < gcTmp.length; i++) {
			if (gcTmp[i].getWidth() > 0 && gcTmp[i].getColumnGroup() == null) {
				eCols[targetIndex] = gcTmp[i];
				targetIndex++;
			}
		}

		return eCols;
	}// end of method eligibleGridColsNoCG()
}// class ends

