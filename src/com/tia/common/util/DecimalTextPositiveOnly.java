package com.tia.common.util;


import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

/**
 * This class is copied  pasted and modified Utility to validate Decimal input to a text
 * that takes only +ve decimal inputs.More over, this guarantees the input decimal text 
 * will allow single period with editing facility.
 * 
 */
public class DecimalTextPositiveOnly implements Listener{
	final static String badchars = "`~!@#$%^&*()_+-[]{}=\\|\"':;?/><, ";
	StringBuilder value;
	int countChar=0;
	
	Text inputText;
	
	/**
	 * @param varText
	 * @param length
	 * 
	 * This is explicit constructor method of this Class.
	 */
	public DecimalTextPositiveOnly(Text varText,int length){
		this.inputText=varText;
		
		this.inputText.setTextLimit(length);
		value=new StringBuilder();
		
		
	}
	public DecimalTextPositiveOnly(Text varText){
		this.inputText=varText;
		
		//this.inputText.setTextLimit(length);
		value=new StringBuilder();
		
		
	}
	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Listener#handleEvent(org.eclipse.swt.widgets.Event)
	 * 
	 * This is implementation code of method present in Listener interface.
	 *  
	 */
	@Override
	public void handleEvent(Event event)
	{
		if(Character.isLetter(event.character) || badchars.indexOf(event.character)>-1){
			event.doit=false;
			return;			
		}
		if(event.character=='.')
		{
			String tmp=inputText.getText();
			
//				{
			if(tmp.contains("."))
			{
				countChar=countPeriod(inputText);			
				
			}
			else
			{
			countChar=0;
				
			}
		
			countChar++;
			if(countChar>1)
			{
				//if(inputText.getText().matches("^(\\d*\\.\\d*)$"))				
				event.doit=false;return;				
			}
			if(countChar<=1)
			{
				event.doit=true;return;
			}
			else
				event.doit=true;return;
		}
	}
	/**
	 * @param srcText
	 * @return countChar
	 * 
	 * This method takes an initialized Text and counts total no. of
	 * period(.)s present on the string of this Text.
	 */
	private int countPeriod(Text srcText)
	{
		char[] charArray=srcText.getText().toCharArray();
		int countChar=0;
		for(int i=0;i<charArray.length;i++)
		{
			if(charArray[i]=='.')
			{
				countChar++;
			}
		}
		return countChar;
	}

}
