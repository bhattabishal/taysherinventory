//created by bishal for fun on 2012-02-06
package com.tia.common.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

public class BeforeSaveMessages {
	Shell sh = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	String compErrors[] = new String[50];
	String msgErrors[] = new String[50];
	private List<String[]> confirmMessages = new ArrayList<String[]>();
	private static int returnConformationValue = 0;
	int i = 0;
	int j = 0;
	//String[] strButtonConfirm = {"ok",
			//"cancel" };
	
	//String[] strButtonOk = { "ok"};

	public BeforeSaveMessages(Shell shell) {
		//sh = shell;
		sh=PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		MessageBox mb1 = new MessageBox(sh, SWT.ERROR);
		mb1.setText("Required");
	}

	public BeforeSaveMessages() {
		this(PlatformUI.getWorkbench().getDisplay().getActiveShell());

	}

	public void setMandatoryMessages(String txt) {
		compErrors[i] = txt;
		i++;
	}

	public void setVoluntaryMessages(String txt) {
		msgErrors[j] = txt;
		j++;
	}

	public void setConformationMessages(String lbl, String value) {
		String[] singleConformation = new String[2];
		singleConformation[0] = lbl;
		singleConformation[1] = value;
		confirmMessages.add(singleConformation);
	}

	public boolean returnMessages() {
		// for prompting messages
		boolean check = true;
		String strCompError = "";
		String strMsgError = "";
		for (int k = 0; k < compErrors.length; k++) {
			if (compErrors[k] != null) {
				strCompError += compErrors[k] + "\n";
			}
		}
		for (int l = 0; l < msgErrors.length; l++) {
			if (msgErrors[l] != null) {
				strMsgError += msgErrors[l] + "\n";
			}
		}

		if (StringUtils.isBlank(strCompError)) {
			System.out.println("comp error is blank");
			// isSave=true;
			// now check for non compulsary fields
			if (StringUtils.isBlank(strMsgError)) {
				check = true;
			} else {
				 MessageBox mb = new MessageBox(sh, SWT.YES | SWT.NO);
				 mb.setText("Confirm");
				 mb.setMessage("Following Fields Are Empty.Do You Want To Continue Anyway?\n\n"
				 + strMsgError);
				 int response = mb.open();
				 if (response == SWT.YES) {
				 check = true;
				 }
//				if (UtilMethods
//						.messageBox(
//								SWT.ICON_QUESTION,
//								"Confirm",
//								"FollowingFieldsAreEmpty.DoYouWantToContinueAnyway?\n\n"
//										+ strMsgError, strButtonConfirm).open() == 0) 
//				{
//					check = true;
//
//				} 
				else {
					check = false;
				}
			}

		} else {
			check = false;
			 MessageBox mb = new MessageBox(sh, SWT.ERROR);
			 mb.setText("Required");
			 mb.setMessage(strCompError);
			 mb.open();
			//this.showIndividualMessage(strCompError, 1);
			//UtilMethods.messageBox(SWT.ERROR,"Required",
					//strCompError, strButtonConfirm).open();
		}
		// clear the array
		compErrors = new String[50];
		msgErrors = new String[50];
		i = 0;
		j = 0;

		return check;
	}

	/*
	 * @returns true or false
	 * 
	 * @param
	 * 
	 * types: 0-information 1-error 2-confirm 3-warning
	 */
	public boolean showIndividualMessage(String msg, int type) {
		boolean value = true;

		 MessageBox mb = null;

		switch (type) {
		case 0:
			 mb = new MessageBox(sh, SWT.YES | SWT.ICON_INFORMATION);
			 mb.setMessage(msg);
			 mb.setText("Information");
			 mb.open();
			//UtilMethods.messageBox(SWT.ICON_INFORMATION,
					//"Information", msg, strButtonOk).open();
			break;
		case 1:
			 mb = new MessageBox(sh, SWT.NO | SWT.ICON_ERROR);
			 mb.setMessage(msg);
			 mb.setText("Error");
			 mb.open();
//			UtilMethods.messageBox(SWT.ICON_ERROR,"Error",
//					msg, strButtonOk).open();
			break;
		case 2:
			 mb = new MessageBox(sh, SWT.YES | SWT.NO | SWT.ICON_QUESTION);
			 mb.setMessage(msg);
			 mb.setText("Confirm");
			 int response = mb.open();
			 if (response == SWT.NO) {
			 value = false;
			 }
//			if (UtilMethods.messageBox(SWT.ICON_QUESTION,
//					"Confirm", msg, strButtonConfirm).open() != 0) {
//				value = false;
//			}
			break;
		case 3:
			 mb = new MessageBox(sh, SWT.YES | SWT.ICON_WARNING);
			 mb.setMessage(msg);
			 mb.setText("Alert");
			 mb.open();
			//UtilMethods.messageBox(SWT.ICON_WARNING,"Alert",
					//msg, strButtonOk).open();
			break;
		}
		return value;
	}

	
	

	

}
