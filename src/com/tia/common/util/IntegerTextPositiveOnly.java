package com.tia.common.util;

import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

/* This is just copied and pasted validation class
 * to validate only +ve integer of given length.
 * 
 * This class was added during SKBBL field-development 
 * on Feb-05-2012 at SKBBL HQ --by suresh.
 * */
public class IntegerTextPositiveOnly implements Listener {
	Text comonetText;
	final static String badchars = "`~!@#$%^&*()_-+{}[]=\\|\"':;?/><,. ";
	private int maxLetters;
	
	public IntegerTextPositiveOnly(Text component,int length){
		this.comonetText=component;
		this.comonetText.setTextLimit(length);
		maxLetters=length;
		
	}
	@Override
	public void handleEvent(Event event) {
		int len=0;
		if(this.comonetText.getText()!=null)
			len=this.comonetText.getText().length();
		if(len==maxLetters && (event.character!=8) && event.character!=127){
			event.doit=false;
			return;
		}
		if(Character.isLetter(event.character) || badchars.indexOf(event.character)>-1){
			event.doit=false;
			return;			
		}	
		event.doit=true;
		return;
		
		
		
	}
}
