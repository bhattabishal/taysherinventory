package com.tia.common.util;



//import org.eclipse.ui.internal.handlers.ReuseEditorTester;

public class CmbQry {

	private static String strLabel = "";
	private static String strId = "";

	public static String getLabel() {
		return strLabel;
	}

	public static String getId() {
		return strId;
	}
	public static String getBranchQuery() {
		strLabel = "br_name";
		strId = "br_id";
		return "SELECT br_id,br_name FROM branch_mcg";

	}

	public static String getCustomerList() {
		strLabel = "customer_name";
		strId = "customer_id";
		return "SELECT customer_id, customer_name FROM ntb_customer";
	}

	public static String getHallList() {
		strLabel = "hall_name";
		strId = "hall_id";
		return "SELECT hall_id, hall_name FROM ntb_hall";
	}

	public static String getBookingSchemes() {
		strLabel = "scheme_name";
		strId = "booking_type_id";
		return "SELECT booking_type_id, scheme_name FROM ntb_booking_type";
	}
	
	public static String getHallName(){
		strLabel ="hall_name";
		strId   ="hall_id";
		return "Select hall_id,hall_name from ntb_hall";
	}
	
	//for user
	public static String getRoleQuery() {
		strLabel = "role";
		strId = "role_id";
		return "SELECT role_id,role FROM user_role_mcg";

	}
	//end user
	/*
	 * Methods required by the Account module.
	 */
	public static String getMapAccCodeQuery(String mapFor) {
		strLabel = "acc_name";
		strId = "acc_code";
		String Strqry = "SELECT acc_name,acc_code FROM acc_head_mcg WHERE acc_head_id NOT IN"
				+ "(SELECT parent FROM acc_head_mcg) AND acc_code LIKE"
				+ " (SELECT acc_code FROM acc_fxn_mapping_mcg WHERE map_for='"
				+ mapFor + "')+'.%'";
		return Strqry;
	}
	public static String getCashBankMapAccCodeQuery() {
		strLabel = "acc_name";
		strId = "acc_code";
		String Strqry = "SELECT acc_name,acc_code FROM acc_head_mcg WHERE acc_head_id NOT IN"
				+ "(SELECT parent FROM acc_head_mcg) AND acc_code LIKE"
				+ " (SELECT acc_code FROM acc_fxn_mapping_mcg WHERE map_for in('Cash','Bank'))+'.%'";
		return Strqry;
	}
	public static String getAccountParent() {
		strLabel = "acc_name";
		strId = "acc_head_id";
		/*
		 * We can make any account head parent unless it has a sub-account head.
		 * That is if an account head's id is listed in acc_code column of
		 * acc_sub_head_mcg, then we filter that account head FROM the list of
		 * possible parent account heads.
		 */
		return "SELECT DISTINCT ah.acc_head_id, ah.acc_name "
				+ "FROM		    acc_head_mcg AS ah "
				+ "LEFT JOIN 	acc_sub_head_mcg AS sh "
				+ "ON 			ah.acc_code = sh.acc_code "
				+ "WHERE 		sh.acc_code IS NULL";
	}

	public static String getAccTypeQuery() {
		strLabel = "cv_lbl";
		strId = "cv_code";
		return "SELECT cv_code,cv_lbl FROM code_value_mcg WHERE cv_type = 'Account Type'";
	}
	public static String getFullName() {
		strLabel = "user_name";
		strId = "user_id";
		return "select  user_id, user_name from inven_user where status= '1'";
	}
	/**
	 * Filling the account head combo in the journal entry form.
	 * 
	 * @param accCode
	 * @return String (Query);
	 */
	public static String getLeafNode(String accCode) {
		strLabel = "acc_name";
		strId = "acc_code";
		return "SELECT acc_code, acc_name FROM acc_head_mcg WHERE acc_code = '"
				+ accCode + "'";
	}
	/**
	 * Filling the alias combo in the journal entry form.
	 * 
	 * @param accCode
	 * @return String (Query)
	 */
	public static String getAliasLeaves(String accCode,String jv_staus) {
		strLabel = "alias";
		strId = "acc_code";
		return "SELECT acc_code, alias FROM acc_head_mcg WHERE acc_code LIKE '"
				+ accCode
				+ ".%' AND acc_head_id NOT IN (SELECT DISTINCT(parent) FROM acc_head_mcg) "+jv_staus;
	}
	/**
	 * Filling the alias combo in the journal entry form.
	 * 
	 * @param accCode
	 * @return String (Query);
	 */
	public static String getAliasLeafNode(String accCode, String jv_status) {
		strLabel = "alias";
		strId = "acc_code";
		return "SELECT acc_code, alias FROM acc_head_mcg WHERE acc_code = '"
				+ accCode + "' "+jv_status;
	}
	// Saving Receipt
	public static String getSavReceipt() {
		strId = "receipt_no";
		strLabel = "receipt_no";
		return "SELECT receipt_no FROM cash_income_mcg order by receipt_no desc";
	}
	/**
	 * Fills the Account Alias bounded combo boz in the Ledger Details form
	 * based on the account type passed.
	 * 
	 * @param accType
	 *            (Account aliases belonging to a particular account type.)
	 * @return String (Query)
	 */
	public static String getAccountAliasByAccType(String accType) {
		strLabel = "alias";
		strId = "acc_head_id";

		// return "SELECT acc_code, alias FROM acc_head_mcg WHERE acc_type = '"
		// + accType + "' AND parent <> 0";

		return "SELECT acc_head_id, alias"
				+ " FROM acc_head_mcg WHERE acc_head_id NOT IN"
				+ "(SELECT parent FROM acc_head_mcg) AND acc_type='" + accType
				+ "'";
		// + "' ORDER BY 2 ASC";
	}
	/**
	 * Filling the Account Code combo box in Ledger Details form when one of the
	 * four account type is SELECTed.
	 * 
	 * @param accType
	 *            (Account type of the accounts to be loaded.)
	 * @return String (Query)
	 */
	public static String getAccCodes(String accType) {
		strLabel = "acc_code";
		strId = "acc_code";

		return "SELECT acc_code, acc_head_id FROM acc_head_mcg "
				+ "WHERE acc_head_id NOT IN (SELECT parent FROM acc_head_mcg)"
				+ "	AND acc_type='" + accType + "' ORDER BY 2 ASC";

	}
	/**
	 * Displays the list of all journal entries in the
	 * journal_voucher_details_mcg table except entries with -1.
	 * 
	 * @param null
	 * @return String (Query)
	 */
	public static String getAllJournalEntries() {
		strLabel = "jv_no";
		strId = "jv_no";

		return "SELECT DISTINCT jv_no FROM journal_voucher_mcg j WHERE j.status = 1 and jv_no<>-1 ORDER BY jv_no desc";
	}

	/**
	 * Gets all the account heads.
	 * 
	 * Usage: i) Combo box in Journal Reports View, Voucher Tab, Account Radio
	 * button selected.
	 * 
	 * ii) Combo box in Journal Reports View, Daily Voucher Tab, Individual
	 * Account Radio button selected.
	 * 
	 * @param null
	 * @return String (Query)
	 */
	public static String getAccHeads() {
		strLabel = "acc_name";
		strId = "acc_code";

		return "SELECT acc_name, acc_head_id, acc_code FROM acc_head_mcg WHERE parent != 0 "
				+ "ORDER BY acc_head_id ASC";
	}
	/**
	 * Gets the account heads. Usaage: i)Filling the Account Head Combo box in
	 * Ledger Details form when one of four account type is selected. ii)
	 * Filling the combo box when the Accounts radio button is selected in
	 * Journal Reports View's Voucher tab.
	 * 
	 * @param accType
	 *            (Account type of the accounts to be loaded.)
	 * @return String (Query)
	 */
	public static String getAccHeads(String accType) {
		strLabel = "acc_name";
		strId = "acc_head_id";

		return "SELECT acc_name, acc_head_id"
				+ " FROM acc_head_mcg WHERE acc_head_id NOT IN"
				+ "(SELECT parent FROM acc_head_mcg) AND acc_type='" + accType
				+ "' ORDER BY 2 ASC";
	}
	/**
	 * Filling the account head combo in the journal entry form.
	 * 
	 * @param accCode
	 * @return String (Query)
	 */
	public static String getLeaves(String accCode, String jv_status) {
		strLabel = "acc_name";
		strId = "acc_code";
		return "SELECT acc_code, acc_name FROM acc_head_mcg WHERE acc_code LIKE '"
				+ accCode
				+ ".%' AND acc_head_id NOT IN (SELECT DISTINCT(parent) FROM acc_head_mcg)"+jv_status;
	}

	/**
	 * Filling the sub-account head combo in the journal entry form.
	 * 
	 * @param accCode
	 * @return String (Query)
	 */
	public static String getSubHeads(String accCode) {
		strLabel = "sub_acc_name";
		strId = "sub_acc_code";
		return "SELECT sub_acc_code, sub_acc_name FROM acc_sub_head_mcg WHERE acc_code = '"
				+ accCode + "'";
	}
	/**
	 * Gets the account alias. Usage: i) For the Account Alias bounded combo box
	 * in the Journal Entry form. ii) For the Alias bounded combo in the Journal
	 * Reports View, Daily Voucher tab.
	 * 
	 * @param null
	 * @return String (Query)
	 */
	public static String getAccountAlias(String jv_status) {
		strLabel = "alias";
		strId = "acc_code";

		return "SELECT acc_code, alias FROM acc_head_mcg WHERE "+jv_status+"  parent <> 0";
	}

	public static String getAccountCode() {
		strLabel = "acc_name";
		strId = "acc_code";
		return "SELECT acc_code, acc_name FROM acc_head_mcg WHERE parent !=0";
	}
	public static String getAccountByAccountName() {
		strLabel = "acc_name";
		strId = "acc_code";
		return "SELECT acc_name, acc_code, acc_head_id FROM acc_head_mcg \n"
				+ "WHERE acc_head_id NOT IN (SELECT parent FROM acc_head_mcg) ORDER BY 1;";
	}

	/**
	 * Method that fetches the type of Nature for the sub account heads.
	 */
	public static String getNature() {
		strLabel = "cv_lbl";
		strId = "cv_code";

		return "SELECT cv_lbl, cv_code FROM code_value_mcg WHERE cv_type = 'Nature'";
	}
	
	public static String getStatus() {
		strLabel = "cv_lbl";
		strId = "cv_code";

		return "SELECT cv_lbl, cv_code FROM code_value_mcg WHERE cv_type = 'Status'";
	}
	public static String getStockType() {
		strLabel = "cv_lbl";
		strId = "cv_code";

		return "SELECT cv_lbl, cv_code FROM code_value_mcg WHERE cv_type = 'StockType'";
	}
	public static String getDepositeType() {
		strLabel = "cv_lbl";
		strId = "cv_id";

		return "SELECT cv_id, cv_lbl FROM code_value_mcg WHERE cv_type = 'cash' or cv_type = 'bank'";
	}
	public static String getOtherTrxnType() {
		strLabel = "cv_lbl";
		strId = "cv_id";

		return "SELECT cv_id, cv_lbl FROM code_value_mcg WHERE cv_type = 'other_trxn'";
	}
	public static String getStockBrand() {
		strLabel = "cv_lbl";
		strId = "cv_code";

		return "SELECT cv_lbl, cv_code FROM code_value_mcg WHERE cv_type = 'Item Brand'";
	}
	public static String getStockItems() {
		strLabel = "item_code";
		strId = "item_id";

		return "SELECT item_id, item_code FROM inven_item";
	}
	public static String getLocalBrand() {
		strLabel = "cv_lbl";
		strId = "cv_code";

		return "SELECT cv_lbl, cv_code FROM code_value_mcg WHERE cv_type = 'localbrand'";
	}
	public static String getCusSupType()
	{
		strLabel = "cv_lbl";
		strId = "cv_code";
		return "SELECT cv_lbl, cv_code FROM code_value_mcg WHERE cv_type = 'cusuptype'";
	}
//	public static String getAllExpenses()
//	{
//		strLabel = "name";
//		strId = "exp_id";
//		return "SELECT name, exp_id FROM inven_pur_expense";
//	}
	public static String getAllExpenses()
	{
		strLabel = "cv_lbl";
		strId = "cv_code";
		return "SELECT cv_lbl, cv_code FROM code_value_mcg WHERE cv_type = 'purexpense'";
	}
	public static String getAllCash()
	{
		strLabel = "cv_lbl";
		strId = "cv_id";
		return "SELECT cv_lbl, cv_id FROM code_value_mcg WHERE cv_type = 'cash'";
	}
	public static String getAllBank()
	{
		strLabel = "cv_lbl";
		strId = "cv_id";
		return "SELECT cv_lbl, cv_id FROM code_value_mcg WHERE cv_type = 'bank'";
	}
	
	//1-Customer
	//2-supplier
	//3-service provider
	public static String getAllSuppliers(int type)
	{
		strLabel = "name";
		strId = "customer_id";
		return "SELECT name, customer_id FROM inven_customer WHERE type = '"+type+"'";
	}
	
	public static String getAllServices()
	{
		strLabel = "service_name";
		strId = "service_id";
		return "SELECT service_name, service_id FROM inven_service";
	}
	
	public static String getServiceType()
	{
		
		strLabel = "cv_lbl";
		strId = "cv_code";
		return "SELECT cv_lbl, cv_code FROM code_value_mcg WHERE cv_type = 'service_type'";

	}
	
	public static String getStockCategory()
	{
		strLabel = "cv_lbl";
		strId = "cv_code";
		return "SELECT cv_lbl, cv_code FROM code_value_mcg WHERE cv_type = 'Item Type'";

		
	}
	public static String getOtherTrxn()
	{
		strLabel = "cv_lbl";
		strId = "cv_id";
		return "SELECT cv_lbl, cv_id FROM code_value_mcg WHERE cv_type = 'other_trxn'";

		
	}
	public static String getUnitType() {
		strLabel = "type_name";
		strId = "unit_type_id";

		return "SELECT unit_type_id, type_name FROM inven_unit_type";
	}
	public static String getBaseUnit() {
		strLabel = "cv_lbl";
		strId = "cv_code";

		return "SELECT cv_lbl, cv_code FROM code_value_mcg WHERE cv_type = 'base_unit'";
	}
	public static String getCommonBaseTypeUnits(String id) {
		strLabel = "unit_name";
		strId = "unit_id";

		return "SELECT ut2.unit_id, ut2.unit_name FROM dbo.inven_unit AS ut1 INNER JOIN dbo.inven_unit AS ut2 ON ut1.base_unit = ut2.base_unit WHERE  ut1.unit_id='"+id+"'";
	}
	public static String getBaseUnitByWholeId(int id)
	{
		strLabel = "type_base_unit";
		strId = "unit_type_id";
		return "SELECT iut.unit_type_id, iut.type_base_unit FROM dbo.inven_unit AS iu " +
				"INNER JOIN dbo.inven_unit_type AS iut ON " +
				"iu.unit_type = iut.unit_type_id WHERE iu.unit_id='"+id+"'";
	}
	
	public static String getUnit() {
		strLabel = "unit_name";
		strId = "unit_id";

		return "SELECT unit_id, unit_name FROM inven_unit WHERE status = '1'";
	}
	public static String getWholeUnitByBaseTypeId(int typeId) {
		strLabel = "unit_name";
		strId = "unit_id";

		return "SELECT unit_id, unit_name FROM inven_unit WHERE status = '1' AND unit_type='"+typeId+"'";
	}
	
	
}// end of class
