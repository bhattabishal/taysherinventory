package com.tia.common.util;

import java.awt.AWTEvent;
import java.awt.EventQueue;
import java.awt.ItemSelectable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.ComboBoxModel;
import javax.swing.event.EventListenerList;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

public class SwtBndCombo extends Combo {
	private String firstData = "Select";

	private ArrayList<Object> bndArray = new ArrayList<Object>();
	Connection cnn;
	protected Object selectedItemReminder = null;
	protected ComboBoxModel dataModel;
	protected EventListenerList listenerList = new EventListenerList();
	private boolean firingActionEvent = false;
	protected String actionCommand = "comboBoxChanged";

	public SwtBndCombo(Composite parent, int style) {
		super(parent, style);
	}

	public void add(String strLvl, String strBndVal) {
		super.add(strLvl);
		bndArray.add(strBndVal);
	}

	public boolean isEditable() {
		return isEditable();
	}

	public void setFirstIndex(String firstIndex) {
		this.firstData = firstIndex;
	}

	public void fillData(String strQuery, String ListCol, String BoundCol,
			Connection con) {
		this.fillData(strQuery, ListCol, BoundCol, con, true);
	}

	public void fillData(String strQuery, String ListCol, String BoundCol,
			Connection con, Boolean putSelect) {
		super.removeAll();
		this.bndArray.clear();
		if (putSelect)
			this.add(firstData, "0");

		try {

			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(strQuery);
			while (rs.next()) {
				this.add(rs.getString(ListCol), rs.getString(BoundCol));
			}
			if (getItemCount() > 0)
				this.select(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * public void fillData(ArrayList listResult, boolean putSelect) {
	 * super.removeAll(); this.bndArray.clear(); if (putSelect)
	 * this.add(firstData, "0"); ListIterator listAccCode =
	 * listResult.listIterator(); while (listAccCode.hasNext()) { AccCode a =
	 * (AccCode) listAccCode.next(); String acc_name = a.getAcc_name(); String
	 * acc_code = a.getAcc_code(); // System.out.println(a.toString());
	 * this.add(acc_code, acc_name); } if (getItemCount() > 0) this.select(0);
	 * 
	 * }
	 */

	/**
	 * This method is overridden to handle the subclass not allowed exceptions
	 */
	@Override
	protected void checkSubclass() {
	}

	/**
	 * 
	 * These are the new methods that 'decorate' the original Combo class. We
	 * have added methods that match the two methods Combo provides for finding
	 * elements in the Mapped Arraylist, except that our new methods access the
	 * bound value list instead of the item list.
	 * 
	 * I only use the getSelectedBoundValue method in this example. I provided
	 * the other because together the two make a complete match to the way the
	 * original class lets you query the original model.
	 */
	public Object getSelectedBoundValue() {
		return (bndArray.get(super.getSelectionIndex()));
	}

	/**
	 * @param anObject
	 */
	public void setSelectedBoundValue(Object anObject) {
		String strPara = anObject.toString();
		String strCmb = "";
		for (int i = 0; i < this.getItemCount(); i++) {
			strCmb = this.getBoundValueAt(i).toString();
			if (strPara.equals(strCmb)) {
				super.select(i);
				return;
			}
		}
	}

	protected void fireItemStateChanged(ItemEvent e) {
		// Guaranteed to return a non-null array
		Object[] listeners = listenerList.getListenerList();
		// Process the listeners last to first, notifying
		// those that are interested in this event
		for (int i = listeners.length - 2; i >= 0; i -= 2) {
			if (listeners[i] == ItemListener.class) {
				// Lazily create the event:
				// if (changeEvent == null)
				// changeEvent = new ChangeEvent(this);
				((ItemListener) listeners[i + 1]).itemStateChanged(e);
			}
		}
	}

	protected void selectedItemChanged() {
		if (selectedItemReminder != null) {
			fireItemStateChanged(new ItemEvent((ItemSelectable) this,
					ItemEvent.ITEM_STATE_CHANGED, selectedItemReminder,
					ItemEvent.DESELECTED));
		}
	}

	public String getActionCommand() {
		return actionCommand;
	}

	protected void fireActionEvent() {
		if (!firingActionEvent) {
			// Set flag to ensure that an infinite loop is not created
			firingActionEvent = true;
			ActionEvent e = null;
			// Guaranteed to return a non-null array
			Object[] listeners = listenerList.getListenerList();
			long mostRecentEventTime = EventQueue.getMostRecentEventTime();
			int modifiers = 0;
			AWTEvent currentEvent = EventQueue.getCurrentEvent();
			if (currentEvent instanceof InputEvent) {
				modifiers = ((InputEvent) currentEvent).getModifiers();
			} else if (currentEvent instanceof ActionEvent) {
				modifiers = ((ActionEvent) currentEvent).getModifiers();
			}
			// Process the listeners last to first, notifying
			// those that are interested in this event
			for (int i = listeners.length - 2; i >= 0; i -= 2) {
				if (listeners[i] == ActionListener.class) {
					// Lazily create the event:
					if (e == null)
						e = new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
								getActionCommand(), mostRecentEventTime,
								modifiers);
					((ActionListener) listeners[i + 1]).actionPerformed(e);
				}
			}
			firingActionEvent = false;
		}
	}

	public void setSelectedItem(Object anObject) {
		Object oldSelection = selectedItemReminder;
		Object objectToSelect = anObject;
		if (oldSelection == null || !oldSelection.equals(anObject)) {

			if (anObject != null && !isEditable()) {
				boolean found = false;
				for (int i = 0; i < dataModel.getSize(); i++) {
					Object element = dataModel.getElementAt(i);
					if (anObject.equals(element)) {
						found = true;
						objectToSelect = element;
						break;
					}
				}
				if (!found) {
					return;
				}
			}

			dataModel.setSelectedItem(objectToSelect);
			if (selectedItemReminder != dataModel.getSelectedItem()) {
				// in case a users implementation of ComboBoxModel
				// doesn't fire a ListDataEvent when the selection
				// changes.
				selectedItemChanged();
			}
		}
		fireActionEvent();
	}

	public Object getBoundValueAt(int index) {
		return (this.bndArray.get(index));
	}

	// public void removeAll(){
	// super.removeAll();
	// this.bndArray.retainAll(bndArray);
	// System.out.println("The item count in bound value is "+this.bndArray.size());
	// //this.bndArray=new ArrayList<Object>();
	//
	// }
}
