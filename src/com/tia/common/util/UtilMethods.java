package com.tia.common.util;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;



import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.tia.common.db.DbConnection;

public class UtilMethods {
	private static Shell sh;

	public UtilMethods() {
		
	}

	/**
	 * Wrapper function for creating a SWT message box. Parameters required to
	 * created a message box can be passed into this method and it will generate
	 * a message box object.
	 * 
	 * @param style
	 *            (Styles to be applied for the message box e.g. SWT.CENTER |
	 *            SWT. ERROR)
	 * @text (String input for displaying on the message box header.)
	 * @msg (String for displaying on the main message box body.)
	 */
	public static MessageBox messageBox(int style, String text, String msg) {
		sh = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		MessageBox mb;

		if (style < -1)
			mb = new MessageBox(sh);
		else
			mb = new MessageBox(sh, style);

		mb.setText(text);
		mb.setMessage(msg);

		return mb;

	}
	/**
	 * messageBox - small wrapper function to generate jface dialog. The
	 * advantage is - it has better customization in comparison to swt dialog. -
	 * it is introduced in our project for customizing the font of the dialog. -
	 * it is used to generate normal information message.
	 * 
	 * @param strTitle
	 * @param strMessage
	 * @param strButtonArray
	 * @return org.eclipse.jface.dialogs.MessageDialog
	 */
	public static MessageDialog messageBox(String strTitle, String strMessage,
			String[] strButtonArray) {
		return messageBox(MessageDialog.NONE, strTitle, strMessage,
				strButtonArray);
	}

	/**
	 * messageBox - small wrapper function to generate jface dialog. The
	 * advantage is - it has better customization in comparison to swt dialog. -
	 * it is introduced in our project for customizing the font of the dialog. -
	 * it is used to generate custom message. - the style parameter could be
	 * MessageDialog.NONE, MessageDialog.CONFIRM etc.
	 * 
	 * @param intStyle
	 * @param strTitle
	 * @param strMessage
	 * @param strButtonArray
	 * @return org.eclipse.jface.dialogs.MessageDialog
	 */
	public static MessageDialog messageBox(int intStyle, String strTitle,
			String strMessage, String[] strButtonArray) {
		Shell sh = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getShell();
		//sh.setFont(CommonFunctions.getFontLabel());
		// MessageDialog.openConfirm(sh, "Confirm", "Please confirm");
		return new MessageDialog(sh, strTitle, null, strMessage, intStyle,
				strButtonArray, 1);
	}

	public static MessageBox messageBoxDialog(int style, String text, String msg) {
		MessageBox mb;
		Display display = null;
		Shell sh = new Shell(display, SWT.NO_TRIM | SWT.ON_TOP);
		if (style < -1)
			mb = new MessageBox(sh);
		else
			mb = new MessageBox(sh, style);

		mb.setText(text);
		mb.setMessage(msg);

		return mb;
	}

	/**
	 * Gets the next maximum value for a integer type column of any given table.
	 * 
	 * @param TableName
	 *            (Name of the table.)
	 * @param ColumnName
	 *            (Name of the column.)
	 * @return int (New maximum integer value.)
	 */
	public static int getMaxId(String TableName, String ColumnName) {
		int id = 0;
		Connection cnn = DbConnection.getConnection();
		String strQry = "Select isnull(max(cast(" + ColumnName
				+ " as numeric(20,0))),0) as id from " + TableName;

		try {
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry);
			if (rs != null) {
				rs.next();
				if (rs.getString("id") != null
						&& !rs.getString("id").equals(""))
					id = Integer.parseInt(rs.getString("id").toString());
			}
		} catch (SQLException ex) {
			System.out.println(ex.toString());
		}

		id++;
		return id;
	}
	public static Image getLogoImage() {
		Image image = null;
		Connection cnn = DbConnection.getConnection();
		String strQry = "SELECT logo from office_info";
		try {
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry);
			if (rs != null) {
				rs.next();
				if (rs.getString("logo") != null
						&& !rs.getString("logo").equals(""))
				{
					Blob img=rs.getBlob("logo");
					image = convertByteToImage(
							convertBlobToByteArray(img), "jpg");

				}
					
			}
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}
		

		return image;
	}
	
	
	public static Image convertByteToImage(byte[] bytes, String ext)
	throws IOException {

ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
Iterator<?> readers;
if (!ext.equals("")) {
	readers = ImageIO.getImageReadersByFormatName(ext);
}

else {
	readers = ImageIO.getImageReadersByFormatName("jpg");
}

// Iterator<?> readers = ImageIO.getImageReadersByFormatName("jpg");
// ImageIO is a class containing static convenience methods for locating
// ImageReaders
// and ImageWriters, and performing simple encoding and decoding.

ImageReader reader = (ImageReader) readers.next();
Object source = bis; // File or InputStream, it seems file is OK

ImageInputStream iis;
iis = ImageIO.createImageInputStream(source);
// Returns an ImageInputStream that will take its input from the given
// Object

reader.setInput(iis, true);
ImageReadParam param = reader.getDefaultReadParam();

Image image = reader.read(0, param);
// got an image file

BufferedImage bufferedImage = new BufferedImage(image.getWidth(null),
		image.getHeight(null), BufferedImage.TYPE_INT_RGB);

return image;

}
	public static byte[] convertBlobToByteArray(Blob itStrm) {
		InputStream is;

		byte[] bytes = null;
		if (itStrm != null) {
			try {
				is = itStrm.getBinaryStream();
				bytes = new byte[(int) itStrm.length()];
				is.read(bytes);
				is.close();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return bytes;
	}
	
	public static Color getFontForegroundColorRed() {
		Color color = PlatformUI.getWorkbench().getDisplay()
				.getSystemColor(SWT.COLOR_RED);
		return color;
	}

	public static Color getFontForegroundColorBlack() {
		Color color = PlatformUI.getWorkbench().getDisplay()
				.getSystemColor(SWT.COLOR_BLACK);
		return color;
	}
	public static Font getFontBold() {
		String fontName = "Arial";
		Integer fontSize = 9;
		String fontStyle = "normal";
		Font Font1 = new Font(PlatformUI.getWorkbench().getDisplay(), fontName,
				fontSize, SWT.BOLD);
		return Font1;
	}
	public static Font getSmallFont() {
		// fontName = "Arial";
		String fontName = "Arial";
		Integer fontSize = 9;
		String fontStyle = "normal";
		Font Font1 = new Font(PlatformUI.getWorkbench().getDisplay(), fontName,
				fontSize, SWT.NORMAL);
		return Font1;
	}
	public static Color getFontBackgroundColor() {
		Color color = PlatformUI.getWorkbench().getDisplay()
				.getSystemColor(SWT.COLOR_RED);
		return color;
	}

}
