package com.tia.common.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;



import com.tia.common.db.DbConnection;

public class UtilFxn {
	/**
	 * @param query
	 * @return ResultSet
	 * @throws SQLException
	 *             This function is used for extracting the data by passing
	 *             query.
	 */
	public static ResultSet getData(String query) throws SQLException {
		Connection cons = DbConnection.getConnection();
		ResultSet rs = null;
		Statement st = cons.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_UPDATABLE);

		try {
			rs = st.executeQuery(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rs;
	}

	public static int modifyTable(String query, Connection cn, String parmeter)
			throws SQLException {

		Statement st = cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_UPDATABLE);

		try {
			if (st.executeUpdate(query) > 0) {
				return 1;

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	public static String getFiscalStartDate(int fiscalYearId) {
		String strQry = "Select start_date from fiscal_year_mcg where fy_id="
				+ fiscalYearId;
		try {
			System.out
					.println("zzz... the getFiscalStartDate method's QRY here = "
							+ strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			ResultSet rs = st.executeQuery(strQry);
			if (rs.next())
				return rs.getString("start_date");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String getFiscalEndDate(int fiscalYearId) {
		String strQry = "Select end_date from fiscal_year_mcg where fy_id="
				+ fiscalYearId;
		try {
			System.out
					.println("zzz... the getFiscalEndDate method's QRY here = "
							+ strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			ResultSet rs = st.executeQuery(strQry);
			if (rs.next())
				return rs.getString("end_date");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String GetMinValueFromTable(String TableName,
			String FieldName, String WhereCondition) {
		String output = "";
		String strQry;

		strQry = "SELECT min(" + FieldName + ") as " + FieldName + "  FROM "
				+ TableName + " WHERE " + WhereCondition;

		try {
			System.out
					.println("zzz... the GetMaxValueFromTable method's QRY here = "
							+ strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			ResultSet rs = st.executeQuery(strQry);

			/*
			 * if (rs != null) { rs.next(); if (rs.getString(FieldName) != null
			 * && !rs.getString(FieldName).equals("")) output =
			 * rs.getString(FieldName).toString();} }
			 */

			if (rs.next()) {
				if (rs.getString(FieldName) != null
						&& !rs.getString(FieldName).equals("")) {
					output = rs.getString(FieldName).toString();
				}
			}
		} catch (SQLException ex) {
			System.out.println(ex.toString());
			ex.printStackTrace();
		}

		return output;
	}

	public static String GetMaxValueFromTable(String TableName,
			String FieldName, String WhereCondition) {
		String output = "";
		String strQry;

		strQry = "SELECT max(" + FieldName + ") as " + FieldName + "  FROM "
				+ TableName + " WHERE " + WhereCondition;

		try {
			System.out
					.println("zzz... the GetMaxValueFromTable method's QRY here = "
							+ strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			ResultSet rs = st.executeQuery(strQry);

			/*
			 * if (rs != null) { rs.next(); if (rs.getString(FieldName) != null
			 * && !rs.getString(FieldName).equals("")) output =
			 * rs.getString(FieldName).toString();} }
			 */

			if (rs.next()) {
				if (rs.getString(FieldName) != null
						&& !rs.getString(FieldName).equals("")) {
					output = rs.getString(FieldName).toString();
				}
			}

		} catch (SQLException ex) {
			System.out.println(ex.toString());
			ex.printStackTrace();
		}

		return output;
	}
	

	public static String GetValueFromTable(String TableName, String FieldName,
			String WhereCondition) {
		String output = "";
		String strQry;

		strQry = "SELECT " + FieldName + " FROM " + TableName + " WHERE "
				+ WhereCondition;

		try {
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			ResultSet rs = st.executeQuery(strQry);

			/*
			 * if (rs != null) { rs.next(); if (rs.getString(FieldName) != null
			 * && !rs.getString(FieldName).equals("")) output =
			 * rs.getString(FieldName).toString();} }
			 */

			if (rs.next())

				if (rs.getString(FieldName) != null
						&& !rs.getString(FieldName).equals(""))
					output = rs.getString(FieldName).toString();

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return output;
	}

	public static String GetSumValueFromTable(String TableName,
			String FieldName, String WhereCondition) {
		String output = "";
		String strQry;

		strQry = "SELECT SUM( " + FieldName + ") AS " + FieldName + " FROM "
				+ TableName + "  WHERE " + WhereCondition;

		try {
			System.out
					.println("zzz... the GetSumValueFromTable method's QRY here = "
							+ strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry);
			if (rs != null) {
				rs.next();
				if (rs.getString(FieldName) != null
						&& !rs.getString(FieldName).equals("")) {
					output = rs.getString(FieldName).toString();
				}
			}
		} catch (SQLException ex) {
			System.out.println(ex.toString());
		}

		return output;
	}

	/**
	 * Returns the level for the tree node.
	 * 
	 * @param strTable
	 * @param strField
	 * @param strParent
	 * @param strCond
	 * @return level (depth level of the tree node)
	 */
	public static int getTVLevel(String strTable, String strField,
			String strParent, String strCond) {
		int level = 0;
		String strQry = "SELECT DISTINCT " + strParent + " FROM " + strTable
				+ (strCond.equals("") ? "" : " WHERE " + strCond);
		strCond = "";
		try {
			System.out.println("zzz... the getTVLevel method's QRY here = "
					+ strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry);
			while (rs.next()) {
				if (!strCond.equals("")) {
					strCond += ",";
				}
				strCond += rs.getString(strParent);
			}
			while (!strCond.equals("")) {
				level++;
				strQry = "SELECT DISTINCT " + strParent + " FROM " + strTable
						+ " WHERE " + strField + " in (" + strQry + ")";
				rs = st.executeQuery(strQry);
				strCond = "";
				while (rs.next()) {
					if (!strCond.equals("")) {
						strCond += ",";
					}
					strCond += rs.getString(strParent);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return level;
	}

	public static String GetCountValueFromTable(String TableName,
			String FieldName, String WhereCondition) {
		String output = "";
		String strQry;

		strQry = "SELECT COUNT( " + FieldName + ") AS " + FieldName + " FROM "
				+ TableName + "  WHERE " + WhereCondition;

		try {
			System.out
					.println("zzz... the GetSumValueFromTable method's QRY here = "
							+ strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry);
			if (rs != null) {
				rs.next();
				if (rs.getString(FieldName) != null
						&& !rs.getString(FieldName).equals("")) {
					output = rs.getString(FieldName).toString();
				}
			}
		} catch (SQLException ex) {
			System.out.println(ex.toString());
		}

		return output;
	}
	
	public static boolean  isNotUnique(String qry)
	{
		
		Connection cnn = DbConnection.getConnection();
		try {
			
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(qry);
			if(getRowCount(rs) > 0)
			{
				return true;
			}
			
		} catch (SQLException ex) {
			System.out.println(ex.toString());
		}
		
		return false;
	}

	public static int getCountValueFromTable(String TableName,
			String testValue, String WhereCondition) {
		int id = 0;
		Connection cnn = DbConnection.getConnection();
		String strQry = "SELECT  (case when(COUNT(*)=0) then 1 else (SELECT COUNT(*) AS children_count FROM acc_head_mcg WHERE acc_code LIKE '"
				+ testValue
				+ ".%')+1 end)"
				+ " AS CNT FROM "
				+ TableName
				+ " WHERE " + WhereCondition + "";

		System.out.println("The count finding query is " + strQry);
		try {
			System.out
					.println("zzz... the getMaxIdWherecondition method's QRY here = "
							+ strQry);
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry);
			if (rs != null) {
				rs.next();
				id = rs.getInt("CNT");
			}
		} catch (SQLException ex) {
			System.out.println(ex.toString());
		}
		return id;
	}

	@SuppressWarnings("deprecation")
	public static Time toDatabaseTime(String strTime) {
		Time time = Time.valueOf(strTime.substring(0, 5) + ":00");
		// System.out.println(strTime.substring(6, 8));
		if (strTime.substring(6, 8).equals("PM")) {
			if (!strTime.substring(0, 2).equals("12")) {
				int hrs = time.getHours() + 12;
				time.setHours(hrs);
			}
		}

		return time;

	}

	public static String GetValueFromTable(String strQry) {
		String output = "";
		try {
			System.out.print("zz... esp method's QRY= " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry);
			if (rs.next())

				if (rs.getString(1) != null && !rs.getString(1).equals(""))
					output = rs.getString(1).toString();

		} catch (SQLException ex) {
			System.out.println(ex.toString());
			ex.printStackTrace();
		}

		return output;
	}

	public static String getFormatedAmount(Double amt) {
		DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
		String rtnAmt = "";
		rtnAmt = decimalFormat.format(amt);
		return rtnAmt;
	}
	public static String getFormatedInt(Double amt) {
		DecimalFormat decimalFormat = new DecimalFormat("#,##0");
		String rtnAmt = "";
		rtnAmt = decimalFormat.format(amt);
		return rtnAmt;
	}
	
	public static String getDoubleExpFormated(Double amt)
	{
		DecimalFormat decimalFormat = new DecimalFormat("#0.00");
		String rtnAmt = "";
		rtnAmt = decimalFormat.format(amt);
		return rtnAmt;
	}
	public static String getDoubleExpFormated(String amt)
	{
		if(amt==null)
		{
			return "0";
		}
		if(amt.equals("0") || amt.equals(""))
		{
			return "0";
		}
		
		double amount=Double.parseDouble(amt.replaceAll(",", ""));
		
		DecimalFormat decimalFormat = new DecimalFormat("#0.00");
		String rtnAmt = "";
		rtnAmt = decimalFormat.format(amount);
		return rtnAmt;
	}

	public static String getFormatedDate(Date dt) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String rtnDate = "";
		rtnDate = sdf.format(dt);
		return rtnDate;
	}

	// counts the no of records and returns it from the given resultset
	public static int getRowCount(ResultSet rs) throws SQLException {
		int rowCount = 0;
		int currentRow = rs.getRow();
		rowCount = rs.last() ? rs.getRow() : 0;
		if (currentRow == 0) // If there was no current row
			rs.beforeFirst(); // We want next() to go to first row
		else
			// If there WAS a current row
			rs.absolute(currentRow); // Restore it
		return rowCount;
	}
}
