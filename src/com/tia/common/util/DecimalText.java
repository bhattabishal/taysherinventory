package com.tia.common.util;



import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

/**
 * This class ensure the Decimal text enter in text field.
 * @author loozah sir Created by Dinup Kandel.
 * 
 */
public class DecimalText implements Listener{
	Text comonetText;
	final static String badchars = "`~!@#$%^&*()_+=\\|\"':;?/><,";
	private int maxLetters;
	double maxVal;
	StringBuilder value;
	int count;
	
	public DecimalText(Text component,int length,Double maxValue){
		this.comonetText=component;
		this.comonetText.setTextLimit(length);
		maxLetters=length;
		maxVal=maxValue;
		value=new StringBuilder();
		count=-1;
		
		
	}
	@Override
	public void handleEvent(Event event) {
		int len=0;
		count=count+1;
		if(event.character==SWT.BS){
			event.doit=true;
			count=count-1;
			return;
		}
		if(this.comonetText.getText().trim()!=null)
			len=this.comonetText.getText().length();
		
		if(len==maxLetters && (event.character!=8) && event.character!=127){
			event.doit=false;
			return;
		}
				
		if(Character.isLetter(event.character) || badchars.indexOf(event.character)>-1){
			event.doit=false;
			return;			
		}

		System.out.println("The char= "+event.character);
		if((count!=0 && Character.isDigit(event.character))||event.character==SWT.BS){
			if(!(event.character==SWT.BS))value.append(event.character);
			else{
				count=value.length();
				//value.deleteCharAt(value.length()-1);
				event.doit=true;
				return;
			}
//			System.out.println("The char= "+event.character);
//			System.out.println("The Str Builder is "+value.toString());
//			System.out.println("The value is "+Double.parseDouble(value.toString()));
//			if(Double.parseDouble(value.toString())>maxVal)
//			{	
//				System.out.println("The length is "+value.length());
//				value.deleteCharAt(value.length()-1);
//				System.out.println("The length After delete is "+value.toString().length()+"\n");
//				event.doit=false;
//				return;
//			}
			
		}
		if(event.character=='.'){
			value.append(event.character);			
		}	
		event.doit=true;
		return;
		
		
		
	}

}
