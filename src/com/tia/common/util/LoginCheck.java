package com.tia.common.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.PlatformUI;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.db.DbConnection;




public class LoginCheck {
	public static boolean isLogIn() {
		boolean flag = false;
		String strQry = "SELECT count(log_in_info_id) as cnt FROM log_in_info_mcg WHERE log_in_info_id='"
				+ ApplicationWorkbenchWindowAdvisor.getLoginInfoId()
				+ "' AND log_out_time is NULL";
		System.out.println("Is Login Query- " + strQry);
		try {
			ResultSet rs = UtilFxn.getData(strQry);
			if (rs.next()) {
				flag = rs.getInt("cnt") == 1 ? true : false;
				if (!flag) {
					LogFileCreate.logFileCreater(
							"LoginCheck",
							"LoginCheck",
							"User already logout",
							"The user "
									+ ApplicationWorkbenchWindowAdvisor
											.getCurrentUser()
									+ " already is already logout.");
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
	public static void showMessage() {
		MessageBox msg = new MessageBox(Display.getDefault().getActiveShell(), SWT.OK | SWT.ICON_ERROR);
		msg.setText("Error");
		if (DbConnection.getConnection()==null)
			msg.setMessage("you are not connected to server.please try again later");
		else
			msg.setMessage("user is already logout.please log in again");
		msg.open();
		try
		{
			PlatformUI.getWorkbench().restart();
		}catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
}
