package com.tia.common.db;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

public class DbConnection {
	static String dbName;
	static Properties dbProperties;
	static Connection dbConnection;
	static boolean isConnected;
	static String strError;

	public static void closeConnection() {

		try {
			System.out.println("Closing connection..");
			dbConnection.close();
			isConnected = false;
		} catch (SQLException e) {
			System.out.println("The ex is closing connection " + e.toString());
			e.printStackTrace();
		}
	}

	private static boolean connect() {
		String dbUrl = getDatabaseUrl();
		try {
			System.out.println("Connecting... ");
			dbConnection = DriverManager.getConnection(dbUrl, dbProperties);
			isConnected = dbConnection != null;
		} catch (SQLException ex) {
			isConnected = false;
			setError("Error while making Connectiing DataBase :" + ex);
			System.out.println("Error in connection " + ex.toString());
		}
		return isConnected;
	}

	/**
	 * @return connection made to the database
	 */
	public static Connection getConnection() {
		setProperties();
		if (!isConnected) {
			for (int countConnection = 1; countConnection < 4; countConnection++) {
				if (connect()) {
					System.out.println("Connected... ");
					return dbConnection;
				}
				Shell shell = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell();
				if (!MessageDialog.openConfirm(shell, "Confirm",
						"Connection confirm.")) {
					System.exit(0);
				}
			}
			connect();
		}
		return dbConnection;
	}
	public static Connection getConnection2() {
		setProperties();
		Connection newConnection = null;
		String dbUrl = getDatabaseUrl();
		try {
			System.out.println("Connecting New... ");
			newConnection = DriverManager.getConnection(dbUrl, dbProperties);
//			status = newConnection != null;
		} catch (SQLException ex) {
//			status = false;
			setError("Error while making Connectiing DataBase :" + ex);
			System.out.println("Error in connection " + ex.toString());
		}

//		if (!isConnected) {
/*			for (int countConnection = 1; countConnection < 4; countConnection++) {
				if (connect2(newConnection)) {
					System.out.println("New Connected... ");
					return newConnection;
				}
				Shell shell = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell();
				if (!MessageDialog.openConfirm(shell,
						Messages.getTitle("confirm"),
						Messages.getMessage("connectionconfirm"))) {
					System.exit(0);
				}
			}
//			connect();
*/		//}
//		System.out.println("returning new connection ... ");
		return newConnection;
	}

	/**
	 * @return url of database with name of database
	 */
	private static String getDatabaseUrl() {
		// return dbProperties.getProperty("db.url") + "/" + dbName;
		return dbProperties.getProperty("db.url") + ";database=" + dbName;

	}

	public static Connection getSecondConnection() {
		setProperties();
		Connection newConnection = null;
		String dbUrl = getDatabaseUrl();
		try {
			System.out.println("Connecting New... ");
			newConnection = DriverManager.getConnection(dbUrl, dbProperties);
		} catch (SQLException ex) {
			setError("Error while making Connectiing DataBase :" + ex);
			System.out.println("Error in connection " + ex.toString());
		}
		return newConnection;
	}

	/**
	 * @param driverName
	 */
	private static void loadDatabaseDriver(String driverName) {
		try {
			Class.forName(driverName);
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}

	}

	public static Properties loadDBProperties() {
		InputStream dbPropInputStream = null;
		dbPropInputStream = DbConnection.class
				.getResourceAsStream("Configuration.properties");
		dbProperties = new Properties();
		try {
			dbProperties.load(dbPropInputStream);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return dbProperties;
	}

	/**
	 * @param strErr
	 *            Details of the error
	 */
	public static void setError(String strErr) {
		strError = strErr;
	}

	private static void setProperties() {
		dbProperties = loadDBProperties();
		dbName = dbProperties.getProperty("db.schema");
		loadDatabaseDriver(dbProperties.getProperty("db.driver"));
	}

	/**
	 * @return detail errors
	 */
	public String getError() {
		return strError;
	}
}
