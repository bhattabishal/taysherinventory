package com.tia;

import java.io.File;
import java.util.Date;

import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.user.controller.LoginController;
import com.tia.user.controller.LoginSession;
import com.tia.user.view.LoginDialog;

public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {
	private static LoginSession loginSession = new LoginSession();
	private static UtilMethods objUtilMethods = new UtilMethods();
	private static String strSeparator = "/";
	
	
	public static int getStatus(){
		return loginSession.getStatus();
	}
	public static String getCurClosingPeriodEndMonthNpDt() {
		return loginSession.getCurPeriodEndMonthDate();
	}
	public static String getCurrentUser(){
		return loginSession.getCurrentUser();
	}
	public static String getUserFullName(){
		return loginSession.getUserFullName();
	}
	public static String getCurClosingPeriodStartMonthNpDt() {
		return loginSession.getCurPeriodStartMonthDate();
	}
	public static String getDateSeperator() {
		return strSeparator;
	}
	public static String getParkingAccount() {
		return loginSession.getParkingAccountCode();
	}

	public static Date getLoginDate(){
		return loginSession.getLoginDate();
	}
	public static Date getCurClosingPeriodStartMonthEnDt() {
		return loginSession.getCurPeriodStartMonthEnDate();
	}
	public static int getLoginId(){
		return loginSession.getUserId();
	}
	public static int getLoginInfoId() {
		return loginSession.getLogin_info_id();
	}
	public static String getOfficeName() {
		return "Teyseer Auto Services";
	}
	public static Date getCurClosingPeriodEndMonthEnDt() {
		return loginSession.getCurPeriodEndMonthEnDate();
	}
	public static String getCurrentClosingPeriod() {
		return loginSession.getCurrentClosingPeriod();
	}
	

	public static String getOfficeAddress() {
		return "Sitapaila-1, Kathmandu";
	}
	public static int getCurrentClosingPeriodId() {
		return loginSession.getCurClosingPeriodId();
	}
	public static int getFiscalId() {
		//return loginSession.getFiscalId();
		return 1;
	}
	
	public static boolean hasAuthOr(String[] strAutorityName) {
		return hasAuthOr(strAutorityName, true);
	}
	public static boolean hasAuthOr(String[] strAutorityName, boolean message) {
		boolean rtn = false;
		
		for (int i = 0; i < strAutorityName.length; i++) {
			if (loginSession.checkAuth(strAutorityName[i])) {
				System.out.println("true");
				rtn = true;
			} else {
				rtn = false;
				
			}
		}
		if (message && !rtn)
			UtilMethods.messageBox(SWT.ICON_WARNING, "Information",
			"You do not have Privilege. Please contact Administrator.").open();

		return rtn;
	}
	public static String getBackUpURL() {
		File dstn = new File(loginSession.getBackupURL());
		if (!dstn.isDirectory()) {
			
			dstn.mkdirs();
		}
		return loginSession.getBackupURL();
	}
	
	public static String getCurrentDbName() {
		return loginSession.getCurrentDatabase();
	}
	
	public static String getLogURL() {
		
		return loginSession.getLogURL();
	}
	
	public ApplicationWorkbenchWindowAdvisor(
			IWorkbenchWindowConfigurer configurer) {
		super(configurer);
	}
	public static String getClosingReportsURL() {
		String reportFolder = "";
		String fiscal_year = UtilFxn.GetValueFromTable("fiscal_year_mcg","fiscal_year","fy_id="+ApplicationWorkbenchWindowAdvisor.getFiscalId());
		reportFolder += "closing_"+fiscal_year.replaceAll("/", "-")+"_"+ApplicationWorkbenchWindowAdvisor.getCurrentClosingPeriod().replaceAll(" ", "")+"\\";
		File target = new File(loginSession.getClosingReportURL()+"\\"+reportFolder);
		if (!target.isDirectory()) {
			target.mkdirs();
		}
		return loginSession.getClosingReportURL()+"\\"+reportFolder;
	}

	@Override
	public ActionBarAdvisor createActionBarAdvisor(
			IActionBarConfigurer configurer) {
		return new ApplicationActionBarAdvisor(configurer);
	}
	@Override
	public boolean preWindowShellClose() {
		boolean returnValue = false;
		LoginController lic = new LoginController();
		String[] strButton = { "ok",
				"cancel" };
		System.out.println("the username is----------"
				+ ApplicationWorkbenchWindowAdvisor.getCurrentUser());
		if (UtilMethods.messageBox(
				SWT.ICON_INFORMATION,
				"LogOut",
				"GoodBye" + " "
						+ ApplicationWorkbenchWindowAdvisor.getCurrentUser()
						+ " .", strButton).open() == 0) {
			System.out.println("the username is----------"
					+ ApplicationWorkbenchWindowAdvisor.getCurrentUser());
			if (lic.LogOutUser()) {
				returnValue = true;
			} else {
				UtilMethods.messageBox(SWT.ICON_INFORMATION,
						"LogOut",
						"Unable to log out at this time.")
						.open();
			}
		}
		return returnValue;
	}

	@Override
	public void preWindowOpen() {
		LoginDialog loginDialog = new LoginDialog(PlatformUI.createDisplay());
		
		loginDialog.setLoginSession(loginSession);
	
		loginDialog.createPartControl(null);
		// return true;
		
		IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
		configurer.setInitialSize(new Point(1024, 768));
		configurer.setShowCoolBar(true);
		configurer.setShowStatusLine(true);
		configurer.setTitle("Inventory Management System"); //$NON-NLS-1$
	}
	@Override
	public void postWindowOpen() {
		IStatusLineManager statusline = getWindowConfigurer()
				.getActionBarConfigurer().getStatusLineManager();
		statusline.setMessage(null, "[Inventory Management System ]");
	}
}
