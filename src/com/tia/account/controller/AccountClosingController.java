package com.tia.account.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.model.AccountClosingModel;
import com.tia.account.model.TempBalanceSheetModel;
import com.tia.common.db.DbConnection;
import com.tia.common.util.LogFileCreate;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;



/**
 * @author minesh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 * @altered & revised by suresh
 */
public class AccountClosingController {
	Connection cnn;
	Statement statement;
	ResultSet resultset;

	SimpleDateFormat simpleDateFormat;
	JCalendarFunctions jCalendarFunctions;
	AccountClosingModel accountClosingModel;
	JournalEntryController journalEntryController;
	StringBuilder journalQuery, ledgerQuery;
	int maxJvDetId = 0, maxLedId = 0;
	double sumIncome = 0.00, sumExpenses = 0.00;
	SimpleDateFormat date;
	DecimalFormat df = new DecimalFormat("#0.00");
	

	public AccountClosingController() {
		cnn = DbConnection.getConnection();
		simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
		date = new SimpleDateFormat("yyyy-MM-dd");
		jCalendarFunctions = new JCalendarFunctions();

		maxJvDetId = UtilMethods.getMaxId("journal_voucher_details_mcg",
				"jv_det_no");
		maxLedId = UtilMethods.getMaxId("ledger_mcg", "led_id");

		journalQuery = new StringBuilder(
				"INSERT INTO journal_voucher_details_mcg (jv_det_no, jv_no, acc_code, sub_acc_code, "
						+ "dr_amt, cr_amt, narration, remarks, created_by, created_date) VALUES ");
		ledgerQuery = new StringBuilder(
				"INSERT INTO ledger_mcg (led_id, jv_no, acc_code, sub_acc_code, to_acc_code, "
						+ "to_sub_acc_code, dr_amt, cr_amt, posted_date, remarks, created_by, created_date, "
						+ "nepali_created_dt, br_id, fy_id,is_opening) VALUES ");

		journalEntryController = new JournalEntryController();
		new AccHeadController();

	}

	/**
	 * Method that handles all the procedures of the account closing process.
	 * The account closing process includes the following steps:
	 * 
	 * i) For each account code transactions in the current closing quarter,
	 * calculate the monthly balance and save to the acc_monthly_closing_quarter
	 * table as summary for future records. The data in this table will never be
	 * deleted.
	 * 
	 * ii) For account codes in ledger table that are of type Assets and
	 * liabilities, calculate the opening balance of each transaction and save
	 * them into the journal and ledger tables except for the Income and
	 * Expenditure type accounts.
	 * 
	 * iii) For income and expenditure type accounts, calculate profit or loss
	 * and save that amount to the journal and ledger table.
	 * 
	 * @param closingEngDate
	 *            (English closing date.)
	 * @param closingNepDate
	 *            (Nepali closing date.)
	 * @return boolean (true if the account closing was successfully performed,
	 *         false otherwise.)
	 * */
	public boolean performAccountClosing(Date closingEngDate,
			String closingNepDate, ArrayList<String> log) {
		/*
		 * Check if there are any invalid transactions in the ledger posting
		 * till now. If there are any, Prompt the user with msg, which voucher
		 * no. trxns belong to such invalid trxns. If there is no invalid
		 * transaction found, proceed for account closing.
		 */
		String logData = "\n\t\t(Account-Closing):";
		boolean retFlag = false;
		if (!checkInvalidTrxns(simpleDateFormat.format(closingEngDate))) {

			// and now first export TB,BS and PL report with balances just
			// before Account closing
			exportClosingReportsToPDF(2);
			int nepMonth = Integer.parseInt(closingNepDate.substring(5, 7));
			String startDate = "", endDate = "";
			Date startingDate = null;
			int quarter = 0;

			endDate = simpleDateFormat.format(jCalendarFunctions
					.GetEnglishDate(closingNepDate));

			String profitRoot = UtilFxn
					.GetValueFromTable("SELECT acc_code from acc_fxn_mapping_mcg where map_for='Profit Account'");
			String lossRoot = UtilFxn
					.GetValueFromTable("SELECT acc_code from acc_fxn_mapping_mcg where map_for='Loss Account'");
			if (profitRoot.equals("")) {
				MessageBox mb = new MessageBox(Display.getCurrent()
						.getActiveShell(), SWT.ICON_ERROR);
				mb.setText("CodeNotFound");
				mb.setMessage("Insert Profit Code In Fix Maping Table");
				mb.open();
				retFlag = false;
			}
			if (lossRoot.equals("")) {
				MessageBox mb = new MessageBox(Display.getCurrent()
						.getActiveShell(), SWT.ICON_ERROR);
				mb.setText("Code Not Found");
				mb.setMessage("Insert Loss Code In FixMaping Table");
				mb.open();
				retFlag = false;
			} else {

				// startDate in date format
				startingDate = ApplicationWorkbenchWindowAdvisor
						.getCurClosingPeriodStartMonthEnDt();
				startDate = simpleDateFormat.format(startingDate);
				/*
				 * try { } catch (ParseException e1) {
				 * 
				 * e1.printStackTrace(); }
				 */

				/*
				 * Query to find out the summary of each account codes in three
				 * subsequent months for the current account closing quarter
				 * under consideration.
				 */
				String sql = "WITH W_RESULT AS (SELECT  "
						+ " SUBSTRING(l.nepali_created_dt, 1,4) AS nepali_year, SUBSTRING(l.nepali_created_dt, 6, 2) AS nepali_mt, "
						+ " dr_amt, cr_amt, l.acc_code AS acc_code"
						+ "	FROM "
						+ "		ledger_mcg l "
						+ "	WHERE "
						+ "		posted_date BETWEEN '"
						+ startDate
						+ "' AND  '"
						+ endDate
						+ "' ) "
						+ "	SELECT nepali_year, nepali_mt, SUM(dr_amt) AS dr_amt, SUM(cr_amt) AS cr_amt, "
						+ " acc_code FROM W_RESULT "
						+ " GROUP BY nepali_year, nepali_mt, acc_code ORDER BY acc_code ";

				System.out.println("start date " + startDate + "end date "
						+ endDate);
				System.out.println("sql: " + sql);

				String insertSql = "";

				try {
					cnn = DbConnection.getConnection();
					cnn.setAutoCommit(false);

					statement = cnn.createStatement(
							ResultSet.TYPE_SCROLL_SENSITIVE,
							ResultSet.CONCUR_UPDATABLE);

					resultset = statement.executeQuery(sql);

					/*
					 * Saving the quarterly summary balance into the
					 * acc_monthly_closing_mcg table.
					 */
					while (resultset.next()) {

						accountClosingModel = new AccountClosingModel();
						// closingModel.setDr_amt(Math.abs(rs.getDouble("dr_amt")));
						// closingModel.setCr_amt(Math.abs(rs.getDouble("cr_amt")));
						accountClosingModel.setDr_amt(resultset
								.getDouble("dr_amt"));
						accountClosingModel.setCr_amt(resultset
								.getDouble("cr_amt"));
						accountClosingModel.setAcc_code(resultset
								.getString("acc_code"));
						accountClosingModel.setNep_year(resultset
								.getInt("nepali_year"));
						accountClosingModel.setNep_month(resultset
								.getInt("nepali_mt"));
						// closingModel.setPosted_date(rs.getString("posted_date"));
						accountClosingModel.setQuarter(quarter);
						accountClosingModel
								.setBr_id(1);
						accountClosingModel
								.setFy_id(ApplicationWorkbenchWindowAdvisor
										.getFiscalId());
						accountClosingModel
								.setCreated_by(ApplicationWorkbenchWindowAdvisor
										.getLoginId());
						accountClosingModel
								.setCreated_dt(jCalendarFunctions.currentDate());
						accountClosingModel.setId(UtilMethods.getMaxId(
								"acc_monthly_closing_mcg", "id"));

						insertSql = accountClosingModel.Insert();

						Statement st1 = cnn.createStatement(
								ResultSet.TYPE_SCROLL_SENSITIVE,
								ResultSet.CONCUR_UPDATABLE);

						// if query is successful, return true
						if (st1.executeUpdate(insertSql) >= 1)
							
								retFlag = true;
							else
								retFlag = false;

						if (!retFlag) {
							cnn.rollback();

							/*
							 * Method returns false and the rest of the method
							 * will not be executed at all.
							 */
							return retFlag;
						}
						logData += "\tDr_amt->"
								+ accountClosingModel.getDr_amt()
								+ "\tCr_amt->"
								+ accountClosingModel.getCr_amt()
								+ "\tAcc_code->"
								+ accountClosingModel.getAcc_code()
								+ "\tNep_year->"
								+ accountClosingModel.getNep_year()
								+ "\tNep_month->"
								+ accountClosingModel.getNep_month()
								+ "\tQuarter->"
								+ accountClosingModel.getQuarter();

					}

					/*
					 * Now fetch the account code details to calculate the
					 * opening balances of Assets and Liabilities type account
					 * codes and profit/loss for Income and Expenditure type
					 * account codes and save them into the journal details and
					 * the ledger_mcg table.
					 */

					String minLedId = UtilFxn.GetMinValueFromTable(
							"ledger_mcg",
							"led_id",
							"jv_no = -1 AND posted_date = '"
									+ jCalendarFunctions
											.addSubractDaysFrmEngDate(
													startingDate, "minus", 1,
													"yyyy-MM-dd") + "' ");
					String getQtrBalance = "SELECT l.acc_code, SUM(dr_amt) AS dr_sum, SUM(cr_amt) AS cr_sum, a.acc_type "
							+ " FROM ledger_mcg l INNER JOIN acc_head_mcg a ON l.acc_code = a.acc_code "
							+ " WHERE "
							+ (minLedId.equals("") ? " "
									: " led_id >= (SELECT MIN(led_id) FROM ledger_mcg WHERE jv_no = -1 "
											+ " AND posted_date = '"
											+ jCalendarFunctions
													.addSubractDaysFrmEngDate(
															startingDate,
															"minus", 1,
															"yyyy-MM-dd")
											+ "') AND ")
							+ ""
							+ "  posted_date <= '"
							+ endDate
							+ "' and l.acc_code not in ('"
							+ lossRoot
							+ "','"
							+ profitRoot
							+ "') "
							+ " GROUP BY a.acc_type, l.acc_code ";

					statement = cnn.createStatement(
							ResultSet.TYPE_SCROLL_SENSITIVE,
							ResultSet.CONCUR_UPDATABLE);

					System.out.println("Closing qry " + getQtrBalance);

					resultset = statement.executeQuery(getQtrBalance);

					while (resultset.next()) {

						Double dr_sum = 0.00;
						Double cr_sum = 0.00;

						/*
						 * Only the account codes of account type Assets(1) and
						 * Liabilities(4) are saved into the ledger_mcg table as
						 * quarterly opening balance.
						 */
						if (resultset.getInt("acc_type") == 1
								|| resultset.getInt("acc_type") == 4) {

							// if ((Math.abs(rs.getDouble("dr_sum")) -
							// Math.abs(rs
							// .getDouble("cr_sum"))) > 0)
							if (((resultset.getDouble("dr_sum")) - (resultset
									.getDouble("cr_sum"))) > 0)
								dr_sum = Math.abs(resultset.getDouble("dr_sum")
										- resultset.getDouble("cr_sum"));
							// dr_sum = (Math.abs(rs.getDouble("dr_sum")) -
							// Math.abs(rs
							// .getDouble("cr_sum")));
							else
								cr_sum = Math.abs(resultset.getDouble("dr_sum")
										- resultset.getDouble("cr_sum"));
							// cr_sum = (Math.abs(rs.getDouble("dr_sum")) -
							// Math.abs(rs
							// .getDouble("cr_sum")));

							// increment the jv_det_id and led_id
							++maxJvDetId;
							++maxLedId;

							/*
							 * Now build the journal and ledger query for each
							 * opening balances.
							 */

							/*
							 * journalQuery .append("(" + maxJvDetId + "," + -1
							 * + ", '" + rs.getString("acc_code") + "' , '0'" +
							 * "," + dr_sum + "," + cr_sum + "," +
							 * " 'Quarterly opening balance.', 'Opening Balance', "
							 * + ApplicationWorkbenchWindowAdvisor .getUserId()
							 * + ", '" +
							 * ApplicationWorkbenchWindowAdvisor.getDayInDate()
							 * + "'),");
							 */

							ledgerQuery.append("("
									+ maxLedId
									+ ", -1, '"
									+ resultset.getString("acc_code")
									+ "', '0', '0', '0', "
									+ dr_sum
									+ ", "
									+ cr_sum
									+ ", '"
									+ jCalendarFunctions.currentDate()
									+ "', 'Opening Balance',"
									+ ApplicationWorkbenchWindowAdvisor
											.getLoginId()
									+ ", '"
									+jCalendarFunctions.currentDate()
									+ "', '"
									+ jCalendarFunctions.getCurrentNepaliDate()
									+ "', "
									+ 1
									+ ", "
									+ ApplicationWorkbenchWindowAdvisor
											.getFiscalId() + ",1),");

						}

					}

					/*
					 * Save the quarterly opening balances in the
					 * journal_details_mcg and ledger_mcg tables respectively.
					 */
					// actual journal and ledger query
					journalQuery.replace(journalQuery.lastIndexOf(","),
							journalQuery.lastIndexOf(",") + 1, " ");
					ledgerQuery.replace(ledgerQuery.lastIndexOf(","),
							ledgerQuery.lastIndexOf(",") + 1, " ");
					System.out.println("kkk... journalQry in closing = "
							+ journalQuery.toString());
					System.out.println("kkk... ledgerQry in closing = "
							+ ledgerQuery.toString());
					// save the quarterly balance
					/*
					 * if (jeController.saveQuarterlyBalance(cnn,
					 * journalQuery.toString(), ledgerQuery.toString()))
					 */

					// JournalEntryController jec=new JournalEntryController();
					// if(jec.savePlTransferJournalsAndPostThem(cnn,jvDetails,jvNo));
					{
						String frmDate="";//added by bishal
						ProfitLossController plController = new ProfitLossController();
						ResultSet incomeRs = plController.loadIncome(
								jCalendarFunctions.currentDate(), "1",frmDate);
						ResultSet expenseRs = plController.loadExpenses(
								jCalendarFunctions.currentDate(), "1",frmDate);

						// calculate the total expenses amount
						while (expenseRs.next()) {
							// add the amounts of the top level account codes
							// only
							if (expenseRs.getString(1).matches("[0-9]*"))
								sumExpenses += expenseRs.getDouble("Amt");

						}

						// calculate the total income amount
						while (incomeRs.next()) {
							// add the amounts of the top level account codes
							// only
							if (incomeRs.getString(1).matches("[0-9]*"))
								sumIncome += incomeRs.getDouble("Amt");

						}
						/*
						 * Now that the quarterly opening balances have been
						 * saved, now we need to save the profit and loss for
						 * this quarter.
						 */
						double plAmt = sumIncome - sumExpenses;
						double crAmt = 0.00, drAmt = 0.00;
						/*
						 * If plAmt > 0, then we have profit else we have loss.
						 */
						if (plAmt > 0)
							crAmt = Math.abs(plAmt);
						else
							drAmt = Math.abs(plAmt);

						if (journalEntryController.saveQuarterlyPLAmt(cnn,
								drAmt, crAmt, jCalendarFunctions.currentDate(), profitRoot, lossRoot,
								log)) { // for
										// PL
										// amt

							// save the quarterly balance
							if (journalEntryController.saveQuarterlyBalance(
									cnn, journalQuery.toString(),
									ledgerQuery.toString(), log))
								retFlag = true;
							retFlag = true;

							cnn.commit();
							LogFileCreate.logFileCreater(cnn, "Admin",
									"Account-Closing", "Closing", log);

							// Final tasks of account closing if it is YEAR_END
							// closing
							int curNepMonth = Integer
									.valueOf(jCalendarFunctions.getCurrentNepaliDate().substring(5, 7));
							if (curNepMonth == (Integer
									.valueOf(UtilFxn
											.GetValueFromTable("select end_month from closing_period_info_mcg where id=(select MAX(id) from closing_period_info_mcg)")))) {
								performFinalYearClosingTasks(cnn);
							}
						}

						/*
						 * If quarterly balances are saved but the profit is not
						 * saved, then we must rollback the quarterly balances
						 * too.
						 */
						else {
							retFlag = true;
							cnn.rollback();
						}
					}

					/*
					 * else { retFlag = false; cnn.rollback(); }
					 */

				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		} else {
			retFlag = false;
		}
		return retFlag;

	}

	/**
	 * Adds one extra day to whatever date is today.
	 * 
	 * @param none
	 * @return String (Date incremented by one day on today's date.)
	 */
	private String getDateIncrementedByOneDay() {
		String dt = simpleDateFormat.format(new Date()); // Start date

		Calendar c = Calendar.getInstance();
		try {
			c.setTime(simpleDateFormat.parse(dt));
		} catch (ParseException e) {

			e.printStackTrace();
		}
		c.add(Calendar.DATE, 1); // number of days to add
		dt = simpleDateFormat.format(c.getTime()); // dt is now the new date

		return dt;
	}

	/**
	 * @return boolean value method that performs account closing as of BS just
	 *         before closing. i.e. this is another method to perform account
	 *         closing. This method has been tested and works OK provided that
	 *         the conditions used for calculation of BalanceSheet remain same
	 *         as used in Original BS view.
	 * 
	 *         performancewise this method is superb.
	 */
	public boolean performClosingAsOfBs() {
		boolean retFlag = false;
		// Actual Closing Process Begins
		// find current PL amount
		String frmDate="";
		ProfitLossController plController = new ProfitLossController();
		ResultSet incomeRs = plController.loadIncome(
				jCalendarFunctions.currentDate(), "1",frmDate);
		ResultSet expenseRs = plController.loadExpenses(
				jCalendarFunctions.currentDate(), "1",frmDate);

		// calculate the total expenses amount
		try {
			while (expenseRs.next()) {
				// add the amounts of the top level account codes only
				if (expenseRs.getString(1).matches("[0-9]*"))
					sumExpenses += expenseRs.getDouble("Amt");

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// calculate the total income amount
		try {
			while (incomeRs.next()) {
				// add the amounts of the top level account codes only
				if (incomeRs.getString(1).matches("[0-9]*"))
					sumIncome += incomeRs.getDouble("Amt");

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		 * Now that the quarterly opening balances have been saved, now we need
		 * to save the profit and loss for this quarter.
		 */
		double plAmt = sumIncome - sumExpenses;
		// the Closing Query
		String trxnDateEn = jCalendarFunctions.currentDate();
		String trxnDateNp = jCalendarFunctions.getCurrentNepaliDate();
		int brId =1;
		int fyId = ApplicationWorkbenchWindowAdvisor.getFiscalId();
		int userId = ApplicationWorkbenchWindowAdvisor.getLoginId();
		String profitRoot = UtilFxn
				.GetValueFromTable("SELECT acc_code from acc_fxn_mapping_mcg where map_for='Profit Account'");
		if (profitRoot.equals("")) {
			MessageBox mb = new MessageBox(Display.getCurrent()
					.getActiveShell(), SWT.ICON_ERROR);
			mb.setText("Code Not Found");
			mb.setMessage("Insert Profit Code In FixMaping Table");
			mb.open();
			return false;
		}
		String lossRoot = UtilFxn
				.GetValueFromTable("SELECT acc_code from acc_fxn_mapping_mcg where map_for='Loss Account'");
		if (lossRoot.equals("")) {
			MessageBox mb = new MessageBox(Display.getCurrent()
					.getActiveShell(), SWT.ICON_ERROR);
			mb.setText("Code Not Found");
			mb.setMessage("Insert Loss Code In FixMaping Table");
			mb.open();
			return false;
		}
		String closingQry = " INSERT INTO ledger_mcg (led_id, jv_no, acc_code,cr_amt ,dr_amt , posted_date, remarks, created_by, created_date, "
				+ " nepali_created_dt, br_id, fy_id,is_opening) "
				+ " (SELECT (select MAX(led_id) from ledger_mcg)+1 ,'-1','"
				+ (plAmt > 0 ? profitRoot : lossRoot)
				+ "','"
				+ (plAmt > 0 ? df.format(plAmt) : 0.00)
				+ "', '"
				+ (plAmt > 0 ? 0.00 : df.format(Math.abs(plAmt)))
				+ "' "
				+ " ,'"
				+ trxnDateEn
				+ "','KHATABAND "
				+ (plAmt > 0 ? "PROFIT" : "LOSS")
				+ " AMOUNT POSTED','"
				+ userId
				+ "',GETDATE(),'"
				+ trxnDateNp
				+ "',"
				+ brId
				+ ","
				+ fyId
				+ ",1 )"
				+ " UNION "
				+ " (SELECT (select MAX(led_id) from ledger_mcg)+1+ROW_NUMBER() OVER(ORDER BY acc_code asc) as led_id,'-1' jv_no, acc_code, "
				+ " (case when acc_type=4 then coalesce(LAmt,0) else 0 end) AS cr_amt,(case when acc_type=1 then coalesce(LAmt,0) else 0 end) AS dr_amt,"
				+ " '"
				+ trxnDateEn
				+ "' as posted_date,'KHATABAND RAKAM POST GAREKO' as remarks,'"
				+ userId
				+ "' as created_by,GETDATE() as created_date, "
				+ " '"
				+ trxnDateNp
				+ "' as nepali_created_dt,"
				+ brId
				+ " as  br_id, "
				+ fyId
				+ " as fy_id, 1 as is_opening  FROM  "
				+ " ( SELECT acc_code,  alias,parent, acc_type, "
				+ " (SELECT  (case when a.acc_type=4 then (SUM(cr_amt)-SUM(dr_amt)) when a.acc_type=1 then (SUM(dr_amt)-SUM(cr_amt)) end  )	FROM ledger_mcg l "
				+ " WHERE (l.acc_code=a.acc_code)  AND (led_id >= (select min(led_id) from ledger_mcg where jv_no = -1  "
				+ " and posted_date = (select cast(MAX(posted_date) as date)as lastClosingDate from ledger_mcg where jv_no=-1 and posted_date <='"
				+ trxnDateEn
				+ "'))) "
				+ " and posted_date !> '"
				+ trxnDateEn
				+ "' ) AS LAmt FROM acc_head_mcg AS a WHERE acc_type in ('1','4') and "
				+ " a.acc_code not in ('"
				+ profitRoot
				+ "','"
				+ lossRoot
				+ "')) AS T1 " + " WHERE coalesce(LAmt,0)>0 )";
		Connection cn = DbConnection.getConnection();
		try {
			System.out.println("\nACC_CLOSING QRY by suresh=" + closingQry);
			cn.setAutoCommit(false);
			Statement st = cn.createStatement();
			if (st.executeUpdate(closingQry) > 0) {
				
					retFlag = true;
					cn.commit();
					cn.setAutoCommit(true);
				
			} else {
				retFlag = false;
				cn.rollback();
				cn.setAutoCommit(true);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			retFlag = false;
		}
		return retFlag;
	}

	/**
	 * @param cn
	 * 
	 *            method to carry-out some specific tasks like flushing records
	 *            from base accounting tables viz. JournalVoucher,
	 *            JournalVoucherDetails and Ledger and Cash Receipt tables. This
	 *            is only done if the closing is Year_End one, otherwise, this
	 *            is simply skipped.
	 * 
	 *            We flush all base-accounting table records (Except
	 *            closing-balances) once, each year for the context of
	 *            SimpleFinance.
	 */
	private void performFinalYearClosingTasks(Connection cn) {
		String yearEndClosingFinalTaskQry = "Truncate Table journal_voucher_mcg "
				+ " Truncate Table journal_voucher_details_mcg "
				+ " DELETE from ledger_mcg where jv_no<>-1 "
				+ " Truncate Table cash_income_mcg "
				+ " Truncate Table cash_in_details_mcg ";
		try {
			System.out.println("\nperforming Year_end closing final tasks="
					+ yearEndClosingFinalTaskQry);
			cn.setAutoCommit(false);
			Statement st = cn.createStatement();
			if (st.executeUpdate(yearEndClosingFinalTaskQry) > 0) {
				System.out.println("\nVouchers restarted, receipts deleted");
				
					System.out.println("\nVouchers restarted qry saved");
					cn.commit();
					cn.setAutoCommit(true);
				
			} else {
				cn.rollback();
				cn.setAutoCommit(true);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * @param rtp_time_cnst
	 * 
	 *            method to export base-accounting reports TB,BS and PL before
	 *            and after Closing while performing system closing.
	 */
	public void exportClosingReportsToPDF(int rtp_time_cnst) {
		// export core account reports before closing
		TrialBalanceController tbc = new TrialBalanceController();
		BalanceSheetController bsc = new BalanceSheetController();
		ProfitLossController plc = new ProfitLossController();
		boolean closingPerformed = false;
		String toDate = jCalendarFunctions.currentDate();
		String reportTitle = jCalendarFunctions.getCurrentNepaliDate().replaceAll("/", "_").replaceAll("-", "_")
				+ "_";
		String when = (rtp_time_cnst == 1) ? "before_closing": (rtp_time_cnst == 2 ? "after_closing" : 
				"after_acc_closing_no_inc_exp");
		/*
		 * rtp_time_cnst=1 [means report Before All Closing] rtp_time_cnst=2
		 * [means report just before Account Closing] rtp_time_cnst=3/others
		 * [means report just after Account Closing]
		 */
		reportTitle += (rtp_time_cnst == 1) ? "BeforeAllClosing"
				: (rtp_time_cnst == 2 ? "AfterAccClosing"
						: "AfterAccClosingNoIncExp");
		if (toDate.equals(ApplicationWorkbenchWindowAdvisor
				.getCurClosingPeriodEndMonthEnDt().toString())
				&& tbc.isClosingPerformed(toDate)) {

			closingPerformed = true;
		} else
			closingPerformed = false;
		List<TempBalanceSheetModel> listTempBalanceSheetModel = new ArrayList<TempBalanceSheetModel>();
		listTempBalanceSheetModel = tbc.loadTrialBalanceClosing(
				jCalendarFunctions.currentDate(),
				ApplicationWorkbenchWindowAdvisor.getFiscalId(),
				closingPerformed, "2", rtp_time_cnst, 1);

		JasperPrint jrTb = generateReport(
				ApplicationWorkbenchWindowAdvisor.getLoginId(),
				";Gt'ng kl/If)f", "trialbalanceClosing.jrxml", when,
				simpleDateFormat.format(ApplicationWorkbenchWindowAdvisor
						.getCurClosingPeriodStartMonthEnDt()),
						jCalendarFunctions.currentDate(),
				ApplicationWorkbenchWindowAdvisor
						.getCurClosingPeriodStartMonthNpDt(),
				jCalendarFunctions.getCurrentNepaliDate(),
				listTempBalanceSheetModel);
		listTempBalanceSheetModel.clear();
		try {
			JasperExportManager.exportReportToPdfFile(jrTb,
					ApplicationWorkbenchWindowAdvisor.getClosingReportsURL()
							+ reportTitle + "TBRpt.pdf");
		} catch (JRException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// re-declare the list to hold balance sheet info
		String frmDate="";
		listTempBalanceSheetModel = new ArrayList<TempBalanceSheetModel>();
		listTempBalanceSheetModel = bsc.loadBSClosing(
				jCalendarFunctions.currentDate(),
				ApplicationWorkbenchWindowAdvisor.getFiscalId(),
				closingPerformed, "2", rtp_time_cnst, 2,frmDate);
		JasperPrint jrBs = generateReport(
				ApplicationWorkbenchWindowAdvisor.getLoginId(), "jf;nft",
				"bsClosing.jrxml", when,
				simpleDateFormat.format(ApplicationWorkbenchWindowAdvisor
						.getCurClosingPeriodStartMonthEnDt()),
						jCalendarFunctions.currentDate(),
				ApplicationWorkbenchWindowAdvisor
						.getCurClosingPeriodStartMonthNpDt(),
				jCalendarFunctions.getCurrentNepaliDate(),
				listTempBalanceSheetModel);
		listTempBalanceSheetModel.clear();
		try {
			JasperExportManager.exportReportToPdfFile(jrBs,
					ApplicationWorkbenchWindowAdvisor.getClosingReportsURL()
							+ reportTitle + "BSRpt.pdf");
		} catch (JRException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// re-declare the list to hold PL info
		//String frmDate="";
		listTempBalanceSheetModel = new ArrayList<TempBalanceSheetModel>();
		listTempBalanceSheetModel = plc.loadPLClosing(
				jCalendarFunctions.currentDate(),
				ApplicationWorkbenchWindowAdvisor.getFiscalId(),
				closingPerformed, "2", rtp_time_cnst, 3,frmDate);
		JasperPrint jrPl = generateReport(
				ApplicationWorkbenchWindowAdvisor.getLoginId(), "gfkmf gf]S;fg",
				"bsClosing.jrxml", when,
				simpleDateFormat.format(ApplicationWorkbenchWindowAdvisor
						.getCurClosingPeriodStartMonthEnDt()),
						jCalendarFunctions.currentDate(),
				ApplicationWorkbenchWindowAdvisor
						.getCurClosingPeriodStartMonthNpDt(),
				jCalendarFunctions.getCurrentNepaliDate(),
				listTempBalanceSheetModel);
		try {
			JasperExportManager.exportReportToPdfFile(jrPl,
					ApplicationWorkbenchWindowAdvisor.getClosingReportsURL()
							+ reportTitle + "PLRpt.pdf");
		} catch (JRException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		listTempBalanceSheetModel.clear();
	}// end of method

	/**
	 * @param id
	 * @param report_title
	 * @param reportDesign
	 * @param when
	 * @param engFromDt
	 * @param engToDt
	 * @param nepfrom
	 * @param nepto
	 * @param listDs
	 * @return jasperprint document
	 * 
	 *         method to generate jasperprint of the base-accounting reports.
	 */
	public JasperPrint generateReport(int id, String report_title,
			String reportDesign, String when, String engFromDt, String engToDt,
			String nepfrom, String nepto, List<TempBalanceSheetModel> listDs) {
		JasperPrint myJPrint = null;

		try {
			JasperDesign jasperDesign = null;
			JasperReport jasperReport = null;

			// String report_title="Trial Balance (Closing)";
			String subTitle = "(" + engFromDt + " To " + engToDt + ")";

			

			jasperDesign = JRXmlLoader.load(AccountClosingController.class
					.getClassLoader().getResourceAsStream(reportDesign));
			jasperReport = JasperCompileManager.compileReport(jasperDesign);
			// Passing parameters to the report
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("report_title", report_title);
			params.put("subTitle", subTitle);
			params.put("id", id);
			params.put("institutionName",
					ApplicationWorkbenchWindowAdvisor.getOfficeName());
			params.put("address",
					ApplicationWorkbenchWindowAdvisor.getOfficeAddress());
			params.put("logo", UtilMethods.getLogoImage());
			/*
			 * Filling the report with data from the database based on the
			 * parameters passed.
			 */
			myJPrint = JasperFillManager
					.fillReport(
							jasperReport,
							params,
							new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource(
									listDs));
			params.clear();

		} catch (JRException ex) {
			ex.printStackTrace();
		}

		return myJPrint;

	}// end of method

	/**
	 * @param toDateEn
	 * @return boolean value
	 * 
	 *         method to check if INVALID TRANSACTIONS exist in the existing
	 *         base-accounting tables for this particular accounting period.
	 * 
	 */
	public boolean checkInvalidTrxns(String toDateEn) {
		boolean retFlag = false;
		String msg = "";
		ResultSet rs = null;
		String checkInvalidTrxnJvNoQry = " select distinct jv_no from ledger_mcg "
				+ " where (posted_date between '"
				+ simpleDateFormat.format(ApplicationWorkbenchWindowAdvisor
						.getCurClosingPeriodStartMonthEnDt())
				+ "' and '"
				+ toDateEn
				+ "') "
				+ " and acc_code in "
				+ " ( select distinct acc_code from ledger_mcg "
				+ " where posted_date between '"
				+ simpleDateFormat.format(ApplicationWorkbenchWindowAdvisor
						.getCurClosingPeriodStartMonthEnDt())
				+ "' and '"
				+ toDateEn
				+ "' "
				+ " except "
				+ " select distinct acc_code from acc_head_mcg ) or (jv_no<>-1  and acc_code is NULL) or (jv_no<>-1  and to_acc_code is NULL) or "
				+ " (jv_no<>-1  and (dr_amt<=0 and cr_amt<=0)) order by 1 asc ";
		System.out.println("Check Invalid TRxn query: "
				+ checkInvalidTrxnJvNoQry);
		try {
			statement = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = statement.executeQuery(checkInvalidTrxnJvNoQry);
			while (rs.next()) {
				msg += rs.getString("jv_no") + ",";
			}
			if (!msg.equals("")) {
				retFlag = true;
				MessageBox mb = new MessageBox(Display.getDefault()
						.getActiveShell(), SWT.ICON_ERROR);
				mb.setText("Error");
				mb.setMessage("Invalid Transactions Found On Following Voucher No"
						+ msg.substring(0, msg.lastIndexOf(","))
						+ "\n"
						+ "correct Those Invalid Trxns First");
				mb.open();
			} else {
				retFlag = false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return retFlag;
	}// method ends

}// end of class