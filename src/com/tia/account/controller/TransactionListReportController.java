/**
 * 
 */
package com.tia.account.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tia.account.model.TransactionListReportModel;
import com.tia.common.db.DbConnection;



/**
 * @author suresh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 */
public class TransactionListReportController {

	private static Connection cnn;
	private static Statement st;
	private static ResultSet rs = null;

	/**
	 * @param fromDate
	 * @param toDate
	 * @return list of all transactions falling between these dates
	 * 
	 *         Note : we do not save query of this method and we do not need to
	 *         maintain any transaction here since this method is invoked each
	 *         time when we print transaction list report in GUI.
	 */
	public static List<TransactionListReportModel> getAllTrxnList(
			String fromDate, String toDate) {
		List<TransactionListReportModel> list = new ArrayList<TransactionListReportModel>();
		String sql = " SELECT l.nepali_created_dt,l.jv_no,a.acc_name particulars,l.acc_code ,l.dr_amt,l.cr_amt,l.remarks,l.created_by,l.br_id , "
			+ " ISNULL(u.full_name,'��')user_name ,mrm.name full_name,jvm.to_from_type  FROM [ledger_mcg] l "
			+ " INNER JOIN acc_head_mcg a on a.acc_code=l.acc_code  "
			+ " INNER JOIN user_accounts u on u.user_id=l.created_by"
			+ " INNER JOIN journal_voucher_mcg jvm on l.jv_no=jvm.jv_no "
			+ " INNER JOIN loan_transaction_mcg ltm on jvm.tran_id=ltm.trxn_ses_id "
			+ " INNER JOIN loan_request_mcg lrm on ltm.loan_id=lrm.loan_id "
			+ " INNER JOIN mem_reg_mcg mrm on lrm.mid=mrm.id "
			+ " WHERE l.posted_date BETWEEN '"
			+ fromDate
			+ "' AND '"
			+ toDate
			+ "' AND  l.jv_no<>-1 AND jvm.to_from_type ='loan' AND jvm.status=1 "
			+ " UNION"
			+ " (SELECT l.nepali_created_dt,l.jv_no,a.acc_name particulars,l.acc_code ,l.dr_amt,l.cr_amt,l.remarks,l.created_by,l.br_id , "
			+ " ISNULL(u.full_name,'��')user_name ,mai.mem_name,jvm.to_from_type "
			+ " FROM [ledger_mcg] l  "
			+ " INNER JOIN acc_head_mcg a on a.acc_code=l.acc_code "
			+ " INNER JOIN user_accounts u on u.user_id=l.created_by "
			+ " INNER JOIN journal_voucher_mcg jvm on l.jv_no=jvm.jv_no "
			+ " INNER JOIN sav_transaction st on jvm.tran_id=st.trxn_id "
			+ " inner join mem_acc_info mai on st.acc_no = mai.acc_no "
			+ " WHERE l.posted_date BETWEEN '"
			+ fromDate
			+ "' AND '"
			+ toDate
			+ "' AND l.jv_no<>-1 AND jvm.to_from_type ='saving' AND jvm.status=1 "
			+ "  ) "
			+ " UNION "
			+ " SELECT l.nepali_created_dt,l.jv_no,a.acc_name particulars,l.acc_code ,l.dr_amt,l.cr_amt,l.remarks,l.created_by,l.br_id , "
			+ " ISNULL(u.full_name,'��')user_name ,mrm.name full_name,jvm.to_from_type  FROM [ledger_mcg] l "
			+ " INNER JOIN journal_voucher_mcg jvm ON l.jv_no=jvm.jv_no"
			+ " AND (jvm.to_from_type='account'  AND jvm.receipt_no IS NOT NULL)"
			+ " AND l.jv_no<>-1 AND jvm.to_from_type='account' AND l.posted_date BETWEEN '"
			+ fromDate
			+ "' AND '"
			+ toDate
			+ "' INNER JOIN acc_head_mcg a ON l.to_acc_code=a.acc_code"
			+ " INNER JOIN cash_income_mcg cim ON jvm.receipt_no=cim.receipt_no"
			+ " INNER JOIN mem_reg_mcg mrm ON cim.grp_id=mrm.id"
			+ " INNER JOIN mem_group_mcg mgm ON mrm.grp_no=mgm.group_id"
			+ " INNER JOIN mem_group_mcg mgm1 ON mgm.parent_id=mgm1.group_id"
			+ " INNER JOIN user_accounts u ON jvm.created_by=u.user_id"
			+ " INNER JOIN code_value_mcg cvm ON mrm.vdc_id=cvm.cv_code AND cvm.cv_type='vdc_muncipality'"
			+ " UNION"
			+ " SELECT l.nepali_created_dt,l.jv_no,a.acc_name particulars,l.acc_code ,l.dr_amt,l.cr_amt,l.remarks,l.created_by,l.br_id ,"
			+ " ISNULL(u.full_name,'��')user_name ,'n]vfaf^' as full_name,jvm.to_from_type "
			+ " FROM [ledger_mcg] l "
			+ " INNER JOIN acc_head_mcg a on a.acc_code=l.acc_code "
			+ " INNER JOIN user_accounts u on u.user_id=l.created_by "
			+ " INNER JOIN journal_voucher_mcg jvm on l.jv_no=jvm.jv_no "
			+ " WHERE (jvm.to_from_type NOT IN ('account','saving','loan') OR jvm.to_from_type IS NULL) AND l.posted_date BETWEEN '"
			+ fromDate
			+ "' AND '"
			+ toDate
			+ "' AND l.jv_no<>-1 AND jvm.status=1 "
			+ " ORDER BY to_from_type ";
		System.out.println("zzz..Transaction List report's query: " + sql);

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);
			while (rs.next()) {
				TransactionListReportModel tlrm = new TransactionListReportModel();
				tlrm.setJv_date(rs.getString("nepali_created_dt"));
				tlrm.setJv_no(rs.getString("jv_no"));
				tlrm.setName(rs.getString("full_name"));
				tlrm.setParticulars(rs.getString("particulars"));
				tlrm.setDr_amt(rs.getString("dr_amt"));
				tlrm.setCr_amt(rs.getString("cr_amt"));
				tlrm.setRemarks(rs.getString("remarks"));
				tlrm.setUser_name(rs.getString("user_name"));

				// add this item into list
				list.add(tlrm);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}// end of method getAllTrxnList()

	/**
	 * @param fromDate
	 * @param toDate
	 * @param limitAmt
	 * @return list of all transactions matching the condition
	 * 
	 *         Note : we do not save query of this method and we do not need to
	 *         maintain any transaction here since this method is invoked each
	 *         time when we print transaction list report in GUI.
	 */
	public static List<TransactionListReportModel> getAllTrxnListWithAmtLimit(
			String fromDate, String toDate, Double limitAmt) {
		List<TransactionListReportModel> list = new ArrayList<TransactionListReportModel>();
		// The following QRY takes only +ve dr, cr entries since the condition
		// in WHERE clause filters out in that way
		String sql = " SELECT l.nepali_created_dt,l.jv_no,a.acc_name particulars,l.acc_code ,l.dr_amt,l.cr_amt,l.remarks,l.created_by,l.br_id , "
			+ " ISNULL(u.user_name,'--')user_name ,mrm.name full_name,jvm.to_from_type ,jvm.status FROM [ledger_mcg] l "
			+ " INNER JOIN acc_head_mcg a on a.acc_code=l.acc_code  "
			+ " INNER JOIN user_accounts u on u.user_id=l.created_by"
			+ " INNER JOIN journal_voucher_mcg jvm on l.jv_no=jvm.jv_no "
			+ " INNER JOIN loan_transaction_mcg ltm on jvm.tran_id=ltm.trxn_ses_id "
			+ " INNER JOIN loan_request_mcg lrm on ltm.loan_id=lrm.loan_id "
			+ " INNER JOIN mem_reg_mcg mrm on lrm.mid=mrm.id "
			+ " WHERE l.posted_date BETWEEN '"
			+ fromDate
			+ "' AND '"
			+ toDate
			+ "' AND  l.jv_no<>-1 AND jvm.to_from_type ='loan' AND jvm.status in (0,1) "
			+ " AND (l.cr_amt >="
			+ limitAmt
			+ " AND l.cr_amt >="
			+ limitAmt
			+ ")"
			+ " UNION"
			+ " (SELECT l.nepali_created_dt,l.jv_no,a.acc_name particulars,l.acc_code ,l.dr_amt,l.cr_amt,l.remarks,l.created_by,l.br_id , "
			+ " ISNULL(u.user_name,'--')user_name ,mai.mem_name full_name,jvm.to_from_type,jvm.status "
			+ " FROM [ledger_mcg] l  "
			+ " INNER JOIN acc_head_mcg a on a.acc_code=l.acc_code "
			+ " INNER JOIN user_accounts u on u.user_id=l.created_by "
			+ " INNER JOIN journal_voucher_mcg jvm on l.jv_no=jvm.jv_no "
			+ " INNER JOIN sav_transaction st on jvm.tran_id=st.trxn_id "
			+ " inner join mem_acc_info mai on st.acc_no = mai.acc_no "
			+ " WHERE l.posted_date BETWEEN '"
			+ fromDate
			+ "' AND '"
			+ toDate
			+ "' AND l.jv_no<>-1 AND jvm.to_from_type ='saving' AND jvm.status in (0,1) "
			+ " AND (l.cr_amt >="
			+ limitAmt
			+ " AND l.cr_amt >="
			+ limitAmt
			+ ")"
			+ "  ) "
			+ " UNION "
			+ " (select jvm.nepali_jv_date nepali_created_dt,jvm.jv_no,ahm.acc_name particulars,jvdm.acc_code,jvdm.dr_amt,jvdm.cr_amt,ISNULL(jvm.remarks,'--')remarks,jvm.created_by,jvm.br_id, "
			+ " ISNULL(u.full_name,'--'), 'Account' as full_name,jvm.to_from_type,jvm.status "
			+ " FROM journal_voucher_mcg jvm "
			+ " INNER JOIN journal_voucher_details_mcg jvdm ON jvm.jv_no=jvdm.jv_no "
			+ " INNER JOIN acc_head_mcg ahm ON jvdm.acc_code=ahm.acc_code "
			+ " INNER JOIN user_accounts u ON jvdm.created_by=u.user_id "
			+ " WHERE jvm.created_date BETWEEN '"
			+ fromDate
			+ "' AND '"
			+ toDate
			+ "' AND jvm.status=0 "
			+ " AND (jvdm.cr_amt >="
			+ limitAmt
			+ " AND jvdm.cr_amt >="
			+ limitAmt
			+ ")"
			+ " UNION "
			+ " SELECT l.nepali_created_dt,l.jv_no,a.acc_name particulars,l.acc_code ,l.dr_amt,l.cr_amt,l.remarks,l.created_by,l.br_id ,"
			+ " ISNULL(u.user_name,'--')user_name ,'Account' as full_name,jvm.to_from_type,jvm.status "
			+ " FROM [ledger_mcg] l "
			+ " INNER JOIN acc_head_mcg a on a.acc_code=l.acc_code "
			+ " INNER JOIN user_accounts u on u.user_id=l.created_by "
			+ " INNER JOIN journal_voucher_mcg jvm on l.jv_no=jvm.jv_no "
			+ " WHERE (jvm.to_from_type NOT IN ('saving','loan') OR jvm.to_from_type IS NULL) AND l.posted_date BETWEEN '"
			+ fromDate
			+ "' AND '"
			+ toDate
			+ "' AND l.jv_no<>-1 AND jvm.status in (0,1) "
			+ " AND (l.cr_amt >="
			+ limitAmt
			+ " AND l.cr_amt >="
			+ limitAmt + ")" + ") ORDER BY to_from_type ";
		System.out.println("kkk..Exception report's query: " + sql);

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);
			while (rs.next()) {
				TransactionListReportModel tlrm = new TransactionListReportModel();
				tlrm.setJv_date(rs.getString("nepali_created_dt"));
				tlrm.setJv_no(rs.getString("jv_no"));
				tlrm.setName(rs.getString("full_name"));
				tlrm.setParticulars(rs.getString("particulars"));
				tlrm.setDr_amt(rs.getString("dr_amt"));
				tlrm.setCr_amt(rs.getString("cr_amt"));
				tlrm.setRemarks(rs.getString("remarks"));
				tlrm.setUser_name(rs.getString("user_name"));
				tlrm.setStatus(rs.getString("status"));// This field should be
				// explicitly set only
				// for
				// Exception report

				// add this item into list
				list.add(tlrm);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}// end of method getAllTrxnListWithAmtLimit()

}// end of class
