package com.tia.account.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.model.ClosingInfoModel;
import com.tia.account.model.JVDetailsModel;
import com.tia.account.model.LedgerModel;
import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import com.tia.user.controller.LoginSession;



/**
 * @author minesh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 * @altered & revised by suresh
 */
public class LedgerController {
	Connection cnn;
	Statement st;
	ResultSet rs;
	LedgerModel objLedger;

	SimpleDateFormat sdf;
	JCalendarFunctions jcal;
	/*
	 * Arraylists for holding the Debit and Credit entries of a single journal
	 * transaction.
	 */
	List<JVDetailsModel> drList = new ArrayList<JVDetailsModel>();
	List<JVDetailsModel> crList = new ArrayList<JVDetailsModel>();
	Object z_WhereClause;

	// For sole Use of Assets-Liability account type only---by surseh
	public static boolean isALdr;

	// For sole Use of Assets-Liability account type only---by surseh
	public LedgerController() {
		this.cnn = DbConnection.getConnection();
		objLedger = new LedgerModel();

		sdf = new SimpleDateFormat("yyyy-MM-dd");
		jcal = new JCalendarFunctions();
	}

	/**
	 * postToLedger() to post the approved journal entries into the ledger
	 * accounts.
	 * 
	 * @param connection
	 *            (The connection that was initiated in the setStatusApproved()
	 *            method. Since approved journal entries must be posted to the
	 *            ledger account, the same connection is used so that the
	 *            transaction can be committed or rolled back if posting to the
	 *            ledger fails.)
	 * @param jvNo
	 *            (Journal Voucher No. which has been clicked as approved. The
	 *            journal with this jvNo is posted into the ledger.)
	 */

	public boolean postToLedger(Connection connection, String jvNo,
			ArrayList<String> log) {

		/* Objects to hold a single Dr. and Cr. entry. */
		JVDetailsModel objJVDr = null;
		JVDetailsModel objJVCr = null;

		boolean status1 = false, status2 = false;

		// clear the global collections to flush previous data+add list items
		List<JVDetailsModel> drList = new ArrayList<JVDetailsModel>();
		List<JVDetailsModel> crList = new ArrayList<JVDetailsModel>();
		// get the journal details
		// setJournalDetails(connection, jvNo);
		JVDetailsModel objJVDetails;
		String logs = "\n\t\t(JV-Details):";
		String strQry = "SELECT jv_det_no, jv_no, acc_code, sub_acc_code, dr_amt, cr_amt,"
				+ "narration, remarks "
				+ "FROM journal_voucher_details_mcg "
				+ "WHERE jv_no = " + jvNo;

		try {
			this.st = connection
					.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
							ResultSet.CONCUR_UPDATABLE);

			this.rs = st.executeQuery(strQry);

			while (rs.next()) {
				objJVDetails = new JVDetailsModel();
				objJVDetails.setJv_det_no(rs.getInt("jv_det_no"));
				objJVDetails.setJv_no(rs.getInt("jv_no"));
				objJVDetails.setAcc_code(rs.getString("acc_code"));
				objJVDetails.setSub_acc_code(rs.getString("sub_acc_code"));
				objJVDetails.setDr_amt(rs.getDouble("dr_amt"));
				objJVDetails.setCr_amt(rs.getDouble("cr_amt"));
				objJVDetails.setNarration(rs.getString("narration").replaceAll(
						"'", "''"));
				objJVDetails.setRemarks(rs.getString("remarks").replaceAll("'",
						"''"));
				objJVDetails.setBr_id(1);

				/*
				 * Separate Debit and Credit entries into ArrayLists of two
				 * types.
				 */
				if (Double.parseDouble((objJVDetails.getDr_amt())) == 0.0) {
					crList.add(objJVDetails);

				} else {
					drList.add(objJVDetails);
				}
				logs += "\tJv_det_no->" + objJVDetails.getJv_det_no()
						+ "\tJv_no->" + objJVDetails.getJv_no()
						+ "\tAcc_code->" + objJVDetails.getAcc_code()
						+ "\tSub_acc_code->" + objJVDetails.getSub_acc_code()
						+ "\tDr_amt" + objJVDetails.getDr_amt() + "\tCr_amt->"
						+ objJVDetails.getCr_amt();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		log.add(logs);
		// if one-to-one entry
		if (drList.size() == crList.size() && drList.size() == 1) {
			objJVCr = crList.get(0);
			objJVDr = drList.get(0);

			/*
			 * Because of the double side effect of a transaction in the
			 * Account, we pass objJVDr once as a debit entity and next as a
			 * credit entity. Similar for the objJVCr too.
			 */
			status1 = setPostingAccount(connection, objJVDr, objJVCr, true,log);
			status2 = setPostingAccount(connection, objJVCr, objJVDr, true,log);

		}
		// if multiple Dr. entries against a single Cr. entry
		else if (drList.size() > crList.size()) {
			objJVCr = crList.get(0);
			for (int i = 0; i < drList.size(); i++) {
				objJVDr = drList.get(i);
				status1 = setPostingAccount(connection, objJVDr, objJVCr,
						false, log);
				status2 = setPostingAccount(connection, objJVCr, objJVDr,
						false, log);
			}
		}
		// if multiple Cr. entries against a single Dr. entry
		else {
			objJVDr = drList.get(0);
			for (int i = 0; i < crList.size(); i++) {
				objJVCr = crList.get(i);
				status1 = setPostingAccount(connection, objJVDr, objJVCr, true,log);
				status2 = setPostingAccount(connection, objJVCr, objJVDr, true,log);
			}
		}
		/*
		 * flushing out all items from drList and crList to ensure they do not
		 * have any prevously collected data before proceeding another conjugate
		 * of Multiple vs Multiple TRXNs being handled via Parking Account
		 * Method.
		 */
		drList.clear();
		crList.clear();

		if (status1 == status2) {
			double drSumLedger = (Double.valueOf(UtilFxn.GetSumValueFromTable(
					"ledger_mcg", "dr_amt", "jv_no=" + jvNo + "")));
			double crSumLedger = (Double.valueOf(UtilFxn.GetSumValueFromTable(
					"ledger_mcg", "cr_amt", "jv_no=" + jvNo + "")));
			double drSumJvDet = (Double.valueOf(UtilFxn.GetSumValueFromTable(
					"journal_voucher_details_mcg", "dr_amt", "jv_no=" + jvNo
							+ "")));
			double crSumJvDet = (Double.valueOf(UtilFxn.GetSumValueFromTable(
					"journal_voucher_details_mcg", "cr_amt", "jv_no=" + jvNo
							+ "")));
			if (drSumLedger == crSumLedger && drSumJvDet == crSumJvDet
					&& crSumLedger == drSumJvDet) {
				return true;
			} else {
				return false;
			}
		}

		else
			return false;
	}

	public boolean postToLedgerSaving(Connection connection, String jvNo) {

		/* Objects to hold a single Dr. and Cr. entry. */
		JVDetailsModel objJVDr = null;
		JVDetailsModel objJVCr = null;

		boolean status1 = false, status2 = false;

		// get the journal details
		setJournalDetails(connection, jvNo);

		// if one-to-one entry
		if (drList.size() == crList.size() && drList.size() == 1) {
			objJVCr = crList.get(0);
			objJVDr = drList.get(0);

			/*
			 * Because of the double side effect of a transaction in the
			 * Account, we pass objJVDr once as a debit entity and next as a
			 * credit entity. Similar for the objJVCr too.
			 */
			status1 = setPostingAccountSaving(connection, objJVDr, objJVCr,
					true);
			status2 = setPostingAccountSaving(connection, objJVCr, objJVDr,
					true);

		}
		// if multiple Dr. entries against a single Cr. entry
		else if (drList.size() > crList.size()) {
			objJVCr = crList.get(0);
			for (int i = 0; i < drList.size(); i++) {
				objJVDr = drList.get(i);
				status1 = setPostingAccountSaving(connection, objJVDr, objJVCr,
						false);
				status2 = setPostingAccountSaving(connection, objJVCr, objJVDr,
						false);
			}
		}
		// if multiple Cr. entries against a single Dr. entry
		else {
			objJVDr = drList.get(0);
			for (int i = 0; i < crList.size(); i++) {
				objJVCr = crList.get(i);
				status1 = setPostingAccountSaving(connection, objJVDr, objJVCr,
						true);
				status2 = setPostingAccountSaving(connection, objJVCr, objJVDr,
						true);
			}
		}
		if (status1 == status2)
			return true;
		else
			return false;
	}

	/**
	 * setJournalDetails() separates Dr. and Cr. entries of a single journal
	 * transaction.
	 * 
	 * If fetches all the detailed Dr. and Cr. entries of a journal entry. It
	 * doesn't return anything but saves each Dr. and Cr. entries of a journal
	 * transaction into an object of type JVDetails. Then each Dr. JVDetails
	 * objects are stored into the ArrayList holding Dr. JVDetails object (i.e.
	 * drList) and Cr. JVDetails objects are stored into the ArrayList holding
	 * Cr. JVDetails object (i.e. crList).
	 * 
	 * @param connection
	 *            (The db connection passed down from the setStatusApproved()
	 *            method in JournalListController class.)
	 * @param jvNo
	 *            (The journal voucher no. of which details are to be fetched.)
	 * @return void
	 */
	private void setJournalDetails(Connection connection, String jvNo) {
		JVDetailsModel objJVDetails;

		String strQry = "SELECT jv_det_no, jv_no, acc_code, sub_acc_code, dr_amt, cr_amt,"
				+ "narration, remarks "
				+ "FROM journal_voucher_details_mcg "
				+ "WHERE jv_no = " + jvNo;

		try {
			this.st = connection
					.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
							ResultSet.CONCUR_UPDATABLE);

			this.rs = st.executeQuery(strQry);

			while (rs.next()) {
				objJVDetails = new JVDetailsModel();
				objJVDetails.setJv_det_no(rs.getInt("jv_det_no"));
				objJVDetails.setJv_no(rs.getInt("jv_no"));
				objJVDetails.setAcc_code(rs.getString("acc_code"));
				objJVDetails.setSub_acc_code(rs.getString("sub_acc_code"));
				objJVDetails.setDr_amt(rs.getDouble("dr_amt"));
				objJVDetails.setCr_amt(rs.getDouble("cr_amt"));
				objJVDetails.setNarration(rs.getString("narration").replaceAll(
						"'", "''"));
				objJVDetails.setRemarks(rs.getString("remarks").replaceAll("'",
						"''"));
				objJVDetails.setBr_id(1);

				/*
				 * Separate Debit and Credit entries into ArrayLists of two
				 * types.
				 */
				if (Double.parseDouble((objJVDetails.getDr_amt())) == 0.0) {
					crList.add(objJVDetails);

				} else {
					drList.add(objJVDetails);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * setPostingAccount() to post the detailed journal entries (i.e. Dr. and
	 * Cr. entries) into the ledger table.
	 * 
	 * @param connection
	 *            (The db connection passed down from the setStatusApproved()
	 *            method in JournalListController class.)
	 * @param objJVDr
	 *            (Object of type objJVDr holding a single Dr. entry of any
	 *            selected journal transaction.)
	 * @param objJVCr
	 *            (Object of type objJVCr holding a single Cr. entry of any
	 *            selected journal transaction.)
	 * @param isReadAmt4Cr
	 *            (IF drList.size() <= crList.size(), THEN isReadAmt4mCr = true
	 *            ELSE isReadAmt4mCr = false
	 * 
	 *            In single Dr. entry vs. multiple Cr. entries, we have to
	 *            record the Cr. amount for the Dr. entry (although against the
	 *            accounting practice) for tracking the multiple Cr. entries of
	 *            the the single Dr. entry. Similarly, we in a single Cr. entry
	 *            vs. multiple Dr. entry, we have to record the Dr. amount for
	 *            the single Cr.entry.)
	 * @return boolean (true if successfully posted into the ledger table, false
	 *         otherwise.)
	 */
	private boolean setPostingAccount(Connection connection,
			JVDetailsModel objJVDr, JVDetailsModel objJVCr,
			boolean isReadAmt4mCr, ArrayList<String> log) {

		LedgerModel objLedger = new LedgerModel();
		String strQry = "";

		JCalendarFunctions jc = new JCalendarFunctions();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date curDate = jc
				.GetEnglishDate(jc.getCurrentNepaliDate());

		try {
			this.st = connection
					.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
							ResultSet.CONCUR_UPDATABLE);

			objLedger.setLed_id(UtilMethods.getMaxId("ledger_mcg", "led_id"));
			objLedger.setJv_no(objJVCr.getJv_no());
			objLedger.setTo_acc_code(objJVCr.getAcc_code());
			objLedger.setTo_sub_acc_code(objJVCr.getSub_acc_code());
			objLedger.setRemarks(objJVCr.getRemarks());
			objLedger.setAcc_code(objJVDr.getAcc_code());
			objLedger.setSub_acc_code(objJVDr.getSub_acc_code());
			objLedger.setUpdate_count(0);
			objLedger.setPosted_date(jc.currentDate());
			objLedger.setNepali_created_dt(jc.getCurrentNepaliDate());
			objLedger.setCreated_date(df.format(curDate));
			objLedger.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
			objLedger.setCreated_by(ApplicationWorkbenchWindowAdvisor
					.getLoginId());
			if (StringUtils.isBlank(objJVDr.getNarration())) {
				objLedger.setRemarks("");
			} else {
				objLedger.setRemarks(objJVDr.getNarration());
			}
			// added by suresh
			objLedger.setBr_id(String.valueOf(1));
			objLedger.setUpdated_by(String
					.valueOf(ApplicationWorkbenchWindowAdvisor.getLoginId()));
			objLedger.setUpdated_date(df.format(curDate));

			/*
			 * If drList.size() <= crList.size(), Then isReadAmt4mCr = true Else
			 * isReadAmt4mCr = false.
			 */
			if (isReadAmt4mCr) {
				objLedger.setDr_amt(Double.parseDouble(objJVCr.getCr_amt()));
				objLedger.setCr_amt(Double.parseDouble(objJVDr.getCr_amt()));
			} else {
				objLedger.setDr_amt(Double.parseDouble(objJVDr.getDr_amt()));
				objLedger.setCr_amt(Double.parseDouble(objJVCr.getDr_amt()));
			}

			/* Log */
			log.add("\n\t\t(Ledger):\tLed_id->" + objLedger.getLed_id()
					+ "\tJv_no->" + objLedger.getJv_no() + "\tTo_acc_code->"
					+ objLedger.getTo_acc_code() + "\tTo_sub_acc_code->"
					+ objLedger.getTo_sub_acc_code() + "\tRemarks->"
					+ objLedger.getRemarks() + "\tAcc_code->"
					+ objLedger.getAcc_code() + "\tSub_acc_code->"
					+ objLedger.getSub_acc_code());
			strQry = objLedger.Insert();

			if (st.executeUpdate(strQry) <= 1)
				return false;

			

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private boolean setPostingAccountSaving(Connection connection,
			JVDetailsModel objJVDr, JVDetailsModel objJVCr,
			boolean isReadAmt4mCr) {

		LedgerModel objLedger = new LedgerModel();
		String strQry = "";

		JCalendarFunctions jc = new JCalendarFunctions();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date curDate = jc
				.GetEnglishDate(jc.getCurrentNepaliDate());

		try {
			this.st = connection
					.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
							ResultSet.CONCUR_UPDATABLE);

			objLedger.setLed_id(UtilMethods.getMaxId("ledger_mcg", "led_id"));
			objLedger.setJv_no(objJVCr.getJv_no());
			objLedger.setTo_acc_code(objJVCr.getAcc_code());
			objLedger.setTo_sub_acc_code(objJVCr.getSub_acc_code());
			objLedger.setRemarks(objJVCr.getRemarks());
			objLedger.setAcc_code(objJVDr.getAcc_code());
			objLedger.setSub_acc_code(objJVDr.getSub_acc_code());
			objLedger.setUpdate_count(0);
			objLedger.setPosted_date(jc.currentDate());
			objLedger.setNepali_created_dt(jc.getCurrentNepaliDate());
			objLedger.setCreated_date(df.format(curDate));
			objLedger.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
			objLedger.setCreated_by(ApplicationWorkbenchWindowAdvisor
					.getLoginId());
			System.out.println("JV number:" + objJVCr.getJv_no());
			System.out.println("Account code number:" + objJVCr.getAcc_code());

			// String idd = UtilFxn.GetValueFromTable("ledger_mcg", "led_id",
			// "jv_no = '"+objJVCr.getJv_no()+"' and acc_code = '"+objJVDr.getAcc_code()+"' and to_acc_code = '"+objJVCr.getAcc_code()+"'");
			String idd = UtilFxn.GetValueFromTable("ledger_mcg", "led_id",
					"jv_no = '" + objJVCr.getJv_no() + "' and br_id = '"
							+ objJVCr.getBr_id() + "' and to_acc_code = '"
							+ objJVCr.getAcc_code() + "'");
			String idd1 = UtilFxn.GetValueFromTable("ledger_mcg", "led_id",
					"jv_no = '" + objJVCr.getJv_no() + "' and br_id = '"
							+ objJVCr.getBr_id() + "' and acc_code = '"
							+ objJVDr.getAcc_code() + "'");
			System.out.println("here is the idd:" + idd);
			if (!idd.equals("")) {
				objLedger.setLed_id(Integer.valueOf(idd));
			} else if (!idd1.equals("")) {
				objLedger.setLed_id(Integer.valueOf(idd1));
			} else {
				objLedger.setLed_id(UtilMethods.getMaxId("ledger_mcg", "led_id"));
			}
			if (StringUtils.isBlank(objJVDr.getNarration())) {
				objLedger.setRemarks("");
			} else {
				objLedger.setRemarks(objJVDr.getNarration());
			}
			// added by suresh
			objLedger.setBr_id(String.valueOf(1));
			objLedger.setUpdated_by(String
					.valueOf(ApplicationWorkbenchWindowAdvisor.getLoginId()));
			objLedger.setUpdated_date(df.format(curDate));

			/*
			 * If drList.size() <= crList.size(), Then isReadAmt4mCr = true Else
			 * isReadAmt4mCr = false.
			 */
			if (isReadAmt4mCr) {
				objLedger.setDr_amt(Double.parseDouble(objJVCr.getCr_amt()));
				objLedger.setCr_amt(Double.parseDouble(objJVDr.getCr_amt()));
			} else {
				objLedger.setDr_amt(Double.parseDouble(objJVDr.getDr_amt()));
				objLedger.setCr_amt(Double.parseDouble(objJVCr.getDr_amt()));
			}

			if (!objLedger.getDr_amt().equals("0.00"))
				if (UtilFxn.GetValueFromTable("ledger_mcg", "jv_no",
						"led_id = '" + objLedger.getLed_id() + "'").equals("")) {
					strQry = objLedger.Insert();
				} else {
					objLedger.z_WhereClause.led_id(objLedger.getLed_id());
					strQry = objLedger.Update();
				}

			if (st.executeUpdate(strQry) <= 1)
				return false;

			

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * Method to save the ledger details into the database.
	 * 
	 * @param existingCnn
	 *            (DB connection, but not the new one)
	 * @param accSet
	 *            (Arraylist holding the objects of type JVDetails)
	 * @param jvNo
	 *            (Journal voucher no.)
	 * @return boolean (true if success, false otherwise.)
	 */
	public boolean saveLedgerDetails(Connection existingCnn,
			ArrayList<JVDetailsModel> accSet, int jvNo) {

		String strQry = "";
		int maxId = UtilMethods.getMaxId("Ledger_mcg", "led_id");

		try {
			JCalendarFunctions jcal = new JCalendarFunctions();
			/* Date dt = new Date(); */
			SimpleDateFormat sdfSource = new SimpleDateFormat(
					"EEE MMM dd hh:mm:ss z yyyy");
			String strjvdate = jcal.currentDate();
			Date jvdate = sdfSource.parse(strjvdate);
			SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd");
			strjvdate = sdfDestination.format(jvdate);

			for (int i = 0; i < accSet.size(); i++) {
				objLedger.setLed_id(maxId++);
				objLedger.setJv_no(jvNo);
				objLedger.setAcc_code(accSet.get(i).getAcc_code());
				objLedger.setSub_acc_code(accSet.get(i).getSub_acc_code());
				// objLedger.setTo_acc_code(value)
				objLedger.setDr_amt(Double.parseDouble(accSet.get(i)
						.getDr_amt()));
				objLedger.setCr_amt(Double.parseDouble(accSet.get(i)
						.getCr_amt()));
				objLedger.setPosted_date(strjvdate);
				objLedger.setNepali_created_dt(jcal.GetNepaliDate(strjvdate));

				strQry = objLedger.Insert();

				Statement st = existingCnn
						.createStatement(ResultSet.TYPE_FORWARD_ONLY,
								ResultSet.CONCUR_UPDATABLE);

				if (st.executeUpdate(strQry) <= 1)
					return false;

				

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
			return false;
		} catch (ParseException e) {
		}
		return true;
	}

	/**
	 * Method to calculate the balance brought down amount.
	 * 
	 * @param accountCode
	 *            (Account code of the selected account head.)
	 * @param toDate
	 *            (Ending date)
	 * @param branchId
	 *            (Branch id of the firm.)
	 * @param fiscalYear
	 *            (Fiscal year.)
	 */
	public double getBalanceBD(String accountCode, String toDate, int branchId,
			int fiscalYear) {

		String strQry = " SELECT isnull((SUM(dr_amt)-SUM(cr_amt)),0.00) AS PrevBalance "
				+ " FROM ledger_mcg   "
				+ " WHERE acc_code = '"
				+ accountCode
				+ "'"
				+ " AND posted_date < '"
				+ toDate
				+ "'"
				+ " AND led_id >= (select min(led_id)"
				+ " from  ledger_mcg "
				+ " where  posted_date = (select posted_date "
				+ "	from  ledger_mcg "
				+ " 		where  led_id = (select max(led_id) from  ledger_mcg"
				+ "  			where jv_no = -1 and posted_date < '"
				+ toDate
				+ "'	))"
				+ "		and is_opening = 1" + " 	)";
		System.out.println("getBalanceBD query:	" + strQry);

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
			if (rs.next()) {

				/*
				 * System.out.println("Previous Balance:" +
				 * rs.getDouble("PrevBalance")); System.exit(0);
				 */

				return rs.getDouble("PrevBalance");
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
		return 0;
	}

	/**
	 * @param accountCode
	 * @return double opening balance of the given acc_code
	 * 
	 *         The method has been added to override above function after
	 *         introducing new field is_opening in ledger_mcg table.
	 */
	public double getBalanceBD(String accountCode) {

		String strQry = " SELECT (SUM(dr_amt)-SUM(cr_amt))PrevBalance FROM ledger_mcg "
				+ " WHERE posted_date=(SELECT MAX(posted_date) FROM ledger_mcg WHERE jv_no=-1 AND is_opening=1) "
				+ " AND jv_no=-1 	AND is_opening=1 AND acc_code='"
				+ accountCode + "'";
		System.out.println("getBalanceBD query NEW:	" + strQry);

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
			if (rs.next()) {
				return rs.getDouble("PrevBalance");
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
		return 0;
	}

	/**
	 * Method to calculate the balance carried down amount.
	 * 
	 * @param accountCode
	 *            (Account code of the account head.)
	 * @param fromDate
	 *            (Starting date.)
	 * @param toDate
	 *            (Ending date.)
	 * @param fiscalYear
	 *            (Fiscal year.)
	 */
	public double getBalanceCD(String accountCode, String fromDate,
			String toDate, int fiscalYear) {
		String strQry = "SELECT SUM(dr_amt)-SUM(cr_amt) AS balance_cd "
				+ "FROM ledger_mcg WHERE jv_no IN "
				+ "(SELECT jv_no FROM ledger_mcg WHERE acc_code='"
				+ accountCode + "' AND " + "(posted_date BETWEEN '" + fromDate
				+ "' AND '" + toDate + "') AND " + "fy_id=" + fiscalYear
				+ ") AND acc_code='" + accountCode + "'";
		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			rs = st.executeQuery(strQry);

			if (rs.next())
				return rs.getDouble("balance_cd");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Method to load the ledger table. The data retrieved the ledger are then
	 * returned as ResultSet.
	 * 
	 * @param accountCode
	 *            (Account code of the account head.)
	 * @param fromDate
	 *            (Starting date to specify the range.)
	 * @param toDate
	 *            (Ending date to specify the range.)
	 * @param fiscalYear
	 *            (Fiscal year)
	 */
	public ResultSet loadLedger(String accountCode, String fromDate,
			String toDate) {

		String strQry = "SELECT "
				+ " CONVERT(date,posted_date,101)posted_date, l.jv_no, to_acc_code, "
				+ "(SELECT acc_name FROM acc_head_mcg WHERE acc_code = to_acc_code) AS Particulars, "
				+ "l.dr_amt, l.cr_amt, a.alias, "
				+ "( case when l.remarks is null  then (SELECT distinct narration FROM journal_voucher_details_mcg AS j "
				+ " WHERE j.jv_no = l.jv_no AND l.to_acc_code = j.acc_code) else l.remarks end  "
				+ ") AS narration, "
				+ "l.nepali_created_dt "
				+ "FROM   "
				+ "	ledger_mcg as l INNER JOIN acc_head_mcg AS a ON a.acc_code = l.to_acc_code "
				+ "WHERE " + "	posted_date BETWEEN '" + fromDate + "' AND '"
				+ toDate + "' AND jv_no <> -1  AND fy_id= "
				+ ApplicationWorkbenchWindowAdvisor.getFiscalId()
				+ "  AND   l.acc_code='" + accountCode
				+ "' ORDER BY posted_date, led_id ASC ";

		System.out.println("Ledger loading query:	" + strQry);

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);

			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}// end of method

	public ResultSet loadLedgerSumary(String accountCode, String fromDate,
			String toDate) {

		String strQry = " with result as "
				+ " ( SELECT  CONVERT(date,posted_date,101)posted_date, '0' as jv_no, to_acc_code,"
				+ " (a.acc_name) AS Particulars, sum(l.dr_amt)dr_amt, sum(l.cr_amt)cr_amt, a.alias, "
				+ " ('' ) AS narration , l.nepali_created_dt FROM   	ledger_mcg as l "
				+ " INNER JOIN acc_head_mcg AS a ON a.acc_code = l.to_acc_code "
				+ " GROUP BY to_acc_code,posted_date,jv_no,l.acc_code,acc_name,alias,nepali_created_dt,fy_id "
				+ " HAVING 	posted_date BETWEEN '"
				+ fromDate
				+ "' AND '"
				+ toDate
				+ "' AND jv_no <> -1"
				+ " AND fy_id= "
				+ ApplicationWorkbenchWindowAdvisor.getFiscalId()
				+ " AND   l.acc_code='"
				+ accountCode
				+ "'	) "
				+ " SELECT "
				+ " '' posted_date,jv_no,to_acc_code,Particulars,SUM(dr_amt)dr_amt,SUM(cr_amt)cr_amt,alias,"
				+ " narration,nepali_created_dt from result "
				+ " GROUP BY to_acc_code,Particulars,posted_date,alias,jv_no,narration,nepali_created_dt ";

		System.out.println("Ledger SUmmary loading query:	" + strQry);

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);

			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * This method posts into the ledger table the opening balance amount of the
	 * newly created account head or the update of the existing account head.
	 * 
	 * @param accCode
	 *            (Account code of the account head.)
	 * @param subAccCode
	 *            (Subaccount code of the account head.)
	 * @param accType
	 *            (Account type of the account head.)
	 * @param d
	 *            (Opening balance amount.)
	 * @param existingCnn
	 * @param qryType
	 *            (Flag to keep track to the insert/update status of the query.)
	 * @return boolean (true if successfully saved into the ledger_mcg table,
	 *         false otherwise.)
	 * 
	 * */
	public boolean postOpeningBalance(String accCode, String subAccCode,
			String accType, String d, String remarks, Connection existingCnn,
			int qryType, ArrayList<String> log) {

		// find the previous closing date in reference with current day_kid date
		ClosingInfoModel cim = LoginSession
				.getClosingInfo(jcal.currentDate());

		String strQry = null;
		int led_id = UtilMethods.getMaxId("ledger_mcg", "led_id");
		int jv_no = -1;
		String logFileData = "";
		/*
		 * We can find previous closing date using
		 * LoginSession.getClosingInfo(refDate) as well as using realtime
		 * calculation as follows.
		 */
		String prevClosingDt = jcal.addSubractDaysFrmEngDate(
				ApplicationWorkbenchWindowAdvisor
						.getCurClosingPeriodStartMonthEnDt(), "minus", 1,
				"yyyy-MM-dd");
		String nepaliCareatedDate = jcal.GetNepaliDate(prevClosingDt);// get
																		// nepali-equivalent
																		// date
																		// of
		// prev-closing date
		/*
		 * There are two possible cases here. The user might be creating a new
		 * account head or the user might be updating the opening balance for an
		 * existing account head. If creating a new account head we need to use
		 * an INSERT query, else an UPDATE query.
		 */
		if (qryType == 1) {

			if (accType.equals("1") || accType.equals("2")) {
				strQry = "INSERT INTO ledger_mcg (led_id, jv_no, acc_code, sub_acc_code,"
						+ " dr_amt, cr_amt, posted_date, created_date, remarks, fy_id,   created_by,nepali_created_dt,update_count,br_id,updated_by,updated_date,is_opening)"
						+ "VALUES ("
						+ led_id
						+ ","
						+ jv_no
						+ ",'"
						+ accCode
						+ "','"
						+ subAccCode
						+ "',"
						+ d
						+ ", 0, '"
						+ prevClosingDt
						+ "','"
						+ cim.getPreviousClosingDate()
						+ "','"
						+ remarks
						+ "',"
						+ " '"
						+ ApplicationWorkbenchWindowAdvisor.getFiscalId()
						+ "',"
						+ " '"
						+ ApplicationWorkbenchWindowAdvisor.getLoginId()
						+ "',"
						+ " '"
						+ nepaliCareatedDate
						+ "',0,'"
						+ 1
						+ "','"
						+ ApplicationWorkbenchWindowAdvisor.getLoginId()
						+ "','"
						+ jcal.CurrentDay()
						+ "',1)";
			} else if (accType.equals("3") || accType.equals("4")) {
				strQry = "INSERT INTO ledger_mcg (led_id, jv_no, acc_code, sub_acc_code,"
						+ " cr_amt, dr_amt, posted_date, created_date, remarks, fy_id,   created_by,nepali_created_dt,update_count,br_id,updated_by,updated_date,is_opening)"
						+ "VALUES ("
						+ led_id
						+ ","
						+ jv_no
						+ ",'"
						+ accCode
						+ "','"
						+ subAccCode
						+ "',"
						+ d
						+ ", 0, '"
						+ prevClosingDt
						+ "','"
						+ cim.getPreviousClosingDate()
						+ "','"
						+ remarks
						+ "',"
						+ " '"
						+ ApplicationWorkbenchWindowAdvisor.getFiscalId()
						+ "',"
						+ " '"
						+ ApplicationWorkbenchWindowAdvisor.getLoginId()
						+ "',"
						+ " '"
						+ nepaliCareatedDate
						+ "',0,'"
						+ 1
						+ "','"
						+ ApplicationWorkbenchWindowAdvisor.getLoginId()
						+ "','"
						+ jcal.currentDate()
						+ "',1)";
			}

		}
		/*
		 * This should be an update query as in the commented code below. But
		 * since user might change the account type of the account head anytime,
		 * the update query will acutally be updating the previous record
		 * erroneously. So, we delete the previous record and insert it again.
		 */
		else {
			// these two local variables were added by suresh on 25Aug2011
			int updateCount = Integer.parseInt((UtilFxn
					.GetValueFromTable("ledger_mcg", "update_count",
							"jv_no = -1 AND acc_code ='" + accCode + "'")
					.toString().equals("") ? "0" : UtilFxn.GetValueFromTable(
					"ledger_mcg", "update_count",
					"jv_no = -1 AND acc_code ='" + accCode + "'").toString()));
			// String createdDt=UtilFxn.GetValueFromTable("ledger_mcg",
			// "created_date", "jv_no = -1 AND acc_code ='"+accCode+"'");
			System.out.println("zzz,, kk this trxn has update cnt="
					+ updateCount);
			String sqlDelete = "DELETE from ledger_mcg WHERE jv_no = -1 AND acc_code ='"
					+ accCode + "'";
			log.add("\n\t\t(Journal-Delete):\tAcc_code->" + accCode
					+ "\tJv_no->-1");

			try {
				st = existingCnn.createStatement();
				if (st.executeUpdate(sqlDelete) == 1
				// because only one row is deleted
						|| st.executeUpdate(sqlDelete) == 0) {

					

					if (accType.equals("1") || accType.equals("2")) {
						strQry = "INSERT INTO ledger_mcg (led_id, jv_no, acc_code, sub_acc_code,"
								+ " dr_amt, cr_amt, posted_date, created_date, remarks, fy_id,   created_by,nepali_created_dt,update_count,br_id,updated_by,updated_date,is_opening)"
								+ "VALUES ("
								+ led_id
								+ ","
								+ jv_no
								+ ",'"
								+ accCode
								+ "','"
								+ subAccCode
								+ "',"
								+ d
								+ ", 0, '"
								+ prevClosingDt
								+ "','"
								+ cim.getPreviousClosingDate()
								+ "','"
								+ remarks
								+ "',"
								+ "'"
								+ ApplicationWorkbenchWindowAdvisor
										.getFiscalId()
								+ "',"
								+ " '"
								+ ApplicationWorkbenchWindowAdvisor.getLoginId()
								+ "',"
								+ " '"
								+ nepaliCareatedDate
								+ "','"
								+ (updateCount + 1)
								+ "','"
								+ 1
								+ "',"
								+ "'"
								+ ApplicationWorkbenchWindowAdvisor.getLoginId()
								+ "','"
								+ jcal.currentDate() + "',1)";

					} else if (accType.equals("3") || accType.equals("4")) {
						strQry = "INSERT INTO ledger_mcg (led_id, jv_no, acc_code, sub_acc_code,"
								+ " cr_amt, dr_amt, posted_date, created_date, remarks, fy_id,   created_by,nepali_created_dt,update_count,br_id,updated_by,updated_date,is_opening)"
								+ "VALUES ("
								+ led_id
								+ ","
								+ jv_no
								+ ",'"
								+ accCode
								+ "','"
								+ subAccCode
								+ "',"
								+ d
								+ ", 0, '"
								+ prevClosingDt
								+ "','"
								+ cim.getPreviousClosingDate()
								+ "','"
								+ remarks
								+ "',"
								+ "'"
								+ ApplicationWorkbenchWindowAdvisor
										.getFiscalId()
								+ "',"
								+ " '"
								+ ApplicationWorkbenchWindowAdvisor.getLoginId()
								+ "',"
								+ " '"
								+ nepaliCareatedDate
								+ "','"
								+ (updateCount + 1)
								+ "','"
								+ 1
								+ "',"
								+ "'"
								+ ApplicationWorkbenchWindowAdvisor.getLoginId()
								+ "','"
								+ jcal.currentDate() + "',1)";

					}

				} else {
					return false;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		try {
			System.out.println("zz.. the Ledger posting qry = " + strQry);
			this.st = existingCnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);

			// if query is successful, return true
			System.out.println("The Ledger QRY= " + strQry);
			if (st.executeUpdate(strQry) == 1)
				 {

					logFileData = "\n\t\t(Journal):\tAccType" + accType
							+ "LedgerId->" + led_id + "\tjv_no->" + jv_no
							+ "\taccCode->" + accCode + "\tsubAccCode->"
							+ subAccCode + "\tDrAmt->" + d
							+ "\tCrAmt->0\tIsClosing->1";
					log.add(logFileData);
					// Reset back the isALdr flag to false for future reuse---
					// by suresh
					isALdr = false;
					return true;
				} else
					return false;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * @param cnn
	 * @param ledgerQuery
	 * @return boolean value method to post closing balance
	 */
	public boolean postQuarterlyBalance(Connection cnn, String ledgerQuery,ArrayList<String> log) {

		boolean retFlag = false;

		try {
			st = cnn.createStatement();

			this.st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);

			// if query is successful, return true
			if (st.executeUpdate(ledgerQuery) > 0)
				
					retFlag = true;
				else
					retFlag = false;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return retFlag;
	}

	/**
	 * @param cnn2
	 * @param drAmt
	 * @param crAmt
	 * @param postedDate
	 * @param profitRoot
	 * @param lossRoot
	 * @return boolean value method to post the closing Profit or Loss amount
	 */
	public boolean postQuarterlyPLAmt(Connection cnn2, Double drAmt,
			Double crAmt, String postedDate, String profitRoot, String lossRoot,ArrayList<String> log) {

		boolean retFlag = false;

		String strQry = null;
		int led_id = UtilMethods.getMaxId("ledger_mcg", "led_id");
		int jv_no = -1;
		String remarks = "Quarterly " + (drAmt > 0 ? "Loss" : "Profit")
				+ " Amount.";
		JCalendarFunctions jcal = new JCalendarFunctions();

		String nepaliDate = jcal.GetNepaliDate(postedDate);
		String logFileData = "";
		try {
			st = cnn.createStatement();

			strQry = "INSERT INTO ledger_mcg (led_id, jv_no, acc_code, "
					+ " dr_amt, cr_amt, posted_date, created_date, remarks, fy_id, nepali_created_dt, created_by, br_id,is_opening)"
					+ "VALUES ("
					+ led_id
					+ ","
					+ jv_no
					+ ",'"
					+ (drAmt > 0 ? lossRoot : profitRoot)
					+ "',"
					+ drAmt
					+ ", "
					+ crAmt
					+ ", '"
					+ postedDate
					+ "','"
					+ postedDate
					+ "','"
					+ remarks
					+ "',"
					+ ApplicationWorkbenchWindowAdvisor.getFiscalId()
					+ ", '"
					+ nepaliDate
					+ "', "
					+ ApplicationWorkbenchWindowAdvisor.getLoginId()
					+ ", "
					+ 1 + ",1)";

			this.st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);

			// if query is successful, return true
			if (st.executeUpdate(strQry) == 1)
				 {
					logFileData = "\t\t(Journal):\tLedgerId->" + led_id
							+ "\tjv_no->" + jv_no + "\taccCode->"
							+ (drAmt > 0 ? lossRoot : profitRoot) + "\tDrAmt->"
							+ drAmt + "\tCrAmt->" + crAmt;
					log.add(logFileData);
					retFlag = true;
				} else
					retFlag = false;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return retFlag;
	}

	/**
	 * @param date
	 * @return boolean value method to check whether or not the system closing
	 *         for this accounting period has been performed
	 */
	public boolean isClosingPerformed(String date) {
		boolean retFlag = false;
		String sql = "SELECT COUNT(*) AS total FROM ledger_mcg WHERE jv_no = -1 AND posted_date = '"
				+ date + "'";

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

			if (rs.next()) {
				if (rs.getInt("total") >= 1)
					retFlag = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retFlag;
	}

}// EOC
