package com.tia.account.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.tia.common.db.DbConnection;



/**
 * @author suresh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 * @altered & revised by suresh
 */
public class CashInOutController {
	Connection cnn;
	Statement statement;
	ResultSet resultset;

	public CashInOutController() {
		cnn = DbConnection.getConnection();

	}


	/**
	 * @param date
	 * @param cashRoot
	 * @param bankRoot
	 * @param isClosingBal
	 * @return double balance
	 * 
	 * This method calculates the summary balance of cash+bank child acc_heads
	 * till the date including condition that if the last closing/opening balances
	 * have to be or not to be included in calculation.
	 */
	public Double getBalanceBeforeFromDate(String date, String cashOrBank,boolean isClosingBal) {

		double balance = 0.00;
		String condition="";
		if(isClosingBal)
		{
			condition=" and led_id in (select led_id from ledger_mcg where jv_no=-1 and posted_date=(select max(posted_date) from ledger_mcg where jv_no=-1 and is_opening=1)) ";
		}
		else
		{
			condition=" AND led_id>=(select min(led_id) from ledger_mcg where jv_no=-1 and posted_date=(select max(posted_date) from ledger_mcg where jv_no=-1 and is_opening=1)) ";
		}
		String sql = "WITH W_RESULT AS (SELECT (SUM(l.dr_amt) - SUM(l.cr_amt))  AS amt "
			+ " FROM ledger_mcg AS l INNER JOIN acc_head_mcg AS a ON a.acc_code = l.acc_code "
			+ " WHERE l.posted_date "
			+ "< '"+ date+"' AND"
			+ " (a.acc_code LIKE '"+cashOrBank+"%') AND a.parent <> a.acc_head_id " 
			+ condition
			+ ") SELECT SUM(amt)AS balance FROM W_RESULT ";

		try {
			System.out.println("the Balance qry="+sql);
			statement = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);

			resultset = statement.executeQuery(sql);

			if (resultset.next())
				balance = resultset.getDouble("balance");

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return balance;
	}


	/**
	 * @param fromDate
	 * @param toDate
	 * @param cashOrBank
	 * @param isDebited
	 * @return resultset
	 * 
	 * This method finds headwise summary of those acc_head entries with their acc_code from ledger table who have
	 * performed conditional Dr. or Cr. operation on acc_heads of cash or Bank (according to condition
	 * found in @param cashOrBank at runtime) and returns the resultset made of such entries.
	 */
	public ResultSet getCashBankDetails(String fromDate, String toDate,String cashOrBank,boolean isDebited) {
		String conditionString="l.cr_amt>0";
		String doThis=" (SUM(cr_amt)-SUM(dr_amt))amt ";
		if (isDebited)
		{
			conditionString="l.dr_amt>0 ";
			doThis=" (SUM(dr_amt)-SUM(cr_amt))amt ";
		}

		String 	strQry = "SELECT l.to_acc_code acc_code,a.acc_name,"+doThis+"  FROM ledger_mcg l " +
		" inner join acc_head_mcg a on l.to_acc_code=a.acc_code AND "+conditionString+" " +
		" AND l.acc_code like '"+cashOrBank+"%' " +
		" AND (posted_date between '"+fromDate+"' AND '"+toDate+"') " +
		" group by l.to_acc_code,a.acc_name ";

		strQry=strQry + " ORDER BY  acc_code ";

		System.out.println(strQry);

		try {
			statement = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);

			resultset = statement.executeQuery(strQry);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return resultset;
	}//end of method
	/**
	 * @param date
	 * @param cashRoot
	 * @param bankRoot
	 * @param isBalBd
	 * @return resultset
	 * 
	 * method to get resultset of cash or Bank BalanceBF
	 */
	public ResultSet getCashBankBalBf(String date, String cashRoot,String bankRoot,boolean isBalBd) {
		String dateOperator=" < ";
		if(isBalBd)
		{
			dateOperator=" <= ";
		}
		String 	strQry = " SELECT * FROM  (SELECT a.acc_code, a.acc_name, a.acc_type, a.parent,a.alias, " +
		"(SELECT (SUM(dr_amt)-SUM(cr_amt)) 	FROM  ledger_mcg l " +
		" WHERE (l.acc_code like '"+cashRoot+"%' OR l.acc_code like '"+bankRoot+"%' ) and (l.acc_code LIKE a.acc_code + '.%' OR l.acc_code=a.acc_code) " +
		" AND (led_id >= (select min(led_id) from ledger_mcg where jv_no = -1 " +
		" 		and posted_date = (select cast(MAX(posted_date) as date)as lastClosingDate " +
		" 			 from ledger_mcg where jv_no=-1 and posted_date "+dateOperator+" '"+date+"' ))) " +
		" 		and posted_date "+dateOperator+" '"+date+"') AS amt    FROM acc_head_mcg AS a " +
		" 		) AS T1 WHERE (acc_code like '"+cashRoot+"%' OR acc_code like '"+bankRoot+"%' )" +
		//				" 	AND acc_code in (select acc_code from acc_head_mcg where acc_head_id not in (select distinct(parent) from acc_head_mcg) " +
		//				" 		and (acc_code like '"+cashRoot+".%' OR acc_code like '"+bankRoot+".%')) " +
		" 	ORDER BY acc_code 	 ";

		System.out.println("cashPlusBankBalBf Qry"+strQry);

		try {
			statement = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);

			resultset = statement.executeQuery(strQry);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return resultset;
	}//end of class

}//end of class
