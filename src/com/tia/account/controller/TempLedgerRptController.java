/**
 * 
 */
package com.tia.account.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.tia.account.model.TempLedgerRpt;
import com.tia.common.db.DbConnection;



/**
 * @author suresh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 */
public class TempLedgerRptController {
	static  Connection cnn=DbConnection.getConnection();
	ResultSet rs;
	static Statement st;
	String rptType1;
	String rptType2;

	public TempLedgerRptController() {		

	}//end of constructor

	/**
	 * @param tempLedgerRpt
	 * @return boolean value
	 * 
	 * This method takes an ArrayList of type TempLedgerRpt and inserts all values 
	 * from it into database table temp_ledger_mcg and returns true if all rows have
	 * been inserted successfully.
	 * 
	 * We do not need to save query of this method despite the fact that it contains DML
	 * query. This is because we invoke this method solely for purpose of report printing;
	 * No calculations are involved.
	 */
	public static  boolean insertTempLedger(ArrayList<TempLedgerRpt> tempLedgerRpt)
	{
		boolean check=false;
		for(int i=0;i<tempLedgerRpt.size();i++)
		{	
			try {
				st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_UPDATABLE);
				String strQry=" INSERT INTO [temp_ledger_mcg] ([id],[date],[jv_no],[alias],[particulars] ,[narration],[dr],[cr],[balance],[br_id],[rpt_order]) " +
				" VALUES (" +
				" '"+tempLedgerRpt.get(i).getId()+"','"+tempLedgerRpt.get(i).getDate()+"','"+tempLedgerRpt.get(i).getJv_no()+"','"+tempLedgerRpt.get(i).getAlias()+"' " +
				" , '"+tempLedgerRpt.get(i).getParticulars()+"','"+tempLedgerRpt.get(i).getNarration().replaceAll("'", "''")+"','"+tempLedgerRpt.get(i).getDr()+"','"+tempLedgerRpt.get(i).getCr()+"' " +
				" , '"+tempLedgerRpt.get(i).getBalance()+"','"+tempLedgerRpt.get(i).getBr_id()+"','"+tempLedgerRpt.get(i).getRpt_order()+"' " +
				")" ;

				//System.out.println("The query to save temp values into temp_ledger_mcg table is :" +strQry);
				int result = st.executeUpdate(strQry);
				if(result==1 )
				{
					check=true;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}//for ends
		return check;

	}//end of method insertTempLedger()

	/**
	 * @param id
	 * @param brId
	 * 
	 * This method deletes all pre-existing rows from temp_ledger_mcg table.
	 * In this method we do not need to save the sql query being used, because
	 * we invoke this method for viewing report and it does not contain any 
	 * calculation to be reflected into database tables.So, we do not need to 
	 * maintain any database transaction here.
	 */
	public static void deleteallTempRptData(int id,int brId)
	{
		String strQry=" DELETE FROM temp_ledger_mcg where id="+id+" and br_id="+brId+" ";
		try {
			st = cnn.createStatement();

			System.out.println("zzz.. The query to delete temp values of temp_ledger_mcg is :" +strQry);
			st.executeUpdate(strQry);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//end of method deleteallTempRptData()
}//end of class
