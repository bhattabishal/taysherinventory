package com.tia.account.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import com.ibm.icu.text.SimpleDateFormat;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.model.SubAccHeadModel;
import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilMethods;



/**
 * @author minesh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 * @altered & revised by suresh
 */
public class SubAccHeadController {
	private Connection cnn;
	private Statement st;
	private ResultSet rs;

	private ArrayList<SubAccHeadModel> subAccList;
	private JournalEntryController jeController;

	public SubAccHeadController() {
		this.cnn = DbConnection.getConnection();
		jeController = new JournalEntryController();
	}

	/**
	 * Checks whether an account entry is leaf or not.
	 * 
	 * @param accId
	 *            (Id of the account to be checked for leaf.)
	 * @return boolean (true if leaf, false otherwise.)
	 */
	public boolean checkNodeForLeaf(String accId) {
		boolean retFlag = false;

		String strQry = "SELECT COUNT(*) AS total FROM acc_head_mcg WHERE parent = '"
				+ accId + "'";

		try {

			this.st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			rs = st.executeQuery(strQry);

			while (rs.next()) {
				if (rs.getInt("total") <= 0)
					return (retFlag = true);
				else
					return retFlag;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return retFlag;
	}

	/**
	 * Gets the account code of the parent under which we are creating a
	 * sub-account head.
	 * 
	 * @param accId
	 *            (Id of the account head)
	 * @return Id (account code of the account head whose Id was passed)
	 * */
	public String getAccCode(String accId) {
		String strQry = "SELECT acc_code FROM acc_head_mcg WHERE acc_head_id="
				+ accId;
		try {
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			ResultSet rs = st.executeQuery(strQry);
			if (rs.next())
				return (rs.getString("acc_code"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Calls the sql server scalar-valued function max_acc_code by passing the
	 * function an accId of an account head and retrieves the new sub-account
	 * code for the account entry under the passed account head's id. If
	 * unsuccessful, returns null.
	 * 
	 * For parent level account heads, an accId of 0 is passed.
	 * 
	 * @param accId
	 *            (account id under which a new new entry is to be created, 0 if
	 *            parent level account is to be created.)
	 * @return accCode (account code of the newly created entry, null if
	 *         unsuccessful.)
	 * */
	public String getAutoSubAccCode(String accCode) {
		String strQry = "SELECT [skbbl].[dbo].[max_subacc_code] ('" + accCode
				+ "') AS subAccCode";

		try {
			this.st = cnn.createStatement();
			ResultSet rs = st.executeQuery(strQry);
			if (rs.next())
				return (rs.getString("subAccCode"));

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return null;

	}

	/**
	 * Checks for the existence of the provided sub-account alias. If found
	 * returns false, true otherwise.
	 * 
	 * @param accAlias
	 *            (newly entered alias for the sub-account)
	 * @return boolean (true if doesn't already exist, false otherwise)
	 */
	public boolean getSubAccAliasCount(String accAlias) {
		boolean flag = false;
		String strQry = "SELECT COUNT(sub_acc_head_id) AS alias_count FROM acc_sub_head_mcg "
				+ "WHERE alias='" + accAlias + "'";

		try {
			st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			rs = st.executeQuery(strQry);
			if (rs.next()) {
				if (rs.getInt("alias_count") <= 1)
					flag = true;

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * Saves the new sub-account head.
	 * 
	 * @param subAccHead
	 *            (An object of type subAccHead that holds the newly entered
	 *            data for subaccount head to be saved.)
	 * @param con
	 *            (Db connection with autocommit set to false.)
	 * @param empty
	 *            (Boolean value that determines if opening balance was entered
	 *            or not.)
	 * @param accType
	 *            (Account type for this subhead.)
	 * @return (true if success, false otherwise.)
	 */
	public boolean saveAccSubhead(SubAccHeadModel subAccHead, Connection con,
			boolean empty, String accType, ArrayList<String> log) {
		boolean retFlag = false;
		Statement stat = null;
		String subAccCode;
		String strQry = "";

		/*
		 * This variable keeps track of whether a user is trying to insert the
		 * opening balance for the first time or trying to update the opening
		 * balance which was previously set. qryType = 0 means neither insert
		 * nor update, 1 means insert, 2 means update.
		 */
		int qryType = 0;

		if (subAccHead.getSub_acc_head_id() <= 0) {
			subAccHead.setSub_acc_head_id(UtilMethods.getMaxId("acc_sub_head_mcg",
					"sub_acc_head_id"));
			subAccHead.setCreated_by(ApplicationWorkbenchWindowAdvisor
					.getLoginId());
			subAccHead.setCreated_date(new SimpleDateFormat("yyyy-MM-dd")
					.format(new Date()));
			subAccHead.setUpdate_count(0);
			strQry = subAccHead.Insert();
			qryType = 1;
			subAccCode = subAccHead.getSub_acc_code();
		} else {

			subAccHead.z_WhereClause.sub_acc_head_id((subAccHead
					.getSub_acc_head_id()));
			subAccHead.z_WhereClause.br_id(1);
			subAccHead.setUpdated_by(ApplicationWorkbenchWindowAdvisor
					.getLoginId());
			subAccHead.setUpdated_date(new SimpleDateFormat("yyyy-MM-dd")
					.format(new Date()));
			subAccHead.setUpdate_count(subAccHead.getUpdate_count() + 1);
			strQry = subAccHead.Update();
			qryType = 2;
			subAccCode = subAccHead.getSub_acc_code();
		}

		/* Log */
		log.add("\n\t\t(subAccHead):\tSub_acc_head_id->"
				+ subAccHead.getSub_acc_head_id() + "\tsubAccCode->"
				+ subAccCode);
		/*
		 * If opening balance is empty(true) i.e. user didn't enter the amount
		 * in the opening balance textbox, just insert account head details into
		 * the account_head_mcg table.
		 */
		if (empty) {

			try {
				stat = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_READ_ONLY);

				if (stat.executeUpdate(strQry) == 1
						)
					retFlag = true;

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		// user entered the opening balance amount
		else {

			try {
				/*
				 * Journal entry and ledger posting for a newly created
				 * subaccount head is a must if opening balance was specified.
				 * So after successfully creating a subaccount head, if journal
				 * entry and/or ledger posting fails for the newly created
				 * subaccount head, we must rollback the newly entered
				 * subaccount head.
				 */

				this.st = cnn.createStatement(
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_READ_ONLY);

				// if this query was successful, save into the journal
				if (st.executeUpdate(strQry) == 1
						) {

					if (jeController.saveOpeningBalance(
							subAccHead.getAcc_code(), subAccCode,
							subAccHead.getOpening_balance(), accType,
							subAccHead.getRemarks(), con, qryType, log)) {
						// journal entry is successful, commit the transaction
						this.cnn.commit();
						retFlag = true;

					} else {
						// journal entry is unsuccessful, rollback the
						// transaction
						this.cnn.rollback();
						retFlag = false;
					}
				} else {
					this.cnn.rollback();
					retFlag = false;
				}

			} catch (SQLException e) {
				e.printStackTrace();

			}

		}

		return retFlag;
	}

	/**
	 * Get the list of all the sub-accounts associated with a particular
	 * sub-account head so that the sub-accounts can be displayed properly in
	 * the table.
	 * 
	 * @param selIndex
	 *            (account code under selection)
	 * @return arraylist (arraylist holding the objects of type SubAccHeadModel)
	 */
	public ArrayList<SubAccHeadModel> getSubAccounts(String selIndex) {
		SubAccHeadModel subAcc;
		subAccList = new ArrayList<SubAccHeadModel>();
		String sqlAll = "SELECT sub_acc_head_id, br_id, sub_acc_name, alias, remarks, acc_code, sub_acc_code "
				+ "FROM acc_sub_head_mcg WHERE acc_code = '" + selIndex + "'";
		ResultSet rs = null;

		try {
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			rs = st.executeQuery(sqlAll);

			if (rs != null)
				while (rs.next()) {
					subAcc = new SubAccHeadModel();
					subAcc.setSub_acc_head_id(rs.getInt(1));
					subAcc.setBr_id(rs.getInt(2));
					subAcc.setSub_acc_name(rs.getString(3));
					subAcc.setAlias(rs.getString(4));
					subAcc.setRemarks(rs.getString(5));
					subAcc.setAcc_code(rs.getString(6));
					subAcc.setSub_acc_code(rs.getString(7));
					subAccList.add(subAcc);
				}
			else {
				return null;
			}
			return subAccList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return subAccList;
	}

	/**
	 * getSubAccList() gets all the details about a particular subaccount head.
	 * 
	 * @param strAccId
	 *            (Subaccount head id.)
	 * @return ResultSet (Result set containing all the information about a
	 *         particular subaccount head.)
	 */

	public ResultSet getSubAccList(String strAccId) {
		String strQry = "";

		strQry = "SELECT s.*, a.acc_code "
				+ "FROM acc_sub_head_mcg s INNER JOIN acc_head_mcg a ON s.acc_code = a.acc_code "
				+ "WHERE s.sub_acc_code = '" + strAccId + "'";

		try {
			this.st = this.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			return (st.executeQuery(strQry));

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * hasTransaction() checks whether a subaccount head is already involved in
	 * a transaction by checking its respective sub_acc_code in
	 * journal_voucher_details_mcg table. If found in the table, it returns
	 * true, false otherwise.
	 * 
	 * @param subAccCode
	 *            (SubAccount code of the subaccount head to be checked.)
	 * @return boolean (true if found, false otherwise.)
	 */
	public boolean hasTransaction(String subAccCode) {
		String strQry = "SELECT COUNT(*) AS totalTransaction FROM journal_voucher_details_mcg WHERE sub_acc_code ='"
				+ subAccCode + "'";

		try {
			this.st = this.cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			this.rs = this.st.executeQuery(strQry);

			while (this.rs.next())
				if (this.rs.getInt("totalTransaction") <= 0)
					return false;
				else
					return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return true;
	}

	/**
	 * deleteSubAccount() the subaccount from the database.
	 * 
	 * @param subAccCode
	 *            (Code of the account entry to be deleted)
	 * @return boolean (true if successful, else false)
	 * */
	public boolean deleteSubAccount(String subAccCode) {
		boolean result = false;
		SubAccHeadModel objModel = new SubAccHeadModel();
		objModel.z_WhereClause.sub_acc_code(subAccCode);

		String strQry = objModel.Delete();

		try {
			this.st = cnn.createStatement();

			if (st.executeUpdate(strQry) >= 1
					)
				result = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Fetches the account type of the newly created subaccount head.
	 * 
	 * @param accCode
	 *            (Account code of the subaccount head.)
	 * @return String(Account type.)
	 */
	public String getAccountType(String accCode) {
		String sql = "SELECT acc_type FROM acc_head_mcg WHERE acc_code = '"
				+ accCode + "'";
		String accType = null;

		try {
			st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

			rs.next();
			accType = rs.getString("acc_type");

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return accType;
	}

	/**
	 * Fetches the update_count of the passed sub-account head id. If the
	 * sub-account head id doesn't exist yet, a negative value is returned.
	 * 
	 * @param subAccHeadId
	 *            (Account head id.)
	 * @return int (Update count if exists, negative value otherwise.)
	 */
	public int getUpdateCount(int subAccHeadId) {
		int count = -1;
		String sql = "SELECT update_count FROM acc_head_mcg WHERE acc_head_id = '"
				+ subAccHeadId + "'";

		try {
			this.st = this.cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			this.rs = this.st.executeQuery(sql);

			if (rs.next())
				count = rs.getInt("update_count");

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return count;
	}

}// end of class