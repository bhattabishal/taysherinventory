package com.tia.account.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.db.DbConnection;
import com.tia.common.util.LogFileCreate;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;



/**
 * @author minesh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 * @altered & revised by suresh
 */
public class JournalListController {
	Connection cnn;
	Statement st;
	ResultSet rs;

	public JournalListController() {
		// db Connection
		this.cnn = DbConnection.getConnection();
	}

	/**
	 * getUnapprovedEntries() to retrieve the voucher numbers whose status is
	 * neither approved nor disapproved.
	 * 
	 * @param null
	 * @return ResultSet (List of journal entries with status set to 0)
	 */
	public ResultSet getUnapprovedEntries() {
		// String strQry =
		// "SELECT jv_no, jv_date FROM journal_voucher_mcg WHERE status <> 2 "
		// + "AND status <> 1 AND jv_no <> 0";
		String strQry = "SELECT jv_no, nepali_jv_date FROM journal_voucher_mcg WHERE status = 0 order by jv_no ";

		try {
			this.st = this.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			this.rs = st.executeQuery(strQry);
			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Retrieves the voucher number details whose status is neither approved nor
	 * disapproved from the database.
	 * 
	 * @param strJvNo
	 * @return ResultSet (Detailed Dr. and Cr. entries of a journal
	 *         transaction.)
	 */
	public ResultSet getJournalDetails(String strJvNo) {
		String strQry = "SELECT j.acc_code, a.acc_name, j.dr_amt, j.cr_amt, j.narration, a.alias FROM "
				+ "journal_voucher_details_mcg j "
				+ "INNER JOIN acc_head_mcg a ON a.acc_code = j.acc_code WHERE j.jv_no="
				+ strJvNo + " order by ABS(dr_amt) desc ";

		try {
			// System.out.println("xx .. the journal detail QRY=" + strQry);
			this.st = this.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			this.rs = st.executeQuery(strQry);
			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param acc_code
	 * @param today
	 * @return resultset
	 * 
	 *         This method is used only for DMS vouchers for selected
	 *         acc_head(PUSTI-PATRA)
	 */
	public ResultSet getJournalForSingleHead(String acc_code, String today) {
		String strQry = " WITH W_RESULT AS  "
				+ " ( SELECT a.acc_name, sum(j.dr_amt) AS dr_sum, sum(j.cr_amt) AS cr_sum, a.alias  FROM  journal_voucher_details_mcg AS j "
				+ " INNER JOIN  acc_head_mcg AS a ON a.acc_code = j.acc_code "
				+ " INNER JOIN  journal_voucher_mcg AS jv ON jv.jv_no = j.jv_no "
				+ " WHERE       jv.jv_date = '"
				+ today
				+ "' AND jv.status = 1 "
				+ " and jv.jv_no in(select distinct jv_no from journal_voucher_details_mcg jvdm where jvdm.acc_code='"
				+ acc_code
				+ "' ) "
				+ " GROUP BY    a.alias , a.acc_name )"
				+ " SELECT alias, acc_name, dr_sum, 0 AS cr_sum  FROM W_RESULT  WHERE dr_sum <> 0 "
				+ " UNION ALL  SELECT alias, acc_name, 0 AS dr_sum, cr_sum  FROM W_RESULT  WHERE cr_sum <> 0 ";

		try {
			// System.out.println("xx .. the journal detail QRY=" + strQry);
			this.st = this.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			this.rs = st.executeQuery(strQry);
			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * setDisapproval() to set the status of specified journal voucher to
	 * disapproved mode. Once the journal voucher is disapproved it cannot be
	 * further deleted or edited. The reason for disapproval is optional part.
	 * 
	 * @param strJvNo
	 *            (The journal voucher no. of the selected journal transaction.)
	 * @param strRemarks
	 *            (Reason for rejecting the journal entry.)
	 * @return boolean (true if successfully set the journal status to 2, false
	 *         otherwise.)
	 */
	public boolean setDisapproval(String strJvNo, String strRemarks) {
		String strQry = "UPDATE journal_voucher_mcg SET status = 2, remarks='"
				+ strRemarks + "' ,updated_by='"
				+ ApplicationWorkbenchWindowAdvisor.getLoginId()
				+ "',update_count=update_count+1, " + " updated_date='"
				+ JCalendarFunctions.currentDate()
				+ "' WHERE jv_no=" + strJvNo;

		try {
			this.st = this.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			if (st.executeUpdate(strQry) == 1
					)
				return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * @param trxn_ses_id
	 * @param strRemarks
	 * @return boolean
	 * 
	 *         This method is invoked when the rejection is related to TRXN of
	 *         type Multiple vs. Multiple handled via Parking Account concept.
	 * 
	 */
	public boolean setDisapprovalViaParking(String tran_id,
			String to_from_type, String strRemarks) {
		String strQry = "UPDATE journal_voucher_mcg SET status = 2, remarks='"
				+ strRemarks + "' ,updated_by='"
				+ ApplicationWorkbenchWindowAdvisor.getLoginId()
				+ "',update_count=update_count+1, " + " updated_date='"
				+ JCalendarFunctions.currentDate()
				+ "' WHERE tran_id='" + tran_id + "' AND to_from_type='"
				+ to_from_type + "' ";
		try {
			this.st = this.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			if (st.executeUpdate(strQry) >= 1
					)
				return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * setStatusApproved() sets the journal entry status to 1 which means a
	 * particular journal entry is approved finally. An approved journal entry
	 * is also posted into the ledger table for further processing.
	 * 
	 * While posting the ledger, the db connection passed is the current
	 * instance of the db connection used to change the journal entry status to
	 * Approved.
	 * 
	 * @param jvNo
	 *            (Journal voucher no. of the journal entry to be approved.)
	 * @return boolean (true if successful, false otherwise.)
	 */
	public boolean setStatusApproved(String jvNo, ArrayList<String> log) {
		String strQry = "UPDATE journal_voucher_mcg SET status = 1,posted_by='"
				+ ApplicationWorkbenchWindowAdvisor.getLoginId()
				+ "',approved_by='"
				+ ApplicationWorkbenchWindowAdvisor.getLoginId() + "', "
				+ " updated_by='"
				+ ApplicationWorkbenchWindowAdvisor.getLoginId()
				+ "',update_count=update_count+1, " + " updated_date='"
				+ JCalendarFunctions.currentDate()
				+ "' WHERE jv_no in (" + jvNo + ") AND status=0";

		try {
			this.cnn.setAutoCommit(false);

			try {
				this.st = this.cnn.createStatement(
						ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_UPDATABLE);

				if (st.executeUpdate(strQry) <= 0)
					return false;

				

				LedgerController objLedgerController = new LedgerController();

				/*
				 * Pass the current connection and jvNo and if successful,
				 * commit the connection.
				 */
				if (objLedgerController.postToLedger(this.cnn, jvNo, log)) {
					LogFileCreate.logFileCreater(cnn, "Account",
							"JV-Mnl-Voucher", "Save", log);
					this.cnn.commit();
				} else {
					this.cnn.rollback();
					this.cnn.setAutoCommit(true);
					return false;
				}

			} catch (SQLException e) {
				e.printStackTrace();
				this.cnn.rollback();
				this.cnn.setAutoCommit(true);
				return false;
			}

			return true;

		} catch (SQLException e) {

			e.printStackTrace();
			return false;
		}
	}

	/**
	 * getMapAccountCode() to get the account code from acc_fxn_mapping_mcg for
	 * auto mated journal posting. for example for share purchase transaction.
	 * 
	 * @param accName
	 *            (Account name for which account code is to be mapped.)
	 * @return String (The mapped account code from acc_fxn_mapping_mcg table
	 *         which is mapped with acc_head_mcg.)
	 */
	public String getMapAccountCode(String accName) {
		String accCode = null;
		try {
			String strQry = "SELECT acc_code FROM acc_fxn_mapping_mcg WHERE map_for='"
					+ accName
					+ "' "
					+ "OR map_for='"
					+ accName.toLowerCase()
					+ "'";
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
			rs.next();
			accCode = rs.getString("acc_code");

			return accCode;

		} catch (SQLException sqEx) {
			sqEx.printStackTrace();

		}

		return accCode;
	}

	/**
	 * Gets the journal entry details either i)for a specific(single/only one)
	 * journal entry if single param is true. or ii) for all journals if single
	 * params is false.
	 * 
	 * @param jvNo
	 *            (Journal voucher no. whose details are to be viewed.)
	 * @param single
	 *            (boolean value to indicate whether a single journal entry is
	 *            to be viewed or not.)
	 * @return ResultSet(ResultSet containing the details of the particular
	 *         journal entry.)
	 */
	public ResultSet getJournalEntries(int jvNo, boolean single) {
		String sql = null;

		if (single)
			sql = "SELECT jv_no, nepali_jv_date FROM journal_voucher_mcg WHERE jv_no = '"
					+ jvNo + "'";
		else
			sql = "SELECT jv_no, nepali_jv_date FROM journal_voucher_mcg";

		System.out.println(sql);

		try {
			System.out
					.println("kkk ... getJournalEntries method of jListControler's QRY="
							+ sql);
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			rs = st.executeQuery(sql + " order by jv_no asc");

		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (rs != null)
			return rs;
		else
			return null;
	}

	/**
	 * Gets the journal entry details either i)for a specific journal entry if
	 * single param is true. or ii) for all journals if single params is false.
	 * 
	 * @param accCode
	 *            (Account code of the journal entries whose details are to be
	 *            viewed.)
	 * @param single
	 *            (boolean value to indicate whether a single journal entry is
	 *            to be viewed or not.)
	 * @param dateSpecified
	 *            (boolean value to indicate whether date was specified or not.)
	 * @param date
	 *            (Transaction date.)
	 * @return ResultSet(ResultSet containing the details of the particular
	 *         journal entry.)
	 */
	public ResultSet getJournalsByAccountCode(String accCode, boolean single,
			boolean dateSpecified, String date) {
		String sql = null;

		if (single) {
			sql = "SELECT DISTINCT jvm.jv_no, nepali_jv_date FROM journal_voucher_mcg AS jvm "
					+ "INNER JOIN journal_voucher_details_mcg AS jvdm ON jvm.jv_no = jvdm.jv_no"
					+ " WHERE jvm.status=1 and jvdm.acc_code = '"
					+ accCode
					+ "'";

			if (dateSpecified)
				sql += " AND jvm.jv_date = '" + date + "'";

		} else
			sql = "SELECT jv_no, nepali_jv_date FROM journal_voucher_mcg where status=1";

		try {
			System.out.println("zzz... the getJournalsByAccCode method's QRY= "
					+ sql);
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			rs = st.executeQuery(sql + " order by jv_no asc ");// this order by
			// statement was
			// later added
			// by suresh

		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (rs != null)
			return rs;
		else
			return null;
	}

	/**
	 * Fetches the journal entries between the selected dates.
	 * 
	 * @param fromDate
	 *            (Starting date after which the journal entries are to be
	 *            viewed.)
	 * @param single
	 *            (Ending date before which the journal entries are to be
	 *            viewed.)
	 * @return ResultSet(Resultset consisting of the journal entries betweent
	 *         the supplied dates.)
	 */
	public ResultSet getJournalsByDate(String fromDate, String toDate) {
		String sql = "SELECT jv_no, nepali_jv_date FROM journal_voucher_mcg WHERE status=1 and jv_date 	BETWEEN '"
				+ fromDate + "' AND '" + toDate + "' order by jv_no asc";

		try {
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			rs = st.executeQuery(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (rs != null)
			return rs;
		else
			return null;
	}

	public ResultSet getUserWiseJournalsByDate(String fromDate, String toDate,
			String UID) {
		String sql = " SELECT jvdm.jv_no, jvm.nepali_jv_date,ahm.alias,ahm.acc_name particular,jvdm.dr_amt,jvdm.cr_amt,isnull(jvdm.narration,'')remarks FROM journal_voucher_details_mcg jvdm"
				+ " INNER JOIN journal_voucher_mcg jvm on jvdm.jv_no=jvm.jv_no "
				+ " INNER JOIN acc_head_mcg ahm on jvdm.acc_code=ahm.acc_code "
				+ " WHERE jvm.jv_date 	BETWEEN '"
				+ fromDate
				+ "' AND '"
				+ toDate
				+ "' AND jvdm.created_by='"
				+ UID
				+ "' order by jv_no asc";

		try {
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			rs = st.executeQuery(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (rs != null)
			return rs;
		else
			return null;
	}

	public ResultSet getRVJournals(String fromDate, String toDate) {
		String sql = "SELECT jv_no, nepali_jv_date FROM journal_voucher_mcg WHERE rv_flag=1 AND jv_date 	BETWEEN '"
				+ fromDate + "' AND '" + toDate + "' order by jv_no asc";

		try {
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			rs = st.executeQuery(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (rs != null)
			return rs;
		else
			return null;
	}

	/**
	 * Fetches the summary journal entries of the user selected account heads.
	 * There are four types of selection: i) All account heads ii) All account
	 * heads except loan iii) Loan collection realted account heads only. And
	 * iv) Loan investments related account heads.
	 * 
	 * @param today
	 *            (The date of which the transactions are to be viewed.)
	 * @param all
	 *            (If true, fetch summary of all account heads.)
	 * @param exceptLoan
	 *            (If true, fetch summary of all the account heads except loan
	 *            related.)
	 * @param loanCollection
	 *            (If true, fetch summary of all the account heads related to
	 *            the loan collection only.)
	 * @param loanInvestment
	 *            (If true, fetch summary of all the account heads related to
	 *            the loan investment only.)
	 * @return ResultSet(Resultset consisting of the journal entries.)
	 */
	public ResultSet getSummaryJournalDetails(String today, boolean all,
			boolean exceptLoan, boolean loanCollection, boolean loanInvestment) {

		String sql = "";

		if (all)
			sql = " WITH W_RESULT AS ( "
					+ "SELECT a.acc_name, sum(j.dr_amt) AS dr_sum, sum(j.cr_amt) AS cr_sum, a.alias "
					+ " FROM  journal_voucher_details_mcg AS j "
					+ " INNER JOIN  acc_head_mcg AS a ON a.acc_code = j.acc_code "
					+ " INNER JOIN  journal_voucher_mcg AS jv ON jv.jv_no = j.jv_no "
					+ " WHERE       jv.jv_date = '" + today
					+ "' AND jv.status = 1 "
					+ " GROUP BY    a.acc_name, a.alias ) "
					+ " SELECT alias, acc_name, dr_sum, 0 AS cr_sum "
					+ " FROM W_RESULT " + " WHERE dr_sum <> 0 " + " UNION ALL "
					+ " SELECT alias, acc_name, 0 AS dr_sum, cr_sum "
					+ " FROM W_RESULT " + " WHERE cr_sum <> 0 ";/*
																 * +
																 * " UNION ALL "
																 * +
																 * " SELECT '' , 'Total', SUM(dr_sum), SUM(cr_sum) "
																 * +
																 * " FROM W_RESULT "
																 * ;
																 */

		else if (exceptLoan)
			sql = "WITH W_RESULT AS ( "
					+ "	SELECT "
					+ " 	a.acc_name, sum(j.dr_amt) AS dr_sum, sum(j.cr_amt) AS cr_sum, a.alias "
					+ "FROM  " + "	journal_voucher_details_mcg AS j "
					+ "INNER JOIN  "
					+ "	acc_head_mcg AS a ON a.acc_code = j.acc_code "
					+ " INNER JOIN  "
					+ " journal_voucher_mcg AS jv ON jv.jv_no = j.jv_no "
					+ " WHERE       " + "	jv.jv_date = '"
					+ today
					+ "' AND jv.status = 1 "
					+ " AND "
					+ "	jv.jv_no not IN "
					+ "	(SELECT	DISTINCT "
					+ "		jvdm.jv_no FROM journal_voucher_details_mcg jvdm "
					+ "	 INNER JOIN "
					+ "		acc_head_mcg a ON a.acc_code = jvdm.acc_code "
					+ "	 WHERE "
					+ "	(((a.acc_code in (select distinct prn_amt_cr from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id ) ) "

					+ " OR  "
					+ "(a.acc_code in (select distinct cur_int_cr from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id )) "
					+ " OR  "
					+ "(a.acc_code in (select distinct kb_int_cr1 from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id ) ) "
					+ " OR  "
					+ "(a.acc_code in (select distinct kb_int_cr2 from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id ) ) "
					+ " OR  "
					+ "(a.acc_code in (select distinct resch_int_cr1 from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id )  ) "
					+ " OR  "
					+ "(a.acc_code in (select distinct resch_int_cr2 from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id ) ) ) AND jvdm.cr_amt>0 )"

					+ " OR "
					+ " (((a.acc_code in (select distinct discount_dr from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id ) )"
					+ " OR "
					+ " (a.acc_code in (select distinct resch_int_dr2 from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id )  )"
					+ " OR "
					+ " (a.acc_code in (select distinct acc_code from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id ) )"
					+ " OR "
					+ " (a.acc_code in (select distinct kb_int_Dr2 from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id )))  AND jvdm.dr_amt>0 )"

					+ ") GROUP BY  "
					+ "	a.acc_name, j.acc_code, a.alias "
					+ " ) "
					+ " SELECT alias, acc_name, dr_sum, 0 AS cr_sum  FROM W_RESULT  WHERE dr_sum <> 0 "
					+ " UNION ALL  "
					+ " SELECT alias, acc_name, 0 AS dr_sum, cr_sum  FROM W_RESULT  WHERE cr_sum <> 0 ";
		/*
		 * sql = " WITH W_RESULT AS ( " +
		 * "SELECT a.acc_name, sum(j.dr_amt) AS dr_sum, sum(j.cr_amt) AS cr_sum, a.alias "
		 * + " FROM  journal_voucher_details_mcg AS j " +
		 * " INNER JOIN  acc_head_mcg AS a ON a.acc_code = j.acc_code " +
		 * " INNER JOIN  journal_voucher_mcg AS jv ON jv.jv_no = j.jv_no " +
		 * " WHERE       jv.jv_date = '" + today + "' AND jv.status = 1 " +
		 * "	AND j.acc_code NOT IN (" +
		 * " (select distinct acc_code as accCode from loan_request_mcg lrm  " +
		 * " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id ) union "
		 * + "select distinct prn_amt_cr as accCode from loan_request_mcg lrm" +
		 * " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union " +
		 * " select distinct cur_int_cr as accCode from loan_request_mcg lrm" +
		 * " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union" +
		 * " select distinct kb_int_cr1 as accCode from loan_request_mcg lrm" +
		 * " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union" +
		 * " select distinct kb_int_cr2 as accCode from loan_request_mcg lrm" +
		 * " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union" +
		 * " select distinct resch_int_cr1 as accCode from loan_request_mcg lrm"
		 * + " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union" +
		 * " select distinct resch_int_cr2 as accCode from loan_request_mcg lrm"
		 * + " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union" +
		 * " select distinct discount_dr as accCode from loan_request_mcg lrm" +
		 * " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union" +
		 * " select distinct resch_int_dr2 as accCode from loan_request_mcg lrm"
		 * + " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union" +
		 * " select distinct kb_int_Dr2 as accCode from loan_request_mcg lrm" +
		 * " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id )"
		 * 
		 * + " GROUP BY    a.acc_name, a.alias ) " +
		 * " SELECT alias, acc_name, dr_sum, 0 AS cr_sum " + " FROM W_RESULT " +
		 * " WHERE dr_sum <> 0 " + " UNION ALL " +
		 * " SELECT alias, acc_name, 0 AS dr_sum, cr_sum " + " FROM W_RESULT " +
		 * " WHERE cr_sum <> 0 ";
		 */

		/*
		 * sql = "WITH W_RESULT AS ( " + "	SELECT " +
		 * " 	a.acc_name, sum(j.dr_amt) AS dr_sum, sum(j.cr_amt) AS cr_sum, a.alias "
		 * + "FROM  " + "	journal_voucher_details_mcg AS j " + "INNER JOIN  " +
		 * "	acc_head_mcg AS a ON a.acc_code = j.acc_code " + " INNER JOIN  " +
		 * " journal_voucher_mcg AS jv ON jv.jv_no = j.jv_no " + " WHERE       "
		 * + "	jv.jv_date = '" + today + "' AND jv.status = 1 " + " AND " +
		 * "	jv.jv_no NOT IN " + "	(SELECT	DISTINCT " +
		 * "		jvdm.jv_no FROM journal_voucher_details_mcg jvdm " +
		 * "	 INNER JOIN " + "		acc_head_mcg a ON a.acc_code = jvdm.acc_code " +
		 * "	 WHERE " + "	jvdm.acc_code NOT IN (" +
		 * " select distinct prn_amt_cr as accCode from loan_request_mcg lrm" +
		 * " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union " +
		 * " select distinct cur_int_cr as accCode from loan_request_mcg lrm" +
		 * " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union" +
		 * " select distinct kb_int_cr1 as accCode from loan_request_mcg lrm" +
		 * " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union" +
		 * " select distinct kb_int_cr2 as accCode from loan_request_mcg lrm" +
		 * " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union" +
		 * " select distinct resch_int_cr1 as accCode from loan_request_mcg lrm"
		 * + " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union" +
		 * " select distinct resch_int_cr2 as accCode from loan_request_mcg lrm"
		 * + " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union" +
		 * " select distinct discount_dr as accCode from loan_request_mcg lrm" +
		 * " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union" +
		 * " select distinct resch_int_dr2 as accCode from loan_request_mcg lrm"
		 * + " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id union" +
		 * " select distinct kb_int_Dr2 as accCode from loan_request_mcg lrm" +
		 * " inner join" +
		 * " loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id"
		 * 
		 * + ") ) GROUP BY  " + "	a.acc_name, j.acc_code, a.alias " + " ) " +
		 * " SELECT alias, acc_name, dr_sum, 0 AS cr_sum  FROM W_RESULT  WHERE dr_sum <> 0 "
		 * + " UNION ALL  " +
		 * " SELECT alias, acc_name, 0 AS dr_sum, cr_sum  FROM W_RESULT  WHERE cr_sum <> 0 "
		 * ;
		 */
		/*
		 * + " UNION ALL  " +
		 * " SELECT '' , 'Total', SUM(dr_sum), SUM(cr_sum)  FROM W_RESULT"
		 */

		else if (loanCollection)
			sql = "WITH W_RESULT AS ( "
					+ "	SELECT "
					+ " 	a.acc_name, sum(j.dr_amt) AS dr_sum, sum(j.cr_amt) AS cr_sum, a.alias "
					+ "FROM  " + "	journal_voucher_details_mcg AS j "
					+ "INNER JOIN  "
					+ "	acc_head_mcg AS a ON a.acc_code = j.acc_code "
					+ " INNER JOIN  "
					+ " journal_voucher_mcg AS jv ON jv.jv_no = j.jv_no "
					+ " WHERE       " + "	jv.jv_date = '"
					+ today
					+ "' AND jv.status = 1 "
					+ " AND "
					+ "	jv.jv_no IN "
					+ "	(SELECT	DISTINCT "
					+ "		jvdm.jv_no FROM journal_voucher_details_mcg jvdm "
					+ "	 INNER JOIN "
					+ "		acc_head_mcg a ON a.acc_code = jvdm.acc_code "
					+ "	 WHERE "
					+ "	(((a.acc_code in (select distinct prn_amt_cr from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id ) ) "

					+ " OR  "
					+ "(a.acc_code in (select distinct cur_int_cr from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id )) "
					+ " OR  "
					+ "(a.acc_code in (select distinct kb_int_cr1 from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id ) ) "
					+ " OR  "
					+ "(a.acc_code in (select distinct kb_int_cr2 from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id ) ) "
					+ " OR  "
					+ "(a.acc_code in (select distinct resch_int_cr1 from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id )  ) "
					+ " OR  "
					+ "(a.acc_code in (select distinct resch_int_cr2 from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id ) ) ) AND jvdm.cr_amt>0 )"

					+ " OR "
					+ " (((a.acc_code in (select distinct discount_dr from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id ) )"
					+ " OR "
					+ " (a.acc_code in (select distinct resch_int_dr2 from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id )  )"
					+ " OR "
					+ " (a.acc_code in (select distinct kb_int_Dr2 from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id )))  AND jvdm.dr_amt>0 )"

					+ ") GROUP BY  "
					+ "	a.acc_name, j.acc_code, a.alias "
					+ " ) "
					+ " SELECT alias, acc_name, dr_sum, 0 AS cr_sum  FROM W_RESULT  WHERE dr_sum <> 0 "
					+ " UNION ALL  "
					+ " SELECT alias, acc_name, 0 AS dr_sum, cr_sum  FROM W_RESULT  WHERE cr_sum <> 0 ";

		else
			sql = "WITH W_RESULT AS ( "
					+ "	SELECT "
					+ " 	a.acc_name, sum(j.dr_amt) AS dr_sum, sum(j.cr_amt) AS cr_sum, a.alias "
					+ "FROM  "
					+ "	journal_voucher_details_mcg AS j "
					+ "INNER JOIN  "
					+ "	acc_head_mcg AS a ON a.acc_code = j.acc_code "
					+ " INNER JOIN  "
					+ " journal_voucher_mcg AS jv ON jv.jv_no = j.jv_no "
					+ " WHERE       "
					+ "	jv.jv_date = '"
					+ today
					+ "' AND jv.status = 1 "
					+ " AND "
					+ "	jv.jv_no IN "
					+ "	(SELECT	DISTINCT "
					+ "		jvdm.jv_no FROM journal_voucher_details_mcg jvdm "
					+ "	 INNER JOIN "
					+ "		acc_head_mcg a ON a.acc_code = jvdm.acc_code "
					+ "	 WHERE "
					+ "		jvdm.acc_code in (select distinct acc_code from loan_request_mcg lrm "
					+ " inner join loan_newscheme_mcg lnm on lrm.loan_objective=lnm.s_id "
					+ " )  AND jvdm.dr_amt >= 0 AND jvdm.cr_amt <= 0) "
					+ " GROUP BY  "
					+ "	a.acc_name, j.acc_code, a.alias "
					+ " ) "
					+ " SELECT alias, acc_name, dr_sum, 0 AS cr_sum  FROM W_RESULT  WHERE dr_sum <> 0 "
					+ " UNION ALL  "
					+ " SELECT alias, acc_name, 0 AS dr_sum, cr_sum  FROM W_RESULT  WHERE cr_sum <> 0 ";

		try {
			System.out
					.println("kxxx... the method getSummaryJournalDetails's QRY ="
							+ sql);
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			rs = st.executeQuery(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return rs;
	}

	/*
	 * public ResultSet getRelatedJvNoOfMultipleTrxn(int trxn_sesID) { String
	 * accCode = null; try { String strQry =
	 * "select distinct(jv_no) from journal_voucher_mcg where trxn_ses_id="
	 * +trxn_sesID+""; st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
	 * ResultSet.CONCUR_UPDATABLE); rs = st.executeQuery(strQry);
	 * 
	 * return rs;
	 * 
	 * } catch (SQLException sqEx) { sqEx.printStackTrace();
	 * 
	 * }
	 * 
	 * return rs; }//end of method
	 */
	public boolean setStatusApprovedParkingTrxn(String tran_id,
			String to_from_type, ArrayList<String> log) {
		String strQry = "UPDATE journal_voucher_mcg SET status = 1,posted_by='"
				+ ApplicationWorkbenchWindowAdvisor.getLoginId()
				+ "',approved_by='"
				+ ApplicationWorkbenchWindowAdvisor.getLoginId() + "', "
				+ " updated_by='"
				+ ApplicationWorkbenchWindowAdvisor.getLoginId()
				+ "',update_count=update_count+1, " + " updated_date='"
				+ JCalendarFunctions.currentDate()
				+ "' WHERE tran_id ='" + tran_id + "' AND to_from_type='"
				+ to_from_type + "'";
		try {
			this.cnn.setAutoCommit(false);

			try {
				this.st = this.cnn.createStatement(
						ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_UPDATABLE);

				if (st.executeUpdate(strQry) <= 0)
					return false;

				

				LedgerController objLedgerController = new LedgerController();

				// Pass the current connection and jvNo and if successful,
				// commit the connection.
				List<Integer> jvList = getAllJvNoForParkingTrxn(cnn, tran_id,
						to_from_type);
				for (int i = 0; i < jvList.size(); i++) {
					String jvNo = Arrays.asList(jvList.get(i)).get(0)
							.toString();
					if (objLedgerController.postToLedger(this.cnn, jvNo, log))
						if (i == jvList.size() - 1) {
							this.cnn.commit();// commit the connection now
						}
				}// for ends

			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}

			return true;

		} catch (SQLException e) {

			e.printStackTrace();
			return false;
		}
	}// end of method

	/**
	 * @param cn
	 * @param tran_id
	 * @param to_from_type
	 * @return collection method to get all jv_no related to one parking trxn
	 */
	private List<Integer> getAllJvNoForParkingTrxn(Connection cn,
			String tran_id, String to_from_type) {
		List<Integer> jvNos = new ArrayList<Integer>();
		String sql = " select distinct(jv_no) from journal_voucher_mcg where tran_id='"
				+ tran_id + "' AND to_from_type='" + to_from_type + "'";

		try {
			Statement st = cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				int jvNo = rs.getInt("jv_no");
				jvNos.add(jvNo);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return jvNos;

	}
} // EOC