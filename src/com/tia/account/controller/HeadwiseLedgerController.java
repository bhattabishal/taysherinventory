package com.tia.account.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.model.LedgerModel;
import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilFxn;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;



/**
 * @author minesh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 * @altered & revised by suresh
 */
public class HeadwiseLedgerController {
	Connection cnn;
	Statement st;
	ResultSet rs = null;
	LedgerModel ledgerModel;

	SimpleDateFormat simpleDateFormat;
	JCalendarFunctions jCalendarFunctions;

	public HeadwiseLedgerController() {
		this.cnn = DbConnection.getConnection();
		ledgerModel = new LedgerModel();

		simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		jCalendarFunctions = new JCalendarFunctions();
	}

	/**
	 * Method to calculate the balance brought down amount.
	 * 
	 * @param accountCode
	 *            (Account code of the selected account head.)
	 * @param fromDate
	 *            (Starting date)
	 * @param branchId
	 *            (Branch id of the firm.)
	 * @param fiscalYear
	 *            (Fiscal year.)
	 */
	public double getBalanceBD(String accountCode, String fromDate,
			int branchId, int fiscalYear) {
		Date openingDate = null;

		try {
			openingDate = simpleDateFormat.parse(UtilFxn
					.getFiscalStartDate(ApplicationWorkbenchWindowAdvisor
							.getFiscalId()));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		/*
		 * We do this because, we have to take the -ve balance i.e. the opening
		 * balance that was first entered at the start of the fiscal year while
		 * we don't take into account the -ve balances that occurred later in
		 * the fiscal year(Quarterly closing balances).
		 */
		String strQry = "SELECT (SUM(dr_amt)-SUM(cr_amt)) AS PrevBalance FROM ledger_mcg WHERE acc_code LIKE '"
			+ accountCode
			+ "%' AND posted_date < '"
			+ fromDate
			+ "'"
			+ " AND led_id >= (select min(led_id) from ledger_mcg where jv_no = -1 and posted_date = " +
			" (select MAX(posted_date) from ledger_mcg where jv_no=-1 and is_opening=1))";

		System.out.println("getBalanceBD query:	" + strQry);

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
			if (rs.next()) {

				return rs.getDouble("PrevBalance");
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
		return 0;
	}

	/**
	 * Method to calculate the balance carried down amount.
	 * 
	 * @param accountCode
	 *            (Account code of the account head.)
	 * @param fromDate
	 *            (Starting date.)
	 * @param toDate
	 *            (Ending date.)
	 * @param fiscalYear
	 *            (Fiscal year.)
	 */
	public double getBalanceCD(String accountCode, String fromDate,
			String toDate, int fiscalYear) {
		String strQry = "SELECT SUM(dr_amt)-SUM(cr_amt) AS balance_cd "
			+ "FROM ledger_mcg WHERE jv_no IN "
			+ "(SELECT jv_no FROM ledger_mcg WHERE acc_code LIKE'"
			+ accountCode + "' AND " + "(posted_date BETWEEN '" + fromDate
			+ "' AND '" + toDate + "') AND " + "fy_id=" + fiscalYear
			+ ") AND acc_code='" + accountCode + "%'";
		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			rs = st.executeQuery(strQry);

			if (rs.next())
				return rs.getDouble("balance_cd");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Method to load the ledger table. The data retrieved the ledger are then
	 * returned as ResultSet.
	 * 
	 * @param accountCode
	 *            (Account code of the account head.)
	 * @param fromDate
	 *            (Starting date to specify the range.)
	 * @param toDate
	 *            (Ending date to specify the range.)
	 * @param fiscalYear
	 *            (Fiscal year)
	 */
	public ResultSet loadLedger(String accountCode, String fromDate,
			String toDate, int fiscalYear) {

		String strQry = "SELECT "
			+ "posted_date, l.jv_no, to_acc_code, "
			+ "(SELECT acc_name FROM acc_head_mcg WHERE acc_code = to_acc_code) AS Particulars, "
			+ "l.dr_amt, l.cr_amt, a.alias, "
			+ "( case when l.remarks is null  then (SELECT distinct narration FROM journal_voucher_details_mcg AS j "
			+ " WHERE j.jv_no = l.jv_no AND l.to_acc_code = j.acc_code) else l.remarks end  " +
			") AS narration, "
			+ "l.nepali_created_dt "
			+ "FROM   "
			+ "	ledger_mcg as l INNER JOIN acc_head_mcg AS a ON a.acc_code = l.to_acc_code "
			+ "WHERE " + "	posted_date BETWEEN '" + fromDate + "' AND '"
			+ toDate + "' AND jv_no <> -1  AND fy_id="
			+ ApplicationWorkbenchWindowAdvisor.getFiscalId()
			+ "  AND   l.acc_code like'" + accountCode
			+ "%' ORDER BY posted_date ";

		System.out.println("Ledger loading query:	" + strQry);

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);

			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param date
	 * @return boolean value
	 * method to check if current period's closing 
	 * has been preformed or not
	 */
	public boolean isClosingPerformed(String date) {
		boolean retFlag = false;
		String sql = "SELECT COUNT(*) AS total FROM ledger_mcg WHERE jv_no = -1 AND posted_date = '"
			+ date + "'";

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

			if (rs.next()) {
				if (rs.getInt("total") >= 1)
					retFlag = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retFlag;
	}
}
