package com.tia.account.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.model.AccHeadModel;
import com.tia.account.model.JVDetailsClosing;
import com.tia.account.model.JVDetailsModel;
import com.tia.account.model.JVSummary;
import com.tia.account.model.JVSummaryClosing;
import com.tia.account.model.LedgerModel;
import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;



/**
 * @author minesh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 * @altered & revised by suresh
 */
public class JournalEntryController {
	static JournalEntryController journalEntryController;
	Connection cnn;
	Statement st;
	ResultSet rs;

	JVDetailsModel objJVDetails;
	LedgerController objLedgerController;
	SimpleDateFormat simpleDateFormat;
	DecimalFormat decimalFormat = new DecimalFormat("#0.00");

	/**
	 * This is the constructor of this class itself
	 */
	public JournalEntryController() {
		if (journalEntryController == null)
			journalEntryController = this;
		// database connection
		this.cnn = DbConnection.getConnection();

		// instance of the JVDetails class
		objJVDetails = new JVDetailsModel();

		// instance of LedgerController class
		objLedgerController = new LedgerController();

		simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	}// end of method constructor method JournalEntryController()

	/**
	 * @return instance of this class
	 */
	public static JournalEntryController getInstance() {
		if (journalEntryController == null) {
			new JournalEntryController();
			return JournalEntryController.getInstance();
		}

		return journalEntryController;
	}// end of method getInstance()

	/**
	 * This method checks whether a selected node is leaf or not. If true,
	 * returns the same and false otherwise
	 * 
	 * @param accCode
	 *            (account code of the selected account entry)
	 * @return boolean (true if the node is leaf, false otherwise)
	 */
	public boolean isNodeLeaf(String accCode) {
		/*
		 * check if the code exists in db, if it exists continue further
		 * checking if it does not exist, abort this trxn with error msg.
		 */
		boolean retFlag = false;
		if (checkItExists(accCode)) {
			String strQry = "SELECT (case when(COUNT(*)=0) then 1 else (SELECT COUNT(*) AS children_count FROM acc_head_mcg WHERE acc_code LIKE '"
					+ accCode
					+ ".%')+1 end) "
					+ " AS children_count FROM acc_head_mcg WHERE acc_code LIKE '"
					+ accCode + ".%'";

			try {
				this.st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
						ResultSet.CONCUR_READ_ONLY);
				ResultSet rs = st.executeQuery(strQry);

				while (rs.next()) {
					if (rs.getInt(1) == 1) {
						// then the selected account is definitely the leaf node
						retFlag = true;
					} else if (rs.getInt(1) > 1) {
						// then the selected account definitely has children
						retFlag = false;
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			retFlag = false;
		}
		return retFlag;
	}// end of method isNodeLeaf()

	public boolean checkItExists(String accCode) {
		String isAccCodeFound = UtilFxn
				.GetValueFromTable("SELECT distinct(acc_code) from acc_head_mcg where acc_code='"
						+ accCode + "'");
		if (!isAccCodeFound.equals("")) {
			return true;
		} else {
			MessageBox mb = new MessageBox(Display.getDefault()
					.getActiveShell(), SWT.ICON_ERROR);
			mb.setText("Code Not Found");
			mb.setMessage("Following Codes Not Found" + "\n"
					+ accCode);
			mb.open();
			return false;
		}
	}// end of method isNodeLeaf()

	/**
	 * getAccountType() to retrieve the account type of the account head through
	 * account code. The account type could be one of them listed below:
	 * 
	 * <pre>
	 * 1. Assets 2. Expenses 3. Income 4. Liability
	 * </pre>
	 * 
	 * @param strAccCode
	 * @return String
	 */
	public String getAccountType(String strAccCode) {
		String strQry = "SELECT cv_lbl FROM code_value_mcg WHERE cv_type ='Account Type' AND cv_code = "
				+ "(SELECT acc_type FROM acc_head_mcg WHERE acc_code='"
				+ strAccCode + "')";

		try {
			this.st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			ResultSet rs = st.executeQuery(strQry);
			while (rs.next())
				return rs.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}// end of method getAccountType()

	/**
	 * getAccountTypeCode() to retrieve the account type code of the account
	 * head through account code. The account type could be one of them listed
	 * below:
	 * 
	 * <pre>
	 * 1. Assets 2. Expenses 3. Income 4. Liability 5. Assets-Liability
	 * </pre>
	 * 
	 * @param strAccCode
	 * @return String
	 */
	public String getAccountTypeCode(String strAccCode) {
		String strQry = "SELECT cv_code FROM code_value_mcg WHERE cv_type ='Account Type' AND cv_code = "
				+ "(SELECT acc_type FROM acc_head_mcg WHERE acc_code='"
				+ strAccCode + "')";

		try {
			this.st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			ResultSet rs = st.executeQuery(strQry);
			while (rs.next())
				return rs.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}// end of mtehod getAccountTypeCode()

	/**
	 * getAccountBalance() to retrieve the balance amount information of the
	 * particular account code from the ledger account.
	 * 
	 * @param strAccCode
	 * @param strAccType
	 * @return double
	 */
	public double getAccountBalance(String strAccCode, String strAccType) {
		String strQry = "  SELECT "
				+ ((strAccType.equals("1")) || (strAccType.equals("2")) ? "  SUM(dr_amt)-SUM(cr_amt) as currbal "
						: "  SUM(cr_amt)-SUM(dr_amt) as currbal ")
				+ "   FROM ledger_mcg  "
				+ "   WHERE posted_date>= (select MAX(posted_date) from ledger_mcg where jv_no=-1 and is_opening=1) "
				+ "   AND  acc_code='" + strAccCode + "' ";
		double amount = 0;
		System.out.println("this is the account balance:" + strQry);
		try {
			this.st = this.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			ResultSet rs = st.executeQuery(strQry);
			while (rs.next()) {
				amount = rs.getDouble(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return amount;
	}// end of method getAccountBalance()

	/**
	 * autoSaveJournal() method to save the journal entries into the table. The
	 * corresponding ledger posting are automatically saved in the ledger_mcg
	 * table automatically. This method in fact calls the saveJournal() method
	 * with the saveJournal() method's fourth parameter always set to true.
	 * 
	 * Loan modules require journal entries be auto approved and posted to the
	 * ledger table automatically. This method is invoked in such scenarios.
	 * 
	 * @param objJVSummary
	 *            ( Object of type JVSummary that has all its fields set to the
	 *            current journal entry's summary info.)
	 * 
	 * @param accSet
	 *            ( An arraylist holding the objects of type JVDetails each of
	 *            which has the transaction specific details. Each instance of
	 *            JVDetails holds the detailed Dr. and Cr. transactions.
	 * 
	 * @return boolean (true if journal summary, journal details and ledger
	 *         posting all are successful, false otherwise.)
	 */
	public boolean autoSaveJournal(Connection cnn, JVSummary objJVSummary,
			ArrayList<JVDetailsModel> accSet, ArrayList<String> log) {

		int val = saveJournal(cnn, objJVSummary, accSet, true, log);
		if (val > 0) {
			return true;
		} else {
			return false;
		}
	}// autoSaveJournal

	/**
	 * approveSaveJournal() method to save the journal entries into the table.
	 * The corresponding ledger posting are saved in the ledger_mcg table only
	 * after the journal entries are approved. This method will in turn invoke
	 * saveJournal() method with the fourth parameter always set to false.
	 * 
	 * Account module require journal entries be inspected and approved and only
	 * then posted to the ledger table. This method is invoked in such
	 * scenarios.
	 * 
	 * @param objJVSummary
	 *            ( Object of type JVSummary that has all its fields set to the
	 *            current journal entry's summary info.)
	 * 
	 * @param accSet
	 *            ( An arraylist holding the objects of type JVDetails each of
	 *            which has the transaction specific details. Each instance of
	 *            JVDetails holds the detailed Dr. and Cr. transactions.
	 * 
	 * @return boolean (true if journal summary, journal details and ledger
	 *         posting all are successful, false otherwise.)
	 */
	public boolean approveSaveJournal(Connection cnn, JVSummary objJVSummary,
			ArrayList<JVDetailsModel> accSet, ArrayList<String> log) {
		int check = saveJournal(cnn, objJVSummary, accSet, false, log);
		if (check > 0) {
			return true;
		} else {
			return false;
		}
	}// end of method approveSaveJournal()

	/**
	 * Saves the journal entries into the journal_voucher_mcg and
	 * journal_voucher_details_mcg table and returns true if saving into both
	 * tables are successful.
	 * 
	 * @param objJVSummary
	 *            ( Object of type JVSummary that has all its fields set to the
	 *            current journal entry's summary info.)
	 * @param accSet
	 *            ( An arraylist holding the objects of type JVDetails each of
	 *            which has the transaction specific details. Each instance of
	 *            JVDetails holds the detailed Dr. and Cr. transactions.
	 * @param autoCommit
	 *            (Boolean variable that determines if posting to the ledger
	 *            should be automatic or manual. If false, the method to save
	 *            into ledger is not invoked,else invoked.)
	 * 
	 * @return boolean (true if journal summary, journal_details and (depending
	 *         if fourth parameter autoCommit set to true) ledger posting is
	 *         successful, false otherwise.)
	 */
	public Integer saveJournal(Connection cnn, JVSummary objJVSummary,
			ArrayList<JVDetailsModel> accSet, boolean autoCommit,
			ArrayList<String> log) {
		String strQry = "";
		int maxId = UtilMethods.getMaxId("journal_voucher_mcg", "jv_no");
		String logFileData = "";

		if (objJVSummary.getJv_no() <= 0) {
			objJVSummary.setJv_no(maxId);
			objJVSummary.setUser_jv_no(maxId);
			objJVSummary.setFy_id(ApplicationWorkbenchWindowAdvisor
					.getFiscalId());
			objJVSummary.setBr_id(1);
			objJVSummary.setCreated_by(ApplicationWorkbenchWindowAdvisor
					.getLoginId());
			objJVSummary.setCreated_date(JCalendarFunctions.currentDate());
			objJVSummary.setUpdated_by(ApplicationWorkbenchWindowAdvisor
					.getLoginId());
			objJVSummary.setUpdated_date(JCalendarFunctions.currentDate());
			objJVSummary.setUpdate_count(0);
			strQry = objJVSummary.Insert();

		} else {
			objJVSummary.z_WhereClause.jv_no(objJVSummary.getJv_no());
			objJVSummary.setUpdate_count(objJVSummary.getUpdate_count() + 1);
			objJVSummary.z_WhereClause.br_id(1);
			strQry = objJVSummary.Update();
		}
		/* Log file Setting */
		int m = (objJVSummary.getJv_no() <= 0) ? maxId : objJVSummary
				.getJv_no();
		logFileData = "\t\t(Journal-Voucher):\tJv_no->" + m + "\tFy_id->"
				+ ApplicationWorkbenchWindowAdvisor.getFiscalId() + "\t";
		log.add(logFileData);
		try {
			try {
				System.out.println("zzz.. saveJournal QRY =" + strQry);
				Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
						ResultSet.CONCUR_UPDATABLE);

				if (st.executeUpdate(strQry) < 1) {
					return 0;
				}

				

				// save the journal details too
				if (saveJournalDetails(cnn, accSet, objJVSummary.getJv_no(),
						false, log)) {

					if (!autoCommit)
						return maxId;
					else {
						// code for auto posting to ledger goes here.
						LedgerController Ldao = new LedgerController();
						if (!Ldao.postToLedger(cnn,
								String.valueOf(objJVSummary.getJv_no()),log)) {
							return 0;
						}
					}

					return maxId;
				}
				// this.cnn.commit();
				else {
					cnn.rollback();
					return 0;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				cnn.rollback();
				return 0;
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			return 0;
		}
	}// end of method saveJournal()

	/**
	 * Saves the journal entries into the jv_summary_closing_mcg and
	 * jv_details_closing_mcg table and returns true if saving into both tables
	 * are successful.
	 * 
	 * @param objJVSummary
	 *            ( Object of type JVSummaryClosing that has all its fields set
	 *            to the current journal entry's summary info (at the time of
	 *            closing).)
	 * @param accSet
	 *            ( An arraylist holding the objects of type JVDetails each of
	 *            which has the transaction specific details. Each instance of
	 *            JVDetails holds the detailed Dr. and Cr. transactions.
	 * @param autoCommit
	 *            (Boolean variable that determines if posting to the ledger
	 *            should be automatic or manual. If false, the method to save
	 *            into ledger is not invoked,else invoked.)
	 * 
	 * @return boolean (true if journal summary, journal_details is successful,
	 *         false otherwise.)
	 */
	public boolean saveJournalForClosing(Connection cnn,
			JVSummaryClosing objJVSummary, ArrayList<JVDetailsClosing> accSet,
			ArrayList<String> log) {
		String strQry = "";
		// int maxId = UtilFxn.getMaxId("journal_voucher_mcg", "jv_no");

		// if (objJVSummary.getJv_no() <= 0) {
		// objJVSummary.setJv_no(maxId);
		// objJVSummary.setUser_jv_no(maxId);
		objJVSummary.setCreated_date(JCalendarFunctions.currentDate());
		objJVSummary.setCreated_by(ApplicationWorkbenchWindowAdvisor
				.getLoginId());
		objJVSummary.setUpdated_date(JCalendarFunctions.currentDate());
		objJVSummary.setUpdate_count(0);
		strQry = objJVSummary.Insert();
		// }
		try {
			try {
				System.out.println("zzz.. saveJournal for Closing QRY ="
						+ strQry);
				Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
						ResultSet.CONCUR_UPDATABLE);

				if (st.executeUpdate(strQry) < 1) {
					return false;
				}

				

				// save the journal details too
				if (saveJournalDetailsForClosing(cnn, accSet,
						objJVSummary.getJv_no())) {
					return true;
				} else {
					cnn.rollback();
					return false;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				cnn.rollback();
				return false;
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			return false;
		}
	}// end of method saveJournalForClosing()

	/**
	 * @param cnn
	 * @param objJVSummary
	 * @param accSet
	 * @param autoCommit
	 * @return integer value
	 */
	public Integer saveJournalSavingEdit(Connection cnn,
			JVSummary objJVSummary, ArrayList<JVDetailsModel> accSet,
			boolean autoCommit) {
		String strQry = "";
		int maxId = UtilMethods.getMaxId("journal_voucher_mcg", "jv_no");

		if (objJVSummary.getJv_no() <= 0) {
			objJVSummary.setJv_no(maxId);
			objJVSummary.setUser_jv_no(maxId);

			objJVSummary.setFy_id(ApplicationWorkbenchWindowAdvisor
					.getFiscalId());
			objJVSummary.setBr_id(1);
			objJVSummary.setCreated_by(ApplicationWorkbenchWindowAdvisor
					.getLoginId());
			objJVSummary.setCreated_date(JCalendarFunctions.currentDate());
			objJVSummary.setUpdated_by(ApplicationWorkbenchWindowAdvisor
					.getLoginId());
			objJVSummary.setUpdated_date(JCalendarFunctions.currentDate());
			objJVSummary.setUpdate_count(0);
			strQry = objJVSummary.Insert();

		} else {
			objJVSummary.z_WhereClause.jv_no(objJVSummary.getJv_no());
			objJVSummary.setUpdate_count(objJVSummary.getUpdate_count() + 1);
			objJVSummary.z_WhereClause.br_id(1);
			strQry = objJVSummary.Update();
		}

		try {
			try {
				System.out.println("zzz.. saveJournal QRY =" + strQry);
				Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
						ResultSet.CONCUR_UPDATABLE);

				if (st.executeUpdate(strQry) < 1) {
					return 0;
				}

				

				// save the journal details too
				if (saveJournalDetailsSavingEdit(cnn, accSet,
						objJVSummary.getJv_no(), false)) {

					if (!autoCommit)
						return maxId;
					else {
						// code for auto posting to ledger goes here.
						LedgerController Ldao = new LedgerController();
						if (!Ldao.postToLedgerSaving(this.cnn,
								String.valueOf(objJVSummary.getJv_no()))) {
							return 0;
						}
					}

					return maxId;
				}
				// this.cnn.commit();
				else {
					cnn.rollback();
					return 0;
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				cnn.rollback();
				return 0;
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			return 0;
		}
	}// end of method saveJournalSavingEdit()

	/**
	 * saveJournalDetails() saves the detailed Dr. and Cr. entries of a Journal
	 * Voucher into the journal_voucher_details_mcg table.
	 * 
	 * @param existingCnn
	 *            (Connection to the database is passed the same connection as
	 *            used in the saveJournal() method. This is because, transaction
	 *            details and the transaction summary both must be saved. For
	 *            this we have set the autocommit false and unless the insert
	 *            query for journal details is not successful, the journal
	 *            summary must be rolled back too.
	 * @param accSet
	 *            (Arraylist holding the objects of type JVDetails. Each object
	 *            holds the detailed Dr. and Cr. entries.)
	 * @param jvNo
	 *            (The journal voucher no. of the single transaction to which
	 *            all these detailed entries belong to.)
	 */
	public boolean saveJournalDetails(Connection existingCnn,
			ArrayList<JVDetailsModel> accSet, int jvNo, boolean autoApprove,
			ArrayList<String> log) {

		String strQry = "";
		boolean retFlag = false;
		String logFileData = "";
		/*
		 * If, the jvNo is already existing, delete them.
		 * 
		 * saveJournalDetails() method is used to update the existing journal
		 * details too. If there are entries in the journal_voucher_details_mcg
		 * table for the passed jvNo, then we are actually updating the journal
		 * details rather than creating the entries for that jvNo. And while
		 * updating/editing the records, the approach here is
		 * 
		 * a) Delete all the entries from the table with jv_no = jvNo. b) Run an
		 * insert query again with the updated values.
		 */

		if (countExistingJvNo(jvNo)) {
			String sqlDelete = "DELETE FROM journal_voucher_details_mcg WHERE jv_no = '"
					+ jvNo + "'";

			try {
				Statement st = existingCnn
						.createStatement(ResultSet.TYPE_FORWARD_ONLY,
								ResultSet.CONCUR_UPDATABLE);

				// at least two rows will be affected
				if (st.executeUpdate(sqlDelete) <= 1)
					retFlag = false;

				

			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		// prepare to run the insert query
		int maxId = UtilMethods
				.getMaxId("journal_voucher_details_mcg", "jv_det_no");

		try {
			// local vars to hold sum of all dr and all cr
			double drTotal = 0.00, crTotal = 0.00;

			for (int i = 0; i < accSet.size(); i++) {
				objJVDetails.setJv_det_no(maxId);
				objJVDetails.setJv_no(jvNo);
				/*
				 * Check if the involved trxn has any invalid acc_code if yes,
				 * abort trxn with msg if no, continue to save the trxn
				 */
				if (!isNodeLeaf(accSet.get(i).getAcc_code())) {
					MessageBox mb = new MessageBox(Display.getCurrent()
							.getActiveShell(), SWT.ERROR);
					mb.setMessage("account code Involved Is Invalid"
							+ "'"
							+ accSet.get(i).getAcc_code() + "'");
					mb.setText("invalid Account Code");
					mb.open();
					return false;
				}

				objJVDetails.setAcc_code(accSet.get(i).getAcc_code());
				objJVDetails.setSub_acc_code(accSet.get(i).getSub_acc_code());
				objJVDetails.setDr_amt(Double.parseDouble(accSet.get(i)
						.getDr_amt()));
				objJVDetails.setCr_amt(Double.parseDouble(accSet.get(i)
						.getCr_amt()));
				// add the dr and cr values
				System.out.println(accSet.get(i).getDr_amt());
				drTotal += Double.valueOf(accSet.get(i).getDr_amt());
				crTotal += Double.valueOf(accSet.get(i).getCr_amt());

				objJVDetails.setNarration(accSet.get(i).getNarration());
				objJVDetails.setRemarks(accSet.get(i).getRemarks());
				objJVDetails.setCreated_by(ApplicationWorkbenchWindowAdvisor
						.getLoginId());
				objJVDetails.setCreated_date(JCalendarFunctions.currentDate());
				// These 4-lines were added later by suresh
				objJVDetails.setBr_id(1);
				objJVDetails
						.setUpdated_by(String
								.valueOf(ApplicationWorkbenchWindowAdvisor
										.getLoginId()));
				objJVDetails.setUpdated_date(JCalendarFunctions.currentDate());
				objJVDetails.setUpdate_count(0);

				strQry = objJVDetails.Insert();

				Statement st = existingCnn
						.createStatement(ResultSet.TYPE_FORWARD_ONLY,
								ResultSet.CONCUR_UPDATABLE);
				/* Log File By Ganesh and increment maxId here */
				logFileData = "\t(New):\t0Jv_det_no" + maxId + "\tJv_no" + jvNo
						+ "\tAcc_code" + jvNo + "\tAcc_code" + jvNo
						+ "\tAcc_code" + jvNo + "\tAcc_code" + jvNo
						+ "\tAcc_code" + jvNo + "\tAcc_code" + jvNo
						+ "\tAcc_code" + jvNo + "\tAcc_code" + jvNo
						+ "\tAcc_code" + jvNo + "\tAcc_code";
				log.add(logFileData);
				System.out.println("zzz.. saveJournalDetails QRY =" + strQry);
				if (st.executeUpdate(strQry) != 1
						)
					retFlag = false;

				maxId++;

			}
			System.out.println("dr total is :"+drTotal +"cr total is :" +crTotal);
			if (Double.parseDouble(decimalFormat.format(drTotal)) == Double
					.parseDouble(decimalFormat.format(crTotal))) {
				retFlag = true;
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
			retFlag = false;
		}
		return retFlag;
	}// end of method saveJournalDetails()

	/**
	 * saveJournalDetails() saves the detailed Dr. and Cr. entries of a Journal
	 * Voucher into the jv_details_closing_mcg table.
	 * 
	 * @param existingCnn
	 *            (Connection to the database is passed the same connection as
	 *            used in the saveJournal() method. This is because, transaction
	 *            details and the transaction summary both must be saved. For
	 *            this we have set the autocommit false and unless the insert
	 *            query for journal details is not successful, the journal
	 *            summary must be rolled back too.
	 * @param accSet
	 *            (Arraylist holding the objects of type JVDetailsClosing. Each
	 *            object holds the detailed Dr. and Cr. entries.)
	 * @param jvNo
	 *            (The journal voucher no. of the single transaction to which
	 *            all these detailed entries belong to.)
	 */
	public boolean saveJournalDetailsForClosing(Connection existingCnn,
			ArrayList<JVDetailsClosing> accSet, int jvNo) {

		String strQry = "";
		boolean retFlag = false;
		/*
		 * If, the jvNo is already existing, delete them.
		 * 
		 * saveJournalDetails() method is used to update the existing journal
		 * details too. If there are entries in the journal_voucher_details_mcg
		 * table for the passed jvNo, then we are actually updating the journal
		 * details rather than creating the entries for that jvNo. And while
		 * updating/editing the records, the approach here is
		 * 
		 * a) Delete all the entries from the table with jv_no = jvNo. b) Run an
		 * insert query again with the updated values.
		 */

		if (countExistingJvNo(jvNo)) {
			String sqlDelete = "DELETE FROM jv_details_closing_mcg WHERE jv_no = '"
					+ jvNo + "'";

			try {
				Statement st = existingCnn
						.createStatement(ResultSet.TYPE_FORWARD_ONLY,
								ResultSet.CONCUR_UPDATABLE);

				// at least two rows will be affected
				if (st.executeUpdate(sqlDelete) <= 1)
					retFlag = false;

				

			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		// prepare to run the insert query
		int maxId = UtilMethods.getMaxId("jv_details_closing_mcg", "jv_det_no");

		JVDetailsClosing objJVDetailsClosing = null;
		try {
			// local vars to hold sum of all dr and all cr
			double drTotal = 0.00, crTotal = 0.00;

			for (int i = 0; i < accSet.size(); i++) {
				objJVDetailsClosing = new JVDetailsClosing();
				objJVDetailsClosing.setJv_det_no(maxId++);
				objJVDetailsClosing.setJv_no(jvNo);
				objJVDetailsClosing.setAcc_code(accSet.get(i).getAcc_code());
				objJVDetailsClosing.setSub_acc_code(accSet.get(i)
						.getSub_acc_code());
				objJVDetailsClosing.setDr_amt(Double.parseDouble(accSet.get(i)
						.getDr_amt()));
				objJVDetailsClosing.setCr_amt(Double.parseDouble(accSet.get(i)
						.getCr_amt()));
				// add the dr and cr values
				System.out.println(accSet.get(i).getDr_amt());
				drTotal += Double.valueOf(accSet.get(i).getDr_amt());
				crTotal += Double.valueOf(accSet.get(i).getCr_amt());

				objJVDetailsClosing.setNarration(accSet.get(i).getNarration());
				objJVDetailsClosing.setRemarks(accSet.get(i).getRemarks());
				objJVDetailsClosing
						.setCreated_by(ApplicationWorkbenchWindowAdvisor
								.getLoginId());
				objJVDetailsClosing
						.setCreated_date(JCalendarFunctions.currentDate());
				// These 4-lines were added later by suresh
				objJVDetailsClosing.setBr_id(1);
				objJVDetailsClosing
						.setUpdated_by(String
								.valueOf(ApplicationWorkbenchWindowAdvisor
										.getLoginId()));
				objJVDetailsClosing
						.setUpdated_date(JCalendarFunctions.currentDate());
				objJVDetailsClosing.setUpdate_count(0);

				strQry = objJVDetailsClosing.Insert();

				Statement st = existingCnn
						.createStatement(ResultSet.TYPE_FORWARD_ONLY,
								ResultSet.CONCUR_UPDATABLE);
				System.out.println("zzz.. saveJournalDetailsClosing QRY ="
						+ strQry);
				if (st.executeUpdate(strQry) != 1
						)
					retFlag = false;

			}
			if (Double.parseDouble(decimalFormat.format(drTotal)) == Double
					.parseDouble(decimalFormat.format(crTotal))) {
				retFlag = true;
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
			retFlag = false;
		}
		return retFlag;
	}// end of method saveJournalDetails()

	/**
	 * @param existingCnn
	 * @param ledgerList
	 * @param jvNo
	 * @return boolean
	 * 
	 *         This method was written by suresh explicitely in this Class;
	 *         especially to address the posting of the journal related to PL
	 *         amount to be transferred to HO if any at very begining of closing
	 *         process as the normal transactions; then only posting of the
	 *         opening balances at last of closing process
	 */

	/**
	 * saveJournalDetails() saves the detailed Dr. and Cr. entries of a Journal
	 * Voucher into the journal_voucher_details_mcg table.
	 * 
	 * @param existingCnn
	 *            (Connection to the database is passed the same connection as
	 *            used in the saveJournal() method. This is because, transaction
	 *            details and the transaction summary both must be saved. For
	 *            this we have set the autocommit false and unless the insert
	 *            query for journal details is not successful, the journal
	 *            summary must be rolled back too.
	 * @param accSet
	 *            (Arraylist holding the objects of type JVDetails. Each object
	 *            holds the detailed Dr. and Cr. entries.)
	 * @param jvNo
	 *            (The journal voucher no. of the single transaction to which
	 *            all these detailed entries belong to.)
	 */
	public boolean saveJournalDetailsSavingEdit(Connection existingCnn,
			ArrayList<JVDetailsModel> accSet, int jvNo, boolean autoApprove) {

		String strQry = "";

		/*
		 * If, the jvNo is already existing, delete them.
		 * 
		 * saveJournalDetails() method is used to update the existing journal
		 * details too. If there are entries in the journal_voucher_details_mcg
		 * table for the passed jvNo, then we are actually updating the journal
		 * details rather than creating the entries for that jvNo. And while
		 * updating/editing the records, the approach here is
		 * 
		 * a) Delete all the entries from the table with jv_no = jvNo. b) Run an
		 * insert query again with the updated values.
		 */
		int maxId = UtilMethods
				.getMaxId("journal_voucher_details_mcg", "jv_det_no");

		if (countExistingJvNo(jvNo)) {
			try {
				for (int i = 0; i < accSet.size(); i++) {
					ResultSet rss = journalDetails(jvNo);
					if (rss.next()) {
						rss.beforeFirst();
						while (rss.next()) {
							String aaa = String.valueOf(UtilFxn
									.GetValueFromTable(
											"journal_voucher_details_mcg",
											"dr_amt",
											"jv_det_no = '"
													+ rss.getInt("jv_det_no")
													+ "'"));
							System.out.println("This is the aaa:" + aaa);
							String aa = String.valueOf(UtilFxn
									.GetValueFromTable(
											"journal_voucher_details_mcg",
											"cr_amt",
											"jv_det_no = '"
													+ rss.getInt("jv_det_no")
													+ "'"));
							System.out.println("This is the aa:" + aa);
							System.out.println("This is the array size:"
									+ accSet.size());
							System.out.println("Second"
									+ accSet.get(i).getCr_amt());
							if (!String.valueOf(
									UtilFxn.GetValueFromTable(
											"journal_voucher_details_mcg",
											"dr_amt",
											"jv_det_no = '"
													+ rss.getInt("jv_det_no")
													+ "'")).equals("0.00")
									&& String
											.valueOf(
													UtilFxn.GetValueFromTable(
															"journal_voucher_details_mcg",
															"cr_amt",
															"jv_det_no = '"
																	+ rss.getInt("jv_det_no")
																	+ "'"))
											.equals("0.00")
									&& !accSet.get(i).getDr_amt().equals("0")) {
								System.out.println("Hello RCP");
								objJVDetails.setJv_no(jvNo);
								objJVDetails.setAcc_code(accSet.get(i)
										.getAcc_code());
								objJVDetails.setSub_acc_code(accSet.get(i)
										.getSub_acc_code());
								objJVDetails
										.setDr_amt(Double.parseDouble(accSet
												.get(i).getDr_amt()));
								System.out.println("This is the debit amount:"
										+ objJVDetails.getDr_amt());
								objJVDetails
										.setCr_amt(Double.parseDouble(accSet
												.get(i).getCr_amt()));
								objJVDetails.setNarration(accSet.get(i)
										.getNarration());
								objJVDetails.setRemarks(accSet.get(i)
										.getRemarks());
								objJVDetails
										.setCreated_by(ApplicationWorkbenchWindowAdvisor
												.getLoginId());
								objJVDetails
										.setCreated_date(JCalendarFunctions.currentDate());
								// These 4-lines were added later by suresh
								objJVDetails
										.setBr_id(1);
								objJVDetails
										.setUpdated_by(String
												.valueOf(ApplicationWorkbenchWindowAdvisor
														.getLoginId()));
								objJVDetails
										.setUpdated_date(JCalendarFunctions.currentDate());
								objJVDetails.setUpdate_count(0);
								System.out.println("Here is the jv_det_no:"
										+ rss.getInt("jv_det_no"));
								// objJVDetails.setJv_det_no(rss.getInt("jv_det_no"));
								objJVDetails.z_WhereClause.jv_det_no(rss
										.getInt("jv_det_no"));
								strQry = objJVDetails.Update();
								Statement st = existingCnn.createStatement(
										ResultSet.TYPE_FORWARD_ONLY,
										ResultSet.CONCUR_UPDATABLE);
								System.out
										.println("zzz.. saveJournalDetails QRY ="
												+ strQry);
								if (st.executeUpdate(strQry) != 1
										)
									return false;
							} else if (!String.valueOf(
									UtilFxn.GetValueFromTable(
											"journal_voucher_details_mcg",
											"cr_amt",
											"jv_det_no = '"
													+ rss.getInt("jv_det_no")
													+ "'")).equals("0.00")
									&& String
											.valueOf(
													UtilFxn.GetValueFromTable(
															"journal_voucher_details_mcg",
															"dr_amt",
															"jv_det_no = '"
																	+ rss.getInt("jv_det_no")
																	+ "'"))
											.equals("0.00")
									&& !accSet.get(i).getCr_amt().equals("0")) {
								System.out.println("Hello RCP2");
								objJVDetails.setJv_no(jvNo);
								objJVDetails.setAcc_code(accSet.get(i)
										.getAcc_code());
								objJVDetails.setSub_acc_code(accSet.get(i)
										.getSub_acc_code());
								objJVDetails
										.setDr_amt(Double.parseDouble(accSet
												.get(i).getDr_amt()));
								System.out.println("This is the debit amount:"
										+ objJVDetails.getDr_amt());
								objJVDetails
										.setCr_amt(Double.parseDouble(accSet
												.get(i).getCr_amt()));
								objJVDetails.setNarration(accSet.get(i)
										.getNarration());
								objJVDetails.setRemarks(accSet.get(i)
										.getRemarks());
								objJVDetails
										.setCreated_by(ApplicationWorkbenchWindowAdvisor
												.getLoginId());
								objJVDetails
										.setCreated_date(JCalendarFunctions.currentDate());
								// These 4-lines were added later by suresh
								objJVDetails
										.setBr_id(1);
								objJVDetails
										.setUpdated_by(String
												.valueOf(ApplicationWorkbenchWindowAdvisor
														.getLoginId()));
								objJVDetails
										.setUpdated_date(JCalendarFunctions.currentDate());
								objJVDetails.setUpdate_count(0);
								System.out.println("Here is the jv_det_no:"
										+ rss.getInt("jv_det_no"));
								// objJVDetails.setJv_det_no(rss.getInt("jv_det_no"));
								objJVDetails.z_WhereClause.jv_det_no(rss
										.getInt("jv_det_no"));
								strQry = objJVDetails.Update();
								Statement st = existingCnn.createStatement(
										ResultSet.TYPE_FORWARD_ONLY,
										ResultSet.CONCUR_UPDATABLE);
								System.out
										.println("zzz.. saveJournalDetails QRY ="
												+ strQry);
								if (st.executeUpdate(strQry) != 1
										)
									return false;
							}
						}
					} else {
						objJVDetails.setJv_no(jvNo);
						objJVDetails.setAcc_code(accSet.get(i).getAcc_code());
						objJVDetails.setSub_acc_code(accSet.get(i)
								.getSub_acc_code());
						objJVDetails.setDr_amt(Double.parseDouble(accSet.get(i)
								.getDr_amt()));
						System.out.println("This is the debit amount:"
								+ objJVDetails.getDr_amt());
						objJVDetails.setCr_amt(Double.parseDouble(accSet.get(i)
								.getCr_amt()));
						objJVDetails.setNarration(accSet.get(i).getNarration());
						objJVDetails.setRemarks(accSet.get(i).getRemarks());
						objJVDetails
								.setCreated_by(ApplicationWorkbenchWindowAdvisor
										.getLoginId());
						objJVDetails
								.setCreated_date(JCalendarFunctions.currentDate());
						// These 4-lines were added later by suresh
						objJVDetails.setBr_id(1);
						objJVDetails.setUpdated_by(String
								.valueOf(ApplicationWorkbenchWindowAdvisor
										.getLoginId()));
						objJVDetails
								.setUpdated_date(JCalendarFunctions.currentDate());
						objJVDetails.setJv_det_no(maxId++);
						strQry = objJVDetails.Insert();

						Statement st = existingCnn.createStatement(
								ResultSet.TYPE_FORWARD_ONLY,
								ResultSet.CONCUR_UPDATABLE);
						System.out.println("zzz.. saveJournalDetails QRY ="
								+ strQry);
						if (st.executeUpdate(strQry) != 1
								)
							return false;
					}

				}

			} catch (SQLException ex) {
				ex.printStackTrace();
				return false;
			}
			return true;
		}// outermost if ends
		else {
			try {

				for (int i = 0; i < accSet.size(); i++) {
					objJVDetails.setJv_det_no(maxId++);
					objJVDetails.setJv_no(jvNo);
					objJVDetails.setAcc_code(accSet.get(i).getAcc_code());
					objJVDetails.setSub_acc_code(accSet.get(i)
							.getSub_acc_code());
					objJVDetails.setDr_amt(Double.parseDouble(accSet.get(i)
							.getDr_amt()));
					objJVDetails.setCr_amt(Double.parseDouble(accSet.get(i)
							.getCr_amt()));
					objJVDetails.setNarration(accSet.get(i).getNarration());
					objJVDetails.setRemarks(accSet.get(i).getRemarks());
					objJVDetails
							.setCreated_by(ApplicationWorkbenchWindowAdvisor
									.getLoginId());
					objJVDetails
							.setCreated_date(JCalendarFunctions.currentDate());
					// These 4-lines were added later by suresh
					objJVDetails.setBr_id(1);
					objJVDetails.setUpdated_by(String
							.valueOf(ApplicationWorkbenchWindowAdvisor
									.getLoginId()));
					objJVDetails
							.setUpdated_date(JCalendarFunctions.currentDate());
					objJVDetails.setUpdate_count(0);

					strQry = objJVDetails.Insert();

					Statement st = existingCnn.createStatement(
							ResultSet.TYPE_FORWARD_ONLY,
							ResultSet.CONCUR_UPDATABLE);
					System.out.println("zzz.. saveJournalDetails QRY ="
							+ strQry);
					if (st.executeUpdate(strQry) != 1
							)
						return false;

				}

			} catch (SQLException ex) {
				ex.printStackTrace();
				return false;
			}
			return true;
		}
	}// end of method saveJournalDetailsSavingEdit()

	/**
	 * @param existingCnn
	 * @param ledgerList
	 * @param jvNo
	 * @return boolean
	 * 
	 *         This method was written by suresh explicitely in this Class;
	 *         especially to address the posting of the journal related to PL
	 *         amount to be transferred to HO if any at very begining of closing
	 *         process as the normal transactions; then only posting of the
	 *         opening balances at last of closing process
	 */
	public boolean savePlTransferJournalsAndPostThem(Connection existingCnn,
			ArrayList<LedgerModel> ledgerList, int jvNo) {
		/*
		 * If, the jvNo is already existing, delete them.
		 * 
		 * saveJournalDetails() method is used to update the existing journal
		 * details too. If there are entries in the journal_voucher_details_mcg
		 * table for the passed jvNo, then we are actually updating the journal
		 * details rather than creating the entries for that jvNo. And while
		 * updating/editing the records, the approach here is
		 * 
		 * a) Delete all the entries from the table with jv_no = jvNo. b) Run an
		 * insert query again with the updated values.
		 */
		if (countExistingJvNo(jvNo)) {
			String sqlDelete = "DELETE FROM journal_voucher_details_mcg WHERE jv_no = '"
					+ jvNo + "'";

			try {
				Statement st = existingCnn
						.createStatement(ResultSet.TYPE_FORWARD_ONLY,
								ResultSet.CONCUR_UPDATABLE);

				// at least two rows will be affected
				if (st.executeUpdate(sqlDelete) <= 1)
					return false;

				

			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		// prepare to run the insert query
		try {

			for (int i = 0; i < ledgerList.size(); i++) {
				Statement st = existingCnn
						.createStatement(ResultSet.TYPE_FORWARD_ONLY,
								ResultSet.CONCUR_UPDATABLE);

				LedgerModel lm = new LedgerModel();
				lm.setLed_id(Integer.valueOf(UtilMethods.getMaxId("ledger_mcg",
						"led_id")));
				lm.setJv_no(jvNo);
				lm.setAcc_code(ledgerList.get(i).getAcc_code());
				lm.setSub_acc_code(ledgerList.get(i).getSub_acc_code());
				lm.setTo_acc_code(ledgerList.get(i).getTo_acc_code());
				lm.setTo_sub_acc_code(ledgerList.get(i).getTo_sub_acc_code());
				lm.setDr_amt(Double.parseDouble(ledgerList.get(i).getDr_amt()));
				lm.setCr_amt(Double.parseDouble(ledgerList.get(i).getCr_amt()));
				lm.setRemarks(ledgerList.get(i).getRemarks());
				lm.setPosted_date(ledgerList.get(i).getPosted_date());
				lm.setCreated_by(Integer.valueOf(ledgerList.get(i)
						.getCreated_by()));
				lm.setCreated_date(ledgerList.get(i).getCreated_date());
				lm.setNepali_created_dt(ledgerList.get(i)
						.getNepali_created_dt());
				lm.setUpdated_by(ledgerList.get(i).getUpdated_by());
				lm.setUpdated_date(ledgerList.get(i).getUpdated_date());
				lm.setBr_id(ledgerList.get(i).getBr_id());
				lm.setFy_id(Integer.valueOf(ledgerList.get(i).getFy_id()));

				String postToLedger = lm.Insert();
				try {
					if (st.executeUpdate(postToLedger) != 1
							) {
						return false;
					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}
				// }//outer else ends

			}// try ends

		} catch (SQLException ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}// end of method savePlTransferJournalsAndPostThem()

	/**
	 * getJournalSummary() gets the summary info of a journal transaction and
	 * returns the result set.
	 * 
	 * @param jvNo
	 *            (Journal voucher no. of the selected journal transaction for
	 *            editing purposes.)
	 * @return result set (Result set holding the summary info of the selected
	 *         journal transaction.)
	 */
	public ResultSet getJournalSummary(int jvNo) {
		String strQry = "SELECT br_id, tran_id, jv_date, narration, receipt_no, receipt_amt, cheque_no, "
				+ "cheque_amt, submitted_by, received_by, audited_by, approved_by, bill_date, remarks,to_from_type,tran_id "
				+ "FROM journal_voucher_mcg WHERE jv_no = '" + jvNo + "'";

		try {
			this.st = this.cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);

			this.rs = this.st.executeQuery(strQry);
			return this.rs;

		} catch (SQLException e) {
			e.printStackTrace();

			// query didn't execute
			return null;
		}
	}// end of method getJournalSummary()

	/**
	 * @param transId
	 * @return resultset method to get journals with Trxn_id passed
	 */
	public ResultSet getJournalWithTid(int transId) {
		String strQry = "SELECT jv_no, br_id, tran_id, jv_date, narration, receipt_no, receipt_amt, cheque_no "
				+ "cheque_amt, submitted_by, received_by, audited_by, approved_by, bill_date, remarks,to_from_type,tran_id "
				+ "FROM journal_voucher_mcg WHERE tran_id = '" + transId + "'";

		try {
			this.st = this.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			this.rs = this.st.executeQuery(strQry);
			return this.rs;

		} catch (SQLException e) {
			e.printStackTrace();

			// query didn't execute
			return null;
		}
	}// end of method getJournalWithTid()

	/**
	 * getJournalDetails() gets the detailed Dr. and Cr. entries of the selected
	 * journal transaction and returns the result set.
	 * 
	 * @param jvNo
	 *            (Journal voucher no. of the selected journal transaction for
	 *            editing purposes.)
	 * @return result set (Result set holding the detailed Dr. and Cr. entries
	 *         of the selected journal transaction.)
	 */
	public ResultSet getJournalDetails(int jvNo) {
		String strQry = "";

		// check if this selected jv_no has a prking conjugate or not
		if (!UtilFxn.GetValueFromTable("journal_voucher_mcg", "tran_id",
				"jv_no=" + jvNo + "").equals("")
				&& !UtilFxn.GetValueFromTable("journal_voucher_mcg",
						"to_from_type", "jv_no=" + jvNo + "").equals("")
				&& UtilFxn.GetValueFromTable("journal_voucher_mcg",
						"to_from_type", "jv_no=" + jvNo + "").equalsIgnoreCase(
						"account")) {
			// it is M vs. M type of TRXN : to be handled via parking account
			String tran_idSelected = UtilFxn.GetValueFromTable(
					"journal_voucher_mcg", "tran_id", "jv_no=" + jvNo + "");
			String toFromType_Selected = UtilFxn
					.GetValueFromTable("journal_voucher_mcg", "to_from_type",
							"jv_no=" + jvNo + "");

			strQry = " SELECT jv_det_no, jvd.acc_code, jvd.sub_acc_code, dr_amt, cr_amt, jvd.narration, "
					+ " ah.acc_name, sh.sub_acc_name	FROM journal_voucher_details_mcg as jvd "
					+ " INNER JOIN acc_head_mcg AS ah ON ah.acc_code = jvd.acc_code "
					+ " LEFT OUTER JOIN acc_sub_head_mcg AS sh ON sh.acc_code = jvd.acc_code "
					+ " inner join journal_voucher_mcg jvm on jvd.jv_no=jvm.jv_no "
					+ " WHERE  jvm.tran_id = '"
					+ tran_idSelected
					+ "' AND jvm.to_from_type='"
					+ toFromType_Selected
					+ "' AND jvd.acc_code<>'"
					+ ApplicationWorkbenchWindowAdvisor.getParkingAccount()
					+ "' ";
		} else {
			// it is normal trxn i.e. one vs multiple or vice-versa type of trxn
			strQry = "SELECT jv_det_no, jvd.acc_code, jvd.sub_acc_code, dr_amt, cr_amt, narration, "
					+ "ah.acc_name, sh.sub_acc_name "
					+ "FROM journal_voucher_details_mcg as jvd "
					+ "INNER JOIN acc_head_mcg AS ah ON ah.acc_code = jvd.acc_code "
					+ "LEFT OUTER JOIN acc_sub_head_mcg AS sh ON sh.acc_code = jvd.acc_code "
					+ "WHERE  jv_no = '" + jvNo + "' ";
		}

		try {
			this.st = this.cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);

			this.rs = this.st.executeQuery(strQry);
			return this.rs;

		} catch (SQLException e) {
			e.printStackTrace();

			// query didn't execute
			return null;
		}
	}// end of method getJournalDetails()

	/**
	 * countExistingJvNo() to check for the existence of a jv_no in the
	 * journal_voucher_details_mcg table. If exist, returns true, else false.
	 * 
	 * @param jvNo
	 *            (Journal voucher no. to be checked for.)
	 * @return boolean(true if exists, false otherwise.)
	 */
	public boolean countExistingJvNo(int jvNo) {
		String strQry = "SELECT count(*) FROM journal_voucher_details_mcg WHERE jv_no ='"
				+ jvNo + "'";

		try {
			Statement st = this.cnn.createStatement(
					ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
			ResultSet rs = st.executeQuery(strQry);

			while (rs.next())
				if (rs.getInt(1) > 0)
					return true;
				else
					return false;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}// end of method countExistingJvNo()

	/**
	 * journalDetails() returns the resultset of the specifiv jv_no
	 * 
	 * @return boolean(true if exists, false otherwise.)
	 */
	private ResultSet journalDetails(int jvNo) {
		String strQry = "SELECT * FROM journal_voucher_details_mcg WHERE jv_no ='"
				+ jvNo + "'";

		try {
			this.st = this.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			this.rs = this.st.executeQuery(strQry);
			return this.rs;

		} catch (SQLException e) {
			e.printStackTrace();

			// query didn't execute
			return null;
		}
	}// end of method journalDetails()

	/**
	 * Method that saves the journal entry for the opening balance of the
	 * account heads. It in turn makes a posting in the ledger table. Only if
	 * that posting is successful it return true, i.e. success.
	 * 
	 * @param accCode
	 *            (Account code of the newly created or updated account head)
	 * @param subAccCode
	 *            (Subaccount code of the account head.)
	 * @param d
	 *            (Amount of opening Balance)
	 * @param accType
	 *            (Account type)
	 * @param remarks
	 *            (Remarks to be entered as the narration.)
	 * @param cnn
	 *            (Db connection with autocommit set to false.)
	 * @param qryType
	 *            (Flag to keep track of the insert/update status of the opening
	 *            balance.)
	 * @return boolean(true if successfuly saved into the ledger and journal
	 *         tables, false otherwise.)
	 */
	public boolean saveOpeningBalance(String accCode, String subAccCode,
			String d, String accType, String remarks, Connection existingCnn,
			int qryType, ArrayList<String> log) {

		String strQry = null;
		int jv_det_no = UtilMethods.getMaxId("journal_voucher_details_mcg",
				"jv_det_no");
		int jv_no = -1;
		String logFileData = "";
		/*
		 * There are two possible cases here. The user might be creating a new
		 * account head or the user might be updating the opening balance for an
		 * existing account head. If creating a new account head we need to use
		 * an INSERT query, else an UPDATE query.
		 */
		if (qryType == 1) {
			if (accType.equals("3") || accType.equals("4"))
				strQry = "INSERT INTO journal_voucher_details_mcg (jv_det_no, jv_no, acc_code, sub_acc_code,"
						+ " cr_amt, dr_amt, remarks)"
						+ " VALUES ("
						+ jv_det_no
						+ ","
						+ jv_no
						+ ",'"
						+ accCode
						+ "','"
						+ subAccCode
						+ "'," + d + ", 0, '" + remarks + "')";
			else
				strQry = "INSERT INTO journal_voucher_details_mcg (jv_det_no, jv_no, acc_code, sub_acc_code,"
						+ " dr_amt, cr_amt, remarks)"
						+ " VALUES ("
						+ jv_det_no
						+ ","
						+ jv_no
						+ ",'"
						+ accCode
						+ "','"
						+ subAccCode
						+ "'," + d + ", 0, '" + remarks + "')";
		}
		/*
		 * Its the update query. But instead of a update query, we delete the
		 * previous record and insert again. This is because the accType might
		 * have been selected a different one.
		 */
		else {
			String sqlDelete = "DELETE from journal_voucher_details_mcg WHERE jv_no = -1 AND acc_code ='"
					+ accCode + "'";
			log.add("\t\t(JV-Detail-Delete):\tJv_no->-1\tAccCode->" + accCode);

			try {
				st = existingCnn.createStatement();
				st.executeUpdate(sqlDelete);

				

				// because only one row is deleted
				if (accType.equals("3") || accType.equals("4"))
					strQry = "INSERT INTO journal_voucher_details_mcg (jv_det_no, jv_no, acc_code, sub_acc_code"
							+ ", cr_amt, dr_amt, remarks)"
							+ " VALUES ("
							+ jv_det_no
							+ ","
							+ jv_no
							+ ",'"
							+ accCode
							+ "','"
							+ subAccCode + "'," + d + ", 0, '" + remarks + "')";
				else
					strQry = "INSERT INTO journal_voucher_details_mcg (jv_det_no, jv_no, acc_code, sub_acc_code,"
							+ " dr_amt, cr_amt, remarks)"
							+ " VALUES ("
							+ jv_det_no
							+ ","
							+ jv_no
							+ ",'"
							+ accCode
							+ "','"
							+ subAccCode + "'," + d + ", 0, '" + remarks + "')";

			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		try {
			this.st = existingCnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);
			logFileData = "\t\t(Journal-Detail):\tjv_det_no->" + jv_det_no
					+ "jv_no->" + jv_no + "\taccCode->" + accCode
					+ "\tsubAccCode->" + subAccCode + "\tDrAmt->" + d
					+ "\tCrAmt->0\tRemark->" + remarks;
			log.add(logFileData);
			/*
			 * if journal was saved we post the ledger entry for this account
			 * head, else return false
			 */
			if (st.executeUpdate(strQry) == 1) { // only one row is inserted

				// if posting successful, return true
				if (objLedgerController.postOpeningBalance(accCode, subAccCode,
						accType, d, remarks, existingCnn, qryType, log)) {

					
						return true;

				} else {
					return false;
				}
			} else {
				return false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}// end of method saveOpeningBalance()

	/**
	 * @param accCode
	 * @param subAccCode
	 * @param accModel
	 * @param existingCnn
	 * @param qryType
	 * @return boolean value
	 */
	public boolean saveOpeningBalance(String accCode, String subAccCode,
			AccHeadModel accModel, Connection existingCnn, int qryType,
			ArrayList<String> log) {

		String strQry = null;
		String logFileData = "";
		int jv_det_no = UtilMethods.getMaxId("journal_voucher_details_mcg",
				"jv_det_no");
		int jv_no = -1;

		/*
		 * There are two possible cases here. The user might be creating a new
		 * account head or the user might be updating the opening balance for an
		 * existing account head. If creating a new account head we need to use
		 * an INSERT query, else an UPDATE query.
		 */
		if (qryType == 1) {
			if (accModel.getAcc_type().equals("3")
					|| accModel.getAcc_type().equals("4"))
				strQry = "INSERT INTO journal_voucher_details_mcg (jv_det_no, jv_no, acc_code, sub_acc_code,"
						+ " cr_amt, dr_amt, remarks, br_id,narration,created_by,created_date,update_count,updated_by,updated_date)"
						+ " VALUES ("
						+ jv_det_no
						+ ","
						+ jv_no
						+ ",'"
						+ accCode
						+ "','"
						+ subAccCode
						+ "',"
						+ accModel.getMin_bal()
						+ ", 0, '"
						+ accModel.getRemarks()
						+ "','"
						+ 1
						+ "',"
						+ "'AC Head being created','"
						+ ApplicationWorkbenchWindowAdvisor.getLoginId()
						+ "','"
						+ JCalendarFunctions.currentDate()
						+ "',0,'"
						+ ApplicationWorkbenchWindowAdvisor.getLoginId()
						+ "','"
						+ JCalendarFunctions.currentDate()
						+ "')";
			else if (accModel.getAcc_type().equals("5")) {

				if (accModel.getIsDr())
					strQry = "INSERT INTO journal_voucher_details_mcg (jv_det_no, jv_no, acc_code, sub_acc_code"
							+ ", dr_amt, cr_amt, remarks, br_id,narration,created_by,created_date,update_count,updated_by,updated_date)"
							+ " VALUES ("
							+ jv_det_no
							+ ","
							+ jv_no
							+ ",'"
							+ accCode
							+ "','"
							+ subAccCode
							+ "',"
							+ accModel.getMin_bal()
							+ ", 0, '"
							+ accModel.getRemarks()
							+ "','"
							+ 1
							+ "',"
							+ "'AC Head being created','"
							+ ApplicationWorkbenchWindowAdvisor.getLoginId()
							+ "','"
							+ JCalendarFunctions.currentDate()
							+ "',0,'"
							+ ApplicationWorkbenchWindowAdvisor.getLoginId()
							+ "','"
							+ JCalendarFunctions.currentDate()
							+ "')";
				else
					strQry = "INSERT INTO journal_voucher_details_mcg (jv_det_no, jv_no, acc_code, sub_acc_code"
							+ ", cr_amt, dr_amt, remarks, br_id,narration,created_by,created_date,update_count,updated_by,updated_date)"
							+ " VALUES ("
							+ jv_det_no
							+ ","
							+ jv_no
							+ ",'"
							+ accCode
							+ "','"
							+ subAccCode
							+ "',"
							+ accModel.getMin_bal()
							+ ", 0, '"
							+ accModel.getRemarks()
							+ "','"
							+ 1
							+ "',"
							+ "'AC Head being created','"
							+ ApplicationWorkbenchWindowAdvisor.getLoginId()
							+ "','"
							+ JCalendarFunctions.currentDate()
							+ "',0,'"
							+ ApplicationWorkbenchWindowAdvisor.getLoginId()
							+ "','"
							+ JCalendarFunctions.currentDate()
							+ "')";
			}

			else
				strQry = "INSERT INTO journal_voucher_details_mcg (jv_det_no, jv_no, acc_code, sub_acc_code,"
						+ " dr_amt, cr_amt, remarks, br_id,narration,created_by,created_date,update_count,updated_by,updated_date)"
						+ " VALUES ("
						+ jv_det_no
						+ ","
						+ jv_no
						+ ",'"
						+ accCode
						+ "','"
						+ subAccCode
						+ "',"
						+ accModel.getMin_bal()
						+ ", 0, '"
						+ accModel.getRemarks()
						+ "','"
						+ 1
						+ "',"
						+ "'AC Head being created','"
						+ ApplicationWorkbenchWindowAdvisor.getLoginId()
						+ "','"
						+ JCalendarFunctions.currentDate()
						+ "',0,'"
						+ ApplicationWorkbenchWindowAdvisor.getLoginId()
						+ "','"
						+ JCalendarFunctions.currentDate()
						+ "')";
		}
		/*
		 * Its the update query. But instead of a update query, we delete the
		 * previous record and insert again. This is because the accType might
		 * have been selected a different one.
		 */
		else {
			// these two local variables were added by suresh on 25Aug2011
			int updateCount = Integer.parseInt((UtilFxn.GetValueFromTable(
					"journal_voucher_details_mcg", "update_count",
					"jv_no = -1 AND acc_code ='" + accCode + "'").toString())
					.equals("") ? "0" : UtilFxn.GetValueFromTable(
					"journal_voucher_details_mcg", "update_count",
					"jv_no = -1 AND acc_code ='" + accCode + "'").toString());
			String createdDt = UtilFxn.GetValueFromTable(
					"journal_voucher_details_mcg", "created_date",
					"jv_no = -1 AND acc_code ='" + accCode + "'");
			createdDt = createdDt.isEmpty() ? UtilFxn.GetValueFromTable(
					"acc_head_mcg", "created_date", "acc_code ='" + accCode
							+ "'") : createdDt;
			String sqlDelete = "DELETE from journal_voucher_details_mcg WHERE jv_no = -1 AND acc_code ='"
					+ accCode + "'";
			log.add("\n\t\t(JV-Deatil):\tJv_n-->-1\tAccCode->"+accCode);
			

			try {
				st = existingCnn.createStatement();
				st.executeUpdate(sqlDelete);

				

				// because only one row is deleted
				if (accModel.getAcc_type().equals("3")
						|| accModel.getAcc_type().equals("4"))
					strQry = "INSERT INTO journal_voucher_details_mcg (jv_det_no, jv_no, acc_code, sub_acc_code"
							+ ", cr_amt, dr_amt, remarks,  br_id,narration,created_by,created_date,update_count,updated_by,updated_date)"
							+ " VALUES ("
							+ jv_det_no
							+ ","
							+ jv_no
							+ ",'"
							+ accCode
							+ "','"
							+ subAccCode
							+ "',"
							+ accModel.getMin_bal()
							+ ", 0, '"
							+ accModel.getRemarks()
							+ "','"
							+ 1
							+ "',"
							+ "'AC Head updated','"
							+ ApplicationWorkbenchWindowAdvisor.getLoginId()
							+ "','"
							+ createdDt
							+ "','"
							+ (updateCount + 1)
							+ "',"
							+ " '"
							+ ApplicationWorkbenchWindowAdvisor.getLoginId()
							+ "','"
							+ JCalendarFunctions.currentDate()
							+ "')";
				else if (accModel.getAcc_type().equals("5")) {

					if (accModel.getIsDr())
						strQry = "INSERT INTO journal_voucher_details_mcg (jv_det_no, jv_no, acc_code, sub_acc_code"
								+ ", dr_amt, cr_amt, remarks,  br_id,narration,created_by,created_date,update_count,updated_by,updated_date)"
								+ " VALUES ("
								+ jv_det_no
								+ ","
								+ jv_no
								+ ",'"
								+ accCode
								+ "','"
								+ subAccCode
								+ "',"
								+ accModel.getMin_bal()
								+ ", 0, '"
								+ accModel.getRemarks()
								+ "','"
								+ 1
								+ "',"
								+ "'AC Head updated','"
								+ ApplicationWorkbenchWindowAdvisor.getLoginId()
								+ "','"
								+ createdDt
								+ "','"
								+ (updateCount + 1)
								+ "',"
								+ " '"
								+ ApplicationWorkbenchWindowAdvisor.getLoginId()
								+ "','"
								+ JCalendarFunctions.currentDate()+ "')";
					else
						strQry = "INSERT INTO journal_voucher_details_mcg (jv_det_no, jv_no, acc_code, sub_acc_code"
								+ ", cr_amt, dr_amt, remarks,  br_id,narration,created_by,created_date,update_count,updated_by,updated_date)"
								+ " VALUES ("
								+ jv_det_no
								+ ","
								+ jv_no
								+ ",'"
								+ accCode
								+ "','"
								+ subAccCode
								+ "',"
								+ accModel.getMin_bal()
								+ ", 0, '"
								+ accModel.getRemarks()
								+ "','"
								+1
								+ "',"
								+ "'AC Head updated','"
								+ ApplicationWorkbenchWindowAdvisor.getLoginId()
								+ "','"
								+ createdDt
								+ "','"
								+ (updateCount + 1)
								+ "',"
								+ " '"
								+ ApplicationWorkbenchWindowAdvisor.getLoginId()
								+ "','"
								+ JCalendarFunctions.currentDate() + "')";
				}

				else
					strQry = "INSERT INTO journal_voucher_details_mcg (jv_det_no, jv_no, acc_code, sub_acc_code,"
							+ " dr_amt, cr_amt, remarks,  br_id,narration,created_by,created_date,update_count,updated_by,updated_date)"
							+ " VALUES ("
							+ jv_det_no
							+ ","
							+ jv_no
							+ ",'"
							+ accCode
							+ "','"
							+ subAccCode
							+ "',"
							+ accModel.getMin_bal()
							+ ", 0, '"
							+ accModel.getRemarks()
							+ "','"
							+ 1
							+ "',"
							+ "'AC Head updated','"
							+ ApplicationWorkbenchWindowAdvisor.getLoginId()
							+ "','"
							+ createdDt
							+ "','"
							+ (updateCount + 1)
							+ "',"
							+ " '"
							+ ApplicationWorkbenchWindowAdvisor.getLoginId()
							+ "','"
							+ JCalendarFunctions.currentDate()
							+ "')";

			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		try {
			System.out.println("zzz... the journal_voucher_details_mcg qry = "
					+ strQry);
			this.st = existingCnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);

			logFileData = "\n\t(Journal):\tAccType->" + accModel.getAcc_type()
					+ "\tjv_det_no->" + jv_det_no + "\tjv_no->" + jv_no
					+ "\taccCode->" + accCode + "\tsubAccCode->" + subAccCode
					+ "\tBalance->" + accModel.getMin_bal();
			log.add(logFileData);
			/*
			 * if journal was saved we post the ledger entry for this account
			 * head, else return false
			 */
			if (st.executeUpdate(strQry) == 1) { // only one row is inserted

				// if posting successful, return true
				if (objLedgerController.postOpeningBalance(accCode, subAccCode,
						accModel.getAcc_type(), accModel.getMin_bal(),
						accModel.getRemarks(), existingCnn, qryType, log)) {

					
						return true;

				} else {
					return false;
				}
			} else {
				return false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * @param cnn
	 * @param journalQuery
	 * @param ledgerQuery
	 * @return boolean value
	 * 
	 *         This method saves the QuearterlyBalance of account_heads
	 */
	public boolean saveQuarterlyBalance(Connection cnn, String journalQuery,
			String ledgerQuery,ArrayList<String> log) {
		boolean retFlag = false;

		try {
			this.st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);

			/*
			 * if journal was saved we post the ledger entry for this account
			 * head, else return false
			 */
			/* if (st.executeUpdate(journalQuery) > 0) */{ // only one row is
				// inserted

				// if posting successful, return true
				if (objLedgerController.postQuarterlyBalance(cnn, ledgerQuery,log)) {

					/*
					 * if (SaveQueryController.saveQuery(cnn, journalQuery,
					 * "account"))
					 */
					retFlag = true;

				} else {
					retFlag = false;
				}
			} /*
			 * else { retFlag = false; }
			 */

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return retFlag;

	}// end of method saveQuarterlyBalance()

	/**
	 * @param cnn
	 * @param drAmt
	 * @param crAmt
	 * @param postedDate
	 * @return boolean value
	 * 
	 *         This method saves the quarterly profitloss amount into
	 *         acc_code=16
	 */
	public boolean saveQuarterlyPLAmt(Connection cnn, Double drAmt,
			Double crAmt, String postedDate, String profitRoot, String lossRoot,ArrayList<String> log) {
		boolean retFlag = false;

		/*
		 * int jv_det_no = UtilFxn.getMaxId("journal_voucher_details_mcg",
		 * "jv_det_no"); String remarks =
		 * "Quarterly "+(drAmt>0?"Loss":"Profit")+" Amount."; String sql =
		 * "INSERT INTO journal_voucher_details_mcg (jv_det_no, jv_no, acc_code, dr_amt, cr_amt, "
		 * + " created_by, created_date, narration, remarks)" + " VALUES ( " +
		 * jv_det_no + ", -1, '" + (drAmt>0?lossRoot:profitRoot) + "', " + drAmt
		 * + ", " + crAmt + "," + ApplicationWorkbenchWindowAdvisor.getUserId()
		 * + ", '" + ApplicationWorkbenchWindowAdvisor.getDayInDate() +
		 * "', '"+remarks+"', '"+remarks+"')";
		 */

		try {
			this.st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);

			/*
			 * if journal was saved we post the ledger entry, else return false
			 */
			// if (st.executeUpdate(sql) == 1)
			{ // only one row is inserted

				// if posting successful, return true
				if (objLedgerController.postQuarterlyPLAmt(cnn, drAmt, crAmt,
						postedDate, profitRoot, lossRoot,log)) {
					/*
					 * if (SaveQueryController.saveQuery(cnn, sql, "account"))
					 */
					retFlag = true;

				} else {
					retFlag = false;
				}
			}/*
			 * else { retFlag = false; }
			 */

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return retFlag;

	}// end of method saveQuarterlyPLAmt()

} // EOC
