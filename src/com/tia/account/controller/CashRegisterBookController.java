package com.tia.account.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tia.account.model.CashRegisterBookModel;
import com.tia.account.model.ClosingInfoModel;
import com.tia.common.db.DbConnection;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import com.tia.user.controller.LoginSession;



/**
 * @author suresh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 */
public class CashRegisterBookController {
	Connection cnn;
	Statement st;
	ResultSet rs = null;

	/**
	 * @param englishDate
	 * @param acc_code
	 * @return list of all trxns on acc_head that have been posted on date @param englishDate
	 */
	public List<CashRegisterBookModel> generateCashRegisterBook(String englishDate,String acc_code)
	{
		List<CashRegisterBookModel> listCrbm=new ArrayList<CashRegisterBookModel>();
		String strQuery=" SELECT '' intGrp,'' grp,'' name,''address,a.acc_name particulars,l.jv_no, " +
		"''asuliNo,ISNULL(jvm.receipt_no,'')receipt_no,l.dr_amt,l.cr_amt,ISNULL(u.full_name,'')received_by FROM ledger_mcg l " +
		" INNER JOIN journal_voucher_mcg jvm ON l.jv_no=jvm.jv_no " +
		" AND (jvm.to_from_type IS NULL OR jvm.to_from_type='') " +
		" AND l.jv_no<>-1 AND l.acc_code like '"+acc_code+".%' AND l.posted_date='"+englishDate+"' " +
		" INNER JOIN acc_head_mcg a ON l.to_acc_code=a.acc_code " +
		" INNER JOIN user_accounts u ON jvm.received_by=u.user_id " +
		" UNION " +
		" SELECT  mgm1.name intGrp,mgm.name grp,mrm.name name,cvm.cv_lbl +','+mrm.ward+','+mrm.tole address ,a.acc_name particulars,l.jv_no," +
		" '' asuliNo,ISNULL(jvm.receipt_no,'')receipt_no,l.dr_amt,l.cr_amt,ISNULL(u.full_name,'')received_by FROM ledger_mcg l" +
		" INNER JOIN journal_voucher_mcg jvm ON l.jv_no=jvm.jv_no" +
		" AND (jvm.to_from_type='account'  AND jvm.receipt_no IS NOT NULL)" +
		" AND l.jv_no<>-1 AND l.acc_code like '"+acc_code+".%' AND l.posted_date='"+englishDate+"'" +
		" INNER JOIN acc_head_mcg a ON l.to_acc_code=a.acc_code" +
		" INNER JOIN cash_income_mcg cim ON jvm.receipt_no=cim.receipt_no" +
		" INNER JOIN mem_reg_mcg mrm ON cim.grp_id=mrm.id" +
		" INNER JOIN mem_group_mcg mgm ON mrm.grp_no=mgm.group_id" +
		" INNER JOIN mem_group_mcg mgm1 ON mgm.parent_id=mgm1.group_id" +
		" INNER JOIN user_accounts u ON jvm.created_by=u.user_id" +
		" INNER JOIN code_value_mcg cvm ON mrm.vdc_id=cvm.cv_code AND cvm.cv_type='vdc_muncipality'" +
		" UNION" +		
		" SELECT (case when mrm.mem_in=1 then '' when mrm.mem_in=2 then (select mm.name from mem_group_mcg m inner join mem_group_mcg mm" +
		" on m.parent_id=mm.group_id where mrm.grp_no=m.group_id ) else '' end )as intGrp, " +
		" (case when mrm.mem_in=1 then '' when mrm.mem_in=2 then (select name from mem_group_mcg where mrm.grp_no=group_id) else '' end ) grp,mrm.name name," +
		" cvm.cv_lbl +','+mrm.ward+','+mrm.tole  address ,a.acc_name particulars,l.jv_no," +
		" lcrm.lcsn asuliNo,ISNULL(jvm.receipt_no,'')receipt_no,l.dr_amt,l.cr_amt,ISNULL(u.full_name,'')received_by FROM ledger_mcg l" +
		" INNER JOIN journal_voucher_mcg jvm ON l.jv_no=jvm.jv_no AND (jvm.to_from_type='loan') AND l.jv_no<>-1 AND l.acc_code like '"+acc_code+".%' AND l.posted_date='"+englishDate+"'" +
		" INNER JOIN acc_head_mcg a ON l.to_acc_code=a.acc_code" +
		" INNER JOIN loan_transaction_mcg ltm ON jvm.tran_id=ltm.trxn_ses_id" +
		" INNER JOIN loan_request_mcg lrm ON ltm.loan_id=lrm.loan_id" +
		" INNER JOIN mem_reg_mcg mrm ON lrm.mid=mrm.id" +
		" INNER JOIN loan_collection_rpt_mcg lcrm ON ltm.tid=lcrm.tid AND ltm.loan_id=lcrm.loan_id" +
		" INNER JOIN user_accounts u ON jvm.created_by=u.user_id" +
		" INNER JOIN code_value_mcg cvm ON mrm.vdc_id=cvm.cv_code AND cvm.cv_type='vdc_muncipality'  "+
		" UNION "+
		" SELECT mai.int_grp_name as intgrp, mai.group_name grp, mai.mem_name name, mai.mem_address address, mai.scheme_name as particulars, l.jv_no, '' asuliNo," +
		" ISNULL(jvm.receipt_no,'')receipt_no,l.dr_amt,l.cr_amt,u.full_name received_by  FROM ledger_mcg l" +
		" INNER JOIN journal_voucher_mcg jvm ON l.jv_no=jvm.jv_no AND jvm.to_from_type='saving' AND l.jv_no<>-1 AND l.acc_code like '"+acc_code+".%' AND l.posted_date='"+englishDate+"'" +
		" INNER JOIN sav_transaction st ON jvm.tran_id=st.trxn_id " +
		" inner join mem_acc_info mai on st.acc_no = mai.acc_no " +
		" INNER JOIN user_accounts u ON jvm.created_by=u.user_id " +
		" UNION " +
		" SELECT (case when mrm.mem_in=1 then '' when mrm.mem_in=2 then (select mm.name from mem_group_mcg m inner join mem_group_mcg mm" +
		" on m.parent_id=mm.group_id where mrm.grp_no=m.group_id ) else '' end )as intGrp, " +
		" (case when mrm.mem_in=1 then '' when mrm.mem_in=2 then (select name from mem_group_mcg where mrm.grp_no=group_id) else '' end ) grp," +
		" mrm.name name,cvm.cv_lbl +','+mrm.ward+','+mrm.tole address , " +
		" ssm.scheme_name particulars,l.jv_no, '' asuliNo,ISNULL(jvm.receipt_no,'')receipt_no,l.dr_amt,l.cr_amt,ISNULL(u.full_name,'')received_by " +
		" FROM ledger_mcg l INNER JOIN journal_voucher_mcg jvm ON l.jv_no=jvm.jv_no AND jvm.to_from_type='share' AND l.jv_no<>-1 AND l.acc_code like '"+acc_code+".%' AND l.posted_date='"+englishDate+"'" +
		" INNER JOIN share_trans_mcg stm ON jvm.tran_id=stm.share_trans_id and jvm.jv_no=stm.jv_no " +
		" INNER JOIN share_crt_mcg scm on stm.share_crt_no=scm.share_crt_no " +
		" INNER JOIN share_holder_mcg shm on scm.buyer_share_holder_id=shm.share_holder_id " +
		" INNER JOIN mem_reg_mcg mrm on shm.mid=mrm.mid " +
		" INNER JOIN share_scheme_mcg ssm on stm.share_scheme_id=ssm.share_scheme_id " +
		" INNER JOIN user_accounts u ON jvm.created_by=u.user_id " +
		" INNER JOIN code_value_mcg cvm ON mrm.vdc_id=cvm.cv_code AND cvm.cv_type='vdc_muncipality' ORDER BY jv_no";

		rs = null;

		try {
			System.out.println("Cash register Book report qry="+strQuery);
			cnn=DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rs = st.executeQuery(strQuery);

			if (rs == null)
			{
				return null;

			}
			else {
				while (rs.next()) {
					CashRegisterBookModel tmpCrbm=new CashRegisterBookModel();
					tmpCrbm.setInterGroupNo(rs.getString("intGrp"));
					tmpCrbm.setGroupNo(rs.getString("grp"));
					tmpCrbm.setMemberName(rs.getString("name"));
					tmpCrbm.setAddress(rs.getString("address"));					
					tmpCrbm.setParticulars(rs.getString("particulars"));
					tmpCrbm.setJvNo(rs.getString("jv_no"));
					tmpCrbm.setAsuliPurjaNo(rs.getString("asuliNo"));
					tmpCrbm.setReceiptNo(rs.getString("receipt_no"));
					tmpCrbm.setDrAmt(rs.getString("dr_amt"));
					tmpCrbm.setCrAmt(rs.getString("cr_amt"));
					tmpCrbm.setReceivedBy(rs.getString("received_by"));	
					//add the initialized model instance into ArrayList
					listCrbm.add(tmpCrbm);
				}
			}
		}//try ends 
		catch (SQLException e) 
		{
			System.out.println("The exception while loading CashRegisterBook.. " + e.toString());
			e.printStackTrace();
		}
		return listCrbm;

	}//method generateCashRegisterBook() ends
	/**
	 * @param todayDate
	 * @param acc_code
	 * @return balance on acc_code till yesterday.i.e. balance before @param todayDate
	 */
	public double getBalanceTillYesterday(String todayDate,String acc_code)
	{
		double balance=0.00;
		JCalendarFunctions jcf =new JCalendarFunctions();
		ClosingInfoModel cim=LoginSession.getClosingInfo(todayDate);
		String strQry=" SELECT ABS(SUM(dr_amt)-SUM(cr_amt))blance FROM ledger_mcg " +
		" WHERE acc_code like '"+acc_code+".%' and jv_no<>-1 AND posted_date between '"+cim.getStartDateEn()+"' " +
		" AND '"+jcf.addSubractDaysFrmEngDate(java.sql.Date.valueOf(todayDate), "minus", 1, "yyyy-MM-dd")+"' ";
		rs = null;

		try {
			System.out.println("Cash register Book's getBalanceCFD qry="+strQry);
			cnn=DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rs = st.executeQuery(strQry);

			if (rs == null)
			{
				return 0;

			}
			else {
				while (rs.next()) {
					balance=balance+rs.getDouble("blance");
				}
			}
		}//try ends 
		catch (SQLException e) 
		{
			System.out.println("The exception while loading CashRegisterBook.. " + e.toString());
			e.printStackTrace();
		}
		return balance;
	}//end of method getBalanceTillYesterday()
}//end of class
