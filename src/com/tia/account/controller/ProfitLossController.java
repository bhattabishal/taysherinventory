package com.tia.account.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.model.ClosingInfoModel;
import com.tia.account.model.LedgerModel;
import com.tia.account.model.TempBalanceSheetModel;
import com.tia.account.model.Temp_profit_loss;
import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import com.tia.user.controller.LoginSession;



/**
 * @author minesh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 * @altered & revised by suresh
 * 
 */
public class ProfitLossController {
	private static Connection cnn;
	private ResultSet rs;
	private static Statement st;

	SimpleDateFormat simpleDateFormat;
	JCalendarFunctions jCalendarFunctions;

	public ProfitLossController() {
		cnn = DbConnection.getConnection();
		simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		jCalendarFunctions = new JCalendarFunctions();

	}

	/**
	 * @param toDate
	 * @param hierarchyLevel
	 * @return resultset 
	 * 
	 * This method returns resultset containing all income heads
	 * with calculation of their dr/cr balances till @param toDate.
	 */
	public ResultSet loadIncome(String toDate,String hierarchyLevel,String frmDate) {
		String profitRoot=UtilFxn.GetValueFromTable("SELECT acc_code from acc_fxn_mapping_mcg where map_for='Profit Account'");
		if(profitRoot.equals(""))
		{
			MessageBox mb = new MessageBox(Display.getCurrent().getActiveShell(),SWT.ICON_ERROR);
			mb.setText("Code Not Found");
			mb.setMessage("Insert Profit Code In FixMapingTable");
			mb.open();
			return null;
		}

		String lastOpeningBalLed_id=UtilFxn.GetMaxValueFromTable("ledger_mcg", "led_id", " jv_no = -1 AND posted_date <='"+toDate+"'");
		String andOrLCD=(lastOpeningBalLed_id.equals("") ?" OR ":" AND ");
		ClosingInfoModel cimm=LoginSession.getClosingInfo(toDate);

		//String date_cond=" between '"+cimm.getPreviousClosingDate()+"' and '"+toDate+"'";
		String date_cond=" between '"+frmDate+"' and '"+toDate+"'";
		String strQry =" ";

//		if(toDate.equals(ApplicationWorkbenchWindowAdvisor
//				.getCurClosingPeriodEndMonthEnDt().toString()) && isClosingPerformed(toDate))
//		{
//			date_cond=" ='"+toDate+"' ";
//			strQry="SELECT acc_code,acc_name,alias,amt FROM (SELECT '0' as acc_code,'last profit' acc_name,'' alias,(SELECT (SUM(cr_amt)-SUM(dr_amt)) FROM ledger_mcg  where acc_code='"+profitRoot+"'" +
//			" and posted_date "+date_cond+")amt) t where amt<>0";
//		}
		//else
		//{
			strQry="SELECT acc_code,acc_name,alias,amt FROM (SELECT '0' as acc_code,'last profit' acc_name,'' alias,(SELECT (SUM(cr_amt)-SUM(dr_amt)) FROM ledger_mcg  where acc_code='"+profitRoot+"'" +
			" and posted_date "+date_cond+")amt) t where amt<>0 "+
			" UNION ALL "+
			"SELECT acc_code,acc_name,alias,amt FROM  (SELECT acc_code,acc_name,alias,"
			+ "(SELECT (SUM(cr_amt)-SUM(dr_amt)) FROM ledger_mcg l "
			+ "WHERE (l.acc_code LIKE a.acc_code + '.%' or l.acc_code = a.acc_code) "
			+ "AND fy_id="
			+ ApplicationWorkbenchWindowAdvisor.getFiscalId()
			+ " AND jv_no <> -1 "
			+ " "+andOrLCD+"  posted_date between'"+frmDate+"'"
			+ " AND '"+toDate
			+ "'  )AS Amt,parent "
			+ "FROM acc_head_mcg as a WHERE (acc_type='3')) AS t1 where Amt<>0 "
			+ (hierarchyLevel.equalsIgnoreCase("1")?" and parent=0 ":" ") ;
		//}
		strQry=strQry + " ORDER BY acc_code ";

		System.out.println("load income query: " + strQry);
		try {
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);

			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param toDate
	 * @param hierarchyLevel
	 * @return resulset
	 * method to load all transaction details related to Expense Heads
	 */
	public ResultSet loadExpenses( String toDate,String hierarchyLevel,String frmDate) {
		String lossRoot=UtilFxn.GetValueFromTable("SELECT acc_code from acc_fxn_mapping_mcg where map_for='Loss Account'");
		if(lossRoot.equals(""))
		{
			MessageBox mb = new MessageBox(Display.getCurrent().getActiveShell(),SWT.ICON_ERROR);
			mb.setText("Code Not Found");
			mb.setMessage("Insert Loss Code In FixMaping Table");
			mb.open();
			return null;
		}

		String strQry="";
		//ClosingInfoModel cimm=LoginSession.getClosingInfo(toDate);
		String date_cond=" between '"+frmDate+"' and '"+toDate+"'";

//		if(toDate.equals(ApplicationWorkbenchWindowAdvisor
//				.getCurClosingPeriodEndMonthEnDt().toString()) && isClosingPerformed(toDate))
//		{
//			date_cond=" ='"+toDate+"' ";
//			strQry="SELECT acc_code,acc_name,alias,amt FROM (SELECT '0' as acc_code,'ut vf= j=sf] gf]S;fg' acc_name,'' alias,(SELECT (SUM(dr_amt)-SUM(cr_amt)) FROM ledger_mcg  where acc_code='"+lossRoot+"'" +
//			" and posted_date "+date_cond+")amt)t where amt<>0";
//		}
		//else
		//{
			strQry ="SELECT acc_code,acc_name,alias,amt FROM (SELECT '0' as acc_code,'last expense' acc_name,'' alias,(SELECT (SUM(dr_amt)-SUM(cr_amt)) FROM ledger_mcg  where acc_code='"+lossRoot+"'" +
			" and posted_date "+date_cond+")amt)t where amt<>0 "+
			" UNION ALL "+ 
			"SELECT acc_code,acc_name,alias,amt FROM (SELECT acc_code, acc_name,alias, "
			+ "(SELECT (SUM(dr_amt)-SUM(cr_amt))amt FROM ledger_mcg l "
			+ "WHERE (l.acc_code LIKE a.acc_code + '.%' OR l.acc_code=a.acc_code) "
			+ "AND fy_id="
			+ ApplicationWorkbenchWindowAdvisor.getFiscalId()
			+ " AND jv_no <> -1 " 
			+		" AND posted_date between '"+frmDate+"'"
			+ " AND '"+toDate
			+ "'  )AS Amt,parent  "
			+ "FROM acc_head_mcg AS a WHERE (acc_type='2')) AS T1  where Amt<>0 "
			+ (hierarchyLevel.equalsIgnoreCase("1")?" and parent=0 ":" ")   ;
		//}
		strQry=strQry + " ORDER BY acc_code ";

		System.out.println("load expenses query: " + strQry);

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);

			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
	/**
	 * @param toDate
	 * @param fiscalYear
	 * @param closingPerformed
	 * @param hierarchyLevel
	 * @param rpt_time
	 * @param rpt_type
	 * @return collection
	 * 
	 * method to get Profit or Loss amounts of closing
	 */
	public List<TempBalanceSheetModel> loadPLClosing( String toDate,
			int fiscalYear, boolean closingPerformed, String hierarchyLevel,int rpt_time, int rpt_type,String frmDate) {
		List<TempBalanceSheetModel> lstTBSM = new ArrayList<TempBalanceSheetModel>();
		lstTBSM.clear();
		DecimalFormat df= new DecimalFormat("##,##0.00");
		double incTotal = 0, expTotal = 0,profitLoss=0,amt=0;
		try {
			// PL rule Debit all Expenses and losses, and Credit all Income and
			// Gains
			ResultSet incomeRs = loadIncome(toDate,"2",frmDate);
			ResultSet expenseRs = loadExpenses(toDate,"2",frmDate);

			// calculate the total expenses amount
			while (expenseRs.next()) {
				// add the amounts of the top level account codes only
				if (expenseRs.getString(1).matches("[0-9]*"))
					expTotal = expTotal + expenseRs.getDouble("Amt");

			}
			// calculate the total income amount
			while (incomeRs.next()) {
				// add the amounts of the top level account codes only
				if (incomeRs.getString(1).matches("[0-9]*"))
					incTotal = incTotal + incomeRs.getDouble("Amt");

			}
			profitLoss = incTotal - expTotal;
			// set the resultset cursor before the first row
			expenseRs.beforeFirst();
			// set the resultset cursor before the first row
			incomeRs.beforeFirst();
			//add Income Label
			TempBalanceSheetModel objModel = new TempBalanceSheetModel();
			objModel.setAccount_alias("");
			objModel.setParticulars("Income Magnus Nepali");
			objModel.setTot_sub_head("");					
			objModel.setTot_head("");
			lstTBSM.add(objModel);
			//The income details
			int i=1+UtilMethods.getMaxId("acc_monthly_closing_mcg", "id");
			while(incomeRs.next())
			{
				//int reportid =ApplicationWorkbenchWindowAdvisor.getUserId();
				objModel = new TempBalanceSheetModel();
				//objModel.setId(reportid);
				objModel.setRpt_order(i);
				objModel.setBr_id(1);
				String acc_code=incomeRs.getString("acc_code");
				objModel.setAcc_code(acc_code);

				amt=incomeRs.getDouble("amt");
				String alias=incomeRs.getString("alias");
				String acc_name=incomeRs.getString("acc_name");
				if (acc_code.matches("[0-9]*")) {
					objModel.setAccount_alias(alias);
					objModel.setParticulars(acc_name);
					objModel.setTot_head(df.format(amt));
					objModel.setTot_sub_head("");
				}
				else if (acc_code.matches("[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("   " +alias);
					objModel.setParticulars("   " +acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else if (acc_code.matches("[0-9]*.[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("      " +alias);
					objModel.setParticulars("      " +acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else if (acc_code.matches("[0-9]*.[0-9]*.[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("         " + alias);
					objModel.setParticulars("         " + acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else if (acc_code.matches("[0-9]*.[0-9]*.[0-9]*.[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("            " +alias);
					objModel.setParticulars( "            " +acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else {
					objModel.setAccount_alias("               " +alias);
					objModel.setParticulars("               "+acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				}
				/*objModel.setAccount_alias(rs.getString("alias"));
					objModel.setParticulars(rs.getString("acc_name"));*/

				lstTBSM.add(objModel);
				i++;

			}//while ends
			//income summaries
			// if loss occurs, show it
			if (profitLoss < 0) {
				objModel = new TempBalanceSheetModel();
				objModel.setAccount_alias("");
				objModel.setParticulars("Loss Magnus Nepali");
				objModel.setTot_sub_head(df.format(Math.abs(profitLoss)));					
				objModel.setTot_head("");
				lstTBSM.add(objModel);

				incTotal = incTotal + Math.abs(profitLoss);
				// loss = Math.abs(incTotal);

				//loss = Math.abs(profitLoss);
			}
			//income total
			objModel = new TempBalanceSheetModel();
			objModel.setAccount_alias("");
			objModel.setParticulars("reportTotal");
			objModel.setTot_sub_head("");					
			objModel.setTot_head(df.format(incTotal));
			lstTBSM.add(objModel);
			//expense details
			while(expenseRs.next())
			{
				//int reportid =ApplicationWorkbenchWindowAdvisor.getUserId();
				objModel = new TempBalanceSheetModel();
				//objModel.setId(reportid);
				objModel.setRpt_order(i);
				objModel.setBr_id(1);
				String acc_code=expenseRs.getString("acc_code");
				objModel.setAcc_code(acc_code);

				amt=expenseRs.getDouble("amt");
				String alias=expenseRs.getString("alias");
				String acc_name=expenseRs.getString("acc_name");
				if (acc_code.matches("[0-9]*")) {
					objModel.setAccount_alias(alias);
					objModel.setParticulars(acc_name);
					objModel.setTot_head(df.format(amt));
					objModel.setTot_sub_head("");
				}
				else if (acc_code.matches("[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("   " +alias);
					objModel.setParticulars("   " +acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else if (acc_code.matches("[0-9]*.[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("      " +alias);
					objModel.setParticulars("      " +acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else if (acc_code.matches("[0-9]*.[0-9]*.[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("         " + alias);
					objModel.setParticulars("         " + acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else if (acc_code.matches("[0-9]*.[0-9]*.[0-9]*.[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("            " +alias);
					objModel.setParticulars( "            " +acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else {
					objModel.setAccount_alias("               " +alias);
					objModel.setParticulars("               "+acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				}
				/*objModel.setAccount_alias(rs.getString("alias"));
					objModel.setParticulars(rs.getString("acc_name"));*/

				lstTBSM.add(objModel);
				i++;

			}//while ends
			//expense summaries
			if (profitLoss > 0) {

				objModel = new TempBalanceSheetModel();
				objModel.setAccount_alias("");
				objModel.setParticulars("Profit Magnus Nepali");
				objModel.setTot_sub_head(df.format(profitLoss));					
				objModel.setTot_head("");
				lstTBSM.add(objModel);

				expTotal += profitLoss;
				//profit = Math.abs(profitLoss);
			}
			//total expense
			objModel = new TempBalanceSheetModel();
			objModel.setAccount_alias("");
			objModel.setParticulars("report Total");
			objModel.setTot_sub_head("");					
			objModel.setTot_head(df.format(expTotal));
			lstTBSM.add(objModel);

			//Now save the data for future use
			int id=UtilMethods.getMaxId("acc_monthly_closing_mcg", "id");//offset value
			//reassign the value of i for other use
			i=0;
			int br_id=1;
			String trxn_date=jCalendarFunctions.currentDate();
			String trxn_np_date=jCalendarFunctions.getCurrentNepaliDate();
			String npDate[]=trxn_np_date.split(ApplicationWorkbenchWindowAdvisor.getDateSeperator());
			int quarter=ApplicationWorkbenchWindowAdvisor.getCurrentClosingPeriodId();
			int creator_id=ApplicationWorkbenchWindowAdvisor.getLoginId();


			for (Iterator<TempBalanceSheetModel> iterator = lstTBSM
					.iterator(); iterator.hasNext();) {
				TempBalanceSheetModel tbsm = iterator.next();
				String futureData="INSERT INTO [acc_monthly_closing_mcg] " +
				"([id],[br_id],[fy_id],[date],[nep_date],[nep_month],[nep_year],[quarter],[acc_code] ,[alias],[acc_name],[dr_amt]" +
				" ,[cr_amt],[created_by],[created_dt] ,[update_count],[summary_time],[summary_type]) " +
				" VALUES " +
				" ("+(id+i)+","+br_id+","+fiscalYear+",'"+trxn_date+"','"+trxn_np_date+"',"+Integer.valueOf(npDate[1])+","+Integer.valueOf(npDate[0])+", " +
				" "+quarter+", '"+tbsm.getAcc_code()+"', '"+tbsm.getAccount_alias()+"', '"+tbsm.getParticulars().replaceAll("'", "''").replaceAll("\"", "\\\"")+"','"+tbsm.getTot_sub_head()+"','"+tbsm.getTot_head()+"',"+creator_id+",GETDATE(),0,"+rpt_time+","+rpt_type+" ) ";
				cnn = DbConnection.getConnection();
				cnn.setAutoCommit(false);
				st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_UPDATABLE);
				if(st.executeUpdate(futureData)>0)
				{
					
						cnn.commit();
						cnn.setAutoCommit(true);
					
					
				}
				else
				{
					cnn.rollback();
					cnn.setAutoCommit(true);
				}
				//increment value of i
				i++;
			}

		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}


		return lstTBSM;
	}

	/**
	 * @return amount
	 * method to get Profit or Loss amount of previous closing
	 */
	public double getLastClosingPL()
	{
		double retVal=0.00;
		String lossRoot=UtilFxn.GetValueFromTable("SELECT acc_code from acc_fxn_mapping_mcg where map_for='Loss Account'");
		if(lossRoot.equals(""))
		{
			MessageBox mb = new MessageBox(Display.getCurrent().getActiveShell(),SWT.ICON_ERROR);
			mb.setText("Code Not Found");
			mb.setMessage("Insert Loss Code In Fix Maping Table");
			mb.open();
			return 0.00;
		}
		String profitRoot=UtilFxn.GetValueFromTable("SELECT acc_code from acc_fxn_mapping_mcg where map_for='Profit Account'");
		if(profitRoot.equals(""))
		{
			MessageBox mb = new MessageBox(Display.getCurrent().getActiveShell(),SWT.ICON_ERROR);
			mb.setText("Code Not Found");
			mb.setMessage("Insert Profit Code In Fix Maping Table");
			mb.open();
			return 0.00;
		}
		String plValue= UtilFxn.GetValueFromTable("select (dr_amt-cr_amt)as PL_Val from ledger_mcg where jv_no=-1 and is_opening=1 " +
				" and posted_date=(select max(posted_date) from ledger_mcg where jv_no=-1 and is_opening=1) and acc_code in ('"+profitRoot+"','"+lossRoot+"')");
		retVal=Double.valueOf(plValue);
		return retVal;
	}
	/**
	 * @param accCode
	 * @return boolean value
	 * method to check if this @param accCode is ultimate child code or not
	 */
	public boolean isChildAccCode(String accCode) {
		boolean flag = false;
		String strQry = "SELECT * FROM acc_head_mcg WHERE parent = "
			+ "(SELECT acc_head_id FROM acc_head_mcg WHERE acc_code='"
			+ accCode + "')";

		try {
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);

			if (rs.next())
				flag = true;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * @param leadger
	 * @return boolean value
	 * 
	 */
	public boolean transferPLToAccHead(LedgerModel leadger) {

		int ledgerId = UtilMethods.getMaxId("ledger_mcg", "led_id");
		String strQry;
		Statement st;
		try {
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			if (leadger.getLed_id() <= 0) {
				// leadger.getLed_id(ledgerId);
				strQry = leadger.Insert();
			} else {
				leadger.setUpdated_date(new SimpleDateFormat("yyyy-MM-dd")
				.format(new Date()));
				leadger.z_WhereClause.led_id(leadger.getLed_id());
				strQry = leadger.Update();
			}

			int result = st.executeUpdate(strQry);
			st.close();

			if (result == 1
					)
				return true;
			else
				return false;
		} catch (SQLException e) {
			return false;

		}
	}

	/**
	 * @param profitCode
	 * @param lossCode
	 * @return resultset
	 * method to get pl entries of prev-closing
	 */
	public ResultSet getQuarterPLEntries(String profitCode,String lossCode) {
		String sql = "select l.acc_code,l.dr_amt,l.cr_amt,(select acc_name from acc_head_mcg where acc_code=l.acc_code)acc_name from ledger_mcg l " +
		" where jv_no = -1 and is_opening=1 and acc_code in( '"+profitCode+"','"+lossCode+"') " +
		" AND posted_date =(select max(posted_date) from ledger_mcg where jv_no=-1 and is_opening=1) order by posted_date ";

		try {
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;

	}

	/**
	 * @param toDate
	 * @return boolean value
	 * method that checks if PL is transferred to some funds or not
	 */
	public boolean isPLTransferred(String toDate) {
		boolean retFlag = false;
		/*Date date = null;

		try {
			date = sdf.parse(ApplicationWorkbenchWindowAdvisor.getCurClosingPeriodStartMonthEnDt().toString());
		} catch (ParseException e1) {
			e1.printStackTrace();
		}*/

		/*		String sql = "SELECT COUNT(*) AS total FROM ledger_mcg WHERE led_id > (SELECT MAX(led_id) FROM ledger_mcg"
				+ " WHERE jv_no = -1 AND posted_date = '"
				+ jcal.addSubractDaysFrmEngDate(date, "minus", 1, "yyyy-MM-dd")
				+ "') AND posted_date =' "
				+ jcal.addSubractDaysFrmEngDate(date, "minus", 1, "yyyy-MM-dd")
				+ "'";
		 */
		String sql = "SELECT COUNT(*) AS total FROM ledger_mcg WHERE led_id > (SELECT MAX(led_id) FROM ledger_mcg"
			+ " WHERE jv_no = -1 AND posted_date = "
			+ "(SELECT MAX(posted_date) FROM ledger_mcg WHERE jv_no = -1 AND posted_date <= '"+toDate+"')"
			+ ") AND posted_date = "
			+ "(SELECT MAX(posted_date) FROM ledger_mcg WHERE jv_no = -1 AND posted_date <= '"+toDate+"')";


		//+ "(SELECT MAX(posted_date) FROM ledger_mcg WHERE jv_no = -1 AND posted_date <= '"+toDate+"')"		
		System.out.println("isPlTransferred: " + sql);

		try {
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

			if (rs.next()) {
				if (rs.getInt("total") > 0)
					retFlag = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retFlag;
	}//end of method
	/**
	 * method to delete all data of PL from Temporary table.
	 * This is needed because, on repeated printing, previously
	 * existing data is repeated in reports, which is not necessary.
	 */
	public static void deleteallTempRptData()
	{
		String strQry="";
		try {
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			strQry="DELETE FROM temp_profit_loss ";
			System.out.println("zzz.. The query to delete temp values of profit_loss is :" +strQry);
			st.executeUpdate(strQry);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//end of method

	/**
	 * @param tempPL
	 * @return boolean value
	 * 
	 * method to insert values from GUI grid to temporary table
	 */
	public static boolean insertTempPL(ArrayList<Temp_profit_loss> tempPL)
	{
		boolean retFlag=false;
		for(int i=0;i<tempPL.size();i++)
		{
			String strQuery=" INSERT INTO [temp_profit_loss]"
				+"([account_alias] ,[particulars] ,[tot_sub_head],[tot_head],[br_id] ,[rpt_order]) "
				+" VALUES ("
				+" '"+tempPL.get(i).getAccount_alias()+"' "
				+" ,'"+tempPL.get(i).getParticulars()+"' "
				+" ,'"+tempPL.get(i).getTot_sub_head()+"' "
				+" ,'"+tempPL.get(i).getTot_head()+"' "
				+" ,'"+tempPL.get(i).getBr_id()+"' "
				+" ,'"+tempPL.get(i).getRpt_order()+"') ";

			try {
				//st = cnn.createStatement(); //such statement is used for select Qyery
				cnn.setAutoCommit(false);
				st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
				if (st.executeUpdate(strQuery) > 0)
				{
					retFlag=true;
					System.out.println("zzz...The tmp data item "+i+"  has been inserted into tmp_PL table!");
					cnn.commit();
				}			
				else
				{
					cnn.rollback();
					System.out.println("zkk..The tmp data item "+i+" insertion has been failed !");
				}

			} catch (SQLException e) {
				retFlag=false;
				e.printStackTrace();
			}
		}//for ends

		return retFlag;
	}
	/**
	 * @param date
	 * @return boolean value
	 * method to check if closing for this accounting-period 
	 * has been performed or not till @param date
	 */
	public boolean isClosingPerformed(String date) {
		boolean retFlag = false;
		String sql = "SELECT COUNT(*) AS total FROM ledger_mcg WHERE jv_no = -1 and is_opening=1 AND posted_date = '"
			+ date + "'";

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

			if (rs.next()) {
				if (rs.getInt("total") >= 1)
					retFlag = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retFlag;
	}//end of method isClosingPerformed()
}//end of class
