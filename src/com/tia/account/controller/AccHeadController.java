package com.tia.account.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;



import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.model.AccHeadModel;
import com.tia.common.db.DbConnection;
import com.tia.common.util.LogFileCreate;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;



public class AccHeadController {
	private Connection cnn = DbConnection.getConnection();
	private Statement statement;
	private ResultSet resultset = null;
	private JournalEntryController journalEntryController;
	ArrayList<String> log = new ArrayList<String>();

	public AccHeadController() {
		// db connection
		this.cnn = DbConnection.getConnection();

		journalEntryController = new JournalEntryController();
	}

	/**
	 * Returns the details of an account head whose account id (account_head_id)
	 * is accId
	 * 
	 * @param accId
	 *            (Id of the account head under consideration.)
	 * @return AccHeadModel (object of type AccHeadModel holding the details of
	 *         the account head)
	 */
	public AccHeadModel getAccount(String accId) {
		AccHeadModel accHeadModel = new AccHeadModel();
		String strQry = "";
		if (accId.equals(""))
			strQry = "SELECT *,(CASE WHEN parent IS NULL THEN '' ELSE "
					+ "(SELECT acc_name FROM acc_head_mcg WHERE acc_head_id = A.parent) END) AS parent_name,"
					+ "(SELECT COUNT(acc_name) FROM acc_head_mcg WHERE parent = A.acc_head_id) AS pcount"
					+ "FROM acc_head_mcg AS A WHERE parent=0";
		else
			strQry = "SELECT *,(CASE WHEN parent=0 THEN '' ELSE"
					+ "(SELECT acc_name FROM acc_head_mcg where acc_head_id = A.parent) END) AS parent_name,"
					+ "(SELECT count(acc_name) FROM acc_head_mcg WHERE parent = A.acc_head_id) AS pcount"
					+ " FROM acc_head_mcg AS A WHERE acc_head_id='" + accId
					+ "'";

		System.out.println("zzz...Acc Detail Query =" + strQry);
		try {
			this.statement = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = statement.executeQuery(strQry);

			if (rs.next()) {
				accHeadModel.setAcc_code(rs.getString("acc_code"));
				accHeadModel.setAcc_head_id(rs.getInt("acc_head_id"));
				accHeadModel.setBr_id(rs.getInt("br_id"));
				accHeadModel.setAlias(rs.getString("alias"));
				accHeadModel.setAcc_name(rs.getString("acc_name"));
				accHeadModel.setAcc_type(rs.getString("acc_type"));
				accHeadModel.setParent(rs.getString("parent"));
				accHeadModel.setAcc_level(rs.getInt("acc_level"));
				accHeadModel.setMin_bal(rs.getString("min_bal"));
				accHeadModel.setRemarks(rs.getString("remarks"));
				accHeadModel.setParent_name(rs.getString("parent_name"));
				accHeadModel.isParent = false;

				if (!rs.getString("pcount").equals("")
						&& (Integer.parseInt(rs.getString("pcount"))) > 0)
					accHeadModel.isParent = true;
				accHeadModel.setDrcr(rs.getString("drcr"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return accHeadModel;
	}

	/**
	 * This method checks whether any account entry in acc_head_mcg table has
	 * been entered in the acc_sub_head_mcg table.
	 * 
	 * @param accCode
	 *            (account code of the account under consideration)
	 * @return count (-ve value when no entry is available in the
	 *         acc_sub_head_mcg table else total count of the entries in the
	 *         mentioned table)
	 */
	public int getAccCodeCount(String accCode) {
		String strQry = "SELECT COUNT(acc_code) FROM acc_sub_head_mcg "
				+ "WHERE acc_code='" + accCode + "'";

		try {
			this.statement = cnn.createStatement();
			ResultSet rs = statement.executeQuery(strQry);
			if (rs.next())
				return (rs.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;

	}

	/**
	 * Calls the sql server scalar-valued function max_acc_code by passing the
	 * function an accId of an account head and retrieves the new account code
	 * for the account entry under the passed account head's id. If
	 * unsuccessful, returns -ve.
	 * 
	 * For parent level account heads, an accId of 0 is passed.
	 * 
	 * @param accId
	 *            (account id under which a new new entry is to be created, 0 if
	 *            parent level account is to be created.)
	 * @return accCode (account code of the newly created entry, -ve if
	 *         unsuccessful.)
	 * */
	public String getAccCode(String accId) {
		String strQry = "SELECT [dbo].[max_acc_code] (" + accId
				+ ") AS accCode";

		try {
			this.statement = cnn.createStatement();
			ResultSet rs = statement.executeQuery(strQry);
			if (rs.next())
				return (rs.getString("accCode"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * It checks the database for the existence of the account alias provided by
	 * the user for the new account entry. If found, returns the positive count,
	 * else the negative count.
	 * 
	 * @param accAlias
	 *            (alias of the new entry that the user wants to create)
	 * @return count (count of the existing accAlias in table acc_head_mcg
	 *         supplied by the user, 0 if nil)
	 */
	public int getAccAlias(String accAlias) {
		String strQry = "SELECT COUNT(alias) AS alias_count FROM acc_head_mcg WHERE alias='"
				+ accAlias + "'";

		try {
			this.statement = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = statement.executeQuery(strQry);

			if (rs.next()) {
				return rs.getInt("alias_count");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Method that performs the saving operation against the database.
	 * 
	 * @param accHeadModel
	 *            (a new object of type AccHeadModel that holds all the user
	 *            inputs provided by the user for saving the new account entry)
	 * @return boolean (true if success, else false)
	 * @empty boolean (true if the user did not entered the amount in the
	 *        opening balance, false otherwise)
	 */
	public boolean SaveAccHead(AccHeadModel accHeadModel, boolean empty) {
		boolean retFlag = false;
		String strQry = "";
		String accCode = "";

		/*
		 * If opening balance is added into the account heads, that account head
		 * won't have subaccount heads. Hence -1 is used here.
		 */
		String subAccCode = "-1";

		/*
		 * This variable keeps track of whether a user is trying to insert the
		 * opening balance for the first time or trying to update the opening
		 * balance which was previously set. qryType = 0 means neither insert
		 * nor update, 1 means insert, 2 means update.
		 */
		int qryType = 0;
		System.out.println("dtaa " + accHeadModel.getLogFile());
		String[] logFile = accHeadModel.getLogFile().toString().split("<---->");
		System.err.println("Log file 0" + logFile[0]);
		System.err.println("Log file 0" + logFile[1]);
		if (accHeadModel.acc_head_id() <= 0) {
			accHeadModel.setAcc_head_id(UtilMethods.getMaxId("acc_head_mcg",
					"acc_head_id"));
			logFile[1] += "\tAccHeadId-" + accHeadModel.getAcc_head_id();
			accHeadModel.setCreated_by(ApplicationWorkbenchWindowAdvisor
					.getLoginId());
			accHeadModel.setCreated_date(JCalendarFunctions.currentDate());
			accHeadModel.setBr_id(1);
			accHeadModel.setUpdated_by(ApplicationWorkbenchWindowAdvisor
					.getLoginId());
			accHeadModel.setUpdated_date(JCalendarFunctions.currentDate());
			accHeadModel.setUpdate_count(0);
			// Set Whether this a/c is Dr or Cr
			accHeadModel.setDrcr(accHeadModel.getIsDr() ? "Dr" : "Cr");
			strQry = accHeadModel.Insert();
			qryType = 1;
			accCode = accHeadModel.getAcc_code();

		} else {
			accHeadModel.getZ_WhereClause().acc_head_id(
					accHeadModel.acc_head_id());
			accHeadModel.getZ_WhereClause().br_id(
					1);
			accHeadModel.setUpdated_by(ApplicationWorkbenchWindowAdvisor
					.getLoginId());
			accHeadModel.setUpdated_date(JCalendarFunctions.currentDate());
			accHeadModel.setUpdate_count(accHeadModel.getUpdate_count() + 1);
			accHeadModel.setCreated_date(JCalendarFunctions.currentDate());
			accHeadModel.setDrcr(accHeadModel.getIsDr() ? "Dr" : "Cr");
			strQry = accHeadModel.Update();
			qryType = 2;
			accCode = accHeadModel.getCurrentAccCode();
		}

		log.add(logFile[0] + "\n" + logFile[1]);
		/*
		 * If opening balance is empty(true) i.e. user didn't enter the amount
		 * in the opening balance textbox, just insert account head details into
		 * the account_head_mcg table.
		 */
		try {
			cnn.setAutoCommit(false);
			if (empty) {

				try {
					System.out.println("zzz.1. the testing QRY =" + strQry);
					if (UtilFxn.modifyTable(strQry, cnn, "account") > 0) {

						retFlag = true;
					} else {

						retFlag = false;
					}

				} catch (SQLException e) {
					e.printStackTrace();
				}
				if (empty
						&& qryType == 2
						&& !UtilFxn.GetValueFromTable("ledger_mcg", "acc_code",
								"jv_no = -1 AND acc_code ='" + accCode + "'")
								.equals("")) {
					/*
					 * if amount is empty but it is update QRY ie. user intends
					 * to reset the opening balance of the acc heads, we need to
					 * delete the previously entered rows with jv_no=-1 in
					 * journal_voucher_details_mcg and ledger_table postings.
					 */

					String sqlDeleteJournalDetail = "DELETE from journal_voucher_details_mcg WHERE jv_no = -1 AND acc_code ='"
							+ accCode + "'";
					String sqlDeleteLedger = "DELETE from ledger_mcg WHERE jv_no = -1 AND acc_code ='"
							+ accCode + "'";
					log.add("\t\t(JV-Delete):\tJV_NO->-1\tAccCode->"+accCode+"\n");
					try {
						statement = cnn.createStatement();
						if (statement.executeUpdate(sqlDeleteJournalDetail) >= 1) {
							if (statement.executeUpdate(sqlDeleteLedger) >= 1) {
								retFlag = true;
							} else {
								retFlag = false;
							}
						} else {
							retFlag = false;
						}

						/*
						 * if (!SaveQueryController.saveQuery(cnn,
						 * sqlDelete,"account")) return false;
						 */
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}// second if ends
			}
			// user entered the opening balance amount
			else {

				try {
					System.out.println("zzz.. the testing QRY =" + strQry);

					/*
					 * Journal entry and ledger posting for a newly created
					 * account head is a must if opening balance was specified.
					 * So after successfully creating an account head, if
					 * journal entry and/or ledger posting fails for the newly
					 * created account head, we must rollback the newly entered
					 * account head. Hence connection for the db, that enters a
					 * newly created account head or updated accounted head is
					 * set to false for auto commit
					 */

					// if this query was successful, save into the journal
					if (UtilFxn.modifyTable(strQry, cnn, "account") > 0) {
						if (journalEntryController.saveOpeningBalance(accCode,
								subAccCode, accHeadModel, this.cnn, qryType,log)) {
							// journal entry is successful, commit the
							// transaction
							retFlag = true;

						} else {
							/*
							 * Journal entry is unsuccessful, rollback the
							 * transaction.
							 */

							retFlag = false;
						}
					} else {

						retFlag = false;
					}

				} catch (SQLException e) {
					System.out.println(" The exception is " + e.toString());
					e.printStackTrace();

				}

			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (retFlag == true) {
			LogFileCreate.logFileCreater(cnn, "Admin", "Account Head",
					logFile[0].equals("Nodata") ? "Save" : "Update", log);
			log.clear();
		}

		return retFlag;
	}

	/**
	 * Deletes the account head from the database. We can delete the account
	 * heads that don't have any associated transactions in the journal. But all
	 * account heads have at least one entry in journal and ledger which is
	 * their opening balance entry. We should delete them too.
	 * 
	 * @param accId
	 *            (Id of the account entry to be deleted)
	 * @param accCode
	 *            (Account code of the account head to be deleted)
	 * @return boolean (true if successful, else false)
	 * */
	public boolean deleteAccHead(int accId, String accCode) {
		boolean result = false;
		AccHeadModel accHeadModel = new AccHeadModel();
		accHeadModel.getZ_WhereClause().acc_head_id(accId);

		String strQry = accHeadModel.Delete();
		try {
			this.statement = cnn.createStatement();

			/*
			 * The delete query affects only one row, hence 1 is returned.
			 */
			if (statement.executeUpdate(strQry) == 1
					) {

				// delete from the journal_voucher_details_mcg table too
				String sqlJournalDelete = "DELETE FROM journal_voucher_details_mcg WHERE jv_no = -1 and acc_code = '"
						+ accCode + "'";

				// if (st.executeUpdate(sqlJournalDelete) == 1) {
				// // delete from the ledger_mcg table too
				// String sqlLedgerDelete =
				// "DELETE FROM ledger_mcg WHERE jv_no = -1 and acc_code = '"
				// + accCode + "'";
				//
				// if (st.executeUpdate(sqlLedgerDelete) == 1) {
				// result = true;
				// }
				// }
				result = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * This method checks whether a selected node is leaf or not. If true,
	 * returns the same and false otherwise
	 * 
	 * @param accCode
	 *            (account code of the selected account entry)
	 * @return boolean (true if the node is leaf, false otherwise)
	 */

	// modified by bishal(on 2011-11-08)
	public boolean isNodeLeaf(String accCode) {
		String strQry = "SELECT COUNT(*) AS children_count FROM acc_head_mcg WHERE acc_code LIKE '"
				+ accCode + ".%'";
		System.out.println("The query to check child acc code is :" + strQry);

		try {
			this.statement = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = statement.executeQuery(strQry);

			while (rs.next()) {
				if (rs.getInt(1) == 0) {
					// then the selected account is definitely the leaf node
					return true;
				} else if (rs.getInt(1) >= 1) {
					// then the selected account definitely has children
					return false;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Method to check if any of the account head has transactions associated
	 * with it. If an account head is associated with a transaction it would be
	 * then reflected in the ledger.
	 * 
	 * @param accCode
	 *            (Account code of the account head to be checked for.)
	 * @return (Count of no. of transactions the account head is associated
	 *         with.)
	 */
	public int checkForTransactions(String accCode) {
		String countSQL = "SELECT COUNT(*) FROM ledger_mcg WHERE acc_code = '"
				+ accCode + "'";

		try {
			this.statement = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = statement.executeQuery(countSQL);

			while (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;

	}

	/**
	 * hasTransaction() checks whether an account head is already involved in a
	 * transaction by checking its respective acc_code in
	 * journal_voucher_details_mcg table. If found in the table, it returns
	 * true, false otherwise.
	 * 
	 * @param accCode
	 *            (Account code of the account head to be checked.)
	 * @return boolean (true if found, false otherwise.)
	 */
	public boolean hasTransaction(String accCode) {
		String strQry = "SELECT COUNT(*) AS totalTransaction FROM journal_voucher_details_mcg WHERE acc_code ='"
				+ accCode + "'";

		try {
			this.statement = this.cnn.createStatement(
					ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			this.resultset = this.statement.executeQuery(strQry);

			while (this.resultset.next())
				if (this.resultset.getInt("totalTransaction") <= 1)
					return false;
				else
					return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return true;
	}

	/**
	 * Fetches the update_count of the passed account head. If the account head
	 * id doesn't exist yet, a negative value is returned.
	 * 
	 * @param accHeadId
	 *            (Account head id.)
	 * @return int (Update count if exists, negative value otherwise.)
	 */
	public int getUpdateCount(int accHeadId) {
		int count = -1;
		String sql = "SELECT update_count FROM acc_head_mcg WHERE acc_head_id = '"
				+ accHeadId + "'";

		try {
			this.statement = this.cnn.createStatement(
					ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			this.resultset = this.statement.executeQuery(sql);

			if (resultset.next())
				count = resultset.getInt("update_count");

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return count;
	}

	/**
	 * @param accCode
	 * @return acctype value
	 * 
	 *         method to get acc_type of the account head passed
	 */
	public int getAccType(String accCode) {
		int accType = 0;

		String sql = "SELECT acc_type FROM 	acc_head_mcg WHERE acc_code = '"
				+ accCode + "'";
		try {
			this.statement = this.cnn.createStatement(
					ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			this.resultset = this.statement.executeQuery(sql);

			if (resultset.next())
				accType = resultset.getInt("acc_type");

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return accType;
	}

	/**
	 * @return resultset method to get all root account heads from existing COA
	 */
	public static ResultSet getAccRoots() {
		String sql = "select acc_code,alias,acc_name,cvm.cv_lbl acc_type,ahm.acc_head_id from acc_head_mcg ahm "
				+ " inner join code_value_mcg cvm on cvm.cv_type='account type' and ahm.acc_type=cvm.cv_code "
				+ " and ahm.parent=0 order by acc_code";
		ResultSet rsRoots = null;
		Statement st = null;
		Connection cnn = DbConnection.getConnection();
		try {
			st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			rsRoots = st.executeQuery(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsRoots;
	}

	/**
	 * @param acc_head_id
	 * @return resultset method to get all children sub_heads of an account head
	 *         belonging to @param acc_head_id
	 */
	public static ResultSet getAccChilds(int acc_head_id) {
		String sql = "select acc_code,alias,acc_name,cvm.cv_lbl acc_type,acc_head_id from acc_head_mcg ahm "
				+ " inner join code_value_mcg cvm on cvm.cv_type='account type' and ahm.acc_type=cvm.cv_code "
				+ " and ahm.parent=" + acc_head_id + " order by acc_head_id";
		ResultSet rsRoots = null;
		Statement st = null;
		Connection cnn = DbConnection.getConnection();
		try {
			st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			rsRoots = st.executeQuery(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsRoots;
	}

} // EOC
