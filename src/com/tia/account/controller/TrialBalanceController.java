package com.tia.account.controller;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.model.TempBalanceSheetModel;
import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;



/**
 * @author minesh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 * @altered & revised by suresh
 */
public class TrialBalanceController {
	Connection cnn;
	ResultSet rs;
	Statement st;

	JCalendarFunctions jcal;
	SimpleDateFormat sdf;

	public TrialBalanceController() {
		cnn = DbConnection.getConnection();

		jcal = new JCalendarFunctions();
		sdf = new SimpleDateFormat("yyyy-MM-dd");

	}

	/**
	 * @param toDate
	 * @param fiscalYear
	 * @param closingPerformed
	 * @param hierarchyLevel
	 * @return resultset containing all data being displayed in TrialBalance
	 */
	public ResultSet loadTrialBalance( String toDate,
			int fiscalYear, boolean closingPerformed, String hierarchyLevel) {
		int currentQuarterId=ApplicationWorkbenchWindowAdvisor.getCurrentClosingPeriodId();
		String filterFyIdCondition=(currentQuarterId==1?" ":"	AND  fy_id = "+fiscalYear+" " );
		/*
		 * Here, we find out which quarter or period the current day-in date belongs to and
		 * determine whether to filter the FY_ID in ledger_mcg data while populating the
		 * TrialBalance GUI. 
		 * If currentQuarterId=1 means that current period belongs to first quarter of this Fiscal Year.
		 * So, if we wish to calculate the TB report from existing data, all closing-balances of heads
		 * belong to the previous fiscal year, resulting in no data in TB immediately a day after 
		 * Year_End closing.
		 * If currentQuarterId!=1 then the current period belongs to other periods of current Fiscal Year.
		 * In that case we have previous closing-data in ledger_mcg with same Fy_Id as of this period, and we
		 * need to filter the Fy_Id otherwise the closing data from previous Year_End closing gets summed and
		 * report gets double or unexpectedly large values.
		 */
		String strQry = " ";
		String firstOpeningBalLed_id=UtilFxn.GetMinValueFromTable("ledger_mcg", "led_id", " jv_no = -1 and  posted_date =  (select cast(MAX(posted_date) as date)as lastClosingDate from ledger_mcg where jv_no=-1 and posted_date <='"+toDate+"') ");
		if (!closingPerformed) {
			strQry = "SELECT * "
				+ "FROM  (SELECT a.acc_code, a.acc_name, a.acc_type, a.parent,a.alias,"
				+ "		 CASE"
				+ "			WHEN ((acc_type='1' OR acc_type='2' OR (acc_type='5' AND drcr='Dr')))"
				+ "			THEN"
				+ "				(SELECT (SUM(dr_amt)-SUM(cr_amt)) "
				+ "				 FROM  ledger_mcg l WHERE (l.acc_code LIKE a.acc_code + '.%' OR l.acc_code=a.acc_code) "
				+ filterFyIdCondition 
				+ " "+(firstOpeningBalLed_id.equals("") ? "":" AND (led_id >= (select min(led_id) from ledger_mcg where jv_no = -1 and posted_date = (select cast(MAX(posted_date) as date)as lastClosingDate from ledger_mcg where jv_no=-1 and posted_date <='"+toDate+"'))) " )+" "
				+ "  and posted_date !> '"
				+ toDate
				+ "') "
				+ "		    WHEN ((acc_type='3' OR acc_type = '4'OR (acc_type='5' AND drcr='Cr')))"
				+ "		    THEN	"
				+ "				(SELECT (SUM(cr_amt)-SUM(dr_amt)) as amt"
				+ "				 FROM  ledger_mcg l WHERE (l.acc_code LIKE a.acc_code + '.%' OR l.acc_code=a.acc_code) "
				+ filterFyIdCondition
				+ " "+(firstOpeningBalLed_id.equals("") ? "":" AND (led_id >= (select min(led_id) from ledger_mcg where jv_no = -1 and posted_date = (select cast(MAX(posted_date) as date)as lastClosingDate from ledger_mcg where jv_no=-1 and posted_date <='"+toDate+"'))) " )+" "
				+ "  and posted_date !> '"
				+ toDate + "'  )" + "		  END		 AS balance "
				+ "   FROM acc_head_mcg AS a) AS T1 "
				+ "WHERE  balance<>0 ";

		}
		// after closing the query should be the following
		else {
			strQry = "SELECT * "
				+ "FROM  (SELECT a.acc_code, a.acc_name, a.acc_type, a.parent,a.alias,"
				+ "		 CASE"
				+ "			WHEN ((acc_type='1' OR acc_type='2' OR  (acc_type='5' AND drcr='Dr')))"
				+ "			THEN"
				+ "				(SELECT (SUM(dr_amt)-SUM(cr_amt)) "
				+ "				 FROM  ledger_mcg l WHERE (l.acc_code LIKE a.acc_code + '.%' OR l.acc_code=a.acc_code) "
				+ filterFyIdCondition 
				+ " AND posted_date = '"
				+ toDate
				+ "'  AND led_id >= (select min(led_id) from ledger_mcg where jv_no = -1 and posted_date = '"
				+ toDate
				+ "') )"
				+ "		    WHEN ((acc_type='3' OR acc_type = '4'OR (acc_type='5' AND drcr='Cr')))"
				+ "		    THEN	"
				+ "				(SELECT (SUM(cr_amt)-SUM(dr_amt)) as amt"
				+ "				 FROM  ledger_mcg l WHERE (l.acc_code LIKE a.acc_code + '.%' OR l.acc_code=a.acc_code) "
				+ filterFyIdCondition 
				+ " AND posted_date = '"
				+ toDate
				+ "'  AND led_id >= (select min(led_id) from ledger_mcg where jv_no = -1 and posted_date = '"
				+ toDate + "') )" + "	END	AS balance "
				+ "   FROM acc_head_mcg AS a ) AS T1 "
				+ "WHERE  balance<>0 ";
		}
		//this esp modification was made to address the Hierarchy of GUI presentation of data--- by suresh on 20DEC2011 TUE
		strQry=strQry +( hierarchyLevel.equalsIgnoreCase("1")? " AND parent=0 order by (cast (acc_code as integer)) asc " : " ORDER BY acc_code ");
		System.out.println("zzz..Trial balance query: " + strQry);

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}//end of method loadTrialBalance()
	/**
	 * @param toDate
	 * @param fiscalYear
	 * @param closingPerformed
	 * @param hierarchyLevel
	 * @param rpt_time
	 * @param rpt_type
	 * @return collection
	 * method to load TrialBalance for closing reports.
	 * We save the TB report data before and after account closing
	 * for future reports and we return a list with formatted data for
	 * directly populating jasper report and export into PDF files.
	 * 
	 */
	public List<TempBalanceSheetModel> loadTrialBalanceClosing( String toDate,
			int fiscalYear, boolean closingPerformed, String hierarchyLevel,int rpt_time, int rpt_type) {
		List<TempBalanceSheetModel> lstTBSM = new ArrayList<TempBalanceSheetModel>();
		lstTBSM.clear();

		try {
			DecimalFormat df=new DecimalFormat("##,##0.00");
			BigDecimal drTotal =new BigDecimal("0.00") ,crTotal=new BigDecimal("0.00"),amt= new BigDecimal("0.00");
			rs = loadTrialBalance(toDate, fiscalYear, closingPerformed, hierarchyLevel);
			int i=1;
			while(rs.next())
			{
				int reportid =ApplicationWorkbenchWindowAdvisor.getLoginId();
				TempBalanceSheetModel objModel = new TempBalanceSheetModel();
				objModel.setId(reportid);
				objModel.setRpt_order(i);
				objModel.setBr_id(1);
				objModel.setRpt_for("trialbalance");
				String acc_code=rs.getString("acc_code");
				objModel.setAcc_code(acc_code);

				if (acc_code.matches("[0-9]*")) {
					objModel.setAccount_alias(rs.getString("alias"));
					objModel.setParticulars(rs.getString("acc_name"));
				}
				else if (acc_code.matches("[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("   " +rs.getString("alias"));
					objModel.setParticulars("   " +rs.getString("acc_name"));
				} else if (acc_code.matches("[0-9]*.[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("      " +rs.getString("alias"));
					objModel.setParticulars("      " +rs.getString("acc_name"));
				} else if (acc_code.matches("[0-9]*.[0-9]*.[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("         " + rs.getString("alias"));
					objModel.setParticulars("         " + rs.getString("acc_name"));
				} else if (acc_code.matches("[0-9]*.[0-9]*.[0-9]*.[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("            " +rs.getString(  "alias"));
					objModel.setParticulars( "            " +rs.getString( "acc_name"));
				} else {
					objModel.setAccount_alias("               " +rs.getString(  "alias"));
					objModel.setParticulars("               "+rs.getString(  "acc_name"));
				}
				/*objModel.setAccount_alias(rs.getString("alias"));
					objModel.setParticulars(rs.getString("acc_name"));*/
				amt=BigDecimal.valueOf(Math.abs(rs.getDouble("balance")));
				String accType=rs.getString("acc_type");
				if (accType.equals("1")
						|| accType.equals("2")) {
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head(df.format(0.00));
					//drTotal = amt.add(drTotal);
					if (acc_code.matches("[0-9]*"))
					{
						drTotal = amt.add(drTotal);
					}
				}
				else if(accType.equals("3")
						|| accType.equals("4"))
				{
					objModel.setTot_sub_head(df.format(0.00));
					objModel.setTot_head(df.format(amt));
					if (acc_code.matches("[0-9]*"))
					{
						crTotal = amt.add(crTotal);
					}
				}
				lstTBSM.add(objModel);
				i++;

			}
			TempBalanceSheetModel objModel1 = new TempBalanceSheetModel();
			objModel1.setAccount_alias("");
			objModel1.setParticulars("hDdf");
			objModel1.setTot_sub_head(df.format(drTotal));					
			objModel1.setTot_head(df.format(crTotal));
			lstTBSM.add(objModel1);
			//now save the data for future reporting purposes
			int id=UtilMethods.getMaxId("acc_monthly_closing_mcg", "id");//offset value
			//reassign the value of i for other use
			i=0;
			int br_id=1;
			String trxn_date=jcal.currentDate();
			String trxn_np_date=jcal.getCurrentNepaliDate();
			String npDate[]=trxn_np_date.split(ApplicationWorkbenchWindowAdvisor.getDateSeperator());
			int quarter=ApplicationWorkbenchWindowAdvisor.getCurrentClosingPeriodId();
			int creator_id=ApplicationWorkbenchWindowAdvisor.getLoginId();


			for (Iterator<TempBalanceSheetModel> iterator = lstTBSM
					.iterator(); iterator.hasNext();) {
				TempBalanceSheetModel tbsm = iterator.next();
				String futureData="INSERT INTO [acc_monthly_closing_mcg] " +
				"([id],[br_id],[fy_id],[date],[nep_date],[nep_month],[nep_year],[quarter],[acc_code] ,[alias],[acc_name],[dr_amt]" +
				" ,[cr_amt],[created_by],[created_dt] ,[update_count],[summary_time],[summary_type]) " +
				" VALUES " +
				" ("+(id+i)+","+br_id+","+fiscalYear+",'"+trxn_date+"','"+trxn_np_date+"',"+Integer.valueOf(npDate[1])+","+Integer.valueOf(npDate[0])+", " +
				" "+quarter+", '"+tbsm.getAcc_code()+"', '"+tbsm.getAccount_alias()+"', '"+tbsm.getParticulars().replaceAll("'", "''").replaceAll("\"", "\\\"")+"','"+tbsm.getTot_sub_head()+"','"+tbsm.getTot_head()+"',"+creator_id+",GETDATE(),0,"+rpt_time+","+rpt_type+" ) ";
				cnn = DbConnection.getConnection();
				cnn.setAutoCommit(false);
				st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_UPDATABLE);
				if(st.executeUpdate(futureData)>0)
				{
					
						cnn.commit();
						cnn.setAutoCommit(true);
					
					
				}
				else
				{
					cnn.rollback();
					cnn.setAutoCommit(true);
				}
				//increment value of i
				i++;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lstTBSM;
	}//end of method loadTrialBalance()
	/**
	 * @param fromDate
	 * @param toDate
	 * @param fiscalYear
	 * @param closingPerformed
	 * @param hierarchyLevel
	 * @return resultset
	 * methd to load TrialBalance for DMS Assets only
	 */
	public ResultSet loadTrialBalanceDMSAssets( String fromDate,String toDate,
			int fiscalYear, boolean closingPerformed, String hierarchyLevel) {
		String strQry = " ";
		String firstOpeningBalLed_id=UtilFxn.GetMinValueFromTable("ledger_mcg", "led_id", " jv_no = -1 and  posted_date =  (select cast(MAX(posted_date) as date)as lastClosingDate from ledger_mcg where jv_no=-1 and posted_date <='"+toDate+"') ");
		strQry = "SELECT * "
			+ "FROM  (SELECT a.acc_code, a.acc_name, a.acc_type, a.parent,a.alias,"
			+ "				(SELECT (SUM(dr_amt)) "
			+ "				 FROM  ledger_mcg l WHERE (l.acc_code LIKE a.acc_code + '.%' OR l.acc_code=a.acc_code) "
			+ " AND a.acc_type in (1,2)	"
			+ "				 AND  fy_id = "+fiscalYear+" " 
			+ " "+(firstOpeningBalLed_id.equals("") ? "":" AND (led_id > (select MAX(led_id) from ledger_mcg where jv_no = -1 and posted_date = (select cast(MAX(posted_date) as date)as lastClosingDate from ledger_mcg where jv_no=-1 and posted_date <='"+toDate+"'))) " )+" "
			+ "  and posted_date BETWEEN '"+fromDate+"' AND '"
			+ toDate
			+ "' AND jv_no<>-1) "
			+ "	 AS drBalance ,"
			+ "				(SELECT (SUM(cr_amt)) "
			+ "				 FROM  ledger_mcg l WHERE (l.acc_code LIKE a.acc_code + '.%' OR l.acc_code=a.acc_code) "
			+ " AND a.acc_type in (1,2)	"
			+ "				 AND  fy_id = "+fiscalYear+" " 
			+ " "+(firstOpeningBalLed_id.equals("") ? "":" AND (led_id > (select MAX(led_id) from ledger_mcg where jv_no = -1 and posted_date = (select cast(MAX(posted_date) as date)as lastClosingDate from ledger_mcg where jv_no=-1 and posted_date <='"+toDate+"'))) " )+" "
			+ "  and posted_date BETWEEN '"+fromDate+"' AND '"
			+ toDate
			+ "' AND jv_no<>-1) "
			+ "	 AS crBalance "
			+ "   FROM acc_head_mcg AS a) AS T1 "
			+ "WHERE  (drBalance<>0 OR crBalance<>0) ";

		//this esp modification was made to address the Hierarchy of GUI presentation of data--- by suresh on 20DEC2011 TUE
		strQry=strQry +( hierarchyLevel.equalsIgnoreCase("1")? " AND parent=0 order by (cast (acc_code as integer)) asc " : " ORDER BY acc_code ");
		System.out.println("zzz..DMS query for assets: " + strQry);

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}//end of method loadTrialBalanceDMSAssets()
	/**
	 * @param fromDate
	 * @param toDate
	 * @param fiscalYear
	 * @param closingPerformed
	 * @param hierarchyLevel
	 * @return resultset
	 * method to load trialBalance for DMS Liabilities only
	 */
	public ResultSet loadTrialBalanceDMSLiab( String fromDate,String toDate,
			int fiscalYear, boolean closingPerformed, String hierarchyLevel) {
		String strQry = " ";
		String firstOpeningBalLed_id=UtilFxn.GetMinValueFromTable("ledger_mcg", "led_id", " jv_no = -1 and  posted_date =  (select cast(MAX(posted_date) as date)as lastClosingDate from ledger_mcg where jv_no=-1 and posted_date <='"+toDate+"') ");
		strQry = "SELECT * "
			+ "FROM  (SELECT a.acc_code, a.acc_name, a.acc_type, a.parent,a.alias,"
			+ "				(SELECT (SUM(dr_amt)) "
			+ "				 FROM  ledger_mcg l WHERE (l.acc_code LIKE a.acc_code + '.%' OR l.acc_code=a.acc_code) "
			+ " AND a.acc_type in (3,4)	"
			+ "				 AND  fy_id = "+fiscalYear+" " 
			+ " "+(firstOpeningBalLed_id.equals("") ? "":" AND (led_id > (select MAX(led_id) from ledger_mcg where jv_no = -1 and posted_date = (select cast(MAX(posted_date) as date)as lastClosingDate from ledger_mcg where jv_no=-1 and posted_date <='"+toDate+"'))) " )+" "
			+ "  and posted_date BETWEEN '"+fromDate+"' AND '"
			+ toDate
			+ "' AND jv_no<>-1) "
			+ "	 AS drBalance ,"
			+ "				(SELECT (SUM(cr_amt)) "
			+ "				 FROM  ledger_mcg l WHERE (l.acc_code LIKE a.acc_code + '.%' OR l.acc_code=a.acc_code) "
			+ " AND a.acc_type in (3,4)	"
			+ "				 AND  fy_id = "+fiscalYear+" " 
			+ " "+(firstOpeningBalLed_id.equals("") ? "":" AND (led_id > (select MAX(led_id) from ledger_mcg where jv_no = -1 and posted_date = (select cast(MAX(posted_date) as date)as lastClosingDate from ledger_mcg where jv_no=-1 and posted_date <='"+toDate+"'))) " )+" "
			+ "  and posted_date BETWEEN '"+fromDate+"' AND '"
			+ toDate
			+ "' AND jv_no<>-1) "
			+ "	 AS crBalance "
			+ "   FROM acc_head_mcg AS a) AS T1 "
			+ "WHERE  (drBalance<>0 OR crBalance<>0) ";

		//this esp modification was made to address the Hierarchy of GUI presentation of data--- by suresh on 20DEC2011 TUE
		strQry=strQry +( hierarchyLevel.equalsIgnoreCase("1")? " AND parent=0 order by (cast (acc_code as integer)) asc " : " ORDER BY acc_code ");
		System.out.println("zzz..DMS query for liabs: " + strQry);

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}//end of method loadTrialBalanceDMS()
	/**
	 * @param date
	 * @return boolean value
	 * 
	 * This method checks whether or not the closing for quarter 
	 * within which given date lies has been performed.
	 */
	public boolean isClosingPerformed(String date) {
		boolean retFlag = false;
		String sql = "SELECT COUNT(*) AS total FROM ledger_mcg WHERE jv_no = -1 and is_opening=1 AND posted_date = '"
			+ date + "'";

		try {
			cnn = DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

			if (rs.next()) {
				if (rs.getInt("total") >= 1)
					retFlag = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retFlag;
	}//method to check if the closing has been performed of not.

}//end of class

