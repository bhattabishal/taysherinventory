package com.tia.account.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tia.account.model.BulkCashReceiptModel;
import com.tia.common.db.DbConnection;



/**
 * @author suresh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 * 
 */
public class BulkCashReceiptController {
	private static Connection cnn;
	private static Statement st;
	private static ResultSet rs = null;

	/**
	 * @param englishDate
	 * @param memberId
	 * @return collection list
	 * 
	 * This method is used to get all trxns on Bank and Cash by given Member on given date
	 */
	public static List<BulkCashReceiptModel> generateBulkCashReport(String englishDate,int memberId,String cashRoot,String bankRoot)
	{
		List<BulkCashReceiptModel> listBcrm=new ArrayList<BulkCashReceiptModel>();
		String strQuery=
			" SELECT * FROM (" +
			" SELECT  distinct(isnull(jvm.receipt_no,'')) receiptNo,a.alias,a.acc_name particulars," +
			" case when (abs(l.dr_amt)>0 AND l.dr_amt is not null) then l.dr_amt else l.cr_amt end amt," +
			" ISNULL(u.full_name,'��')received_by,mrm.id FROM ledger_mcg l" +
			" INNER JOIN journal_voucher_mcg jvm ON l.jv_no=jvm.jv_no AND (jvm.to_from_type='account'" +
			" AND jvm.receipt_no IS NOT NULL) AND l.jv_no<>-1 AND (l.acc_code like '"+cashRoot+".%' OR l.acc_code like '"+bankRoot+".%') AND l.posted_date='"+englishDate+"'" +
			" INNER JOIN acc_head_mcg a ON l.to_acc_code=a.acc_code" +
			" INNER JOIN cash_income_mcg cim ON jvm.receipt_no=cim.receipt_no" +
			" INNER JOIN mem_reg_mcg mrm ON cim.grp_id=mrm.id" +
			" INNER JOIN user_accounts u ON jvm.created_by=u.user_id" +
			/*" UNION" +
			" SELECT  lcrm.lcsn receiptNo,a.alias,a.acc_name particulars," +
			" case when (abs(l.dr_amt)>0 AND l.dr_amt is not null) then l.dr_amt else l.cr_amt end amt," +
			" ISNULL(u.user_name,'')received_by,mrm.id FROM ledger_mcg l" +
			" INNER JOIN journal_voucher_mcg jvm ON l.jv_no=jvm.jv_no AND (jvm.to_from_type='loan') AND l.jv_no<>-1" +
			" AND (l.acc_code like '04.%' OR l.acc_code like '05.%') AND l.posted_date='"+englishDate+"'" +
			" INNER JOIN acc_head_mcg a ON l.to_acc_code=a.acc_code" +
			" INNER JOIN loan_transaction_mcg ltm ON jvm.tran_id=ltm.trxn_ses_id" +
			" INNER JOIN loan_request_mcg lrm ON ltm.loan_id=lrm.loan_id" +
			" INNER JOIN mem_reg_mcg mrm ON lrm.mid=mrm.id" +
			" INNER JOIN loan_collection_rpt_mcg lcrm ON ltm.tid=lcrm.tid AND ltm.loan_id=lcrm.loan_id" +
			" INNER JOIN user_accounts u ON jvm.created_by=u.user_id" +*/
			" UNION ALL " +
			" SELECT  distinct(isnull(jvm.receipt_no,'')) receiptNo,a.alias,a.acc_name particulars," +
			" case when (abs(l.dr_amt)>0 AND l.dr_amt is not null)  then l.dr_amt else l.cr_amt end amt," +
			" ISNULL(u.full_name,'��')received_by,mrm.id FROM ledger_mcg l" +
			" INNER JOIN journal_voucher_mcg jvm ON l.jv_no=jvm.jv_no AND jvm.to_from_type='saving' AND l.jv_no<>-1" +
			" AND (l.acc_code like '"+cashRoot+".%' OR l.acc_code like '"+bankRoot+".%') AND l.posted_date='"+englishDate+"'" +
			" INNER JOIN acc_head_mcg a ON l.to_acc_code=a.acc_code" +
			" INNER JOIN sav_transaction st ON jvm.tran_id=st.trxn_id" +
			" inner join cash_income_mcg cim on st.trxn_id=cim.trxn_id "+
			" INNER JOIN sav_acc_mcg sam ON st.acc_no=sam.acc_no" +
			" INNER JOIN mau_sav_scheme mss on sam.scheme_id=mss.scheme_id" +
			" INNER JOIN mem_reg_mcg mrm ON st.group_id=mrm.id" +
			" INNER JOIN user_accounts u ON jvm.created_by=u.user_id" +
			" UNION ALL" +
			" SELECT  distinct(isnull(jvm.receipt_no,'')) receiptNo,a.alias,a.acc_name particulars," +
			" case when (abs(l.dr_amt)>0 AND l.dr_amt is not null)  then l.dr_amt else l.cr_amt end amt," +
			" ISNULL(u.full_name,'��')received_by,mrm.id FROM ledger_mcg l" +
			" INNER JOIN journal_voucher_mcg jvm ON l.jv_no=jvm.jv_no AND jvm.to_from_type='share' AND l.jv_no<>-1" +
			" AND (l.acc_code like '"+cashRoot+".%' OR l.acc_code like '"+bankRoot+".%') AND l.posted_date='"+englishDate+"'" +
			" INNER JOIN acc_head_mcg a ON l.to_acc_code=a.acc_code" +
			" INNER JOIN share_trans_mcg stm ON jvm.tran_id=stm.share_trans_id and jvm.jv_no=stm.jv_no" +
			" INNER JOIN share_crt_mcg scm on stm.share_crt_no=scm.share_crt_no" +
			" INNER JOIN share_holder_mcg shm on scm.buyer_share_holder_id=shm.share_holder_id" +
			" INNER JOIN mem_reg_mcg mrm on shm.mid=mrm.mid" +
			" INNER JOIN user_accounts u ON jvm.created_by=u.user_id" +
			" )t " +
			" WHERE id="+memberId+" ";

		rs = null;

		try {
			System.out.println("Bulk Cash report qry="+strQuery);
			cnn=DbConnection.getConnection();
			st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rs = st.executeQuery(strQuery);

			if (rs == null)
			{
				return null;

			}
			else {
				while (rs.next()) {
					BulkCashReceiptModel tmpBcrm=new BulkCashReceiptModel();
					//tmpBcrm.setSn(rs.getString(""));//no need
					tmpBcrm.setReceiptNo(rs.getString("receiptNo"));
					tmpBcrm.setAlias(rs.getString("alias"));
					tmpBcrm.setParticular(rs.getString("particulars"));
					tmpBcrm.setAmount(rs.getString("amt"));
					tmpBcrm.setReceivedBy(rs.getString("received_by"));
					//add the initialized model instance into ArrayList
					listBcrm.add(tmpBcrm);
				}
			}
		}//try ends 
		catch (SQLException e) 
		{
			System.out.println("The exception while loading Bulk Cash receipt... " + e.toString());
			e.printStackTrace();
		}

		return listBcrm;

	}//method generateCashRegisterBook() ends
}//end of class
