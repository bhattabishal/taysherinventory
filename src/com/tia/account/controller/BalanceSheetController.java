package com.tia.account.controller;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;



import com.ibm.icu.text.DecimalFormat;
import com.ibm.icu.text.SimpleDateFormat;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.model.TempBalanceSheetModel;
import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;

/**
 * @author minesh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 * @altered & revised by suresh
 * 
 */
public class BalanceSheetController {
	Connection cnn;
	Statement statement;
	ResultSet resultset;

	JCalendarFunctions jCalendarFunctions;
	SimpleDateFormat simpleDateFormat;

	/**
	 * constructor method
	 */
	public BalanceSheetController() {
		cnn = DbConnection.getConnection();

		jCalendarFunctions = new JCalendarFunctions();
		simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	}


	/**
	 * @param toDate
	 * @param hierarchyLevel
	 * @return
	 */
	public ResultSet loadLiabilities( String toDate, String hierarchyLevel) {
		String profitRoot=UtilFxn.GetValueFromTable("SELECT acc_code from acc_fxn_mapping_mcg where map_for='Profit Account'");
		if(profitRoot.equals(""))
		{
			MessageBox mb = new MessageBox(Display.getCurrent().getActiveShell(),SWT.ICON_ERROR);
			mb.setText("Code Not Found");
			mb.setMessage("Insert Profit Code In Fix Maping Table");
			mb.open();
			return null;
		}
		String strQry = "";

		String firstOpeningBalLed_id=UtilFxn.GetMinValueFromTable("ledger_mcg", "led_id", " jv_no = -1 " +
				"and  posted_date =  (select cast(MAX(posted_date) as date)as lastClosingDate from ledger_mcg where jv_no=-1 and is_opening=1 " +
				"and posted_date <='"+toDate+"') ");
		strQry = "SELECT "
			+ "	acc_code, acc_name, alias, LAmt,parent,coalesce(LAmt,0) AS Amt "
			+ " FROM ( "
			+ "		SELECT "
			+ "			acc_code, acc_name, alias,parent, "
			+ "			(SELECT  "
			+ "				(SUM(cr_amt)-SUM(dr_amt)) "
			+ "			FROM "
			+ "				 ledger_mcg l "
			+ "			WHERE "
			+ "				 (l.acc_code LIKE a.acc_code + '.%' OR l.acc_code=a.acc_code) "
			+ " "+(firstOpeningBalLed_id.equals("") ? "":" AND (led_id >= (select min(led_id) from ledger_mcg where jv_no = -1 and posted_date = (select cast(MAX(posted_date) as date)as lastClosingDate from ledger_mcg where jv_no=-1 and posted_date <='"+toDate+"'))) " )+" "
			+ " and posted_date !> '"
			+ toDate + "' ) AS LAmt  " + "		FROM "
			+ "			acc_head_mcg AS a WHERE acc_type='4' and a.acc_code<>'"+profitRoot+"') AS T1 "
			+ " WHERE coalesce(LAmt,0)>0   ";

		strQry=strQry + ( hierarchyLevel.equalsIgnoreCase("1")?" and parent=0 ":" "+ "ORDER BY case when acc_name !='profit account' then acc_code else acc_name end ");

		try {
			System.out.println("zzz...Liability qry is:"+strQry);
			statement = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			resultset = statement.executeQuery(strQry);

			return resultset;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}//end of method loadLiabilities()

	/**
	 * @param toDate
	 * @param hierarchyLevel
	 * @return
	 */
	public ResultSet loadAssets( String toDate, String hierarchyLevel) {
		String lossRoot=UtilFxn.GetValueFromTable("SELECT acc_code from acc_fxn_mapping_mcg where map_for='Loss Account'");
		if(lossRoot.equals(""))
		{
			MessageBox mb = new MessageBox(Display.getCurrent().getActiveShell(),SWT.ICON_ERROR);
			mb.setText("Code Not Found");
			mb.setMessage("Insert Loss Code In Fix Maping Table");
			mb.open();
			return null;
		}

		String strQry = "";
		String firstOpeningBalLed_id=UtilFxn.GetMinValueFromTable("ledger_mcg", "led_id", " jv_no = -1 and  posted_date =  (select cast(MAX(posted_date) as date)as lastClosingDate from ledger_mcg where jv_no=-1 and posted_date <='"+toDate+"') ");
		strQry = "SELECT "
			+ "	acc_code, acc_name, alias, LAmt ,parent,coalesce(LAmt,0) AS Amt "
			+ " FROM ( "
			+ "		SELECT "
			+ "			acc_code, acc_name, alias,parent, "
			+ "			(SELECT "
			+ "				SUM(dr_amt)-SUM(cr_amt) "
			+ "			 FROM ledger_mcg l "
			+ "				WHERE "
			+ "			(l.acc_code LIKE a.acc_code + '.%' or l.acc_code = a.acc_code) "
			+ " "+(firstOpeningBalLed_id.equals("") ? "":" AND (led_id >= (select min(led_id) from ledger_mcg where jv_no = -1 and posted_date = (select cast(MAX(posted_date) as date)as lastClosingDate from ledger_mcg where jv_no=-1 and posted_date <='"+toDate+"'))) " )+" "
			+ "  and posted_date !> '"
			+ toDate + "') AS LAmt " + "		FROM "
			+ "			acc_head_mcg AS a " + "		WHERE "
			+ "			acc_type ='1' and a.acc_code<>'"+lossRoot+"') AS T1 "
			+ " WHERE coalesce(LAmt,0)>0 ";//ORDER BY 1 --this order by has been shifted into following ternary

		strQry=strQry + ( hierarchyLevel.equalsIgnoreCase("1")?" and parent=0 ":" "+ " ORDER BY 1 ");
		System.out.println("Assets loading query: " + strQry);

		try {
			statement = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			resultset = statement.executeQuery(strQry);

			return resultset;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}//end of method loadAssets()

	public List<TempBalanceSheetModel> loadBSClosing( String toDate,
			int fiscalYear, boolean closingPerformed, String hierarchyLevel,int rpt_time, int rpt_type,String frmDate) {
		List<TempBalanceSheetModel> lstTBSM = new ArrayList<TempBalanceSheetModel>();
		lstTBSM.clear();
		ProfitLossController profitLossController =new ProfitLossController();
		try {
			DecimalFormat df=new DecimalFormat("##,##0.00");
			BigDecimal total =new BigDecimal("0.00") ,amt= new BigDecimal("0.00");
			double expTotal=0.00,incTotal=0.00,profit=0.00,income=0.00;
			//PL calculation
			if (!closingPerformed) {

				ResultSet incomeRs = profitLossController.loadIncome(toDate, "0",frmDate);
				ResultSet expenseRs = profitLossController.loadExpenses(toDate,	"0",frmDate);

				// calculate the total expenses amount
				while (expenseRs.next()) {
					// add the amounts of the top level account codes only
					if (expenseRs.getString(1).matches("[0-9]*"))
						expTotal = expTotal + expenseRs.getDouble("Amt");

				}

				// calculate the total income amount
				while (incomeRs.next()) {
					// add the amounts of the top level account codes only
					if (incomeRs.getString(1).matches("[0-9]*"))
						incTotal = incTotal + incomeRs.getDouble("Amt");

				}

				income= incTotal - expTotal;
				profit = Math.abs(income);

			}
			else if(closingPerformed)
			{
				profit=profitLossController.getLastClosingPL();				
			}

			//Liabilities
			TempBalanceSheetModel objModel = new TempBalanceSheetModel();
			objModel.setAccount_alias("");
			objModel.setParticulars("bfloTj");
			objModel.setTot_sub_head("");
			objModel.setTot_head("");
			lstTBSM.add(objModel);

			ResultSet rsLiability = loadLiabilities(toDate,hierarchyLevel);
			int i=1+UtilMethods.getMaxId("acc_monthly_closing_mcg", "id");
			while(rsLiability.next())
			{
				//int reportid =ApplicationWorkbenchWindowAdvisor.getUserId();
				objModel = new TempBalanceSheetModel();
				//objModel.setId(reportid);
				objModel.setRpt_order(i);
				objModel.setBr_id(1);
				String acc_code=rsLiability.getString("acc_code");
				objModel.setAcc_code(acc_code);

				amt=BigDecimal.valueOf(Math.abs(rsLiability.getDouble("amt")));
				String alias=rsLiability.getString("alias");
				String acc_name=rsLiability.getString("acc_name");
				if (acc_code.matches("[0-9]*")) {
					objModel.setAccount_alias(alias);
					objModel.setParticulars(acc_name);
					objModel.setTot_head(df.format(amt));
					objModel.setTot_sub_head("");
					total = amt.add(total);
				}
				else if (acc_code.matches("[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("   " +alias);
					objModel.setParticulars("   " +acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else if (acc_code.matches("[0-9]*.[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("      " +alias);
					objModel.setParticulars("      " +acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else if (acc_code.matches("[0-9]*.[0-9]*.[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("         " + alias);
					objModel.setParticulars("         " + acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else if (acc_code.matches("[0-9]*.[0-9]*.[0-9]*.[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("            " +alias);
					objModel.setParticulars( "            " +acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else {
					objModel.setAccount_alias("               " +alias);
					objModel.setParticulars("               "+acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				}
				/*objModel.setAccount_alias(rs.getString("alias"));
					objModel.setParticulars(rs.getString("acc_name"));*/

				lstTBSM.add(objModel);
				i++;

			}
			if (!closingPerformed && (income > 0)) {
				objModel = new TempBalanceSheetModel();
				objModel.setAccount_alias("");
				objModel.setParticulars("Profit Magnus Nepali");
				objModel.setTot_sub_head(df.format(profit));					
				objModel.setTot_head("");
				lstTBSM.add(objModel);

				total = total.add(BigDecimal.valueOf(profit));

			}
			else if(closingPerformed && profit<0.00)
			{
				objModel = new TempBalanceSheetModel();
				objModel.setAccount_alias("");
				objModel.setParticulars("Profit Magnus Nepali");
				objModel.setTot_sub_head(df.format(Math.abs(profit)));					
				objModel.setTot_head("");
				lstTBSM.add(objModel);

				total = total.add(BigDecimal.valueOf(Math.abs(profit)));
			}
			objModel = new TempBalanceSheetModel();
			objModel.setAccount_alias("");
			objModel.setParticulars("hDdf");
			objModel.setTot_sub_head("");					
			objModel.setTot_head(df.format(total));
			lstTBSM.add(objModel);

			//re-assign the value of total
			total =new BigDecimal("0.00"); 
			//Assets
			objModel = new TempBalanceSheetModel();
			objModel.setAccount_alias("");
			objModel.setParticulars(";DklQ");
			objModel.setTot_sub_head("");
			objModel.setTot_head("");
			lstTBSM.add(objModel);

			ResultSet rsAssets = loadAssets(toDate,hierarchyLevel);
			while(rsAssets.next())
			{
				//int reportid =ApplicationWorkbenchWindowAdvisor.getUserId();
				objModel = new TempBalanceSheetModel();
				//objModel.setId(reportid);
				objModel.setRpt_order(i);//continue the incremented value of i
				objModel.setBr_id(1);
				String acc_code=rsAssets.getString("acc_code");
				objModel.setAcc_code(acc_code);

				amt=BigDecimal.valueOf(Math.abs(rsAssets.getDouble("amt")));
				String alias=rsAssets.getString("alias");
				String acc_name=rsAssets.getString("acc_name");
				if (acc_code.matches("[0-9]*")) {
					objModel.setAccount_alias(alias);
					objModel.setParticulars(acc_name);
					objModel.setTot_head(df.format(amt));
					objModel.setTot_sub_head("");
					total = amt.add(total);
				}
				else if (acc_code.matches("[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("   " +alias);
					objModel.setParticulars("   " +acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else if (acc_code.matches("[0-9]*.[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("      " +alias);
					objModel.setParticulars("      " +acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else if (acc_code.matches("[0-9]*.[0-9]*.[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("         " + alias);
					objModel.setParticulars("         " + acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else if (acc_code.matches("[0-9]*.[0-9]*.[0-9]*.[0-9]*.[0-9]*")) {
					objModel.setAccount_alias("            " +alias);
					objModel.setParticulars( "            " +acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				} else {
					objModel.setAccount_alias("               " +alias);
					objModel.setParticulars("               "+acc_name);
					objModel.setTot_sub_head(df.format(amt));
					objModel.setTot_head("");
				}
				/*objModel.setAccount_alias(rs.getString("alias"));
					objModel.setParticulars(rs.getString("acc_name"));*/

				lstTBSM.add(objModel);
				i++;

			}
			if (!closingPerformed && (income < 0)) {
				objModel = new TempBalanceSheetModel();
				objModel.setAccount_alias("");
				objModel.setParticulars("Loss Magnus Nepali");
				objModel.setTot_sub_head(df.format(profit));					
				objModel.setTot_head("");
				lstTBSM.add(objModel);

				total = total.add(BigDecimal.valueOf(profit));
			}
			else if(closingPerformed && profit>0.00)
			{
				objModel = new TempBalanceSheetModel();
				objModel.setAccount_alias("");
				objModel.setParticulars("Loss Magnus Nepali");
				objModel.setTot_sub_head(df.format(profit));					
				objModel.setTot_head("");
				lstTBSM.add(objModel);

				total = total.add(BigDecimal.valueOf(profit));
			}
			objModel = new TempBalanceSheetModel();
			objModel.setAccount_alias("");
			objModel.setParticulars("hDdf");
			objModel.setTot_sub_head("");					
			objModel.setTot_head(df.format(total));
			lstTBSM.add(objModel);
			//now save the data for future reporting purposes
			int id=UtilMethods.getMaxId("acc_monthly_closing_mcg", "id");//offset value
			//reassign the value of i for other use
			i=0;
			int br_id=1;
			String trxn_date=jCalendarFunctions.currentDate();
			String trxn_np_date=jCalendarFunctions.getCurrentNepaliDate();
			String npDate[]=trxn_np_date.split(ApplicationWorkbenchWindowAdvisor.getDateSeperator());
			int quarter=ApplicationWorkbenchWindowAdvisor.getCurrentClosingPeriodId();
			int creator_id=ApplicationWorkbenchWindowAdvisor.getLoginId();


			for (Iterator<TempBalanceSheetModel> iterator = lstTBSM
					.iterator(); iterator.hasNext();) {
				TempBalanceSheetModel tbsm = iterator.next();
				String futureData="INSERT INTO [acc_monthly_closing_mcg] " +
				"([id],[br_id],[fy_id],[date],[nep_date],[nep_month],[nep_year],[quarter],[acc_code] ,[alias],[acc_name],[dr_amt]" +
				" ,[cr_amt],[created_by],[created_dt] ,[update_count],[summary_time],[summary_type]) " +
				" VALUES " +
				" ("+(id+i)+","+br_id+","+fiscalYear+",'"+trxn_date+"','"+trxn_np_date+"',"+Integer.valueOf(npDate[1])+","+Integer.valueOf(npDate[0])+", " +
				" "+quarter+", '"+tbsm.getAcc_code()+"', '"+tbsm.getAccount_alias()+"', '"+tbsm.getParticulars().replaceAll("'", "''").replaceAll("\"", "\\\"")+"','"+tbsm.getTot_sub_head()+"','"+tbsm.getTot_head()+"',"+creator_id+",GETDATE(),0,"+rpt_time+","+rpt_type+" ) ";
				cnn = DbConnection.getConnection();
				cnn.setAutoCommit(false);
				statement = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_UPDATABLE);
				if(statement.executeUpdate(futureData)>0)
				{
					
						cnn.commit();
						cnn.setAutoCommit(true);
					
					
				}
				else
				{
					cnn.rollback();
					cnn.setAutoCommit(true);
				}
				//increment value of i
				i++;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lstTBSM;
	}//end of method loadTrialBalance()
	/**
	 * @param date
	 * @return
	 */
	public boolean isClosingPerformed(String date) {
		boolean retFlag = false;
		String sql = "SELECT COUNT(*) AS total FROM ledger_mcg WHERE jv_no = -1 and is_opening=1 AND posted_date = '"
			+ date + "'";

		try {
			cnn = DbConnection.getConnection();
			statement = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			resultset = statement.executeQuery(sql);

			if (resultset.next()) {
				if (resultset.getInt("total") >= 1)
					retFlag = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retFlag;
	}//end of method isClosingPerformed()

	/**
	 * On the assumption that after the end of closing entries with jv_no = -1
	 * in ledger_mcg, the profit/loss amount will be transferred. If jv_no for
	 * transactions exists on the closing date after closing entries, then
	 * profit/loss amount is assumed to be transferred to the head office.
	 * That's the rule.
	 * 
	 */
	public boolean isTransferred(String date) {
		boolean retFlag = false;
		String sql = "SELECT COUNT(*) AS total FROM ledger_mcg WHERE "
			+ "led_id > (SELECT max(led_id)  FROM ledger_mcg WHERE posted_date = '"
			+ date + "'" + " and jv_no = -1 ) and posted_date = '" + date
			+ "'";

		System.out.println("isTransferred sql:" + sql);

		try {
			statement = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			resultset = statement.executeQuery(sql);

			if (resultset.next()) {
				if (resultset.getInt("total") > 0)
					retFlag = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("retFlag: " + retFlag);
		return retFlag;

	}

}//end of class
