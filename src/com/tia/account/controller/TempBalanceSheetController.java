package com.tia.account.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.tia.account.model.TempBalanceSheetModel;
import com.tia.common.db.DbConnection;



/**
 * @author suresh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 */
public class TempBalanceSheetController {
	Connection cnn;
	ResultSet rs;
	Statement st;
	String rptType1;
	String rptType2;

	/**
	 * default constructor method
	 */
	public TempBalanceSheetController() {
		cnn = DbConnection.getConnection();
		rptType1="trialbalance";
		rptType2="balancesheet";

	}//end of constructor

	/**
	 * @param model
	 * @return boolean value
	 * 
	 * This method inserts the values from model class into
	 * its respective database table each time when Balance sheet report
	 * is intended to be printed. No transaction need to be maintained here
	 * and we do not need to save the query for this method.
	 */
	public boolean insert(TempBalanceSheetModel model)
	{
		boolean check=false;
		String strQry="";
		try {
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			strQry=model.Insert();
			System.out.println("The query to save temp values is :" +strQry);
			int result = st.executeUpdate(strQry);
			if(result==1)
			{
				check=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return check;
	}//end of method insert()

	/**
	 * @param rtpType
	 * @param id
	 * 
	 * This method deletes temporary data from table temp_balance_sheet
	 * each time when Balance sheet report is intended to be printed. 
	 * No transaction need to be maintained here and we do not need 
	 * to save the query for this method.
	 */
	public void deleteAll(String rtpType,int id)
	{
		String strQry="";
		try {
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			strQry="DELETE FROM temp_balance_sheet WHERE id='"+id+"' AND rpt_for='"+rtpType+"'";
			System.out.println("The query to delete temp values is :" +strQry);
			int result = st.executeUpdate(strQry);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//end of method deleteall()
	/**
	 * @param id
	 * 
	 * This method sets the action for 
	 * rptType1=trialbalance
	 * rptType2=balancesheet
	 * 
	 * This is because we use same temp_balance_sheet table in database to
	 * hold the temporary data for reports of Balance sheet and Trial balance.
	 */
	public void deleteAction(int id)
	{
		this.deleteAll(rptType1,id);
		this.deleteAll(rptType2,id);

	}//end of method deleteAction()
}//end of class
