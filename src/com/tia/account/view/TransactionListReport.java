/**
 * 
 */
package com.tia.account.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Panel;
import java.io.InputStream;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JRootPane;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JRViewer;

import org.eclipse.nebula.widgets.grid.Grid;
import org.eclipse.nebula.widgets.grid.GridColumn;
import org.eclipse.nebula.widgets.grid.GridColumnGroup;
import org.eclipse.nebula.widgets.grid.GridItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;



import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.controller.TransactionListReportController;
import com.tia.account.model.TransactionListReportModel;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.LoginCheck;
import com.tia.common.util.POITest;
import com.tia.common.util.UtilMethods;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import com.tia.plugins.JnepCalendar.calendar.JDateChooser;

/**
 * @author suresh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 */
public class TransactionListReport extends ViewPart {

	/**
	 * 
	 */
	private JDateChooser txtFromDate;
	private JDateChooser txtToDate;
	private Grid gridTransactionList;
	private Composite comParent;

	private Composite swtAwtComposite;
	private Button cmdToExcel;
	private Button cmdPrint;

	JCalendarFunctions jCalendarFunctions = new JCalendarFunctions();
	Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();

	// amount formatter
	DecimalFormat decimalFormat = new DecimalFormat("##,##0.00");
	// initialize from and to date params from GUI inputs
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	public TransactionListReport() {
		setTitleImage(SWTResourceManager.getImage(TransactionListReport.class,
		"/logo.png"));
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite comMain = new Composite(parent, SWT.NONE);
		Banner.getTitle("Transaction List Report", comMain,
				TransactionListReport.class);
		comMain.setLayout(new GridLayout(1, false));

		comParent = new Composite(comMain, SWT.NONE);
		GridData gd_comParent = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_comParent.heightHint = 631;
		gd_comParent.widthHint = 959;
		comParent.setLayoutData(gd_comParent);

		Group grpCommands = new Group(comParent, SWT.NONE);
		grpCommands.setBounds(10, 6, 757, 52);

		Label lblFrom = new Label(grpCommands, SWT.NONE);
		lblFrom.setAlignment(SWT.RIGHT);
		lblFrom.setText("From");
		lblFrom.setBounds(14, 19, 44, 18);

		Label lblTo = new Label(grpCommands, SWT.NONE);
		lblTo.setAlignment(SWT.RIGHT);
		lblTo.setText("To");
		
		lblTo.setBounds(256, 19, 44, 18);

		// composite for from date
		Composite compFromDatePicker = new Composite(grpCommands, SWT.NONE);
		compFromDatePicker.setBounds(72, 18, 170, 21);

		// From date
		Composite comFromDate = new Composite(compFromDatePicker,
				SWT.NO_BACKGROUND | SWT.EMBEDDED);
		comFromDate.setToolTipText("Swing Date Picker");
		comFromDate.setBounds(1, 1, 169, 20);

		GridData gd_dobComp = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_dobComp.heightHint = 20;
		gd_dobComp.widthHint = 188;
		comFromDate.setLayoutData(gd_dobComp);

		Frame fromFrame = SWT_AWT.new_Frame(comFromDate);
		Panel fromPanel = new Panel(new BorderLayout()) {

			@Override
			public void update(java.awt.Graphics g) {

				paint(g);
			}
		};
		fromFrame.add(fromPanel);
		JRootPane fromRoot = new JRootPane();
		fromPanel.add(fromRoot);
		Container fromContentPane = fromRoot.getContentPane();

		txtFromDate = new JDateChooser();
		fromContentPane.setLayout(new BorderLayout());
		fromContentPane.add(txtFromDate, BorderLayout.CENTER);
		/*
		 * From date should be the start date of the fiscal year by default.
		 */

		Date fiscalStartDate = new Date();

		try {
			fiscalStartDate = ApplicationWorkbenchWindowAdvisor
			.getCurClosingPeriodStartMonthEnDt();
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		txtFromDate.setDate(fiscalStartDate);

		// composite holding the date picker for To
		Composite comToDatePicker = new Composite(grpCommands, SWT.NONE);
		comToDatePicker.setBounds(314, 18, 170, 21);

		// To date
		Composite comToDate = new Composite(comToDatePicker, SWT.NO_BACKGROUND
				| SWT.EMBEDDED);
		comToDate.setToolTipText("Swing Date Picker");
		comToDate.setBounds(1, 1, 169, 20);

		Frame toFrame = SWT_AWT.new_Frame(comToDate);
		Panel toPanel = new Panel(new BorderLayout()) {

			@Override
			public void update(java.awt.Graphics g) {

				paint(g);
			}
		};
		toFrame.add(toPanel);
		JRootPane toRoot = new JRootPane();
		toPanel.add(toRoot);
		Container toContentPane = toRoot.getContentPane();

		txtToDate = new JDateChooser();
		toContentPane.setLayout(new BorderLayout());
		toContentPane.add(txtToDate, BorderLayout.CENTER);

		// display the date
		txtToDate.setDate(java.sql.Date
				.valueOf(JCalendarFunctions.currentDate()));
		// create the grid
		createGrid();

		Button cmdView = new Button(grpCommands, SWT.NONE);
		cmdView.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (!LoginCheck.isLogIn()) {
					LoginCheck.showMessage();
					return;
				}
				cmdPrint.setEnabled(false);
				cmdToExcel.setEnabled(false);
				// fill grid
				if (gridTransactionList.isDisposed()) {
					swtAwtComposite.dispose();

					createGrid();
				} else {

					gridTransactionList.dispose();
					createGrid();
				}

				fillGrid();
				cmdToExcel.setEnabled(true);
				cmdPrint.setEnabled(true);
			}
		});
		cmdView.setBounds(498, 16, 70, 25);
		cmdView.setText("View");
		

		cmdPrint = new Button(grpCommands, SWT.NONE);
		cmdPrint.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// method call for printing report
				cmdPrint.setEnabled(false);
				cmdToExcel.setEnabled(false);
				if (gridTransactionList.getItemCount() < 1) {
					MessageBox mb = new MessageBox(shell, SWT.ICON_ERROR);
					mb.setMessage("There is nothing to print");
					mb.setText("NoData");
					mb.open();
					return;
				}
				cmdPrintActionPerformed();
			}
		});
		cmdPrint.setText("Print");
		cmdPrint.setBounds(582, 16, 70, 25);
		cmdPrint.setEnabled(false);

		cmdToExcel = new Button(grpCommands, SWT.NONE);
		cmdToExcel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// method call for export the GUI presented data into excel file
				cmdToExcel.setEnabled(false);
				cmdPrint.setEnabled(false);
				// little stuff for Messaging for errors
				if (gridTransactionList.getItemCount() < 1) {
					MessageBox mb = new MessageBox(shell, SWT.ICON_ERROR);
					mb.setMessage("There is no Data in Grid to export to excel");
					mb.setText("Empty data Source");
					mb.open();
				} else {
					// actual exporting method call
					String titleAndSub = "Transaction List Report ( From "
						+ simpleDateFormat.format(txtFromDate.getDate()) + " To "
						+ simpleDateFormat.format(txtToDate.getDate()) + " )";
					
					if (POITest.exportToExcel(
							gridTransactionList,
							"TransactionListReport_"
							+ jCalendarFunctions.getCurrentNepaliDate(), titleAndSub,
							"", 2, 3)) {
						MessageBox mb = new MessageBox(shell, SWT.ICON_INFORMATION);
						mb.setMessage("TransactionListReport"
								+ jCalendarFunctions.GetNepaliDate(simpleDateFormat.format(txtFromDate
										.getDate()))
										+ ("To"
												+ jCalendarFunctions.GetNepaliDate(simpleDateFormat
														.format(txtToDate.getDate())) + ("has been successfuly exported to excel file.")));
						mb.setText("ExporttoExcelSuccess");
						mb.open();
					}
				}
			}
		});
		cmdToExcel.setText("ToExcel");
		cmdToExcel.setBounds(666, 16, 70, 25);
		cmdToExcel.setEnabled(false);
		grpCommands.setTabList(new Control[] { cmdView, cmdPrint, cmdToExcel });

		Button btnJavamethodcallinjasper = new Button(comParent, SWT.NONE);
		btnJavamethodcallinjasper.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				createViewerComposite();
				gridTransactionList.setVisible(true);
				cmdPrintActionPerformedTesting();
			}
		});
		btnJavamethodcallinjasper.setBounds(10, 231, 192, 25);
		btnJavamethodcallinjasper.setText("Java Method Call In Jasper");
		btnJavamethodcallinjasper.setVisible(false);

		GridData gd_toDateComp = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_toDateComp.heightHint = 20;
		gd_toDateComp.widthHint = 188;

		// TODO Auto-generated method stub

	}// end of create partControl() method

	/**
	 * method to create GUI Grid to present data
	 */
	private void createGrid() {
		// Displaying the ledger table
		gridTransactionList = new Grid(comParent, SWT.BORDER | SWT.H_SCROLL
				| SWT.V_SCROLL);
		gridTransactionList.setBounds(10, 74, 948, 490);
		gridTransactionList.setHeaderVisible(true);
		gridTransactionList.setLinesVisible(true);
		gridTransactionList.setRowHeaderVisible(true);
		
		// defining the columns in the table
		String[] colNames = { ("Date"),
				("VoucherNo."),"Name",
				"Particulars", "Dr.",
				"Cr.", "Remarks",
				"UserName" };
		/*
		 * We need to either Left align or Right align the columns. O means Left
		 * align, 1 means Right align. The other is for width.
		 */
		int[][] intAlign = { { 0, 80 }, { 1, 80 }, { 0, 150 }, { 0, 200 },
				{ 1, 100 }, { 1, 100 }, { 0, 500 }, { 0, 150 } };

		GridColumn[] columns = new GridColumn[colNames.length];

		for (int i = 0; i < colNames.length; i++) {
			if (intAlign[i][0] == 0)
				columns[i] = new GridColumn(gridTransactionList, SWT.NONE);
			else
				columns[i] = new GridColumn(gridTransactionList, SWT.RIGHT);
			// columns[i].setResizable(true);
			columns[i].setWidth(intAlign[i][1]);
			columns[i].setText(colNames[i]);

		}
		GridColumnGroup gcg = new GridColumnGroup(gridTransactionList, SWT.NONE);

	}// end of method createGrid()

	/**
	 * method to populate the GUI Grid with data obtained
	 */
	private void fillGrid() {
		// clear whatever the data in grid already be
		gridTransactionList.removeAll();

		String fromDate = simpleDateFormat.format(txtFromDate.getDate());
		String toDate = simpleDateFormat.format(txtToDate.getDate());
		// initialize the list from controller
		List<TransactionListReportModel> listTransactionListReportModel = TransactionListReportController
		.getAllTrxnList(fromDate, toDate);
		// little stuff for messeging
		if (listTransactionListReportModel.isEmpty()) {
			MessageBox mb = new MessageBox(shell, SWT.ICON_ERROR);
			mb.setMessage("ThereisnoTransaction(s)foundfrom"
					+ jCalendarFunctions.GetNepaliDate(fromDate)
					+ " and to "
					+ jCalendarFunctions.GetNepaliDate(toDate) + "");
			mb.setText("NoTransactions");
			mb.open();
		}
		GridItem giDataRow;
		// now actually start filling data into grid from list
		for (Iterator<TransactionListReportModel> iterator = listTransactionListReportModel
				.iterator(); iterator.hasNext();) {
			TransactionListReportModel tlrm = iterator.next();
			giDataRow = new GridItem(gridTransactionList, SWT.None);

			giDataRow.setText(0, tlrm.getJv_date());
			giDataRow.setText(1, tlrm.getJv_no());
			giDataRow.setText(2, tlrm.getName());
			giDataRow.setText(3, tlrm.getParticulars());
			giDataRow.setText(4, decimalFormat.format(Double.valueOf(tlrm.getDr_amt())));
			giDataRow.setText(5, decimalFormat.format(Double.valueOf(tlrm.getCr_amt())));
			giDataRow.setText(6, tlrm.getRemarks());
			giDataRow.setText(7, tlrm.getUser_name());

		}
		gridTransactionList.setRedraw(true);
	}// end of method

	/**
	 * method to create composite to present report 
	 */
	private void createViewerComposite() {
		// Defining an instance of ViewerComposite into the view
		swtAwtComposite = new Composite(comParent, SWT.EMBEDDED);
		swtAwtComposite.setBounds(10, 74, 948, 490);

	}// end of method

	/**
	 * method to perform report print operation 
	 */
	public void cmdPrintActionPerformed() {
		try {
			final JasperPrint jprint = generateReport();
			if (jprint.getPages().isEmpty()) {
				System.out.println("Report is null");
				MessageBox mb = new MessageBox(shell, SWT.ERROR);
				mb.setText("ReportError");
				mb.setMessage("NoDataToPrint");
				mb.open();
				if (gridTransactionList.isDisposed()) {
					swtAwtComposite.dispose();
					createViewerComposite();
				}
			} else {
				System.out.println("Report is not null");
				org.eclipse.swt.widgets.Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						// dispose off the table now
						gridTransactionList.dispose();
						createViewerComposite();
						//gridTransactionList.setVisible(false);
						// Defining an instance of ViewerComposite into the view
						JRViewer jasperviewer = new JRViewer(jprint);
						// viewerComposite.setVisible(false);

						Frame frame = SWT_AWT.new_Frame(swtAwtComposite);

						Panel panel = new Panel();
						frame.add(panel);
						panel.setLayout(new BorderLayout(0, 0));

						JRootPane rootPane = new JRootPane();
						rootPane.setSize(767, 600);
						panel.add(rootPane);
						Container c = rootPane.getContentPane();
						c.add(jasperviewer);
					}
				});

			}// else ends

		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}// end of method

	/**
	 * @return jasperprint
	 * 
	 * method to generate jasperprint document of the 
	 * report being printed
	 */
	public JasperPrint generateReport() {
		// load the collection list for report datasource
		java.util.List<TransactionListReportModel> listTransactionListReportModel = new java.util.ArrayList<TransactionListReportModel>();
		TransactionListReportModel transactinListReportModel;
		for (int rowCount = 0; rowCount < gridTransactionList.getItemCount(); rowCount++) {
			transactinListReportModel = new TransactionListReportModel();
			transactinListReportModel.setJv_date(gridTransactionList.getItem(rowCount).getText(0));
			transactinListReportModel.setJv_no(gridTransactionList.getItem(rowCount).getText(1));
			transactinListReportModel.setName(gridTransactionList.getItem(rowCount).getText(2));
			transactinListReportModel.setParticulars(gridTransactionList.getItem(rowCount).getText(3));
			transactinListReportModel.setDr_amt(gridTransactionList.getItem(rowCount).getText(4));
			transactinListReportModel.setCr_amt(gridTransactionList.getItem(rowCount).getText(5));
			transactinListReportModel.setRemarks(gridTransactionList.getItem(rowCount).getText(6));
			transactinListReportModel.setUser_name(gridTransactionList.getItem(rowCount).getText(7));
			// add the object into collection
			listTransactionListReportModel.add(transactinListReportModel);
		}
		JasperPrint myJPrint = null;
		try {
			String fromDate = simpleDateFormat.format(txtFromDate.getDate());
			String toDate = simpleDateFormat.format(txtToDate.getDate());

			String reportTitle="Transaction List";
			String reportSubTitle="("+fromDate + " To "+toDate+")";
			

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("REPORT_DIR", TransactionListReport.class
					.getClassLoader()
					.getResource("TransactionListReport.jrxml").toString()
					.replace("TransactionListReport.jrxml", ""));
			params.put("BANK_NAME",
					ApplicationWorkbenchWindowAdvisor.getOfficeName());
			params.put("BANK_ADDRESS",
					ApplicationWorkbenchWindowAdvisor.getOfficeAddress());
			params.put("report_title", reportTitle);
			params.put("subTitle",reportSubTitle );
			params.put("logo", UtilMethods.getLogoImage());
			InputStream jasperDesign = null;
			JasperReport jasperReport = null;

			jasperDesign = TransactionListReport.class.getClassLoader()
			.getResourceAsStream("TransactionListReport.jrxml");

			jasperReport = JasperCompileManager.compileReport(jasperDesign);

			/*
			 * Filling the report with data from the database based on the
			 * parameters passed.
			 */
			myJPrint = JasperFillManager
			.fillReport(
					jasperReport,
					params,
					new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource(
							listTransactionListReportModel));
			params.clear();
		} catch (JRException ex) {
			ex.printStackTrace();
		}
		return myJPrint;
	}

	/**
	 * method to print testing report
	 */
	public void cmdPrintActionPerformedTesting() {
		try {
			JasperPrint jprint = null;
			jprint = generateReportTest();
			if (jprint.getPages().isEmpty()) {
				System.out.println("Report is null");
				MessageBox mb = new MessageBox(shell, SWT.ERROR);
				mb.setText("ReportError");
				mb.setMessage("NoDataToPrint");
				mb.open();
			} else {
				System.out.println("Report is not null");
				// dispose off the table now
				gridTransactionList.setVisible(false);
				// Defining an instance of ViewerComposite into the view

				// createViewerComposite();
				JRViewer jasperviewer = new JRViewer(jprint);
				// viewerComposite.setVisible(false);

				Frame frame = SWT_AWT.new_Frame(swtAwtComposite);

				Panel panel = new Panel();
				frame.add(panel);
				panel.setLayout(new BorderLayout(0, 0));

				JRootPane rootPane = new JRootPane();
				rootPane.setSize(767, 600);
				panel.add(rootPane);
				Container c = rootPane.getContentPane();
				c.add(jasperviewer);

			}// else ends

		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}// end of method

	/**
	 * @return
	 * method to generate jasperprint document for 
	 * testing print
	 */
	public JasperPrint generateReportTest() {
		Connection conn = null;
		JasperPrint jasperPrint = null;
		try {

			conn = DbConnection.getConnection();

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("REPORT_DIR", TransactionListReport.class
					.getClassLoader().getResource("JavaTesting.jrxml")
					.toString().replace("JavaTesting.jrxml", ""));
			InputStream jasperDesign = null;
			JasperReport jasperReport = null;

			jasperDesign = TransactionListReport.class.getClassLoader()
			.getResourceAsStream("JavaTesting.jrxml");

			jasperReport = JasperCompileManager.compileReport(jasperDesign);

			/*
			 * Filling the report with data from the database based on the
			 * parameters passed.
			 */
			jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
			params.clear();
		} catch (JRException ex) {
			ex.printStackTrace();
		}
		return jasperPrint;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}
}// end of class
