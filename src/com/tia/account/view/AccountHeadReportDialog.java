package com.tia.account.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Panel;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JRootPane;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JRViewer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.model.AccHeadModel;
import com.tia.common.util.Banner;
import com.tia.common.util.UtilMethods;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;



/**
 * @author suresh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 */
public class AccountHeadReportDialog extends Dialog {
	private Composite comParent;
	private Composite comMainContainer;
	Shell shell;
	Object rtn;
	java.util.List<AccHeadModel> listHead;
	JCalendarFunctions jcal;

	public AccountHeadReportDialog(Shell parentShell,java.util.List<AccHeadModel> listRptCol) {
		super(parentShell);
		listHead=listRptCol;
		jcal=new JCalendarFunctions();

	}

	public void open() {
		Shell parent = getParent();
		shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setText("Account Head Report");
		Image image = new Image(shell.getDisplay(),AccountHeadReportDialog.class.getResourceAsStream("/ah16.png"));
		shell.setImage(image);
		
		shell.setSize(970, 670);
		// code for widget creation, set result, etc
		createDialogArea(shell);
		shell.open();
		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	protected Control createDialogArea(Composite parent) {
		comParent = parent;// (Composite) super.createDialogArea(parent);
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("AccountHeadReport", comParent,AccountHeadReportDialog.class);

		// Main container that holds all other controls/widgets of the view
		comMainContainer = new Composite(comParent, SWT.NONE);
		comMainContainer.setLayout(new FormLayout());
		GridData gd_comMainContainer = new GridData(SWT.LEFT, SWT.CENTER,false, false, 1, 1);
		gd_comMainContainer.widthHint = 959;
		gd_comMainContainer.heightHint = 640;
		comMainContainer.setLayoutData(gd_comMainContainer);

		Group grpReport = new Group(comMainContainer, SWT.NONE);
		FormData fd_grpMinistry = new FormData();
		fd_grpMinistry.bottom = new FormAttachment(100, -10);
		fd_grpMinistry.right = new FormAttachment(0, 959);
		fd_grpMinistry.top = new FormAttachment(0, 5);
		fd_grpMinistry.left = new FormAttachment(0);
		grpReport.setLayoutData(fd_grpMinistry);
		grpReport.setBounds(206, 25, 270, 135);
		try{

			JasperDesign jasperDesign = JRXmlLoader.load(AccountHeadReportDialog.class
					.getClassLoader().getResourceAsStream(
							"AccountHeads.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

			// parameters to be passed to generate the report
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("REPORT_DIR", AccountHeadReportDialog.class.getClassLoader()
					.getResource("AccountHeads.jrxml").toString().replace("AccountHeads.jrxml", ""));
			params.put("institutionName",
					ApplicationWorkbenchWindowAdvisor.getOfficeName());
			params.put("address",
					ApplicationWorkbenchWindowAdvisor.getOfficeAddress());
			params.put("report_title", "n]vf zLif{s");
			params.put("subTitle", "-"+jcal.getCurrentNepaliDate()+"_");
			params.put("logo", UtilMethods.getLogoImage());
			// generating the final print
			JasperPrint jprint= JasperFillManager.fillReport(jasperReport,params,new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource(
					listHead));

			Composite swtAwtComposite = new Composite(grpReport, SWT.EMBEDDED);
			swtAwtComposite.setBounds(10, 16, 923, 596);

			if (jprint.getPages().isEmpty()) {
				System.out.println("Report is null");
				MessageBox mb = new MessageBox(shell, SWT.ERROR);
				mb.setText("Report Error");
				mb.setMessage("No Data To Print");
				mb.open();
			} else {
				System.out.println("Report is not null");
				// Defining an instance of ViewerComposite into the view
				JRViewer jasperviewer = new JRViewer(jprint);
				// viewerComposite.setVisible(false);

				Frame frame = SWT_AWT.new_Frame(swtAwtComposite);

				Panel panel = new Panel();
				frame.add(panel);
				panel.setLayout(new BorderLayout(0, 0));

				JRootPane rootPane = new JRootPane();
				rootPane.setSize(703, 600);
				panel.add(rootPane);
				Container c = rootPane.getContentPane();
				c.add(jasperviewer);

			}// else ends


		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return comParent;

		// TODO Auto-generated method stub

	}

	private boolean closeIt(){
		shell.dispose();
		return true;
	}
}//class ends