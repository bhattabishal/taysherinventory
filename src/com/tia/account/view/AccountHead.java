package com.tia.account.view;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;


import com.swtdesigner.SWTResourceManager;
import com.tia.account.controller.AccHeadController;
import com.tia.account.controller.LedgerController;
import com.tia.account.model.AccHeadModel;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.CmbQry;
import com.tia.common.util.DecimalTextPositiveOnly;
import com.tia.common.util.LoginCheck;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.plugins.DynaTree.controller.DynaTree;
import com.tia.plugins.DynaTree.model.TreeNode;
import com.tia.plugins.DynaTree.ui.TreeNodeLabelProvider;

public class AccountHead extends ViewPart {
	private Text txtAccCode;
	private Text txtAlias;
	private Text txtAccName;
	private Text txtMinBal;
	private Text txtRemark;
	private Text txtParent;
	private Text txtId;
	private Text txtParentId;
	private Composite compositeTree;
	private Composite comMain;
	private Composite comBody;
	private Composite comButton;
	private Button cmdNew;
	private Button cmdSave;
	private Button cmdEdit;
	private Button cmdCancel;
	private Button cmdDelete;
	private SwtBndCombo cmbAccType;
	private Group grpDrCr;
	private Button rdoDr;
	private Button rdoCr;
	private Button cmdAddeditOpeningBalance;
	private Button cmdPrint;
	private Tree tree;
	private Tree rptTree;
	private DynaTree dynaTree;
	private String oldLogData = "";
	private Shell shell;

	protected TreeViewer treeViewer;
	protected TreeNode root;
	protected TreeNodeLabelProvider labelProvider;
	protected Action onlyBoardGamesAction, atLeatThreeItems;
	protected Action booksBoxesGamesAction, noArticleAction;
	protected Action addTreeNodeAction, removeTreeNodeAction, addLeafAction,
			removeLeafAction;
	protected ViewerFilter onlyBoardGamesFilter, atLeastThreeFilter;
	protected ViewerSorter booksBoxesGamesSorter, noArticleSorter;

	private static java.util.List<AccHeadModel> listHead;

	int maxSubAccId = 0;
	AccHeadController objController;
	AccHeadModel objModel;

	/**
	 * Default Constructor
	 */
	public AccountHead() {
		objController = new AccHeadController();
		objModel = new AccHeadModel();

		shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();

		setTitleImage(SWTResourceManager.getImage(AccountHead.class,
				"/ah16.png"));

	}

	@Override
	public void createPartControl(Composite parent) {
		// set the Gridlayout to the main form
		GridLayout gl_parent1 = new GridLayout(1, false);
		gl_parent1.marginHeight = 1;
		parent.setLayout(gl_parent1);
		Banner.getTitle("AccountHead", parent,
				AccountHead.class);

		// Main composite holding other controls and set Grid Layout to it too
		comMain = new Composite(parent, SWT.NONE);
		GridData gd_comMain = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_comMain.heightHint = 613;
		gd_comMain.widthHint = 948;
		comMain.setBounds(5, 5, 970, 590);
		comMain.setLayout(null);
		comMain.setLayoutData(gd_comMain);

		// composite for holding the tree and set the Grid Layout
		compositeTree = new Group(comMain, SWT.NONE);
		compositeTree.setBounds(5, 10, 392, 503);

		// tree displaying the account heads
		treeViewer = new TreeViewer(compositeTree);
		tree = treeViewer.getTree();
		
		tree.setBounds(10, 18, 372, 474);

		/*
		 * Initially had a problem displaying the tree on the composite. This
		 * problem was solved by implementing the form layout for the Tree
		 * instance i.e. tree in this case Same method followed in other forms
		 * as well.
		 */
		// the compositeTree composite has Form layout set to it.
		FormData fd_tree = new FormData();
		fd_tree.top = new FormAttachment(0, 10);
		fd_tree.bottom = new FormAttachment(100, -10);
		fd_tree.right = new FormAttachment(100, -10);
		fd_tree.left = new FormAttachment(0, 10);
		tree.setLayoutData(fd_tree);

		rptTree = new Tree(compositeTree, SWT.BORDER | SWT.H_SCROLL
				| SWT.V_SCROLL);
		// treeJournalList.setFont(CommonFunctions.getFont());
		rptTree.setBounds(10, 20, 359, 435);
		rptTree.setHeaderVisible(true);
		rptTree.setLinesVisible(true);
		// treeJournalList.redraw();
		
		TreeColumn[] col = new TreeColumn[9];

		int[][] intAlign = { { 0, 40 }, { 0, 40 }, { 0, 80 }, { 0, 40 },
				{ 0, 80 } };
		String[] colNames = { "", "Account Code", "Account Alias", "Account Code",
				"Type" };
		for (int i = 0; i < colNames.length; i++) {
			if (intAlign[i][0] == 0)
				col[i] = new TreeColumn(rptTree, SWT.NONE);

			col[i].setResizable(true);
			col[i].setWidth(intAlign[i][1]);
			col[i].setText(colNames[i]);
		}
		// fillRptTree();//fill the tree just before printing report to make
		// initial loading time of view faster

		// now, create the dynamic tree
		createTree();

		// composite for the Body
		comBody = new Group(comMain, SWT.NONE);
		comBody.setBounds(415, 10, 496, 503);
		comBody.setLayout(null);
		Composite comForm = new Group(comBody, SWT.NONE);
		comForm.setBounds(24, 21, 451, 315);
		comForm.setLayout(null);

		// label for account code (AccCode)
		Label labAccCode = new Label(comForm, SWT.NONE);
		labAccCode.setAlignment(SWT.RIGHT);
		labAccCode.setBounds(19, 21, 92, 18);
		labAccCode.setText("AccountCode");
		

		// textbox and settings for account code
		txtAccCode = new Text(comForm, SWT.BORDER);
		txtAccCode.setBounds(117, 16, 314, 24);
		
		txtAccCode.setEditable(false);

		// label for parent account
		Label labParent = new Label(comForm, SWT.NONE);
		labParent.setAlignment(SWT.RIGHT);
		labParent.setBounds(6, 50, 105, 18);
		labParent.setText("Sub Account Code");
		

		// textbox and settings for the txtParentId
		txtParent = new Text(comForm, SWT.BORDER);
		txtParent.setBounds(117, 45, 314, 24);
		
		txtParent.setEditable(false);

		// label for alias
		Label lblAlias = new Label(comForm, SWT.NONE);
		lblAlias.setAlignment(SWT.RIGHT);
		lblAlias.setBounds(19, 79, 92, 18);
		lblAlias.setText(("Alias"));
		// lblAlias.setFont(CommonFunctions.getFontLabel());
		lblAlias.setForeground(UtilMethods.getFontForegroundColorRed());

		// textbox for alias
		txtAlias = new Text(comForm, SWT.BORDER);
		txtAlias.setBounds(117, 74, 314, 24);
		

		// label for account name
		Label lblAccName = new Label(comForm, SWT.NONE);
		lblAccName.setAlignment(SWT.RIGHT);
		lblAccName.setBounds(11, 108, 100, 18);
		lblAccName.setText("AccountName");
		// lblAccName.setFont(CommonFunctions.getFontLabel());
		lblAccName.setForeground(UtilMethods.getFontForegroundColorRed());

		// textbox for account name
		txtAccName = new Text(comForm, SWT.BORDER);
		txtAccName.setBounds(117, 103, 314, 24);
		

		// label for account type
		Label lblAccType = new Label(comForm, SWT.NONE);
		lblAccType.setAlignment(SWT.RIGHT);
		lblAccType.setBounds(26, 138, 85, 18);
		lblAccType.setText("AccountType");
		lblAccType.setForeground(UtilMethods.getFontForegroundColorRed());

		// combo box for account type
		cmbAccType = new SwtBndCombo(comForm, SWT.READ_ONLY);
		cmbAccType.setBounds(117, 132, 314, 26);
		
		cmbAccType.setEnabled(false);
		cmbAccType.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// set the Dr. Cr. radio buttons -- altered by suresh on 11 DEC
				// 2011
				if (cmbAccType.getSelectedBoundValue().equals("1")
						|| cmbAccType.getSelectedBoundValue().equals("2")) {
					rdoDr.setSelection(true);
					rdoCr.setSelection(false);
				} else if (cmbAccType.getSelectedBoundValue().equals("3")
						|| cmbAccType.getSelectedBoundValue().equals("4")) {
					rdoCr.setSelection(true);
					rdoDr.setSelection(false);
				} else if (cmbAccType.getSelectedBoundValue().equals("5")) {
					// clear the selections of the Dr. Cr. Radio Buttons if any
					// is checked already
					rdoCr.setSelection(false);
					rdoDr.setSelection(false);
					// if(objModel)
				}
				// if selection type is Account/Liabilities
				if (cmbAccType.getSelectedBoundValue().equals("5")) {

					grpDrCr.setEnabled(true);
					rdoDr.setEnabled(true);
					rdoCr.setEnabled(true);

				} else {
					grpDrCr.setEnabled(false);
					rdoDr.setEnabled(false);
					rdoCr.setEnabled(false);
				}

			}
		});
		cmbAccType.fillData(CmbQry.getAccTypeQuery(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection());

		Label lblDrcr = new Label(comForm, SWT.NONE);
		lblDrcr.setAlignment(SWT.RIGHT);
		lblDrcr.setBounds(13, 181, 98, 18);
		lblDrcr.setText("Dr/Cr");
		

		grpDrCr = new Group(comForm, SWT.NONE);
		grpDrCr.setBounds(117, 163, 314, 49);
		grpDrCr.setEnabled(false);

		rdoDr = new Button(grpDrCr, SWT.RADIO);
		rdoDr.setSelection(true);
		rdoDr.setBounds(10, 10, 142, 18);
		rdoDr.setText("Debit Opening Balance");
		
		rdoDr.setEnabled(false);

		rdoCr = new Button(grpDrCr, SWT.RADIO);
		rdoCr.setBounds(10, 29, 142, 16);
		rdoCr.setText("Credit Opening Balance");
		
		rdoCr.setEnabled(false);

		// label for minimum balance
		Label lblOpeningBalance = new Label(comForm, SWT.NONE);
		lblOpeningBalance.setAlignment(SWT.RIGHT);
		lblOpeningBalance.setBounds(19, 219, 92, 20);
		lblOpeningBalance.setText("OpeningBalance");
		

		// textbox for opening balance
		txtMinBal = new Text(comForm, SWT.BORDER);
		txtMinBal.setBounds(119, 217, 136, 24);
		txtMinBal.setEnabled(true);
		txtMinBal.setEnabled(false);// default nature of this textbox
		/*
		 * Validation stuff
		 */
		txtMinBal.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtMinBal, 20));
		

		// label for remarks
		Label lblRemark = new Label(comForm, SWT.NONE);
		lblRemark.setAlignment(SWT.RIGHT);
		lblRemark.setBounds(26, 246, 85, 18);
		lblRemark.setText("Remarks");
		

		// textbox for remarks
		txtRemark = new Text(comForm, SWT.BORDER | SWT.MULTI);
		txtRemark.setBounds(117, 246, 314, 59);
		

		cmdAddeditOpeningBalance = new Button(comForm, SWT.NONE);
		cmdAddeditOpeningBalance.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// validation
				if (objController.hasTransaction(txtAccCode.getText())
						|| !objController.isNodeLeaf(txtAccCode.getText())) {
					MessageBox mb = new MessageBox(shell, SWT.ERROR);
					mb.setText("Error");
					mb.setMessage("The selected account head already has either a child node or a transaction."
							+ "Editing is not allowed.");
					mb.open();
				} else if (normalPostingsFound()) {
					MessageBox mb = new MessageBox(shell, SWT.ERROR);
					mb.setText("Error");
					mb.setMessage("Opening Balance AddEdit Not Allowed");
					mb.open();
					return;
				} else {
					// code to handle add/Edit opening balance
					AccHeadModel ahm = setAccount();
					OpeningBalanceAddEdit obDialog = new OpeningBalanceAddEdit(
							PlatformUI.getWorkbench()
									.getActiveWorkbenchWindow().getShell(), ahm);
					txtMinBal.setText(obDialog.open());
				}
			}
		});
		cmdAddeditOpeningBalance.setBounds(259, 216, 172, 25);
		cmdAddeditOpeningBalance.setText("Add/Edit Opening Balance");
		

		// composite that holds the buttons with Grid Layout
		comButton = new Group(comBody, SWT.NONE);
		comButton.setBounds(24, 351, 451, 46);
		comButton.setLayout(null);

		cmdNew = new Button(comButton, SWT.NONE);
		cmdNew.setBounds(7, 13, 65, 25);

		// widget selected code for New button
		cmdNew.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (!LoginCheck.isLogIn()) {
					LoginCheck.showMessage();
					return;
				}
				cmdNewActionPerformed();
			}
		});
		cmdNew.setText("New");
		

		// Save button
		cmdSave = new Button(comButton, SWT.NONE);
		cmdSave.setBounds(79, 13, 65, 25);

		// widget selected code for Save button
		cmdSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (!LoginCheck.isLogIn()) {
					LoginCheck.showMessage();
					return;
				}
				cmdSaveActionPerformed();
			}
		});
		cmdSave.setText("Save");
		
		cmdSave.setEnabled(false);

		// Edit button
		cmdEdit = new Button(comButton, SWT.NONE);
		cmdEdit.setBounds(151, 13, 65, 25);
		cmdEdit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (!LoginCheck.isLogIn()) {
					LoginCheck.showMessage();
					return;
				}
				cmdEditActionPerformed();
			}
		});
		cmdEdit.setText("Edit");
		

		// Delete button
		cmdDelete = new Button(comButton, SWT.NONE);
		cmdDelete.setBounds(223, 13, 65, 25);

		// widget selected code for delete button
		cmdDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				cmdDeleteActionPerformed();
			}
		});
		cmdDelete.setText("Delete");
		

		// Cancel button
		cmdCancel = new Button(comButton, SWT.NONE);
		cmdCancel.setBounds(296, 13, 65, 25);
		cmdCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				cmdCancel_actionPerformed();
			}
		});
		cmdCancel.setText("Cancel");
		

		cmdPrint = new Button(comButton, SWT.NONE);
		cmdPrint.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// disable the button during printing pends
				cmdPrint.setEnabled(false);
				// load the tree with data from data
				fillRptTree();
				listHead = new ArrayList<AccHeadModel>();
				/*
				 * for(int i=0;i<treeViewer.getTree().getItemCount();i++) {
				 * TreeItem[] ti=treeViewer.getTree().getItems();
				 * System.out.println(ti.toString()); }
				 */
				expandTreeAll(rptTree, true);
				traverse(rptTree);
				/*
				 * for(int row=0;row<listHead.size();row++) { AccHeadModel
				 * ahm=listHead.get(row);
				 * System.out.println(ahm.getAcc_code()+", "
				 * +ahm.getAlias()+", "+
				 * ahm.getAcc_name()+", "+ahm.getAcc_type()); }
				 */
				new AccountHeadReportDialog(new Shell(), listHead).open();
				// finally enable the print button
				cmdPrint.setEnabled(true);
			}
		});
		cmdPrint.setText("Print");
		cmdPrint.setBounds(367, 13, 65, 25);
		cmdPrint.setVisible(false);

		// hidden txtId
		txtId = new Text(comBody, SWT.BORDER);
		txtId.setBounds(115, 406, 404, 24);
		
		txtId.setEnabled(false);
		txtId.setEditable(false);
		txtId.setVisible(false);

		// hidden txtParentId
		txtParentId = new Text(comBody, SWT.BORDER);
		txtParentId.setBounds(115, 438, 404, 24);
		
		txtParentId.setEnabled(false);
		txtParentId.setEditable(false);
		txtParentId.setVisible(false);

		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		Label label = new Label(parent, SWT.NONE);
		label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false,
				1, 1));
	}// end of method createPartControl()

	/**
	 * When a node in the tree is selected, this method is invoked which will
	 * fetch all the details of the selected node and fills up the form fields
	 * for the selected node.
	 * 
	 * @param accId
	 *            (acc_head_id of the selected node)
	 * @return void
	 */
	private void LoadData(String accId) {
		setEditableFields(false);
		objModel = objController.getAccount(accId);

		txtParent.setText(String.valueOf(objModel.getParent_name()));
		txtAlias.setText(objModel.getAlias());
		txtAccName.setText(objModel.getAcc_name());
		cmbAccType.setSelectedBoundValue(objModel.getAcc_type());
		/*
		 * These if-else LOC till immediate println() line below is to handle
		 * the Dr Cr radio selection according to user clicks on the respective
		 * A/C heads on the left tree in GUI. --- added by suresh on 11 DEC 2011
		 */
		if (objModel.getAcc_type().equals("1")
				|| objModel.getAcc_type().equals("2")
				|| (objModel.getAcc_type().equals("5") && objModel.getDrcr()
						.equalsIgnoreCase("dr"))) {
			rdoDr.setSelection(true);
			rdoCr.setSelection(false);
		} else if (objModel.getAcc_type().equals("3")
				|| objModel.getAcc_type().equals("4")
				|| (objModel.getAcc_type().equals("5") && objModel.getDrcr()
						.equalsIgnoreCase("cr"))) {
			rdoCr.setSelection(true);
			rdoDr.setSelection(false);
		}
		System.out.println("min bal is " + objModel.getMin_bal());

		/*
		 * Account heads with opening balance not set should have their opening
		 * balance control set to null (not 0,zero)
		 */
		if (objModel.getMin_bal() != null)
			if (Double.parseDouble(objModel.getMin_bal()) > 0)
				txtMinBal.setText(objModel.getMin_bal());
			else
				txtMinBal.setText("");

		txtRemark.setText(objModel.getRemarks());
		txtId.setText(String.valueOf(objModel.getAcc_head_id()));
		txtParentId.setText(String.valueOf(objModel.getParent()));

		/* Setting the old data for log file */
		oldLogData = "\t(Old):\tAccCode->" + txtAccCode.getText()
				+ "\tSubAccCode->" + txtParent.getText() + "\tSubName->"
				+ txtAlias.getText() + "\tAccName->" + txtAccName.getText()
				+ "\tAccTyp->" + cmbAccType.getText() + "\tDrCrOpen-"
				+ (rdoCr.getSelection() == true ? "Cr" : "Dr") + "\tOpenAmt->"
				+ txtMinBal.getText() + "\tAccHeadId->"
				+ objModel.getAcc_head_id();
		System.err.println("Old Data Setting-" + oldLogData);

		cmdSave.setEnabled(false);
	}// end of method LoadData()

	/**
	 * cmdNewActionPerformed() to register and handle the action generated by
	 * the New button.
	 * 
	 * @param null
	 * @return void
	 */
	private void cmdNewActionPerformed() {
		/*
		 * If none of the account head is selected, i.e. the txtAccCode text
		 * input is empty, then the newly created entry will be of the top-most.
		 * So, we set the txtId to 0 because txtId will ultimately be parentId.
		 */
		if (txtAccCode.getText().isEmpty()) {
			txtId.setText("");
			txtParentId.setText("0");

			cmbAccType.setSelectedBoundValue(0);
			cmbAccType.setEnabled(true);
			// set the parent name
			txtParent.setText("");
			txtAccName.setText("");
			txtAlias.setText("");
			txtAccCode.setText("");
			txtMinBal.setText("");
			txtRemark.setText("");

			txtAlias.setEditable(true);
			txtAccName.setEditable(true);
			cmbAccType.setEnabled(true);
			txtRemark.setEditable(true);
			cmdNew.setEnabled(false);
			cmdSave.setEnabled(true);
			cmdEdit.setEnabled(false);
			cmdCancel.setEnabled(true);

		} else {
			/*
			 * if any account is selected Check to see if the selected account
			 * has any sub-accounts created for it or not. If yes, we cannot
			 * make it parent anymore, else proceed further.
			 */
			if (objController.getAccCodeCount(txtAccCode.getText()) > 0) {
				MessageBox mb = new MessageBox(shell, SWT.ERROR);
				mb.setText("Error");
				mb.setMessage("Asub-account for this entry already exists.");
				mb.open();
				return;
			}

			/*
			 * Check to see if the selected account has had any journal
			 * transaction yet. If yes, we cannot create further children for
			 * it.
			 */
			if (objController.hasTransaction(txtAccCode.getText())) {
				MessageBox mb = new MessageBox(shell, SWT.ERROR);
				mb.setText("Error");
				mb.setMessage("The account is already involved in a transaction.");
				mb.open();
				return;
			}

			/*
			 * Check to see if the selected account head has a opening balance
			 * set to it. If yes, we cannot create further children for it.
			 */
			if (objController.checkForTransactions(txtAccCode.getText()) >= 1) {
				MessageBox mb = new MessageBox(shell, SWT.ERROR);
				mb.setText("Error");
				mb.setMessage("The account head has a opening balance associated with it.");
				mb.open();
				return;
			}

			// current txtId becomes the parentId for the next entry
			txtParentId.setText(txtId.getText());
			txtId.setText("");

			/*
			 * If creating an entry for a parent other than the top-most level,
			 * the newly created entry must have the same account type as its
			 * parent.
			 */
			if (txtParentId.getText().equals(""))
				cmbAccType.setSelectedBoundValue(0);
			else {
				cmbAccType.setSelectedBoundValue(cmbAccType
						.getSelectedBoundValue());
				cmbAccType.setFocus();
			}

			// set the parent name
			txtParent.setText(txtAccName.getText());
			txtAccName.setText("");
			txtAlias.setText("");
			txtAccCode.setText("");
			txtMinBal.setText("");
			txtRemark.setText("");

			txtAlias.setEditable(true);
			txtAccName.setEditable(true);
			cmbAccType.setEnabled(false);
			txtMinBal.setEditable(true);
			txtRemark.setEditable(true);
			cmdNew.setEnabled(false);
			cmdSave.setEnabled(true);
			cmdEdit.setEnabled(false);
			cmdCancel.setEnabled(true);

		}
	}// end of method cmdNewActionPerformed()

	/**
	 * cmdSaveActionPerformed() to register and handle the action generated by
	 * the Save button.
	 * 
	 * @param null
	 * @return void
	 */
	private void cmdSaveActionPerformed() {
		System.out
				.println("zzError tracing mechanism ..... before beforeSave() call !");
		if (!beforeSave()) {
			System.out
					.println("zzError tracing mechanism ..... after beforeSave() call !");
			return;
		}
		// case for creating a new account head with new account alias
		if ((objController.getAccAlias(txtAlias.getText())) <= 0) {
			System.out
					.println("zzError tracing mechanism ..... before saveData() call !");
			saveData();
			System.out
					.println("zzError tracing mechanism ..... after saveData() call !");
		}

		/*
		 * Case for updating an account head.
		 * 
		 * If updating an account head, the account code is already showing up
		 * in the txtAccCode textbox.
		 */
		else if ((objController.getAccAlias(txtAlias.getText()) == 1)
				&& !txtAccCode.getText().isEmpty()) {
			saveData();
		}

		// case when an attempt to create an account head with a duplicate alias
		else if ((objController.getAccAlias(txtAlias.getText())) == 1) {

			MessageBox mb = new MessageBox(shell, SWT.ERROR);
			mb.setText("Error");
			mb.setMessage("Anaccountentrywiththesamealiasalreadyexists.");
			mb.open();

		} else {
			MessageBox mb = new MessageBox(shell, SWT.ERROR);
			mb.setText("Error");
			mb.setMessage("An error occurred.");
			mb.open();
		}
	}// end of method cmdSaveActionPerformed()

	/**
	 * Method that saves the new account entry.
	 * 
	 * @param null
	 * @return void
	 * */
	private void saveData() {
		AccHeadModel headModel = setAccount();
		AccHeadController headController = new AccHeadController();
		boolean empty = true;

		// flag to check if opening balance was entered or not
		if (!txtMinBal.getText().isEmpty())
			empty = false;

		if (headController.SaveAccHead(headModel, empty)) {
			cmbAccType.setEnabled(true);
			cmdNew.setEnabled(true);
			cmdSave.setEnabled(false);
			cmdEdit.setEnabled(true);

			MessageBox msgSuccess = new MessageBox(shell);
			msgSuccess.setMessage("Datasaved/updated successfully.");
			msgSuccess.setText("Success");
			msgSuccess.open();
			IWorkbenchPage wbp = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage();
			final IViewReference[] viewRefs = wbp.getViewReferences();
			for (int i = 0; i < viewRefs.length; i++) {
				if (viewRefs[i].getId().equalsIgnoreCase(
						"com.tia.account.accountHead")) {
					IViewReference ivr = viewRefs[i];
					// close view first and reopen
					wbp.hideView(ivr);
					// then open it again
				}
			}
			// now reopen the view
			try {
				wbp.showView("com.tia.account.accountHead");
			} catch (PartInitException e1) {
				e1.printStackTrace();
			}
		} else {
			MessageBox msgFailed = new MessageBox(shell);
			msgFailed.setMessage("Data couldn't be saved.");
			msgFailed.setText("Failed");
			msgFailed.open();
		}
	}// end of method saveData()

	/**
	 * This method receives the inputs from the user to create a new account
	 * entry and saves them to an object of type AccHeadModel. This object is
	 * then returned.
	 * 
	 * @param null
	 * @return object (object of type AccHeadModel in which the fields are set
	 *         to the values received from the user)
	 */
	private AccHeadModel setAccount() {
		String newLogData = "";
		System.out.println("Set Account Method is called");
		if (!(objController.getAccCode(txtParentId.getText())).equals(null)) {
			objModel.setAlias(txtAlias.getText());
			objModel.setAcc_head_id(Integer.parseInt(!txtId.getText()
					.equals("") ? txtId.getText() : "0"));

			// parent for the new entry is set from the hidden parentId field
			objModel.setParent(!txtParentId.getText().equals("") ? txtParentId
					.getText() : "0");

			/*
			 * Allowing the users to enter an apostrophe in the text. For each
			 * added apostrophe we add an extra apostrophe for escaping purposes
			 * in SQL Server.
			 */
			objModel.setAcc_name(txtAccName.getText().replace("'", "''"));

			/*
			 * Get the account code from the database directly in case of
			 * insert. But if its a update, fetch it from the current value in
			 * txtAccCode textbox.
			 */
			if (txtAccCode.getText().isEmpty())
				objModel.setAcc_code(objController.getAccCode(txtParentId
						.getText()));
			else

				objModel.setAcc_code(txtAccCode.getText());

			objModel.setAcc_type(cmbAccType.getSelectedBoundValue().toString());

			/*
			 * For 5th account types i.e. Assets/Liabilities we need to specify
			 * explicitly either Debit or Credit to save the opening balance.
			 */
			if (Integer.parseInt(objModel.getAcc_type()) == 5) {
				if (rdoDr.getSelection()) {
					objModel.setIsDr(true);
					LedgerController.isALdr = true;
				} else {
					objModel.setIsDr(false);
					LedgerController.isALdr = false;
				}
			} else {
				if (cmbAccType.getText().equalsIgnoreCase("assets")
						|| cmbAccType.getText().equalsIgnoreCase("expenses")) {
					objModel.isDr = true;
				} else if (cmbAccType.getText().equalsIgnoreCase("income")
						|| cmbAccType.getText().equalsIgnoreCase("liability")) {
					objModel.isDr = false;
				}
			}
			if (!txtMinBal.getText().isEmpty())
				objModel.setMin_bal(Double.parseDouble(txtMinBal.getText()));
			else
				objModel.setMin_bal(0);

			objModel.setRemarks(txtRemark.getText().replace("'", "''"));

			objModel.setAcc_level(UtilFxn.getTVLevel("acc_head_mcg",
					"acc_head_id", "parent", ""));

			/*
			 * To load the tree node dynamically, we need to call the method for
			 * which the newly added node's Id must be passed.
			 */
			maxSubAccId = UtilMethods.getMaxId("acc_head_mcg", "acc_head_id");

			objModel.setBr_id(1);
			objModel.setJv_status(1);
			// if the current account head is being updated, store its accCode
			objModel.setCurrentAccCode(txtAccCode.getText());

			int tmp = objController.getUpdateCount(objModel.getAcc_head_id());

			if (tmp >= 0)
				objModel.setUpdate_count(tmp);
			else
				objModel.setUpdate_count(0);

			newLogData = "\t\t(New):\tAccCode->" + txtAccCode.getText()
					+ "\tSubAccCode->" + txtParent.getText() + "\tSubName->"
					+ txtAlias.getText() + "\tAccName->" + txtAccName.getText()
					+ "\tAccTyp->" + cmbAccType.getText() + "\tDrCrOpen->"
					+ (rdoCr.getSelection() == true ? "Cr" : "Dr")
					+ "\tOpenAmt->" + txtMinBal.getText();
			/* Setting data for logfile */
			objModel.setLogFile(oldLogData.length() == 0 ? "Nodata"
					: oldLogData + "\t <----> \t" + newLogData);
			System.err.println("Log file-" + objModel.getLogFile());
			System.err.println("Old dAta-" + oldLogData);
			System.err.println("New file-" + newLogData);

			return objModel;

		} else if ((objController.getAccCode(txtParentId.getText())).equals(0)) {
			objModel.setAlias(txtAlias.getText());
			objModel.setAcc_head_id(UtilMethods.getMaxId("acc_head_mcg",
					"acc_head_id"));

			// parent for the new entry is set from the hidden parentId field
			objModel.setParent(!txtParentId.getText().equals("") ? txtParentId
					.getText() : "0");
			objModel.setAcc_name(txtAccName.getText().replace("'", "''"));

			// get the account code from the database directly
			objModel.setAcc_code(objController.getAccCode(txtParentId.getText()));
			objModel.setAcc_type(cmbAccType.getSelectedBoundValue().toString());
			objModel.setMin_bal(Double.parseDouble(!txtMinBal.getText().equals(
					"") ? txtMinBal.getText() : "0"));
			objModel.setRemarks(txtRemark.getText().replace("'", "''"));

			objModel.setAcc_level(UtilFxn.getTVLevel("acc_head_mcg",
					"acc_head_id", "parent", ""));

			/*
			 * To load the tree node dynamically, we need to call the method for
			 * which the newly added node's Id must be passed.
			 */
			maxSubAccId = UtilMethods.getMaxId("acc_head_mcg", "sub_acc_head_id");

			// set the br_id statically
			objModel.setBr_id(1);

			int tmp = objController.getUpdateCount(objModel.getAcc_head_id());

			if (tmp >= 0)
				objModel.setUpdate_count(tmp);
			else
				objModel.setUpdate_count(0);

			newLogData = "\t\t(New:)\tAccCode-" + txtAccCode.getText()
					+ "\tSubAccCode-" + txtParent.getText() + "\tSubName-"
					+ txtAlias.getText() + "\tAccName" + txtAccName.getText()
					+ "\tAccTyp" + cmbAccType.getText() + "\tDrCrOpen-"
					+ (rdoCr.getSelection() == true ? "Cr" : "Dr")
					+ "\tOpenAmt-" + txtMinBal.getText();
			/* Setting data for logfile */
			objModel.setLogFile(oldLogData.length() == 0 ? "Nodata"
					: oldLogData + "\t <---->  \t" + newLogData);
			System.err.println("Log file-" + objModel.getLogFile());
			System.err.println("Old dAta-" + oldLogData);
			System.err.println("New file-" + newLogData);

			return objModel;
		} else {
			newLogData = "\t\t\t(New:)\tAccCode-" + txtAccCode.getText()
					+ "\tSubAccCode-" + txtParent.getText() + "\tSubName-"
					+ txtAlias.getText() + "\tAccName" + txtAccName.getText()
					+ "\tAccTyp" + cmbAccType.getText() + "\tDrCrOpen-"
					+ (rdoCr.getSelection() == true ? "Cr" : "Dr")
					+ "\tOpenAmt-" + txtMinBal.getText();
			/* Setting data for logfile */
			objModel.setLogFile(oldLogData.length() == 0 ? "Nodata"
					: oldLogData + "\t <----> \t" + newLogData);
			System.err.println("Log file-" + objModel.getLogFile());
			System.err.println("Old dAta-" + oldLogData);
			System.err.println("New file-" + newLogData);
			return null;
		}
	}// end of method setAccount()

	/**
	 * This method contains LOC for validation of needed input data.
	 */
	private boolean beforeSave() {
		String msg = "The following fields are mandatory."
				+ "\n";
		boolean flag = true;

		if (cmbAccType.getSelectedBoundValue().toString() == null
				|| cmbAccType.getSelectionIndex() == 0) {
			msg += "Account type must be selected" + "\n";

			flag = false;
		}
		if (txtAccName.getText().isEmpty()) {
			msg += "Account name must be specified" + "\n";
			flag = false;
		}
		if (txtAlias.getText().isEmpty()) {
			msg += "Alias must be specified" + "\n";
			flag = false;
		}
		if (!txtMinBal.getText().trim().equals("")) {
			if (Double.valueOf(txtMinBal.getText()) > 0
					&& normalPostingsFound()) {
				msg += "Opening Balance Add Edit Not Allowed"
						+ "\n";
				flag = false;
			}
		}
		if (!flag) {
			messageBox(SWT.ICON_ERROR, "Validation Error",
					msg).open();
		} else {
			flag = true;
		}
		return flag;
	}// end of method beforeSave()

	/**
	 * This method returns true if normal transactions are found in either or
	 * both of Journal Voucher table or ledger table and false if not found in
	 * both.
	 * 
	 * We'll employ this method to prevent adding opening balance in account
	 * head view when there is at least one normal posting(s) in either of those
	 * tables.
	 */
	private boolean normalPostingsFound() {
		if (Integer
				.valueOf(UtilFxn
						.GetCountValueFromTable(
								"ledger_mcg",
								"acc_code",
								"jv_no<>-1 and posted_date>=(select max(posted_date) from ledger_mcg where jv_no=-1 and is_opening=1)")) > 0
				|| Integer
						.valueOf(UtilFxn
								.GetCountValueFromTable(
										"journal_voucher_details_mcg",
										"acc_code",
										"jv_no<>-1 and created_date>=(select max(posted_date) from ledger_mcg where jv_no=-1 and is_opening=1)")) > 0) {
			return true;
		} else
			return false;

	}

	/**
	 * Method thats renders the form fields to either editable or uneditable.
	 * 
	 * @param boolean (true or false flag)
	 * @return void
	 */
	private void setEditableFields(boolean flg) {
		txtAccCode.setEditable(flg);
		txtParent.setEditable(flg);
		txtAlias.setEditable(flg);
		txtAccName.setEditable(flg);
		cmbAccType.setEnabled(flg);
		txtMinBal.setEditable(flg);
		txtRemark.setText("");
		txtRemark.setEditable(flg);
	}// end of method setEditableFields()

	/**
	 * cmdCancel_actionPerformed() to register and handle the action generated
	 * by the cancel button.
	 * 
	 * @param none
	 * @return void
	 */
	private void cmdCancel_actionPerformed() {
		txtAccCode.setText("");
		txtParent.setText("");
		txtAlias.setText("");
		txtAccName.setText("");
		cmbAccType.setSelectedBoundValue(0);
		cmbAccType.setEnabled(false);
		txtMinBal.setText("");
		txtRemark.setText("");
		txtId.setText("");
		txtParentId.setText("0");
		oldLogData = "";

		cmdNew.setEnabled(true);
		cmdSave.setEnabled(false);
		cmdEdit.setEnabled(true);
		cmdCancel.setEnabled(false);
		/*
		 * make Dr and Cr radios inactive and no selected -- altered by suresh
		 * on 11 DEC 2011
		 */
		rdoDr.setEnabled(false);
		rdoDr.setSelection(false);
		rdoCr.setEnabled(false);
		rdoCr.setSelection(false);

		treeViewer.setSelection(null);
	}

	/**
	 * cmdEdit_actionPerformed() to register and handle the action event
	 * generated by the Edit button.
	 * 
	 * @return void
	 */
	private void cmdEditActionPerformed() {

		/*
		 * We allow editing of account head fields freely. But for the fields
		 * opening balance and account type of an account head, it is editable
		 * only if the account head has no associated transaction in journal and
		 * ledger.
		 * 
		 * Suppose, a user created an account head with no opening balance.
		 * There will be no transaction in journal and ledger and the user can
		 * at later date can edit any fields. And while editing, the user added
		 * a opening balance, it would be treated as a transaction and recorded
		 * in journal and ledger. Thenafter the user will not be able to edit
		 * the opening balance and the account type of the account head.
		 */

		// dynaTree.removeSelected();
		if (// objController.checkForTransactions(txtAccCode.getText()) >= 1
		objController.hasTransaction(txtAccCode.getText())
				|| !objController.isNodeLeaf(txtAccCode.getText())) {
			MessageBox mb = new MessageBox(shell, SWT.ERROR);
			mb.setText("Error");
			mb.setMessage("The selected account head already has either a child node or a transaction."
					+ "Editing is not allowed.");
			mb.open();
		} else {

			txtAlias.setEditable(true);
			txtAccName.setEditable(true);
			cmbAccType.setEnabled(true);
			txtMinBal.setEditable(true);
			txtMinBal.setEnabled(true);
			txtRemark.setEditable(true);
			cmdSave.setEnabled(true);
			cmdEdit.setEnabled(false);
			cmdCancel.setEnabled(true);

			// if a node is being edited, and that node is a child node,
			// select/highlight the node to which it is a parent
			// treeViewer.setSelection("", true);
			// dynaTree.
			// dynaTree.addNewTreeNode(
			// Integer.toString(headModel.getAcc_head_id()),
			// headModel.getAcc_name(), headModel.getAcc_code());

		}
	}// end of method

	private MessageBox messageBox(int style, String text, String msg) {
		MessageBox mb;

		if (style < -1)
			mb = new MessageBox(shell);
		else
			mb = new MessageBox(shell, style);

		mb.setText(text);
		mb.setMessage(msg);
		// System.out.println("++++++++"+msg);

		return mb;

	}// end of function

	/**
	 * cmdDeleteActionPerformed() to register and handle the action event
	 * generated by the delete button.
	 * 
	 * @param null
	 * @return void
	 */
	private void cmdDeleteActionPerformed() {
		if (beforeDelete(txtAccCode.getText())) {
			MessageBox mb = new MessageBox(shell, SWT.ICON_QUESTION | SWT.YES
					| SWT.NO | SWT.CANCEL);
			mb.setText("Delete Confirmation");
			mb.setMessage("Do You Really Want To Delete the selected Record?");

			if (mb.open() == SWT.YES) {
				if (objController
						.deleteAccHead(Integer.parseInt(txtId.getText()),
								txtAccCode.getText())) {
					// remove the selected node from tree
					dynaTree.removeSelected();

					MessageBox succMB = new MessageBox(shell,
							SWT.ICON_INFORMATION);
					succMB.setText("Delete");
					succMB.setMessage("Delete Successfull");
					succMB.open();

					return;

				} else {
					MessageBox errMB = new MessageBox(shell, SWT.ERROR);
					errMB.setText("Delete Error");
					errMB.setMessage("Delete not successfull");
					errMB.open();

					return;
				}
			}
		}
	}

	/**
	 * beforeDelete() checks for the validity for an account head to be deleted.
	 * The valid cases are: a) An account head must be a leaf node. b) An
	 * account head must not be involved in any kind of transaction. c) An
	 * account head must not have any subaccount heads.
	 * 
	 * @param accCode
	 *            (Account code of the account head whose validity is to be
	 *            tested for.)
	 * @return boolean (true if eligible for deletion, false otherwise);
	 */
	private boolean beforeDelete(String accCode) {
		/*
		 * Check if there are any children for this node. If yes, return false,
		 * else return true.
		 */
		if (!objController.isNodeLeaf(accCode)) {
			MessageBox mb = new MessageBox(shell, SWT.ERROR);
			mb.setText("Error");
			mb.setMessage("The selected node is not a leaf node.");
			mb.open();
			return false;
		}

		/*
		 * Even if this node is a leaf node, it must not be involved in any kind
		 * of transaction in journal_voucher_details_mcg table.
		 */
		if (objController.hasTransaction(accCode)
				|| objController.checkForTransactions(accCode) >= 1) {
			MessageBox mb = new MessageBox(shell, SWT.ERROR);
			mb.setText("Error");
			mb.setMessage("The selected node is involved in transactions.");
			mb.open();
			return false;
		}

		/*
		 * Check if there are any subaccount heads associated with this account
		 * head.
		 */
		if (objController.getAccCodeCount(accCode) > 0) {
			MessageBox mb = new MessageBox(shell, SWT.ERROR);
			mb.setText("Error");
			mb.setMessage("Subaccount(s)are associated with this account head.");
			mb.open();
			return false;

		}
		return true;
	}

	private void createTree() {
		dynaTree = new DynaTree(
				DbConnection.getConnection(),
				"SELECT acc_head_id AS id, acc_name AS label, parent AS parent, CAST((REPLACE(acc_code,'.','')) AS float) AS sort, "
						+ "acc_code AS code " + "FROM acc_head_mcg ", "parent",
				"sort");

		treeViewer = dynaTree.createTree(treeViewer);
		labelProvider = new TreeNodeLabelProvider();

		// widget selection listener for the dynamic tree
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				// if the selection is empty, clear all the control fields
				labelProvider = new TreeNodeLabelProvider();
				if (event.getSelection().isEmpty()) {
					txtAccCode.setText("");
					txtParent.setText("");
					txtAlias.setText("");
					txtAccName.setText("");
					txtMinBal.setText("");
					txtRemark.setText("");
					return;
				}
				if (event.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) event
							.getSelection();
					/*
					 * Only three things acc_id, acc_code and acc_name are
					 * provided by the TreeNodeLabelProvider.
					 */
					// StringBuffer for id(acc_head_id)
					StringBuffer accId = new StringBuffer();

					// StringBuffer for acc_code
					StringBuffer accCode = new StringBuffer();

					// StringBuffer for acc_name
					StringBuffer accName = new StringBuffer();

					for (Iterator iterator = selection.iterator(); iterator
							.hasNext();) {
						Object domain = iterator.next();
						String id = labelProvider.getId(domain);
						String code = labelProvider.getCode(domain);
						String name = labelProvider.getText(domain);

						// set the StringBuffers to the tree node values
						accId.append(id);
						accCode.append(code);
						accName.append(name);
					}
					// working here today--06-FRIDAY
					/*
					 * // remove the trailing comma space pair if
					 * (toShow.length() > 0) { toShow.setLength(toShow.length()
					 * - 2); }
					 */
					txtAccCode.setText(accCode.toString());
					txtAccName.setText(accName.toString());
					// txtId.setText(accName.toString());

					/*
					 * Now use the acc_id (id) to fetch other details from the
					 * database and load the values into the form fields.
					 */
					LoadData(accId.toString());

					/*
					 * While selecting a new node, Cancel button must be
					 * disabled, while New, Edit and Delete buttons must be
					 * enabled.
					 */
					cmdNew.setEnabled(true);
					cmdSave.setEnabled(false);
					cmdEdit.setEnabled(true);
					cmdDelete.setEnabled(true);
					cmdCancel.setEnabled(true);

					/*
					 * opening balance is editable only when the account head
					 * currently under considertaion is a leaf node.
					 */
					if (objController.isNodeLeaf(accCode.toString()))
						txtMinBal.setEditable(true);
					else
						txtMinBal.setEditable(false);
				}
			}
		});
	}// end of method createTree()

	@Override
	public void setFocus() {

	}

	/**
	 * @param tree
	 *            method to traverse tree
	 */
	static void traverse(Tree tree) {
		org.eclipse.swt.widgets.TreeItem[] items = tree.getItems();
		for (int i = 0; i < items.length; i++) {
			traverse(items[i], tree.getColumnCount());
		}
	}

	/**
	 * @param item
	 * @param cols
	 * 
	 *            method to traverse all items and sub-items of tree in all
	 *            columns
	 */
	static void traverse(org.eclipse.swt.widgets.TreeItem item, int cols) {
		String string = "";
		AccHeadModel ahm = new AccHeadModel();
		string += item.getText();
		// i++;
		// }
		ahm.setAcc_code(item.getText(1));
		ahm.setAlias(item.getText(2));
		ahm.setAcc_name(item.getText(3));
		ahm.setAcc_type(item.getText(4));

		listHead.add(ahm);

		org.eclipse.swt.widgets.TreeItem temp = item.getParentItem();
		while (temp != null) {
			temp = temp.getParentItem();
		}

		System.out.println(string);
		org.eclipse.swt.widgets.TreeItem[] items = item.getItems();
		for (int i = 0; i < items.length; i++) {
			traverse(items[i], cols);
		}
	}

	private void expandTreeAll(org.eclipse.swt.widgets.Tree tree,
			boolean expandFlag) {
		TreeItem[] treeItems = tree.getItems();
		if (treeItems != null) {
			for (TreeItem treeItem : treeItems) {
				treeItem.setExpanded(expandFlag);
			}
		}
	}

	/**
	 * This method fills the rptTree in GUI with account heads and sub heads
	 * from database table. However, user can not see the UI; coz we have sent
	 * it backside of actual tree being rendered in UI.
	 */
	private void fillRptTree() {
		java.sql.ResultSet rsRoots = AccHeadController.getAccRoots();
		for (int i = 0; i < rptTree.getColumnCount(); i++) {
			try {
				while (rsRoots.next()) {
					TreeItem item = new TreeItem(rptTree, SWT.NONE);
					// item.setFont(CommonFunctions.getFont());
					item.setText(new String[] { "+", rsRoots.getString(1),
							rsRoots.getString(2), rsRoots.getString(3),
							rsRoots.getString("acc_type") });
					int isRoot = Integer.valueOf(UtilFxn
							.GetCountValueFromTable("acc_head_mcg", "acc_code",
									"parent=" + rsRoots.getInt("acc_head_id")
											+ ""));
					if (isRoot > 0) {
						fillAllChilds(rsRoots.getInt("acc_head_id"), item);
					}

				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

	private void fillAllChilds(int acc_head_id, TreeItem parent) {
		java.sql.ResultSet rsChilds = AccHeadController
				.getAccChilds(acc_head_id);
		try {
			while (rsChilds.next()) {
				int isChildRoot = Integer
						.valueOf(UtilFxn.GetCountValueFromTable("acc_head_mcg",
								"acc_code",
								"parent=" + rsChilds.getInt("acc_head_id") + ""));
				TreeItem tiChild = new TreeItem(parent, SWT.NONE);
				String whiteSpaces = "";
				if (rsChilds.getString("acc_code").matches("[0-9]*.[0-9]*")) {
					whiteSpaces = "   ";
				} else if (rsChilds.getString("acc_code").matches(
						"[0-9]*.[0-9]*.[0-9]*")) {
					whiteSpaces = "     ";
				} else if (rsChilds.getString("acc_code").matches(
						"[0-9]*.[0-9]*.[0-9]*.[0-9]*")) {
					whiteSpaces = "       ";
				} else if (rsChilds.getString("acc_code").matches(
						"[0-9]*.[0-9]*.[0-9]*.[0-9]*.[0-9]*")) {
					whiteSpaces = "         ";
				} else if (rsChilds.getString("acc_code").matches(
						"[0-9]*.[0-9]*.[0-9]*.[0-9]*.[0-9]*.[0-9]*")) {
					whiteSpaces = "           ";
				}
				if (isChildRoot > 0) {
					tiChild.setText(new String[] { "+",
							whiteSpaces + rsChilds.getString(1),
							whiteSpaces + rsChilds.getString(2),
							whiteSpaces + rsChilds.getString(3),
							rsChilds.getString("acc_type") });
					fillAllChilds(rsChilds.getInt("acc_head_id"), tiChild);
				} else {
					tiChild.setText(new String[] { "",
							whiteSpaces + rsChilds.getString(1),
							whiteSpaces + rsChilds.getString(2),
							whiteSpaces + rsChilds.getString(3),
							rsChilds.getString("acc_type") });

				}
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}// end of class
