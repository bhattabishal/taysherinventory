package com.tia.account.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Panel;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JRootPane;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JRViewer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;



import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.controller.TrialBalanceController;
import com.tia.account.model.DailyMobilizationStatementModel;
import com.tia.common.util.Banner;
import com.tia.common.util.LoginCheck;
import com.tia.common.util.POITest;
import com.tia.common.util.UtilMethods;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import com.tia.plugins.JnepCalendar.calendar.JDateChooser;

/**
 * @author suresh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 */
public class DailyMobilizationStatement extends ViewPart {

	private Shell shell;
	private Composite comContainer;
	private Composite comToDate;
	private JDateChooser txtFromDate;
	private JDateChooser txtToDate;
	private Composite comBSReport;
	private Button cmdPrint;
	private Button cmdView;
	private Composite comChooser;
	private Table tblData;
	private Combo cmbLevel;
	private Composite swtAwtComposite;
	private Button cmdToExcel;


	/*
	 * Instance of viewer composite from SWTJasperViewer to create a composite
	 * suitable for displaying the balance sheet Jasper report.
	 */
	JCalendarFunctions jCalendarFunctions;
	SimpleDateFormat simpleDateFormat;
	DecimalFormat decimalFormat;
	BigDecimal drTotal, crTotal, amtDr, amtCr, drGrandTotal,
	crGrandTotal;

	/**
	 * constructor method
	 */
	public DailyMobilizationStatement() {
		setTitleImage(SWTResourceManager.getImage(
				DailyMobilizationStatement.class, "/logo.png"));
		shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		jCalendarFunctions = new JCalendarFunctions();
		simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		decimalFormat = new DecimalFormat("##,##0.00");
	}

	@Override
	public void createPartControl(final Composite parent) {

		// create a main composite and set it Grid layout
		comContainer = new Composite(parent, SWT.NONE);
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comContainer.setLayout(gl_comOuterMain);

		// display the banner first in the empty shell(view)
		Banner.getTitle("DailyMobizationStatement",
				comContainer, DailyMobilizationStatement.class);

		// composite holding the choose options
		comChooser = new Composite(comContainer, SWT.NONE);
		GridData gd_comChooser = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_comChooser.heightHint = 609;
		gd_comChooser.widthHint = 963;
		comChooser.setLayoutData(gd_comChooser);
		comChooser.setBounds(10, 33, 174, 676);

		Group grpDate = new Group(comChooser, SWT.NONE);
		grpDate.setBounds(14, 10, 943, 54);

		GridData gd_dobComp = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_dobComp.heightHint = 20;
		gd_dobComp.widthHint = 188;

		// label for To Date
		Label lblFrom = new Label(grpDate, SWT.NONE);
		lblFrom.setAlignment(SWT.RIGHT);
		lblFrom.setBounds(218, 21, 48, 18);
		lblFrom.setText("To");
		

		// composite for To date
		Composite comFromDatePicker = new Composite(grpDate, SWT.NONE);
		comFromDatePicker.setBounds(74, 19, 134, 22);

		/*
		 * NON-SWT DATE PICKER for toDate
		 * 
		 * Create and setting up frame.
		 */
		Composite comFromDate = new Composite(comFromDatePicker,
				SWT.NO_BACKGROUND | SWT.EMBEDDED);
		comFromDate.setToolTipText("Swing Date Picker");
		comFromDate.setBounds(1, 1, 130, 20);

		GridData gd_fromDateComp = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_fromDateComp.heightHint = 20;
		gd_fromDateComp.widthHint = 188;
		comFromDate.setLayoutData(gd_fromDateComp);

		Frame fromFrame = SWT_AWT.new_Frame(comFromDate);
		Panel fromPanel = new Panel(new BorderLayout()) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void update(java.awt.Graphics g) {

				paint(g);
			}
		};
		fromFrame.add(fromPanel);
		JRootPane fromRoot = new JRootPane();
		fromPanel.add(fromRoot);
		Container fromContentPane = fromRoot.getContentPane();

		txtFromDate = new JDateChooser();
		fromContentPane.setLayout(new BorderLayout());
		fromContentPane.add(txtFromDate, BorderLayout.CENTER);

		// display the from date pointing begining date of this quarter
		txtFromDate.setDate((ApplicationWorkbenchWindowAdvisor
				.getCurClosingPeriodStartMonthEnDt()));

		// composite for To date
		Composite comToDatePicker = new Composite(grpDate, SWT.NONE);
		comToDatePicker.setBounds(276, 19, 134, 22);

		/*
		 * NON-SWT DATE PICKER for toDate
		 * 
		 * Create and setting up frame.
		 */
		comToDate = new Composite(comToDatePicker, SWT.NO_BACKGROUND
				| SWT.EMBEDDED);
		comToDate.setToolTipText("Swing Date Picker");
		comToDate.setBounds(1, 1, 130, 20);

		GridData gd_toDateComp = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_toDateComp.heightHint = 20;
		gd_toDateComp.widthHint = 188;
		comToDate.setLayoutData(gd_toDateComp);

		Frame toFrame = SWT_AWT.new_Frame(comToDate);
		Panel toPanel = new Panel(new BorderLayout()) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void update(java.awt.Graphics g) {

				paint(g);
			}
		};
		toFrame.add(toPanel);
		JRootPane toRoot = new JRootPane();
		toPanel.add(toRoot);
		Container toContentPane = toRoot.getContentPane();

		txtToDate = new JDateChooser();
		toContentPane.setLayout(new BorderLayout());
		toContentPane.add(txtToDate, BorderLayout.CENTER);

		// display the date
		txtToDate.setDate(java.sql.Date
				.valueOf(JCalendarFunctions.currentDate()));

		cmbLevel = new Combo(grpDate, SWT.READ_ONLY);
		cmbLevel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// set the print button disabled whenever the selection
				// has been changed in hierarchy combo in GUI
				cmdPrint.setEnabled(false);
				cmdToExcel.setEnabled(false);
			}
		});
		cmbLevel.setItems(new String[] {"Select",
				"Summary", "Details" });
		cmbLevel.setBounds(537, 16, 134, 29);
		
		cmbLevel.select(0);
		// View button
		cmdView = new Button(grpDate, SWT.NONE);
		cmdView.setBounds(681, 18, 75, 25);

		// widget selection code for the View button
		cmdView.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (!LoginCheck.isLogIn()) {
					LoginCheck.showMessage();
					return;
				}
				//disable during viewing
				cmdPrint.setEnabled(false);
				cmdToExcel.setEnabled(false);
				if (!tblData.isDisposed()) {
					tblData.removeAll();
				}
				cmdViewActionPerformed();
				// enable the print button now
				cmdPrint.setEnabled(true);
				cmdToExcel.setEnabled(true);
			}
		});
		cmdView.setText("View");
		

		// Print button
		cmdPrint = new Button(grpDate, SWT.NONE);
		cmdPrint.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// disable print button
				cmdPrint.setEnabled(false);
				cmdToExcel.setEnabled(false);
				/*
				 * perform printing report iff the grid has at least one items
				 * i.e. validation block We hav chosen item count greater than 5
				 * because five rows are our basic rows in GUI table
				 */
				if (tblData.getItemCount() <= 5) {
					MessageBox mb = new MessageBox(shell, SWT.ICON_ERROR);
					mb.setMessage("NoData");
					mb.setText("Error");
					mb.open();
				}
				// if(tblData.getItemCount()>5)
				else {
					/*
					 * if the GUI Grid has data in it, load all data into the
					 * list to be used as data-source for iReport later.
					 */
					java.util.ArrayList<DailyMobilizationStatementModel> listDataSource = new ArrayList<DailyMobilizationStatementModel>();
					for (int rows = 0; rows < tblData.getItemCount(); rows++) {
						DailyMobilizationStatementModel tmpDMSM = new DailyMobilizationStatementModel();
						tmpDMSM.setAlias(tblData.getItem(rows).getText(0));
						tmpDMSM.setParticulars(tblData.getItem(rows).getText(1));
						tmpDMSM.setDrAmt(tblData.getItem(rows).getText(2));
						tmpDMSM.setCrAmt(tblData.getItem(rows).getText(3));

						/*
						 * add the model object into collection list. Each row
						 * get loaded into list.
						 */
						listDataSource.add(tmpDMSM);
					}
					try {
						final JasperPrint jprint = generateReport(listDataSource);

						// Loading the JasperPrint object in the viewerComposite
						if (jprint.getPages().isEmpty()) {
							System.out.println("Report is null");

							MessageBox mb = new MessageBox(shell, SWT.ERROR);
							mb.setText("Error");
							mb.setMessage("No Data To Print");
							mb.open();
							if (tblData.isDisposed()) {
								swtAwtComposite.dispose();
								createViewerComposite();
							}
						} else {
							System.out.println("Report is not null");
							org.eclipse.swt.widgets.Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									if (tblData.isDisposed()) {
										swtAwtComposite.dispose();
									}
									// dispose off the table now
									tblData.dispose();
									// Defining an instance of ViewerComposite into the
									// view

									createViewerComposite();
									JRViewer jasperviewer = new JRViewer(jprint);
									// viewerComposite.setVisible(false);

									Frame frame = SWT_AWT.new_Frame(swtAwtComposite);

									Panel panel = new Panel();
									frame.add(panel);
									panel.setLayout(new BorderLayout(0, 0));

									JRootPane rootPane = new JRootPane();
									rootPane.setSize(767, 600);
									panel.add(rootPane);
									Container c = rootPane.getContentPane();
									c.add(jasperviewer);
								}
							});
						}

					} catch (Exception e2) {
						e2.printStackTrace();
					}

				}// validation if() ends
			}
		});
		cmdPrint.setBounds(766, 18, 75, 25);
		cmdPrint.setEnabled(false);

		// widget selected code for the Print button

		cmdPrint.setText("Print");
		

		Label lblFromDate = new Label(grpDate, SWT.NONE);
		lblFromDate.setAlignment(SWT.RIGHT);
		lblFromDate.setFont(SWTResourceManager.getFont("Arial", 12, SWT.NORMAL));
		lblFromDate.setBounds(10, 21, 54, 18);
		lblFromDate.setText("From");

		cmdToExcel = new Button(grpDate, SWT.NONE);
		cmdToExcel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				cmdPrint.setEnabled(false);
				cmdToExcel.setEnabled(false);
				// validation; because five rows in GUI are minimal output
				// labels
				if (tblData.getItemCount() <= 5) {
					MessageBox mb = new MessageBox(shell, SWT.ICON_ERROR);
					mb.setMessage("NoData");
					mb.setText("Error");
					mb.open();
				} else {
					String fromDt = simpleDateFormat.format(txtFromDate.getDate());
					String toDt = simpleDateFormat.format(txtToDate.getDate());
					String fromDtNp = jCalendarFunctions.GetNepaliDate(fromDt);
					String toDtNp = jCalendarFunctions.GetNepaliDate(toDt);

					String fileName = "DailyMobilizationStatementReport_"
						+ jCalendarFunctions.getCurrentNepaliDate();
					String reportTitle = "Daily Mobilization Statement Report from "
						+ fromDt + "to " + toDt;
					
					String subTitle = cmbLevel.getSelectionIndex() == 1 ? ("(Brief)")
									: ("(Details)");

					if (POITest.exportToExcel(tblData, fileName, reportTitle,
							subTitle, 1, 2)) {
						MessageBox mb = new MessageBox(shell,
								SWT.ICON_INFORMATION);
						mb.setMessage("ExportDone");
						mb.setText("Success");
						mb.open();
					}
				}
			}
		});
		cmdToExcel.setText("ToExcel");
		cmdToExcel.setFont(SWTResourceManager.getFont("Arial", 12, SWT.NORMAL));
		cmdToExcel.setEnabled(false);
		cmdToExcel.setBounds(851, 18, 75, 25);

		Label lblLevel = new Label(grpDate, SWT.NONE);
		lblLevel.setAlignment(SWT.RIGHT);
		lblLevel.setText("Level");
		lblLevel.setFont(SWTResourceManager.getFont("Arial", 11, SWT.NORMAL));
		lblLevel.setBounds(420, 21, 107, 18);
		lblLevel.setForeground(UtilMethods.getFontForegroundColorRed());

		grpDate.setTabList(new Control[] { cmbLevel, cmdView, cmdPrint,
				cmdToExcel });

		// composite for holding bs data and report
		comBSReport = new Composite(comChooser, SWT.NONE);
		comBSReport.setBounds(14, 82, 943, 530);

		// create the viewer composite but keep it hidden
		createViewerComposite();
		swtAwtComposite.setVisible(false);

		// create report table
		createReportTable();

	}// end of createPartControl() method

	/**
	 * cmdViewActionPerformed() to perform the action i.e. to display the ledger
	 * on table.
	 * 
	 * @param null
	 * @return void
	 */
	private void cmdViewActionPerformed() {
		if (!beforeViewing()) {
			return;
		} else {
			if (tblData.isDisposed()) {
				swtAwtComposite.dispose();
				createReportTable();
			} else {
				tblData.dispose();
				createReportTable();
			}
			fillTable();
		}

	}

	/**
	 * This method will create a an instance of the ViewerComposite such that
	 * the Jasper report can be displayed in the SWT composite on display.
	 * 
	 * @param null
	 * @return void
	 */
	private void createViewerComposite() {
		// Defining an instance of ViewerComposite into the view
		swtAwtComposite = new Composite(comBSReport, SWT.EMBEDDED);
		swtAwtComposite.setBounds(0, 0, 943, 490);
	}

	/**
	 * Method to create the report grid.
	 * 
	 * @param null
	 * @return void
	 */
	private void createReportTable() {

		tblData = new Table(comBSReport, SWT.BORDER | SWT.FULL_SELECTION
				| SWT.CENTER | SWT.V_SCROLL | SWT.H_SCROLL);
		tblData.setBounds(0, 0, 943, 490);
		tblData.setHeaderVisible(true);
		tblData.setLinesVisible(true);
		
		// creating the columns for the Grid
		String[] colNames = {"AccountAlias",
				"Particulars", "Debit",
				"Credit" };

		TableColumn[] columns = new TableColumn[colNames.length];

		int[][] intAlign = { { 0, 150 }, { 0, 400 }, { 1, 150 }, { 1, 150 } };

		for (int i = 0; i < colNames.length; i++) {
			if (intAlign[i][0] == 0)
				columns[i] = new TableColumn(tblData, SWT.NONE);
			else
				columns[i] = new TableColumn(tblData, SWT.RIGHT);
			columns[i].setResizable(true);
			columns[i].setWidth(intAlign[i][1]);
			columns[i].setText(colNames[i]);
			// columns[i].setFont(CommonFunctions.getFont());
		}
	}// end of method createReportTable()

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {

	}

	/**
	 * This is the method that fills GUI table with the transactions that have
	 * been performed on Cash account head on selected transaction date.
	 */
	private void fillTable() {
		drTotal = new BigDecimal(0);
		crTotal = new BigDecimal(0);
		TrialBalanceController tbController = new TrialBalanceController();
		String to = "", fromDate = "";
		ResultSet rsAssets = null, rsLiabilities = null;
		boolean closingPerformed = false;

		int fiscalYear = ApplicationWorkbenchWindowAdvisor.getFiscalId();

		/*
		 * To date is always the user selected date in the trial balance.
		 */
		fromDate = simpleDateFormat.format(txtFromDate.getDate());
		to = simpleDateFormat.format(txtToDate.getDate());
		try {

			/*
			 * TB loading query depends on two things. If the user selected to
			 * date is the current quarter's closing date and the closing for
			 * the current quarter has already been performed, we omit the
			 * income and expenditure heads and display only the profit/loss
			 * amount. Else we display the income and expenditure account heads
			 * too.
			 */
			if (to.equals(ApplicationWorkbenchWindowAdvisor
					.getCurClosingPeriodEndMonthEnDt().toString())
					&& tbController.isClosingPerformed(to)) {

				closingPerformed = true;
			} else
				closingPerformed = false;

			/**
			 * rs = tbController.loadTrialBalance(to,
			 * fiscalYear,closingPerformed); This rs originally by Minesh, was
			 * modified into following rs with method having extra one @param to
			 * address the Hierarchy of GUI rendering for the account head only
			 * GUI and detailed GUI. ---by suresh on 20DEC2011 TUE
			 */
			//initialize grandTotals
			drGrandTotal=new BigDecimal(0);
			crGrandTotal=new BigDecimal(0);
			//load the resultsets
			rsLiabilities = tbController.loadTrialBalanceDMSLiab(fromDate, to,
					fiscalYear, closingPerformed,
					String.valueOf(cmbLevel.getSelectionIndex()));
			addLabelRow("Liabilities Magnus Nepali");
			addDataRows(rsLiabilities);
			//addTotalRow(Messages.getLabel("reportTotal"), drTotal, crTotal);
			rsAssets = tbController.loadTrialBalanceDMSAssets(fromDate, to,
					fiscalYear, closingPerformed,
					String.valueOf(cmbLevel.getSelectionIndex()));
			addLabelRow("Assets Magnus Nepali");
			/*
			 * Hold the current total dr and cr of Liabilities into grand total
			 * dr and cr variables
			 */
			/*drGrandTotal = drTotal;
			crGrandTotal = crTotal;*/
			/*drTotal = new BigDecimal(0);
			crTotal = new BigDecimal(0);*/
			addDataRows(rsAssets);
			//addTotalRow(Messages.getLabel("reportTotal"), drTotal, crTotal);
			/*
			 * Update the grandTotal dr and cr variable's values with the total
			 * dr and cr values of assets now
			 */
			/*

			drGrandTotal = drGrandTotal.add(drTotal);
			crGrandTotal = crGrandTotal.add(crTotal);*/
			// and finally display the calculated Grand total dr and cr too.
			//present grand total
			addTotalRow("grandTotalReport", drGrandTotal,
					crGrandTotal);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * addTotalRow() to add the total row to the table model. Both sides total
	 * are calculated and forwarded to be added to the table model.
	 * 
	 * @param drTotal
	 *            (Debit total amount.)
	 * @param crTotal
	 *            (Credit total amount.)
	 * @param rowCnt
	 *            (Total rows in the table.)
	 * @return void
	 */
	private void addTotalRow(final String totalLabel, final BigDecimal drTotal,
			final BigDecimal crTotal) {

		new org.eclipse.swt.widgets.Shell().getDisplay().syncExec(new Runnable(){
			@Override
			public void run()
			{
				TableItem item = new TableItem(tblData, SWT.CENTER);

				item.setFont(UtilMethods.getFontBold());
				item.setText(1, "          " + totalLabel);
				// item.setFont(font);
				item.setText(2, decimalFormat.format(drTotal));
				item.setText(3, decimalFormat.format(crTotal));
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

	}// end of addTotalRow()

	/**
	 * Adds the title rows.
	 * 
	 * @param text
	 *            (String to be displayed in the table.)
	 * @return void
	 */
	private void addLabelRow(final String text) {
		new org.eclipse.swt.widgets.Shell().getDisplay().syncExec(new Runnable(){
			@Override
			public void run()
			{
				TableItem item = new TableItem(tblData, SWT.CENTER);
				item.setText(1, text);
				// item.setFont(font1);
				item.setFont(UtilMethods.getFontBold());
				/*try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
			}
		});
	}

	/**
	 * @param rs 
	 * 
	 */
	private void addDataRows(final java.sql.ResultSet rs) {

		try {
			new org.eclipse.swt.widgets.Shell().getDisplay().syncExec(new Runnable(){
				@Override
				public void run()
				{
					//flush global vars
					drTotal=new BigDecimal(0);
					crTotal=new BigDecimal(0);
					try{
						while (rs.next()) {

							TableItem item = new TableItem(tblData, SWT.NONE);
							
							if (rs.getString("acc_code").matches("[0-9]*")) {
								item.setText(0, String.valueOf(rs.getString("alias")));
								item.setFont(UtilMethods.getFontBold());
								item.setText(1, String.valueOf(rs.getString("acc_name")));

							} else if (rs.getString("acc_code").matches("[0-9]*.[0-9]*")) {
								item.setText(0, "  " + rs.getString("alias"));
								item.setText(1, "  " + rs.getString("acc_name"));
							} else if (rs.getString("acc_code").matches(
							"[0-9]*.[0-9]*.[0-9]*")) {
								item.setText(0, "    " + rs.getString("alias"));
								item.setText(1, "    " + rs.getString("acc_name"));
							} else if (rs.getString("acc_code").matches(
							"[0-9]*.[0-9]*.[0-9]*.[0-9]*")) {
								item.setText(0, "      " + rs.getString("alias"));
								item.setText(1, "      " + rs.getString("acc_name"));
							} else if (rs.getString("acc_code").matches(
							"[0-9]*.[0-9]*.[0-9]*.[0-9]*.[0-9]*")) {
								item.setText(0, "        " + rs.getString("alias"));
								item.setText(1, "        " + rs.getString("acc_name"));
							} else {
								item.setText(0, "          " + rs.getString("alias"));
								item.setText(1, "          " + rs.getString("acc_name"));
							}

							// determining the dr. and cr. side for the alias
							// amtDr = (accType.equals("1") || accType.equals("2"))?
							// BigDecimal.valueOf(rs.getDouble("drBalance")):BigDecimal.valueOf(rs.getDouble("crBalance"));
							amtDr = BigDecimal.valueOf(rs.getDouble("drBalance"));
							amtCr = BigDecimal.valueOf(rs.getDouble("crBalance"));

							item.setText(2, String.valueOf(decimalFormat.format(Math.abs(rs
									.getDouble("drBalance")))));
							item.setText(3, String.valueOf(decimalFormat.format(Math.abs(Double
									.valueOf(amtCr.toString())))));
							if (rs.getString("acc_code").matches("[0-9]*")) {
								drTotal = amtDr.add(drTotal);
								crTotal = amtCr.add(crTotal);
							}

							tblData.update();
							Thread.sleep(100);
						}
					}
					catch(Exception exx)
					{
						exx.printStackTrace();
					}
				}


			});
			addTotalRow("reportTotal", drTotal, crTotal);

			//update the grand totals
			drGrandTotal = drGrandTotal.add(drTotal);
			crGrandTotal = crGrandTotal.add(crTotal);
		}// try ends
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}// end of addDataRows()

	/**
	 * beforeViewing() to verify the necessary mandatory fields before
	 * displaying the report on table.
	 * 
	 * @param null
	 * @return boolean (true if success, false otherwise.)
	 */
	private boolean beforeViewing() {
		if ((txtFromDate.getDate() == null) || (txtToDate.getDate() == null)) {
			MessageBox mb = new MessageBox(shell, SWT.ERROR);
			mb.setText("Invalid Selection");
			mb.setMessage("Please Select the Single Option.");
			mb.open();
			return false;
		}
		if (cmbLevel.getSelectionIndex() <= 0) {
			MessageBox mb = new MessageBox(shell, SWT.ERROR);
			mb.setText("Error");
			mb.setMessage("Please Select avalid item from Level combo");
			mb.open();
			return false;
		}
		return true;
	}// end of beforeViewing()

	/**
	 * @param listDataSource
	 * @return jasperprint
	 * 
	 *         This method generates JasperReport which is exact replica of the
	 *         data being presented in GUI data grid.
	 * 
	 *         In this method, a new technique of specifying data-source for
	 *         iReport has been used i.e. JavaBean collection has been used as
	 *         data-source for the report being presented in JRViewer. This is
	 *         why we have not used sql connection to populate the report data.
	 */
	public JasperPrint generateReport(
			java.util.ArrayList<DailyMobilizationStatementModel> listDataSource) {
		JasperPrint myJPrint = null;

		try {

			JasperDesign jasperDesign = null;
			JasperReport jasperReport = null;

			String fromDate = simpleDateFormat.format(txtFromDate.getDate());
			String toDate = simpleDateFormat.format(txtToDate.getDate());

			String report_title="Daily Mobilization Statement";
			String subTitle="("+fromDate + " To "+toDate+(cmbLevel.getSelectionIndex()==1?" : Brief":"Details )");
			

			jasperDesign = JRXmlLoader.load(DailyMobilizationStatement.class
					.getClassLoader().getResourceAsStream(
					"DailyMobilizationStatementReport.jrxml"));
			jasperReport = JasperCompileManager.compileReport(jasperDesign);
			// Passing parameters to the report
			Map<String, Object> params = new HashMap<String, Object>();


			params.put("institutionName",
					ApplicationWorkbenchWindowAdvisor.getOfficeName());
			params.put("address",
					ApplicationWorkbenchWindowAdvisor.getOfficeAddress());
			params.put("report_title", report_title	);
			params.put("subTitle",subTitle );
			params.put("logo", UtilMethods.getLogoImage());

			/*
			 * Filling the report with data from the database based on the
			 * parameters passed.
			 */
			myJPrint = JasperFillManager
			.fillReport(
					jasperReport,
					params,
					new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource(
							listDataSource));
			params.clear();

		} catch (JRException ex) {
			ex.printStackTrace();
		}

		return myJPrint;

	}// end of method generateReport()
}// end of class

