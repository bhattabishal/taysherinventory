/**
 * 
 */
package com.tia.account.model;

/**
 * @author suresh
 * @supervisor loojah bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 */
public class DailyMobilizationStatementModel {
	//Member fields
	private String alias;
	private String particulars;
	private String drAmt;
	private String crAmt;
	/**
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}
	/**
	 * @param alias the alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}
	/**
	 * @return the particulars
	 */
	public String getParticulars() {
		return particulars;
	}
	/**
	 * @param particulars the particulars to set
	 */
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}
	/**
	 * @return the drAmt
	 */
	public String getDrAmt() {
		return drAmt;
	}
	/**
	 * @param drAmt the drAmt to set
	 */
	public void setDrAmt(String drAmt) {
		this.drAmt = drAmt;
	}
	/**
	 * @return the crAmt
	 */
	public String getCrAmt() {
		return crAmt;
	}
	/**
	 * @param crAmt the crAmt to set
	 */
	public void setCrAmt(String crAmt) {
		this.crAmt = crAmt;
	}
	
}//end of model class
