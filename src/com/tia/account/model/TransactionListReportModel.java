/**
 * 
 */
package com.tia.account.model;

/**
 * @author suresh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 */
public class TransactionListReportModel {
	private String jv_date;
	private String jv_no;
	private String name;
	private String particulars;
	private String dr_amt;
	private String cr_amt;
	private String remarks;
	private String user_name;

	private String status;

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the jv_date
	 */
	public String getJv_date() {
		return jv_date;
	}

	/**
	 * @param jv_date
	 *            the jv_date to set
	 */
	public void setJv_date(String jv_date) {
		this.jv_date = jv_date;
	}

	/**
	 * @return the jv_no
	 */
	public String getJv_no() {
		return jv_no;
	}

	/**
	 * @param jv_no
	 *            the jv_no to set
	 */
	public void setJv_no(String jv_no) {
		this.jv_no = jv_no;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the particulars
	 */
	public String getParticulars() {
		return particulars;
	}

	/**
	 * @param particulars
	 *            the particulars to set
	 */
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}

	/**
	 * @return the dr_amt
	 */
	public String getDr_amt() {
		return dr_amt;
	}

	/**
	 * @param dr_amt
	 *            the dr_amt to set
	 */
	public void setDr_amt(String dr_amt) {
		this.dr_amt = dr_amt;
	}

	/**
	 * @return the cr_amt
	 */
	public String getCr_amt() {
		return cr_amt;
	}

	/**
	 * @param cr_amt
	 *            the cr_amt to set
	 */
	public void setCr_amt(String cr_amt) {
		this.cr_amt = cr_amt;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the user_name
	 */
	public String getUser_name() {
		return user_name;
	}

	/**
	 * @param user_name
	 *            the user_name to set
	 */
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

}
