package com.tia.account.model;

/**
 * @author suresh
 * @supervisor Loojah Bajracharya
 * @copyright Magnus Consulting Group Pvt. Ltd.
 */
public class Temp_profit_loss {

	private String account_alias;
	private String particulars;
	private String tot_sub_head;
	private String tot_head;
	private int br_id;
	private int rpt_order;

	public String getAccount_alias() {
		return account_alias;
	}
	public void setAccount_alias(String account_alias) {
		this.account_alias = account_alias;
	}
	public String getParticulars() {
		return particulars;
	}
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}
	public String getTot_sub_head() {
		return tot_sub_head;
	}
	public void setTot_sub_head(String tot_sub_head) {
		this.tot_sub_head = tot_sub_head;
	}
	public String getTot_head() {
		return tot_head;
	}
	public void setTot_head(String tot_head) {
		this.tot_head = tot_head;
	}
	public int getBr_id() {
		return br_id;
	}
	public void setBr_id(int br_id) {
		this.br_id = br_id;
	}
	public int getRpt_order() {
		return rpt_order;
	}
	public void setRpt_order(int rpt_order) {
		this.rpt_order = rpt_order;
	}

}
