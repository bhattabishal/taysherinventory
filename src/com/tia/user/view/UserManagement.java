package com.tia.user.view;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.GridLayout;

import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.CmbQry;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.common.util.Validator;
import com.tia.user.controller.UserManagementController;
import com.tia.user.controller.encryptDectyptPassword;
import com.tia.user.model.UserManagementModel;

public class UserManagement extends ViewPart {
	private Text txtFullName;
	private Text txtPhoneNo;
	private Text txtCellno;
	private Text txtAdress;
	private Text txtLoginName;
	private Text txtPassword;
	private Text txtRepassword;
	private Table tblDetails;
	private Text txtEmail;
	private Button cmdNew;
	private Button cmdSave;
	private Button cmdCancel;
	private Button cmdEdit;
	private Button cmdDelete;
	private SwtBndCombo cmbRole;
	private Composite comParent;
	Shell sh = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	UserManagementModel objUserManagemetnModel = new UserManagementModel();
	static Connection cnn = DbConnection.getConnection();
	Validator validator = new Validator();
	UserManagementController objUserCntrl = new UserManagementController();
	private Text txtUserId;
	private Button btnIsAdmin;

	public UserManagement() {
		setTitleImage(SWTResourceManager.getImage(UserManagement.class, "/user16.png"));

	}

	@Override
	public void createPartControl(Composite parent) {

		comParent = new Composite(parent, SWT.NONE);
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("User Management Form", comParent, UserManagement.class);

		Composite cmpMain = new Composite(comParent, SWT.NONE);
		Group grpPersonalInfo = new Group(cmpMain, SWT.NONE);
		grpPersonalInfo.setText("Personal Info");
		grpPersonalInfo.setBounds(10, 10, 972, 95);

		Label lblFullName = new Label(grpPersonalInfo, SWT.NONE);
		lblFullName.setAlignment(SWT.RIGHT);
		lblFullName.setBounds(10, 34, 85, 15);
		lblFullName.setText("Full Name :");
		lblFullName.setForeground(UtilMethods.getFontForegroundColorRed());

		txtFullName = new Text(grpPersonalInfo, SWT.BORDER);
		txtFullName.setBounds(101, 28, 199, 21);
		txtFullName.setEnabled(false);

		Label lblPhoneNo = new Label(grpPersonalInfo, SWT.NONE);
		lblPhoneNo.setText("Phone No :");
		lblPhoneNo.setAlignment(SWT.RIGHT);
		lblPhoneNo.setBounds(343, 31, 85, 15);

		txtPhoneNo = new Text(grpPersonalInfo, SWT.BORDER);
		txtPhoneNo.addListener(SWT.Verify, new Listener() {
			public void handleEvent(Event e) {
				String string = e.text;
				char[] chars = new char[string.length()];
				string.getChars(0, chars.length, chars, 0);
				for (int i = 0; i < chars.length; i++) {
					if (!('0' <= chars[i] && chars[i] <= '9')
							&& (chars[i] != '+') && (chars[i] != '-')) {
						// This added condition "&& (chars[i] != ',')" is to
						// allow verify the character , too
						// If it is not added the value to be displayed in the
						// txtCurInt textField will only permit those
						// values <=999; after that the values will be separated
						// by commas and this Listener will block them from
						// being displayed.
						e.doit = false;
						return;
					}
				}
			}
		});
		txtPhoneNo.setBounds(434, 28, 199, 21);
		txtPhoneNo.setEnabled(false);

		Label lblCellNo = new Label(grpPersonalInfo, SWT.NONE);
		lblCellNo.setText("Cell No :");
		lblCellNo.setAlignment(SWT.RIGHT);
		lblCellNo.setBounds(343, 62, 85, 15);

		txtCellno = new Text(grpPersonalInfo, SWT.BORDER);
		txtCellno.addListener(SWT.Verify, new Listener() {
			public void handleEvent(Event e) {
				String string = e.text;
				char[] chars = new char[string.length()];
				string.getChars(0, chars.length, chars, 0);
				for (int i = 0; i < chars.length; i++) {
					if (!('0' <= chars[i] && chars[i] <= '9')
							&& (chars[i] != '+') && (chars[i] != '-')) {
						// This added condition "&& (chars[i] != ',')" is to
						// allow verify the character , too
						// If it is not added the value to be displayed in the
						// txtCurInt textField will only permit those
						// values <=999; after that the values will be separated
						// by commas and this Listener will block them from
						// being displayed.
						e.doit = false;
						return;
					}
				}
			}
		});
		txtCellno.setBounds(434, 56, 199, 21);
		txtCellno.setEnabled(false);

		Label lblAdress = new Label(grpPersonalInfo, SWT.NONE);
		lblAdress.setText("Adress :");
		lblAdress.setAlignment(SWT.RIGHT);
		lblAdress.setBounds(651, 34, 85, 15);

		txtAdress = new Text(grpPersonalInfo, SWT.BORDER);
		txtAdress.setBounds(742, 28, 216, 49);
		txtAdress.setEnabled(false);

		Label lblEmail = new Label(grpPersonalInfo, SWT.NONE);
		lblEmail.setText("Email :");
		lblEmail.setAlignment(SWT.RIGHT);
		lblEmail.setBounds(10, 61, 85, 15);

		txtEmail = new Text(grpPersonalInfo, SWT.BORDER);
		txtEmail.setBounds(101, 55, 199, 21);
		txtEmail.setEnabled(false);
		
		grpPersonalInfo.setTabList(new Control[] { txtFullName, txtPhoneNo,
				txtEmail, txtCellno, txtAdress });

		Group grpLoginInformation = new Group(cmpMain, SWT.NONE);
		grpLoginInformation.setText("Login Info");
		grpLoginInformation.setBounds(10, 111, 662, 104);

		Label login = new Label(grpLoginInformation, SWT.NONE);
		login.setText("Login Name :");
		login.setAlignment(SWT.RIGHT);
		login.setBounds(10, 32, 85, 15);
		login.setForeground(UtilMethods.getFontForegroundColorRed());

		txtLoginName = new Text(grpLoginInformation, SWT.BORDER);
		txtLoginName.setBounds(101, 29, 199, 21);
		txtLoginName.setEnabled(false);

		txtPassword = new Text(grpLoginInformation, SWT.BORDER | SWT.PASSWORD);
		txtPassword.setBounds(101, 58, 199, 21);
		txtPassword.setEnabled(false);

		Label lblPassword = new Label(grpLoginInformation, SWT.NONE);
		lblPassword.setText("Password :");
		lblPassword.setAlignment(SWT.RIGHT);
		lblPassword.setBounds(10, 61, 85, 15);
		lblPassword.setForeground(UtilMethods.getFontForegroundColorRed());

		txtRepassword = new Text(grpLoginInformation, SWT.BORDER | SWT.PASSWORD);
		txtRepassword.setBounds(434, 58, 199, 21);
		txtRepassword.setEnabled(false);

		Label lblRetypePassword = new Label(grpLoginInformation, SWT.NONE);
		lblRetypePassword.setText("Re Password :");
		lblRetypePassword.setAlignment(SWT.RIGHT);
		lblRetypePassword.setBounds(327, 61, 101, 15);
		lblRetypePassword.setForeground(UtilMethods.getFontForegroundColorRed());

		Label lblRole = new Label(grpLoginInformation, SWT.NONE);
		lblRole.setText("Role :");
		lblRole.setAlignment(SWT.RIGHT);
		lblRole.setBounds(343, 32, 85, 15);
		lblRole.setForeground(UtilMethods.getFontForegroundColorRed());

		// cmbStatus.add("Active");
		// cmbStatus.add("Inactive");

		cmbRole = new SwtBndCombo(grpLoginInformation, SWT.READ_ONLY);
		//cmbRole.setItems(new String[] { "Select", "Administrator", "Operator", "Guest" });
		cmbRole
		.fillData(CmbQry.getRoleQuery(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection());
		cmbRole.select(0);
		cmbRole.setEnabled(false);
		cmbRole.setBounds(434, 27, 133, 23);

		txtUserId = new Text(grpLoginInformation, SWT.BORDER);
		txtUserId.setBounds(26, 162, 76, 21);
		txtUserId.setVisible(false);
		
		btnIsAdmin = new Button(grpLoginInformation, SWT.CHECK);
		btnIsAdmin.setEnabled(false);
		btnIsAdmin.setBounds(584, 31, 68, 16);
		btnIsAdmin.setText("is admin");
		
		grpLoginInformation.setTabList(new Control[]{txtLoginName, cmbRole, txtPassword,txtRepassword});

		Group grpControls = new Group(cmpMain, SWT.NONE);
		grpControls.setBounds(687, 111, 295, 104);

		cmdNew = new Button(grpControls, SWT.NONE);
		cmdNew.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int status = ApplicationWorkbenchWindowAdvisor.getStatus();
				if (status == 1) {
					SetFieldDisable();
					SetFieldEnabled();
					cmdNew.setEnabled(false);
					cmdDelete.setEnabled(false);
					txtFullName.forceFocus();
				} else {
					MessageBoxInformation("You must login to As a Administratro to Add a new User");
				}
			}
		});
		cmdNew.setBounds(23, 25, 75, 25);
		cmdNew.setText("New");

		cmdEdit = new Button(grpControls, SWT.NONE);
		cmdEdit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String currentUser = ApplicationWorkbenchWindowAdvisor
						.getCurrentUser();
				System.out.println(currentUser);
				int status = ApplicationWorkbenchWindowAdvisor.getStatus();
				System.out.println(status);
				if (status == 1) {
					SetFieldEnabled();
					cmdEdit.setEnabled(false);
					cmdNew.setEnabled(false);
				} else if (txtLoginName.getText().equals(currentUser)) {
					SetFieldEnabled();
					cmbRole.setEnabled(false);
					cmdEdit.setEnabled(false);
					cmdNew.setEnabled(false);
				} else {
					{
						String message = "Your are not login as Administrator, please contact your Administrator";
						MessageBoxInformation(message);
					}

				}
			}
		});
		cmdEdit.setEnabled(false);
		cmdEdit.setText("Edit");
		cmdEdit.setBounds(110, 25, 75, 25);

		cmdDelete = new Button(grpControls, SWT.NONE);
		cmdDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				MessageBox quesBox = new MessageBox(sh, SWT.YES | SWT.NO);
				quesBox.setText("Confirm Delete");
				quesBox.setMessage("Are u sure u want to Delete?");

				if (quesBox.open() == SWT.YES) {

					int userId = Integer.parseInt(txtUserId.getText());
					if (objUserCntrl.DeleteUser(userId))
					{
						String Message = "Sucessfully Deleted";
						MessageBoxInformation(Message);
						tblDetails.removeAll();
						FillTableDetails();
						SetFieldDisable();
						cmdNew.setEnabled(true);
					}
				}
			}
		});
		cmdDelete.setEnabled(false);
		cmdDelete.setText("Delete");
		cmdDelete.setBounds(194, 25, 75, 25);

		cmdCancel = new Button(grpControls, SWT.NONE);
		cmdCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				SetFieldDisable();
				cmdNew.setEnabled(true);
			}
		});
		cmdCancel.setText("Cancel");
		cmdCancel.setBounds(155, 56, 75, 25);

		cmdSave = new Button(grpControls, SWT.NONE);
		cmdSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (checkSave()) {
					int role;
					MessageBox quesBox = new MessageBox(sh, SWT.YES | SWT.NO);
					quesBox.setMessage("Confirm Save");
					quesBox.setMessage("Are u sure u want to Save?");

					Date dt = new Date();
					SimpleDateFormat sdfSource = new SimpleDateFormat(
							"EEE MMM dd hh:mm:ss z yyyy");
					String todayDate = dt.toString();
					Date currentDt = null;
					try {
						currentDt = sdfSource.parse(todayDate);
					} catch (ParseException e1) {
						e1.printStackTrace();
					}
					SimpleDateFormat sdfDestination = new SimpleDateFormat(
							"yyyy-MM-dd");
					todayDate = sdfDestination.format(currentDt);
					objUserManagemetnModel.setAddress(txtAdress.getText());
					objUserManagemetnModel.setCell(txtCellno.getText());
					objUserManagemetnModel.setEmail(txtEmail.getText());
					objUserManagemetnModel.setLogin_name(txtLoginName.getText());
					objUserManagemetnModel.setUser_name(txtFullName.getText());
					objUserManagemetnModel.setPhone(txtPhoneNo.getText());
					if(btnIsAdmin.getSelection())
					{
						objUserManagemetnModel.setUser_type(1);
					}
					else
					{
						objUserManagemetnModel.setUser_type(0);
					}
					objUserManagemetnModel.setStatus(1);
					objUserManagemetnModel.setRole_id(Integer.valueOf(cmbRole.getSelectedBoundValue().toString()));
					objUserManagemetnModel.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
					objUserManagemetnModel.setUpdt_cnt(1);
					objUserManagemetnModel.setUpdt_dt(todayDate);
					try {
						String strQry = "";
						java.sql.Statement st = cnn.createStatement(
								ResultSet.TYPE_SCROLL_SENSITIVE,
								ResultSet.CONCUR_UPDATABLE);
						if (!txtUserId.getText().equals("")) {
							if (!(txtPassword.getText() == "")) {
								String password1 = encryptDectyptPassword
										.encryptPassword(txtPassword.getText());
								objUserManagemetnModel.setLogin_pwd(password1);
							}
							objUserManagemetnModel.z_WhereClause
									.user_id(Integer.parseInt(txtUserId
											.getText()));
							strQry = objUserManagemetnModel.Update();
						} else {
							String password = encryptDectyptPassword
									.encryptPassword(txtPassword.getText());
							objUserManagemetnModel.setLogin_pwd(password);
							if (!txtPassword.getText().equals(
									txtRepassword.getText())) {
								MessageBox mb = new MessageBox(sh, SWT.ERROR);
								mb.setText("Errors");
								mb.setMessage("your Password Donot match");
								mb.open();
							}
							if (txtPassword.getText().equals("")) {
								MessageBox mb = new MessageBox(sh, SWT.ERROR);
								mb.setText("Errors");
								mb.setMessage("Please Enter your Password");
								mb.open();

								return;
							}
							strQry = objUserManagemetnModel.Insert();
						}
						System.out.println(strQry);
						st.executeUpdate(strQry);
						String message = "sucessfully Saved/Updated";
						MessageBoxInformation(message);
						tblDetails.removeAll();
						FillTableDetails();
						SetFieldDisable();
						cmdNew.setEnabled(true);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			}
		});
		cmdSave.setEnabled(false);
		cmdSave.setBounds(68, 56, 75, 25);
		cmdSave.setText("Save");

		tblDetails = new Table(cmpMain, SWT.BORDER | SWT.FULL_SELECTION);
		tblDetails.setLocation(10, 223);
		tblDetails.setSize(972, 236);
		tblDetails.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				cmdSave.setEnabled(false);
				SetFieldDisable();
				txtFullName.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(2));
				txtPhoneNo.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(7));
				txtCellno.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(8));
				txtAdress.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(3));
				txtEmail.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(4));
				txtLoginName.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(1));
				txtUserId.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(6));
				// String password = UtilFxn.GetValueFromTable("ntb_user",
				// "login_pwd",
				// "user_id = '"+Integer.parseInt(txtUserId.getText())+"'");
				txtPassword.setText("");
				txtRepassword.setText("");
				cmbRole.setSelectedBoundValue(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(9));
				if(Integer.valueOf(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(10)) > 0)
				{
					btnIsAdmin.setSelection(true);
				}
				else
				{
					btnIsAdmin.setSelection(false);
				}
				cmdEdit.setEnabled(true);
			}
		});
		tblDetails.setHeaderVisible(true);
		tblDetails.setLinesVisible(true);

		TableColumn tblclmnUserId = new TableColumn(tblDetails, SWT.NONE);
		tblclmnUserId.setWidth(51);
		tblclmnUserId.setText("S. No.");

		TableColumn tblclmnLoginName = new TableColumn(tblDetails, SWT.NONE);
		tblclmnLoginName.setWidth(146);
		tblclmnLoginName.setText("Login Name");

		TableColumn tblclmnFullName = new TableColumn(tblDetails, SWT.NONE);
		tblclmnFullName.setWidth(131);
		tblclmnFullName.setText("Full Name");

		TableColumn tblclmnAdress = new TableColumn(tblDetails, SWT.NONE);
		tblclmnAdress.setWidth(139);
		tblclmnAdress.setText("Adress");

		TableColumn tblclmnEmail = new TableColumn(tblDetails, SWT.NONE);
		tblclmnEmail.setWidth(126);
		tblclmnEmail.setText("Email");

		TableColumn tblclmnRole = new TableColumn(tblDetails, SWT.NONE);
		tblclmnRole.setWidth(147);
		tblclmnRole.setText("Role");

		TableColumn tableColumn = new TableColumn(tblDetails, SWT.NONE);
		tableColumn.setWidth(0);
		tableColumn.setText("UserId");
		tableColumn.setResizable(false);

		TableColumn tblclmnPhoneNo = new TableColumn(tblDetails, SWT.NONE);
		tblclmnPhoneNo.setWidth(87);
		tblclmnPhoneNo.setText("Phone No.");

		TableColumn tblclmnCellNo = new TableColumn(tblDetails, SWT.NONE);
		tblclmnCellNo.setWidth(100);
		tblclmnCellNo.setText("Cell No.");
		
		TableColumn tblclmnRoleId = new TableColumn(tblDetails, SWT.NONE);
		tblclmnRoleId.setWidth(0);
		tblclmnRoleId.setText("roll id");
		tblclmnRoleId.setResizable(false);
		
		TableColumn tblclmnType = new TableColumn(tblDetails, SWT.NONE);
		tblclmnType.setWidth(0);
		tblclmnType.setText("user type");
		tblclmnType.setResizable(false);
		// TODO Auto-generated method stub
		FillTableDetails();

	}

	protected void MessageBoxInformation(String message) {
		MessageBox mbInfo = new MessageBox(sh, SWT.ICON_INFORMATION);
		mbInfo.setText("Sucess");
		mbInfo.setMessage(message);
		mbInfo.open();

	}

	protected boolean checkSave() {
		@SuppressWarnings("unused")
		boolean check = true;

		String[] errors;
		errors = new String[22];
		int cnt = 0;

		if (txtFullName.getText().equals("")) {
			check = false;
			errors[cnt] = "Please Enter Your Full Name";
			++cnt;
		}

		TableItem[] Item = tblDetails.getItems();

		for (int i = 0; i < tblDetails.getItemCount(); i++) {
			System.out.println("dfdf" + tblDetails.getSelectionIndex());
			// this one is for making him edit only his current field
			if (i == tblDetails.getSelectionIndex()) {

			} else {
				if (txtLoginName.getText().equals(Item[i].getText(1))) {
					System.out.println(Item[i].getText(1));

					check = false;
					errors[cnt] = "The following user Name is already exist please use another user name";
					++cnt;
				}
			}
		}

		if (txtLoginName.getText().equals("")) {

			check = false;
			errors[cnt] = "Please Enter Your login Name.";
			txtLoginName.setText("");
			++cnt;
		}

		if (!txtPassword.getText().equals(txtRepassword.getText())) {
			check = false;
			errors[cnt] = "Your passwword Donot match Re-enter Your Password";
			txtPassword.setText("");
			txtRepassword.setText("");
			++cnt;
		}

		if (cmbRole.getSelectionIndex() <= 0) {
			check = false;
			errors[cnt] = "Select the Role.";
			cmbRole.select(0);
			++cnt;
		}
		if (!txtEmail.getText().equals("")) {
			if (!validator.validateEmail(txtEmail.getText())) {
				check = false;
				errors[cnt] = "Enter the Valid Email Adress Such as user@Domain.com...";
				txtEmail.setText("");
				++cnt;
			}
		}

		boolean hasError = false;
		String strError = "";
		for (int i = 0; i < errors.length; i++) {
			if (errors[i] != null) {
				strError += errors[i] + "\n";
				hasError = true;
			}

		}
		if (hasError) {
			MessageBox mb = new MessageBox(sh, SWT.ERROR);
			mb.setText("Errors");
			mb.setMessage(strError);
			mb.open();
			return false;
		} else {
			return true;
		}

	}

	protected void SetFieldEnabled() {
		txtAdress.setEnabled(true);
		txtCellno.setEnabled(true);
		txtEmail.setEnabled(true);
		txtFullName.setEnabled(true);
		txtLoginName.setEnabled(true);
		txtPassword.setEnabled(true);
		txtPhoneNo.setEnabled(true);
		txtRepassword.setEnabled(true);
		cmdSave.setEnabled(true);
		cmdDelete.setEnabled(true);
		cmbRole.setEnabled(true);
		btnIsAdmin.setEnabled(true);
	}

	protected void SetFieldDisable() {
		txtUserId.setText("");
		txtAdress.setText("");
		txtCellno.setText("");
		txtEmail.setText("");
		txtFullName.setText("");
		txtLoginName.setText("");
		txtPassword.setText("");
		txtRepassword.setText("");
		txtPhoneNo.setText("");
		txtAdress.setEnabled(false);
		txtCellno.setEnabled(false);
		txtEmail.setEnabled(false);
		txtFullName.setEnabled(false);
		txtLoginName.setEnabled(false);
		txtPassword.setEnabled(false);
		txtPhoneNo.setEnabled(false);
		txtRepassword.setEnabled(false);
		cmdSave.setEnabled(false);
		cmdDelete.setEnabled(false);
		cmdEdit.setEnabled(false);
		cmbRole.select(0);
		cmbRole.setEnabled(false);
		btnIsAdmin.setSelection(false);
		btnIsAdmin.setEnabled(false);

	}

	private void FillTableDetails() {

		ResultSet rsUserDetail = objUserCntrl.GetUserDetail();
		try {
			if (rsUserDetail == null) {
				TableItem item = new TableItem(tblDetails, SWT.NONE);
				item.setText(0, "");
				item.setText(1, "");
				item.setText(2, "");
				item.setText(3, "");
				item.setText(4, "");
				item.setText(5, "");
				item.setText(6, "");
				item.setText(7, "");
				item.setText(8, "");

			} else
				do {
					TableItem item = new TableItem(tblDetails, SWT.NONE);
					item.setText(0, String.valueOf(tblDetails.getItemCount()));

					item.setText(1, rsUserDetail.getString("login_name"));
					item.setText(2, rsUserDetail.getString("user_name"));
					if (rsUserDetail.getString("address") == null) {
						item.setText(3, "");
					} else
						item.setText(3, rsUserDetail.getString("address"));
					if (rsUserDetail.getString("email") == null) {
						item.setText(4, "");
					} else
						item.setText(4, rsUserDetail.getString("email"));
					//int loginStatus = Integer.parseInt(rsUserDetail
							//.getString("role_id"));				
					item.setText(5, rsUserDetail.getString("role"));
					item.setText(6, rsUserDetail.getString("user_id"));
					if (rsUserDetail.getString("phone") == null) {
						item.setText(7, "");
					} else
						item.setText(7, rsUserDetail.getString("phone"));
					if (rsUserDetail.getString("cell") == null) {
						item.setText(8, "");
					} else
						item.setText(8, rsUserDetail.getString("cell"));
					
					item.setText(9, rsUserDetail.getString("role_id"));
					item.setText(10, rsUserDetail.getString("user_type"));

				} while (rsUserDetail.next());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// public static String encryptPassword( String password )
	// {
	// String encrypted = "";
	// try
	// {
	// MessageDigest digest = MessageDigest.getInstance( "MD5" );
	// byte[] passwordBytes = password.getBytes( );
	// digest.reset( );
	// digest.update( passwordBytes );
	// byte[] message = digest.digest( );
	//
	// StringBuffer hexString = new StringBuffer();
	// for ( int i=0; i < message.length; i++)
	// {
	// hexString.append( Integer.toHexString(
	// 0xFF & message[ i ] ) );
	// }
	// encrypted = hexString.toString();
	// }
	// catch( Exception e ) { }
	// return encrypted;
	// }

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}
}
