package com.tia.user.view;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.user.controller.encryptDectyptPassword;

public class ChangePasswordDialog extends Dialog {
	private Text txtCurrentPassword;
	private Text txtNewPassword;
	private Text txtConfrompsw;
	private UtilMethods objUtilMethods;
	private Composite comParent;
	private Composite comMainContainer;
	Object rtn;

	public ChangePasswordDialog(Shell parentShell) {
		super(parentShell);
		objUtilMethods = new UtilMethods();

	}

	public Object open() {
		Shell parent = getParent();
		Shell shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setText("Password Management");
		shell.setSize(350, 350);
		// Your code goes here (widget creation, set result, etc).
		createDialogArea(shell);
		shell.open();
		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		return rtn;
	}

	protected Control createDialogArea(Composite parent) {
		comParent = parent;// (Composite) super.createDialogArea(parent);
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("Password Management", comParent,
				ChangePasswordDialog.class);

		// Main container that holds all other controls/widgets of the view
		comMainContainer = new Composite(comParent, SWT.NONE);
		comMainContainer.setLayout(new FormLayout());
		GridData gd_comMainContainer = new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1);
		gd_comMainContainer.widthHint = 983;
		gd_comMainContainer.heightHint = 561;
		comMainContainer.setLayoutData(gd_comMainContainer);

		Group grpPassword = new Group(comMainContainer, SWT.NONE);
		grpPassword.setText("Password Management");
		grpPassword.setBounds(206, 25, 388, 282);

		Button btnChangePassoword = new Button(grpPassword, SWT.NONE);
		btnChangePassoword.setBounds(59, 211, 120, 25);
		btnChangePassoword.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int currentUserId = ApplicationWorkbenchWindowAdvisor
						.getLoginId();
				String loginPwd = UtilFxn.GetValueFromTable("inven_user",
						"login_pwd", "user_id='" + currentUserId + "'");

				String password = txtCurrentPassword.getText();
				String decryptpassword = encryptDectyptPassword
						.encryptPassword(password);
				if (loginPwd.equals(decryptpassword)) {
					if (txtNewPassword.getText()
							.equals(txtConfrompsw.getText())) {
						String newPwd = encryptDectyptPassword
								.encryptPassword(txtNewPassword.getText());
						if(changePasswrd(newPwd, currentUserId)){
							UtilMethods.messageBox(SWT.ICON_INFORMATION,
									"Success",
									"Your Password has been Sucessfully Changed")
									.open();
							setFieldBlank();
						}else {
							UtilMethods.messageBox(SWT.ICON_ERROR,
									"Errot",
									"Something is wroung, Password is not updated")
									.open();							
						}

					} else
						UtilMethods
								.messageBox(SWT.ERROR, "Error",
										"Your Confirmation password donot match with new password Please try once.")
								.open();
					txtConfrompsw.setText("");
					txtNewPassword.setText("");
				} else {
					UtilMethods.messageBox(SWT.ERROR, "Error",
							"your Password Input is incorrect").open();
					setFieldBlank();
				}

			}
		});
		btnChangePassoword.setText("Change Password");

		Button btnCancel = new Button(grpPassword, SWT.NONE);
		btnCancel.setBounds(201, 211, 114, 25);
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setFieldBlank();
			}
		});
		btnCancel.setText("Cancel");

		Label lblConformPassword = new Label(grpPassword, SWT.NONE);
		lblConformPassword.setBounds(10, 141, 114, 15);
		lblConformPassword.setText("Conform Password :");
		lblConformPassword.setAlignment(SWT.RIGHT);

		txtConfrompsw = new Text(grpPassword, SWT.BORDER | SWT.PASSWORD);
		txtConfrompsw.setBounds(130, 141, 136, 21);

		txtNewPassword = new Text(grpPassword, SWT.BORDER | SWT.PASSWORD);
		txtNewPassword.setBounds(130, 97, 136, 21);

		Label lblNewPassword = new Label(grpPassword, SWT.NONE);
		lblNewPassword.setBounds(10, 97, 114, 15);
		lblNewPassword.setText("New Password :");
		lblNewPassword.setAlignment(SWT.RIGHT);

		txtCurrentPassword = new Text(grpPassword, SWT.BORDER | SWT.PASSWORD);
		txtCurrentPassword.setBounds(130, 49, 136, 21);

		Label lblCurrentPassword = new Label(grpPassword, SWT.NONE);
		lblCurrentPassword.setBounds(10, 55, 114, 15);
		lblCurrentPassword.setText("Current Password :");
		lblCurrentPassword.setAlignment(SWT.RIGHT);

		grpPassword.setTabList(new Control[] { txtCurrentPassword,
				txtNewPassword, txtConfrompsw, btnChangePassoword, btnCancel });

		return comParent;

		// TODO Auto-generated method stub

	}

	protected void setFieldBlank() {
		txtConfrompsw.setText("");
		txtCurrentPassword.setText("");
		txtNewPassword.setText("");

	}

	protected boolean changePasswrd(String decryptpassword, int currentUserId) {
		boolean rtnVal = false;
		Connection cnn = DbConnection.getConnection();
		String strQry = "Update inven_user set login_pwd='" + decryptpassword
				+ "' where user_id='" + currentUserId + "' ";
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);
			int rsUserDetail = st.executeUpdate(strQry);
			if (rsUserDetail == 1) {
				System.out.println("Sucessfully changed");
				rtnVal = true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rtnVal;
	}

}
