package com.tia.user.view;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;

import com.swtdesigner.SWTResourceManager;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import com.tia.user.model.LogInInfo;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class ForceLogOutDialog extends Dialog{
	private Shell sh;
	private static Composite comContainer;
	private static Composite comTitle;
	private static Composite comMain;
	private static GridLayout glOuterMain;
	private static GridData gdMain;
	
	private static Button cmdView;
	private Button cmdLogOff;
	private Table tblViewUsers;
	private TableColumn tblclmnSno;
	private TableColumn tblclmnUser;
	private TableColumn tblclmnUserName;
	private TableColumn tblclmnLogout;
	private TableColumn tblclmnUserid;
	private TableColumn tblclmnLoginid;
	
	private Connection cnn;
	private Statement st;
	private JCalendarFunctions jCal = new JCalendarFunctions();
	private TableColumn tblclmnLogInTime;
	static Shell shell;
	Object rtn;

	public ForceLogOutDialog(Shell parent) {
	//	sh = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		/*setTitleImage(SWTResourceManager.getImage(ForceLogOutDialog.class,
				"/logo.png"));*/
		super(parent);
	}
	
	public Object open()
	{
		Shell parent = getParent();
		shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL |SWT.CENTER);
		shell.setSize(646, 287);
		shell.setText("Force LogoutView");
		shell.setBounds(30, 50,  630, 240);
		createDialogArea(shell);
		shell.open();
		return rtn;
	}

	public void createDialogArea(Shell parent) {

		comContainer = parent;
		glOuterMain = new GridLayout();
		glOuterMain.marginHeight = 1;
		comContainer.setLayout(glOuterMain);

		comTitle = Banner.getTitle("ForceLogoutView", comContainer,
				ForceLogOutDialog.class);

		comMain = new Composite(comContainer, SWT.NONE);
		gdMain = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdMain.heightHint = 254;
		gdMain.widthHint = 621;
		comMain.setLayoutData(gdMain);
		comMain.setLayout(null);
		
		cmdView = new Button(comMain, SWT.NONE);
		cmdView.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				loadUsers() ;
			}
		});
		cmdView.setImage(SWTResourceManager.getImage(ForceLogOutDialog.class, "/refresh.gif"));
		cmdView.setBounds(10, 10, 75, 23);
		cmdView.setText("View");
		
		
		cmdLogOff = new Button(comMain, SWT.NONE);
		cmdLogOff.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				logoffUser();
				loadUsers();
			}
		});
		cmdLogOff.setText("LogOff");
		cmdLogOff.setImage(SWTResourceManager.getImage(ForceLogOutDialog.class, "/remove.png"));
		cmdLogOff.setBounds(91, 10, 75, 23);
		
		tblViewUsers = new Table(comMain, SWT.BORDER | SWT.FULL_SELECTION);
		tblViewUsers.setBounds(10, 39, 609, 173);
		tblViewUsers.setHeaderVisible(true);
		tblViewUsers.setLinesVisible(true);
		
		tblclmnSno = new TableColumn(tblViewUsers, SWT.CENTER);
		tblclmnSno.setResizable(false);
		tblclmnSno.setWidth(50);
		tblclmnSno.setText("S.No");
		
		tblclmnUser = new TableColumn(tblViewUsers, SWT.NONE);
		tblclmnUser.setWidth(300);
		tblclmnUser.setText("User");
		
		tblclmnUserName = new TableColumn(tblViewUsers, SWT.CENTER);
		tblclmnUserName.setWidth(100);
		tblclmnUserName.setText("UserName");
		
		tblclmnUserid = new TableColumn(tblViewUsers, SWT.NONE);
		tblclmnUserid.setResizable(false);
		tblclmnUserid.setText("userId");
		
		tblclmnLoginid = new TableColumn(tblViewUsers, SWT.NONE);
		tblclmnLoginid.setResizable(false);
		tblclmnLoginid.setText("logInId");
		
		tblclmnLogout = new TableColumn(tblViewUsers, SWT.CENTER);
		tblclmnLogout.setResizable(false);
		tblclmnLogout.setText("Logout");
		
		tblclmnLogInTime = new TableColumn(tblViewUsers, SWT.NONE);
		tblclmnLogInTime.setResizable(false);
		tblclmnLogInTime.setWidth(150);
		tblclmnLogInTime.setText("LogInTime");
		
	}
	
	private void loadUsers() {
		String strQry = "SELECT usr.user_name, usr.login_name, lii.log_in_time, "
				+ "lii.user_id, lii.log_in_info_id FROM log_in_info_mcg lii "
				+ "LEFT JOIN inven_user usr ON usr.user_id = lii.user_id "
				+ "WHERE ISNULL(lii.log_out_time, 0) = 0";
		
		/* AND lii.user_id <> '"
			+ LoginDialog.userId + "
*/		ResultSet rs = null;
		int i = 0;

		cnn = DbConnection.getConnection();

		try {
			tblViewUsers.removeAll();
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			rs = st.executeQuery(strQry);
			System.err.println("\nLoad Users : "+strQry);
			while (rs.next()) {
				if (!rs.getString("user_id").equals(null)) {
					TableItem item = new TableItem(tblViewUsers, SWT.NONE);
					item.setText(0, String.valueOf(++i));
					item.setText(1, rs.getString("user_name"));
					item.setText(2, rs.getString("login_name"));
					item.setText(3, rs.getString("user_id"));
					item.setText(4, rs.getString("log_in_info_id"));
					item.setText(
							6,
							jCal.GetNepaliDate(rs.getString("log_in_time")
									.substring(0, 10))
									+ "  "
									+ rs.getString("log_in_time").substring(11,
											19));
				}
			}
			if (i < 1){
				UtilMethods.messageBoxDialog(SWT.ICON_INFORMATION, "Information","There is no one logged in at the moment."
						).open();
				shell.close();
				LoginDialog.chkLogin.setSelection(false);
				LoginDialog.clbl_Message.setText("");
			}
		} catch (Exception ex) {
			UtilMethods.messageBoxDialog(SWT.ICON_ERROR, "Error", ex.toString()).open();
		}
	}
	
	private void logoffUser() {
		String id = tblViewUsers.getItem(tblViewUsers.getSelectionIndex())
				.getText(4);
		LogInInfo lii = new LogInInfo();

		lii.setLog_out_time(jCal.getCurrentEnglishDateTime(""));
		lii.setUpdated_by(Integer.valueOf(LoginDialog.userId));
		lii.setUpdated_date(JCalendarFunctions.currentDate());
		lii.setUpdate_count(Integer.parseInt(UtilFxn.GetValueFromTable(
				"log_in_info_mcg", "update_count", "log_in_info_id = '"
						+ id + "'")) + 1);
		lii.z_WhereClause.log_in_info_id(Integer.parseInt(id));
		String strQry = lii.Update();

		try {
			System.err.println("jCal.getCurrentEnglishDateTime()"+jCal.getCurrentEnglishDateTime(""));
			System.err.println("\njCal.Today()"+jCal.Today());
			System.err.println("\nstrQry"+strQry);
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			if (st.executeUpdate(strQry) > 0) {
				UtilMethods.messageBoxDialog(SWT.ICON_INFORMATION, ("Information"),
						("Selected user logged off successfully."))
						.open();
			} else {
				UtilMethods.messageBoxDialog(SWT.ICON_INFORMATION,("Information"),
						("Unable to log off selected user.\nPlease try again later."))
						.open();
			}
		} catch (SQLException ex) {
			System.out.println(ex.toString());
		}
	}

	
}
