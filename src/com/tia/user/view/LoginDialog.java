package com.tia.user.view;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.*;

import com.swtdesigner.SWTResourceManager;
import com.tia.common.util.LogFileCreate;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import com.tia.user.controller.LoginController;
import com.tia.user.controller.LoginSession;

public class LoginDialog extends ViewPart {

	private Text txt_Password;
	private Text txt_Username;// /scope redefined
	private Display display;
	public static Label clbl_Message;
	private LoginController loginController;
	private LoginSession loginSession;
	public static Button chkLogin;
	static String userId;
	JCalendarFunctions jcal = new JCalendarFunctions();

	public void setLoginSession(LoginSession loginSession) {
		this.loginSession = loginSession;
		System.out
				.println("Setting Login Session :...> This msg is from LoginDialog's setLoginSession() method");
	}

	public LoginDialog(Display display) {
		this.display = display;

		loginController = new LoginController();
		// loginSession = new LoginSession();

	}

	@Override
	public void createPartControl(Composite parent) {
		// Shell must be created with style SWT.NO_TRIM
		final Shell shell = new Shell(display, SWT.NO_TRIM | SWT.ON_TOP);

		FillLayout fillLayout = new FillLayout();
		fillLayout.marginHeight = 1;
		shell.setLayout(fillLayout);
		shell.setSize(600, 300);

		// Create a composite with grid layout.
		Composite composite = new Composite(shell, SWT.NONE);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		gridLayout.marginHeight = 0;
		gridLayout.verticalSpacing = 0;
		gridLayout.marginWidth = 40;
		gridLayout.horizontalSpacing = 0;
		composite.setLayout(gridLayout);

		/*
		 * Setting the background of the composite with the image background for
		 * login dialog
		 */
		Label img_Label = new Label(composite, SWT.NONE);
		img_Label.setLayoutData(new GridData(300, 300));
		img_Label.setImage(SWTResourceManager.getImage(LoginDialog.class,
				"/login.jpg"));
		

		/*
		 * Creating the composite which will contain the login related widgets
		 */
		Composite cmp_Login = new Composite(composite, SWT.NONE);
		RowLayout rowLayout = new RowLayout();
		rowLayout.fill = true;
		cmp_Login.setLayout(rowLayout);
		GridData gridData = new GridData(GridData.FILL, GridData.FILL, false,
				false);
		gridData.widthHint = 200;
		gridData.verticalIndent = 40;
		cmp_Login.setLayoutData(gridData);

		// Label for the heading
		CLabel clbl_UserLogin = new CLabel(cmp_Login, SWT.NONE);
		RowData rowData = new RowData();
		rowData.width = 180;
		rowData.height = 50;
		clbl_UserLogin.setLayoutData(rowData);
		clbl_UserLogin.setText("User Login");
		clbl_UserLogin.setFont(SWTResourceManager
				.getFont("Arial", 12, SWT.BOLD));

		// Label for the username
		CLabel clbl_Username = new CLabel(cmp_Login, SWT.NONE);
		RowData rowData_1 = new RowData();
		rowData_1.width = 180;
		clbl_Username.setLayoutData(rowData_1);
		clbl_Username.setText("Username");

		// Textfield for the username
		txt_Username = new Text(cmp_Login, SWT.BORDER);
		RowData rowData_2 = new RowData();
		rowData_2.width = 170;
		txt_Username.setLayoutData(rowData_2);
		// txt_Username.setText("loojah");

		// Label for the password
		CLabel clbl_Password = new CLabel(cmp_Login, SWT.NONE);
		RowData rowData_3 = new RowData();
		rowData_3.width = 180;
		clbl_Password.setLayoutData(rowData_3);
		clbl_Password.setText("Password");

		// Textfield for the password
		txt_Password = new Text(cmp_Login, SWT.BORDER);
		RowData rowData_4 = new RowData();
		rowData_4.width = 170;
		txt_Password.setLayoutData(rowData_4);
		// txt_Password.setText("loojah");
		txt_Password.setEchoChar('*');
		txt_Password.addListener(SWT.KeyDown, new Listener() {

			@Override
			public void handleEvent(Event e) {

				if (e.keyCode == SWT.CR)
					// text fields must not be empty
					if (txt_Username.getText().isEmpty()
							|| txt_Password.getText().isEmpty()) {
						clbl_Message.setText("");
						clbl_Message.setText("Both fields are mandatory.");
					} else {

						// Call the authentication method here.
						if (loginController.isAuthenticated(
								txt_Username.getText(), txt_Password.getText())) {

							String user = loginController
									.GetUserIdFromUserName(txt_Username
											.getText());

							if (chkLogin.getSelection()) {
								if (loginController.toCheckAdminUser(user) == true) {
									userId = user;
									ForceLogOutDialog force = new ForceLogOutDialog(
											new Shell(display, SWT.NO_TRIM
													| SWT.ON_TOP));
									(force).open();
									return;
								}
							}
							// check if current user is already logged on
							if (loginController.HasLoggedOn(user)) {
								clbl_Message
										.setText("This user have already been logged on."
												+ "Please try another user,or contact your administrator.");
							} else {
								if (loginController.SaveUserData(user)) {
									setSessionData();
									LogFileCreate.logAtLogin();
									shell.close();
								} else {
									clbl_Message
											.setText("Unable to login at the moment."
													+ (("\nPlease try again later,or contact your administrator.")));
								}
							}
							// successful logins set session data in
							// LoginSession
							// class
							setSessionData();

							shell.close();

						} else {
							// stay on this dialog and display the warning
							// message
							clbl_Message
									.setText("Wrong credentials!Or A/C INACTIVE.");
						}
					}

			}
		});

		/*
		 * Composite to hold buttons.
		 */
		Composite cmp_ButtonBar = new Composite(cmp_Login, SWT.NONE);
		RowData rowData_5 = new RowData();
		rowData_5.height = 38;
		rowData_5.width = 200;
		cmp_ButtonBar.setLayoutData(rowData_5);
		cmp_ButtonBar.setLayout(new FormLayout());
		// cmp_ButtonBar.setBackground(SWTResourceManager.getColor(240,240,240));

		// Button for login
		Button btn_login = new Button(cmp_ButtonBar, SWT.FLAT);
		FormData formData = new FormData();
		formData.bottom = new FormAttachment(0, 28);
		formData.top = new FormAttachment(0, 5);
		formData.right = new FormAttachment(100, -70);
		formData.left = new FormAttachment(100, -120);
		btn_login.setLayoutData(formData);
		btn_login.setText("Login");

		chkLogin = new Button(cmp_ButtonBar, SWT.CHECK);
		chkLogin.setBounds(10, 100, 70, 16);
		chkLogin.setText("As Admin");
		// Adding widget selected code for the login button.
		btn_login.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event e) {

				if (txt_Username.getText().isEmpty()
						|| txt_Password.getText().isEmpty()) {
					clbl_Message.setText("");
					clbl_Message.setText("Both fields are mandatory.");
				} else {

					// Call the authentication method here.
					if (loginController.isAuthenticated(txt_Username.getText(),
							txt_Password.getText())) {

						String user = loginController
								.GetUserIdFromUserName(txt_Username.getText());

						if (chkLogin.getSelection()) {
							if (loginController.toCheckAdminUser(user) == true) {
								userId = user;
								ForceLogOutDialog force = new ForceLogOutDialog(
										new Shell(display, SWT.NO_TRIM
												| SWT.ON_TOP));
								(force).open();
								return;
							}
							else
							{
								clbl_Message
								.setText("This user does not have administrative previlage.");
										
							}
						}
						// check if current user is already logged on
						if (loginController.HasLoggedOn(user)) {
							clbl_Message
									.setText("This user have already been logged on."
											+ "Please try another user,or contact your administrator.");
						} else {
							if (loginController.SaveUserData(user)) {
								setSessionData();
								LogFileCreate.logAtLogin();
								shell.close();
							} else {
								clbl_Message
										.setText("Unable to login at the moment."
												+ (("\nPlease try again later,or contact your administrator.")));
							}
						}
						// successful logins set session data in LoginSession
						// class
						//setSessionData();

						//shell.close();

					} else {
						// stay on this dialog and display the warning message
						clbl_Message
								.setText("Wrong credentials!Or A/C INACTIVE.");
					}
				}

			}
		});

		// Button for cancel
		Button btnCancel = new Button(cmp_ButtonBar, SWT.FLAT);
		FormData fdCancel = new FormData();
		fdCancel.bottom = new FormAttachment(0, 28);
		fdCancel.top = new FormAttachment(0, 5);
		fdCancel.right = new FormAttachment(100, -15);
		fdCancel.left = new FormAttachment(100, -65);
		btnCancel.setLayoutData(fdCancel);
		btnCancel.setText("Cancel");

		// Adding widget selected action for the cancel button.
		btnCancel.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event e) {

				shell.close();
				System.exit(0);

			}
		});

		// Label for copyright info
		clbl_Message = new Label(cmp_Login, SWT.NONE);
		clbl_Message.setAlignment(SWT.RIGHT);
		RowData rowData_6 = new RowData();
		rowData_6.width = 188;
		rowData_6.height = 15;
		clbl_Message.setLayoutData(rowData_6);
		clbl_Message.setText("");
		clbl_Message.setForeground(new Color(display, 255, 0, 0));
		// clbl_Message.setBackground(SWTResourceManager.getColor(240,240,240));

		cmp_Login.setTabList(new Control[] { txt_Username, txt_Password,
				cmp_ButtonBar });
		/*
		 * Drawing a region which will form the base of the login
		 */
		Region region = new Region();
		Rectangle pixel = new Rectangle(1, 1, 550, 300);
		region.add(pixel);
		shell.setRegion(region);

		Rectangle resolution = display.getPrimaryMonitor().getBounds();
		shell.setLocation((resolution.width / 2) - 250,
				(resolution.height / 2) - 150);
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		region.dispose();

	}

	private void setSessionData() {

		this.loginSession.loadSessionUserData(txt_Username.getText());
	}

	@Override
	public void setFocus() {

	}

}
