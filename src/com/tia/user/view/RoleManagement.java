package com.tia.user.view;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;



import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.CmbQry;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.user.controller.RoleController;
import com.tia.user.model.AuthClass;
import com.tia.user.model.RoleClass;

public class RoleManagement extends ViewPart {

	
	private static final int COLUMNS = 9;
	private Connection cnn=DbConnection.getConnection();
	public static Tree treeAuthList;
	private Text text;
	private TreeItem item;
	private Shell sh;
	static int selIndex;
	private RoleClass objRoleClass;
	private RoleController objRoleController;
	private Label lblRoleName;
	private SwtBndCombo cmbUserCheck;
	private TableColumn tblclmnAlias;
	ResultSet authid;
	private AuthClass objAuthModel;
	private TreeItem[] childItems;
	public static String namerole;

	boolean flag;
	boolean flag1;
	boolean Result;
	Button cmdSelectall;

	Button cmdSave;
	Button cmdNew;
	public Button cmdEdit;
	private Button cmdDelete;

     private static String[] strButtonYesNo = { "Yes",
			"No" };
     private static String[] strButtonOk = {"Ok"};
	private Composite compParent;
	private Composite cmpMain;
	private Group grpCommands;
  
	public RoleManagement() {

		// instance of the controller
		objRoleClass = new RoleClass();
		objRoleController = new RoleController();
		// get the shell
		setTitleImage(SWTResourceManager.getImage(UserManagement.class, "/user16.png"));
		this.sh = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getShell();
			}

	@Override
	public void createPartControl(Composite parent) {

		compParent = new Composite(parent, SWT.NONE);
		GridLayout gl_comOuterMain = new GridLayout();
		//gl_comOuterMain.numColumns = 2;
		gl_comOuterMain.marginHeight = 1;
		compParent.setLayout(gl_comOuterMain);
		Banner.getTitle("RoleManagement", compParent,
	    RoleManagement.class);
	
		GridData gd_cmbUserType = new GridData(SWT.LEFT, SWT.CENTER, true,
				false, 1, 1);
		gd_cmbUserType.widthHint = 150;
		
		cmpMain = new Composite(compParent, SWT.NONE);
		GridData gd_cmpMain = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_cmpMain.heightHint = 671;
		gd_cmpMain.widthHint = 983;
		cmpMain.setLayoutData(gd_cmpMain);
		
				Group btngroup = new Group(cmpMain, SWT.NONE);
				btngroup.setLocation(10, 146);
				btngroup.setSize(379, 145);
				
				cmdSave = new Button(btngroup, SWT.NONE);
				cmdSave.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e)
				{
					System.out.println("save inside");
					namerole = (cmbUserCheck.getSelectedBoundValue().toString());
					String txtEditRole=text.getText().toString();
					System.out.println("txtEditRole::"+txtEditRole);
					cmdSave.setEnabled(false);
					cmdSelectall.setEnabled(false);
					System.out.println("flagis:"+flag);
					
					try {
						cnn.setAutoCommit(false);
						//validation for not allowing to create duplicate role//// --mani
						//case of edit
						if(flag==true){
						String NameR=objRoleClass.GetName(txtEditRole,namerole);
						if(!NameR.equals("0")){
							UtilMethods.messageBox(SWT.ICON_INFORMATION,  "Error",
  				   			 "rolealreadyexists.duplicatenamenotallowed.")
  							.open();
  						System.out.println("duplicaterolename");
  						return;
						}
						}
						//case for creating new role and validation
						else if(flag==false){
							String NameR=objRoleClass.GetName(txtEditRole,"");
							if(!NameR.equals("0")){
								UtilMethods.messageBox(SWT.ICON_INFORMATION, "Error",
	  				   			 "rolealreadyexists.duplicatenamenotallowed.")
	  							.open();
	  						System.out.println("duplicaterolename");
	  						return;
							}
							
						}
					//	else{
						//if(select(*)count from from user_role_mcg where role='txtEditRole.toString()';)
//				 ResultSet authid=objRoleClass.getnames();
//	       			try {
//	       				authid.beforeFirst();
//	       				while(authid.next()){
//	       					if(txtEditRole.toString().equals(authid.getString("role"))){
//	       						UtilFxn.messageBox(SWT.ICON_INFORMATION,  Messages.getLabel("Error"),
//	       				   			 Messages.getLabel("rolealreadyexists.duplicatenamenotallowed."))
//	       							.open();
//	       						System.out.println("duplicaterolename");
//	       						retValue=false;
//	       				}
//	       					
//	       				}
//	       			} catch (SQLException e1) {
//	       				// TODO Auto-generated catch block
//	       				e1.printStackTrace();
//	       			}
						
						//for purpose of saving role with different authories
						Result=objRoleController.saveAction(flag,namerole,txtEditRole);
						 if(Result==true)
							{
							 cnn.commit();
							 cnn.setAutoCommit(true);
							 System.out.println("save inside2");
							 sucessSave();
							}else
							{	cnn.rollback();
								cnn.setAutoCommit(true);
								System.out.println("save inside3");
							 ErrorSave();
							}
					//} 
					}catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();

					}
						}
	 });
				
				cmdSave.setText("Save");
				
					cmdSave.setBounds(137, 84, 105, 36);
					cmdSave.setEnabled(false);
					
							cmdEdit = new Button(btngroup, SWT.NONE);
							cmdEdit.setBounds(137, 31, 105, 36);
							cmdEdit.setText("Edit");
							
							cmdEdit.addSelectionListener( new SelectionAdapter() {
								@Override
								public void widgetSelected(SelectionEvent e) {
									if(ApplicationWorkbenchWindowAdvisor
									.hasAuthOr(new String[] { "role_update" }))
									{
								if(cmbUserCheck.getSelectionIndex()>0)
								{
									
								cmdSave.setEnabled(true);
								cmdSelectall.setEnabled(true);
								treeAuthList.setEnabled(true);
								flag=true;
								cmbUserCheck.setVisible(false);
								// displaying tree of selected  role
								displaytree(namerole);
								String namerole = (cmbUserCheck.getText().toString());
								text = new Text(grpCommands, SWT.NONE);//final grp
								text.setText(namerole);
							
							    text.setBounds(10, 45, 262, 26);
								
								}else
								{
									UtilMethods.messageBox(SWT.ICON_INFORMATION,  "Error",
								   			 "SelectRoleToEdit")
											.open();	
								}
								}
								}
								
							});
							
							Button btnCancel = new Button(btngroup, SWT.NONE);
							btnCancel.addSelectionListener(new SelectionAdapter() {
								@Override
								public void widgetSelected(SelectionEvent e) {
									text.setText("");		
									
									cmdSave.setEnabled(false);
								   	 cmbUserCheck.setVisible(true);
									 cmdEdit.setEnabled(true);
								   	 displaytree("-1");
								   	 cmbUserCheck.fillData(CmbQry.getRoleQuery(), CmbQry.getLabel(),
								 				CmbQry.getId(), DbConnection.getConnection());
								   	 treeAuthList.setEnabled(true);
											
								}
							});
							btnCancel.setText("Cancel");
							
							btnCancel.setBounds(10, 84, 105, 36);
							
//		text = new Text(grpCommands, SWT.NONE);//final grp
//		text.setFont(CommonFunctions.getFont());
//		text.setBounds(10, 45, 262, 26);
	
							cmdNew = new Button(btngroup, SWT.NONE);
							cmdNew.addSelectionListener(new SelectionAdapter() {
								@Override
								public void widgetSelected(SelectionEvent e) {
									if(ApplicationWorkbenchWindowAdvisor
											.hasAuthOr(new String[] { "role_add" }))
									{
									cmbUserCheck.setVisible(false);
									cmdEdit.setEnabled(false);
									cmdSave.setEnabled(true);
									cmdSelectall.setEnabled(true);
									flag=false;
//				/System.out.println("new role is::---"+text.getText());
									text = new Text(grpCommands, SWT.NONE);//final grp
									
									text.setBounds(10, 45, 262, 23);
									text.setText("");
									treeAuthList.setEnabled(true);
								}
									
								}
							});
							cmdNew.setText("New");
							
								cmdNew.setBounds(10, 31, 105, 36);
								
								cmdDelete = new Button(btngroup, SWT.NONE);
								cmdDelete.addSelectionListener(new SelectionAdapter() {
									@Override
									public void widgetSelected(SelectionEvent e) {
										if(ApplicationWorkbenchWindowAdvisor
												.hasAuthOr(new String[] { "role_update" }))
												{
										String Role_id=(cmbUserCheck.getSelectedBoundValue().toString());
										//check if the selected role is assigned to any active user before deleting selected role.
										if(unassigned(Role_id)==true){
											if(UtilMethods.messageBox(
													SWT.ERROR | SWT.ICON_ERROR,
													"DeleteConfirmation",
													"Doyouwanttodeletetherole?",
													strButtonYesNo).open() == 0) {
												//deletion in user_role_mcg and role_auth_mcg
												RoleClass.deleterole(Role_id);
												RoleClass.deletetablrole(Role_id);
												cmbUserCheck.setVisible(true);
												cmbUserCheck.select(0);
												
												cmbUserCheck.fillData(CmbQry.getRoleQuery(), CmbQry.getLabel(),
											 				CmbQry.getId(), DbConnection.getConnection());
												//displayselected(Role_id);
												displaytree("-1");
												// text.setText("");
												///// text.setVisible(false);
												
												 //cmbUserCheck.setEnabled(true);
											 System.out.println("role is deleted.");}
											
										}
										else{
											UtilMethods.messageBox(SWT.ICON_ERROR,  "Error",
										   			"Thisroleisassignedtoactiveuser.Removetheuserfromthisrolefirst.",strButtonOk)
													.open();
										}
									}
									}
								});
								cmdDelete.setBounds(264, 31, 105, 36);
								cmdDelete.setText("Delete");
								
								btngroup.setTabList(new Control[]{cmdNew, cmdEdit, btnCancel, cmdSave, cmdDelete});
								 
								 grpCommands = new Group(cmpMain, SWT.NONE);
								 grpCommands.setText("Actions");
								 grpCommands.setBounds(10, 10, 379, 98);
								 
								 		lblRoleName = new Label(grpCommands, SWT.NONE);
								 		lblRoleName.setBounds(10, 21, 68, 23);
								 		lblRoleName.setText("RoleName");
								 		
								 		
								 		cmbUserCheck = new SwtBndCombo(grpCommands, SWT.READ_ONLY);
								 		
								 		cmbUserCheck.addSelectionListener(new SelectionAdapter()
								 		{
								 			@Override
								 			public void widgetSelected(SelectionEvent e) 
								 			{
								 				//objRoleController.getCmbAction();
								 				if(cmbUserCheck.getSelectionIndex()>0)
								 				{
								 				String txtrolename = (cmbUserCheck.getText().toString());
								 			    
								 			    namerole = (cmbUserCheck.getSelectedBoundValue().toString());
								 			    // displaying table of selected role
								 				
								 				//displaytree(namerole);
								 				treeAuthList.setEnabled(false);
								 				}
								 			}

								 		});
								 		cmbUserCheck.setBounds(10, 45, 262, 29);
								 		cmbUserCheck.select(0);
								 		cmbUserCheck.setLayoutData(gd_cmbUserType);
								 		
								 		cmdSelectall = new Button(grpCommands, SWT.NONE);
								 		cmdSelectall.addSelectionListener(new SelectionAdapter() {
								 			@Override
								 			public void widgetSelected(SelectionEvent e) {
								 				 TreeItem[] items= treeAuthList.getItems();
								 			      
								 			     for(int i=0; i<items.length; i++){
								 			    	 
								 			    	 RoleController.checkItems(items[i],true);
								 			      }
								 			}
								 		});
								 		cmdSelectall.setBounds(294, 43, 75, 27);
								 		cmdSelectall.setText("SelectAll");
								 		
								 		cmdSelectall.setEnabled(false);
								 		  
								 		  
	    // composite for table tree
								 		  Composite compTableTree = new Composite(cmpMain, SWT.NONE);
								 		  compTableTree.setBounds(395, 10, 578, 508);
								 		  compTableTree.setLayout(null);
								 		  treeAuthList = new Tree(compTableTree, SWT.BORDER | SWT.CHECK | SWT.Expand | SWT.V_SCROLL | SWT.H_SCROLL);
								 		  treeAuthList.setBounds(5, 5, 573, 493);
								 		  
								 		   treeAuthList.setEnabled(false);
								 		   treeAuthList.addSelectionListener(new SelectionAdapter()
								 		   {
								 		   	@Override
								 		   	public void widgetSelected(SelectionEvent e)
								 		   	{
								 		   		TreeItem selItem = (TreeItem) e.item;
								 		   		System.out.println("The selected item is" +selItem.getText(2));
								 		   	}
								 		   });
								 		   treeAuthList.addListener(SWT.Selection, new Listener()
								 		   {
	        @Override
			public void handleEvent(Event event)
	        {
	            if (event.detail == SWT.CHECK)
	            {
	                TreeItem item = (TreeItem) event.item;
	                objRoleController.getCheckeditem(item);
	            }
	        }
	    });
								 		   treeAuthList.setHeaderVisible(true);
								 		   treeAuthList.setLinesVisible(true);
								 		  
	       // displaydata();
								 		      treeAuthList.redraw();
								 		
								 		cmbUserCheck.fillData(CmbQry.getRoleQuery(), CmbQry.getLabel(),
								 				CmbQry.getId(), DbConnection.getConnection(),true);
		String[] titles = { "S.No","RoleName","AuthenticationName", "AuthenticationModules"};
		   
		    //treeJournalList.setFont(CommonFunctions.getFont());
		// the tree columns

		String[] colNames = { "S.No",
				"Modules",
				"Authentications","id"};
		
	
		/*
		 * Two dimensional array: first element for alignment, second element
		 * for width.
		 */
		int[][] intAlign = { { 0, 80 }, { 0, 150 }, { 0, 200 } ,{0,0}};

		TreeColumn[] col = new TreeColumn[4];
		

		for (int i = 0; i < colNames.length; i++) {
			if (intAlign[i][0] == 0)
				col[i] = new TreeColumn(treeAuthList, SWT.NONE);

			else
				col[i] = new TreeColumn(treeAuthList, SWT.RIGHT);

			col[i].setResizable(true);
			col[i].setWidth(intAlign[i][1]);
			col[i].setText(colNames[i]);
			
		}
        displayauthmodules();
	}

	protected boolean unassigned(String name) {
		// TODO Auto-generated method stub
		String id=UtilFxn.GetValueFromTable("user_accounts", "role_id", "role_id='"+name+"' and status='1'");
		if(id.equals("")){
			
			return true;
		}
		else{
			return false;
		}
	}

	

	private void displayauthmodules() {
		ResultSet roles = objRoleClass.getauthEntries();

		for (int i = 0; i < COLUMNS; i++) {

			try {
				while (roles.next()) {
					item = new TreeItem(treeAuthList, SWT.NONE);
					
					item.setText((new String[] { "+", roles.getString("mod_name_display") }));

					ResultSet details = objRoleClass.getauthDetails(roles.getString("mod_name"));

					// fetch the detailed entries
					while (details.next()) {
						TreeItem subItem = new TreeItem(item, SWT.NONE);
						subItem.setText(new String[] { "", "",
								details.getString("auth_name_display"),details.getString("auth_id") });
						//subItem.setFont(CommonFunctions.getFontLabel());
					}

				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		treeAuthList.setRedraw(true);

	}

public void displaytree(String txtrolename) {
		
	      TreeItem[] items= treeAuthList.getItems();
	      
	      for(int i=0; i<items.length; i++){
	    	  
	    	items[i].setChecked(false);
	    	items[i].setExpanded(false);
			TreeItem a = items[i];
			TreeItem[] childItems = a.getItems();
			
			for(int j=0; j<childItems.length; j++){
				
			boolean i1=objRoleClass.getEntries(txtrolename,childItems[j].getText(3));
			if(i1)
			{
			    items[i].setExpanded(true);
				childItems[j].setChecked(true);
				
			}
			else
			{
				childItems[j].setChecked(false);
			}
			}
			
		}
	}
	 

	public void sucessSave() {
		
		UtilMethods.messageBox(SWT.ICON_INFORMATION, "Sucess",
   			 "savedsucessfully.")
			.open();
   	 
	 text.dispose();
	 cmdSave.setEnabled(false);
   	 cmbUserCheck.setVisible(true);
	 cmdEdit.setEnabled(true);
   	 cmbUserCheck.fillData(CmbQry.getRoleQuery(), CmbQry.getLabel(),
 				CmbQry.getId(), DbConnection.getConnection());
   	 displaytree("-1");
		
	}
	
	

	public void ErrorSave() {
		UtilMethods.messageBox(SWT.ICON_INFORMATION,  "Error",
	   			 "DatanotSaved.")
				.open();
	  
	   	}
	@Override
	public void setFocus() {

	}
}
