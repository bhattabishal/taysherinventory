	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : UserManagementModel.
	/// </summary>
package com.tia.user.model;
	public class UserManagementModel
	{
		private int m_role_id;
		private int m_updt_by;
		private String m_address;
		private String m_login_pwd;
		private int m_user_id;
		private int m_user_type;
		private int m_updt_cnt;
		private String m_email;
		private String m_phone;
		private String m_updt_dt;
		private int m_status;
		private String m_login_name;
		private String m_user_name;
		private String m_cell;
		public UserManagementModel_Criteria z_WhereClause;

		private IsDirty_UserManagementModel z_bool;

		public UserManagementModel()
		{
			z_WhereClause = new UserManagementModel_Criteria();
			z_bool = new IsDirty_UserManagementModel();
		}
		public class IsDirty_UserManagementModel
		{
			public boolean  m_role_id;
			public boolean  m_updt_by;
			public boolean  m_address;
			public boolean  m_login_pwd;
			public boolean  m_user_id;
			public boolean  m_user_type;
			public boolean  m_updt_cnt;
			public boolean  m_email;
			public boolean  m_phone;
			public boolean  m_updt_dt;
			public boolean  m_status;
			public boolean  m_login_name;
			public boolean  m_user_name;
			public boolean  m_cell;
		}
		public int getRole_id()
		{
			return m_role_id;
		}
		public void setRole_id(int value)
		{
			z_bool.m_role_id = true;
			m_role_id = value;
		}
		public int getUpdt_by()
		{
			return m_updt_by;
		}
		public void setUpdt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
		}
		public String getAddress()
		{
			return m_address;
		}
		public void setAddress(String value)
		{
			z_bool.m_address = true;
			m_address = value;
		}
		public String getLogin_pwd()
		{
			return m_login_pwd;
		}
		public void setLogin_pwd(String value)
		{
			z_bool.m_login_pwd = true;
			m_login_pwd = value;
		}
		public int getUser_id()
		{
			return m_user_id;
		}
		public void setUser_id(int value)
		{
			z_bool.m_user_id = true;
			m_user_id = value;
		}
		public int getUser_type()
		{
			return m_user_type;
		}
		public void setUser_type(int value)
		{
			z_bool.m_user_type = true;
			m_user_type = value;
		}
		public int getUpdt_cnt()
		{
			return m_updt_cnt;
		}
		public void setUpdt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
		}
		public String getEmail()
		{
			return m_email;
		}
		public void setEmail(String value)
		{
			z_bool.m_email = true;
			m_email = value;
		}
		public String getPhone()
		{
			return m_phone;
		}
		public void setPhone(String value)
		{
			z_bool.m_phone = true;
			m_phone = value;
		}
		public String getUpdt_dt()
		{
			return m_updt_dt;
		}
		public void setUpdt_dt(String value)
		{
			z_bool.m_updt_dt = true;
			m_updt_dt = value;
		}
		public int getStatus()
		{
			return m_status;
		}
		public void setStatus(int value)
		{
			z_bool.m_status = true;
			m_status = value;
		}
		public String getLogin_name()
		{
			return m_login_name;
		}
		public void setLogin_name(String value)
		{
			z_bool.m_login_name = true;
			m_login_name = value;
		}
		public String getUser_name()
		{
			return m_user_name;
		}
		public void setUser_name(String value)
		{
			z_bool.m_user_name = true;
			m_user_name = value;
		}
		public String getCell()
		{
			return m_cell;
		}
		public void setCell(String value)
		{
			z_bool.m_cell = true;
			m_cell = value;
		}
		/*
		public UserManagementModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public UserManagementModel_Criteria Where(UserManagementModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_user ( ";
			if (z_bool.m_role_id)
			{
				SQL+= z_sep +"role_id";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by";
				z_sep=" , ";
				}
			if (z_bool.m_address)
			{
				SQL+= z_sep +"address";
				z_sep=" , ";
				}
			if (z_bool.m_login_pwd)
			{
				SQL+= z_sep +"login_pwd";
				z_sep=" , ";
				}
			if (z_bool.m_user_id)
			{
				SQL+= z_sep +"user_id";
				z_sep=" , ";
				}
			if (z_bool.m_user_type)
			{
				SQL+= z_sep +"user_type";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt";
				z_sep=" , ";
				}
			if (z_bool.m_email)
			{
				SQL+= z_sep +"email";
				z_sep=" , ";
				}
			if (z_bool.m_phone)
			{
				SQL+= z_sep +"phone";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt";
				z_sep=" , ";
				}
			if (z_bool.m_status)
			{
				SQL+= z_sep +"status";
				z_sep=" , ";
				}
			if (z_bool.m_login_name)
			{
				SQL+= z_sep +"login_name";
				z_sep=" , ";
				}
			if (z_bool.m_user_name)
			{
				SQL+= z_sep +"user_name";
				z_sep=" , ";
				}
			if (z_bool.m_cell)
			{
				SQL+= z_sep +"cell";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_role_id)
			{
				SQL+= z_sep +"'" + m_role_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"'" + m_updt_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_address)
			{
				SQL+= z_sep +"'" + m_address + "'";
				z_sep=" , ";
				}
			if (z_bool.m_login_pwd)
			{
				SQL+= z_sep +"'" + m_login_pwd + "'";
				z_sep=" , ";
				}
			if (z_bool.m_user_id)
			{
				SQL+= z_sep +"'" + m_user_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_user_type)
			{
				SQL+= z_sep +"'" + m_user_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"'" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_email)
			{
				SQL+= z_sep +"'" + m_email + "'";
				z_sep=" , ";
				}
			if (z_bool.m_phone)
			{
				SQL+= z_sep +"'" + m_phone + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"'" + m_updt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_status)
			{
				SQL+= z_sep +"'" + m_status + "'";
				z_sep=" , ";
				}
			if (z_bool.m_login_name)
			{
				SQL+= z_sep +"'" + m_login_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_user_name)
			{
				SQL+= z_sep +"'" + m_user_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cell)
			{
				SQL+= z_sep +"'" + m_cell + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_user SET ";
			if (z_bool.m_role_id)
			{
				SQL+= z_sep +"role_id='" + m_role_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + m_updt_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_address)
			{
				SQL+= z_sep +"address='" + m_address + "'";
				z_sep=" , ";
				}
			if (z_bool.m_login_pwd)
			{
				SQL+= z_sep +"login_pwd='" + m_login_pwd + "'";
				z_sep=" , ";
				}
			if (z_bool.m_user_id)
			{
				SQL+= z_sep +"user_id='" + m_user_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_user_type)
			{
				SQL+= z_sep +"user_type='" + m_user_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_email)
			{
				SQL+= z_sep +"email='" + m_email + "'";
				z_sep=" , ";
				}
			if (z_bool.m_phone)
			{
				SQL+= z_sep +"phone='" + m_phone + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + m_updt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_status)
			{
				SQL+= z_sep +"status='" + m_status + "'";
				z_sep=" , ";
				}
			if (z_bool.m_login_name)
			{
				SQL+= z_sep +"login_name='" + m_login_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_user_name)
			{
				SQL+= z_sep +"user_name='" + m_user_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cell)
			{
				SQL+= z_sep +"cell='" + m_cell + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_role_id)
			{
				SQL+= z_sep +"role_id='" + z_WhereClause.role_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_address)
			{
				SQL+= z_sep +"address='" + z_WhereClause.address() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_login_pwd)
			{
				SQL+= z_sep +"login_pwd='" + z_WhereClause.login_pwd() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_user_id)
			{
				SQL+= z_sep +"user_id='" + z_WhereClause.user_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_user_type)
			{
				SQL+= z_sep +"user_type='" + z_WhereClause.user_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_email)
			{
				SQL+= z_sep +"email='" + z_WhereClause.email() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_phone)
			{
				SQL+= z_sep +"phone='" + z_WhereClause.phone() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_status)
			{
				SQL+= z_sep +"status='" + z_WhereClause.status() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_login_name)
			{
				SQL+= z_sep +"login_name='" + z_WhereClause.login_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_user_name)
			{
				SQL+= z_sep +"user_name='" + z_WhereClause.user_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cell)
			{
				SQL+= z_sep +"cell='" + z_WhereClause.cell() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_user";
			if (z_WhereClause.z_bool.m_role_id)
			{
				SQL+= z_sep +"role_id='" + z_WhereClause.role_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_address)
			{
				SQL+= z_sep +"address='" + z_WhereClause.address() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_login_pwd)
			{
				SQL+= z_sep +"login_pwd='" + z_WhereClause.login_pwd() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_user_id)
			{
				SQL+= z_sep +"user_id='" + z_WhereClause.user_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_user_type)
			{
				SQL+= z_sep +"user_type='" + z_WhereClause.user_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_email)
			{
				SQL+= z_sep +"email='" + z_WhereClause.email() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_phone)
			{
				SQL+= z_sep +"phone='" + z_WhereClause.phone() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_status)
			{
				SQL+= z_sep +"status='" + z_WhereClause.status() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_login_name)
			{
				SQL+= z_sep +"login_name='" + z_WhereClause.login_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_user_name)
			{
				SQL+= z_sep +"user_name='" + z_WhereClause.user_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cell)
			{
				SQL+= z_sep +"cell='" + z_WhereClause.cell() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_user";
			if (z_WhereClause.z_bool.m_role_id)
			{
				SQL+= z_sep +"role_id='" + z_WhereClause.role_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_address)
			{
				SQL+= z_sep +"address='" + z_WhereClause.address() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_login_pwd)
			{
				SQL+= z_sep +"login_pwd='" + z_WhereClause.login_pwd() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_user_id)
			{
				SQL+= z_sep +"user_id='" + z_WhereClause.user_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_user_type)
			{
				SQL+= z_sep +"user_type='" + z_WhereClause.user_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_email)
			{
				SQL+= z_sep +"email='" + z_WhereClause.email() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_phone)
			{
				SQL+= z_sep +"phone='" + z_WhereClause.phone() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_status)
			{
				SQL+= z_sep +"status='" + z_WhereClause.status() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_login_name)
			{
				SQL+= z_sep +"login_name='" + z_WhereClause.login_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_user_name)
			{
				SQL+= z_sep +"user_name='" + z_WhereClause.user_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cell)
			{
				SQL+= z_sep +"cell='" + z_WhereClause.cell() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : UserManagementModel_Criteria.
	/// </summary>
	public class UserManagementModel_Criteria 
	{
		private int m_role_id;
		private int m_updt_by;
		private String m_address;
		private String m_login_pwd;
		private int m_user_id;
		private int m_user_type;
		private int m_updt_cnt;
		private String m_email;
		private String m_phone;
		private String m_updt_dt;
		private int m_status;
		private String m_login_name;
		private String m_user_name;
		private String m_cell;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_UserManagementModel_Criteria  z_bool;

		public UserManagementModel_Criteria()

		{
		z_bool=new IsDirty_UserManagementModel_Criteria();  
		}
		public class IsDirty_UserManagementModel_Criteria 
		{
			public boolean  m_role_id;
			public boolean  m_updt_by;
			public boolean  m_address;
			public boolean  m_login_pwd;
			public boolean  m_user_id;
			public boolean  m_user_type;
			public boolean  m_updt_cnt;
			public boolean  m_email;
			public boolean  m_phone;
			public boolean  m_updt_dt;
			public boolean  m_status;
			public boolean  m_login_name;
			public boolean  m_user_name;
			public boolean  m_cell;
			public boolean  MyWhere;

		}
		public int role_id()
		{
			return m_role_id;
		}
		public void role_id(int value)
		{
			z_bool.m_role_id = true;
			m_role_id = value;
			if (z_bool.m_role_id)
			{
				_zWhereClause += z_sep+"role_id='"+role_id() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_by()
		{
			return m_updt_by;
		}
		public void updt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
			if (z_bool.m_updt_by)
			{
				_zWhereClause += z_sep+"updt_by='"+updt_by() + "'";
				z_sep=" AND ";
			}
		}
		public String address()
		{
			return m_address;
		}
		public void address(String value)
		{
			z_bool.m_address = true;
			m_address = value;
			if (z_bool.m_address)
			{
				_zWhereClause += z_sep+"address='"+address() + "'";
				z_sep=" AND ";
			}
		}
		public String login_pwd()
		{
			return m_login_pwd;
		}
		public void login_pwd(String value)
		{
			z_bool.m_login_pwd = true;
			m_login_pwd = value;
			if (z_bool.m_login_pwd)
			{
				_zWhereClause += z_sep+"login_pwd='"+login_pwd() + "'";
				z_sep=" AND ";
			}
		}
		public int user_id()
		{
			return m_user_id;
		}
		public void user_id(int value)
		{
			z_bool.m_user_id = true;
			m_user_id = value;
			if (z_bool.m_user_id)
			{
				_zWhereClause += z_sep+"user_id='"+user_id() + "'";
				z_sep=" AND ";
			}
		}
		public int user_type()
		{
			return m_user_type;
		}
		public void user_type(int value)
		{
			z_bool.m_user_type = true;
			m_user_type = value;
			if (z_bool.m_user_type)
			{
				_zWhereClause += z_sep+"user_type='"+user_type() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_cnt()
		{
			return m_updt_cnt;
		}
		public void updt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
			if (z_bool.m_updt_cnt)
			{
				_zWhereClause += z_sep+"updt_cnt='"+updt_cnt() + "'";
				z_sep=" AND ";
			}
		}
		public String email()
		{
			return m_email;
		}
		public void email(String value)
		{
			z_bool.m_email = true;
			m_email = value;
			if (z_bool.m_email)
			{
				_zWhereClause += z_sep+"email='"+email() + "'";
				z_sep=" AND ";
			}
		}
		public String phone()
		{
			return m_phone;
		}
		public void phone(String value)
		{
			z_bool.m_phone = true;
			m_phone = value;
			if (z_bool.m_phone)
			{
				_zWhereClause += z_sep+"phone='"+phone() + "'";
				z_sep=" AND ";
			}
		}
		public String updt_dt()
		{
			return m_updt_dt;
		}
		public void updt_dt(String value)
		{
			z_bool.m_updt_dt = true;
			m_updt_dt = value;
			if (z_bool.m_updt_dt)
			{
				_zWhereClause += z_sep+"updt_dt='"+updt_dt() + "'";
				z_sep=" AND ";
			}
		}
		public int status()
		{
			return m_status;
		}
		public void status(int value)
		{
			z_bool.m_status = true;
			m_status = value;
			if (z_bool.m_status)
			{
				_zWhereClause += z_sep+"status='"+status() + "'";
				z_sep=" AND ";
			}
		}
		public String login_name()
		{
			return m_login_name;
		}
		public void login_name(String value)
		{
			z_bool.m_login_name = true;
			m_login_name = value;
			if (z_bool.m_login_name)
			{
				_zWhereClause += z_sep+"login_name='"+login_name() + "'";
				z_sep=" AND ";
			}
		}
		public String user_name()
		{
			return m_user_name;
		}
		public void user_name(String value)
		{
			z_bool.m_user_name = true;
			m_user_name = value;
			if (z_bool.m_user_name)
			{
				_zWhereClause += z_sep+"user_name='"+user_name() + "'";
				z_sep=" AND ";
			}
		}
		public String cell()
		{
			return m_cell;
		}
		public void cell(String value)
		{
			z_bool.m_cell = true;
			m_cell = value;
			if (z_bool.m_cell)
			{
				_zWhereClause += z_sep+"cell='"+cell() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}