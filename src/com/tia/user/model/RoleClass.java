	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : RoleClass.
	/// </summary>
package com.tia.user.model;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilFxn;




public class RoleClass {

	private static Connection cnn;
	private static Statement st;
	private ResultSet rs;
	private UtilFxn utlFxn=new UtilFxn();
	
	public RoleClass() {
		// db Connection
		RoleClass.cnn = DbConnection.getConnection();
	}
	
	public ResultSet getauthEntries() {
		String strQry = "SELECT * FROM mod_table_mcg";
		try {
			System.out.println("xx .. the role management QRY="+strQry);
				RoleClass.st = RoleClass.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_UPDATABLE);
				this.rs = st.executeQuery(strQry);
				if(UtilFxn.getRowCount(this.rs) > 0)
				{
			    System.out.println("xx ..");
				return rs;
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
			return null;
		}
	
	public ResultSet getauthDetails(String ModName) {
		String strQry = "SELECT * FROM authentication_mcg where auth_modules='"+ModName+"' ";
				
		try {
			System.out.println("xx .. the journal detail QRY="+strQry);
			RoleClass.st = RoleClass.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			this.rs = st.executeQuery(strQry);
			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


		
		

public boolean btnSaveActionPerformed(String subqry) {
	String strQry="";
	boolean retValue=false;
	 strQry="INSERT into role_auth_mcg (role_id,auth_id) VALUES "+subqry+" ";
	 try {
			cnn.setAutoCommit(false);
			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			
				if (st.executeUpdate(strQry) > 0) {
					retValue = true;
					System.out
							.println("The New Role with differnt authorities has been Created(Inserted into the user_accounts) !");
					cnn.commit();
				} else {
					cnn.rollback();
				}
			
			

		} catch (SQLException e) {
			retValue = false;
			e.printStackTrace();
		}
		return retValue;

//	try {
//		System.out.println("xx .. the save querry execute="+strQry);
//		this.st = this.cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//		if(st.executeUpdate(strQry)==1){
//		
//			return true;
//			
//		}
//		else{
//			
//			return false;
//		}
//		//return rs;
//
//	} catch (SQLException e) {
//		e.printStackTrace();
//		return false;
//	}
	//return true;

}

//public ResultSet getresults() {
//	String strQry = "SELECT auth_name,auth_modules,role FROM role_auth_mcg INNER JOIN user_role_mcg ON user_role_mcg.role_id = role_auth_mcg.role_id INNER JOIN authentication_mcg ON role_auth_mcg.auth_id = authentication_mcg.auth_id";
//	try {
//		System.out.println("xx .. the role management QRY="+strQry);
//			this.st = this.c.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
//					ResultSet.CONCUR_UPDATABLE);
//			this.rs = st.executeQuery(strQry);
//			if(cmnFxn.getRowCount(this.rs) > 0)
//			{
//		    System.out.println("xx done");
//			return rs;
//			}
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return rs;
//}

public ResultSet getselectedEntries(String namerole) {
	
	String strQry = "SELECT auth_name_display,auth_modules,role FROM role_auth_mcg INNER JOIN user_role_mcg ON user_role_mcg.role_id = role_auth_mcg.role_id INNER JOIN authentication_mcg ON role_auth_mcg.auth_id = authentication_mcg.auth_id where user_role_mcg.role_id='"+namerole+"' ";
	
	
	try {
		System.out.println("xx .. the journal detail QRY="+strQry);
		RoleClass.st = RoleClass.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_UPDATABLE);
		this.rs = st.executeQuery(strQry);
		return rs;

	} catch (SQLException e) {
		e.printStackTrace();
	}
	return null;
}

public boolean checkifPresent(String role) {

	String strQry = "SELECT * FROM user_role_mcg where  role='"+role+"' ";
	try {
		System.out.println("xx .. the journal detail QRY="+strQry);
		RoleClass.st = RoleClass.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_UPDATABLE);
		this.rs = st.executeQuery(strQry);
		if(UtilFxn.getRowCount(this.rs) > 0)
		{
	    System.out.println("xx done");
		return true;
		}
		else
		{
		return false;
		}
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return false;
	
}

public boolean getEntries(String text, String ii) {
	String strQry = "SELECT * FROM role_auth_mcg where  role_id="+text+" and auth_id="+ii+"";
	try {
		System.out.println("xx .. the tree check  QRY="+strQry);
		RoleClass.st = RoleClass.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_UPDATABLE);
		this.rs = st.executeQuery(strQry);
		if(UtilFxn.getRowCount(this.rs) > 0)
		{
	    System.out.println("check done");
		return true;
		}
		else
			return false;
	
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	return false;
}

public static boolean deletetablrole(String namerole) {
	boolean retValue = false;
String strQry="DELETE from role_auth_mcg where role_id='"+namerole+"' ";
try {
	cnn.setAutoCommit(false);
	st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
			ResultSet.CONCUR_UPDATABLE);
	
		if (st.executeUpdate(strQry) > 0) {
			System.out.println("The role with role_id "
					+ namerole + "  has been Deleted from DB!");
			retValue=true;
			cnn.commit();
		} else {
			cnn.rollback();
		}
	

} catch (SQLException e) {
	retValue = false;

	e.printStackTrace();
}
return retValue;


//		try {
//		
//			System.out.println("xx .. the delete execute="+strQry);
//			st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//	          int res = st.executeUpdate(strQry);	          
//		
//			if(st.executeUpdate(strQry)>0){
//				
//				return true; 
//			}
//			else{
//				return false;}
//			//return rs;
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return true;

}


	


public boolean btnSaveActionRole(String textrole) {
	boolean retValue = false;

	String strQry="INSERT into user_role_mcg (role) VALUES ('"+textrole+"') ";
	try {
		// st = cnn.createStatement(); //such statement is used for select
		// Qyery
		cnn.setAutoCommit(false);
		st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_UPDATABLE);
		
			if (st.executeUpdate(strQry) > 0) {
				retValue = true;
				System.out
						.println("The New Role with differnt authorities has been Created(Inserted into the user_accounts) !");
				cnn.commit();
			} else {
				cnn.rollback();
			}
		

	} catch (SQLException e) {
		retValue = false;
		e.printStackTrace();
	}
	return retValue;

	
//	try {
//		System.out.println("xx .. the save querry execute="+strQry);
//		this.st = this.cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//		if(st.executeUpdate(strQry)==1){
//		
//			return true;
//			
//		}
//		else
//			
//			return false;
//		//return rs;
//
//	} catch (SQLException e) {
//		e.printStackTrace();
//		return false;
//	}
}

public ResultSet btnSelectIdRole(String textrole) {
	String strQry = "SELECT role_id FROM user_role_mcg where  role='"+textrole+"'";
	try {
		System.out.println("xx .. the role id from name check  QRY="+strQry);
		RoleClass.st = RoleClass.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_UPDATABLE);
		this.rs = st.executeQuery(strQry);
		if(UtilFxn.getRowCount(this.rs) > 0)
		{
	    System.out.println("check done");
	    return rs;
		}else
			return null;
	
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	
	return rs;
}
public String getAuth_id(String name) {
	String strQry = "SELECT auth_id FROM authentication_mcg where auth_name_display='"+name+"' ";
	try {
		System.out.println("xx .. the role management QRY="+strQry);
			RoleClass.st = RoleClass.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			this.rs = st.executeQuery(strQry);
			if(UtilFxn.getRowCount(this.rs) > 0)
			{
				if(rs.next())
				{
		    System.out.println("xx ..");
			return rs.getString("auth_id");
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
}

public void updateRoleName(String txtEditRole, String namerole) {
	String strQry="UPDATE user_role_mcg SET role=('"+txtEditRole+"')WHERE role_id=('"+namerole+"') ";
	try {
		cnn.setAutoCommit(false);
		st = cnn.createStatement();
		
			if (st.executeUpdate(strQry) > 0) {
				System.out.println("The role with different authorities  has been updated !");
				cnn.commit();
			} else {
				cnn.rollback();
			}
		

	} catch (SQLException e) {
		e.printStackTrace();
	}

//	try {
//		System.out.println("xx .. the update role execute="+strQry);
//		this.st = this.cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//		if(st.executeUpdate(strQry)==1){
//			System.out.println("updated");
//			return;
//			
//		}
//		else
//			
//			return;
//		//return rs;
//
//	} catch (SQLException e) {
//		e.printStackTrace();
//		return;
//	}

}	
	


public boolean checkActionRole(String txtEditRole) {
	boolean res=false;
	String strQry = "SELECT role_id FROM user_role_mcg where role='"+txtEditRole+"' ";
	try {
		System.out.println("xx .. the role management QRY="+strQry);
			RoleClass.st = RoleClass.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			this.rs = st.executeQuery(strQry);
			if(UtilFxn.getRowCount(this.rs) > 0)
			{
		    System.out.println("xx ..");
			res=true;
			}
			else{
				res=false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
}

public static void deleterole(String namerole) {
	String strQry="DELETE from user_role_mcg where role_id='"+namerole+"' ";
	
	try {
		cnn.setAutoCommit(false);
		st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_UPDATABLE);
		
			if (st.executeUpdate(strQry) > 0) {
				System.out.println("The role with role_id "
						+ namerole + "  has been Deleted from DB!");
				cnn.commit();
			} else {
				cnn.rollback();
			}
		

	} catch (SQLException e) {
		e.printStackTrace();
	}
	// TODO Auto-generated method stub
//	try {
//		
//		System.out.println("xx .. the delete execute="+strQry);
//		 st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//          int res = st.executeUpdate(strQry);	          
//	
//          if(res == 1){
//        	  System.out.println("role is deleted.");
//        	  }
//        	  else{
//        	  System.out.println("role is not deleted.");
//        	  }
//		//return rs;
//
//	} catch (SQLException e) {
//		e.printStackTrace();
//	}
	
	return;
}

public ResultSet getnames() {
	String strQry ="select role from user_role_mcg";
try {
	System.out.println("The different roles are::" + strQry);
	 st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
			ResultSet.CONCUR_UPDATABLE);
	ResultSet rs = st.executeQuery(strQry);
	if (rs.next())
		return rs;
} catch (SQLException e) {
	e.printStackTrace();
}
return null;
}

public String GetName(String txtEditRole,String nameRole) {
	// TODO Auto-generated method stub
	String strQry ="";
	if(nameRole.equals("")){
		 strQry ="select  count (*) as role from user_role_mcg where role='"+txtEditRole+"'";
	}
	else
		strQry ="select  count (*) as role from user_role_mcg where role='"+txtEditRole+"' and role_id<>'"+nameRole+"'";
	String messg="";
	try {System.out.println("query"+strQry);
		System.out.println("xx .. the role management QRY="+strQry);
			RoleClass.st = RoleClass.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			this.rs = st.executeQuery(strQry);
			if(rs.next())
			{	messg=rs.getString("role");
				System.out.println("hello");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("strQRRRRRy"+messg);
		return messg;
		
}




}	


	
	
	