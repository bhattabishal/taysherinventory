	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : RoleAuthClass.
	/// </summary>
package com.tia.user.model;
	public class RoleAuthClass
	{
		private int m_auth_id;
		private int m_role_auth_id;
		private int m_role_id;
		private RoleAuthClass_Criteria z_WhereClause;

		private IsDirty_RoleAuthClass z_bool;

		public RoleAuthClass()
		{
			z_WhereClause = new RoleAuthClass_Criteria();
			z_bool = new IsDirty_RoleAuthClass();
		}
		public class IsDirty_RoleAuthClass
		{
			public boolean  m_auth_id;
			public boolean  m_role_auth_id;
			public boolean  m_role_id;
		}
		public int getAuth_id()
		{
			return m_auth_id;
		}
		public void setAuth_id(int value)
		{
			z_bool.m_auth_id = true;
			m_auth_id = value;
		}
		public int getRole_auth_id()
		{
			return m_role_auth_id;
		}
		public void setRole_auth_id(int value)
		{
			z_bool.m_role_auth_id = true;
			m_role_auth_id = value;
		}
		public int getRole_id()
		{
			return m_role_id;
		}
		public void setRole_id(int value)
		{
			z_bool.m_role_id = true;
			m_role_id = value;
		}
		/*
		public RoleAuthClass_Criteria Where()
		{
				return z_WhereClause;
			}
		public RoleAuthClass_Criteria Where(RoleAuthClass_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO role_auth_mcg ( ";
			if (z_bool.m_auth_id)
			{
				SQL+= z_sep +"auth_id";
				z_sep=" , ";
				}
			if (z_bool.m_role_auth_id)
			{
				SQL+= z_sep +"role_auth_id";
				z_sep=" , ";
				}
			if (z_bool.m_role_id)
			{
				SQL+= z_sep +"role_id";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_auth_id)
			{
				SQL+= z_sep +"'" + m_auth_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_role_auth_id)
			{
				SQL+= z_sep +"'" + m_role_auth_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_role_id)
			{
				SQL+= z_sep +"'" + m_role_id + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE role_auth_mcg SET ";
			if (z_bool.m_auth_id)
			{
				SQL+= z_sep +"auth_id='" + m_auth_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_role_auth_id)
			{
				SQL+= z_sep +"role_auth_id='" + m_role_auth_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_role_id)
			{
				SQL+= z_sep +"role_id='" + m_role_id + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_auth_id)
			{
				SQL+= z_sep +"auth_id='" + z_WhereClause.auth_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_role_auth_id)
			{
				SQL+= z_sep +"role_auth_id='" + z_WhereClause.role_auth_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_role_id)
			{
				SQL+= z_sep +"role_id='" + z_WhereClause.role_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM role_auth_mcg";
			if (z_WhereClause.z_bool.m_auth_id)
			{
				SQL+= z_sep +"auth_id='" + z_WhereClause.auth_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_role_auth_id)
			{
				SQL+= z_sep +"role_auth_id='" + z_WhereClause.role_auth_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_role_id)
			{
				SQL+= z_sep +"role_id='" + z_WhereClause.role_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM role_auth_mcg";
			if (z_WhereClause.z_bool.m_auth_id)
			{
				SQL+= z_sep +"auth_id='" + z_WhereClause.auth_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_role_auth_id)
			{
				SQL+= z_sep +"role_auth_id='" + z_WhereClause.role_auth_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_role_id)
			{
				SQL+= z_sep +"role_id='" + z_WhereClause.role_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : RoleAuthClass_Criteria.
	/// </summary>
	public class RoleAuthClass_Criteria 
	{
		private int m_auth_id;
		private int m_role_auth_id;
		private int m_role_id;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_RoleAuthClass_Criteria  z_bool;

		public RoleAuthClass_Criteria()

		{
		z_bool=new IsDirty_RoleAuthClass_Criteria();  
		}
		public class IsDirty_RoleAuthClass_Criteria 
		{
			public boolean  m_auth_id;
			public boolean  m_role_auth_id;
			public boolean  m_role_id;
			public boolean  MyWhere;

		}
		public int auth_id()
		{
			return m_auth_id;
		}
		public void auth_id(int value)
		{
			z_bool.m_auth_id = true;
			m_auth_id = value;
			if (z_bool.m_auth_id)
			{
				_zWhereClause += z_sep+"auth_id='"+auth_id() + "'";
				z_sep=" AND ";
			}
		}
		public int role_auth_id()
		{
			return m_role_auth_id;
		}
		public void role_auth_id(int value)
		{
			z_bool.m_role_auth_id = true;
			m_role_auth_id = value;
			if (z_bool.m_role_auth_id)
			{
				_zWhereClause += z_sep+"role_auth_id='"+role_auth_id() + "'";
				z_sep=" AND ";
			}
		}
		public int role_id()
		{
			return m_role_id;
		}
		public void role_id(int value)
		{
			z_bool.m_role_id = true;
			m_role_id = value;
			if (z_bool.m_role_id)
			{
				_zWhereClause += z_sep+"role_id='"+role_id() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}