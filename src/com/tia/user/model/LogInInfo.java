	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : LogInInfo.
	/// </summary>
package com.tia.user.model;
	public class LogInInfo
	{
		private String m_updated_date;
		private String m_log_in_time;
		private int m_br_id;
		private int m_created_by;
		private String m_created_date;
		private int m_user_id;
		private int m_update_count;
		private String m_log_out_time;
		private int m_log_in_info_id;
		private int m_updated_by;
		public LogInInfo_Criteria z_WhereClause;

		private IsDirty_LogInInfo z_bool;

		public LogInInfo()
		{
			z_WhereClause = new LogInInfo_Criteria();
			z_bool = new IsDirty_LogInInfo();
		}
		public class IsDirty_LogInInfo
		{
			public boolean  m_updated_date;
			public boolean  m_log_in_time;
			public boolean  m_br_id;
			public boolean  m_created_by;
			public boolean  m_created_date;
			public boolean  m_user_id;
			public boolean  m_update_count;
			public boolean  m_log_out_time;
			public boolean  m_log_in_info_id;
			public boolean  m_updated_by;
		}
		public String getUpdated_date()
		{
			return m_updated_date;
		}
		public void setUpdated_date(String value)
		{
			z_bool.m_updated_date = true;
			m_updated_date = value;
		}
		public String getLog_in_time()
		{
			return m_log_in_time;
		}
		public void setLog_in_time(String value)
		{
			z_bool.m_log_in_time = true;
			m_log_in_time = value;
		}
		public int getBr_id()
		{
			return m_br_id;
		}
		public void setBr_id(int value)
		{
			z_bool.m_br_id = true;
			m_br_id = value;
		}
		public int getCreated_by()
		{
			return m_created_by;
		}
		public void setCreated_by(int value)
		{
			z_bool.m_created_by = true;
			m_created_by = value;
		}
		public String getCreated_date()
		{
			return m_created_date;
		}
		public void setCreated_date(String value)
		{
			z_bool.m_created_date = true;
			m_created_date = value;
		}
		public int getUser_id()
		{
			return m_user_id;
		}
		public void setUser_id(int value)
		{
			z_bool.m_user_id = true;
			m_user_id = value;
		}
		public int getUpdate_count()
		{
			return m_update_count;
		}
		public void setUpdate_count(int value)
		{
			z_bool.m_update_count = true;
			m_update_count = value;
		}
		public String getLog_out_time()
		{
			return m_log_out_time;
		}
		public void setLog_out_time(String value)
		{
			z_bool.m_log_out_time = true;
			m_log_out_time = value;
		}
		public int getLog_in_info_id()
		{
			return m_log_in_info_id;
		}
		public void setLog_in_info_id(int value)
		{
			z_bool.m_log_in_info_id = true;
			m_log_in_info_id = value;
		}
		public int getUpdated_by()
		{
			return m_updated_by;
		}
		public void setUpdated_by(int value)
		{
			z_bool.m_updated_by = true;
			m_updated_by = value;
		}
		/*
		public LogInInfo_Criteria Where()
		{
				return z_WhereClause;
			}
		public LogInInfo_Criteria Where(LogInInfo_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			String z_sep ="";
			String SQL = "INSERT INTO log_in_info_mcg ( ";
			if (z_bool.m_updated_date)
			{
				SQL+= z_sep +"updated_date";
				z_sep=" , ";
				}
			if (z_bool.m_log_in_time)
			{
				SQL+= z_sep +"log_in_time";
				z_sep=" , ";
				}
			if (z_bool.m_br_id)
			{
				SQL+= z_sep +"br_id";
				z_sep=" , ";
				}
			if (z_bool.m_created_by)
			{
				SQL+= z_sep +"created_by";
				z_sep=" , ";
				}
			if (z_bool.m_created_date)
			{
				SQL+= z_sep +"created_date";
				z_sep=" , ";
				}
			if (z_bool.m_user_id)
			{
				SQL+= z_sep +"user_id";
				z_sep=" , ";
				}
			if (z_bool.m_update_count)
			{
				SQL+= z_sep +"update_count";
				z_sep=" , ";
				}
			if (z_bool.m_log_out_time)
			{
				SQL+= z_sep +"log_out_time";
				z_sep=" , ";
				}
			if (z_bool.m_log_in_info_id)
			{
				SQL+= z_sep +"log_in_info_id";
				z_sep=" , ";
				}
			if (z_bool.m_updated_by)
			{
				SQL+= z_sep +"updated_by";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_updated_date)
			{
				SQL+= z_sep +"'" + m_updated_date + "'";
				z_sep=" , ";
				}
			if (z_bool.m_log_in_time)
			{
				SQL+= z_sep +"'" + m_log_in_time + "'";
				z_sep=" , ";
				}
			if (z_bool.m_br_id)
			{
				SQL+= z_sep +"'" + m_br_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_created_by)
			{
				SQL+= z_sep +"'" + m_created_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_created_date)
			{
				SQL+= z_sep +"'" + m_created_date + "'";
				z_sep=" , ";
				}
			if (z_bool.m_user_id)
			{
				SQL+= z_sep +"'" + m_user_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_update_count)
			{
				SQL+= z_sep +"" + m_update_count + "";
				z_sep=" , ";
				}
			if (z_bool.m_log_out_time)
			{
				SQL+= z_sep +"'" + m_log_out_time + "'";
				z_sep=" , ";
				}
			if (z_bool.m_log_in_info_id)
			{
				SQL+= z_sep +"'" + m_log_in_info_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updated_by)
			{
				SQL+= z_sep +"'" + m_updated_by + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE log_in_info_mcg SET ";
			if (z_bool.m_updated_date)
			{
				SQL+= z_sep +"updated_date='" + m_updated_date + "'";
				z_sep=" , ";
				}
			if (z_bool.m_log_in_time)
			{
				SQL+= z_sep +"log_in_time='" + m_log_in_time + "'";
				z_sep=" , ";
				}
			if (z_bool.m_br_id)
			{
				SQL+= z_sep +"br_id='" + m_br_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_created_by)
			{
				SQL+= z_sep +"created_by='" + m_created_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_created_date)
			{
				SQL+= z_sep +"created_date='" + m_created_date + "'";
				z_sep=" , ";
				}
			if (z_bool.m_user_id)
			{
				SQL+= z_sep +"user_id='" + m_user_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_update_count)
			{
				SQL+= z_sep +"update_count='" + m_update_count + "'";
				z_sep=" , ";
				}
			if (z_bool.m_log_out_time)
			{
				SQL+= z_sep +"log_out_time='" + m_log_out_time + "'";
				z_sep=" , ";
				}
			if (z_bool.m_log_in_info_id)
			{
				SQL+= z_sep +"log_in_info_id='" + m_log_in_info_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updated_by)
			{
				SQL+= z_sep +"updated_by='" + m_updated_by + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_updated_date)
			{
				SQL+= z_sep +"updated_date='" + z_WhereClause.updated_date() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_log_in_time)
			{
				SQL+= z_sep +"log_in_time='" + z_WhereClause.log_in_time() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_br_id)
			{
				SQL+= z_sep +"br_id='" + z_WhereClause.br_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_by)
			{
				SQL+= z_sep +"created_by='" + z_WhereClause.created_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_date)
			{
				SQL+= z_sep +"created_date='" + z_WhereClause.created_date() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_user_id)
			{
				SQL+= z_sep +"user_id='" + z_WhereClause.user_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_update_count)
			{
				SQL+= z_sep +"update_count='" + z_WhereClause.update_count() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_log_out_time)
			{
				SQL+= z_sep +"log_out_time='" + z_WhereClause.log_out_time() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_log_in_info_id)
			{
				SQL+= z_sep +"log_in_info_id='" + z_WhereClause.log_in_info_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updated_by)
			{
				SQL+= z_sep +"updated_by='" + z_WhereClause.updated_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM log_in_info_mcg";
			if (z_WhereClause.z_bool.m_updated_date)
			{
				SQL+= z_sep +"updated_date='" + z_WhereClause.updated_date() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_log_in_time)
			{
				SQL+= z_sep +"log_in_time='" + z_WhereClause.log_in_time() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_br_id)
			{
				SQL+= z_sep +"br_id='" + z_WhereClause.br_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_by)
			{
				SQL+= z_sep +"created_by='" + z_WhereClause.created_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_date)
			{
				SQL+= z_sep +"created_date='" + z_WhereClause.created_date() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_user_id)
			{
				SQL+= z_sep +"user_id='" + z_WhereClause.user_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_update_count)
			{
				SQL+= z_sep +"update_count='" + z_WhereClause.update_count() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_log_out_time)
			{
				SQL+= z_sep +"log_out_time='" + z_WhereClause.log_out_time() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_log_in_info_id)
			{
				SQL+= z_sep +"log_in_info_id='" + z_WhereClause.log_in_info_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updated_by)
			{
				SQL+= z_sep +"updated_by='" + z_WhereClause.updated_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM log_in_info_mcg";
			if (z_WhereClause.z_bool.m_updated_date)
			{
				SQL+= z_sep +"updated_date='" + z_WhereClause.updated_date() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_log_in_time)
			{
				SQL+= z_sep +"log_in_time='" + z_WhereClause.log_in_time() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_br_id)
			{
				SQL+= z_sep +"br_id='" + z_WhereClause.br_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_by)
			{
				SQL+= z_sep +"created_by='" + z_WhereClause.created_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_date)
			{
				SQL+= z_sep +"created_date='" + z_WhereClause.created_date() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_user_id)
			{
				SQL+= z_sep +"user_id='" + z_WhereClause.user_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_update_count)
			{
				SQL+= z_sep +"update_count='" + z_WhereClause.update_count() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_log_out_time)
			{
				SQL+= z_sep +"log_out_time='" + z_WhereClause.log_out_time() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_log_in_info_id)
			{
				SQL+= z_sep +"log_in_info_id='" + z_WhereClause.log_in_info_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updated_by)
			{
				SQL+= z_sep +"updated_by='" + z_WhereClause.updated_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : LogInInfo_Criteria.
	/// </summary>
	public class LogInInfo_Criteria 
	{
		private String m_updated_date;
		private String m_log_in_time;
		private int m_br_id;
		private int m_created_by;
		private String m_created_date;
		private int m_user_id;
		private int m_update_count;
		private String m_log_out_time;
		private int m_log_in_info_id;
		private int m_updated_by;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_LogInInfo_Criteria  z_bool;

		public LogInInfo_Criteria()

		{
		z_bool=new IsDirty_LogInInfo_Criteria();  
		}
		public class IsDirty_LogInInfo_Criteria 
		{
			public boolean  m_updated_date;
			public boolean  m_log_in_time;
			public boolean  m_br_id;
			public boolean  m_created_by;
			public boolean  m_created_date;
			public boolean  m_user_id;
			public boolean  m_update_count;
			public boolean  m_log_out_time;
			public boolean  m_log_in_info_id;
			public boolean  m_updated_by;
			public boolean  MyWhere;

		}
		public String updated_date()
		{
			return m_updated_date;
		}
		public void updated_date(String value)
		{
			z_bool.m_updated_date = true;
			m_updated_date = value;
			if (z_bool.m_updated_date)
			{
				_zWhereClause += z_sep+"updated_date='"+updated_date() + "'";
				z_sep=" AND ";
			}
		}
		public String log_in_time()
		{
			return m_log_in_time;
		}
		public void log_in_time(String value)
		{
			z_bool.m_log_in_time = true;
			m_log_in_time = value;
			if (z_bool.m_log_in_time)
			{
				_zWhereClause += z_sep+"log_in_time='"+log_in_time() + "'";
				z_sep=" AND ";
			}
		}
		public int br_id()
		{
			return m_br_id;
		}
		public void br_id(int value)
		{
			z_bool.m_br_id = true;
			m_br_id = value;
			if (z_bool.m_br_id)
			{
				_zWhereClause += z_sep+"br_id='"+br_id() + "'";
				z_sep=" AND ";
			}
		}
		public int created_by()
		{
			return m_created_by;
		}
		public void created_by(int value)
		{
			z_bool.m_created_by = true;
			m_created_by = value;
			if (z_bool.m_created_by)
			{
				_zWhereClause += z_sep+"created_by='"+created_by() + "'";
				z_sep=" AND ";
			}
		}
		public String created_date()
		{
			return m_created_date;
		}
		public void created_date(String value)
		{
			z_bool.m_created_date = true;
			m_created_date = value;
			if (z_bool.m_created_date)
			{
				_zWhereClause += z_sep+"created_date='"+created_date() + "'";
				z_sep=" AND ";
			}
		}
		public int user_id()
		{
			return m_user_id;
		}
		public void user_id(int value)
		{
			z_bool.m_user_id = true;
			m_user_id = value;
			if (z_bool.m_user_id)
			{
				_zWhereClause += z_sep+"user_id='"+user_id() + "'";
				z_sep=" AND ";
			}
		}
		public int update_count()
		{
			return m_update_count;
		}
		public void update_count(int value)
		{
			z_bool.m_update_count = true;
			m_update_count = value;
			if (z_bool.m_update_count)
			{
				_zWhereClause += z_sep+"update_count='"+update_count() + "'";
				z_sep=" AND ";
			}
		}
		public String log_out_time()
		{
			return m_log_out_time;
		}
		public void log_out_time(String value)
		{
			z_bool.m_log_out_time = true;
			m_log_out_time = value;
			if (z_bool.m_log_out_time)
			{
				_zWhereClause += z_sep+"log_out_time='"+log_out_time() + "'";
				z_sep=" AND ";
			}
		}
		public int log_in_info_id()
		{
			return m_log_in_info_id;
		}
		public void log_in_info_id(int value)
		{
			z_bool.m_log_in_info_id = true;
			m_log_in_info_id = value;
			if (z_bool.m_log_in_info_id)
			{
				_zWhereClause += z_sep+"log_in_info_id='"+log_in_info_id() + "'";
				z_sep=" AND ";
			}
		}
		public int updated_by()
		{
			return m_updated_by;
		}
		public void updated_by(int value)
		{
			z_bool.m_updated_by = true;
			m_updated_by = value;
			if (z_bool.m_updated_by)
			{
				_zWhereClause += z_sep+"updated_by='"+updated_by() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}