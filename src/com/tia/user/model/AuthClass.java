	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : AuthClass.
	/// </summary>
package com.tia.user.model;
	public class AuthClass
	{
		private int m_auth_id;
		private String m_auth_modules;
		private String m_auth_name;
		private AuthClass_Criteria z_WhereClause;

		private IsDirty_AuthClass z_bool;

		public AuthClass()
		{
			z_WhereClause = new AuthClass_Criteria();
			z_bool = new IsDirty_AuthClass();
		}
		public class IsDirty_AuthClass
		{
			public boolean  m_auth_id;
			public boolean  m_auth_modules;
			public boolean  m_auth_name;
		}
		public int getAuth_id()
		{
			return m_auth_id;
		}
		public void setAuth_id(int value)
		{
			z_bool.m_auth_id = true;
			m_auth_id = value;
		}
		public String getAuth_modules()
		{
			return m_auth_modules;
		}
		public void setAuth_modules(String value)
		{
			z_bool.m_auth_modules = true;
			m_auth_modules = value;
		}
		public String getAuth_name()
		{
			return m_auth_name;
		}
		public void setAuth_name(String value)
		{
			z_bool.m_auth_name = true;
			m_auth_name = value;
		}
		/*
		public AuthClass_Criteria Where()
		{
				return z_WhereClause;
			}
		public AuthClass_Criteria Where(AuthClass_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO authentication_mcg ( ";
			if (z_bool.m_auth_id)
			{
				SQL+= z_sep +"auth_id";
				z_sep=" , ";
				}
			if (z_bool.m_auth_modules)
			{
				SQL+= z_sep +"auth_modules";
				z_sep=" , ";
				}
			if (z_bool.m_auth_name)
			{
				SQL+= z_sep +"auth_name";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_auth_id)
			{
				SQL+= z_sep +"'" + m_auth_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_auth_modules)
			{
				SQL+= z_sep +"'" + m_auth_modules + "'";
				z_sep=" , ";
				}
			if (z_bool.m_auth_name)
			{
				SQL+= z_sep +"'" + m_auth_name + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE authentication_mcg SET ";
			if (z_bool.m_auth_id)
			{
				SQL+= z_sep +"auth_id='" + m_auth_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_auth_modules)
			{
				SQL+= z_sep +"auth_modules='" + m_auth_modules + "'";
				z_sep=" , ";
				}
			if (z_bool.m_auth_name)
			{
				SQL+= z_sep +"auth_name='" + m_auth_name + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_auth_id)
			{
				SQL+= z_sep +"auth_id='" + z_WhereClause.auth_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_auth_modules)
			{
				SQL+= z_sep +"auth_modules='" + z_WhereClause.auth_modules() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_auth_name)
			{
				SQL+= z_sep +"auth_name='" + z_WhereClause.auth_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM authentication_mcg";
			if (z_WhereClause.z_bool.m_auth_id)
			{
				SQL+= z_sep +"auth_id='" + z_WhereClause.auth_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_auth_modules)
			{
				SQL+= z_sep +"auth_modules='" + z_WhereClause.auth_modules() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_auth_name)
			{
				SQL+= z_sep +"auth_name='" + z_WhereClause.auth_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM authentication_mcg";
			if (z_WhereClause.z_bool.m_auth_id)
			{
				SQL+= z_sep +"auth_id='" + z_WhereClause.auth_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_auth_modules)
			{
				SQL+= z_sep +"auth_modules='" + z_WhereClause.auth_modules() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_auth_name)
			{
				SQL+= z_sep +"auth_name='" + z_WhereClause.auth_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : AuthClass_Criteria.
	/// </summary>
	public class AuthClass_Criteria 
	{
		private int m_auth_id;
		private String m_auth_modules;
		private String m_auth_name;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_AuthClass_Criteria  z_bool;

		public AuthClass_Criteria()

		{
		z_bool=new IsDirty_AuthClass_Criteria();  
		}
		public class IsDirty_AuthClass_Criteria 
		{
			public boolean  m_auth_id;
			public boolean  m_auth_modules;
			public boolean  m_auth_name;
			public boolean  MyWhere;

		}
		public int auth_id()
		{
			return m_auth_id;
		}
		public void auth_id(int value)
		{
			z_bool.m_auth_id = true;
			m_auth_id = value;
			if (z_bool.m_auth_id)
			{
				_zWhereClause += z_sep+"auth_id='"+auth_id() + "'";
				z_sep=" AND ";
			}
		}
		public String auth_modules()
		{
			return m_auth_modules;
		}
		public void auth_modules(String value)
		{
			z_bool.m_auth_modules = true;
			m_auth_modules = value;
			if (z_bool.m_auth_modules)
			{
				_zWhereClause += z_sep+"auth_modules='"+auth_modules() + "'";
				z_sep=" AND ";
			}
		}
		public String auth_name()
		{
			return m_auth_name;
		}
		public void auth_name(String value)
		{
			z_bool.m_auth_name = true;
			m_auth_name = value;
			if (z_bool.m_auth_name)
			{
				_zWhereClause += z_sep+"auth_name='"+auth_name() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}