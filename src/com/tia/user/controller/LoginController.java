package com.tia.user.controller;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;








import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.db.DbConnection;
import com.tia.common.util.LogFileCreate;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import com.tia.user.model.LogInInfo;



public class LoginController {
	private Connection cnn;
	private Statement st;
	private ResultSet rs;
	private final JCalendarFunctions jCal = new JCalendarFunctions();


	public LoginController() {
		cnn = DbConnection.getConnection();
		rs = null;
		

	}

	public boolean LogOutUser() {
		boolean flag = false;
		Connection con = DbConnection.getConnection();
		LogInInfo lii = new LogInInfo();
		String undoId = UtilFxn.GetValueFromTable(
				"log_in_info_mcg",
				"log_in_info_id",
				"log_in_info_id = (SELECT MAX(log_in_info_id) FROM log_in_info_mcg "
						+ "WHERE log_in_info_id = (SELECT MAX(log_in_info_id) "
						+ "FROM log_in_info_mcg WHERE user_id = '"
						+ ApplicationWorkbenchWindowAdvisor.getLoginId()
						+ "' AND br_id = '"
						+ 1
						+ "'))");
		int upCnt = Integer.parseInt(UtilFxn.GetValueFromTable(
				"log_in_info_mcg", "update_count", "log_in_info_id = '"
						+ undoId + "'")) + 1;

		lii.setLog_out_time(jCal.getCurrentEnglishDateTime(""));
		lii.setUpdated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		lii.setUpdated_date(JCalendarFunctions.currentDate());
		lii.setUpdate_count(upCnt);
		lii.z_WhereClause
				.user_id(ApplicationWorkbenchWindowAdvisor.getLoginId());
		lii.z_WhereClause
				.br_id(1);

		String strQry = lii.Update() + " AND ISNULL(log_out_time, 0) = 0";

		try {
			con.setAutoCommit(false);
			Statement st = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			if (st.executeUpdate(strQry) > 0
					) {
				LogFileCreate.logAtLogout();
				con.commit();
				flag = true;
			}
			con.setAutoCommit(true);
		} catch (SQLException ex) {
			System.out.println(ex.toString());
		}

		return flag;
	}

	public boolean LogOutUser(Connection cn) {
		boolean flag = false;
		LogInInfo lii = new LogInInfo();
		String undoId = UtilFxn.GetValueFromTable(
				"log_in_info_mcg",
				"log_in_info_id",
				"log_in_info_id = (SELECT MAX(log_in_info_id) FROM log_in_info_mcg "
						+ "WHERE log_in_info_id = (SELECT MAX(log_in_info_id) "
						+ "FROM log_in_info_mcg WHERE user_id = '"
						+ ApplicationWorkbenchWindowAdvisor.getLoginId()
						+ "' AND br_id = '"
						+ 1
						+ "'))");
		int upCnt = Integer.parseInt(UtilFxn.GetValueFromTable(
				"log_in_info_mcg", "update_count", "log_in_info_id = '"
						+ undoId + "'")) + 1;

		lii.setLog_out_time(jCal.getCurrentEnglishDateTime(""));
		lii.setUpdated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		lii.setUpdated_date(JCalendarFunctions.currentDate());
		lii.setUpdate_count(upCnt);
		lii.z_WhereClause
				.user_id(ApplicationWorkbenchWindowAdvisor.getLoginId());
		lii.z_WhereClause
				.br_id(1);

		String strQry = lii.Update() + " AND ISNULL(log_out_time, 0) = 0";

		try {
			Statement st = cn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			if (st.executeUpdate(strQry) > 0
					) {
				LogFileCreate.logAtLogout();
				flag = true;
			}
		} catch (SQLException ex) {
			System.out.println(ex.toString());
		}

		return flag;
	}
	public boolean SaveUserData(String user) {
		boolean flag = false;
		JCalendarFunctions jCal = new JCalendarFunctions();
		LogInInfo lii = new LogInInfo();

		lii.setLog_in_info_id(UtilMethods.getMaxId("log_in_info_mcg",
				"log_in_info_id"));
		lii.setBr_id(1);
		lii.setUser_id(Integer.parseInt(user));
		lii.setLog_in_time(jCal.getCurrentEnglishDateTime(""));
		lii.setCreated_by(lii.getUser_id());
		lii.setCreated_date(JCalendarFunctions.currentDate());
		lii.setUpdated_by(lii.getUser_id());
		lii.setUpdated_date(JCalendarFunctions.currentDate());
		lii.setUpdate_count(0);

		String strQry = lii.Insert();

		try {
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			if (st.executeUpdate(strQry) > 0) {
				flag = true;
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return flag;
	}
	public boolean HasLoggedOn(String user) {
		boolean flag = false;
		String strQry = "SELECT COUNT(*) cnt FROM log_in_info_mcg WHERE user_id = '"
				+ user
				+ "' AND ISNULL(log_out_time, 0) = 0";

		try {
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			rs = st.executeQuery(strQry);

			if (rs.next()) {
				flag = rs.getInt("cnt") > 0 ? true : false;
			}
		} catch (SQLException ex) {
			System.out.println(ex.toString());
		}

		return flag;
	}

	/**
	 * This method checks the authenticity of a user with the supplied
	 * credentials.
	 * 
	 * @param username
	 *            (Username of the user to be authenticated.)
	 * @param password
	 *            (Password of the user to be authenticated.)
	 * 
	 * @boolean (true if authenticated, false otherwise.)
	 */
	public boolean isAuthenticated(String username, String password) {		
		
		String decryptpassword =encryptDectyptPassword.encryptPassword(password);
		
		String sql = "SELECT COUNT(*) FROM inven_user WHERE login_name = '"
				+ username + "' AND login_pwd = '" + decryptpassword + "' AND status=1" ;
		System.out.println(sql);
		int temp = 0;

		try {
			st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			rs = st.executeQuery(sql);

			while (rs.next()) {
				temp = rs.getInt(1);
			}
			
			if (temp >= 1)
				return true;
			else
				return false;

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return false;

	}
	public boolean UndoLogout() {
		boolean flag = false;
		String undoId = UtilFxn.GetValueFromTable(
				"log_in_info_mcg",
				"log_in_info_id",
				"log_in_info_id = (SELECT MAX(log_in_info_id) FROM log_in_info_mcg "
						+ "WHERE log_in_info_id=(SELECT MAX(log_in_info_id) "
						+ "FROM log_in_info_mcg WHERE user_id='"
						+ ApplicationWorkbenchWindowAdvisor.getLoginId()
						+ "' AND br_id='"
						+ 1
						+ "'))");

		String strQry = "UPDATE log_in_info_mcg SET updated_date='"
				+ jCal.currentDate()
				+ "' , update_count='"
				+ (Integer.parseInt(UtilFxn.GetValueFromTable(
						"log_in_info_mcg", "update_count", "log_in_info_id = '"
								+ undoId + "'")) + 1)
				+ "' , log_out_time=Null , updated_by='"
				+ ApplicationWorkbenchWindowAdvisor.getLoginId()
				+ "' WHERE log_in_info_id='" + undoId + "';";

		try {
			Statement st = cnn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			if (st.executeUpdate(strQry) > 0) {
				LogFileCreate.logAtLogin();
				flag = true;
			}
		} catch (SQLException ex) {
			System.out.println(ex.toString());
		}

		return flag;
	}

	public ResultSet GetAuthnames(int role) {
		String strQry = "SELECT authentication_mcg.auth_name FROM authentication_mcg INNER JOIN role_auth_mcg ON authentication_mcg.auth_id =role_auth_mcg.auth_id where role_auth_mcg.role_id="
				+ role + "";
		try {
			System.out.println("xx .. the role management QRY=" + strQry);
			this.st = this.cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			this.rs = st.executeQuery(strQry);

			System.out.println("xx ..");
			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	public String GetUserIdFromUserName(String user) {
		String str = UtilFxn.GetValueFromTable("inven_user", "user_id",
				"login_name='" + user + "';");

		return str == "" ? "-1" : str.trim();
	}
	public boolean toCheckAdminUser(String user) {
		boolean flag = false;
		
		String role_id = UtilFxn.GetValueFromTable("inven_user", "user_type",
				"user_id = '" + user + "'");
		

		if (Integer.valueOf(role_id)==1) {
			flag = true;
		}
		return flag;

	}


}
