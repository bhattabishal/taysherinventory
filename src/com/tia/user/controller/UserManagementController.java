package com.tia.user.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.tia.common.db.DbConnection;


public class UserManagementController {
Connection cnn= DbConnection.getConnection();
	
	
	public ResultSet GetUserDetail() {
		String strQry ="select us.*,rol.role from inven_user as us inner join user_role_mcg as rol on us.role_id=rol.role_id";
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet rsUserDetail = st.executeQuery(strQry);
			if(rsUserDetail.next()){
			return rsUserDetail;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			return null;
		}
	public boolean DeleteUser(int userId){
		String strQry ="Delete  from inven_user where user_id='"+userId+"'";
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			st.executeUpdate(strQry);				
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
		
	}

}
