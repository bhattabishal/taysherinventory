package com.tia.user.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;



import org.eclipse.swt.widgets.TreeItem;

import com.tia.common.db.DbConnection;
import com.tia.user.model.RoleClass;
import com.tia.user.view.RoleManagement;

public class RoleController {
	private RoleManagement objRoleView;
	private RoleClass objRoleClass;
	private Connection cnn=DbConnection.getConnection();
	String authid;
	public static ResultSet data;
	boolean t;
	boolean j;

	boolean role;
	boolean flag;
	String subQry = "";
	public  RoleController(){
		objRoleClass = new RoleClass();
	}
	

	public void getCheckeditem(TreeItem item) {
		  boolean checked = item.getChecked();
          checkItems(item, checked);
          checkPath(item.getParentItem(), checked, false);
		
	}

	private void checkPath(TreeItem item, boolean checked, boolean grayed) {
		if (item == null)
			return;
		if (grayed) {
			checked = true;
		} else {
			int index = 0;
			TreeItem[] items = item.getItems();
			while (index < items.length) {
				TreeItem child = items[index];
				if (child.getGrayed() || checked != child.getChecked()) {
					checked = grayed = true;
					break;
				}
				index++;
			}
		}
		item.setChecked(checked);
		item.setGrayed(grayed);
		checkPath(item.getParentItem(), checked, grayed);
	}

	/*check the all child items of treeitem */
		public static void checkItems(TreeItem item, boolean checked) {
			item.setGrayed(false);
			item.setChecked(checked);
			TreeItem[] items = item.getItems();
			for (int i = 0; i < items.length; i++) {
				System.out.println(items.length);
		       checkItems(items[i], checked);
			}
			item.setExpanded(true);
		}

		public boolean saveAction(boolean flag, String namerole, String txtEditRole) {
		try {
			cnn.setAutoCommit(false);
			  boolean retValue=false;
			  System.out.println("txtEditRole_----------"+txtEditRole);
			  // EDIT CASE
				 if(flag==true)
				 {
				  //cnn.setAutoCommit(false);
			   // System.out.println("Insidedelete");
				RoleClass.deletetablrole(namerole);
				System.out.println(namerole);
				System.out.println(txtEditRole);
				String subQry = "";
				objRoleClass.updateRoleName(txtEditRole,namerole);
				TreeItem[] items= RoleManagement.treeAuthList.getItems();
			
				System.out.println("subqry after initializing to null:"+subQry);
					for(int i=0; i<items.length; i++)
					{
						System.out.println("This is Items "+items[i].getText(1));
						TreeItem a = items[i];
						TreeItem[] childItems = a.getItems();
						int i1=0;
						
						for(int j=0; j<childItems.length; j++)
						{	System.out.println("unchecked");
							System.out.println("child is::"+childItems[j].getText(2).replaceAll("'", "''"));
							System.out.println("length of child"+childItems[j].getText(2).length());
						if(a.getItem(j).getChecked())
						   {
							System.out.println("checked");
						   this.getauthid(childItems[j].getText(2).replaceAll("'", "''"));
						   System.out.println(" This is Sub Items from auth id get "+childItems[j].getText(2));
						   subQry += "('"+namerole+"','"+authid+"'),";	
						   System.out.println("i1"+i1);
						   i1++;
						   }
					  
						}
						 
					}
					
					 String Subqry= subQry.substring( 0, subQry.length() - 1 );
					 System.out.println("SubqryStringEdit"+Subqry);
					 retValue=storeauthid(Subqry,namerole);
					
				 }
				 //NEW SAVING CASE
				else if(flag==false)
						{
					      boolean role1=objRoleClass.checkActionRole(txtEditRole);
				          if(role1==false)
				          {
					      role=objRoleClass.btnSaveActionRole(txtEditRole);
				          if(role==true)
						   {					 
							  ResultSet rs1=objRoleClass.btnSelectIdRole(txtEditRole);
							
						      try 
						      {
						    	subQry="";
						      rs1.beforeFirst();
							    while(rs1.next())
								 {
							    	TreeItem[] items= RoleManagement.treeAuthList.getItems();
									for(int i=0; i<items.length; i++)
									{
										
										System.out.println("This is Items "+items[i].getText(1));
										TreeItem a = items[i];
										TreeItem[] childItems = a.getItems();
										
										for(int j=0; j<childItems.length; j++)
										{
											System.out.println("unchecked");
											System.out.println("child is::"+childItems[j].getText(2).replaceAll("'", "''"));
											System.out.println("length of child"+childItems[j].getText(2).length());
										if(a.getItem(j).getChecked())
										   {
											
										   this.getauthid(childItems[j].getText(2).replaceAll("'", "''"));
										  
										   subQry += "("+rs1.getString("role_id")+","+authid+"),";		
										   System.out.println(" This is Sub Items New "+childItems[j].getText(2));
										   System.out.println(" This is Sub Items New "+subQry);
										   }
										}
										 
									}
									 String Subqry= subQry.substring( 0, subQry.length()-1);
									 System.out.println("subqryis:::::::::::"+Subqry);
									 retValue=storeauthid(Subqry,namerole);
								
								  System.out.println(" This is Sub Items"+rs1.getString(1));
							    
								 }
						      }
						      catch (SQLException e1)
						      {
							   e1.printStackTrace();
							   retValue=false;
							  
							  }
						   }
				         
				          }
//			               else{
//			            	   ResultSet authid=objRoleClass.getnames();
//			       			try {
//			       				authid.beforeFirst();
//			       				while(authid.next()){
//			       					if(txtEditRole.toString().equals(authid.getString("role"))){
//			       						UtilFxn.messageBox(SWT.ICON_INFORMATION,  Messages.getLabel("Error"),
//			       				   			 Messages.getLabel("rolealreadyexists.duplicatenamenotallowed."))
//			       							.open();
//			       						System.out.println("duplicaterolename");
//			       						retValue=false;
//			       				}
//			       					
//			       				}
//			       			} catch (SQLException e1) {
//			       				// TODO Auto-generated catch block
//			       				e1.printStackTrace();
//			       			}
//			       			
//			               }
						}
				return retValue;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;

	
		
		}
		
		public void getauthid(String text) {
			authid = objRoleClass.getAuth_id(text);
			}
//	
//		private boolean storeauthid(String subqry)
//		{
//
//			j=objRoleClass.btnSaveActionPerformed(subqry);
//			System.out.println(j);
//			if(j=true)
//			{
//				objRoleView.table.removeAll();
//			    data=objRoleClass.getresults();
//				this.displayingitems(data);
//			}
//			else
//			{
//			System.out.println("not saved");
//			
//			}
//			  return true;		
//		}
		private boolean storeauthid(String subqry,String namerole)
		{
			j=objRoleClass.btnSaveActionPerformed(subqry);
			System.out.println(j);
			if(j==true)
			{
				
			   // data=objRoleClass.getresults();
				data=objRoleClass.getselectedEntries(namerole);
				
			}
			else
			{
			System.out.println("not saved");
			
			}
			  return true;		
		}

		
		
		
}
	

	



	


	