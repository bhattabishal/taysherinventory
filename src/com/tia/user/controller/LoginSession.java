package com.tia.user.controller;



import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.PlatformUI;

import com.tia.account.model.ClosingInfoModel;
import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilFxn;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;


public class LoginSession {
	private int userId;
	private String currentUser;
	private String currentUserPassword;
	private int status=0;
	private String strToday;
	private Date loginDate;
	private int roleId;
	private int fiscalId = 0;
	private String backupURL;
	private String logURL;
	private String currentDatabase;
	// for closing info
	private int curPeriodStartMonth = 0;
	private int curPeriodEndMonth = 0;
	private int curClosingPeriodId;
	private String parkingAccountCode;
	private String userFullName;
	static Properties dbProperties = DbConnection.loadDBProperties();

	public String getUserFullName() {
		return userFullName;
	}
	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

	private String currentClosingPeriod = "";
	private String curPeriodStartMonthDate = "";
	private String curPeriodEndMonthDate = "";
	private Date curPeriodStartMonthEnDate;
	private String previousClosingDate = "";
	private LoginController loginController;
	private static JCalendarFunctions jcalfxn=new JCalendarFunctions();
	
	public int getRoleId() {
		return roleId;
	}
	public int getFiscalId() {
		return fiscalId;
	}

	public void setFiscalId(int fiscalId) {
		this.fiscalId = fiscalId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getStrToday() {
		return strToday;
	}

	public void setStrToday(String strToday) {
		this.strToday = strToday;
	}
	public String getBackupURL() {
		// returns the name of file URL to where the backups of
		// this database will reside in.
		 return backupURL = dbProperties.getProperty("dbBackupURL");
		//return backupURL = UtilFxn
				//.GetValueFromTable("select master_setting_value from master_setting_mcg where master_setting_key='database_dir'");

	}

	public String getLogURL() {
		// returns the name of file URL to where the backups of
		// this database will reside in.
		return logURL = UtilFxn
				.GetValueFromTable("SELECT master_setting_value FROM master_setting_mcg WHERE master_setting_key='log_dir'");
	}
	public String getCurrentDatabase() {
		// returns the name of database currently SKBBL is connected to.
		return currentDatabase = dbProperties.getProperty("db.schema");
	}

	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}



	public String getCurrentUserPassword() {
		return currentUserPassword;
	}
	public int getCurPeriodStartMonth() {
		return curPeriodStartMonth;
	}

	public void setCurPeriodStartMonth(int curPeriodStartMonth) {
		this.curPeriodStartMonth = curPeriodStartMonth;
	}

	public int getCurPeriodEndMonth() {
		return curPeriodEndMonth;
	}

	public void setCurPeriodEndMonth(int curPeriodEndMonth) {
		this.curPeriodEndMonth = curPeriodEndMonth;
	}

	public String getCurrentClosingPeriod() {
		return currentClosingPeriod;
	}

	public void setCurrentClosingPeriod(String currentClosingPeriod) {
		this.currentClosingPeriod = currentClosingPeriod;
	}

	public String getCurPeriodStartMonthDate() {
		return curPeriodStartMonthDate;
	}

	public void setCurPeriodStartMonthDate(String curPeriodStartMonthDate) {
		this.curPeriodStartMonthDate = curPeriodStartMonthDate;
	}

	public String getPreviousClosingDate() {
		return previousClosingDate;
	}

	public void setPreviousClosingDate(String previousClosingDate) {
		this.previousClosingDate = previousClosingDate;
	}

	public String getCurPeriodEndMonthDate() {
		return curPeriodEndMonthDate;
	}

	public void setCurPeriodEndMonthDate(String curPeriodEndMonthDate) {
		this.curPeriodEndMonthDate = curPeriodEndMonthDate;
	}

	public int getCurClosingPeriodId() {
		return curClosingPeriodId;
	}

	public void setCurClosingPeriodId(int curClosingPeriodId) {
		this.curClosingPeriodId = curClosingPeriodId;
	}

	public Date getCurPeriodStartMonthEnDate() {
		return curPeriodStartMonthEnDate;
	}

	public void setCurPeriodStartMonthEnDate(Date curPeriodStartMonthEnDate) {
		this.curPeriodStartMonthEnDate = curPeriodStartMonthEnDate;
	}

	public Date getCurPeriodEndMonthEnDate() {
		return curPeriodEndMonthEnDate;
	}

	public void setCurPeriodEndMonthEnDate(Date curPeriodEndMonthEnDate) {
		this.curPeriodEndMonthEnDate = curPeriodEndMonthEnDate;
	}

	private Date curPeriodEndMonthEnDate;

	public void setCurrentUserPassword(String currentUserPassword) {
		this.currentUserPassword = currentUserPassword;
	}

	private Connection cnn;
	private Statement st;
	private ResultSet rs;
	private String[] elements;
	private String docPath;
	private int login_info_id;

	public int getLogin_info_id() {
		return login_info_id;
	}

	public void setLogin_id(int login_info_id) {
		this.login_info_id = login_info_id;
	}

	public LoginSession(){
		cnn = DbConnection.getConnection();
		st = null;
		rs = null;
	
	}	
	public String getClosingReportURL() {
		// returns the URL of closing reports to be uploaded into
		// at the time of system closing eg. contra report, share report etc.
		// return docPath = dbProperties.getProperty("closingreport.path");
		return docPath = UtilFxn
				.GetValueFromTable("select master_setting_value from master_setting_mcg where master_setting_key='report_dir'");

	}
	public String getParkingAccountCode() {
		setParkingAccountCode();
		System.out.println("zz.. FROM LoginSession class, PARKING ACCOUNT ="
				+ parkingAccountCode);
		return parkingAccountCode;
	}
	public void setParkingAccountCode() {
		String sql = " select afmm.acc_code,ahm.acc_name from acc_fxn_mapping_mcg afmm "
				+ " inner join acc_head_mcg ahm on ahm.acc_code= afmm.acc_code "
				+ " where afmm.remarks='parking' ";
		try {
			st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			rs = st.executeQuery(sql);

			if (rs != null)
				while (rs.next()) {
					this.parkingAccountCode = rs.getString("acc_code");
				}
			else {
				MessageBox mbBox = new MessageBox(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell(), SWT.ICON_ERROR);
				mbBox.setText("ACC NOT FOUND");
				mbBox.setMessage("Dear user, You have not created a PARKING account yet. Please, create PARKING account first !");
				mbBox.open();
			}

			// return ls;

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean checkAuth(String strAuthoritytName) {
		if(this.elements == null)
		{
			return false;
		}
		for (int j = 0; j < this.elements.length - 1; j++) {
			System.out.println("this ischecking null" + this.elements[j]);
			if (this.elements[j].equals(strAuthoritytName)) {
				System.out.println("True" + elements[j]);
				return true;
			}
		}
		return false;

	}
	

	public LoginSession loadSessionUserData(String sessionUser) {
		
		// for fiscal year id
		this.fiscalId = this.getFyId();
		// fiscal year id ends
		LoginSession ls=new LoginSession();		
		//String strQry = "select top(1) user_id,user_name,login_name,login_pwd,role_id,status from inven_user where login_name='"+sessionUser+"'";
		String strQry = "SELECT l.log_in_info_id,u.user_id,u.role_id,u.user_name,u.login_name,u.login_pwd,u.status "
			+ "FROM inven_user u "
			+ "INNER JOIN log_in_info_mcg as l ON  u.user_id=l.user_id and log_out_time is NULL WHERE login_name ='"
			+ sessionUser + "'";
		try {
			st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			rs = st.executeQuery(strQry);

			if (rs != null)
				while (rs.next()) {
					this.login_info_id = rs.getInt("log_in_info_id");
					this.userId=rs.getInt("user_id");
					this.userFullName=rs.getString("user_name");
					this.currentUser= rs.getString("login_name");
					this.currentUserPassword=rs.getString("login_pwd");					
					this.status=Integer.parseInt(rs.getString("status"));
					this.roleId = rs.getInt("role_id");
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					loginDate = new Date();
					this.strToday = sdf.format(loginDate);
				}
			//return ls;

		} catch (SQLException e) {			
			e.printStackTrace();
			
		}
		int role = this.getRoleId();
		loginController = new LoginController();
		ResultSet rs = loginController.GetAuthnames(role);
		int count = 0;
		if (!(rs == null)) {
			try {
				rs.last();
				count = rs.getRow();
				rs.beforeFirst();
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		System.out.println("this is row from  this is doneresultser" + count);
		elements = new String[count + 1];
		try {

			int i = 0;
			while (rs.next()) {
				String element = (rs.getString("auth_name"));
				System.out.println("ths is " + i);
				elements[i] = element;

				System.out.println("This is array data" + elements[i]);
				i++;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		getClosingInfo();
		return ls;

	}
	private int getFyId() {
		int fsId = 0;
		String strQry = "SELECT * FROM fiscal_year_mcg WHERE status='1' ";
		try {
			st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			rs = st.executeQuery(strQry);

			if (rs != null)
				while (rs.next()) {
					fsId = rs.getInt("fy_id");

				}

		} catch (SQLException e) {
			e.printStackTrace();

		}
		return fsId;
	}
	private void getClosingInfo() {
		String strDt = "";
		try {
			// SimpleDateFormat sdfSource = new SimpleDateFormat(
			// "EEE MMM dd hh:mm:ss z yyyy");
			// Date curRawEngDate = new Date();
			// strDt = curRawEngDate.toString();
			//
			// System.out.println("Raw current Date:" + curRawEngDate);
			//
			// Date Fromdate = sdfSource.parse(strDt);
			//
			// SimpleDateFormat sdfDestination = new
			// SimpleDateFormat("yyyy/MM/dd");
			// strDt = sdfDestination.format(Fromdate);
			strDt = JCalendarFunctions.currentDate();

			// return strDt;
			System.out.println("StrDt print is:" + strDt);

		} catch (Exception e1) {

			e1.printStackTrace();

		}

		String nepaliYear = jcalfxn.GetNepaliDate(strDt);
		System.out.println("nepali Year:" + nepaliYear);

		int nepaliMonth = jcalfxn.getNepMonth(nepaliYear);
		System.out.println("nepali Month:" + nepaliMonth);

		int nepaliYearDate = jcalfxn.getNepYear(String.valueOf(nepaliYear));
		System.out.println("nepaliYearDate:" + nepaliYearDate);

		String strQry2 = "SELECT * FROM closing_period_info_mcg WHERE \n"
				+ "(CASE WHEN "
				+ nepaliMonth
				+ "<(SELECT MIN(CAST(start_month AS NUMERIC)) \n"
				+ "FROM closing_period_info_mcg) THEN "
				+ nepaliMonth
				+ "+12 ELSE "
				+ nepaliMonth
				+ " END)\n"
				+ "BETWEEN start_month AND \n"
				+ "(CASE WHEN CAST(start_month AS NUMERIC)>CAST(end_month AS NUMERIC) \n"
				+ "THEN end_month+12 ELSE end_month END);";
		try {
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry2);
			if (rs != null)
				while (rs.next()) {
					this.curPeriodStartMonth = rs.getInt("start_month");
					this.curPeriodEndMonth = rs.getInt("end_month");
					this.currentClosingPeriod = rs.getString("period");
					this.curClosingPeriodId = rs.getInt("id");
					System.out.println("periodSrartMonth:"
							+ curPeriodStartMonth);
					System.out.println("periodEndMonth:" + curPeriodEndMonth);
					System.out.println("currentClosingPeriod:"
							+ currentClosingPeriod);

				}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		int periodEndMonthDateDays = jcalfxn.NoOfDaysInMonth(nepaliYearDate,
				curPeriodEndMonth);
		System.out.println("periodEndMonthDateDays" + periodEndMonthDateDays);

		String npStartMth = "";
		String npEndMth = "";

		if (curPeriodStartMonth < 10) {
			npStartMth = "0" + Integer.toString(curPeriodStartMonth);
		} else {
			npStartMth = Integer.toString(curPeriodStartMonth);
		}

		if (curPeriodEndMonth < 10) {
			npEndMth = "0" + Integer.toString(curPeriodEndMonth);
		} else {
			npEndMth = Integer.toString(curPeriodEndMonth);
		}
		this.curPeriodEndMonthDate = String.valueOf(nepaliYearDate) + "/"
				+ npEndMth + "/" + Integer.toString(periodEndMonthDateDays);

		if (curPeriodStartMonth > Integer.valueOf(nepaliMonth)) {
			nepaliYearDate--;
		}

		this.curPeriodStartMonthDate = String.valueOf(nepaliYearDate) + "/"
				+ npStartMth + "/" + "01";
		this.curPeriodStartMonthEnDate = java.sql.Date
				.valueOf(npClosingDtToEnDt(this.curPeriodStartMonthDate));

		this.curPeriodStartMonthEnDate = java.sql.Date
				.valueOf(npClosingDtToEnDt(this.curPeriodStartMonthDate));
		System.out.println("periodStartMonthDate" + curPeriodStartMonthDate);

		Date curPSMD = jcalfxn.GetEnglishDate(curPeriodStartMonthDate
				.replaceAll("/", jcalfxn.getSeperator()));
		System.out.println("This is the english date:" + curPSMD);
		String afterAddSub = jcalfxn.addSubractDaysFrmEngDate(curPSMD, "minus",
				1, "yyyy-MM-dd");
		this.previousClosingDate = jcalfxn.GetNepaliDate(afterAddSub);

		this.curPeriodEndMonthEnDate = java.sql.Date
				.valueOf(npClosingDtToEnDt(this.curPeriodEndMonthDate));

		this.curPeriodEndMonthEnDate = java.sql.Date
				.valueOf(npClosingDtToEnDt(this.curPeriodEndMonthDate));
		System.out.println("periodEndMonthDate" + curPeriodEndMonthDate);
	}

	public static ClosingInfoModel getClosingInfo(String refDate) {
		ClosingInfoModel cim = new ClosingInfoModel();
		String strDt = refDate;

		String nepaliYear = jcalfxn.GetNepaliDate(strDt);
		System.out.println("nepali Year:" + nepaliYear);

		int nepaliMonth = jcalfxn.getNepMonth(nepaliYear);
		System.out.println("nepali Month:" + nepaliMonth);

		int nepaliYearDate = jcalfxn.getNepYear(String.valueOf(nepaliYear));
		System.out.println("nepaliYearDate:" + nepaliYearDate);

		String strQry2 = "SELECT * FROM closing_period_info_mcg WHERE \n"
				+ "(CASE WHEN "
				+ nepaliMonth
				+ "<(SELECT MIN(CAST(start_month AS NUMERIC)) \n"
				+ "FROM closing_period_info_mcg) THEN "
				+ nepaliMonth
				+ "+12 ELSE "
				+ nepaliMonth
				+ " END)\n"
				+ "BETWEEN start_month AND \n"
				+ "(CASE WHEN CAST(start_month AS NUMERIC)>CAST(end_month AS NUMERIC) \n"
				+ "THEN end_month+12 ELSE end_month END);";

		int curPeriodStartMonth = 0, curPeriodEndMonth = 0, curClosingPeriodId = 0;
		String currentClosingPeriod = "";
		try {
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry2);

			if (rs != null)
				while (rs.next()) {
					curPeriodStartMonth = rs.getInt("start_month");
					curPeriodEndMonth = rs.getInt("end_month");
					currentClosingPeriod = rs.getString("period");
					curClosingPeriodId = rs.getInt("id");
				}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		int periodEndMonthDateDays = jcalfxn.NoOfDaysInMonth(nepaliYearDate,
				curPeriodEndMonth);
		System.out.println("periodEndMonthDateDays" + periodEndMonthDateDays);

		String npStartMth = "";
		String npEndMth = "";

		if (curPeriodStartMonth < 10) {
			npStartMth = "0" + Integer.toString(curPeriodStartMonth);
		} else {
			npStartMth = Integer.toString(curPeriodStartMonth);
		}

		if (curPeriodEndMonth < 10) {
			npEndMth = "0" + Integer.toString(curPeriodEndMonth);
		} else {
			npEndMth = Integer.toString(curPeriodEndMonth);
		}
		String curPeriodEndMonthDate = String.valueOf(nepaliYearDate) + "/"
				+ npEndMth + "/" + Integer.toString(periodEndMonthDateDays);

		if (curPeriodStartMonth > Integer.valueOf(nepaliMonth)) {
			nepaliYearDate--;
		}

		String curPeriodStartMonthDate = String.valueOf(nepaliYearDate) + "/"
				+ npStartMth + "/" + "01";
		Date curPeriodStartMonthEnDate = java.sql.Date
				.valueOf(npClosingDtToEnDt(curPeriodStartMonthDate));

		// curPeriodStartMonthEnDate =
		// java.sql.Date.valueOf(npClosingDtToEnDt(curPeriodStartMonthDate));
		// System.out.println("periodStartMonthDate" + curPeriodStartMonthDate);

		Date curPSMD = jcalfxn.GetEnglishDate(curPeriodStartMonthDate
				.replaceAll("/", jcalfxn.getSeperator()));
		// System.out.println("This is the english date:" + curPSMD);
		String afterAddSub = jcalfxn.addSubractDaysFrmEngDate(curPSMD, "minus",
				1, "yyyy-MM-dd");
		String previousClosingDate = jcalfxn.GetNepaliDate(afterAddSub);

		Date curPeriodEndMonthEnDate = java.sql.Date
				.valueOf(npClosingDtToEnDt(curPeriodEndMonthDate));

		// curPeriodEndMonthEnDate =
		// java.sql.Date.valueOf(npClosingDtToEnDt(curPeriodEndMonthDate));
		System.out.println("periodEndMonthDate" + curPeriodEndMonthDate
				+ " and begin date =" + curPeriodStartMonthEnDate);
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
				"yyyy-MM-dd");
		cim.setStartDateEn(sdf.format(curPeriodStartMonthEnDate));
		cim.setEndDateEn(sdf.format(curPeriodEndMonthEnDate));
		cim.setClosingId(curClosingPeriodId);
		cim.setClosingPeriod(currentClosingPeriod);
		cim.setStartDateNp(curPeriodStartMonthDate);
		cim.setEndDateNp(curPeriodEndMonthDate);

		return cim;
	}

	private static String npClosingDtToEnDt(String npClosingDt) {
		String strDt = "";
		try {

			SimpleDateFormat sdfSource = new SimpleDateFormat(
					"EEE MMM dd hh:mm:ss z yyyy",Locale.ENGLISH);
			Date curRawEngDate = jcalfxn.GetEnglishDate(npClosingDt.replaceAll(
					"/", jcalfxn.getSeperator()).replaceAll("-",
					jcalfxn.getSeperator()));
			strDt = curRawEngDate.toString();

			Date Fromdate = sdfSource.parse(strDt);
			System.out.println("From Date:" + Fromdate);

			SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
			strDt = sdfDestination.format(Fromdate);

			// return strDt;
			System.out.println("StrDt:" + strDt);

		} catch (Exception e1) {

			e1.printStackTrace();

		}
		return strDt;

	}
	
}
