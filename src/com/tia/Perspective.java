package com.tia;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class Perspective implements IPerspectiveFactory {

	@Override
	public void createInitialLayout(IPageLayout layout) {
		layout.setEditorAreaVisible(false);
		IFolderLayout folderLayout = layout.createFolder("folder",
		IPageLayout.TOP, 0.5f, IPageLayout.ID_EDITOR_AREA);
		folderLayout.addView("tia.view.welcome");
		layout.getViewLayout("tia.view.welcome").setCloseable(false);
		layout.getViewLayout("tia.view.welcome").setMoveable(false);
		
		/* The following Filter listener is responsible for global listener for event
		 * of pressing F10 key from keyboard whenever(from anywhere withing application)
		 * user desires to reload last opened view in the workbench.
		 */
		org.eclipse.ui.PlatformUI.getWorkbench().getDisplay().addFilter(org.eclipse.swt.SWT.KeyDown, new org.eclipse.swt.widgets.Listener() {

			public void handleEvent(org.eclipse.swt.widgets.Event e) {
				if(e.keyCode == org.eclipse.swt.SWT.F10)
				{
					org.eclipse.ui.IWorkbenchPage wbp = org.eclipse.ui.PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage();
					String id=wbp.getActivePartReference().getId();
					//now close and reopen the view
					try {
						//org.eclipse.swt.widgets.Display.getCurrent().getActiveShell().close();//it works too
						//org.eclipse.ui.PlatformUI.getWorkbench().getDisplay().getActiveShell();
						org.eclipse.swt.widgets.Menu mnu=org.eclipse.swt.widgets.Display.getCurrent().getActiveShell().getMenuBar();
						if(mnu==null)
						{
							org.eclipse.ui.PlatformUI.getWorkbench().getDisplay().getActiveShell().close();
						}
						else
						{
							wbp.hideView(wbp.findView(id));
							wbp.showView(id);
						}
					} catch (org.eclipse.ui.PartInitException e1) {
						e1.printStackTrace();
					}
				}

			}
		});
	}
}
