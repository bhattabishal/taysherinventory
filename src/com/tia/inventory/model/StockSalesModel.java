	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : StockSalesModel.
	/// </summary>
package com.tia.inventory.model;
	public class StockSalesModel
	{
		private int m_discount_expense_jv_no;
		private String m_discount;
		private String m_discount_percentage;
		private String m_vat_percentage;
		private String m_crtd_dt;
		private int m_customer_id;
		private int m_updt_by;
		private String m_remarks;
		private int m_service_income_jv_no;
		private int m_stock_out_id;
		private String m_paid_amt;
		private int m_billing_type;
		private String m_vat;
		private String m_service_income;
		private int m_vat_expense_jv_no;
		private int m_crtd_by;
		private String m_updt_dt;
		private String m_trxn_dt;
		private int m_sales_jv_no;
		private String m_total_amt;
		private String m_service_expense;
		private int m_bill_no;
		private int m_service_expense_jv_no;
		private int m_updt_cnt;
		private int m_fy_id;
		public StockSalesModel_Criteria z_WhereClause;

		private IsDirty_StockSalesModel z_bool;

		public StockSalesModel()
		{
			z_WhereClause = new StockSalesModel_Criteria();
			z_bool = new IsDirty_StockSalesModel();
		}
		public class IsDirty_StockSalesModel
		{
			public boolean  m_discount_expense_jv_no;
			public boolean  m_discount;
			public boolean m_discount_percentage;
			public boolean m_vat_percentage;
			public boolean  m_crtd_dt;
			public boolean  m_customer_id;
			public boolean  m_updt_by;
			public boolean  m_remarks;
			public boolean  m_service_income_jv_no;
			public boolean  m_stock_out_id;
			public boolean  m_paid_amt;
			public boolean  m_billing_type;
			public boolean  m_vat;
			public boolean  m_service_income;
			public boolean  m_vat_expense_jv_no;
			public boolean  m_crtd_by;
			public boolean  m_updt_dt;
			public boolean  m_trxn_dt;
			public boolean  m_sales_jv_no;
			public boolean  m_total_amt;
			public boolean  m_service_expense;
			public boolean  m_bill_no;
			public boolean  m_service_expense_jv_no;
			public boolean  m_updt_cnt;
			public boolean  m_fy_id;
		}
		public int getDiscount_expense_jv_no()
		{
			return m_discount_expense_jv_no;
		}
		public void setDiscount_expense_jv_no(int value)
		{
			z_bool.m_discount_expense_jv_no = true;
			m_discount_expense_jv_no = value;
		}
		public String getDiscount()
		{
			return m_discount;
		}
		public void setDiscount(String value)
		{
			z_bool.m_discount = true;
			m_discount = value;
		}
		public String getDiscount_percentage()
		{
			return m_discount_percentage;
		}
		public void setDiscount_percentage(String value)
		{
			z_bool.m_discount_percentage = true;
			m_discount_percentage = value;
		}
		public String getVat_percentage()
		{
			return m_vat_percentage;
		}
		public void setVat_perentage(String value)
		{
			z_bool.m_vat_percentage = true;
			m_vat_percentage = value;
		}
		public String getCrtd_dt()
		{
			return m_crtd_dt;
		}
		public void setCrtd_dt(String value)
		{
			z_bool.m_crtd_dt = true;
			m_crtd_dt = value;
		}
		public int getCustomer_id()
		{
			return m_customer_id;
		}
		public void setCustomer_id(int value)
		{
			z_bool.m_customer_id = true;
			m_customer_id = value;
		}
		public int getUpdt_by()
		{
			return m_updt_by;
		}
		public void setUpdt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
		}
		public String getRemarks()
		{
			return m_remarks;
		}
		public void setRemarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
		}
		public int getService_income_jv_no()
		{
			return m_service_income_jv_no;
		}
		public void setService_income_jv_no(int value)
		{
			z_bool.m_service_income_jv_no = true;
			m_service_income_jv_no = value;
		}
		public int getStock_out_id()
		{
			return m_stock_out_id;
		}
		public void setStock_out_id(int value)
		{
			z_bool.m_stock_out_id = true;
			m_stock_out_id = value;
		}
		public String getPaid_amt()
		{
			return m_paid_amt;
		}
		public void setPaid_amt(String value)
		{
			z_bool.m_paid_amt = true;
			m_paid_amt = value;
		}
		public int getBilling_type()
		{
			return m_billing_type;
		}
		public void setBilling_type(int value)
		{
			z_bool.m_billing_type = true;
			m_billing_type = value;
		}
		public String getVat()
		{
			return m_vat;
		}
		public void setVat(String value)
		{
			z_bool.m_vat = true;
			m_vat = value;
		}
		public String getService_income()
		{
			return m_service_income;
		}
		public void setService_income(String value)
		{
			z_bool.m_service_income = true;
			m_service_income = value;
		}
		public int getVat_expense_jv_no()
		{
			return m_vat_expense_jv_no;
		}
		public void setVat_expense_jv_no(int value)
		{
			z_bool.m_vat_expense_jv_no = true;
			m_vat_expense_jv_no = value;
		}
		public int getCrtd_by()
		{
			return m_crtd_by;
		}
		public void setCrtd_by(int value)
		{
			z_bool.m_crtd_by = true;
			m_crtd_by = value;
		}
		public String getUpdt_dt()
		{
			return m_updt_dt;
		}
		public void setUpdt_dt(String value)
		{
			z_bool.m_updt_dt = true;
			m_updt_dt = value;
		}
		public String getTrxn_dt()
		{
			return m_trxn_dt;
		}
		public void setTrxn_dt(String value)
		{
			z_bool.m_trxn_dt = true;
			m_trxn_dt = value;
		}
		public int getSales_jv_no()
		{
			return m_sales_jv_no;
		}
		public void setSales_jv_no(int value)
		{
			z_bool.m_sales_jv_no = true;
			m_sales_jv_no = value;
		}
		public String getTotal_amt()
		{
			return m_total_amt;
		}
		public void setTotal_amt(String value)
		{
			z_bool.m_total_amt = true;
			m_total_amt = value;
		}
		public String getService_expense()
		{
			return m_service_expense;
		}
		public void setService_expense(String value)
		{
			z_bool.m_service_expense = true;
			m_service_expense = value;
		}
		public int getBill_no()
		{
			return m_bill_no;
		}
		public void setBill_no(int value)
		{
			z_bool.m_bill_no = true;
			m_bill_no = value;
		}
		public int getService_expense_jv_no()
		{
			return m_service_expense_jv_no;
		}
		public void setService_expense_jv_no(int value)
		{
			z_bool.m_service_expense_jv_no = true;
			m_service_expense_jv_no = value;
		}
		public int getUpdt_cnt()
		{
			return m_updt_cnt;
		}
		public void setUpdt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
		}
		public int getFy_id()
		{
			return m_fy_id;
		}
		public void setFy_id(int value)
		{
			z_bool.m_fy_id = true;
			m_fy_id = value;
		}
		/*
		public StockSalesModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public StockSalesModel_Criteria Where(StockSalesModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_stock_out ( ";
			if (z_bool.m_discount_expense_jv_no)
			{
				SQL+= z_sep +"discount_expense_jv_no";
				z_sep=" , ";
				}
			if (z_bool.m_discount)
			{
				SQL+= z_sep +"discount";
				z_sep=" , ";
				}
			if (z_bool.m_discount_percentage)
			{
				SQL+= z_sep +"discount_percentage";
				z_sep=" , ";
				}
			if (z_bool.m_vat_percentage)
			{
				SQL+= z_sep +"vat_percentage";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt";
				z_sep=" , ";
				}
			if (z_bool.m_customer_id)
			{
				SQL+= z_sep +"customer_id";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks";
				z_sep=" , ";
				}
			if (z_bool.m_service_income_jv_no)
			{
				SQL+= z_sep +"service_income_jv_no";
				z_sep=" , ";
				}
			if (z_bool.m_stock_out_id)
			{
				SQL+= z_sep +"stock_out_id";
				z_sep=" , ";
				}
			if (z_bool.m_paid_amt)
			{
				SQL+= z_sep +"paid_amt";
				z_sep=" , ";
				}
			if (z_bool.m_billing_type)
			{
				SQL+= z_sep +"billing_type";
				z_sep=" , ";
				}
			if (z_bool.m_vat)
			{
				SQL+= z_sep +"vat";
				z_sep=" , ";
				}
			if (z_bool.m_service_income)
			{
				SQL+= z_sep +"service_income";
				z_sep=" , ";
				}
			if (z_bool.m_vat_expense_jv_no)
			{
				SQL+= z_sep +"vat_expense_jv_no";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt";
				z_sep=" , ";
				}
			if (z_bool.m_sales_jv_no)
			{
				SQL+= z_sep +"sales_jv_no";
				z_sep=" , ";
				}
			if (z_bool.m_total_amt)
			{
				SQL+= z_sep +"total_amt";
				z_sep=" , ";
				}
			if (z_bool.m_service_expense)
			{
				SQL+= z_sep +"service_expense";
				z_sep=" , ";
				}
			if (z_bool.m_bill_no)
			{
				SQL+= z_sep +"bill_no";
				z_sep=" , ";
				}
			if (z_bool.m_service_expense_jv_no)
			{
				SQL+= z_sep +"service_expense_jv_no";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt";
				z_sep=" , ";
				}
			if (z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_discount_expense_jv_no)
			{
				SQL+= z_sep +"'" + m_discount_expense_jv_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_discount)
			{
				SQL+= z_sep +"'" + m_discount + "'";
				z_sep=" , ";
				}
			if (z_bool.m_discount_percentage)
			{
				SQL+= z_sep +"'" + m_discount_percentage + "'";
				z_sep=" , ";
				}
			if (z_bool.m_vat_percentage)
			{
				SQL+= z_sep +"'" + m_vat_percentage + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"'" + m_crtd_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_customer_id)
			{
				SQL+= z_sep +"'" + m_customer_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"'" + m_updt_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"'" + m_remarks + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_income_jv_no)
			{
				SQL+= z_sep +"'" + m_service_income_jv_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_stock_out_id)
			{
				SQL+= z_sep +"'" + m_stock_out_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_paid_amt)
			{
				SQL+= z_sep +"'" + m_paid_amt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_billing_type)
			{
				SQL+= z_sep +"'" + m_billing_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_vat)
			{
				SQL+= z_sep +"'" + m_vat + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_income)
			{
				SQL+= z_sep +"'" + m_service_income + "'";
				z_sep=" , ";
				}
			if (z_bool.m_vat_expense_jv_no)
			{
				SQL+= z_sep +"'" + m_vat_expense_jv_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_by)
			{
				SQL+= z_sep +"'" + m_crtd_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"'" + m_updt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"'" + m_trxn_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_sales_jv_no)
			{
				SQL+= z_sep +"'" + m_sales_jv_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_total_amt)
			{
				SQL+= z_sep +"'" + m_total_amt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_expense)
			{
				SQL+= z_sep +"'" + m_service_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_bill_no)
			{
				SQL+= z_sep +"'" + m_bill_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_expense_jv_no)
			{
				SQL+= z_sep +"'" + m_service_expense_jv_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"'" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_fy_id)
			{
				SQL+= z_sep +"'" + m_fy_id + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_stock_out SET ";
			if (z_bool.m_discount_expense_jv_no)
			{
				SQL+= z_sep +"discount_expense_jv_no='" + m_discount_expense_jv_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_discount)
			{
				SQL+= z_sep +"discount='" + m_discount + "'";
				z_sep=" , ";
				}
			if (z_bool.m_discount_percentage)
			{
				SQL+= z_sep +"discount_percentage='" + m_discount_percentage + "'";
				z_sep=" , ";
				}
			if (z_bool.m_vat_percentage)
			{
				SQL+= z_sep +"vat_percentage='" + m_vat_percentage + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + m_crtd_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_customer_id)
			{
				SQL+= z_sep +"customer_id='" + m_customer_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + m_updt_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + m_remarks + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_income_jv_no)
			{
				SQL+= z_sep +"service_income_jv_no='" + m_service_income_jv_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_stock_out_id)
			{
				SQL+= z_sep +"stock_out_id='" + m_stock_out_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_paid_amt)
			{
				SQL+= z_sep +"paid_amt='" + m_paid_amt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_billing_type)
			{
				SQL+= z_sep +"billing_type='" + m_billing_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_vat)
			{
				SQL+= z_sep +"vat='" + m_vat + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_income)
			{
				SQL+= z_sep +"service_income='" + m_service_income + "'";
				z_sep=" , ";
				}
			if (z_bool.m_vat_expense_jv_no)
			{
				SQL+= z_sep +"vat_expense_jv_no='" + m_vat_expense_jv_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + m_crtd_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + m_updt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + m_trxn_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_sales_jv_no)
			{
				SQL+= z_sep +"sales_jv_no='" + m_sales_jv_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_total_amt)
			{
				SQL+= z_sep +"total_amt='" + m_total_amt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_expense)
			{
				SQL+= z_sep +"service_expense='" + m_service_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_bill_no)
			{
				SQL+= z_sep +"bill_no='" + m_bill_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_expense_jv_no)
			{
				SQL+= z_sep +"service_expense_jv_no='" + m_service_expense_jv_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + m_fy_id + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_discount_expense_jv_no)
			{
				SQL+= z_sep +"discount_expense_jv_no='" + z_WhereClause.discount_expense_jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_discount)
			{
				SQL+= z_sep +"discount='" + z_WhereClause.discount() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_customer_id)
			{
				SQL+= z_sep +"customer_id='" + z_WhereClause.customer_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_income_jv_no)
			{
				SQL+= z_sep +"service_income_jv_no='" + z_WhereClause.service_income_jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_stock_out_id)
			{
				SQL+= z_sep +"stock_out_id='" + z_WhereClause.stock_out_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_paid_amt)
			{
				SQL+= z_sep +"paid_amt='" + z_WhereClause.paid_amt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_billing_type)
			{
				SQL+= z_sep +"billing_type='" + z_WhereClause.billing_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_vat)
			{
				SQL+= z_sep +"vat='" + z_WhereClause.vat() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_income)
			{
				SQL+= z_sep +"service_income='" + z_WhereClause.service_income() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_vat_expense_jv_no)
			{
				SQL+= z_sep +"vat_expense_jv_no='" + z_WhereClause.vat_expense_jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + z_WhereClause.crtd_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + z_WhereClause.trxn_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_sales_jv_no)
			{
				SQL+= z_sep +"sales_jv_no='" + z_WhereClause.sales_jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_total_amt)
			{
				SQL+= z_sep +"total_amt='" + z_WhereClause.total_amt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_expense)
			{
				SQL+= z_sep +"service_expense='" + z_WhereClause.service_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_bill_no)
			{
				SQL+= z_sep +"bill_no='" + z_WhereClause.bill_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_expense_jv_no)
			{
				SQL+= z_sep +"service_expense_jv_no='" + z_WhereClause.service_expense_jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + z_WhereClause.fy_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_stock_out";
			if (z_WhereClause.z_bool.m_discount_expense_jv_no)
			{
				SQL+= z_sep +"discount_expense_jv_no='" + z_WhereClause.discount_expense_jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_discount)
			{
				SQL+= z_sep +"discount='" + z_WhereClause.discount() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_customer_id)
			{
				SQL+= z_sep +"customer_id='" + z_WhereClause.customer_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_income_jv_no)
			{
				SQL+= z_sep +"service_income_jv_no='" + z_WhereClause.service_income_jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_stock_out_id)
			{
				SQL+= z_sep +"stock_out_id='" + z_WhereClause.stock_out_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_paid_amt)
			{
				SQL+= z_sep +"paid_amt='" + z_WhereClause.paid_amt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_billing_type)
			{
				SQL+= z_sep +"billing_type='" + z_WhereClause.billing_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_vat)
			{
				SQL+= z_sep +"vat='" + z_WhereClause.vat() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_income)
			{
				SQL+= z_sep +"service_income='" + z_WhereClause.service_income() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_vat_expense_jv_no)
			{
				SQL+= z_sep +"vat_expense_jv_no='" + z_WhereClause.vat_expense_jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + z_WhereClause.crtd_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + z_WhereClause.trxn_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_sales_jv_no)
			{
				SQL+= z_sep +"sales_jv_no='" + z_WhereClause.sales_jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_total_amt)
			{
				SQL+= z_sep +"total_amt='" + z_WhereClause.total_amt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_expense)
			{
				SQL+= z_sep +"service_expense='" + z_WhereClause.service_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_bill_no)
			{
				SQL+= z_sep +"bill_no='" + z_WhereClause.bill_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_expense_jv_no)
			{
				SQL+= z_sep +"service_expense_jv_no='" + z_WhereClause.service_expense_jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + z_WhereClause.fy_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_stock_out";
			if (z_WhereClause.z_bool.m_discount_expense_jv_no)
			{
				SQL+= z_sep +"discount_expense_jv_no='" + z_WhereClause.discount_expense_jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_discount)
			{
				SQL+= z_sep +"discount='" + z_WhereClause.discount() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_customer_id)
			{
				SQL+= z_sep +"customer_id='" + z_WhereClause.customer_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_income_jv_no)
			{
				SQL+= z_sep +"service_income_jv_no='" + z_WhereClause.service_income_jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_stock_out_id)
			{
				SQL+= z_sep +"stock_out_id='" + z_WhereClause.stock_out_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_paid_amt)
			{
				SQL+= z_sep +"paid_amt='" + z_WhereClause.paid_amt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_billing_type)
			{
				SQL+= z_sep +"billing_type='" + z_WhereClause.billing_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_vat)
			{
				SQL+= z_sep +"vat='" + z_WhereClause.vat() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_income)
			{
				SQL+= z_sep +"service_income='" + z_WhereClause.service_income() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_vat_expense_jv_no)
			{
				SQL+= z_sep +"vat_expense_jv_no='" + z_WhereClause.vat_expense_jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + z_WhereClause.crtd_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + z_WhereClause.trxn_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_sales_jv_no)
			{
				SQL+= z_sep +"sales_jv_no='" + z_WhereClause.sales_jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_total_amt)
			{
				SQL+= z_sep +"total_amt='" + z_WhereClause.total_amt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_expense)
			{
				SQL+= z_sep +"service_expense='" + z_WhereClause.service_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_bill_no)
			{
				SQL+= z_sep +"bill_no='" + z_WhereClause.bill_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_expense_jv_no)
			{
				SQL+= z_sep +"service_expense_jv_no='" + z_WhereClause.service_expense_jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + z_WhereClause.fy_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : StockSalesModel_Criteria.
	/// </summary>
	public class StockSalesModel_Criteria 
	{
		private int m_discount_expense_jv_no;
		private int m_discount;
		private String m_crtd_dt;
		private int m_customer_id;
		private int m_updt_by;
		private String m_remarks;
		private int m_service_income_jv_no;
		private int m_stock_out_id;
		private int m_paid_amt;
		private int m_billing_type;
		private int m_vat;
		private int m_service_income;
		private int m_vat_expense_jv_no;
		private int m_crtd_by;
		private String m_updt_dt;
		private String m_trxn_dt;
		private int m_sales_jv_no;
		private int m_total_amt;
		private int m_service_expense;
		private int m_bill_no;
		private int m_service_expense_jv_no;
		private int m_updt_cnt;
		private int m_fy_id;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_StockSalesModel_Criteria  z_bool;

		public StockSalesModel_Criteria()

		{
		z_bool=new IsDirty_StockSalesModel_Criteria();  
		}
		public class IsDirty_StockSalesModel_Criteria 
		{
			public boolean  m_discount_expense_jv_no;
			public boolean  m_discount;
			public boolean  m_crtd_dt;
			public boolean  m_customer_id;
			public boolean  m_updt_by;
			public boolean  m_remarks;
			public boolean  m_service_income_jv_no;
			public boolean  m_stock_out_id;
			public boolean  m_paid_amt;
			public boolean  m_billing_type;
			public boolean  m_vat;
			public boolean  m_service_income;
			public boolean  m_vat_expense_jv_no;
			public boolean  m_crtd_by;
			public boolean  m_updt_dt;
			public boolean  m_trxn_dt;
			public boolean  m_sales_jv_no;
			public boolean  m_total_amt;
			public boolean  m_service_expense;
			public boolean  m_bill_no;
			public boolean  m_service_expense_jv_no;
			public boolean  m_updt_cnt;
			public boolean  m_fy_id;
			public boolean  MyWhere;

		}
		public int discount_expense_jv_no()
		{
			return m_discount_expense_jv_no;
		}
		public void discount_expense_jv_no(int value)
		{
			z_bool.m_discount_expense_jv_no = true;
			m_discount_expense_jv_no = value;
			if (z_bool.m_discount_expense_jv_no)
			{
				_zWhereClause += z_sep+"discount_expense_jv_no='"+discount_expense_jv_no() + "'";
				z_sep=" AND ";
			}
		}
		public int discount()
		{
			return m_discount;
		}
		public void discount(int value)
		{
			z_bool.m_discount = true;
			m_discount = value;
			if (z_bool.m_discount)
			{
				_zWhereClause += z_sep+"discount='"+discount() + "'";
				z_sep=" AND ";
			}
		}
		public String crtd_dt()
		{
			return m_crtd_dt;
		}
		public void crtd_dt(String value)
		{
			z_bool.m_crtd_dt = true;
			m_crtd_dt = value;
			if (z_bool.m_crtd_dt)
			{
				_zWhereClause += z_sep+"crtd_dt='"+crtd_dt() + "'";
				z_sep=" AND ";
			}
		}
		public int customer_id()
		{
			return m_customer_id;
		}
		public void customer_id(int value)
		{
			z_bool.m_customer_id = true;
			m_customer_id = value;
			if (z_bool.m_customer_id)
			{
				_zWhereClause += z_sep+"customer_id='"+customer_id() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_by()
		{
			return m_updt_by;
		}
		public void updt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
			if (z_bool.m_updt_by)
			{
				_zWhereClause += z_sep+"updt_by='"+updt_by() + "'";
				z_sep=" AND ";
			}
		}
		public String remarks()
		{
			return m_remarks;
		}
		public void remarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
			if (z_bool.m_remarks)
			{
				_zWhereClause += z_sep+"remarks='"+remarks() + "'";
				z_sep=" AND ";
			}
		}
		public int service_income_jv_no()
		{
			return m_service_income_jv_no;
		}
		public void service_income_jv_no(int value)
		{
			z_bool.m_service_income_jv_no = true;
			m_service_income_jv_no = value;
			if (z_bool.m_service_income_jv_no)
			{
				_zWhereClause += z_sep+"service_income_jv_no='"+service_income_jv_no() + "'";
				z_sep=" AND ";
			}
		}
		public int stock_out_id()
		{
			return m_stock_out_id;
		}
		public void stock_out_id(int value)
		{
			z_bool.m_stock_out_id = true;
			m_stock_out_id = value;
			if (z_bool.m_stock_out_id)
			{
				_zWhereClause += z_sep+"stock_out_id='"+stock_out_id() + "'";
				z_sep=" AND ";
			}
		}
		public int paid_amt()
		{
			return m_paid_amt;
		}
		public void paid_amt(int value)
		{
			z_bool.m_paid_amt = true;
			m_paid_amt = value;
			if (z_bool.m_paid_amt)
			{
				_zWhereClause += z_sep+"paid_amt='"+paid_amt() + "'";
				z_sep=" AND ";
			}
		}
		public int billing_type()
		{
			return m_billing_type;
		}
		public void billing_type(int value)
		{
			z_bool.m_billing_type = true;
			m_billing_type = value;
			if (z_bool.m_billing_type)
			{
				_zWhereClause += z_sep+"billing_type='"+billing_type() + "'";
				z_sep=" AND ";
			}
		}
		public int vat()
		{
			return m_vat;
		}
		public void vat(int value)
		{
			z_bool.m_vat = true;
			m_vat = value;
			if (z_bool.m_vat)
			{
				_zWhereClause += z_sep+"vat='"+vat() + "'";
				z_sep=" AND ";
			}
		}
		public int service_income()
		{
			return m_service_income;
		}
		public void service_income(int value)
		{
			z_bool.m_service_income = true;
			m_service_income = value;
			if (z_bool.m_service_income)
			{
				_zWhereClause += z_sep+"service_income='"+service_income() + "'";
				z_sep=" AND ";
			}
		}
		public int vat_expense_jv_no()
		{
			return m_vat_expense_jv_no;
		}
		public void vat_expense_jv_no(int value)
		{
			z_bool.m_vat_expense_jv_no = true;
			m_vat_expense_jv_no = value;
			if (z_bool.m_vat_expense_jv_no)
			{
				_zWhereClause += z_sep+"vat_expense_jv_no='"+vat_expense_jv_no() + "'";
				z_sep=" AND ";
			}
		}
		public int crtd_by()
		{
			return m_crtd_by;
		}
		public void crtd_by(int value)
		{
			z_bool.m_crtd_by = true;
			m_crtd_by = value;
			if (z_bool.m_crtd_by)
			{
				_zWhereClause += z_sep+"crtd_by='"+crtd_by() + "'";
				z_sep=" AND ";
			}
		}
		public String updt_dt()
		{
			return m_updt_dt;
		}
		public void updt_dt(String value)
		{
			z_bool.m_updt_dt = true;
			m_updt_dt = value;
			if (z_bool.m_updt_dt)
			{
				_zWhereClause += z_sep+"updt_dt='"+updt_dt() + "'";
				z_sep=" AND ";
			}
		}
		public String trxn_dt()
		{
			return m_trxn_dt;
		}
		public void trxn_dt(String value)
		{
			z_bool.m_trxn_dt = true;
			m_trxn_dt = value;
			if (z_bool.m_trxn_dt)
			{
				_zWhereClause += z_sep+"trxn_dt='"+trxn_dt() + "'";
				z_sep=" AND ";
			}
		}
		public int sales_jv_no()
		{
			return m_sales_jv_no;
		}
		public void sales_jv_no(int value)
		{
			z_bool.m_sales_jv_no = true;
			m_sales_jv_no = value;
			if (z_bool.m_sales_jv_no)
			{
				_zWhereClause += z_sep+"sales_jv_no='"+sales_jv_no() + "'";
				z_sep=" AND ";
			}
		}
		public int total_amt()
		{
			return m_total_amt;
		}
		public void total_amt(int value)
		{
			z_bool.m_total_amt = true;
			m_total_amt = value;
			if (z_bool.m_total_amt)
			{
				_zWhereClause += z_sep+"total_amt='"+total_amt() + "'";
				z_sep=" AND ";
			}
		}
		public int service_expense()
		{
			return m_service_expense;
		}
		public void service_expense(int value)
		{
			z_bool.m_service_expense = true;
			m_service_expense = value;
			if (z_bool.m_service_expense)
			{
				_zWhereClause += z_sep+"service_expense='"+service_expense() + "'";
				z_sep=" AND ";
			}
		}
		public int bill_no()
		{
			return m_bill_no;
		}
		public void bill_no(int value)
		{
			z_bool.m_bill_no = true;
			m_bill_no = value;
			if (z_bool.m_bill_no)
			{
				_zWhereClause += z_sep+"bill_no='"+bill_no() + "'";
				z_sep=" AND ";
			}
		}
		public int service_expense_jv_no()
		{
			return m_service_expense_jv_no;
		}
		public void service_expense_jv_no(int value)
		{
			z_bool.m_service_expense_jv_no = true;
			m_service_expense_jv_no = value;
			if (z_bool.m_service_expense_jv_no)
			{
				_zWhereClause += z_sep+"service_expense_jv_no='"+service_expense_jv_no() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_cnt()
		{
			return m_updt_cnt;
		}
		public void updt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
			if (z_bool.m_updt_cnt)
			{
				_zWhereClause += z_sep+"updt_cnt='"+updt_cnt() + "'";
				z_sep=" AND ";
			}
		}
		public int fy_id()
		{
			return m_fy_id;
		}
		public void fy_id(int value)
		{
			z_bool.m_fy_id = true;
			m_fy_id = value;
			if (z_bool.m_fy_id)
			{
				_zWhereClause += z_sep+"fy_id='"+fy_id() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}