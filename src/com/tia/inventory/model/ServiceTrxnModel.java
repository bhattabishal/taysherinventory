	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : ServiceTrxnModel.
	/// </summary>
package com.tia.inventory.model;
	public class ServiceTrxnModel
	{
		private String m_crtd_dt;
		private String m_selling_cost;
		private int m_service_id;
		private int m_service_provider_id;
		private int m_service_trxn_id;
		private int m_sales_id;
		private String m_perc_earned;
		private String m_perc_paid;
		private String m_trxn_dt;
		public ServiceTrxnModel_Criteria z_WhereClause;

		private IsDirty_ServiceTrxnModel z_bool;

		public ServiceTrxnModel()
		{
			z_WhereClause = new ServiceTrxnModel_Criteria();
			z_bool = new IsDirty_ServiceTrxnModel();
		}
		public class IsDirty_ServiceTrxnModel
		{
			public boolean  m_crtd_dt;
			public boolean  m_selling_cost;
			public boolean  m_service_id;
			public boolean  m_service_provider_id;
			public boolean  m_service_trxn_id;
			public boolean  m_sales_id;
			public boolean  m_perc_earned;
			public boolean  m_perc_paid;
			public boolean  m_trxn_dt;
		}
		public String getCrtd_dt()
		{
			return m_crtd_dt;
		}
		public void setCrtd_dt(String value)
		{
			z_bool.m_crtd_dt = true;
			m_crtd_dt = value;
		}
		public String getSelling_cost()
		{
			return m_selling_cost;
		}
		public void setSelling_cost(String value)
		{
			z_bool.m_selling_cost = true;
			m_selling_cost = value;
		}
		public int getService_id()
		{
			return m_service_id;
		}
		public void setService_id(int value)
		{
			z_bool.m_service_id = true;
			m_service_id = value;
		}
		public int getService_provider_id()
		{
			return m_service_provider_id;
		}
		public void setService_provider_id(int value)
		{
			z_bool.m_service_provider_id = true;
			m_service_provider_id = value;
		}
		public int getService_trxn_id()
		{
			return m_service_trxn_id;
		}
		public void setService_trxn_id(int value)
		{
			z_bool.m_service_trxn_id = true;
			m_service_trxn_id = value;
		}
		public int getSales_id()
		{
			return m_sales_id;
		}
		public void setSales_id(int value)
		{
			z_bool.m_sales_id = true;
			m_sales_id = value;
		}
		public String getPerc_earned()
		{
			return m_perc_earned;
		}
		public void setPerc_earned(String value)
		{
			z_bool.m_perc_earned = true;
			m_perc_earned = value;
		}
		public String getPerc_paid()
		{
			return m_perc_paid;
		}
		public void setPerc_paid(String value)
		{
			z_bool.m_perc_paid = true;
			m_perc_paid = value;
		}
		public String getTrxn_dt()
		{
			return m_trxn_dt;
		}
		public void setTrxn_dt(String value)
		{
			z_bool.m_trxn_dt = true;
			m_trxn_dt = value;
		}
		/*
		public ServiceTrxnModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public ServiceTrxnModel_Criteria Where(ServiceTrxnModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_service_trxn ( ";
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt";
				z_sep=" , ";
				}
			if (z_bool.m_selling_cost)
			{
				SQL+= z_sep +"selling_cost";
				z_sep=" , ";
				}
			if (z_bool.m_service_id)
			{
				SQL+= z_sep +"service_id";
				z_sep=" , ";
				}
			if (z_bool.m_service_provider_id)
			{
				SQL+= z_sep +"service_provider_id";
				z_sep=" , ";
				}
			if (z_bool.m_service_trxn_id)
			{
				SQL+= z_sep +"service_trxn_id";
				z_sep=" , ";
				}
			if (z_bool.m_sales_id)
			{
				SQL+= z_sep +"sales_id";
				z_sep=" , ";
				}
			if (z_bool.m_perc_earned)
			{
				SQL+= z_sep +"perc_earned";
				z_sep=" , ";
				}
			if (z_bool.m_perc_paid)
			{
				SQL+= z_sep +"perc_paid";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"'" + m_crtd_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_selling_cost)
			{
				SQL+= z_sep +"'" + m_selling_cost + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_id)
			{
				SQL+= z_sep +"'" + m_service_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_provider_id)
			{
				SQL+= z_sep +"'" + m_service_provider_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_trxn_id)
			{
				SQL+= z_sep +"'" + m_service_trxn_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_sales_id)
			{
				SQL+= z_sep +"'" + m_sales_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_perc_earned)
			{
				SQL+= z_sep +"'" + m_perc_earned + "'";
				z_sep=" , ";
				}
			if (z_bool.m_perc_paid)
			{
				SQL+= z_sep +"'" + m_perc_paid + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"'" + m_trxn_dt + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_service_trxn SET ";
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + m_crtd_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_selling_cost)
			{
				SQL+= z_sep +"selling_cost='" + m_selling_cost + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_id)
			{
				SQL+= z_sep +"service_id='" + m_service_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_provider_id)
			{
				SQL+= z_sep +"service_provider_id='" + m_service_provider_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_trxn_id)
			{
				SQL+= z_sep +"service_trxn_id='" + m_service_trxn_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_sales_id)
			{
				SQL+= z_sep +"sales_id='" + m_sales_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_perc_earned)
			{
				SQL+= z_sep +"perc_earned='" + m_perc_earned + "'";
				z_sep=" , ";
				}
			if (z_bool.m_perc_paid)
			{
				SQL+= z_sep +"perc_paid='" + m_perc_paid + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + m_trxn_dt + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_selling_cost)
			{
				SQL+= z_sep +"selling_cost='" + z_WhereClause.selling_cost() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_id)
			{
				SQL+= z_sep +"service_id='" + z_WhereClause.service_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_provider_id)
			{
				SQL+= z_sep +"service_provider_id='" + z_WhereClause.service_provider_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_trxn_id)
			{
				SQL+= z_sep +"service_trxn_id='" + z_WhereClause.service_trxn_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_sales_id)
			{
				SQL+= z_sep +"sales_id='" + z_WhereClause.sales_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_perc_earned)
			{
				SQL+= z_sep +"perc_earned='" + z_WhereClause.perc_earned() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_perc_paid)
			{
				SQL+= z_sep +"perc_paid='" + z_WhereClause.perc_paid() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + z_WhereClause.trxn_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_service_trxn";
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_selling_cost)
			{
				SQL+= z_sep +"selling_cost='" + z_WhereClause.selling_cost() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_id)
			{
				SQL+= z_sep +"service_id='" + z_WhereClause.service_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_provider_id)
			{
				SQL+= z_sep +"service_provider_id='" + z_WhereClause.service_provider_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_trxn_id)
			{
				SQL+= z_sep +"service_trxn_id='" + z_WhereClause.service_trxn_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_sales_id)
			{
				SQL+= z_sep +"sales_id='" + z_WhereClause.sales_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_perc_earned)
			{
				SQL+= z_sep +"perc_earned='" + z_WhereClause.perc_earned() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_perc_paid)
			{
				SQL+= z_sep +"perc_paid='" + z_WhereClause.perc_paid() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + z_WhereClause.trxn_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_service_trxn";
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_selling_cost)
			{
				SQL+= z_sep +"selling_cost='" + z_WhereClause.selling_cost() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_id)
			{
				SQL+= z_sep +"service_id='" + z_WhereClause.service_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_provider_id)
			{
				SQL+= z_sep +"service_provider_id='" + z_WhereClause.service_provider_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_trxn_id)
			{
				SQL+= z_sep +"service_trxn_id='" + z_WhereClause.service_trxn_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_sales_id)
			{
				SQL+= z_sep +"sales_id='" + z_WhereClause.sales_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_perc_earned)
			{
				SQL+= z_sep +"perc_earned='" + z_WhereClause.perc_earned() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_perc_paid)
			{
				SQL+= z_sep +"perc_paid='" + z_WhereClause.perc_paid() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + z_WhereClause.trxn_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : ServiceTrxnModel_Criteria.
	/// </summary>
	public class ServiceTrxnModel_Criteria 
	{
		private String m_crtd_dt;
		private int m_selling_cost;
		private int m_service_id;
		private int m_service_provider_id;
		private int m_service_trxn_id;
		private int m_sales_id;
		private int m_perc_earned;
		private int m_perc_paid;
		private String m_trxn_dt;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_ServiceTrxnModel_Criteria  z_bool;

		public ServiceTrxnModel_Criteria()

		{
		z_bool=new IsDirty_ServiceTrxnModel_Criteria();  
		}
		public class IsDirty_ServiceTrxnModel_Criteria 
		{
			public boolean  m_crtd_dt;
			public boolean  m_selling_cost;
			public boolean  m_service_id;
			public boolean  m_service_provider_id;
			public boolean  m_service_trxn_id;
			public boolean  m_sales_id;
			public boolean  m_perc_earned;
			public boolean  m_perc_paid;
			public boolean  m_trxn_dt;
			public boolean  MyWhere;

		}
		public String crtd_dt()
		{
			return m_crtd_dt;
		}
		public void crtd_dt(String value)
		{
			z_bool.m_crtd_dt = true;
			m_crtd_dt = value;
			if (z_bool.m_crtd_dt)
			{
				_zWhereClause += z_sep+"crtd_dt='"+crtd_dt() + "'";
				z_sep=" AND ";
			}
		}
		public int selling_cost()
		{
			return m_selling_cost;
		}
		public void selling_cost(int value)
		{
			z_bool.m_selling_cost = true;
			m_selling_cost = value;
			if (z_bool.m_selling_cost)
			{
				_zWhereClause += z_sep+"selling_cost='"+selling_cost() + "'";
				z_sep=" AND ";
			}
		}
		public int service_id()
		{
			return m_service_id;
		}
		public void service_id(int value)
		{
			z_bool.m_service_id = true;
			m_service_id = value;
			if (z_bool.m_service_id)
			{
				_zWhereClause += z_sep+"service_id='"+service_id() + "'";
				z_sep=" AND ";
			}
		}
		public int service_provider_id()
		{
			return m_service_provider_id;
		}
		public void service_provider_id(int value)
		{
			z_bool.m_service_provider_id = true;
			m_service_provider_id = value;
			if (z_bool.m_service_provider_id)
			{
				_zWhereClause += z_sep+"service_provider_id='"+service_provider_id() + "'";
				z_sep=" AND ";
			}
		}
		public int service_trxn_id()
		{
			return m_service_trxn_id;
		}
		public void service_trxn_id(int value)
		{
			z_bool.m_service_trxn_id = true;
			m_service_trxn_id = value;
			if (z_bool.m_service_trxn_id)
			{
				_zWhereClause += z_sep+"service_trxn_id='"+service_trxn_id() + "'";
				z_sep=" AND ";
			}
		}
		public int sales_id()
		{
			return m_sales_id;
		}
		public void sales_id(int value)
		{
			z_bool.m_sales_id = true;
			m_sales_id = value;
			if (z_bool.m_sales_id)
			{
				_zWhereClause += z_sep+"sales_id='"+sales_id() + "'";
				z_sep=" AND ";
			}
		}
		public int perc_earned()
		{
			return m_perc_earned;
		}
		public void perc_earned(int value)
		{
			z_bool.m_perc_earned = true;
			m_perc_earned = value;
			if (z_bool.m_perc_earned)
			{
				_zWhereClause += z_sep+"perc_earned='"+perc_earned() + "'";
				z_sep=" AND ";
			}
		}
		public int perc_paid()
		{
			return m_perc_paid;
		}
		public void perc_paid(int value)
		{
			z_bool.m_perc_paid = true;
			m_perc_paid = value;
			if (z_bool.m_perc_paid)
			{
				_zWhereClause += z_sep+"perc_paid='"+perc_paid() + "'";
				z_sep=" AND ";
			}
		}
		public String trxn_dt()
		{
			return m_trxn_dt;
		}
		public void trxn_dt(String value)
		{
			z_bool.m_trxn_dt = true;
			m_trxn_dt = value;
			if (z_bool.m_trxn_dt)
			{
				_zWhereClause += z_sep+"trxn_dt='"+trxn_dt() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}