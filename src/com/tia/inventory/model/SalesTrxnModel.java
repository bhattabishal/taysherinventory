	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : SalesTrxnModel.
	/// </summary>
package com.tia.inventory.model;
	public class SalesTrxnModel
	{
		private String m_vno;
		private String m_crtd_dt;
		private String m_bank_acc_code;
		private String m_source;
		private String m_bank_name;
		private int m_sales_id;
		private String m_trxn_amt;
		private String m_trxn_dt;
		private int m_sales_trxn_id;
		public SalesTrxnModel_Criteria z_WhereClause;

		private IsDirty_SalesTrxnModel z_bool;

		public SalesTrxnModel()
		{
			z_WhereClause = new SalesTrxnModel_Criteria();
			z_bool = new IsDirty_SalesTrxnModel();
		}
		public class IsDirty_SalesTrxnModel
		{
			public boolean  m_vno;
			public boolean  m_crtd_dt;
			public boolean  m_bank_acc_code;
			public boolean  m_source;
			public boolean  m_bank_name;
			public boolean  m_sales_id;
			public boolean  m_trxn_amt;
			public boolean  m_trxn_dt;
			public boolean  m_sales_trxn_id;
		}
		public String getVno()
		{
			return m_vno;
		}
		public void setVno(String value)
		{
			z_bool.m_vno = true;
			m_vno = value;
		}
		public String getCrtd_dt()
		{
			return m_crtd_dt;
		}
		public void setCrtd_dt(String value)
		{
			z_bool.m_crtd_dt = true;
			m_crtd_dt = value;
		}
		public String getBank_acc_code()
		{
			return m_bank_acc_code;
		}
		public void setBank_acc_code(String value)
		{
			z_bool.m_bank_acc_code = true;
			m_bank_acc_code = value;
		}
		public String getSource()
		{
			return m_source;
		}
		public void setSource(String value)
		{
			z_bool.m_source = true;
			m_source = value;
		}
		public String getBank_name()
		{
			return m_bank_name;
		}
		public void setBank_name(String value)
		{
			z_bool.m_bank_name = true;
			m_bank_name = value;
		}
		public int getSales_id()
		{
			return m_sales_id;
		}
		public void setSales_id(int value)
		{
			z_bool.m_sales_id = true;
			m_sales_id = value;
		}
		public String getTrxn_amt()
		{
			return m_trxn_amt;
		}
		public void setTrxn_amt(String value)
		{
			z_bool.m_trxn_amt = true;
			m_trxn_amt = value;
		}
		public String getTrxn_dt()
		{
			return m_trxn_dt;
		}
		public void setTrxn_dt(String value)
		{
			z_bool.m_trxn_dt = true;
			m_trxn_dt = value;
		}
		public int getSales_trxn_id()
		{
			return m_sales_trxn_id;
		}
		public void setSales_trxn_id(int value)
		{
			z_bool.m_sales_trxn_id = true;
			m_sales_trxn_id = value;
		}
		/*
		public SalesTrxnModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public SalesTrxnModel_Criteria Where(SalesTrxnModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_sales_trxn ( ";
			if (z_bool.m_vno)
			{
				SQL+= z_sep +"vno";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt";
				z_sep=" , ";
				}
			if (z_bool.m_bank_acc_code)
			{
				SQL+= z_sep +"bank_acc_code";
				z_sep=" , ";
				}
			if (z_bool.m_source)
			{
				SQL+= z_sep +"source";
				z_sep=" , ";
				}
			if (z_bool.m_bank_name)
			{
				SQL+= z_sep +"bank_name";
				z_sep=" , ";
				}
			if (z_bool.m_sales_id)
			{
				SQL+= z_sep +"sales_id";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_amt)
			{
				SQL+= z_sep +"trxn_amt";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt";
				z_sep=" , ";
				}
			if (z_bool.m_sales_trxn_id)
			{
				SQL+= z_sep +"sales_trxn_id";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_vno)
			{
				SQL+= z_sep +"'" + m_vno + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"'" + m_crtd_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_bank_acc_code)
			{
				SQL+= z_sep +"'" + m_bank_acc_code + "'";
				z_sep=" , ";
				}
			if (z_bool.m_source)
			{
				SQL+= z_sep +"'" + m_source + "'";
				z_sep=" , ";
				}
			if (z_bool.m_bank_name)
			{
				SQL+= z_sep +"'" + m_bank_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_sales_id)
			{
				SQL+= z_sep +"'" + m_sales_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_amt)
			{
				SQL+= z_sep +"'" + m_trxn_amt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"'" + m_trxn_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_sales_trxn_id)
			{
				SQL+= z_sep +"'" + m_sales_trxn_id + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_sales_trxn SET ";
			if (z_bool.m_vno)
			{
				SQL+= z_sep +"vno='" + m_vno + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + m_crtd_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_bank_acc_code)
			{
				SQL+= z_sep +"bank_acc_code='" + m_bank_acc_code + "'";
				z_sep=" , ";
				}
			if (z_bool.m_source)
			{
				SQL+= z_sep +"source='" + m_source + "'";
				z_sep=" , ";
				}
			if (z_bool.m_bank_name)
			{
				SQL+= z_sep +"bank_name='" + m_bank_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_sales_id)
			{
				SQL+= z_sep +"sales_id='" + m_sales_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_amt)
			{
				SQL+= z_sep +"trxn_amt='" + m_trxn_amt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + m_trxn_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_sales_trxn_id)
			{
				SQL+= z_sep +"sales_trxn_id='" + m_sales_trxn_id + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_vno)
			{
				SQL+= z_sep +"vno='" + z_WhereClause.vno() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_bank_acc_code)
			{
				SQL+= z_sep +"bank_acc_code='" + z_WhereClause.bank_acc_code() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_source)
			{
				SQL+= z_sep +"source='" + z_WhereClause.source() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_bank_name)
			{
				SQL+= z_sep +"bank_name='" + z_WhereClause.bank_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_sales_id)
			{
				SQL+= z_sep +"sales_id='" + z_WhereClause.sales_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_amt)
			{
				SQL+= z_sep +"trxn_amt='" + z_WhereClause.trxn_amt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + z_WhereClause.trxn_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_sales_trxn_id)
			{
				SQL+= z_sep +"sales_trxn_id='" + z_WhereClause.sales_trxn_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_sales_trxn";
			if (z_WhereClause.z_bool.m_vno)
			{
				SQL+= z_sep +"vno='" + z_WhereClause.vno() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_bank_acc_code)
			{
				SQL+= z_sep +"bank_acc_code='" + z_WhereClause.bank_acc_code() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_source)
			{
				SQL+= z_sep +"source='" + z_WhereClause.source() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_bank_name)
			{
				SQL+= z_sep +"bank_name='" + z_WhereClause.bank_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_sales_id)
			{
				SQL+= z_sep +"sales_id='" + z_WhereClause.sales_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_amt)
			{
				SQL+= z_sep +"trxn_amt='" + z_WhereClause.trxn_amt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + z_WhereClause.trxn_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_sales_trxn_id)
			{
				SQL+= z_sep +"sales_trxn_id='" + z_WhereClause.sales_trxn_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_sales_trxn";
			if (z_WhereClause.z_bool.m_vno)
			{
				SQL+= z_sep +"vno='" + z_WhereClause.vno() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_bank_acc_code)
			{
				SQL+= z_sep +"bank_acc_code='" + z_WhereClause.bank_acc_code() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_source)
			{
				SQL+= z_sep +"source='" + z_WhereClause.source() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_bank_name)
			{
				SQL+= z_sep +"bank_name='" + z_WhereClause.bank_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_sales_id)
			{
				SQL+= z_sep +"sales_id='" + z_WhereClause.sales_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_amt)
			{
				SQL+= z_sep +"trxn_amt='" + z_WhereClause.trxn_amt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + z_WhereClause.trxn_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_sales_trxn_id)
			{
				SQL+= z_sep +"sales_trxn_id='" + z_WhereClause.sales_trxn_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : SalesTrxnModel_Criteria.
	/// </summary>
	public class SalesTrxnModel_Criteria 
	{
		private String m_vno;
		private String m_crtd_dt;
		private String m_bank_acc_code;
		private String m_source;
		private String m_bank_name;
		private int m_sales_id;
		private int m_trxn_amt;
		private String m_trxn_dt;
		private int m_sales_trxn_id;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_SalesTrxnModel_Criteria  z_bool;

		public SalesTrxnModel_Criteria()

		{
		z_bool=new IsDirty_SalesTrxnModel_Criteria();  
		}
		public class IsDirty_SalesTrxnModel_Criteria 
		{
			public boolean  m_vno;
			public boolean  m_crtd_dt;
			public boolean  m_bank_acc_code;
			public boolean  m_source;
			public boolean  m_bank_name;
			public boolean  m_sales_id;
			public boolean  m_trxn_amt;
			public boolean  m_trxn_dt;
			public boolean  m_sales_trxn_id;
			public boolean  MyWhere;

		}
		public String vno()
		{
			return m_vno;
		}
		public void vno(String value)
		{
			z_bool.m_vno = true;
			m_vno = value;
			if (z_bool.m_vno)
			{
				_zWhereClause += z_sep+"vno='"+vno() + "'";
				z_sep=" AND ";
			}
		}
		public String crtd_dt()
		{
			return m_crtd_dt;
		}
		public void crtd_dt(String value)
		{
			z_bool.m_crtd_dt = true;
			m_crtd_dt = value;
			if (z_bool.m_crtd_dt)
			{
				_zWhereClause += z_sep+"crtd_dt='"+crtd_dt() + "'";
				z_sep=" AND ";
			}
		}
		public String bank_acc_code()
		{
			return m_bank_acc_code;
		}
		public void bank_acc_code(String value)
		{
			z_bool.m_bank_acc_code = true;
			m_bank_acc_code = value;
			if (z_bool.m_bank_acc_code)
			{
				_zWhereClause += z_sep+"bank_acc_code='"+bank_acc_code() + "'";
				z_sep=" AND ";
			}
		}
		public String source()
		{
			return m_source;
		}
		public void source(String value)
		{
			z_bool.m_source = true;
			m_source = value;
			if (z_bool.m_source)
			{
				_zWhereClause += z_sep+"source='"+source() + "'";
				z_sep=" AND ";
			}
		}
		public String bank_name()
		{
			return m_bank_name;
		}
		public void bank_name(String value)
		{
			z_bool.m_bank_name = true;
			m_bank_name = value;
			if (z_bool.m_bank_name)
			{
				_zWhereClause += z_sep+"bank_name='"+bank_name() + "'";
				z_sep=" AND ";
			}
		}
		public int sales_id()
		{
			return m_sales_id;
		}
		public void sales_id(int value)
		{
			z_bool.m_sales_id = true;
			m_sales_id = value;
			if (z_bool.m_sales_id)
			{
				_zWhereClause += z_sep+"sales_id='"+sales_id() + "'";
				z_sep=" AND ";
			}
		}
		public int trxn_amt()
		{
			return m_trxn_amt;
		}
		public void trxn_amt(int value)
		{
			z_bool.m_trxn_amt = true;
			m_trxn_amt = value;
			if (z_bool.m_trxn_amt)
			{
				_zWhereClause += z_sep+"trxn_amt='"+trxn_amt() + "'";
				z_sep=" AND ";
			}
		}
		public String trxn_dt()
		{
			return m_trxn_dt;
		}
		public void trxn_dt(String value)
		{
			z_bool.m_trxn_dt = true;
			m_trxn_dt = value;
			if (z_bool.m_trxn_dt)
			{
				_zWhereClause += z_sep+"trxn_dt='"+trxn_dt() + "'";
				z_sep=" AND ";
			}
		}
		public int sales_trxn_id()
		{
			return m_sales_trxn_id;
		}
		public void sales_trxn_id(int value)
		{
			z_bool.m_sales_trxn_id = true;
			m_sales_trxn_id = value;
			if (z_bool.m_sales_trxn_id)
			{
				_zWhereClause += z_sep+"sales_trxn_id='"+sales_trxn_id() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}