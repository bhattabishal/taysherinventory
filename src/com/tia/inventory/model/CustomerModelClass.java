	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : CustomerModelClass.
	/// </summary>
package com.tia.inventory.model;
	public class CustomerModelClass
	{
		private String m_name;
		private String m_mobile;
		private String m_address;
		private String m_phone;
		private String m_contact_person;
		private String m_created_dt;
		private String m_email;
		private String m_remarks;
		private int m_customer_id;
		private int m_type;
		public CustomerModelClass_Criteria z_WhereClause;

		private IsDirty_CustomerModelClass z_bool;

		public CustomerModelClass()
		{
			z_WhereClause = new CustomerModelClass_Criteria();
			z_bool = new IsDirty_CustomerModelClass();
		}
		public class IsDirty_CustomerModelClass
		{
			public boolean  m_name;
			public boolean  m_mobile;
			public boolean  m_address;
			public boolean  m_phone;
			public boolean  m_contact_person;
			public boolean  m_created_dt;
			public boolean  m_email;
			public boolean  m_remarks;
			public boolean  m_customer_id;
			public boolean  m_type;
		}
		public String getName()
		{
			return m_name;
		}
		public void setName(String value)
		{
			z_bool.m_name = true;
			m_name = value;
		}
		public String getMobile()
		{
			return m_mobile;
		}
		public void setMobile(String value)
		{
			z_bool.m_mobile = true;
			m_mobile = value;
		}
		public String getAddress()
		{
			return m_address;
		}
		public void setAddress(String value)
		{
			z_bool.m_address = true;
			m_address = value;
		}
		public String getPhone()
		{
			return m_phone;
		}
		public void setPhone(String value)
		{
			z_bool.m_phone = true;
			m_phone = value;
		}
		public String getContact_person()
		{
			return m_contact_person;
		}
		public void setContact_person(String value)
		{
			z_bool.m_contact_person = true;
			m_contact_person = value;
		}
		public String getCreated_dt()
		{
			return m_created_dt;
		}
		public void setCreated_dt(String value)
		{
			z_bool.m_created_dt = true;
			m_created_dt = value;
		}
		public String getEmail()
		{
			return m_email;
		}
		public void setEmail(String value)
		{
			z_bool.m_email = true;
			m_email = value;
		}
		public String getRemarks()
		{
			return m_remarks;
		}
		public void setRemarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
		}
		public int getCustomer_id()
		{
			return m_customer_id;
		}
		public void setCustomer_id(int value)
		{
			z_bool.m_customer_id = true;
			m_customer_id = value;
		}
		public int getType()
		{
			return m_type;
		}
		public void setType(int value)
		{
			z_bool.m_type = true;
			m_type = value;
		}
		/*
		public CustomerModelClass_Criteria Where()
		{
				return z_WhereClause;
			}
		public CustomerModelClass_Criteria Where(CustomerModelClass_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_customer ( ";
			if (z_bool.m_name)
			{
				SQL+= z_sep +"name";
				z_sep=" , ";
				}
			if (z_bool.m_mobile)
			{
				SQL+= z_sep +"mobile";
				z_sep=" , ";
				}
			if (z_bool.m_address)
			{
				SQL+= z_sep +"address";
				z_sep=" , ";
				}
			if (z_bool.m_phone)
			{
				SQL+= z_sep +"phone";
				z_sep=" , ";
				}
			if (z_bool.m_contact_person)
			{
				SQL+= z_sep +"contact_person";
				z_sep=" , ";
				}
			if (z_bool.m_created_dt)
			{
				SQL+= z_sep +"created_dt";
				z_sep=" , ";
				}
			if (z_bool.m_email)
			{
				SQL+= z_sep +"email";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks";
				z_sep=" , ";
				}
//			if (z_bool.m_customer_id)
//			{
//				SQL+= z_sep +"customer_id";
//				z_sep=" , ";
//				}
			if (z_bool.m_type)
			{
				SQL+= z_sep +"type";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_name)
			{
				SQL+= z_sep +"'" + m_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_mobile)
			{
				SQL+= z_sep +"'" + m_mobile + "'";
				z_sep=" , ";
				}
			if (z_bool.m_address)
			{
				SQL+= z_sep +"'" + m_address + "'";
				z_sep=" , ";
				}
			if (z_bool.m_phone)
			{
				SQL+= z_sep +"'" + m_phone + "'";
				z_sep=" , ";
				}
			if (z_bool.m_contact_person)
			{
				SQL+= z_sep +"'" + m_contact_person + "'";
				z_sep=" , ";
				}
			if (z_bool.m_created_dt)
			{
				SQL+= z_sep +"'" + m_created_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_email)
			{
				SQL+= z_sep +"'" + m_email + "'";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"'" + m_remarks + "'";
				z_sep=" , ";
				}
//			if (z_bool.m_customer_id)
//			{
//				SQL+= z_sep +"'" + m_customer_id + "'";
//				z_sep=" , ";
//				}
			if (z_bool.m_type)
			{
				SQL+= z_sep +"'" + m_type + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_customer SET ";
			if (z_bool.m_name)
			{
				SQL+= z_sep +"name='" + m_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_mobile)
			{
				SQL+= z_sep +"mobile='" + m_mobile + "'";
				z_sep=" , ";
				}
			if (z_bool.m_address)
			{
				SQL+= z_sep +"address='" + m_address + "'";
				z_sep=" , ";
				}
			if (z_bool.m_phone)
			{
				SQL+= z_sep +"phone='" + m_phone + "'";
				z_sep=" , ";
				}
			if (z_bool.m_contact_person)
			{
				SQL+= z_sep +"contact_person='" + m_contact_person + "'";
				z_sep=" , ";
				}
			if (z_bool.m_created_dt)
			{
				SQL+= z_sep +"created_dt='" + m_created_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_email)
			{
				SQL+= z_sep +"email='" + m_email + "'";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + m_remarks + "'";
				z_sep=" , ";
				}
//			if (z_bool.m_customer_id)
//			{
//				SQL+= z_sep +"customer_id='" + m_customer_id + "'";
//				z_sep=" , ";
//				}
			if (z_bool.m_type)
			{
				SQL+= z_sep +"type='" + m_type + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_name)
			{
				SQL+= z_sep +"name='" + z_WhereClause.name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_mobile)
			{
				SQL+= z_sep +"mobile='" + z_WhereClause.mobile() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_address)
			{
				SQL+= z_sep +"address='" + z_WhereClause.address() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_phone)
			{
				SQL+= z_sep +"phone='" + z_WhereClause.phone() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_contact_person)
			{
				SQL+= z_sep +"contact_person='" + z_WhereClause.contact_person() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_dt)
			{
				SQL+= z_sep +"created_dt='" + z_WhereClause.created_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_email)
			{
				SQL+= z_sep +"email='" + z_WhereClause.email() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_customer_id)
			{
				SQL+= z_sep +"customer_id='" + z_WhereClause.customer_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_type)
			{
				SQL+= z_sep +"type='" + z_WhereClause.type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_customer";
			if (z_WhereClause.z_bool.m_name)
			{
				SQL+= z_sep +"name='" + z_WhereClause.name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_mobile)
			{
				SQL+= z_sep +"mobile='" + z_WhereClause.mobile() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_address)
			{
				SQL+= z_sep +"address='" + z_WhereClause.address() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_phone)
			{
				SQL+= z_sep +"phone='" + z_WhereClause.phone() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_contact_person)
			{
				SQL+= z_sep +"contact_person='" + z_WhereClause.contact_person() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_dt)
			{
				SQL+= z_sep +"created_dt='" + z_WhereClause.created_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_email)
			{
				SQL+= z_sep +"email='" + z_WhereClause.email() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_customer_id)
			{
				SQL+= z_sep +"customer_id='" + z_WhereClause.customer_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_type)
			{
				SQL+= z_sep +"type='" + z_WhereClause.type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_customer";
			if (z_WhereClause.z_bool.m_name)
			{
				SQL+= z_sep +"name='" + z_WhereClause.name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_mobile)
			{
				SQL+= z_sep +"mobile='" + z_WhereClause.mobile() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_address)
			{
				SQL+= z_sep +"address='" + z_WhereClause.address() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_phone)
			{
				SQL+= z_sep +"phone='" + z_WhereClause.phone() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_contact_person)
			{
				SQL+= z_sep +"contact_person='" + z_WhereClause.contact_person() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_dt)
			{
				SQL+= z_sep +"created_dt='" + z_WhereClause.created_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_email)
			{
				SQL+= z_sep +"email='" + z_WhereClause.email() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_customer_id)
			{
				SQL+= z_sep +"customer_id='" + z_WhereClause.customer_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_type)
			{
				SQL+= z_sep +"type='" + z_WhereClause.type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : CustomerModelClass_Criteria.
	/// </summary>
	public class CustomerModelClass_Criteria 
	{
		private String m_name;
		private String m_mobile;
		private String m_address;
		private String m_phone;
		private String m_contact_person;
		private String m_created_dt;
		private String m_email;
		private String m_remarks;
		private int m_customer_id;
		private int m_type;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_CustomerModelClass_Criteria  z_bool;

		public CustomerModelClass_Criteria()

		{
		z_bool=new IsDirty_CustomerModelClass_Criteria();  
		}
		public class IsDirty_CustomerModelClass_Criteria 
		{
			public boolean  m_name;
			public boolean  m_mobile;
			public boolean  m_address;
			public boolean  m_phone;
			public boolean  m_contact_person;
			public boolean  m_created_dt;
			public boolean  m_email;
			public boolean  m_remarks;
			public boolean  m_customer_id;
			public boolean  m_type;
			public boolean  MyWhere;

		}
		public String name()
		{
			return m_name;
		}
		public void name(String value)
		{
			z_bool.m_name = true;
			m_name = value;
			if (z_bool.m_name)
			{
				_zWhereClause += z_sep+"name='"+name() + "'";
				z_sep=" AND ";
			}
		}
		public String mobile()
		{
			return m_mobile;
		}
		public void mobile(String value)
		{
			z_bool.m_mobile = true;
			m_mobile = value;
			if (z_bool.m_mobile)
			{
				_zWhereClause += z_sep+"mobile='"+mobile() + "'";
				z_sep=" AND ";
			}
		}
		public String address()
		{
			return m_address;
		}
		public void address(String value)
		{
			z_bool.m_address = true;
			m_address = value;
			if (z_bool.m_address)
			{
				_zWhereClause += z_sep+"address='"+address() + "'";
				z_sep=" AND ";
			}
		}
		public String phone()
		{
			return m_phone;
		}
		public void phone(String value)
		{
			z_bool.m_phone = true;
			m_phone = value;
			if (z_bool.m_phone)
			{
				_zWhereClause += z_sep+"phone='"+phone() + "'";
				z_sep=" AND ";
			}
		}
		public String contact_person()
		{
			return m_contact_person;
		}
		public void contact_person(String value)
		{
			z_bool.m_contact_person = true;
			m_contact_person = value;
			if (z_bool.m_contact_person)
			{
				_zWhereClause += z_sep+"contact_person='"+contact_person() + "'";
				z_sep=" AND ";
			}
		}
		public String created_dt()
		{
			return m_created_dt;
		}
		public void created_dt(String value)
		{
			z_bool.m_created_dt = true;
			m_created_dt = value;
			if (z_bool.m_created_dt)
			{
				_zWhereClause += z_sep+"created_dt='"+created_dt() + "'";
				z_sep=" AND ";
			}
		}
		public String email()
		{
			return m_email;
		}
		public void email(String value)
		{
			z_bool.m_email = true;
			m_email = value;
			if (z_bool.m_email)
			{
				_zWhereClause += z_sep+"email='"+email() + "'";
				z_sep=" AND ";
			}
		}
		public String remarks()
		{
			return m_remarks;
		}
		public void remarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
			if (z_bool.m_remarks)
			{
				_zWhereClause += z_sep+"remarks='"+remarks() + "'";
				z_sep=" AND ";
			}
		}
		public int customer_id()
		{
			return m_customer_id;
		}
		public void customer_id(int value)
		{
			z_bool.m_customer_id = true;
			m_customer_id = value;
			if (z_bool.m_customer_id)
			{
				_zWhereClause += z_sep+"customer_id='"+customer_id() + "'";
				z_sep=" AND ";
			}
		}
		public int type()
		{
			return m_type;
		}
		public void type(int value)
		{
			z_bool.m_type = true;
			m_type = value;
			if (z_bool.m_type)
			{
				_zWhereClause += z_sep+"type='"+type() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}