	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : StockPurchseExpenseTrxnModel.
	/// </summary>
package com.tia.inventory.model;
	public class StockPurchseExpenseTrxnModel
	{
		private String m_amount;
		private int m_exp_id;
		private int m_exp_trxn_id;
		private int m_pur_id;
		public StockPurchseExpenseTrxnModel_Criteria z_WhereClause;

		private IsDirty_StockPurchseExpenseTrxnModel z_bool;

		public StockPurchseExpenseTrxnModel()
		{
			z_WhereClause = new StockPurchseExpenseTrxnModel_Criteria();
			z_bool = new IsDirty_StockPurchseExpenseTrxnModel();
		}
		public class IsDirty_StockPurchseExpenseTrxnModel
		{
			public boolean  m_amount;
			public boolean  m_exp_id;
			public boolean  m_exp_trxn_id;
			public boolean  m_pur_id;
		}
		public String getAmount()
		{
			return m_amount;
		}
		public void setAmount(String value)
		{
			z_bool.m_amount = true;
			m_amount = value;
		}
		public int getExp_id()
		{
			return m_exp_id;
		}
		public void setExp_id(int value)
		{
			z_bool.m_exp_id = true;
			m_exp_id = value;
		}
		public int getExp_trxn_id()
		{
			return m_exp_trxn_id;
		}
		public void setExp_trxn_id(int value)
		{
			z_bool.m_exp_trxn_id = true;
			m_exp_trxn_id = value;
		}
		public int getPur_id()
		{
			return m_pur_id;
		}
		public void setPur_id(int value)
		{
			z_bool.m_pur_id = true;
			m_pur_id = value;
		}
		/*
		public StockPurchseExpenseTrxnModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public StockPurchseExpenseTrxnModel_Criteria Where(StockPurchseExpenseTrxnModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_pur_exp_trxn ( ";
			if (z_bool.m_amount)
			{
				SQL+= z_sep +"amount";
				z_sep=" , ";
				}
			if (z_bool.m_exp_id)
			{
				SQL+= z_sep +"exp_id";
				z_sep=" , ";
				}
//			if (z_bool.m_exp_trxn_id)
//			{
//				SQL+= z_sep +"exp_trxn_id";
//				z_sep=" , ";
//				}
			if (z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_amount)
			{
				SQL+= z_sep +"'" + m_amount + "'";
				z_sep=" , ";
				}
			if (z_bool.m_exp_id)
			{
				SQL+= z_sep +"'" + m_exp_id + "'";
				z_sep=" , ";
				}
//			if (z_bool.m_exp_trxn_id)
//			{
//				SQL+= z_sep +"'" + m_exp_trxn_id + "'";
//				z_sep=" , ";
//				}
			if (z_bool.m_pur_id)
			{
				SQL+= z_sep +"'" + m_pur_id + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_pur_exp_trxn SET ";
			if (z_bool.m_amount)
			{
				SQL+= z_sep +"amount='" + m_amount + "'";
				z_sep=" , ";
				}
			if (z_bool.m_exp_id)
			{
				SQL+= z_sep +"exp_id='" + m_exp_id + "'";
				z_sep=" , ";
				}
//			if (z_bool.m_exp_trxn_id)
//			{
//				SQL+= z_sep +"exp_trxn_id='" + m_exp_trxn_id + "'";
//				z_sep=" , ";
//				}
			if (z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + m_pur_id + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_amount)
			{
				SQL+= z_sep +"amount='" + z_WhereClause.amount() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_exp_id)
			{
				SQL+= z_sep +"exp_id='" + z_WhereClause.exp_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_exp_trxn_id)
			{
				SQL+= z_sep +"exp_trxn_id='" + z_WhereClause.exp_trxn_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + z_WhereClause.pur_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_pur_exp_trxn";
			if (z_WhereClause.z_bool.m_amount)
			{
				SQL+= z_sep +"amount='" + z_WhereClause.amount() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_exp_id)
			{
				SQL+= z_sep +"exp_id='" + z_WhereClause.exp_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_exp_trxn_id)
			{
				SQL+= z_sep +"exp_trxn_id='" + z_WhereClause.exp_trxn_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + z_WhereClause.pur_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_pur_exp_trxn";
			if (z_WhereClause.z_bool.m_amount)
			{
				SQL+= z_sep +"amount='" + z_WhereClause.amount() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_exp_id)
			{
				SQL+= z_sep +"exp_id='" + z_WhereClause.exp_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_exp_trxn_id)
			{
				SQL+= z_sep +"exp_trxn_id='" + z_WhereClause.exp_trxn_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + z_WhereClause.pur_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : StockPurchseExpenseTrxnModel_Criteria.
	/// </summary>
	public class StockPurchseExpenseTrxnModel_Criteria 
	{
		private int m_amount;
		private int m_exp_id;
		private int m_exp_trxn_id;
		private int m_pur_id;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_StockPurchseExpenseTrxnModel_Criteria  z_bool;

		public StockPurchseExpenseTrxnModel_Criteria()

		{
		z_bool=new IsDirty_StockPurchseExpenseTrxnModel_Criteria();  
		}
		public class IsDirty_StockPurchseExpenseTrxnModel_Criteria 
		{
			public boolean  m_amount;
			public boolean  m_exp_id;
			public boolean  m_exp_trxn_id;
			public boolean  m_pur_id;
			public boolean  MyWhere;

		}
		public int amount()
		{
			return m_amount;
		}
		public void amount(int value)
		{
			z_bool.m_amount = true;
			m_amount = value;
			if (z_bool.m_amount)
			{
				_zWhereClause += z_sep+"amount='"+amount() + "'";
				z_sep=" AND ";
			}
		}
		public int exp_id()
		{
			return m_exp_id;
		}
		public void exp_id(int value)
		{
			z_bool.m_exp_id = true;
			m_exp_id = value;
			if (z_bool.m_exp_id)
			{
				_zWhereClause += z_sep+"exp_id='"+exp_id() + "'";
				z_sep=" AND ";
			}
		}
		public int exp_trxn_id()
		{
			return m_exp_trxn_id;
		}
		public void exp_trxn_id(int value)
		{
			z_bool.m_exp_trxn_id = true;
			m_exp_trxn_id = value;
			if (z_bool.m_exp_trxn_id)
			{
				_zWhereClause += z_sep+"exp_trxn_id='"+exp_trxn_id() + "'";
				z_sep=" AND ";
			}
		}
		public int pur_id()
		{
			return m_pur_id;
		}
		public void pur_id(int value)
		{
			z_bool.m_pur_id = true;
			m_pur_id = value;
			if (z_bool.m_pur_id)
			{
				_zWhereClause += z_sep+"pur_id='"+pur_id() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}