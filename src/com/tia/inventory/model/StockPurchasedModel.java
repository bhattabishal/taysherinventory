	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : StockPurchasedModel.
	/// </summary>
package com.tia.inventory.model;
	public class StockPurchasedModel
	{
		private String m_lu_india_expense;
		private String m_remarks;
		private int m_purchased_payment_id;
		private int m_supplier_id;
		private String m_total_weight;
		private String m_total_amount;
		private String m_paid_amt;
		private String m_other_expense;
		private String m_lu_tax_clearence_expense;
		private int m_pur_id;
		private String m_lu_ktm_expense;
		private String m_trxn_dt;
		private int m_item_purchased_type;
		private String m_bill_no;
		private String m_lu_border_expense;
		private int m_item_local_purchased_type;
		private String m_lu_shop_expense;
		private int m_fy_id;
		private int m_jv_no;
		public StockPurchasedModel_Criteria z_WhereClause;

		private IsDirty_StockPurchasedModel z_bool;

		public StockPurchasedModel()
		{
			z_WhereClause = new StockPurchasedModel_Criteria();
			z_bool = new IsDirty_StockPurchasedModel();
		}
		public class IsDirty_StockPurchasedModel
		{
			public boolean  m_lu_india_expense;
			public boolean  m_remarks;
			public boolean  m_purchased_payment_id;
			public boolean  m_supplier_id;
			public boolean  m_total_weight;
			public boolean  m_total_amount;
			public boolean  m_paid_amt;
			public boolean  m_other_expense;
			public boolean  m_lu_tax_clearence_expense;
			public boolean  m_pur_id;
			public boolean  m_lu_ktm_expense;
			public boolean  m_trxn_dt;
			public boolean  m_item_purchased_type;
			public boolean  m_bill_no;
			public boolean  m_lu_border_expense;
			public boolean  m_item_local_purchased_type;
			public boolean  m_lu_shop_expense;
			public boolean  m_fy_id;
			public boolean  m_jv_no;
		}
		public String getLu_india_expense()
		{
			return m_lu_india_expense;
		}
		public void setLu_india_expense(String value)
		{
			z_bool.m_lu_india_expense = true;
			m_lu_india_expense = value;
		}
		public String getRemarks()
		{
			return m_remarks;
		}
		public void setRemarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
		}
		public int getPurchased_payment_id()
		{
			return m_purchased_payment_id;
		}
		public void setPurchased_payment_id(int value)
		{
			z_bool.m_purchased_payment_id = true;
			m_purchased_payment_id = value;
		}
		public int getSupplier_id()
		{
			return m_supplier_id;
		}
		public void setSupplier_id(int value)
		{
			z_bool.m_supplier_id = true;
			m_supplier_id = value;
		}
		public String getTotal_weight()
		{
			return m_total_weight;
		}
		public void setTotal_weight(String value)
		{
			z_bool.m_total_weight = true;
			m_total_weight = value;
		}
		public String getTotal_amount()
		{
			return m_total_amount;
		}
		public void setTotal_amount(String value)
		{
			z_bool.m_total_amount = true;
			m_total_amount = value;
		}
		public String getPaid_amt()
		{
			return m_paid_amt;
		}
		public void setPaid_amt(String value)
		{
			z_bool.m_paid_amt = true;
			m_paid_amt = value;
		}
		public String getOther_expense()
		{
			return m_other_expense;
		}
		public void setOther_expense(String value)
		{
			z_bool.m_other_expense = true;
			m_other_expense = value;
		}
		public String getLu_tax_clearence_expense()
		{
			return m_lu_tax_clearence_expense;
		}
		public void setLu_tax_clearence_expense(String value)
		{
			z_bool.m_lu_tax_clearence_expense = true;
			m_lu_tax_clearence_expense = value;
		}
		public int getPur_id()
		{
			return m_pur_id;
		}
		public void setPur_id(int value)
		{
			z_bool.m_pur_id = true;
			m_pur_id = value;
		}
		public String getLu_ktm_expense()
		{
			return m_lu_ktm_expense;
		}
		public void setLu_ktm_expense(String value)
		{
			z_bool.m_lu_ktm_expense = true;
			m_lu_ktm_expense = value;
		}
		public String getTrxn_dt()
		{
			return m_trxn_dt;
		}
		public void setTrxn_dt(String value)
		{
			z_bool.m_trxn_dt = true;
			m_trxn_dt = value;
		}
		public int getItem_purchased_type()
		{
			return m_item_purchased_type;
		}
		public void setItem_purchased_type(int value)
		{
			z_bool.m_item_purchased_type = true;
			m_item_purchased_type = value;
		}
		public String getBill_no()
		{
			return m_bill_no;
		}
		public void setBill_no(String value)
		{
			z_bool.m_bill_no = true;
			m_bill_no = value;
		}
		public String getLu_border_expense()
		{
			return m_lu_border_expense;
		}
		public void setLu_border_expense(String value)
		{
			z_bool.m_lu_border_expense = true;
			m_lu_border_expense = value;
		}
		public int getItem_local_purchased_type()
		{
			return m_item_local_purchased_type;
		}
		public void setItem_local_purchased_type(int value)
		{
			z_bool.m_item_local_purchased_type = true;
			m_item_local_purchased_type = value;
		}
		public String getLu_shop_expense()
		{
			return m_lu_shop_expense;
		}
		public void setLu_shop_expense(String value)
		{
			z_bool.m_lu_shop_expense = true;
			m_lu_shop_expense = value;
		}
		public int getFy_id()
		{
			return m_fy_id;
		}
		public void setFy_id(int value)
		{
			z_bool.m_fy_id = true;
			m_fy_id = value;
		}
		public int getJv_no()
		{
			return m_jv_no;
		}
		public void setJv_no(int value)
		{
			z_bool.m_jv_no = true;
			m_jv_no = value;
		}
		/*
		public StockPurchasedModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public StockPurchasedModel_Criteria Where(StockPurchasedModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_item_purchase ( ";
			if (z_bool.m_lu_india_expense)
			{
				SQL+= z_sep +"lu_india_expense";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks";
				z_sep=" , ";
				}
			if (z_bool.m_purchased_payment_id)
			{
				SQL+= z_sep +"purchased_payment_id";
				z_sep=" , ";
				}
			if (z_bool.m_supplier_id)
			{
				SQL+= z_sep +"supplier_id";
				z_sep=" , ";
				}
			if (z_bool.m_total_weight)
			{
				SQL+= z_sep +"total_weight";
				z_sep=" , ";
				}
			if (z_bool.m_total_amount)
			{
				SQL+= z_sep +"total_amount";
				z_sep=" , ";
				}
			if (z_bool.m_paid_amt)
			{
				SQL+= z_sep +"paid_amt";
				z_sep=" , ";
				}
			if (z_bool.m_other_expense)
			{
				SQL+= z_sep +"other_expense";
				z_sep=" , ";
				}
			if (z_bool.m_lu_tax_clearence_expense)
			{
				SQL+= z_sep +"lu_tax_clearence_expense";
				z_sep=" , ";
				}
			if (z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id";
				z_sep=" , ";
				}
			if (z_bool.m_lu_ktm_expense)
			{
				SQL+= z_sep +"lu_ktm_expense";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt";
				z_sep=" , ";
				}
			if (z_bool.m_item_purchased_type)
			{
				SQL+= z_sep +"item_purchased_type";
				z_sep=" , ";
				}
			if (z_bool.m_bill_no)
			{
				SQL+= z_sep +"bill_no";
				z_sep=" , ";
				}
			if (z_bool.m_lu_border_expense)
			{
				SQL+= z_sep +"lu_border_expense";
				z_sep=" , ";
				}
			if (z_bool.m_item_local_purchased_type)
			{
				SQL+= z_sep +"item_local_purchased_type";
				z_sep=" , ";
				}
			if (z_bool.m_lu_shop_expense)
			{
				SQL+= z_sep +"lu_shop_expense";
				z_sep=" , ";
				}
			if (z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id";
				z_sep=" , ";
				}
			if (z_bool.m_jv_no)
			{
				SQL+= z_sep +"jv_no";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_lu_india_expense)
			{
				SQL+= z_sep +"'" + m_lu_india_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"'" + m_remarks + "'";
				z_sep=" , ";
				}
			if (z_bool.m_purchased_payment_id)
			{
				SQL+= z_sep +"'" + m_purchased_payment_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_supplier_id)
			{
				SQL+= z_sep +"'" + m_supplier_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_total_weight)
			{
				SQL+= z_sep +"'" + m_total_weight + "'";
				z_sep=" , ";
				}
			if (z_bool.m_total_amount)
			{
				SQL+= z_sep +"'" + m_total_amount + "'";
				z_sep=" , ";
				}
			if (z_bool.m_paid_amt)
			{
				SQL+= z_sep +"'" + m_paid_amt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_other_expense)
			{
				SQL+= z_sep +"'" + m_other_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_lu_tax_clearence_expense)
			{
				SQL+= z_sep +"'" + m_lu_tax_clearence_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_pur_id)
			{
				SQL+= z_sep +"'" + m_pur_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_lu_ktm_expense)
			{
				SQL+= z_sep +"'" + m_lu_ktm_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"'" + m_trxn_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_purchased_type)
			{
				SQL+= z_sep +"'" + m_item_purchased_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_bill_no)
			{
				SQL+= z_sep +"'" + m_bill_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_lu_border_expense)
			{
				SQL+= z_sep +"'" + m_lu_border_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_local_purchased_type)
			{
				SQL+= z_sep +"'" + m_item_local_purchased_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_lu_shop_expense)
			{
				SQL+= z_sep +"'" + m_lu_shop_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_fy_id)
			{
				SQL+= z_sep +"'" + m_fy_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_jv_no)
			{
				SQL+= z_sep +"'" + m_jv_no + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_item_purchase SET ";
			if (z_bool.m_lu_india_expense)
			{
				SQL+= z_sep +"lu_india_expense='" + m_lu_india_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + m_remarks + "'";
				z_sep=" , ";
				}
			if (z_bool.m_purchased_payment_id)
			{
				SQL+= z_sep +"purchased_payment_id='" + m_purchased_payment_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_supplier_id)
			{
				SQL+= z_sep +"supplier_id='" + m_supplier_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_total_weight)
			{
				SQL+= z_sep +"total_weight='" + m_total_weight + "'";
				z_sep=" , ";
				}
			if (z_bool.m_total_amount)
			{
				SQL+= z_sep +"total_amount='" + m_total_amount + "'";
				z_sep=" , ";
				}
			if (z_bool.m_paid_amt)
			{
				SQL+= z_sep +"paid_amt='" + m_paid_amt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_other_expense)
			{
				SQL+= z_sep +"other_expense='" + m_other_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_lu_tax_clearence_expense)
			{
				SQL+= z_sep +"lu_tax_clearence_expense='" + m_lu_tax_clearence_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + m_pur_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_lu_ktm_expense)
			{
				SQL+= z_sep +"lu_ktm_expense='" + m_lu_ktm_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + m_trxn_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_purchased_type)
			{
				SQL+= z_sep +"item_purchased_type='" + m_item_purchased_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_bill_no)
			{
				SQL+= z_sep +"bill_no='" + m_bill_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_lu_border_expense)
			{
				SQL+= z_sep +"lu_border_expense='" + m_lu_border_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_local_purchased_type)
			{
				SQL+= z_sep +"item_local_purchased_type='" + m_item_local_purchased_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_lu_shop_expense)
			{
				SQL+= z_sep +"lu_shop_expense='" + m_lu_shop_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + m_fy_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_jv_no)
			{
				SQL+= z_sep +"jv_no='" + m_jv_no + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_lu_india_expense)
			{
				SQL+= z_sep +"lu_india_expense='" + z_WhereClause.lu_india_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_purchased_payment_id)
			{
				SQL+= z_sep +"purchased_payment_id='" + z_WhereClause.purchased_payment_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_supplier_id)
			{
				SQL+= z_sep +"supplier_id='" + z_WhereClause.supplier_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_total_weight)
			{
				SQL+= z_sep +"total_weight='" + z_WhereClause.total_weight() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_total_amount)
			{
				SQL+= z_sep +"total_amount='" + z_WhereClause.total_amount() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_paid_amt)
			{
				SQL+= z_sep +"paid_amt='" + z_WhereClause.paid_amt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_other_expense)
			{
				SQL+= z_sep +"other_expense='" + z_WhereClause.other_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_lu_tax_clearence_expense)
			{
				SQL+= z_sep +"lu_tax_clearence_expense='" + z_WhereClause.lu_tax_clearence_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + z_WhereClause.pur_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_lu_ktm_expense)
			{
				SQL+= z_sep +"lu_ktm_expense='" + z_WhereClause.lu_ktm_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + z_WhereClause.trxn_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_purchased_type)
			{
				SQL+= z_sep +"item_purchased_type='" + z_WhereClause.item_purchased_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_bill_no)
			{
				SQL+= z_sep +"bill_no='" + z_WhereClause.bill_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_lu_border_expense)
			{
				SQL+= z_sep +"lu_border_expense='" + z_WhereClause.lu_border_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_local_purchased_type)
			{
				SQL+= z_sep +"item_local_purchased_type='" + z_WhereClause.item_local_purchased_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_lu_shop_expense)
			{
				SQL+= z_sep +"lu_shop_expense='" + z_WhereClause.lu_shop_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + z_WhereClause.fy_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_item_purchase";
			if (z_WhereClause.z_bool.m_lu_india_expense)
			{
				SQL+= z_sep +"lu_india_expense='" + z_WhereClause.lu_india_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_purchased_payment_id)
			{
				SQL+= z_sep +"purchased_payment_id='" + z_WhereClause.purchased_payment_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_supplier_id)
			{
				SQL+= z_sep +"supplier_id='" + z_WhereClause.supplier_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_total_weight)
			{
				SQL+= z_sep +"total_weight='" + z_WhereClause.total_weight() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_total_amount)
			{
				SQL+= z_sep +"total_amount='" + z_WhereClause.total_amount() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_paid_amt)
			{
				SQL+= z_sep +"paid_amt='" + z_WhereClause.paid_amt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_other_expense)
			{
				SQL+= z_sep +"other_expense='" + z_WhereClause.other_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_lu_tax_clearence_expense)
			{
				SQL+= z_sep +"lu_tax_clearence_expense='" + z_WhereClause.lu_tax_clearence_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + z_WhereClause.pur_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_lu_ktm_expense)
			{
				SQL+= z_sep +"lu_ktm_expense='" + z_WhereClause.lu_ktm_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + z_WhereClause.trxn_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_purchased_type)
			{
				SQL+= z_sep +"item_purchased_type='" + z_WhereClause.item_purchased_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_bill_no)
			{
				SQL+= z_sep +"bill_no='" + z_WhereClause.bill_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_lu_border_expense)
			{
				SQL+= z_sep +"lu_border_expense='" + z_WhereClause.lu_border_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_local_purchased_type)
			{
				SQL+= z_sep +"item_local_purchased_type='" + z_WhereClause.item_local_purchased_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_lu_shop_expense)
			{
				SQL+= z_sep +"lu_shop_expense='" + z_WhereClause.lu_shop_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + z_WhereClause.fy_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_item_purchase";
			if (z_WhereClause.z_bool.m_lu_india_expense)
			{
				SQL+= z_sep +"lu_india_expense='" + z_WhereClause.lu_india_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_purchased_payment_id)
			{
				SQL+= z_sep +"purchased_payment_id='" + z_WhereClause.purchased_payment_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_supplier_id)
			{
				SQL+= z_sep +"supplier_id='" + z_WhereClause.supplier_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_total_weight)
			{
				SQL+= z_sep +"total_weight='" + z_WhereClause.total_weight() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_total_amount)
			{
				SQL+= z_sep +"total_amount='" + z_WhereClause.total_amount() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_paid_amt)
			{
				SQL+= z_sep +"paid_amt='" + z_WhereClause.paid_amt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_other_expense)
			{
				SQL+= z_sep +"other_expense='" + z_WhereClause.other_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_lu_tax_clearence_expense)
			{
				SQL+= z_sep +"lu_tax_clearence_expense='" + z_WhereClause.lu_tax_clearence_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + z_WhereClause.pur_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_lu_ktm_expense)
			{
				SQL+= z_sep +"lu_ktm_expense='" + z_WhereClause.lu_ktm_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + z_WhereClause.trxn_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_purchased_type)
			{
				SQL+= z_sep +"item_purchased_type='" + z_WhereClause.item_purchased_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_bill_no)
			{
				SQL+= z_sep +"bill_no='" + z_WhereClause.bill_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_lu_border_expense)
			{
				SQL+= z_sep +"lu_border_expense='" + z_WhereClause.lu_border_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_local_purchased_type)
			{
				SQL+= z_sep +"item_local_purchased_type='" + z_WhereClause.item_local_purchased_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_lu_shop_expense)
			{
				SQL+= z_sep +"lu_shop_expense='" + z_WhereClause.lu_shop_expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + z_WhereClause.fy_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : StockPurchasedModel_Criteria.
	/// </summary>
	public class StockPurchasedModel_Criteria 
	{
		private int m_lu_india_expense;
		private String m_remarks;
		private int m_purchased_payment_id;
		private int m_supplier_id;
		private int m_total_weight;
		private int m_total_amount;
		private int m_paid_amt;
		private int m_other_expense;
		private int m_lu_tax_clearence_expense;
		private int m_pur_id;
		private int m_lu_ktm_expense;
		private String m_trxn_dt;
		private int m_item_purchased_type;
		private String m_bill_no;
		private int m_lu_border_expense;
		private int m_item_local_purchased_type;
		private int m_lu_shop_expense;
		private int m_fy_id;
		private int m_jv_no;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_StockPurchasedModel_Criteria  z_bool;

		public StockPurchasedModel_Criteria()

		{
		z_bool=new IsDirty_StockPurchasedModel_Criteria();  
		}
		public class IsDirty_StockPurchasedModel_Criteria 
		{
			public boolean  m_lu_india_expense;
			public boolean  m_remarks;
			public boolean  m_purchased_payment_id;
			public boolean  m_supplier_id;
			public boolean  m_total_weight;
			public boolean  m_total_amount;
			public boolean  m_paid_amt;
			public boolean  m_other_expense;
			public boolean  m_lu_tax_clearence_expense;
			public boolean  m_pur_id;
			public boolean  m_lu_ktm_expense;
			public boolean  m_trxn_dt;
			public boolean  m_item_purchased_type;
			public boolean  m_bill_no;
			public boolean  m_lu_border_expense;
			public boolean  m_item_local_purchased_type;
			public boolean  m_lu_shop_expense;
			public boolean  m_fy_id;
			public boolean  m_jv_no;
			public boolean  MyWhere;

		}
		public int lu_india_expense()
		{
			return m_lu_india_expense;
		}
		public void lu_india_expense(int value)
		{
			z_bool.m_lu_india_expense = true;
			m_lu_india_expense = value;
			if (z_bool.m_lu_india_expense)
			{
				_zWhereClause += z_sep+"lu_india_expense='"+lu_india_expense() + "'";
				z_sep=" AND ";
			}
		}
		public String remarks()
		{
			return m_remarks;
		}
		public void remarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
			if (z_bool.m_remarks)
			{
				_zWhereClause += z_sep+"remarks='"+remarks() + "'";
				z_sep=" AND ";
			}
		}
		public int purchased_payment_id()
		{
			return m_purchased_payment_id;
		}
		public void purchased_payment_id(int value)
		{
			z_bool.m_purchased_payment_id = true;
			m_purchased_payment_id = value;
			if (z_bool.m_purchased_payment_id)
			{
				_zWhereClause += z_sep+"purchased_payment_id='"+purchased_payment_id() + "'";
				z_sep=" AND ";
			}
		}
		public int supplier_id()
		{
			return m_supplier_id;
		}
		public void supplier_id(int value)
		{
			z_bool.m_supplier_id = true;
			m_supplier_id = value;
			if (z_bool.m_supplier_id)
			{
				_zWhereClause += z_sep+"supplier_id='"+supplier_id() + "'";
				z_sep=" AND ";
			}
		}
		public int total_weight()
		{
			return m_total_weight;
		}
		public void total_weight(int value)
		{
			z_bool.m_total_weight = true;
			m_total_weight = value;
			if (z_bool.m_total_weight)
			{
				_zWhereClause += z_sep+"total_weight='"+total_weight() + "'";
				z_sep=" AND ";
			}
		}
		public int total_amount()
		{
			return m_total_amount;
		}
		public void total_amount(int value)
		{
			z_bool.m_total_amount = true;
			m_total_amount = value;
			if (z_bool.m_total_amount)
			{
				_zWhereClause += z_sep+"total_amount='"+total_amount() + "'";
				z_sep=" AND ";
			}
		}
		public int paid_amt()
		{
			return m_paid_amt;
		}
		public void paid_amt(int value)
		{
			z_bool.m_paid_amt = true;
			m_paid_amt = value;
			if (z_bool.m_paid_amt)
			{
				_zWhereClause += z_sep+"paid_amt='"+paid_amt() + "'";
				z_sep=" AND ";
			}
		}
		public int other_expense()
		{
			return m_other_expense;
		}
		public void other_expense(int value)
		{
			z_bool.m_other_expense = true;
			m_other_expense = value;
			if (z_bool.m_other_expense)
			{
				_zWhereClause += z_sep+"other_expense='"+other_expense() + "'";
				z_sep=" AND ";
			}
		}
		public int lu_tax_clearence_expense()
		{
			return m_lu_tax_clearence_expense;
		}
		public void lu_tax_clearence_expense(int value)
		{
			z_bool.m_lu_tax_clearence_expense = true;
			m_lu_tax_clearence_expense = value;
			if (z_bool.m_lu_tax_clearence_expense)
			{
				_zWhereClause += z_sep+"lu_tax_clearence_expense='"+lu_tax_clearence_expense() + "'";
				z_sep=" AND ";
			}
		}
		public int pur_id()
		{
			return m_pur_id;
		}
		public void pur_id(int value)
		{
			z_bool.m_pur_id = true;
			m_pur_id = value;
			if (z_bool.m_pur_id)
			{
				_zWhereClause += z_sep+"pur_id='"+pur_id() + "'";
				z_sep=" AND ";
			}
		}
		public int lu_ktm_expense()
		{
			return m_lu_ktm_expense;
		}
		public void lu_ktm_expense(int value)
		{
			z_bool.m_lu_ktm_expense = true;
			m_lu_ktm_expense = value;
			if (z_bool.m_lu_ktm_expense)
			{
				_zWhereClause += z_sep+"lu_ktm_expense='"+lu_ktm_expense() + "'";
				z_sep=" AND ";
			}
		}
		public String trxn_dt()
		{
			return m_trxn_dt;
		}
		public void trxn_dt(String value)
		{
			z_bool.m_trxn_dt = true;
			m_trxn_dt = value;
			if (z_bool.m_trxn_dt)
			{
				_zWhereClause += z_sep+"trxn_dt='"+trxn_dt() + "'";
				z_sep=" AND ";
			}
		}
		public int item_purchased_type()
		{
			return m_item_purchased_type;
		}
		public void item_purchased_type(int value)
		{
			z_bool.m_item_purchased_type = true;
			m_item_purchased_type = value;
			if (z_bool.m_item_purchased_type)
			{
				_zWhereClause += z_sep+"item_purchased_type='"+item_purchased_type() + "'";
				z_sep=" AND ";
			}
		}
		public String bill_no()
		{
			return m_bill_no;
		}
		public void bill_no(String value)
		{
			z_bool.m_bill_no = true;
			m_bill_no = value;
			if (z_bool.m_bill_no)
			{
				_zWhereClause += z_sep+"bill_no='"+bill_no() + "'";
				z_sep=" AND ";
			}
		}
		public int lu_border_expense()
		{
			return m_lu_border_expense;
		}
		public void lu_border_expense(int value)
		{
			z_bool.m_lu_border_expense = true;
			m_lu_border_expense = value;
			if (z_bool.m_lu_border_expense)
			{
				_zWhereClause += z_sep+"lu_border_expense='"+lu_border_expense() + "'";
				z_sep=" AND ";
			}
		}
		public int item_local_purchased_type()
		{
			return m_item_local_purchased_type;
		}
		public void item_local_purchased_type(int value)
		{
			z_bool.m_item_local_purchased_type = true;
			m_item_local_purchased_type = value;
			if (z_bool.m_item_local_purchased_type)
			{
				_zWhereClause += z_sep+"item_local_purchased_type='"+item_local_purchased_type() + "'";
				z_sep=" AND ";
			}
		}
		public int lu_shop_expense()
		{
			return m_lu_shop_expense;
		}
		public void lu_shop_expense(int value)
		{
			z_bool.m_lu_shop_expense = true;
			m_lu_shop_expense = value;
			if (z_bool.m_lu_shop_expense)
			{
				_zWhereClause += z_sep+"lu_shop_expense='"+lu_shop_expense() + "'";
				z_sep=" AND ";
			}
		}
		public int fy_id()
		{
			return m_fy_id;
		}
		public void fy_id(int value)
		{
			z_bool.m_fy_id = true;
			m_fy_id = value;
			if (z_bool.m_fy_id)
			{
				_zWhereClause += z_sep+"fy_id='"+fy_id() + "'";
				z_sep=" AND ";
			}
		}
		public int jv_no()
		{
			return m_jv_no;
		}
		public void jv_no(int value)
		{
			z_bool.m_jv_no = true;
			m_jv_no = value;
			if (z_bool.m_jv_no)
			{
				_zWhereClause += z_sep+"jv_no='"+jv_no() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}