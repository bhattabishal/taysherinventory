	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : StockSalesReportModel.
	/// </summary>
package com.tia.inventory.model;
	public class StockSalesReportModel
	{
		private String m_name;
		private String m_bill_no;
		private int m_sales_id;
		private String m_trxn_dt;
		private String m_rpt_order;
		private String m_rate;
		private String m_total;
		private int m_id;
		private String m_report_for;
		private String m_quantity;
		private String m_unit;
		public StockSalesReportModel_Criteria z_WhereClause;

		private IsDirty_StockSalesReportModel z_bool;

		public StockSalesReportModel()
		{
			z_WhereClause = new StockSalesReportModel_Criteria();
			z_bool = new IsDirty_StockSalesReportModel();
		}
		public class IsDirty_StockSalesReportModel
		{
			public boolean  m_name;
			public boolean  m_bill_no;
			public boolean  m_sales_id;
			public boolean  m_trxn_dt;
			public boolean  m_rpt_order;
			public boolean  m_rate;
			public boolean  m_total;
			public boolean  m_id;
			public boolean  m_report_for;
			public boolean  m_quantity;
			public boolean  m_unit;
		}
		public String getName()
		{
			return m_name;
		}
		public void setName(String value)
		{
			z_bool.m_name = true;
			m_name = value;
		}
		public String getBill_no()
		{
			return m_bill_no;
		}
		public void setBill_no(String value)
		{
			z_bool.m_bill_no = true;
			m_bill_no = value;
		}
		public int getSales_id()
		{
			return m_sales_id;
		}
		public void setSales_id(int value)
		{
			z_bool.m_sales_id = true;
			m_sales_id = value;
		}
		public String getTrxn_dt()
		{
			return m_trxn_dt;
		}
		public void setTrxn_dt(String value)
		{
			z_bool.m_trxn_dt = true;
			m_trxn_dt = value;
		}
		public String getRpt_order()
		{
			return m_rpt_order;
		}
		public void setRpt_order(String value)
		{
			z_bool.m_rpt_order = true;
			m_rpt_order = value;
		}
		public String getRate()
		{
			return m_rate;
		}
		public void setRate(String value)
		{
			z_bool.m_rate = true;
			m_rate = value;
		}
		public String getTotal()
		{
			return m_total;
		}
		public void setTotal(String value)
		{
			z_bool.m_total = true;
			m_total = value;
		}
		public int getId()
		{
			return m_id;
		}
		public void setId(int value)
		{
			z_bool.m_id = true;
			m_id = value;
		}
		public String getReport_for()
		{
			return m_report_for;
		}
		public void setReport_for(String value)
		{
			z_bool.m_report_for = true;
			m_report_for = value;
		}
		public String getQuantity()
		{
			return m_quantity;
		}
		public void setQuantity(String value)
		{
			z_bool.m_quantity = true;
			m_quantity = value;
		}
		public String getUnit()
		{
			return m_unit;
		}
		public void setUnit(String value)
		{
			z_bool.m_unit = true;
			m_unit = value;
		}
		/*
		public StockSalesReportModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public StockSalesReportModel_Criteria Where(StockSalesReportModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_sales_report ( ";
			if (z_bool.m_name)
			{
				SQL+= z_sep +"name";
				z_sep=" , ";
				}
			if (z_bool.m_bill_no)
			{
				SQL+= z_sep +"bill_no";
				z_sep=" , ";
				}
			if (z_bool.m_sales_id)
			{
				SQL+= z_sep +"sales_id";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt";
				z_sep=" , ";
				}
			if (z_bool.m_rpt_order)
			{
				SQL+= z_sep +"rpt_order";
				z_sep=" , ";
				}
			if (z_bool.m_rate)
			{
				SQL+= z_sep +"rate";
				z_sep=" , ";
				}
			if (z_bool.m_total)
			{
				SQL+= z_sep +"total";
				z_sep=" , ";
				}
//			if (z_bool.m_id)
//			{
//				SQL+= z_sep +"id";
//				z_sep=" , ";
//				}
			if (z_bool.m_report_for)
			{
				SQL+= z_sep +"report_for";
				z_sep=" , ";
				}
			if (z_bool.m_quantity)
			{
				SQL+= z_sep +"quantity";
				z_sep=" , ";
				}
			if (z_bool.m_unit)
			{
				SQL+= z_sep +"unit";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_name)
			{
				SQL+= z_sep +"'" + m_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_bill_no)
			{
				SQL+= z_sep +"'" + m_bill_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_sales_id)
			{
				SQL+= z_sep +"'" + m_sales_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"'" + m_trxn_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_rpt_order)
			{
				SQL+= z_sep +"'" + m_rpt_order + "'";
				z_sep=" , ";
				}
			if (z_bool.m_rate)
			{
				SQL+= z_sep +"'" + m_rate + "'";
				z_sep=" , ";
				}
			if (z_bool.m_total)
			{
				SQL+= z_sep +"'" + m_total + "'";
				z_sep=" , ";
				}
//			if (z_bool.m_id)
//			{
//				SQL+= z_sep +"'" + m_id + "'";
//				z_sep=" , ";
//				}
			if (z_bool.m_report_for)
			{
				SQL+= z_sep +"'" + m_report_for + "'";
				z_sep=" , ";
				}
			if (z_bool.m_quantity)
			{
				SQL+= z_sep +"'" + m_quantity + "'";
				z_sep=" , ";
				}
			if (z_bool.m_unit)
			{
				SQL+= z_sep +"'" + m_unit + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_sales_report SET ";
			if (z_bool.m_name)
			{
				SQL+= z_sep +"name='" + m_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_bill_no)
			{
				SQL+= z_sep +"bill_no='" + m_bill_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_sales_id)
			{
				SQL+= z_sep +"sales_id='" + m_sales_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + m_trxn_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_rpt_order)
			{
				SQL+= z_sep +"rpt_order='" + m_rpt_order + "'";
				z_sep=" , ";
				}
			if (z_bool.m_rate)
			{
				SQL+= z_sep +"rate='" + m_rate + "'";
				z_sep=" , ";
				}
			if (z_bool.m_total)
			{
				SQL+= z_sep +"total='" + m_total + "'";
				z_sep=" , ";
				}
//			if (z_bool.m_id)
//			{
//				SQL+= z_sep +"id='" + m_id + "'";
//				z_sep=" , ";
//				}
			if (z_bool.m_report_for)
			{
				SQL+= z_sep +"report_for='" + m_report_for + "'";
				z_sep=" , ";
				}
			if (z_bool.m_quantity)
			{
				SQL+= z_sep +"quantity='" + m_quantity + "'";
				z_sep=" , ";
				}
			if (z_bool.m_unit)
			{
				SQL+= z_sep +"unit='" + m_unit + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_name)
			{
				SQL+= z_sep +"name='" + z_WhereClause.name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_bill_no)
			{
				SQL+= z_sep +"bill_no='" + z_WhereClause.bill_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_sales_id)
			{
				SQL+= z_sep +"sales_id='" + z_WhereClause.sales_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + z_WhereClause.trxn_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_rpt_order)
			{
				SQL+= z_sep +"rpt_order='" + z_WhereClause.rpt_order() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_rate)
			{
				SQL+= z_sep +"rate='" + z_WhereClause.rate() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_total)
			{
				SQL+= z_sep +"total='" + z_WhereClause.total() + "'";
				z_sep=" AND ";
				}
//			if (z_WhereClause.z_bool.m_id)
//			{
//				SQL+= z_sep +"id='" + z_WhereClause.id() + "'";
//				z_sep=" AND ";
//				}
			if (z_WhereClause.z_bool.m_report_for)
			{
				SQL+= z_sep +"report_for='" + z_WhereClause.report_for() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_quantity)
			{
				SQL+= z_sep +"quantity='" + z_WhereClause.quantity() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit)
			{
				SQL+= z_sep +"unit='" + z_WhereClause.unit() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_sales_report";
			if (z_WhereClause.z_bool.m_name)
			{
				SQL+= z_sep +"name='" + z_WhereClause.name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_bill_no)
			{
				SQL+= z_sep +"bill_no='" + z_WhereClause.bill_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_sales_id)
			{
				SQL+= z_sep +"sales_id='" + z_WhereClause.sales_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + z_WhereClause.trxn_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_rpt_order)
			{
				SQL+= z_sep +"rpt_order='" + z_WhereClause.rpt_order() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_rate)
			{
				SQL+= z_sep +"rate='" + z_WhereClause.rate() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_total)
			{
				SQL+= z_sep +"total='" + z_WhereClause.total() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_id)
			{
				SQL+= z_sep +"id='" + z_WhereClause.id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_report_for)
			{
				SQL+= z_sep +"report_for='" + z_WhereClause.report_for() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_quantity)
			{
				SQL+= z_sep +"quantity='" + z_WhereClause.quantity() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit)
			{
				SQL+= z_sep +"unit='" + z_WhereClause.unit() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_sales_report";
			if (z_WhereClause.z_bool.m_name)
			{
				SQL+= z_sep +"name='" + z_WhereClause.name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_bill_no)
			{
				SQL+= z_sep +"bill_no='" + z_WhereClause.bill_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_sales_id)
			{
				SQL+= z_sep +"sales_id='" + z_WhereClause.sales_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_dt)
			{
				SQL+= z_sep +"trxn_dt='" + z_WhereClause.trxn_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_rpt_order)
			{
				SQL+= z_sep +"rpt_order='" + z_WhereClause.rpt_order() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_rate)
			{
				SQL+= z_sep +"rate='" + z_WhereClause.rate() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_total)
			{
				SQL+= z_sep +"total='" + z_WhereClause.total() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_id)
			{
				SQL+= z_sep +"id='" + z_WhereClause.id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_report_for)
			{
				SQL+= z_sep +"report_for='" + z_WhereClause.report_for() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_quantity)
			{
				SQL+= z_sep +"quantity='" + z_WhereClause.quantity() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit)
			{
				SQL+= z_sep +"unit='" + z_WhereClause.unit() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : StockSalesReportModel_Criteria.
	/// </summary>
	public class StockSalesReportModel_Criteria 
	{
		private String m_name;
		private String m_bill_no;
		private int m_sales_id;
		private String m_trxn_dt;
		private String m_rpt_order;
		private String m_rate;
		private String m_total;
		private int m_id;
		private String m_report_for;
		private String m_quantity;
		private String m_unit;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_StockSalesReportModel_Criteria  z_bool;

		public StockSalesReportModel_Criteria()

		{
		z_bool=new IsDirty_StockSalesReportModel_Criteria();  
		}
		public class IsDirty_StockSalesReportModel_Criteria 
		{
			public boolean  m_name;
			public boolean  m_bill_no;
			public boolean  m_sales_id;
			public boolean  m_trxn_dt;
			public boolean  m_rpt_order;
			public boolean  m_rate;
			public boolean  m_total;
			public boolean  m_id;
			public boolean  m_report_for;
			public boolean  m_quantity;
			public boolean  m_unit;
			public boolean  MyWhere;

		}
		public String name()
		{
			return m_name;
		}
		public void name(String value)
		{
			z_bool.m_name = true;
			m_name = value;
			if (z_bool.m_name)
			{
				_zWhereClause += z_sep+"name='"+name() + "'";
				z_sep=" AND ";
			}
		}
		public String bill_no()
		{
			return m_bill_no;
		}
		public void bill_no(String value)
		{
			z_bool.m_bill_no = true;
			m_bill_no = value;
			if (z_bool.m_bill_no)
			{
				_zWhereClause += z_sep+"bill_no='"+bill_no() + "'";
				z_sep=" AND ";
			}
		}
		public int sales_id()
		{
			return m_sales_id;
		}
		public void sales_id(int value)
		{
			z_bool.m_sales_id = true;
			m_sales_id = value;
			if (z_bool.m_sales_id)
			{
				_zWhereClause += z_sep+"sales_id='"+sales_id() + "'";
				z_sep=" AND ";
			}
		}
		public String trxn_dt()
		{
			return m_trxn_dt;
		}
		public void trxn_dt(String value)
		{
			z_bool.m_trxn_dt = true;
			m_trxn_dt = value;
			if (z_bool.m_trxn_dt)
			{
				_zWhereClause += z_sep+"trxn_dt='"+trxn_dt() + "'";
				z_sep=" AND ";
			}
		}
		public String rpt_order()
		{
			return m_rpt_order;
		}
		public void rpt_order(String value)
		{
			z_bool.m_rpt_order = true;
			m_rpt_order = value;
			if (z_bool.m_rpt_order)
			{
				_zWhereClause += z_sep+"rpt_order='"+rpt_order() + "'";
				z_sep=" AND ";
			}
		}
		public String rate()
		{
			return m_rate;
		}
		public void rate(String value)
		{
			z_bool.m_rate = true;
			m_rate = value;
			if (z_bool.m_rate)
			{
				_zWhereClause += z_sep+"rate='"+rate() + "'";
				z_sep=" AND ";
			}
		}
		public String total()
		{
			return m_total;
		}
		public void total(String value)
		{
			z_bool.m_total = true;
			m_total = value;
			if (z_bool.m_total)
			{
				_zWhereClause += z_sep+"total='"+total() + "'";
				z_sep=" AND ";
			}
		}
		public int id()
		{
			return m_id;
		}
		public void id(int value)
		{
			z_bool.m_id = true;
			m_id = value;
			if (z_bool.m_id)
			{
				_zWhereClause += z_sep+"id='"+id() + "'";
				z_sep=" AND ";
			}
		}
		public String report_for()
		{
			return m_report_for;
		}
		public void report_for(String value)
		{
			z_bool.m_report_for = true;
			m_report_for = value;
			if (z_bool.m_report_for)
			{
				_zWhereClause += z_sep+"report_for='"+report_for() + "'";
				z_sep=" AND ";
			}
		}
		public String quantity()
		{
			return m_quantity;
		}
		public void quantity(String value)
		{
			z_bool.m_quantity = true;
			m_quantity = value;
			if (z_bool.m_quantity)
			{
				_zWhereClause += z_sep+"quantity='"+quantity() + "'";
				z_sep=" AND ";
			}
		}
		public String unit()
		{
			return m_unit;
		}
		public void unit(String value)
		{
			z_bool.m_unit = true;
			m_unit = value;
			if (z_bool.m_unit)
			{
				_zWhereClause += z_sep+"unit='"+unit() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}