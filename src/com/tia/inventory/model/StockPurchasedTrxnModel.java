	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : StockPurchasedTrxnModel.
	/// </summary>
package com.tia.inventory.model;
	public class StockPurchasedTrxnModel
	{
		private String m_amount;
		private String m_trxn_source;
		private int m_pur_id;
		private int m_jv_no;
		private String m_acc_code;
		private int m_pur_trxn_id;
		public StockPurchasedTrxnModel_Criteria z_WhereClause;

		private IsDirty_StockPurchasedTrxnModel z_bool;

		public StockPurchasedTrxnModel()
		{
			z_WhereClause = new StockPurchasedTrxnModel_Criteria();
			z_bool = new IsDirty_StockPurchasedTrxnModel();
		}
		public class IsDirty_StockPurchasedTrxnModel
		{
			public boolean  m_amount;
			public boolean  m_trxn_source;
			public boolean  m_pur_id;
			public boolean  m_jv_no;
			public boolean  m_acc_code;
			public boolean  m_pur_trxn_id;
		}
		public String getAmount()
		{
			return m_amount;
		}
		public void setAmount(String value)
		{
			z_bool.m_amount = true;
			m_amount = value;
		}
		public String getTrxn_source()
		{
			return m_trxn_source;
		}
		public void setTrxn_source(String value)
		{
			z_bool.m_trxn_source = true;
			m_trxn_source = value;
		}
		public int getPur_id()
		{
			return m_pur_id;
		}
		public void setPur_id(int value)
		{
			z_bool.m_pur_id = true;
			m_pur_id = value;
		}
		public int getJv_no()
		{
			return m_jv_no;
		}
		public void setJv_no(int value)
		{
			z_bool.m_jv_no = true;
			m_jv_no = value;
		}
		public String getAcc_code()
		{
			return m_acc_code;
		}
		public void setAcc_code(String value)
		{
			z_bool.m_acc_code = true;
			m_acc_code = value;
		}
		public int getPur_trxn_id()
		{
			return m_pur_trxn_id;
		}
		public void setPur_trxn_id(int value)
		{
			z_bool.m_pur_trxn_id = true;
			m_pur_trxn_id = value;
		}
		/*
		public StockPurchasedTrxnModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public StockPurchasedTrxnModel_Criteria Where(StockPurchasedTrxnModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_item_pur_trxn ( ";
			if (z_bool.m_amount)
			{
				SQL+= z_sep +"amount";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_source)
			{
				SQL+= z_sep +"trxn_source";
				z_sep=" , ";
				}
			if (z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id";
				z_sep=" , ";
				}
			if (z_bool.m_jv_no)
			{
				SQL+= z_sep +"jv_no";
				z_sep=" , ";
				}
			if (z_bool.m_acc_code)
			{
				SQL+= z_sep +"acc_code";
				z_sep=" , ";
				}
			if (z_bool.m_pur_trxn_id)
			{
				SQL+= z_sep +"pur_trxn_id";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_amount)
			{
				SQL+= z_sep +"'" + m_amount + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_source)
			{
				SQL+= z_sep +"'" + m_trxn_source + "'";
				z_sep=" , ";
				}
			if (z_bool.m_pur_id)
			{
				SQL+= z_sep +"'" + m_pur_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_jv_no)
			{
				SQL+= z_sep +"'" + m_jv_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_acc_code)
			{
				SQL+= z_sep +"'" + m_acc_code + "'";
				z_sep=" , ";
				}
			if (z_bool.m_pur_trxn_id)
			{
				SQL+= z_sep +"'" + m_pur_trxn_id + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_item_pur_trxn SET ";
			if (z_bool.m_amount)
			{
				SQL+= z_sep +"amount='" + m_amount + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_source)
			{
				SQL+= z_sep +"trxn_source='" + m_trxn_source + "'";
				z_sep=" , ";
				}
			if (z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + m_pur_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_jv_no)
			{
				SQL+= z_sep +"jv_no='" + m_jv_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_acc_code)
			{
				SQL+= z_sep +"acc_code='" + m_acc_code + "'";
				z_sep=" , ";
				}
			if (z_bool.m_pur_trxn_id)
			{
				SQL+= z_sep +"pur_trxn_id='" + m_pur_trxn_id + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_amount)
			{
				SQL+= z_sep +"amount='" + z_WhereClause.amount() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_source)
			{
				SQL+= z_sep +"trxn_source='" + z_WhereClause.trxn_source() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + z_WhereClause.pur_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_jv_no)
			{
				SQL+= z_sep +"jv_no='" + z_WhereClause.jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_acc_code)
			{
				SQL+= z_sep +"acc_code='" + z_WhereClause.acc_code() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_trxn_id)
			{
				SQL+= z_sep +"pur_trxn_id='" + z_WhereClause.pur_trxn_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_item_pur_trxn";
			if (z_WhereClause.z_bool.m_amount)
			{
				SQL+= z_sep +"amount='" + z_WhereClause.amount() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_source)
			{
				SQL+= z_sep +"trxn_source='" + z_WhereClause.trxn_source() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + z_WhereClause.pur_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_jv_no)
			{
				SQL+= z_sep +"jv_no='" + z_WhereClause.jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_acc_code)
			{
				SQL+= z_sep +"acc_code='" + z_WhereClause.acc_code() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_trxn_id)
			{
				SQL+= z_sep +"pur_trxn_id='" + z_WhereClause.pur_trxn_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_item_pur_trxn";
			if (z_WhereClause.z_bool.m_amount)
			{
				SQL+= z_sep +"amount='" + z_WhereClause.amount() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_source)
			{
				SQL+= z_sep +"trxn_source='" + z_WhereClause.trxn_source() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + z_WhereClause.pur_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_jv_no)
			{
				SQL+= z_sep +"jv_no='" + z_WhereClause.jv_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_acc_code)
			{
				SQL+= z_sep +"acc_code='" + z_WhereClause.acc_code() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_trxn_id)
			{
				SQL+= z_sep +"pur_trxn_id='" + z_WhereClause.pur_trxn_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : StockPurchasedTrxnModel_Criteria.
	/// </summary>
	public class StockPurchasedTrxnModel_Criteria 
	{
		private int m_amount;
		private String m_trxn_source;
		private int m_pur_id;
		private int m_jv_no;
		private String m_acc_code;
		private int m_pur_trxn_id;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_StockPurchasedTrxnModel_Criteria  z_bool;

		public StockPurchasedTrxnModel_Criteria()

		{
		z_bool=new IsDirty_StockPurchasedTrxnModel_Criteria();  
		}
		public class IsDirty_StockPurchasedTrxnModel_Criteria 
		{
			public boolean  m_amount;
			public boolean  m_trxn_source;
			public boolean  m_pur_id;
			public boolean  m_jv_no;
			public boolean  m_acc_code;
			public boolean  m_pur_trxn_id;
			public boolean  MyWhere;

		}
		public int amount()
		{
			return m_amount;
		}
		public void amount(int value)
		{
			z_bool.m_amount = true;
			m_amount = value;
			if (z_bool.m_amount)
			{
				_zWhereClause += z_sep+"amount='"+amount() + "'";
				z_sep=" AND ";
			}
		}
		public String trxn_source()
		{
			return m_trxn_source;
		}
		public void trxn_source(String value)
		{
			z_bool.m_trxn_source = true;
			m_trxn_source = value;
			if (z_bool.m_trxn_source)
			{
				_zWhereClause += z_sep+"trxn_source='"+trxn_source() + "'";
				z_sep=" AND ";
			}
		}
		public int pur_id()
		{
			return m_pur_id;
		}
		public void pur_id(int value)
		{
			z_bool.m_pur_id = true;
			m_pur_id = value;
			if (z_bool.m_pur_id)
			{
				_zWhereClause += z_sep+"pur_id='"+pur_id() + "'";
				z_sep=" AND ";
			}
		}
		public int jv_no()
		{
			return m_jv_no;
		}
		public void jv_no(int value)
		{
			z_bool.m_jv_no = true;
			m_jv_no = value;
			if (z_bool.m_jv_no)
			{
				_zWhereClause += z_sep+"jv_no='"+jv_no() + "'";
				z_sep=" AND ";
			}
		}
		public String acc_code()
		{
			return m_acc_code;
		}
		public void acc_code(String value)
		{
			z_bool.m_acc_code = true;
			m_acc_code = value;
			if (z_bool.m_acc_code)
			{
				_zWhereClause += z_sep+"acc_code='"+acc_code() + "'";
				z_sep=" AND ";
			}
		}
		public int pur_trxn_id()
		{
			return m_pur_trxn_id;
		}
		public void pur_trxn_id(int value)
		{
			z_bool.m_pur_trxn_id = true;
			m_pur_trxn_id = value;
			if (z_bool.m_pur_trxn_id)
			{
				_zWhereClause += z_sep+"pur_trxn_id='"+pur_trxn_id() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}