	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : ServiceRecordModel.
	/// </summary>
package com.tia.inventory.model;
	public class ServiceRecordModel
	{
		private String m_crdt_dt;
		private String m_next_servicing_at_km;
		private int m_service_record_id;
		private String m_remarks;
		private String m_phone_no;
		private String m_servicing_at_km;
		private String m_customer_name;
		private String m_car_no;
		public ServiceRecordModel_Criteria z_WhereClause;

		private IsDirty_ServiceRecordModel z_bool;

		public ServiceRecordModel()
		{
			z_WhereClause = new ServiceRecordModel_Criteria();
			z_bool = new IsDirty_ServiceRecordModel();
		}
		public class IsDirty_ServiceRecordModel
		{
			public boolean  m_crdt_dt;
			public boolean  m_next_servicing_at_km;
			public boolean  m_service_record_id;
			public boolean  m_remarks;
			public boolean  m_phone_no;
			public boolean  m_servicing_at_km;
			public boolean  m_customer_name;
			public boolean  m_car_no;
		}
		public String getCrdt_dt()
		{
			return m_crdt_dt;
		}
		public void setCrdt_dt(String value)
		{
			z_bool.m_crdt_dt = true;
			m_crdt_dt = value;
		}
		public String getNext_servicing_at_km()
		{
			return m_next_servicing_at_km;
		}
		public void setNext_servicing_at_km(String value)
		{
			z_bool.m_next_servicing_at_km = true;
			m_next_servicing_at_km = value;
		}
		public int getService_record_id()
		{
			return m_service_record_id;
		}
		public void setService_record_id(int value)
		{
			z_bool.m_service_record_id = true;
			m_service_record_id = value;
		}
		public String getRemarks()
		{
			return m_remarks;
		}
		public void setRemarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
		}
		public String getPhone_no()
		{
			return m_phone_no;
		}
		public void setPhone_no(String value)
		{
			z_bool.m_phone_no = true;
			m_phone_no = value;
		}
		public String getServicing_at_km()
		{
			return m_servicing_at_km;
		}
		public void setServicing_at_km(String value)
		{
			z_bool.m_servicing_at_km = true;
			m_servicing_at_km = value;
		}
		public String getCustomer_name()
		{
			return m_customer_name;
		}
		public void setCustomer_name(String value)
		{
			z_bool.m_customer_name = true;
			m_customer_name = value;
		}
		public String getCar_no()
		{
			return m_car_no;
		}
		public void setCar_no(String value)
		{
			z_bool.m_car_no = true;
			m_car_no = value;
		}
		/*
		public ServiceRecordModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public ServiceRecordModel_Criteria Where(ServiceRecordModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_service_record ( ";
			if (z_bool.m_crdt_dt)
			{
				SQL+= z_sep +"crdt_dt";
				z_sep=" , ";
				}
			if (z_bool.m_next_servicing_at_km)
			{
				SQL+= z_sep +"next_servicing_at_km";
				z_sep=" , ";
				}
			if (z_bool.m_service_record_id)
			{
				SQL+= z_sep +"service_record_id";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks";
				z_sep=" , ";
				}
			if (z_bool.m_phone_no)
			{
				SQL+= z_sep +"phone_no";
				z_sep=" , ";
				}
			if (z_bool.m_servicing_at_km)
			{
				SQL+= z_sep +"servicing_at_km";
				z_sep=" , ";
				}
			if (z_bool.m_customer_name)
			{
				SQL+= z_sep +"customer_name";
				z_sep=" , ";
				}
			if (z_bool.m_car_no)
			{
				SQL+= z_sep +"car_no";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_crdt_dt)
			{
				SQL+= z_sep +"'" + m_crdt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_next_servicing_at_km)
			{
				SQL+= z_sep +"'" + m_next_servicing_at_km + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_record_id)
			{
				SQL+= z_sep +"'" + m_service_record_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"'" + m_remarks + "'";
				z_sep=" , ";
				}
			if (z_bool.m_phone_no)
			{
				SQL+= z_sep +"'" + m_phone_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_servicing_at_km)
			{
				SQL+= z_sep +"'" + m_servicing_at_km + "'";
				z_sep=" , ";
				}
			if (z_bool.m_customer_name)
			{
				SQL+= z_sep +"'" + m_customer_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_car_no)
			{
				SQL+= z_sep +"'" + m_car_no + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_service_record SET ";
			if (z_bool.m_crdt_dt)
			{
				SQL+= z_sep +"crdt_dt='" + m_crdt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_next_servicing_at_km)
			{
				SQL+= z_sep +"next_servicing_at_km='" + m_next_servicing_at_km + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_record_id)
			{
				SQL+= z_sep +"service_record_id='" + m_service_record_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + m_remarks + "'";
				z_sep=" , ";
				}
			if (z_bool.m_phone_no)
			{
				SQL+= z_sep +"phone_no='" + m_phone_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_servicing_at_km)
			{
				SQL+= z_sep +"servicing_at_km='" + m_servicing_at_km + "'";
				z_sep=" , ";
				}
			if (z_bool.m_customer_name)
			{
				SQL+= z_sep +"customer_name='" + m_customer_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_car_no)
			{
				SQL+= z_sep +"car_no='" + m_car_no + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_crdt_dt)
			{
				SQL+= z_sep +"crdt_dt='" + z_WhereClause.crdt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_next_servicing_at_km)
			{
				SQL+= z_sep +"next_servicing_at_km='" + z_WhereClause.next_servicing_at_km() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_record_id)
			{
				SQL+= z_sep +"service_record_id='" + z_WhereClause.service_record_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_phone_no)
			{
				SQL+= z_sep +"phone_no='" + z_WhereClause.phone_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_servicing_at_km)
			{
				SQL+= z_sep +"servicing_at_km='" + z_WhereClause.servicing_at_km() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_customer_name)
			{
				SQL+= z_sep +"customer_name='" + z_WhereClause.customer_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_car_no)
			{
				SQL+= z_sep +"car_no='" + z_WhereClause.car_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_service_record";
			if (z_WhereClause.z_bool.m_crdt_dt)
			{
				SQL+= z_sep +"crdt_dt='" + z_WhereClause.crdt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_next_servicing_at_km)
			{
				SQL+= z_sep +"next_servicing_at_km='" + z_WhereClause.next_servicing_at_km() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_record_id)
			{
				SQL+= z_sep +"service_record_id='" + z_WhereClause.service_record_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_phone_no)
			{
				SQL+= z_sep +"phone_no='" + z_WhereClause.phone_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_servicing_at_km)
			{
				SQL+= z_sep +"servicing_at_km='" + z_WhereClause.servicing_at_km() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_customer_name)
			{
				SQL+= z_sep +"customer_name='" + z_WhereClause.customer_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_car_no)
			{
				SQL+= z_sep +"car_no='" + z_WhereClause.car_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_service_record";
			if (z_WhereClause.z_bool.m_crdt_dt)
			{
				SQL+= z_sep +"crdt_dt='" + z_WhereClause.crdt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_next_servicing_at_km)
			{
				SQL+= z_sep +"next_servicing_at_km='" + z_WhereClause.next_servicing_at_km() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_record_id)
			{
				SQL+= z_sep +"service_record_id='" + z_WhereClause.service_record_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_phone_no)
			{
				SQL+= z_sep +"phone_no='" + z_WhereClause.phone_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_servicing_at_km)
			{
				SQL+= z_sep +"servicing_at_km='" + z_WhereClause.servicing_at_km() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_customer_name)
			{
				SQL+= z_sep +"customer_name='" + z_WhereClause.customer_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_car_no)
			{
				SQL+= z_sep +"car_no='" + z_WhereClause.car_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : ServiceRecordModel_Criteria.
	/// </summary>
	public class ServiceRecordModel_Criteria 
	{
		private String m_crdt_dt;
		private int m_next_servicing_at_km;
		private int m_service_record_id;
		private String m_remarks;
		private String m_phone_no;
		private int m_servicing_at_km;
		private String m_customer_name;
		private String m_car_no;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_ServiceRecordModel_Criteria  z_bool;

		public ServiceRecordModel_Criteria()

		{
		z_bool=new IsDirty_ServiceRecordModel_Criteria();  
		}
		public class IsDirty_ServiceRecordModel_Criteria 
		{
			public boolean  m_crdt_dt;
			public boolean  m_next_servicing_at_km;
			public boolean  m_service_record_id;
			public boolean  m_remarks;
			public boolean  m_phone_no;
			public boolean  m_servicing_at_km;
			public boolean  m_customer_name;
			public boolean  m_car_no;
			public boolean  MyWhere;

		}
		public String crdt_dt()
		{
			return m_crdt_dt;
		}
		public void crdt_dt(String value)
		{
			z_bool.m_crdt_dt = true;
			m_crdt_dt = value;
			if (z_bool.m_crdt_dt)
			{
				_zWhereClause += z_sep+"crdt_dt='"+crdt_dt() + "'";
				z_sep=" AND ";
			}
		}
		public int next_servicing_at_km()
		{
			return m_next_servicing_at_km;
		}
		public void next_servicing_at_km(int value)
		{
			z_bool.m_next_servicing_at_km = true;
			m_next_servicing_at_km = value;
			if (z_bool.m_next_servicing_at_km)
			{
				_zWhereClause += z_sep+"next_servicing_at_km='"+next_servicing_at_km() + "'";
				z_sep=" AND ";
			}
		}
		public int service_record_id()
		{
			return m_service_record_id;
		}
		public void service_record_id(int value)
		{
			z_bool.m_service_record_id = true;
			m_service_record_id = value;
			if (z_bool.m_service_record_id)
			{
				_zWhereClause += z_sep+"service_record_id='"+service_record_id() + "'";
				z_sep=" AND ";
			}
		}
		public String remarks()
		{
			return m_remarks;
		}
		public void remarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
			if (z_bool.m_remarks)
			{
				_zWhereClause += z_sep+"remarks='"+remarks() + "'";
				z_sep=" AND ";
			}
		}
		public String phone_no()
		{
			return m_phone_no;
		}
		public void phone_no(String value)
		{
			z_bool.m_phone_no = true;
			m_phone_no = value;
			if (z_bool.m_phone_no)
			{
				_zWhereClause += z_sep+"phone_no='"+phone_no() + "'";
				z_sep=" AND ";
			}
		}
		public int servicing_at_km()
		{
			return m_servicing_at_km;
		}
		public void servicing_at_km(int value)
		{
			z_bool.m_servicing_at_km = true;
			m_servicing_at_km = value;
			if (z_bool.m_servicing_at_km)
			{
				_zWhereClause += z_sep+"servicing_at_km='"+servicing_at_km() + "'";
				z_sep=" AND ";
			}
		}
		public String customer_name()
		{
			return m_customer_name;
		}
		public void customer_name(String value)
		{
			z_bool.m_customer_name = true;
			m_customer_name = value;
			if (z_bool.m_customer_name)
			{
				_zWhereClause += z_sep+"customer_name='"+customer_name() + "'";
				z_sep=" AND ";
			}
		}
		public String car_no()
		{
			return m_car_no;
		}
		public void car_no(String value)
		{
			z_bool.m_car_no = true;
			m_car_no = value;
			if (z_bool.m_car_no)
			{
				_zWhereClause += z_sep+"car_no='"+car_no() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}