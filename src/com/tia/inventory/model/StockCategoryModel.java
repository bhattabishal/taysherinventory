	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : StockCategoryModel.
	/// </summary>
package com.tia.inventory.model;
	public class StockCategoryModel
	{
		private String m_cat_name;
		private int m_cat_status;
		private String m_lineage;
		private int m_cat_stock_type;
		private String m_income_acc;
		private int m_parent_id;
		private int m_updt_cnt;
		private String m_cr_dt;
		private int m_deep;
		private String m_updt_dt;
		private String m_remarks;
		private int m_updt_by;
		private int m_cat_id;
		private int m_br_id;
		private int m_cr_by;
		private String m_assets_acc;
		public StockCategoryModel_Criteria z_WhereClause;

		private IsDirty_StockCategoryModel z_bool;

		public StockCategoryModel()
		{
			z_WhereClause = new StockCategoryModel_Criteria();
			z_bool = new IsDirty_StockCategoryModel();
		}
		public class IsDirty_StockCategoryModel
		{
			public boolean  m_cat_name;
			public boolean  m_cat_status;
			public boolean  m_lineage;
			public boolean  m_cat_stock_type;
			public boolean  m_income_acc;
			public boolean  m_parent_id;
			public boolean  m_updt_cnt;
			public boolean  m_cr_dt;
			public boolean  m_deep;
			public boolean  m_updt_dt;
			public boolean  m_remarks;
			public boolean  m_updt_by;
			public boolean  m_cat_id;
			public boolean  m_br_id;
			public boolean  m_cr_by;
			public boolean  m_assets_acc;
		}
		public String getCat_name()
		{
			return m_cat_name;
		}
		public void setCat_name(String value)
		{
			z_bool.m_cat_name = true;
			m_cat_name = value;
		}
		public int getCat_status()
		{
			return m_cat_status;
		}
		public void setCat_status(int value)
		{
			z_bool.m_cat_status = true;
			m_cat_status = value;
		}
		public String getLineage()
		{
			return m_lineage;
		}
		public void setLineage(String value)
		{
			z_bool.m_lineage = true;
			m_lineage = value;
		}
		public int getCat_stock_type()
		{
			return m_cat_stock_type;
		}
		public void setCat_stock_type(int value)
		{
			z_bool.m_cat_stock_type = true;
			m_cat_stock_type = value;
		}
		public String getIncome_acc()
		{
			return m_income_acc;
		}
		public void setIncome_acc(String value)
		{
			z_bool.m_income_acc = true;
			m_income_acc = value;
		}
		public int getParent_id()
		{
			return m_parent_id;
		}
		public void setParent_id(int value)
		{
			z_bool.m_parent_id = true;
			m_parent_id = value;
		}
		public int getUpdt_cnt()
		{
			return m_updt_cnt;
		}
		public void setUpdt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
		}
		public String getCr_dt()
		{
			return m_cr_dt;
		}
		public void setCr_dt(String value)
		{
			z_bool.m_cr_dt = true;
			m_cr_dt = value;
		}
		public int getDeep()
		{
			return m_deep;
		}
		public void setDeep(int value)
		{
			z_bool.m_deep = true;
			m_deep = value;
		}
		public String getUpdt_dt()
		{
			return m_updt_dt;
		}
		public void setUpdt_dt(String value)
		{
			z_bool.m_updt_dt = true;
			m_updt_dt = value;
		}
		public String getRemarks()
		{
			return m_remarks;
		}
		public void setRemarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
		}
		public int getUpdt_by()
		{
			return m_updt_by;
		}
		public void setUpdt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
		}
		public int getCat_id()
		{
			return m_cat_id;
		}
		public void setCat_id(int value)
		{
			z_bool.m_cat_id = true;
			m_cat_id = value;
		}
		public int getBr_id()
		{
			return m_br_id;
		}
		public void setBr_id(int value)
		{
			z_bool.m_br_id = true;
			m_br_id = value;
		}
		public int getCr_by()
		{
			return m_cr_by;
		}
		public void setCr_by(int value)
		{
			z_bool.m_cr_by = true;
			m_cr_by = value;
		}
		public String getAssets_acc()
		{
			return m_assets_acc;
		}
		public void setAssets_acc(String value)
		{
			z_bool.m_assets_acc = true;
			m_assets_acc = value;
		}
		/*
		public StockCategoryModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public StockCategoryModel_Criteria Where(StockCategoryModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_stock_category ( ";
			if (z_bool.m_cat_name)
			{
				SQL+= z_sep +"cat_name";
				z_sep=" , ";
				}
			if (z_bool.m_cat_status)
			{
				SQL+= z_sep +"cat_status";
				z_sep=" , ";
				}
			if (z_bool.m_lineage)
			{
				SQL+= z_sep +"lineage";
				z_sep=" , ";
				}
			if (z_bool.m_cat_stock_type)
			{
				SQL+= z_sep +"cat_stock_type";
				z_sep=" , ";
				}
			if (z_bool.m_income_acc)
			{
				SQL+= z_sep +"income_acc";
				z_sep=" , ";
				}
			if (z_bool.m_parent_id)
			{
				SQL+= z_sep +"parent_id";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt";
				z_sep=" , ";
				}
			if (z_bool.m_cr_dt)
			{
				SQL+= z_sep +"cr_dt";
				z_sep=" , ";
				}
			if (z_bool.m_deep)
			{
				SQL+= z_sep +"deep";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by";
				z_sep=" , ";
				}
			if (z_bool.m_cat_id)
			{
				SQL+= z_sep +"cat_id";
				z_sep=" , ";
				}
			if (z_bool.m_br_id)
			{
				SQL+= z_sep +"br_id";
				z_sep=" , ";
				}
			if (z_bool.m_cr_by)
			{
				SQL+= z_sep +"cr_by";
				z_sep=" , ";
				}
			if (z_bool.m_assets_acc)
			{
				SQL+= z_sep +"assets_acc";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_cat_name)
			{
				SQL+= z_sep +"'" + m_cat_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cat_status)
			{
				SQL+= z_sep +"'" + m_cat_status + "'";
				z_sep=" , ";
				}
			if (z_bool.m_lineage)
			{
				SQL+= z_sep +"'" + m_lineage + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cat_stock_type)
			{
				SQL+= z_sep +"'" + m_cat_stock_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_income_acc)
			{
				SQL+= z_sep +"'" + m_income_acc + "'";
				z_sep=" , ";
				}
			if (z_bool.m_parent_id)
			{
				SQL+= z_sep +"'" + m_parent_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"'" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cr_dt)
			{
				SQL+= z_sep +"'" + m_cr_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_deep)
			{
				SQL+= z_sep +"'" + m_deep + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"'" + m_updt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"'" + m_remarks + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"'" + m_updt_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cat_id)
			{
				SQL+= z_sep +"'" + m_cat_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_br_id)
			{
				SQL+= z_sep +"'" + m_br_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cr_by)
			{
				SQL+= z_sep +"'" + m_cr_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_assets_acc)
			{
				SQL+= z_sep +"'" + m_assets_acc + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_stock_category SET ";
			if (z_bool.m_cat_name)
			{
				SQL+= z_sep +"cat_name='" + m_cat_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cat_status)
			{
				SQL+= z_sep +"cat_status='" + m_cat_status + "'";
				z_sep=" , ";
				}
			if (z_bool.m_lineage)
			{
				SQL+= z_sep +"lineage='" + m_lineage + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cat_stock_type)
			{
				SQL+= z_sep +"cat_stock_type='" + m_cat_stock_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_income_acc)
			{
				SQL+= z_sep +"income_acc='" + m_income_acc + "'";
				z_sep=" , ";
				}
			if (z_bool.m_parent_id)
			{
				SQL+= z_sep +"parent_id='" + m_parent_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cr_dt)
			{
				SQL+= z_sep +"cr_dt='" + m_cr_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_deep)
			{
				SQL+= z_sep +"deep='" + m_deep + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + m_updt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + m_remarks + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + m_updt_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cat_id)
			{
				SQL+= z_sep +"cat_id='" + m_cat_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_br_id)
			{
				SQL+= z_sep +"br_id='" + m_br_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cr_by)
			{
				SQL+= z_sep +"cr_by='" + m_cr_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_assets_acc)
			{
				SQL+= z_sep +"assets_acc='" + m_assets_acc + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_cat_name)
			{
				SQL+= z_sep +"cat_name='" + z_WhereClause.cat_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cat_status)
			{
				SQL+= z_sep +"cat_status='" + z_WhereClause.cat_status() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_lineage)
			{
				SQL+= z_sep +"lineage='" + z_WhereClause.lineage() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cat_stock_type)
			{
				SQL+= z_sep +"cat_stock_type='" + z_WhereClause.cat_stock_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_income_acc)
			{
				SQL+= z_sep +"income_acc='" + z_WhereClause.income_acc() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_parent_id)
			{
				SQL+= z_sep +"parent_id='" + z_WhereClause.parent_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cr_dt)
			{
				SQL+= z_sep +"cr_dt='" + z_WhereClause.cr_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_deep)
			{
				SQL+= z_sep +"deep='" + z_WhereClause.deep() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cat_id)
			{
				SQL+= z_sep +"cat_id='" + z_WhereClause.cat_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_br_id)
			{
				SQL+= z_sep +"br_id='" + z_WhereClause.br_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cr_by)
			{
				SQL+= z_sep +"cr_by='" + z_WhereClause.cr_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_assets_acc)
			{
				SQL+= z_sep +"assets_acc='" + z_WhereClause.assets_acc() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_stock_category";
			if (z_WhereClause.z_bool.m_cat_name)
			{
				SQL+= z_sep +"cat_name='" + z_WhereClause.cat_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cat_status)
			{
				SQL+= z_sep +"cat_status='" + z_WhereClause.cat_status() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_lineage)
			{
				SQL+= z_sep +"lineage='" + z_WhereClause.lineage() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cat_stock_type)
			{
				SQL+= z_sep +"cat_stock_type='" + z_WhereClause.cat_stock_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_income_acc)
			{
				SQL+= z_sep +"income_acc='" + z_WhereClause.income_acc() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_parent_id)
			{
				SQL+= z_sep +"parent_id='" + z_WhereClause.parent_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cr_dt)
			{
				SQL+= z_sep +"cr_dt='" + z_WhereClause.cr_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_deep)
			{
				SQL+= z_sep +"deep='" + z_WhereClause.deep() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cat_id)
			{
				SQL+= z_sep +"cat_id='" + z_WhereClause.cat_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_br_id)
			{
				SQL+= z_sep +"br_id='" + z_WhereClause.br_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cr_by)
			{
				SQL+= z_sep +"cr_by='" + z_WhereClause.cr_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_assets_acc)
			{
				SQL+= z_sep +"assets_acc='" + z_WhereClause.assets_acc() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_stock_category";
			if (z_WhereClause.z_bool.m_cat_name)
			{
				SQL+= z_sep +"cat_name='" + z_WhereClause.cat_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cat_status)
			{
				SQL+= z_sep +"cat_status='" + z_WhereClause.cat_status() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_lineage)
			{
				SQL+= z_sep +"lineage='" + z_WhereClause.lineage() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cat_stock_type)
			{
				SQL+= z_sep +"cat_stock_type='" + z_WhereClause.cat_stock_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_income_acc)
			{
				SQL+= z_sep +"income_acc='" + z_WhereClause.income_acc() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_parent_id)
			{
				SQL+= z_sep +"parent_id='" + z_WhereClause.parent_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cr_dt)
			{
				SQL+= z_sep +"cr_dt='" + z_WhereClause.cr_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_deep)
			{
				SQL+= z_sep +"deep='" + z_WhereClause.deep() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cat_id)
			{
				SQL+= z_sep +"cat_id='" + z_WhereClause.cat_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_br_id)
			{
				SQL+= z_sep +"br_id='" + z_WhereClause.br_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cr_by)
			{
				SQL+= z_sep +"cr_by='" + z_WhereClause.cr_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_assets_acc)
			{
				SQL+= z_sep +"assets_acc='" + z_WhereClause.assets_acc() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : StockCategoryModel_Criteria.
	/// </summary>
	public class StockCategoryModel_Criteria 
	{
		private String m_cat_name;
		private int m_cat_status;
		private String m_lineage;
		private int m_cat_stock_type;
		private String m_income_acc;
		private int m_parent_id;
		private int m_updt_cnt;
		private String m_cr_dt;
		private int m_deep;
		private String m_updt_dt;
		private String m_remarks;
		private int m_updt_by;
		private int m_cat_id;
		private int m_br_id;
		private int m_cr_by;
		private String m_assets_acc;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_StockCategoryModel_Criteria  z_bool;

		public StockCategoryModel_Criteria()

		{
		z_bool=new IsDirty_StockCategoryModel_Criteria();  
		}
		public class IsDirty_StockCategoryModel_Criteria 
		{
			public boolean  m_cat_name;
			public boolean  m_cat_status;
			public boolean  m_lineage;
			public boolean  m_cat_stock_type;
			public boolean  m_income_acc;
			public boolean  m_parent_id;
			public boolean  m_updt_cnt;
			public boolean  m_cr_dt;
			public boolean  m_deep;
			public boolean  m_updt_dt;
			public boolean  m_remarks;
			public boolean  m_updt_by;
			public boolean  m_cat_id;
			public boolean  m_br_id;
			public boolean  m_cr_by;
			public boolean  m_assets_acc;
			public boolean  MyWhere;

		}
		public String cat_name()
		{
			return m_cat_name;
		}
		public void cat_name(String value)
		{
			z_bool.m_cat_name = true;
			m_cat_name = value;
			if (z_bool.m_cat_name)
			{
				_zWhereClause += z_sep+"cat_name='"+cat_name() + "'";
				z_sep=" AND ";
			}
		}
		public int cat_status()
		{
			return m_cat_status;
		}
		public void cat_status(int value)
		{
			z_bool.m_cat_status = true;
			m_cat_status = value;
			if (z_bool.m_cat_status)
			{
				_zWhereClause += z_sep+"cat_status='"+cat_status() + "'";
				z_sep=" AND ";
			}
		}
		public String lineage()
		{
			return m_lineage;
		}
		public void lineage(String value)
		{
			z_bool.m_lineage = true;
			m_lineage = value;
			if (z_bool.m_lineage)
			{
				_zWhereClause += z_sep+"lineage='"+lineage() + "'";
				z_sep=" AND ";
			}
		}
		public int cat_stock_type()
		{
			return m_cat_stock_type;
		}
		public void cat_stock_type(int value)
		{
			z_bool.m_cat_stock_type = true;
			m_cat_stock_type = value;
			if (z_bool.m_cat_stock_type)
			{
				_zWhereClause += z_sep+"cat_stock_type='"+cat_stock_type() + "'";
				z_sep=" AND ";
			}
		}
		public String income_acc()
		{
			return m_income_acc;
		}
		public void income_acc(String value)
		{
			z_bool.m_income_acc = true;
			m_income_acc = value;
			if (z_bool.m_income_acc)
			{
				_zWhereClause += z_sep+"income_acc='"+income_acc() + "'";
				z_sep=" AND ";
			}
		}
		public int parent_id()
		{
			return m_parent_id;
		}
		public void parent_id(int value)
		{
			z_bool.m_parent_id = true;
			m_parent_id = value;
			if (z_bool.m_parent_id)
			{
				_zWhereClause += z_sep+"parent_id='"+parent_id() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_cnt()
		{
			return m_updt_cnt;
		}
		public void updt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
			if (z_bool.m_updt_cnt)
			{
				_zWhereClause += z_sep+"updt_cnt='"+updt_cnt() + "'";
				z_sep=" AND ";
			}
		}
		public String cr_dt()
		{
			return m_cr_dt;
		}
		public void cr_dt(String value)
		{
			z_bool.m_cr_dt = true;
			m_cr_dt = value;
			if (z_bool.m_cr_dt)
			{
				_zWhereClause += z_sep+"cr_dt='"+cr_dt() + "'";
				z_sep=" AND ";
			}
		}
		public int deep()
		{
			return m_deep;
		}
		public void deep(int value)
		{
			z_bool.m_deep = true;
			m_deep = value;
			if (z_bool.m_deep)
			{
				_zWhereClause += z_sep+"deep='"+deep() + "'";
				z_sep=" AND ";
			}
		}
		public String updt_dt()
		{
			return m_updt_dt;
		}
		public void updt_dt(String value)
		{
			z_bool.m_updt_dt = true;
			m_updt_dt = value;
			if (z_bool.m_updt_dt)
			{
				_zWhereClause += z_sep+"updt_dt='"+updt_dt() + "'";
				z_sep=" AND ";
			}
		}
		public String remarks()
		{
			return m_remarks;
		}
		public void remarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
			if (z_bool.m_remarks)
			{
				_zWhereClause += z_sep+"remarks='"+remarks() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_by()
		{
			return m_updt_by;
		}
		public void updt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
			if (z_bool.m_updt_by)
			{
				_zWhereClause += z_sep+"updt_by='"+updt_by() + "'";
				z_sep=" AND ";
			}
		}
		public int cat_id()
		{
			return m_cat_id;
		}
		public void cat_id(int value)
		{
			z_bool.m_cat_id = true;
			m_cat_id = value;
			if (z_bool.m_cat_id)
			{
				_zWhereClause += z_sep+"cat_id='"+cat_id() + "'";
				z_sep=" AND ";
			}
		}
		public int br_id()
		{
			return m_br_id;
		}
		public void br_id(int value)
		{
			z_bool.m_br_id = true;
			m_br_id = value;
			if (z_bool.m_br_id)
			{
				_zWhereClause += z_sep+"br_id='"+br_id() + "'";
				z_sep=" AND ";
			}
		}
		public int cr_by()
		{
			return m_cr_by;
		}
		public void cr_by(int value)
		{
			z_bool.m_cr_by = true;
			m_cr_by = value;
			if (z_bool.m_cr_by)
			{
				_zWhereClause += z_sep+"cr_by='"+cr_by() + "'";
				z_sep=" AND ";
			}
		}
		public String assets_acc()
		{
			return m_assets_acc;
		}
		public void assets_acc(String value)
		{
			z_bool.m_assets_acc = true;
			m_assets_acc = value;
			if (z_bool.m_assets_acc)
			{
				_zWhereClause += z_sep+"assets_acc='"+assets_acc() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}