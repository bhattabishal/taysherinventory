	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : DayBookDetailsModel.
	/// </summary>
package com.tia.inventory.model;

import java.text.DecimalFormat;
	public class DayBookDetailsModel
	{
		private String m_expense;
		private int m_trxn_with_type_id;
		private String m_created_dt;
		private int m_day_book_id;
		private int m_created_by;
		private int m_day_book_details_id;
		private int m_updt_cnt;
		private String m_updt_dt;
		private int m_updt_by;
		private int m_trxn_with_id;
		private String m_income;
		public DayBookDetailsModel_Criteria z_WhereClause;

		private IsDirty_DayBookDetailsModel z_bool;

		public DayBookDetailsModel()
		{
			z_WhereClause = new DayBookDetailsModel_Criteria();
			z_bool = new IsDirty_DayBookDetailsModel();
		}
		public class IsDirty_DayBookDetailsModel
		{
			public boolean  m_expense;
			public boolean  m_trxn_with_type_id;
			public boolean  m_created_dt;
			public boolean  m_day_book_id;
			public boolean  m_created_by;
			public boolean  m_day_book_details_id;
			public boolean  m_updt_cnt;
			public boolean  m_updt_dt;
			public boolean  m_updt_by;
			public boolean  m_trxn_with_id;
			public boolean  m_income;
		}
		public String getExpense()
		{
			return m_expense;
		}
		public void setExpense(double value)
		{
			z_bool.m_expense = true;
			DecimalFormat df = new DecimalFormat("#.##");
			m_expense = df.format(value);
		}
		public int getTrxn_with_type_id()
		{
			return m_trxn_with_type_id;
		}
		public void setTrxn_with_type_id(int value)
		{
			z_bool.m_trxn_with_type_id = true;
			m_trxn_with_type_id = value;
		}
		public String getCreated_dt()
		{
			return m_created_dt;
		}
		public void setCreated_dt(String value)
		{
			z_bool.m_created_dt = true;
			m_created_dt = value;
		}
		public int getDay_book_id()
		{
			return m_day_book_id;
		}
		public void setDay_book_id(int value)
		{
			z_bool.m_day_book_id = true;
			m_day_book_id = value;
		}
		public int getCreated_by()
		{
			return m_created_by;
		}
		public void setCreated_by(int value)
		{
			z_bool.m_created_by = true;
			m_created_by = value;
		}
		public int getDay_book_details_id()
		{
			return m_day_book_details_id;
		}
		public void setDay_book_details_id(int value)
		{
			z_bool.m_day_book_details_id = true;
			m_day_book_details_id = value;
		}
		public int getUpdt_cnt()
		{
			return m_updt_cnt;
		}
		public void setUpdt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
		}
		public String getUpdt_dt()
		{
			return m_updt_dt;
		}
		public void setUpdt_dt(String value)
		{
			z_bool.m_updt_dt = true;
			m_updt_dt = value;
		}
		public int getUpdt_by()
		{
			return m_updt_by;
		}
		public void setUpdt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
		}
		public int getTrxn_with_id()
		{
			return m_trxn_with_id;
		}
		public void setTrxn_with_id(int value)
		{
			z_bool.m_trxn_with_id = true;
			m_trxn_with_id = value;
		}
		public String getIncome()
		{
			return m_income;
		}
		public void setIncome(double value)
		{
			z_bool.m_income = true;
			DecimalFormat df = new DecimalFormat("#.##");
			m_income = df.format(value);
		}
		/*
		public DayBookDetailsModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public DayBookDetailsModel_Criteria Where(DayBookDetailsModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO day_book_details ( ";
			if (z_bool.m_expense)
			{
				SQL+= z_sep +"expense";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_with_type_id)
			{
				SQL+= z_sep +"trxn_with_type_id";
				z_sep=" , ";
				}
			if (z_bool.m_created_dt)
			{
				SQL+= z_sep +"created_dt";
				z_sep=" , ";
				}
			if (z_bool.m_day_book_id)
			{
				SQL+= z_sep +"day_book_id";
				z_sep=" , ";
				}
			if (z_bool.m_created_by)
			{
				SQL+= z_sep +"created_by";
				z_sep=" , ";
				}
			if (z_bool.m_day_book_details_id)
			{
				SQL+= z_sep +"day_book_details_id";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_with_id)
			{
				SQL+= z_sep +"trxn_with_id";
				z_sep=" , ";
				}
			if (z_bool.m_income)
			{
				SQL+= z_sep +"income";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_expense)
			{
				SQL+= z_sep +"'" + m_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_with_type_id)
			{
				SQL+= z_sep +"'" + m_trxn_with_type_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_created_dt)
			{
				SQL+= z_sep +"'" + m_created_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_day_book_id)
			{
				SQL+= z_sep +"'" + m_day_book_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_created_by)
			{
				SQL+= z_sep +"'" + m_created_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_day_book_details_id)
			{
				SQL+= z_sep +"'" + m_day_book_details_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"'" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"'" + m_updt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"'" + m_updt_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_with_id)
			{
				SQL+= z_sep +"'" + m_trxn_with_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_income)
			{
				SQL+= z_sep +"'" + m_income + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE day_book_details SET ";
			if (z_bool.m_expense)
			{
				SQL+= z_sep +"expense='" + m_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_with_type_id)
			{
				SQL+= z_sep +"trxn_with_type_id='" + m_trxn_with_type_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_created_dt)
			{
				SQL+= z_sep +"created_dt='" + m_created_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_day_book_id)
			{
				SQL+= z_sep +"day_book_id='" + m_day_book_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_created_by)
			{
				SQL+= z_sep +"created_by='" + m_created_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_day_book_details_id)
			{
				SQL+= z_sep +"day_book_details_id='" + m_day_book_details_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + m_updt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + m_updt_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_with_id)
			{
				SQL+= z_sep +"trxn_with_id='" + m_trxn_with_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_income)
			{
				SQL+= z_sep +"income='" + m_income + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_expense)
			{
				SQL+= z_sep +"expense='" + z_WhereClause.expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_with_type_id)
			{
				SQL+= z_sep +"trxn_with_type_id='" + z_WhereClause.trxn_with_type_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_dt)
			{
				SQL+= z_sep +"created_dt='" + z_WhereClause.created_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_day_book_id)
			{
				SQL+= z_sep +"day_book_id='" + z_WhereClause.day_book_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_by)
			{
				SQL+= z_sep +"created_by='" + z_WhereClause.created_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_day_book_details_id)
			{
				SQL+= z_sep +"day_book_details_id='" + z_WhereClause.day_book_details_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_with_id)
			{
				SQL+= z_sep +"trxn_with_id='" + z_WhereClause.trxn_with_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_income)
			{
				SQL+= z_sep +"income='" + z_WhereClause.income() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM day_book_details";
			if (z_WhereClause.z_bool.m_expense)
			{
				SQL+= z_sep +"expense='" + z_WhereClause.expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_with_type_id)
			{
				SQL+= z_sep +"trxn_with_type_id='" + z_WhereClause.trxn_with_type_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_dt)
			{
				SQL+= z_sep +"created_dt='" + z_WhereClause.created_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_day_book_id)
			{
				SQL+= z_sep +"day_book_id='" + z_WhereClause.day_book_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_by)
			{
				SQL+= z_sep +"created_by='" + z_WhereClause.created_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_day_book_details_id)
			{
				SQL+= z_sep +"day_book_details_id='" + z_WhereClause.day_book_details_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_with_id)
			{
				SQL+= z_sep +"trxn_with_id='" + z_WhereClause.trxn_with_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_income)
			{
				SQL+= z_sep +"income='" + z_WhereClause.income() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM day_book_details";
			if (z_WhereClause.z_bool.m_expense)
			{
				SQL+= z_sep +"expense='" + z_WhereClause.expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_with_type_id)
			{
				SQL+= z_sep +"trxn_with_type_id='" + z_WhereClause.trxn_with_type_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_dt)
			{
				SQL+= z_sep +"created_dt='" + z_WhereClause.created_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_day_book_id)
			{
				SQL+= z_sep +"day_book_id='" + z_WhereClause.day_book_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_by)
			{
				SQL+= z_sep +"created_by='" + z_WhereClause.created_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_day_book_details_id)
			{
				SQL+= z_sep +"day_book_details_id='" + z_WhereClause.day_book_details_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_with_id)
			{
				SQL+= z_sep +"trxn_with_id='" + z_WhereClause.trxn_with_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_income)
			{
				SQL+= z_sep +"income='" + z_WhereClause.income() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : DayBookDetailsModel_Criteria.
	/// </summary>
	public class DayBookDetailsModel_Criteria 
	{
		private int m_expense;
		private int m_trxn_with_type_id;
		private String m_created_dt;
		private int m_day_book_id;
		private int m_created_by;
		private int m_day_book_details_id;
		private int m_updt_cnt;
		private String m_updt_dt;
		private int m_updt_by;
		private int m_trxn_with_id;
		private int m_income;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_DayBookDetailsModel_Criteria  z_bool;

		public DayBookDetailsModel_Criteria()

		{
		z_bool=new IsDirty_DayBookDetailsModel_Criteria();  
		}
		public class IsDirty_DayBookDetailsModel_Criteria 
		{
			public boolean  m_expense;
			public boolean  m_trxn_with_type_id;
			public boolean  m_created_dt;
			public boolean  m_day_book_id;
			public boolean  m_created_by;
			public boolean  m_day_book_details_id;
			public boolean  m_updt_cnt;
			public boolean  m_updt_dt;
			public boolean  m_updt_by;
			public boolean  m_trxn_with_id;
			public boolean  m_income;
			public boolean  MyWhere;

		}
		public int expense()
		{
			return m_expense;
		}
		public void expense(int value)
		{
			z_bool.m_expense = true;
			m_expense = value;
			if (z_bool.m_expense)
			{
				_zWhereClause += z_sep+"expense='"+expense() + "'";
				z_sep=" AND ";
			}
		}
		public int trxn_with_type_id()
		{
			return m_trxn_with_type_id;
		}
		public void trxn_with_type_id(int value)
		{
			z_bool.m_trxn_with_type_id = true;
			m_trxn_with_type_id = value;
			if (z_bool.m_trxn_with_type_id)
			{
				_zWhereClause += z_sep+"trxn_with_type_id='"+trxn_with_type_id() + "'";
				z_sep=" AND ";
			}
		}
		public String created_dt()
		{
			return m_created_dt;
		}
		public void created_dt(String value)
		{
			z_bool.m_created_dt = true;
			m_created_dt = value;
			if (z_bool.m_created_dt)
			{
				_zWhereClause += z_sep+"created_dt='"+created_dt() + "'";
				z_sep=" AND ";
			}
		}
		public int day_book_id()
		{
			return m_day_book_id;
		}
		public void day_book_id(int value)
		{
			z_bool.m_day_book_id = true;
			m_day_book_id = value;
			if (z_bool.m_day_book_id)
			{
				_zWhereClause += z_sep+"day_book_id='"+day_book_id() + "'";
				z_sep=" AND ";
			}
		}
		public int created_by()
		{
			return m_created_by;
		}
		public void created_by(int value)
		{
			z_bool.m_created_by = true;
			m_created_by = value;
			if (z_bool.m_created_by)
			{
				_zWhereClause += z_sep+"created_by='"+created_by() + "'";
				z_sep=" AND ";
			}
		}
		public int day_book_details_id()
		{
			return m_day_book_details_id;
		}
		public void day_book_details_id(int value)
		{
			z_bool.m_day_book_details_id = true;
			m_day_book_details_id = value;
			if (z_bool.m_day_book_details_id)
			{
				_zWhereClause += z_sep+"day_book_details_id='"+day_book_details_id() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_cnt()
		{
			return m_updt_cnt;
		}
		public void updt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
			if (z_bool.m_updt_cnt)
			{
				_zWhereClause += z_sep+"updt_cnt='"+updt_cnt() + "'";
				z_sep=" AND ";
			}
		}
		public String updt_dt()
		{
			return m_updt_dt;
		}
		public void updt_dt(String value)
		{
			z_bool.m_updt_dt = true;
			m_updt_dt = value;
			if (z_bool.m_updt_dt)
			{
				_zWhereClause += z_sep+"updt_dt='"+updt_dt() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_by()
		{
			return m_updt_by;
		}
		public void updt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
			if (z_bool.m_updt_by)
			{
				_zWhereClause += z_sep+"updt_by='"+updt_by() + "'";
				z_sep=" AND ";
			}
		}
		public int trxn_with_id()
		{
			return m_trxn_with_id;
		}
		public void trxn_with_id(int value)
		{
			z_bool.m_trxn_with_id = true;
			m_trxn_with_id = value;
			if (z_bool.m_trxn_with_id)
			{
				_zWhereClause += z_sep+"trxn_with_id='"+trxn_with_id() + "'";
				z_sep=" AND ";
			}
		}
		public int income()
		{
			return m_income;
		}
		public void income(int value)
		{
			z_bool.m_income = true;
			m_income = value;
			if (z_bool.m_income)
			{
				_zWhereClause += z_sep+"income='"+income() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}