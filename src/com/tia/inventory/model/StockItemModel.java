	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : StockItemModel.
	/// </summary>
package com.tia.inventory.model;
	public class StockItemModel
	{
		private int m_crtd_by;
		private String m_crtd_dt;
		private int m_updt_by;
		private int m_item_id;
		private String m_item_name;
		private String m_item_rack_no;
		private String m_unit_weight;
		private int m_item_pur_unit_id;
		private String m_item_description;
		private int m_updt_cnt;
		private String m_updt_dt;
		private int m_item_min_stock;
		private String m_item_code;
		private int m_item_sel_unit_id;
		private String m_item_selling_price;
		private int m_cat_id;
		private int m_fy_id;
		public StockItemModel_Criteria z_WhereClause;

		private IsDirty_StockItemModel z_bool;

		public StockItemModel()
		{
			z_WhereClause = new StockItemModel_Criteria();
			z_bool = new IsDirty_StockItemModel();
		}
		public class IsDirty_StockItemModel
		{
			public boolean  m_crtd_by;
			public boolean  m_crtd_dt;
			public boolean  m_updt_by;
			public boolean  m_item_id;
			public boolean  m_item_name;
			public boolean  m_item_rack_no;
			public boolean  m_unit_weight;
			public boolean  m_item_pur_unit_id;
			public boolean  m_item_description;
			public boolean  m_updt_cnt;
			public boolean  m_updt_dt;
			public boolean  m_item_min_stock;
			public boolean  m_item_code;
			public boolean  m_item_sel_unit_id;
			public boolean  m_item_selling_price;
			public boolean  m_cat_id;
			public boolean  m_fy_id;
		}
		public int getCrtd_by()
		{
			return m_crtd_by;
		}
		public void setCrtd_by(int value)
		{
			z_bool.m_crtd_by = true;
			m_crtd_by = value;
		}
		public String getCrtd_dt()
		{
			return m_crtd_dt;
		}
		public void setCrtd_dt(String value)
		{
			z_bool.m_crtd_dt = true;
			m_crtd_dt = value;
		}
		public int getUpdt_by()
		{
			return m_updt_by;
		}
		public void setUpdt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
		}
		public int getItem_id()
		{
			return m_item_id;
		}
		public void setItem_id(int value)
		{
			z_bool.m_item_id = true;
			m_item_id = value;
		}
		public String getItem_name()
		{
			return m_item_name;
		}
		public void setItem_name(String value)
		{
			z_bool.m_item_name = true;
			m_item_name = value;
		}
		public String getItem_rack_no()
		{
			return m_item_rack_no;
		}
		public void setItem_rack_no(String value)
		{
			z_bool.m_item_rack_no = true;
			m_item_rack_no = value;
		}
		public String getUnit_weight()
		{
			return m_unit_weight;
		}
		public void setUnit_weight(String value)
		{
			z_bool.m_unit_weight = true;
			m_unit_weight = value;
		}
		public int getItem_pur_unit_id()
		{
			return m_item_pur_unit_id;
		}
		public void setItem_pur_unit_id(int value)
		{
			z_bool.m_item_pur_unit_id = true;
			m_item_pur_unit_id = value;
		}
		public String getItem_description()
		{
			return m_item_description;
		}
		public void setItem_description(String value)
		{
			z_bool.m_item_description = true;
			m_item_description = value;
		}
		public int getUpdt_cnt()
		{
			return m_updt_cnt;
		}
		public void setUpdt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
		}
		public String getUpdt_dt()
		{
			return m_updt_dt;
		}
		public void setUpdt_dt(String value)
		{
			z_bool.m_updt_dt = true;
			m_updt_dt = value;
		}
		public int getItem_min_stock()
		{
			return m_item_min_stock;
		}
		public void setItem_min_stock(int value)
		{
			z_bool.m_item_min_stock = true;
			m_item_min_stock = value;
		}
		public String getItem_code()
		{
			return m_item_code;
		}
		public void setItem_code(String value)
		{
			z_bool.m_item_code = true;
			m_item_code = value;
		}
		public int getItem_sel_unit_id()
		{
			return m_item_sel_unit_id;
		}
		public void setItem_sel_unit_id(int value)
		{
			z_bool.m_item_sel_unit_id = true;
			m_item_sel_unit_id = value;
		}
		public String getItem_selling_price()
		{
			return m_item_selling_price;
		}
		public void setItem_selling_price(String value)
		{
			z_bool.m_item_selling_price = true;
			m_item_selling_price = value;
		}
		public int getCat_id()
		{
			return m_cat_id;
		}
		public void setCat_id(int value)
		{
			z_bool.m_cat_id = true;
			m_cat_id = value;
		}
		public int getFy_id()
		{
			return m_fy_id;
		}
		public void setFy_id(int value)
		{
			z_bool.m_fy_id = true;
			m_fy_id = value;
		}
		/*
		public StockItemModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public StockItemModel_Criteria Where(StockItemModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_item ( ";
			if (z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by";
				z_sep=" , ";
				}
//			if (z_bool.m_item_id)
//			{
//				SQL+= z_sep +"item_id";
//				z_sep=" , ";
//				}
			if (z_bool.m_item_name)
			{
				SQL+= z_sep +"item_name";
				z_sep=" , ";
				}
			if (z_bool.m_item_rack_no)
			{
				SQL+= z_sep +"item_rack_no";
				z_sep=" , ";
				}
			if (z_bool.m_unit_weight)
			{
				SQL+= z_sep +"unit_weight";
				z_sep=" , ";
				}
			if (z_bool.m_item_pur_unit_id)
			{
				SQL+= z_sep +"item_pur_unit_id";
				z_sep=" , ";
				}
			if (z_bool.m_item_description)
			{
				SQL+= z_sep +"item_description";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt";
				z_sep=" , ";
				}
			if (z_bool.m_item_min_stock)
			{
				SQL+= z_sep +"item_min_stock";
				z_sep=" , ";
				}
			if (z_bool.m_item_code)
			{
				SQL+= z_sep +"item_code";
				z_sep=" , ";
				}
			if (z_bool.m_item_sel_unit_id)
			{
				SQL+= z_sep +"item_sel_unit_id";
				z_sep=" , ";
				}
			if (z_bool.m_item_selling_price)
			{
				SQL+= z_sep +"item_selling_price";
				z_sep=" , ";
				}
			if (z_bool.m_cat_id)
			{
				SQL+= z_sep +"cat_id";
				z_sep=" , ";
				}
			if (z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_crtd_by)
			{
				SQL+= z_sep +"'" + m_crtd_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"'" + m_crtd_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"'" + m_updt_by + "'";
				z_sep=" , ";
				}
//			if (z_bool.m_item_id)
//			{
//				SQL+= z_sep +"'" + m_item_id + "'";
//				z_sep=" , ";
//				}
			if (z_bool.m_item_name)
			{
				SQL+= z_sep +"'" + m_item_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_rack_no)
			{
				SQL+= z_sep +"'" + m_item_rack_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_unit_weight)
			{
				SQL+= z_sep +"'" + m_unit_weight + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_pur_unit_id)
			{
				SQL+= z_sep +"'" + m_item_pur_unit_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_description)
			{
				SQL+= z_sep +"'" + m_item_description + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"'" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"'" + m_updt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_min_stock)
			{
				SQL+= z_sep +"'" + m_item_min_stock + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_code)
			{
				SQL+= z_sep +"'" + m_item_code + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_sel_unit_id)
			{
				SQL+= z_sep +"'" + m_item_sel_unit_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_selling_price)
			{
				SQL+= z_sep +"'" + m_item_selling_price + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cat_id)
			{
				SQL+= z_sep +"'" + m_cat_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_fy_id)
			{
				SQL+= z_sep +"'" + m_fy_id + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_item SET ";
			if (z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + m_crtd_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + m_crtd_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + m_updt_by + "'";
				z_sep=" , ";
				}
//			if (z_bool.m_item_id)
//			{
//				SQL+= z_sep +"item_id='" + m_item_id + "'";
//				z_sep=" , ";
//				}
			if (z_bool.m_item_name)
			{
				SQL+= z_sep +"item_name='" + m_item_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_rack_no)
			{
				SQL+= z_sep +"item_rack_no='" + m_item_rack_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_unit_weight)
			{
				SQL+= z_sep +"unit_weight='" + m_unit_weight + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_pur_unit_id)
			{
				SQL+= z_sep +"item_pur_unit_id='" + m_item_pur_unit_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_description)
			{
				SQL+= z_sep +"item_description='" + m_item_description + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + m_updt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_min_stock)
			{
				SQL+= z_sep +"item_min_stock='" + m_item_min_stock + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_code)
			{
				SQL+= z_sep +"item_code='" + m_item_code + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_sel_unit_id)
			{
				SQL+= z_sep +"item_sel_unit_id='" + m_item_sel_unit_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_selling_price)
			{
				SQL+= z_sep +"item_selling_price='" + m_item_selling_price + "'";
				z_sep=" , ";
				}
			if (z_bool.m_cat_id)
			{
				SQL+= z_sep +"cat_id='" + m_cat_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + m_fy_id + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + z_WhereClause.crtd_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_id)
			{
				SQL+= z_sep +"item_id='" + z_WhereClause.item_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_name)
			{
				SQL+= z_sep +"item_name='" + z_WhereClause.item_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_rack_no)
			{
				SQL+= z_sep +"item_rack_no='" + z_WhereClause.item_rack_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit_weight)
			{
				SQL+= z_sep +"unit_weight='" + z_WhereClause.unit_weight() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_pur_unit_id)
			{
				SQL+= z_sep +"item_pur_unit_id='" + z_WhereClause.item_pur_unit_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_description)
			{
				SQL+= z_sep +"item_description='" + z_WhereClause.item_description() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_min_stock)
			{
				SQL+= z_sep +"item_min_stock='" + z_WhereClause.item_min_stock() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_code)
			{
				SQL+= z_sep +"item_code='" + z_WhereClause.item_code() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sel_unit_id)
			{
				SQL+= z_sep +"item_sel_unit_id='" + z_WhereClause.item_sel_unit_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_selling_price)
			{
				SQL+= z_sep +"item_selling_price='" + z_WhereClause.item_selling_price() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cat_id)
			{
				SQL+= z_sep +"cat_id='" + z_WhereClause.cat_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + z_WhereClause.fy_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_item";
			if (z_WhereClause.z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + z_WhereClause.crtd_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_id)
			{
				SQL+= z_sep +"item_id='" + z_WhereClause.item_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_name)
			{
				SQL+= z_sep +"item_name='" + z_WhereClause.item_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_rack_no)
			{
				SQL+= z_sep +"item_rack_no='" + z_WhereClause.item_rack_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit_weight)
			{
				SQL+= z_sep +"unit_weight='" + z_WhereClause.unit_weight() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_pur_unit_id)
			{
				SQL+= z_sep +"item_pur_unit_id='" + z_WhereClause.item_pur_unit_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_description)
			{
				SQL+= z_sep +"item_description='" + z_WhereClause.item_description() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_min_stock)
			{
				SQL+= z_sep +"item_min_stock='" + z_WhereClause.item_min_stock() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_code)
			{
				SQL+= z_sep +"item_code='" + z_WhereClause.item_code() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sel_unit_id)
			{
				SQL+= z_sep +"item_sel_unit_id='" + z_WhereClause.item_sel_unit_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_selling_price)
			{
				SQL+= z_sep +"item_selling_price='" + z_WhereClause.item_selling_price() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cat_id)
			{
				SQL+= z_sep +"cat_id='" + z_WhereClause.cat_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + z_WhereClause.fy_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_item";
			if (z_WhereClause.z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + z_WhereClause.crtd_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_id)
			{
				SQL+= z_sep +"item_id='" + z_WhereClause.item_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_name)
			{
				SQL+= z_sep +"item_name='" + z_WhereClause.item_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_rack_no)
			{
				SQL+= z_sep +"item_rack_no='" + z_WhereClause.item_rack_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit_weight)
			{
				SQL+= z_sep +"unit_weight='" + z_WhereClause.unit_weight() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_pur_unit_id)
			{
				SQL+= z_sep +"item_pur_unit_id='" + z_WhereClause.item_pur_unit_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_description)
			{
				SQL+= z_sep +"item_description='" + z_WhereClause.item_description() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_min_stock)
			{
				SQL+= z_sep +"item_min_stock='" + z_WhereClause.item_min_stock() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_code)
			{
				SQL+= z_sep +"item_code='" + z_WhereClause.item_code() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_sel_unit_id)
			{
				SQL+= z_sep +"item_sel_unit_id='" + z_WhereClause.item_sel_unit_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_selling_price)
			{
				SQL+= z_sep +"item_selling_price='" + z_WhereClause.item_selling_price() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_cat_id)
			{
				SQL+= z_sep +"cat_id='" + z_WhereClause.cat_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + z_WhereClause.fy_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : StockItemModel_Criteria.
	/// </summary>
	public class StockItemModel_Criteria 
	{
		private int m_crtd_by;
		private String m_crtd_dt;
		private int m_updt_by;
		private int m_item_id;
		private String m_item_name;
		private String m_item_rack_no;
		private int m_unit_weight;
		private int m_item_pur_unit_id;
		private String m_item_description;
		private int m_updt_cnt;
		private String m_updt_dt;
		private int m_item_min_stock;
		private String m_item_code;
		private int m_item_sel_unit_id;
		private int m_item_selling_price;
		private int m_cat_id;
		private int m_fy_id;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_StockItemModel_Criteria  z_bool;

		public StockItemModel_Criteria()

		{
		z_bool=new IsDirty_StockItemModel_Criteria();  
		}
		public class IsDirty_StockItemModel_Criteria 
		{
			public boolean  m_crtd_by;
			public boolean  m_crtd_dt;
			public boolean  m_updt_by;
			public boolean  m_item_id;
			public boolean  m_item_name;
			public boolean  m_item_rack_no;
			public boolean  m_unit_weight;
			public boolean  m_item_pur_unit_id;
			public boolean  m_item_description;
			public boolean  m_updt_cnt;
			public boolean  m_updt_dt;
			public boolean  m_item_min_stock;
			public boolean  m_item_code;
			public boolean  m_item_sel_unit_id;
			public boolean  m_item_selling_price;
			public boolean  m_cat_id;
			public boolean  m_fy_id;
			public boolean  MyWhere;

		}
		public int crtd_by()
		{
			return m_crtd_by;
		}
		public void crtd_by(int value)
		{
			z_bool.m_crtd_by = true;
			m_crtd_by = value;
			if (z_bool.m_crtd_by)
			{
				_zWhereClause += z_sep+"crtd_by='"+crtd_by() + "'";
				z_sep=" AND ";
			}
		}
		public String crtd_dt()
		{
			return m_crtd_dt;
		}
		public void crtd_dt(String value)
		{
			z_bool.m_crtd_dt = true;
			m_crtd_dt = value;
			if (z_bool.m_crtd_dt)
			{
				_zWhereClause += z_sep+"crtd_dt='"+crtd_dt() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_by()
		{
			return m_updt_by;
		}
		public void updt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
			if (z_bool.m_updt_by)
			{
				_zWhereClause += z_sep+"updt_by='"+updt_by() + "'";
				z_sep=" AND ";
			}
		}
		public int item_id()
		{
			return m_item_id;
		}
		public void item_id(int value)
		{
			z_bool.m_item_id = true;
			m_item_id = value;
			if (z_bool.m_item_id)
			{
				_zWhereClause += z_sep+"item_id='"+item_id() + "'";
				z_sep=" AND ";
			}
		}
		public String item_name()
		{
			return m_item_name;
		}
		public void item_name(String value)
		{
			z_bool.m_item_name = true;
			m_item_name = value;
			if (z_bool.m_item_name)
			{
				_zWhereClause += z_sep+"item_name='"+item_name() + "'";
				z_sep=" AND ";
			}
		}
		public String item_rack_no()
		{
			return m_item_rack_no;
		}
		public void item_rack_no(String value)
		{
			z_bool.m_item_rack_no = true;
			m_item_rack_no = value;
			if (z_bool.m_item_rack_no)
			{
				_zWhereClause += z_sep+"item_rack_no='"+item_rack_no() + "'";
				z_sep=" AND ";
			}
		}
		public int unit_weight()
		{
			return m_unit_weight;
		}
		public void unit_weight(int value)
		{
			z_bool.m_unit_weight = true;
			m_unit_weight = value;
			if (z_bool.m_unit_weight)
			{
				_zWhereClause += z_sep+"unit_weight='"+unit_weight() + "'";
				z_sep=" AND ";
			}
		}
		public int item_pur_unit_id()
		{
			return m_item_pur_unit_id;
		}
		public void item_pur_unit_id(int value)
		{
			z_bool.m_item_pur_unit_id = true;
			m_item_pur_unit_id = value;
			if (z_bool.m_item_pur_unit_id)
			{
				_zWhereClause += z_sep+"item_pur_unit_id='"+item_pur_unit_id() + "'";
				z_sep=" AND ";
			}
		}
		public String item_description()
		{
			return m_item_description;
		}
		public void item_description(String value)
		{
			z_bool.m_item_description = true;
			m_item_description = value;
			if (z_bool.m_item_description)
			{
				_zWhereClause += z_sep+"item_description='"+item_description() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_cnt()
		{
			return m_updt_cnt;
		}
		public void updt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
			if (z_bool.m_updt_cnt)
			{
				_zWhereClause += z_sep+"updt_cnt='"+updt_cnt() + "'";
				z_sep=" AND ";
			}
		}
		public String updt_dt()
		{
			return m_updt_dt;
		}
		public void updt_dt(String value)
		{
			z_bool.m_updt_dt = true;
			m_updt_dt = value;
			if (z_bool.m_updt_dt)
			{
				_zWhereClause += z_sep+"updt_dt='"+updt_dt() + "'";
				z_sep=" AND ";
			}
		}
		public int item_min_stock()
		{
			return m_item_min_stock;
		}
		public void item_min_stock(int value)
		{
			z_bool.m_item_min_stock = true;
			m_item_min_stock = value;
			if (z_bool.m_item_min_stock)
			{
				_zWhereClause += z_sep+"item_min_stock='"+item_min_stock() + "'";
				z_sep=" AND ";
			}
		}
		public String item_code()
		{
			return m_item_code;
		}
		public void item_code(String value)
		{
			z_bool.m_item_code = true;
			m_item_code = value;
			if (z_bool.m_item_code)
			{
				_zWhereClause += z_sep+"item_code='"+item_code() + "'";
				z_sep=" AND ";
			}
		}
		public int item_sel_unit_id()
		{
			return m_item_sel_unit_id;
		}
		public void item_sel_unit_id(int value)
		{
			z_bool.m_item_sel_unit_id = true;
			m_item_sel_unit_id = value;
			if (z_bool.m_item_sel_unit_id)
			{
				_zWhereClause += z_sep+"item_sel_unit_id='"+item_sel_unit_id() + "'";
				z_sep=" AND ";
			}
		}
		public int item_selling_price()
		{
			return m_item_selling_price;
		}
		public void item_selling_price(int value)
		{
			z_bool.m_item_selling_price = true;
			m_item_selling_price = value;
			if (z_bool.m_item_selling_price)
			{
				_zWhereClause += z_sep+"item_selling_price='"+item_selling_price() + "'";
				z_sep=" AND ";
			}
		}
		public int cat_id()
		{
			return m_cat_id;
		}
		public void cat_id(int value)
		{
			z_bool.m_cat_id = true;
			m_cat_id = value;
			if (z_bool.m_cat_id)
			{
				_zWhereClause += z_sep+"cat_id='"+cat_id() + "'";
				z_sep=" AND ";
			}
		}
		public int fy_id()
		{
			return m_fy_id;
		}
		public void fy_id(int value)
		{
			z_bool.m_fy_id = true;
			m_fy_id = value;
			if (z_bool.m_fy_id)
			{
				_zWhereClause += z_sep+"fy_id='"+fy_id() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}