	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : ServiceModel.
	/// </summary>
package com.tia.inventory.model;
	public class ServiceModel
	{
		private int m_service_id;
		private int m_service_type;
		private String m_crtd_dt;
		private String m_remarks;
		private String m_service_code;
		private String m_service_name;
		public ServiceModel_Criteria z_WhereClause;

		private IsDirty_ServiceModel z_bool;

		public ServiceModel()
		{
			z_WhereClause = new ServiceModel_Criteria();
			z_bool = new IsDirty_ServiceModel();
		}
		public class IsDirty_ServiceModel
		{
			public boolean  m_service_id;
			public boolean  m_service_type;
			public boolean  m_crtd_dt;
			public boolean  m_remarks;
			public boolean  m_service_code;
			public boolean  m_service_name;
		}
		public int getService_id()
		{
			return m_service_id;
		}
		public void setService_id(int value)
		{
			z_bool.m_service_id = true;
			m_service_id = value;
		}
		public int getService_type()
		{
			return m_service_type;
		}
		public void setService_type(int value)
		{
			z_bool.m_service_type = true;
			m_service_type = value;
		}
		public String getCrtd_dt()
		{
			return m_crtd_dt;
		}
		public void setCrtd_dt(String value)
		{
			z_bool.m_crtd_dt = true;
			m_crtd_dt = value;
		}
		public String getRemarks()
		{
			return m_remarks;
		}
		public void setRemarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
		}
		public String getService_code()
		{
			return m_service_code;
		}
		public void setService_code(String value)
		{
			z_bool.m_service_code = true;
			m_service_code = value;
		}
		public String getService_name()
		{
			return m_service_name;
		}
		public void setService_name(String value)
		{
			z_bool.m_service_name = true;
			m_service_name = value;
		}
		/*
		public ServiceModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public ServiceModel_Criteria Where(ServiceModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_service ( ";
//			if (z_bool.m_service_id)
//			{
//				SQL+= z_sep +"service_id";
//				z_sep=" , ";
//				}
			if (z_bool.m_service_type)
			{
				SQL+= z_sep +"service_type";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks";
				z_sep=" , ";
				}
			if (z_bool.m_service_code)
			{
				SQL+= z_sep +"service_code";
				z_sep=" , ";
				}
			if (z_bool.m_service_name)
			{
				SQL+= z_sep +"service_name";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
//			if (z_bool.m_service_id)
//			{
//				SQL+= z_sep +"'" + m_service_id + "'";
//				z_sep=" , ";
//				}
			if (z_bool.m_service_type)
			{
				SQL+= z_sep +"'" + m_service_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"'" + m_crtd_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"'" + m_remarks + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_code)
			{
				SQL+= z_sep +"'" + m_service_code + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_name)
			{
				SQL+= z_sep +"'" + m_service_name + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_service SET ";
//			if (z_bool.m_service_id)
//			{
//				SQL+= z_sep +"service_id='" + m_service_id + "'";
//				z_sep=" , ";
//				}
			if (z_bool.m_service_type)
			{
				SQL+= z_sep +"service_type='" + m_service_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + m_crtd_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + m_remarks + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_code)
			{
				SQL+= z_sep +"service_code='" + m_service_code + "'";
				z_sep=" , ";
				}
			if (z_bool.m_service_name)
			{
				SQL+= z_sep +"service_name='" + m_service_name + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_service_id)
			{
				SQL+= z_sep +"service_id='" + z_WhereClause.service_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_type)
			{
				SQL+= z_sep +"service_type='" + z_WhereClause.service_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_code)
			{
				SQL+= z_sep +"service_code='" + z_WhereClause.service_code() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_name)
			{
				SQL+= z_sep +"service_name='" + z_WhereClause.service_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_service";
			if (z_WhereClause.z_bool.m_service_id)
			{
				SQL+= z_sep +"service_id='" + z_WhereClause.service_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_type)
			{
				SQL+= z_sep +"service_type='" + z_WhereClause.service_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_code)
			{
				SQL+= z_sep +"service_code='" + z_WhereClause.service_code() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_name)
			{
				SQL+= z_sep +"service_name='" + z_WhereClause.service_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_service";
			if (z_WhereClause.z_bool.m_service_id)
			{
				SQL+= z_sep +"service_id='" + z_WhereClause.service_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_type)
			{
				SQL+= z_sep +"service_type='" + z_WhereClause.service_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_code)
			{
				SQL+= z_sep +"service_code='" + z_WhereClause.service_code() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_service_name)
			{
				SQL+= z_sep +"service_name='" + z_WhereClause.service_name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : ServiceModel_Criteria.
	/// </summary>
	public class ServiceModel_Criteria 
	{
		private int m_service_id;
		private int m_service_type;
		private String m_crtd_dt;
		private String m_remarks;
		private String m_service_code;
		private String m_service_name;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_ServiceModel_Criteria  z_bool;

		public ServiceModel_Criteria()

		{
		z_bool=new IsDirty_ServiceModel_Criteria();  
		}
		public class IsDirty_ServiceModel_Criteria 
		{
			public boolean  m_service_id;
			public boolean  m_service_type;
			public boolean  m_crtd_dt;
			public boolean  m_remarks;
			public boolean  m_service_code;
			public boolean  m_service_name;
			public boolean  MyWhere;

		}
		public int service_id()
		{
			return m_service_id;
		}
		public void service_id(int value)
		{
			z_bool.m_service_id = true;
			m_service_id = value;
			if (z_bool.m_service_id)
			{
				_zWhereClause += z_sep+"service_id='"+service_id() + "'";
				z_sep=" AND ";
			}
		}
		public int service_type()
		{
			return m_service_type;
		}
		public void service_type(int value)
		{
			z_bool.m_service_type = true;
			m_service_type = value;
			if (z_bool.m_service_type)
			{
				_zWhereClause += z_sep+"service_type='"+service_type() + "'";
				z_sep=" AND ";
			}
		}
		public String crtd_dt()
		{
			return m_crtd_dt;
		}
		public void crtd_dt(String value)
		{
			z_bool.m_crtd_dt = true;
			m_crtd_dt = value;
			if (z_bool.m_crtd_dt)
			{
				_zWhereClause += z_sep+"crtd_dt='"+crtd_dt() + "'";
				z_sep=" AND ";
			}
		}
		public String remarks()
		{
			return m_remarks;
		}
		public void remarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
			if (z_bool.m_remarks)
			{
				_zWhereClause += z_sep+"remarks='"+remarks() + "'";
				z_sep=" AND ";
			}
		}
		public String service_code()
		{
			return m_service_code;
		}
		public void service_code(String value)
		{
			z_bool.m_service_code = true;
			m_service_code = value;
			if (z_bool.m_service_code)
			{
				_zWhereClause += z_sep+"service_code='"+service_code() + "'";
				z_sep=" AND ";
			}
		}
		public String service_name()
		{
			return m_service_name;
		}
		public void service_name(String value)
		{
			z_bool.m_service_name = true;
			m_service_name = value;
			if (z_bool.m_service_name)
			{
				_zWhereClause += z_sep+"service_name='"+service_name() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}