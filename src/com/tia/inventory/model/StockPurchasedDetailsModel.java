	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : StockPurchasedDetailsModel.
	/// </summary>
package com.tia.inventory.model;
	public class StockPurchasedDetailsModel
	{
		private String m_crtd_dt;
		private String m_unit_quantity;
		private String m_unit_weight;
		private String m_quantity;
		private int m_updt_by;
		private int m_item_id;
		private String m_actual_rate;
		private int m_pur_det_id;
		private int m_pur_id;
		private int m_crtd_by;
		private String m_updt_dt;
		private String m_selling_price;
		private String m_total;
		private String m_rate;
		private int m_unit_id;
		private int m_updt_cnt;
		public StockPurchasedDetailsModel_Criteria z_WhereClause;

		private IsDirty_StockPurchasedDetailsModel z_bool;

		public StockPurchasedDetailsModel()
		{
			z_WhereClause = new StockPurchasedDetailsModel_Criteria();
			z_bool = new IsDirty_StockPurchasedDetailsModel();
		}
		public class IsDirty_StockPurchasedDetailsModel
		{
			public boolean  m_crtd_dt;
			public boolean  m_unit_quantity;
			public boolean  m_unit_weight;
			public boolean  m_quantity;
			public boolean  m_updt_by;
			public boolean  m_item_id;
			public boolean  m_actual_rate;
			public boolean  m_pur_det_id;
			public boolean  m_pur_id;
			public boolean  m_crtd_by;
			public boolean  m_updt_dt;
			public boolean  m_selling_price;
			public boolean  m_total;
			public boolean  m_rate;
			public boolean  m_unit_id;
			public boolean  m_updt_cnt;
		}
		public String getCrtd_dt()
		{
			return m_crtd_dt;
		}
		public void setCrtd_dt(String value)
		{
			z_bool.m_crtd_dt = true;
			m_crtd_dt = value;
		}
		public String getUnit_quantity()
		{
			return m_unit_quantity;
		}
		public void setUnit_quantity(String value)
		{
			z_bool.m_unit_quantity = true;
			m_unit_quantity = value;
		}
		public String getUnit_weight()
		{
			return m_unit_weight;
		}
		public void setUnit_weight(String value)
		{
			z_bool.m_unit_weight = true;
			m_unit_weight = value;
		}
		public String getQuantity()
		{
			return m_quantity;
		}
		public void setQuantity(String value)
		{
			z_bool.m_quantity = true;
			m_quantity = value;
		}
		public int getUpdt_by()
		{
			return m_updt_by;
		}
		public void setUpdt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
		}
		public int getItem_id()
		{
			return m_item_id;
		}
		public void setItem_id(int value)
		{
			z_bool.m_item_id = true;
			m_item_id = value;
		}
		public String getActual_rate()
		{
			return m_actual_rate;
		}
		public void setActual_rate(String value)
		{
			z_bool.m_actual_rate = true;
			m_actual_rate = value;
		}
		public int getPur_det_id()
		{
			return m_pur_det_id;
		}
		public void setPur_det_id(int value)
		{
			z_bool.m_pur_det_id = true;
			m_pur_det_id = value;
		}
		public int getPur_id()
		{
			return m_pur_id;
		}
		public void setPur_id(int value)
		{
			z_bool.m_pur_id = true;
			m_pur_id = value;
		}
		public int getCrtd_by()
		{
			return m_crtd_by;
		}
		public void setCrtd_by(int value)
		{
			z_bool.m_crtd_by = true;
			m_crtd_by = value;
		}
		public String getUpdt_dt()
		{
			return m_updt_dt;
		}
		public void setUpdt_dt(String value)
		{
			z_bool.m_updt_dt = true;
			m_updt_dt = value;
		}
		public String getSelling_price()
		{
			return m_selling_price;
		}
		public void setSelling_price(String value)
		{
			z_bool.m_selling_price = true;
			m_selling_price = value;
		}
		public String getTotal()
		{
			return m_total;
		}
		public void setTotal(String value)
		{
			z_bool.m_total = true;
			m_total = value;
		}
		public String getRate()
		{
			return m_rate;
		}
		public void setRate(String value)
		{
			z_bool.m_rate = true;
			m_rate = value;
		}
		public int getUnit_id()
		{
			return m_unit_id;
		}
		public void setUnit_id(int value)
		{
			z_bool.m_unit_id = true;
			m_unit_id = value;
		}
		public int getUpdt_cnt()
		{
			return m_updt_cnt;
		}
		public void setUpdt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
		}
		/*
		public StockPurchasedDetailsModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public StockPurchasedDetailsModel_Criteria Where(StockPurchasedDetailsModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_item_purchsed_details ( ";
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt";
				z_sep=" , ";
				}
			if (z_bool.m_unit_quantity)
			{
				SQL+= z_sep +"unit_quantity";
				z_sep=" , ";
				}
			if (z_bool.m_unit_weight)
			{
				SQL+= z_sep +"unit_weight";
				z_sep=" , ";
				}
			if (z_bool.m_quantity)
			{
				SQL+= z_sep +"quantity";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by";
				z_sep=" , ";
				}
			if (z_bool.m_item_id)
			{
				SQL+= z_sep +"item_id";
				z_sep=" , ";
				}
			if (z_bool.m_actual_rate)
			{
				SQL+= z_sep +"actual_rate";
				z_sep=" , ";
				}
			if (z_bool.m_pur_det_id)
			{
				SQL+= z_sep +"pur_det_id";
				z_sep=" , ";
				}
			if (z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt";
				z_sep=" , ";
				}
			if (z_bool.m_selling_price)
			{
				SQL+= z_sep +"selling_price";
				z_sep=" , ";
				}
			if (z_bool.m_total)
			{
				SQL+= z_sep +"total";
				z_sep=" , ";
				}
			if (z_bool.m_rate)
			{
				SQL+= z_sep +"rate";
				z_sep=" , ";
				}
			if (z_bool.m_unit_id)
			{
				SQL+= z_sep +"unit_id";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"'" + m_crtd_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_unit_quantity)
			{
				SQL+= z_sep +"'" + m_unit_quantity + "'";
				z_sep=" , ";
				}
			if (z_bool.m_unit_weight)
			{
				SQL+= z_sep +"'" + m_unit_weight + "'";
				z_sep=" , ";
				}
			if (z_bool.m_quantity)
			{
				SQL+= z_sep +"'" + m_quantity + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"'" + m_updt_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_id)
			{
				SQL+= z_sep +"'" + m_item_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_actual_rate)
			{
				SQL+= z_sep +"'" + m_actual_rate + "'";
				z_sep=" , ";
				}
			if (z_bool.m_pur_det_id)
			{
				SQL+= z_sep +"'" + m_pur_det_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_pur_id)
			{
				SQL+= z_sep +"'" + m_pur_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_by)
			{
				SQL+= z_sep +"'" + m_crtd_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"'" + m_updt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_selling_price)
			{
				SQL+= z_sep +"'" + m_selling_price + "'";
				z_sep=" , ";
				}
			if (z_bool.m_total)
			{
				SQL+= z_sep +"'" + m_total + "'";
				z_sep=" , ";
				}
			if (z_bool.m_rate)
			{
				SQL+= z_sep +"'" + m_rate + "'";
				z_sep=" , ";
				}
			if (z_bool.m_unit_id)
			{
				SQL+= z_sep +"'" + m_unit_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"'" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_item_purchsed_details SET ";
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + m_crtd_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_unit_quantity)
			{
				SQL+= z_sep +"unit_quantity='" + m_unit_quantity + "'";
				z_sep=" , ";
				}
			if (z_bool.m_unit_weight)
			{
				SQL+= z_sep +"unit_weight='" + m_unit_weight + "'";
				z_sep=" , ";
				}
			if (z_bool.m_quantity)
			{
				SQL+= z_sep +"quantity='" + m_quantity + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + m_updt_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_item_id)
			{
				SQL+= z_sep +"item_id='" + m_item_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_actual_rate)
			{
				SQL+= z_sep +"actual_rate='" + m_actual_rate + "'";
				z_sep=" , ";
				}
			if (z_bool.m_pur_det_id)
			{
				SQL+= z_sep +"pur_det_id='" + m_pur_det_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + m_pur_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + m_crtd_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + m_updt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_selling_price)
			{
				SQL+= z_sep +"selling_price='" + m_selling_price + "'";
				z_sep=" , ";
				}
			if (z_bool.m_total)
			{
				SQL+= z_sep +"total='" + m_total + "'";
				z_sep=" , ";
				}
			if (z_bool.m_rate)
			{
				SQL+= z_sep +"rate='" + m_rate + "'";
				z_sep=" , ";
				}
			if (z_bool.m_unit_id)
			{
				SQL+= z_sep +"unit_id='" + m_unit_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit_quantity)
			{
				SQL+= z_sep +"unit_quantity='" + z_WhereClause.unit_quantity() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit_weight)
			{
				SQL+= z_sep +"unit_weight='" + z_WhereClause.unit_weight() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_quantity)
			{
				SQL+= z_sep +"quantity='" + z_WhereClause.quantity() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_id)
			{
				SQL+= z_sep +"item_id='" + z_WhereClause.item_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_actual_rate)
			{
				SQL+= z_sep +"actual_rate='" + z_WhereClause.actual_rate() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_det_id)
			{
				SQL+= z_sep +"pur_det_id='" + z_WhereClause.pur_det_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + z_WhereClause.pur_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + z_WhereClause.crtd_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_selling_price)
			{
				SQL+= z_sep +"selling_price='" + z_WhereClause.selling_price() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_total)
			{
				SQL+= z_sep +"total='" + z_WhereClause.total() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_rate)
			{
				SQL+= z_sep +"rate='" + z_WhereClause.rate() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit_id)
			{
				SQL+= z_sep +"unit_id='" + z_WhereClause.unit_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_item_purchsed_details";
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit_quantity)
			{
				SQL+= z_sep +"unit_quantity='" + z_WhereClause.unit_quantity() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit_weight)
			{
				SQL+= z_sep +"unit_weight='" + z_WhereClause.unit_weight() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_quantity)
			{
				SQL+= z_sep +"quantity='" + z_WhereClause.quantity() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_id)
			{
				SQL+= z_sep +"item_id='" + z_WhereClause.item_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_actual_rate)
			{
				SQL+= z_sep +"actual_rate='" + z_WhereClause.actual_rate() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_det_id)
			{
				SQL+= z_sep +"pur_det_id='" + z_WhereClause.pur_det_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + z_WhereClause.pur_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + z_WhereClause.crtd_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_selling_price)
			{
				SQL+= z_sep +"selling_price='" + z_WhereClause.selling_price() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_total)
			{
				SQL+= z_sep +"total='" + z_WhereClause.total() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_rate)
			{
				SQL+= z_sep +"rate='" + z_WhereClause.rate() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit_id)
			{
				SQL+= z_sep +"unit_id='" + z_WhereClause.unit_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_item_purchsed_details";
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit_quantity)
			{
				SQL+= z_sep +"unit_quantity='" + z_WhereClause.unit_quantity() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit_weight)
			{
				SQL+= z_sep +"unit_weight='" + z_WhereClause.unit_weight() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_quantity)
			{
				SQL+= z_sep +"quantity='" + z_WhereClause.quantity() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_item_id)
			{
				SQL+= z_sep +"item_id='" + z_WhereClause.item_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_actual_rate)
			{
				SQL+= z_sep +"actual_rate='" + z_WhereClause.actual_rate() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_det_id)
			{
				SQL+= z_sep +"pur_det_id='" + z_WhereClause.pur_det_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_pur_id)
			{
				SQL+= z_sep +"pur_id='" + z_WhereClause.pur_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + z_WhereClause.crtd_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_selling_price)
			{
				SQL+= z_sep +"selling_price='" + z_WhereClause.selling_price() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_total)
			{
				SQL+= z_sep +"total='" + z_WhereClause.total() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_rate)
			{
				SQL+= z_sep +"rate='" + z_WhereClause.rate() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_unit_id)
			{
				SQL+= z_sep +"unit_id='" + z_WhereClause.unit_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : StockPurchasedDetailsModel_Criteria.
	/// </summary>
	public class StockPurchasedDetailsModel_Criteria 
	{
		private String m_crtd_dt;
		private int m_unit_quantity;
		private int m_unit_weight;
		private int m_quantity;
		private int m_updt_by;
		private int m_item_id;
		private int m_actual_rate;
		private int m_pur_det_id;
		private int m_pur_id;
		private int m_crtd_by;
		private String m_updt_dt;
		private int m_selling_price;
		private int m_total;
		private int m_rate;
		private int m_unit_id;
		private int m_updt_cnt;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_StockPurchasedDetailsModel_Criteria  z_bool;

		public StockPurchasedDetailsModel_Criteria()

		{
		z_bool=new IsDirty_StockPurchasedDetailsModel_Criteria();  
		}
		public class IsDirty_StockPurchasedDetailsModel_Criteria 
		{
			public boolean  m_crtd_dt;
			public boolean  m_unit_quantity;
			public boolean  m_unit_weight;
			public boolean  m_quantity;
			public boolean  m_updt_by;
			public boolean  m_item_id;
			public boolean  m_actual_rate;
			public boolean  m_pur_det_id;
			public boolean  m_pur_id;
			public boolean  m_crtd_by;
			public boolean  m_updt_dt;
			public boolean  m_selling_price;
			public boolean  m_total;
			public boolean  m_rate;
			public boolean  m_unit_id;
			public boolean  m_updt_cnt;
			public boolean  MyWhere;

		}
		public String crtd_dt()
		{
			return m_crtd_dt;
		}
		public void crtd_dt(String value)
		{
			z_bool.m_crtd_dt = true;
			m_crtd_dt = value;
			if (z_bool.m_crtd_dt)
			{
				_zWhereClause += z_sep+"crtd_dt='"+crtd_dt() + "'";
				z_sep=" AND ";
			}
		}
		public int unit_quantity()
		{
			return m_unit_quantity;
		}
		public void unit_quantity(int value)
		{
			z_bool.m_unit_quantity = true;
			m_unit_quantity = value;
			if (z_bool.m_unit_quantity)
			{
				_zWhereClause += z_sep+"unit_quantity='"+unit_quantity() + "'";
				z_sep=" AND ";
			}
		}
		public int unit_weight()
		{
			return m_unit_weight;
		}
		public void unit_weight(int value)
		{
			z_bool.m_unit_weight = true;
			m_unit_weight = value;
			if (z_bool.m_unit_weight)
			{
				_zWhereClause += z_sep+"unit_weight='"+unit_weight() + "'";
				z_sep=" AND ";
			}
		}
		public int quantity()
		{
			return m_quantity;
		}
		public void quantity(int value)
		{
			z_bool.m_quantity = true;
			m_quantity = value;
			if (z_bool.m_quantity)
			{
				_zWhereClause += z_sep+"quantity='"+quantity() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_by()
		{
			return m_updt_by;
		}
		public void updt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
			if (z_bool.m_updt_by)
			{
				_zWhereClause += z_sep+"updt_by='"+updt_by() + "'";
				z_sep=" AND ";
			}
		}
		public int item_id()
		{
			return m_item_id;
		}
		public void item_id(int value)
		{
			z_bool.m_item_id = true;
			m_item_id = value;
			if (z_bool.m_item_id)
			{
				_zWhereClause += z_sep+"item_id='"+item_id() + "'";
				z_sep=" AND ";
			}
		}
		public int actual_rate()
		{
			return m_actual_rate;
		}
		public void actual_rate(int value)
		{
			z_bool.m_actual_rate = true;
			m_actual_rate = value;
			if (z_bool.m_actual_rate)
			{
				_zWhereClause += z_sep+"actual_rate='"+actual_rate() + "'";
				z_sep=" AND ";
			}
		}
		public int pur_det_id()
		{
			return m_pur_det_id;
		}
		public void pur_det_id(int value)
		{
			z_bool.m_pur_det_id = true;
			m_pur_det_id = value;
			if (z_bool.m_pur_det_id)
			{
				_zWhereClause += z_sep+"pur_det_id='"+pur_det_id() + "'";
				z_sep=" AND ";
			}
		}
		public int pur_id()
		{
			return m_pur_id;
		}
		public void pur_id(int value)
		{
			z_bool.m_pur_id = true;
			m_pur_id = value;
			if (z_bool.m_pur_id)
			{
				_zWhereClause += z_sep+"pur_id='"+pur_id() + "'";
				z_sep=" AND ";
			}
		}
		public int crtd_by()
		{
			return m_crtd_by;
		}
		public void crtd_by(int value)
		{
			z_bool.m_crtd_by = true;
			m_crtd_by = value;
			if (z_bool.m_crtd_by)
			{
				_zWhereClause += z_sep+"crtd_by='"+crtd_by() + "'";
				z_sep=" AND ";
			}
		}
		public String updt_dt()
		{
			return m_updt_dt;
		}
		public void updt_dt(String value)
		{
			z_bool.m_updt_dt = true;
			m_updt_dt = value;
			if (z_bool.m_updt_dt)
			{
				_zWhereClause += z_sep+"updt_dt='"+updt_dt() + "'";
				z_sep=" AND ";
			}
		}
		public int selling_price()
		{
			return m_selling_price;
		}
		public void selling_price(int value)
		{
			z_bool.m_selling_price = true;
			m_selling_price = value;
			if (z_bool.m_selling_price)
			{
				_zWhereClause += z_sep+"selling_price='"+selling_price() + "'";
				z_sep=" AND ";
			}
		}
		public int total()
		{
			return m_total;
		}
		public void total(int value)
		{
			z_bool.m_total = true;
			m_total = value;
			if (z_bool.m_total)
			{
				_zWhereClause += z_sep+"total='"+total() + "'";
				z_sep=" AND ";
			}
		}
		public int rate()
		{
			return m_rate;
		}
		public void rate(int value)
		{
			z_bool.m_rate = true;
			m_rate = value;
			if (z_bool.m_rate)
			{
				_zWhereClause += z_sep+"rate='"+rate() + "'";
				z_sep=" AND ";
			}
		}
		public int unit_id()
		{
			return m_unit_id;
		}
		public void unit_id(int value)
		{
			z_bool.m_unit_id = true;
			m_unit_id = value;
			if (z_bool.m_unit_id)
			{
				_zWhereClause += z_sep+"unit_id='"+unit_id() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_cnt()
		{
			return m_updt_cnt;
		}
		public void updt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
			if (z_bool.m_updt_cnt)
			{
				_zWhereClause += z_sep+"updt_cnt='"+updt_cnt() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}