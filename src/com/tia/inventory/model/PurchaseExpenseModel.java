	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : PurchaseExpenseModel.
	/// </summary>
package com.tia.inventory.model;
	public class PurchaseExpenseModel
	{
		private String m_expense_dr;
		private String m_expense_cr;
		private String m_name;
		private int m_crtd_by;
		private int m_exp_id;
		private String m_remarks;
		private String m_crtd_dt;
		public PurchaseExpenseModel_Criteria z_WhereClause;

		private IsDirty_PurchaseExpenseModel z_bool;

		public PurchaseExpenseModel()
		{
			z_WhereClause = new PurchaseExpenseModel_Criteria();
			z_bool = new IsDirty_PurchaseExpenseModel();
		}
		public class IsDirty_PurchaseExpenseModel
		{
			public boolean  m_expense_dr;
			public boolean  m_expense_cr;
			public boolean  m_name;
			public boolean  m_crtd_by;
			public boolean  m_exp_id;
			public boolean  m_remarks;
			public boolean  m_crtd_dt;
		}
		public String getExpense_dr()
		{
			return m_expense_dr;
		}
		public void setExpense_dr(String value)
		{
			z_bool.m_expense_dr = true;
			m_expense_dr = value;
		}
		public String getExpense_cr()
		{
			return m_expense_cr;
		}
		public void setExpense_cr(String value)
		{
			z_bool.m_expense_cr = true;
			m_expense_cr = value;
		}
		public String getName()
		{
			return m_name;
		}
		public void setName(String value)
		{
			z_bool.m_name = true;
			m_name = value;
		}
		public int getCrtd_by()
		{
			return m_crtd_by;
		}
		public void setCrtd_by(int value)
		{
			z_bool.m_crtd_by = true;
			m_crtd_by = value;
		}
		public int getExp_id()
		{
			return m_exp_id;
		}
		public void setExp_id(int value)
		{
			z_bool.m_exp_id = true;
			m_exp_id = value;
		}
		public String getRemarks()
		{
			return m_remarks;
		}
		public void setRemarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
		}
		public String getCrtd_dt()
		{
			return m_crtd_dt;
		}
		public void setCrtd_dt(String value)
		{
			z_bool.m_crtd_dt = true;
			m_crtd_dt = value;
		}
		/*
		public PurchaseExpenseModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public PurchaseExpenseModel_Criteria Where(PurchaseExpenseModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO inven_pur_expense ( ";
			if (z_bool.m_expense_dr)
			{
				SQL+= z_sep +"expense_dr";
				z_sep=" , ";
				}
			if (z_bool.m_expense_cr)
			{
				SQL+= z_sep +"expense_cr";
				z_sep=" , ";
				}
			if (z_bool.m_name)
			{
				SQL+= z_sep +"name";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by";
				z_sep=" , ";
				}
//			if (z_bool.m_exp_id)
//			{
//				SQL+= z_sep +"exp_id";
//				z_sep=" , ";
//				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_expense_dr)
			{
				SQL+= z_sep +"'" + m_expense_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_expense_cr)
			{
				SQL+= z_sep +"'" + m_expense_cr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_name)
			{
				SQL+= z_sep +"'" + m_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_by)
			{
				SQL+= z_sep +"'" + m_crtd_by + "'";
				z_sep=" , ";
				}
//			if (z_bool.m_exp_id)
//			{
//				SQL+= z_sep +"'" + m_exp_id + "'";
//				z_sep=" , ";
//				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"'" + m_remarks + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"'" + m_crtd_dt + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE inven_pur_expense SET ";
			if (z_bool.m_expense_dr)
			{
				SQL+= z_sep +"expense_dr='" + m_expense_dr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_expense_cr)
			{
				SQL+= z_sep +"expense_cr='" + m_expense_cr + "'";
				z_sep=" , ";
				}
			if (z_bool.m_name)
			{
				SQL+= z_sep +"name='" + m_name + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + m_crtd_by + "'";
				z_sep=" , ";
				}
//			if (z_bool.m_exp_id)
//			{
//				SQL+= z_sep +"exp_id='" + m_exp_id + "'";
//				z_sep=" , ";
//				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + m_remarks + "'";
				z_sep=" , ";
				}
			if (z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + m_crtd_dt + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_expense_dr)
			{
				SQL+= z_sep +"expense_dr='" + z_WhereClause.expense_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_expense_cr)
			{
				SQL+= z_sep +"expense_cr='" + z_WhereClause.expense_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_name)
			{
				SQL+= z_sep +"name='" + z_WhereClause.name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + z_WhereClause.crtd_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_exp_id)
			{
				SQL+= z_sep +"exp_id='" + z_WhereClause.exp_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM inven_pur_expense";
			if (z_WhereClause.z_bool.m_expense_dr)
			{
				SQL+= z_sep +"expense_dr='" + z_WhereClause.expense_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_expense_cr)
			{
				SQL+= z_sep +"expense_cr='" + z_WhereClause.expense_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_name)
			{
				SQL+= z_sep +"name='" + z_WhereClause.name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + z_WhereClause.crtd_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_exp_id)
			{
				SQL+= z_sep +"exp_id='" + z_WhereClause.exp_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM inven_pur_expense";
			if (z_WhereClause.z_bool.m_expense_dr)
			{
				SQL+= z_sep +"expense_dr='" + z_WhereClause.expense_dr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_expense_cr)
			{
				SQL+= z_sep +"expense_cr='" + z_WhereClause.expense_cr() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_name)
			{
				SQL+= z_sep +"name='" + z_WhereClause.name() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_by)
			{
				SQL+= z_sep +"crtd_by='" + z_WhereClause.crtd_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_exp_id)
			{
				SQL+= z_sep +"exp_id='" + z_WhereClause.exp_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_crtd_dt)
			{
				SQL+= z_sep +"crtd_dt='" + z_WhereClause.crtd_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : PurchaseExpenseModel_Criteria.
	/// </summary>
	public class PurchaseExpenseModel_Criteria 
	{
		private String m_expense_dr;
		private String m_expense_cr;
		private String m_name;
		private int m_crtd_by;
		private int m_exp_id;
		private String m_remarks;
		private String m_crtd_dt;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_PurchaseExpenseModel_Criteria  z_bool;

		public PurchaseExpenseModel_Criteria()

		{
		z_bool=new IsDirty_PurchaseExpenseModel_Criteria();  
		}
		public class IsDirty_PurchaseExpenseModel_Criteria 
		{
			public boolean  m_expense_dr;
			public boolean  m_expense_cr;
			public boolean  m_name;
			public boolean  m_crtd_by;
			public boolean  m_exp_id;
			public boolean  m_remarks;
			public boolean  m_crtd_dt;
			public boolean  MyWhere;

		}
		public String expense_dr()
		{
			return m_expense_dr;
		}
		public void expense_dr(String value)
		{
			z_bool.m_expense_dr = true;
			m_expense_dr = value;
			if (z_bool.m_expense_dr)
			{
				_zWhereClause += z_sep+"expense_dr='"+expense_dr() + "'";
				z_sep=" AND ";
			}
		}
		public String expense_cr()
		{
			return m_expense_cr;
		}
		public void expense_cr(String value)
		{
			z_bool.m_expense_cr = true;
			m_expense_cr = value;
			if (z_bool.m_expense_cr)
			{
				_zWhereClause += z_sep+"expense_cr='"+expense_cr() + "'";
				z_sep=" AND ";
			}
		}
		public String name()
		{
			return m_name;
		}
		public void name(String value)
		{
			z_bool.m_name = true;
			m_name = value;
			if (z_bool.m_name)
			{
				_zWhereClause += z_sep+"name='"+name() + "'";
				z_sep=" AND ";
			}
		}
		public int crtd_by()
		{
			return m_crtd_by;
		}
		public void crtd_by(int value)
		{
			z_bool.m_crtd_by = true;
			m_crtd_by = value;
			if (z_bool.m_crtd_by)
			{
				_zWhereClause += z_sep+"crtd_by='"+crtd_by() + "'";
				z_sep=" AND ";
			}
		}
		public int exp_id()
		{
			return m_exp_id;
		}
		public void exp_id(int value)
		{
			z_bool.m_exp_id = true;
			m_exp_id = value;
			if (z_bool.m_exp_id)
			{
				_zWhereClause += z_sep+"exp_id='"+exp_id() + "'";
				z_sep=" AND ";
			}
		}
		public String remarks()
		{
			return m_remarks;
		}
		public void remarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
			if (z_bool.m_remarks)
			{
				_zWhereClause += z_sep+"remarks='"+remarks() + "'";
				z_sep=" AND ";
			}
		}
		public String crtd_dt()
		{
			return m_crtd_dt;
		}
		public void crtd_dt(String value)
		{
			z_bool.m_crtd_dt = true;
			m_crtd_dt = value;
			if (z_bool.m_crtd_dt)
			{
				_zWhereClause += z_sep+"crtd_dt='"+crtd_dt() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}