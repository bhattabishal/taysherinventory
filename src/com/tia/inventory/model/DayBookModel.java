	/// <summary>
	/// <Magnus Consulting Group Pvt. Ltd.>
	/// <SFACL Java Version>
	/// Class For Table : DayBookModel.
	/// </summary>
package com.tia.inventory.model;

import java.text.DecimalFormat;
	public class DayBookModel
	{
		private String m_expense;
		private int m_trxn_type;
		private int m_trxn_other_id;
		private int m_fy_id;
		private int m_updt_by;
		private String m_trxn_date_ad;
		private int m_day_book_id;
		private int m_dept_id;
		private String m_trxn_date_bs;
		private int m_updt_cnt;
		private String m_updt_dt;
		private String m_remarks;
		private int m_receipt_no;
		private String m_created_dt;
		private int m_created_by;
		private String m_income;
		public DayBookModel_Criteria z_WhereClause;

		public IsDirty_DayBookModel z_bool;

		public DayBookModel()
		{
			z_WhereClause = new DayBookModel_Criteria();
			z_bool = new IsDirty_DayBookModel();
		}
		public class IsDirty_DayBookModel
		{
			public boolean  m_expense;
			public boolean  m_trxn_type;
			public boolean m_trxn_other_id; 
			public boolean  m_fy_id;
			public boolean  m_updt_by;
			public boolean  m_trxn_date_ad;
			public boolean  m_day_book_id;
			public boolean  m_dept_id;
			public boolean  m_trxn_date_bs;
			public boolean  m_updt_cnt;
			public boolean  m_updt_dt;
			public boolean  m_remarks;
			public boolean  m_receipt_no;
			public boolean  m_created_dt;
			public boolean  m_created_by;
			public boolean  m_income;
		}
		public String getExpense()
		{
			return m_expense;
		}
		public void setExpense(double value)
		{
			z_bool.m_expense = true;
			DecimalFormat df = new DecimalFormat("#.##");
			m_expense = df.format(value);
		}
		
		public int getTrxn_type()
		{
			return m_trxn_type;
		}
		public void setTrxn_other_id(int value)
		{
			z_bool.m_trxn_other_id = true;
			m_trxn_other_id = value;
		}
		public int getTrxn_other_id()
		{
			return m_trxn_other_id;
		}
		public void setTrxn_type(int value)
		{
			z_bool.m_trxn_type = true;
			m_trxn_type = value;
		}
		public int getFy_id()
		{
			return m_fy_id;
		}
		public void setFy_id(int value)
		{
			z_bool.m_fy_id = true;
			m_fy_id = value;
		}
		public int getUpdt_by()
		{
			return m_updt_by;
		}
		public void setUpdt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
		}
		public String getTrxn_date_ad()
		{
			return m_trxn_date_ad;
		}
		public void setTrxn_date_ad(String value)
		{
			z_bool.m_trxn_date_ad = true;
			m_trxn_date_ad = value;
		}
		public int getDay_book_id()
		{
			return m_day_book_id;
		}
		public void setDay_book_id(int value)
		{
			z_bool.m_day_book_id = true;
			m_day_book_id = value;
		}
		public int getDept_id()
		{
			return m_dept_id;
		}
		public void setDept_id(int value)
		{
			z_bool.m_dept_id = true;
			m_dept_id = value;
		}
		public String getTrxn_date_bs()
		{
			return m_trxn_date_bs;
		}
		public void setTrxn_date_bs(String value)
		{
			z_bool.m_trxn_date_bs = true;
			m_trxn_date_bs = value;
		}
		public int getUpdt_cnt()
		{
			return m_updt_cnt;
		}
		public void setUpdt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
		}
		public String getUpdt_dt()
		{
			return m_updt_dt;
		}
		public void setUpdt_dt(String value)
		{
			z_bool.m_updt_dt = true;
			m_updt_dt = value;
		}
		public String getRemarks()
		{
			return m_remarks;
		}
		public void setRemarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
		}
		public int getReceipt_no()
		{
			return m_receipt_no;
		}
		public void setReceipt_no(int value)
		{
			z_bool.m_receipt_no = true;
			m_receipt_no = value;
		}
		public String getCreated_dt()
		{
			return m_created_dt;
		}
		public void setCreated_dt(String value)
		{
			z_bool.m_created_dt = true;
			m_created_dt = value;
		}
		public int getCreated_by()
		{
			return m_created_by;
		}
		public void setCreated_by(int value)
		{
			z_bool.m_created_by = true;
			m_created_by = value;
		}
		public String getIncome()
		{
			return m_income;
		}
		public void setIncome(double value)
		{
			z_bool.m_income = true;
			DecimalFormat df = new DecimalFormat("#.##");
			m_income = df.format(value);
		}
		/*
		public DayBookModel_Criteria Where()
		{
				return z_WhereClause;
			}
		public DayBookModel_Criteria Where(DayBookModel_Criteria value)
		{
				z_WhereClause = value;
			}
		}

		*/
		public String Insert()
		{
			//this.setUpdate_count(1);
			String z_sep ="";
			String SQL = "INSERT INTO day_book ( ";
			if (z_bool.m_expense)
			{
				SQL+= z_sep +"expense";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_type)
			{
				SQL+= z_sep +"trxn_type";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_other_id)
			{
				SQL+= z_sep +"trxn_other_id";
				z_sep=" , ";
				}
			if (z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_date_ad)
			{
				SQL+= z_sep +"trxn_date_ad";
				z_sep=" , ";
				}
			if (z_bool.m_day_book_id)
			{
				SQL+= z_sep +"day_book_id";
				z_sep=" , ";
				}
			if (z_bool.m_dept_id)
			{
				SQL+= z_sep +"dept_id";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_date_bs)
			{
				SQL+= z_sep +"trxn_date_bs";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks";
				z_sep=" , ";
				}
			if (z_bool.m_receipt_no)
			{
				SQL+= z_sep +"receipt_no";
				z_sep=" , ";
				}
			if (z_bool.m_created_dt)
			{
				SQL+= z_sep +"created_dt";
				z_sep=" , ";
				}
			if (z_bool.m_created_by)
			{
				SQL+= z_sep +"created_by";
				z_sep=" , ";
				}
			if (z_bool.m_income)
			{
				SQL+= z_sep +"income";
				z_sep=" , ";
				}
			SQL+= ") VALUES (";
			z_sep="";
			if (z_bool.m_expense)
			{
				SQL+= z_sep +"'" + m_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_type)
			{
				SQL+= z_sep +"'" + m_trxn_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_other_id)
			{
				SQL+= z_sep +"'" + m_trxn_other_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_fy_id)
			{
				SQL+= z_sep +"'" + m_fy_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"'" + m_updt_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_date_ad)
			{
				SQL+= z_sep +"'" + m_trxn_date_ad + "'";
				z_sep=" , ";
				}
			if (z_bool.m_day_book_id)
			{
				SQL+= z_sep +"'" + m_day_book_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_dept_id)
			{
				SQL+= z_sep +"'" + m_dept_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_date_bs)
			{
				SQL+= z_sep +"'" + m_trxn_date_bs + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"'" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"'" + m_updt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"'" + m_remarks + "'";
				z_sep=" , ";
				}
			if (z_bool.m_receipt_no)
			{
				SQL+= z_sep +"'" + m_receipt_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_created_dt)
			{
				SQL+= z_sep +"'" + m_created_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_created_by)
			{
				SQL+= z_sep +"'" + m_created_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_income)
			{
				SQL+= z_sep +"'" + m_income + "'";
				z_sep=" , ";
				}
			SQL+= ")";
			return SQL;
		}
		public String Update()
		{
			String z_sep ="";
			String SQL = "UPDATE day_book SET ";
			if (z_bool.m_expense)
			{
				SQL+= z_sep +"expense='" + m_expense + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_type)
			{
				SQL+= z_sep +"trxn_type='" + m_trxn_type + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_other_id)
			{
				SQL+= z_sep +"trxn_other_id='" + m_trxn_other_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + m_fy_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + m_updt_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_date_ad)
			{
				SQL+= z_sep +"trxn_date_ad='" + m_trxn_date_ad + "'";
				z_sep=" , ";
				}
			if (z_bool.m_day_book_id)
			{
				SQL+= z_sep +"day_book_id='" + m_day_book_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_dept_id)
			{
				SQL+= z_sep +"dept_id='" + m_dept_id + "'";
				z_sep=" , ";
				}
			if (z_bool.m_trxn_date_bs)
			{
				SQL+= z_sep +"trxn_date_bs='" + m_trxn_date_bs + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + m_updt_cnt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + m_updt_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + m_remarks + "'";
				z_sep=" , ";
				}
			if (z_bool.m_receipt_no)
			{
				SQL+= z_sep +"receipt_no='" + m_receipt_no + "'";
				z_sep=" , ";
				}
			if (z_bool.m_created_dt)
			{
				SQL+= z_sep +"created_dt='" + m_created_dt + "'";
				z_sep=" , ";
				}
			if (z_bool.m_created_by)
			{
				SQL+= z_sep +"created_by='" + m_created_by + "'";
				z_sep=" , ";
				}
			if (z_bool.m_income)
			{
				SQL+= z_sep +"income='" + m_income + "'";
				z_sep=" , ";
				}
			z_sep=" WHERE ";
			if (z_WhereClause.z_bool.m_expense)
			{
				SQL+= z_sep +"expense='" + z_WhereClause.expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_type)
			{
				SQL+= z_sep +"trxn_type='" + z_WhereClause.trxn_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + z_WhereClause.fy_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_date_ad)
			{
				SQL+= z_sep +"trxn_date_ad='" + z_WhereClause.trxn_date_ad() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_day_book_id)
			{
				SQL+= z_sep +"day_book_id='" + z_WhereClause.day_book_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_dept_id)
			{
				SQL+= z_sep +"dept_id='" + z_WhereClause.dept_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_date_bs)
			{
				SQL+= z_sep +"trxn_date_bs='" + z_WhereClause.trxn_date_bs() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_receipt_no)
			{
				SQL+= z_sep +"receipt_no='" + z_WhereClause.receipt_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_dt)
			{
				SQL+= z_sep +"created_dt='" + z_WhereClause.created_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_by)
			{
				SQL+= z_sep +"created_by='" + z_WhereClause.created_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_income)
			{
				SQL+= z_sep +"income='" + z_WhereClause.income() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String Delete()
		{
			String z_sep=" WHERE ";
			String SQL = "DELETE FROM day_book";
			if (z_WhereClause.z_bool.m_expense)
			{
				SQL+= z_sep +"expense='" + z_WhereClause.expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_type)
			{
				SQL+= z_sep +"trxn_type='" + z_WhereClause.trxn_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + z_WhereClause.fy_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_date_ad)
			{
				SQL+= z_sep +"trxn_date_ad='" + z_WhereClause.trxn_date_ad() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_day_book_id)
			{
				SQL+= z_sep +"day_book_id='" + z_WhereClause.day_book_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_dept_id)
			{
				SQL+= z_sep +"dept_id='" + z_WhereClause.dept_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_date_bs)
			{
				SQL+= z_sep +"trxn_date_bs='" + z_WhereClause.trxn_date_bs() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_receipt_no)
			{
				SQL+= z_sep +"receipt_no='" + z_WhereClause.receipt_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_dt)
			{
				SQL+= z_sep +"created_dt='" + z_WhereClause.created_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_by)
			{
				SQL+= z_sep +"created_by='" + z_WhereClause.created_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_income)
			{
				SQL+= z_sep +"income='" + z_WhereClause.income() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
		public String SearchSQL()
		{
			String z_sep=" WHERE ";
			String SQL = "SELECT * FROM day_book";
			if (z_WhereClause.z_bool.m_expense)
			{
				SQL+= z_sep +"expense='" + z_WhereClause.expense() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_type)
			{
				SQL+= z_sep +"trxn_type='" + z_WhereClause.trxn_type() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_fy_id)
			{
				SQL+= z_sep +"fy_id='" + z_WhereClause.fy_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_by)
			{
				SQL+= z_sep +"updt_by='" + z_WhereClause.updt_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_date_ad)
			{
				SQL+= z_sep +"trxn_date_ad='" + z_WhereClause.trxn_date_ad() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_day_book_id)
			{
				SQL+= z_sep +"day_book_id='" + z_WhereClause.day_book_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_dept_id)
			{
				SQL+= z_sep +"dept_id='" + z_WhereClause.dept_id() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_trxn_date_bs)
			{
				SQL+= z_sep +"trxn_date_bs='" + z_WhereClause.trxn_date_bs() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_cnt)
			{
				SQL+= z_sep +"updt_cnt='" + z_WhereClause.updt_cnt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_updt_dt)
			{
				SQL+= z_sep +"updt_dt='" + z_WhereClause.updt_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_remarks)
			{
				SQL+= z_sep +"remarks='" + z_WhereClause.remarks() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_receipt_no)
			{
				SQL+= z_sep +"receipt_no='" + z_WhereClause.receipt_no() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_dt)
			{
				SQL+= z_sep +"created_dt='" + z_WhereClause.created_dt() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_created_by)
			{
				SQL+= z_sep +"created_by='" + z_WhereClause.created_by() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.m_income)
			{
				SQL+= z_sep +"income='" + z_WhereClause.income() + "'";
				z_sep=" AND ";
				}
			if (z_WhereClause.z_bool.MyWhere)
			{
				SQL+= z_sep + z_WhereClause.MyWhere();
				z_sep=" AND ";
				}
			return SQL;
		}
	/// <summary>
	/// Generated Class for Table : DayBookModel_Criteria.
	/// </summary>
	public class DayBookModel_Criteria 
	{
		private int m_expense;
		private int m_trxn_type;
		private int m_fy_id;
		private int m_updt_by;
		private String m_trxn_date_ad;
		private int m_day_book_id;
		private int m_dept_id;
		private String m_trxn_date_bs;
		private int m_updt_cnt;
		private String m_updt_dt;
		private String m_remarks;
		private int m_receipt_no;
		private String m_created_dt;
		private int m_created_by;
		private int m_income;
		private String z_MyWhere;
		private String _zWhereClause;
		private String z_sep=" ";

		public IsDirty_DayBookModel_Criteria  z_bool;

		public DayBookModel_Criteria()

		{
		z_bool=new IsDirty_DayBookModel_Criteria();  
		}
		public class IsDirty_DayBookModel_Criteria 
		{
			public boolean  m_expense;
			public boolean  m_trxn_type;
			public boolean  m_trxn_other_id;
			public boolean  m_fy_id;
			public boolean  m_updt_by;
			public boolean  m_trxn_date_ad;
			public boolean  m_day_book_id;
			public boolean  m_dept_id;
			public boolean  m_trxn_date_bs;
			public boolean  m_updt_cnt;
			public boolean  m_updt_dt;
			public boolean  m_remarks;
			public boolean  m_receipt_no;
			public boolean  m_created_dt;
			public boolean  m_created_by;
			public boolean  m_income;
			public boolean  MyWhere;

		}
		public int expense()
		{
			return m_expense;
		}
		public void expense(int value)
		{
			z_bool.m_expense = true;
			m_expense = value;
			if (z_bool.m_expense)
			{
				_zWhereClause += z_sep+"expense='"+expense() + "'";
				z_sep=" AND ";
			}
		}
		public int trxn_type()
		{
			return m_trxn_type;
		}
		public void trxn_type(int value)
		{
			z_bool.m_trxn_type = true;
			m_trxn_type = value;
			if (z_bool.m_trxn_type)
			{
				_zWhereClause += z_sep+"trxn_type='"+trxn_type() + "'";
				z_sep=" AND ";
			}
		}
		
		public int trxn_other_id()
		{
			return m_trxn_other_id;
		}
		public void trxn_other_id(int value)
		{
			z_bool.m_trxn_other_id = true;
			m_trxn_other_id = value;
			if (z_bool.m_trxn_other_id)
			{
				_zWhereClause += z_sep+"trxn_other_id='"+trxn_other_id() + "'";
				z_sep=" AND ";
			}
		}
		
		public int fy_id()
		{
			return m_fy_id;
		}
		public void fy_id(int value)
		{
			z_bool.m_fy_id = true;
			m_fy_id = value;
			if (z_bool.m_fy_id)
			{
				_zWhereClause += z_sep+"fy_id='"+fy_id() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_by()
		{
			return m_updt_by;
		}
		public void updt_by(int value)
		{
			z_bool.m_updt_by = true;
			m_updt_by = value;
			if (z_bool.m_updt_by)
			{
				_zWhereClause += z_sep+"updt_by='"+updt_by() + "'";
				z_sep=" AND ";
			}
		}
		public String trxn_date_ad()
		{
			return m_trxn_date_ad;
		}
		public void trxn_date_ad(String value)
		{
			z_bool.m_trxn_date_ad = true;
			m_trxn_date_ad = value;
			if (z_bool.m_trxn_date_ad)
			{
				_zWhereClause += z_sep+"trxn_date_ad='"+trxn_date_ad() + "'";
				z_sep=" AND ";
			}
		}
		public int day_book_id()
		{
			return m_day_book_id;
		}
		public void day_book_id(int value)
		{
			z_bool.m_day_book_id = true;
			m_day_book_id = value;
			if (z_bool.m_day_book_id)
			{
				_zWhereClause += z_sep+"day_book_id='"+day_book_id() + "'";
				z_sep=" AND ";
			}
		}
		public int dept_id()
		{
			return m_dept_id;
		}
		public void dept_id(int value)
		{
			z_bool.m_dept_id = true;
			m_dept_id = value;
			if (z_bool.m_dept_id)
			{
				_zWhereClause += z_sep+"dept_id='"+dept_id() + "'";
				z_sep=" AND ";
			}
		}
		public String trxn_date_bs()
		{
			return m_trxn_date_bs;
		}
		public void trxn_date_bs(String value)
		{
			z_bool.m_trxn_date_bs = true;
			m_trxn_date_bs = value;
			if (z_bool.m_trxn_date_bs)
			{
				_zWhereClause += z_sep+"trxn_date_bs='"+trxn_date_bs() + "'";
				z_sep=" AND ";
			}
		}
		public int updt_cnt()
		{
			return m_updt_cnt;
		}
		public void updt_cnt(int value)
		{
			z_bool.m_updt_cnt = true;
			m_updt_cnt = value;
			if (z_bool.m_updt_cnt)
			{
				_zWhereClause += z_sep+"updt_cnt='"+updt_cnt() + "'";
				z_sep=" AND ";
			}
		}
		public String updt_dt()
		{
			return m_updt_dt;
		}
		public void updt_dt(String value)
		{
			z_bool.m_updt_dt = true;
			m_updt_dt = value;
			if (z_bool.m_updt_dt)
			{
				_zWhereClause += z_sep+"updt_dt='"+updt_dt() + "'";
				z_sep=" AND ";
			}
		}
		public String remarks()
		{
			return m_remarks;
		}
		public void remarks(String value)
		{
			z_bool.m_remarks = true;
			m_remarks = value;
			if (z_bool.m_remarks)
			{
				_zWhereClause += z_sep+"remarks='"+remarks() + "'";
				z_sep=" AND ";
			}
		}
		public int receipt_no()
		{
			return m_receipt_no;
		}
		public void receipt_no(int value)
		{
			z_bool.m_receipt_no = true;
			m_receipt_no = value;
			if (z_bool.m_receipt_no)
			{
				_zWhereClause += z_sep+"receipt_no='"+receipt_no() + "'";
				z_sep=" AND ";
			}
		}
		public String created_dt()
		{
			return m_created_dt;
		}
		public void created_dt(String value)
		{
			z_bool.m_created_dt = true;
			m_created_dt = value;
			if (z_bool.m_created_dt)
			{
				_zWhereClause += z_sep+"created_dt='"+created_dt() + "'";
				z_sep=" AND ";
			}
		}
		public int created_by()
		{
			return m_created_by;
		}
		public void created_by(int value)
		{
			z_bool.m_created_by = true;
			m_created_by = value;
			if (z_bool.m_created_by)
			{
				_zWhereClause += z_sep+"created_by='"+created_by() + "'";
				z_sep=" AND ";
			}
		}
		public int income()
		{
			return m_income;
		}
		public void income(int value)
		{
			z_bool.m_income = true;
			m_income = value;
			if (z_bool.m_income)
			{
				_zWhereClause += z_sep+"income='"+income() + "'";
				z_sep=" AND ";
			}
		}
		public String MyWhere()
			{
				return z_MyWhere ;
			}
		public void MyWhere(String value)
		{
				z_bool.MyWhere = true;
				z_MyWhere = value;
				if (z_bool.MyWhere)
				{
					_zWhereClause += z_sep + z_MyWhere;
					z_sep=" AND ";
				}
		}
		public String WhereClause()
		{
			return _zWhereClause;
		}
	}
}