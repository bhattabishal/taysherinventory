package com.tia.inventory.view;



import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.BeforeSaveMessages;
import com.tia.common.util.CmbQry;
import com.tia.common.util.DecimalTextPositiveOnly;
import com.tia.common.util.IntegerTextPositiveOnly;
import com.tia.common.util.SearchBox;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;

import com.tia.inventory.controller.StockItemController;
import com.tia.inventory.model.StockItemModel;
import com.tia.master.controller.UnitMapController;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;

import org.eclipse.swt.widgets.Combo;

public class StockEntryView extends Dialog {
	public static String StockEntry_ViewId = "com.tia.inven.stockEntry";
	private Composite comParent;
	private Composite comMainContainer;
	private Text txtName;
	private Text txtCode;
	private Text txtRackNo;
	private Label lblCellNo;
	private Label lblAddress;
	private Text txtRemarks;
	private Group grpCustomerDetails;
	private Label lblName;
	private Label lblPhone;
	private Label lblContactPerson;

	private StockItemController objController;
	private StockItemModel objModel;
	private SimpleDateFormat sdf;
	private UtilMethods objUtilMethods;
	private static StockEntryView stockEntryView;
	private boolean uglyFlag;
	private Text txtMinStock;
	private Text txtSellingPrice;
	private Button btnSave;
	private Button btnClear;
	private SwtBndCombo bcmbCategory;
	private SwtBndCombo bcmbSellingUnit;
	private SwtBndCombo bcmbPurUnit;
	private int base_pur_unitId=0;
	private int whole_pur_unitId=0;
	private int itemId=0;
	private String form="main";
	private BeforeSaveMessages msg=new BeforeSaveMessages();
	Object rtn;
	private Shell shell;
	private Text txtWeight;
	
	public StockEntryView(Shell parentShell, int purId,String forms) {
		//setTitleImage(SWTResourceManager.getImage(StockEntryView.class, "/customer16.png"));
		super(parentShell);
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		objController = new StockItemController();
		objModel = new StockItemModel();

		objUtilMethods = new UtilMethods();
		stockEntryView=this;
		this.itemId=purId;
		this.form=forms;

	}
//	public StockEntryView(Shell parentShell) {
//		super(parentShell);
//		
//		
//	}
	public static StockEntryView getInstance() {
		return stockEntryView;
	}
	
	public Object open() {
		Shell parent = getParent();
		shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setText("Stock Item Entry");
		shell.setSize(1000, 296);
		// Your code goes here (widget creation, set result, etc).
		createDialogArea(shell);
		shell.open();
		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		return rtn;
	}
	// @Override
	@SuppressWarnings("deprecation")
	public Control createDialogArea(Composite parent) {

		/*
		 * The top-most composite that holds title bar and the comMainContainer
		 * composite together.
		 */
		comParent = parent;
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("Stock Item Entry", comParent,
				StockEntryView.class);

		// Main container that holds all other controls/widgets of the view
		comMainContainer = new Composite(comParent, SWT.NONE);
		comMainContainer.setLayout(new FormLayout());
		GridData gd_comMainContainer = new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1);
		gd_comMainContainer.widthHint = 983;
		gd_comMainContainer.heightHint = 257;
		comMainContainer.setLayoutData(gd_comMainContainer);

		// Group holding widgets to enter the customer details
		grpCustomerDetails = new Group(comMainContainer, SWT.NONE);
		FormData fd_grpCustomerDetails = new FormData();
		fd_grpCustomerDetails.top = new FormAttachment(0, 10);
		fd_grpCustomerDetails.left = new FormAttachment(0, 10);
		fd_grpCustomerDetails.bottom = new FormAttachment(0, 208);
		fd_grpCustomerDetails.right = new FormAttachment(0, 973);
		grpCustomerDetails.setLayoutData(fd_grpCustomerDetails);
		grpCustomerDetails.setText("Item Details");

		// Name label
		lblName = new Label(grpCustomerDetails, SWT.NONE);
		lblName.setAlignment(SWT.RIGHT);
		lblName.setBounds(10, 31, 75, 18);
		lblName.setText("Name:");
		lblName.setForeground(UtilMethods.getFontForegroundColorRed());

		// Phone label
		lblPhone = new Label(grpCustomerDetails, SWT.NONE);
		lblPhone.setAlignment(SWT.RIGHT);
		lblPhone.setBounds(345, 31, 55, 15);
		lblPhone.setText("Code:");
		lblPhone.setForeground(UtilMethods.getFontForegroundColorRed());

		// Contact Person label
		lblContactPerson = new Label(grpCustomerDetails, SWT.NONE);
		lblContactPerson.setAlignment(SWT.RIGHT);
		lblContactPerson.setBounds(653, 31, 75, 18);
		lblContactPerson.setText("Rack No:");
		lblContactPerson.setForeground(UtilMethods.getFontForegroundColorRed());

		// Name text widget
		txtName = new Text(grpCustomerDetails, SWT.BORDER);
		txtName.setBounds(91, 28, 219, 21);

		// Phone text widget
		txtCode = new Text(grpCustomerDetails, SWT.BORDER);
		txtCode.setBounds(406, 28, 219, 21);

		// Contact Person text widget
		txtRackNo = new Text(grpCustomerDetails, SWT.BORDER);
		txtRackNo.setBounds(734, 28, 219, 21);

		// Cell No. label
		lblCellNo = new Label(grpCustomerDetails, SWT.NONE);
		lblCellNo.setAlignment(SWT.RIGHT);
		lblCellNo.setBounds(10, 63, 76, 15);
		lblCellNo.setText("Category:");
		lblCellNo.setForeground(UtilMethods.getFontForegroundColorRed());

		// Address label
		lblAddress = new Label(grpCustomerDetails, SWT.NONE);
		lblAddress.setAlignment(SWT.RIGHT);
		lblAddress.setBounds(30, 143, 55, 15);
		lblAddress.setText("Remarks:");

		// Address text widget
		txtRemarks = new Text(grpCustomerDetails, SWT.BORDER | SWT.WRAP);
		txtRemarks.setBounds(91, 128, 219, 45);
		
		bcmbCategory = new SwtBndCombo(grpCustomerDetails, SWT.READ_ONLY);
		bcmbCategory.setBounds(91, 60, 219, 23);
		bcmbCategory.fillData(CmbQry.getStockCategory(), CmbQry.getLabel(), CmbQry.getId(), DbConnection.getConnection());
		
		Label lblItemMinimumStock = new Label(grpCustomerDetails, SWT.NONE);
		lblItemMinimumStock.setBounds(313, 63, 88, 15);
		lblItemMinimumStock.setText("Minimum Stock:");
		lblItemMinimumStock.setForeground(UtilMethods.getFontForegroundColorRed());
		
		txtMinStock = new Text(grpCustomerDetails, SWT.BORDER);
		txtMinStock.setBounds(407, 60, 219, 21);
		txtMinStock.addListener(SWT.Verify, new IntegerTextPositiveOnly(
				txtMinStock, 20));
		
		Label lblSellingPrice = new Label(grpCustomerDetails, SWT.NONE);
		lblSellingPrice.setAlignment(SWT.RIGHT);
		lblSellingPrice.setBounds(324, 98, 76, 15);
		lblSellingPrice.setText("Selling Price:");
		
		Label lblSellingUnit = new Label(grpCustomerDetails, SWT.NONE);
		lblSellingUnit.setAlignment(SWT.RIGHT);
		lblSellingUnit.setBounds(11, 98, 76, 15);
		lblSellingUnit.setText("Selling Unit:");
		lblSellingUnit.setForeground(UtilMethods.getFontForegroundColorRed());
		
		txtSellingPrice = new Text(grpCustomerDetails, SWT.BORDER);
		txtSellingPrice.setBounds(406, 94, 219, 21);
		txtSellingPrice.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtSellingPrice, 20));
		
		if(itemId <= 0)
		{
			txtSellingPrice.setEnabled(false);
		}
		
		Label lblPurchaseUnit = new Label(grpCustomerDetails, SWT.NONE);
		lblPurchaseUnit.setBounds(653, 62, 75, 15);
		lblPurchaseUnit.setText("Purchase Unit:");
		lblPurchaseUnit.setForeground(UtilMethods.getFontForegroundColorRed());
		
		btnSave = new Button(grpCustomerDetails, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				beforeSave();
				
				if(msg.returnMessages())
				{
					saveAction();
				}
			}
		});
		btnSave.setBounds(410, 143, 75, 25);
		btnSave.setText("Save");
		
		btnClear = new Button(grpCustomerDetails, SWT.NONE);
		btnClear.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clearAll();
				
				
			}
		});
		btnClear.setBounds(500, 143, 75, 25);
		btnClear.setText("clear");
		
		bcmbPurUnit = new SwtBndCombo(grpCustomerDetails, SWT.READ_ONLY);
		bcmbPurUnit.setBounds(734, 60, 219, 23);
		
		bcmbSellingUnit = new SwtBndCombo(grpCustomerDetails, SWT.READ_ONLY);
		bcmbSellingUnit.setBounds(91, 94, 219, 23);
		
		Label lblItemWeight = new Label(grpCustomerDetails, SWT.NONE);
		lblItemWeight.setAlignment(SWT.RIGHT);
		lblItemWeight.setBounds(653, 94, 75, 15);
		lblItemWeight.setText("Unit Weight: ");
		lblItemWeight.setForeground(UtilMethods.getFontForegroundColorRed());
		
		txtWeight = new Text(grpCustomerDetails, SWT.BORDER);
		txtWeight.setBounds(734, 92, 219, 21);
		txtWeight.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtWeight, 20));
		bcmbSellingUnit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(bcmbPurUnit.getSelectedBoundValue().equals("0"))
				{
					msg.showIndividualMessage("Please select purchase unit first", 0);
					bcmbSellingUnit.setSelectedBoundValue("0");
				}
			}
		});
		bcmbSellingUnit.fillData(CmbQry.getUnit(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection());
		bcmbPurUnit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				bcmbSellingUnit.setSelectedBoundValue("0");
				
				if(!bcmbPurUnit.getSelectedBoundValue().toString().equals("0"))
				{
					bcmbSellingUnit.fillData(CmbQry.getCommonBaseTypeUnits(bcmbPurUnit.getSelectedBoundValue().toString()), CmbQry.getLabel(),
							CmbQry.getId(), DbConnection.getConnection());
				}
				
				
			}
		});
		bcmbPurUnit.fillData(CmbQry.getUnit(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection());

		// fill the table
		loadData(itemId);
		return comParent;
	}

	
private void beforeSave()
{
	if(txtName.getText().equals(""))
	{
		msg.setMandatoryMessages("Item name is required");
	}
	if(txtCode.getText().equals(""))
	{
		msg.setMandatoryMessages("Item code is required");
	}
	else
	{
		if(itemId <= 0)
		{
			if(UtilFxn.isNotUnique("select * from inven_item where item_code='"+txtCode.getText()+"'"))
			{
				msg.setMandatoryMessages("Item Code you entered is already in use");
			}
		}
		else
		{
			if(UtilFxn.isNotUnique("select * from inven_item_purchase where bill_no='"+txtCode.getText()+"' and item_id !='"+itemId+"'"))
			{
				msg.setMandatoryMessages("Item Code you entered is already in use");
			}
		}
	}
	if(txtRackNo.getText().equals(""))
	{
		msg.setMandatoryMessages("Rack no is required");
	}
	if(bcmbCategory.getSelectedBoundValue().equals("0"))
	{
		msg.setMandatoryMessages("Item category is required");
	}
	
	
	
	if(txtMinStock.getText().equals(""))
	{
		msg.setMandatoryMessages("Minimum stock is required");
	}
	else if(txtMinStock.getText().equals("0"))
	{
		msg.setMandatoryMessages("Minimum Stock must be greater than 0");
	}
	
	if(bcmbPurUnit.getSelectedBoundValue().equals("0"))
	{
		msg.setMandatoryMessages("Purchased unit is required");
	}
	if(bcmbSellingUnit.getSelectedBoundValue().equals("0"))
	{
		msg.setMandatoryMessages("Selling unit is required");
	}
	
	if(itemId > 0)
	{
		if(txtSellingPrice.getText().equals(""))
		{
			msg.setMandatoryMessages("Selling price is required");
		}
		else if(Double.parseDouble(txtSellingPrice.getText()) <= 0)
		{
			msg.setMandatoryMessages("Selling price must be greater than 0");
		}
	}
//	else
//	{
//		if(txtSellingPrice.getText().equals(""))
//		{
//			msg.setMandatoryMessages("Selling price is required");
//		}
//	}
	
	if(txtWeight.getText().equals(""))
	{
		msg.setMandatoryMessages("Item Unit Weight is required");
	}
		
	
	
}
	
private void clearAll()
{
	txtName.setText("");
	txtCode.setText("");
	txtRackNo.setText("");
	bcmbCategory.setSelectedBoundValue("0");
	
	txtMinStock.setText("");
	
	bcmbPurUnit.fillData(CmbQry.getUnit(), CmbQry.getLabel(),
			CmbQry.getId(), DbConnection.getConnection());
	bcmbSellingUnit.fillData(CmbQry.getUnit(), CmbQry.getLabel(),
			CmbQry.getId(), DbConnection.getConnection());
	txtSellingPrice.setText("");
	txtRemarks.setText("");
	txtWeight.setText("");
	
	base_pur_unitId=0;
	whole_pur_unitId=0;
}
	
private void saveAction()
{
	
		objModel.setItem_id(itemId);
	
	
	objModel.setItem_name(txtName.getText());
	objModel.setItem_code(txtCode.getText());
	objModel.setItem_rack_no(txtRackNo.getText());
	objModel.setItem_description(txtRemarks.getText());
	objModel.setCat_id(Integer.valueOf(bcmbCategory.getSelectedBoundValue().toString()));
	objModel.setItem_min_stock(Integer.valueOf(txtMinStock.getText().equals("")?"0":txtMinStock.getText()));
	objModel.setItem_selling_price(txtSellingPrice.getText().equals("")?"0":txtSellingPrice.getText());
	objModel.setUnit_weight(txtWeight.getText().equals("")?"0":txtWeight.getText());
		objModel.setItem_pur_unit_id(Integer.valueOf(bcmbPurUnit.getSelectedBoundValue().toString()));
		objModel.setItem_sel_unit_id(Integer.valueOf(bcmbSellingUnit.getSelectedBoundValue().toString()));
	objModel.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
	
	if(itemId <= 0)
	{
		objModel.setCrtd_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		objModel.setCrtd_dt(JCalendarFunctions.currentDate());
		objModel.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		objModel.setUpdt_dt(JCalendarFunctions.currentDate());
		objModel.setUpdt_cnt(0);
	}
	else
	{
		objModel.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		objModel.setUpdt_dt(JCalendarFunctions.currentDate());
		objModel.setUpdt_cnt(objModel.getUpdt_cnt()+1);
	}
	
	
	
	
	try
	{
		Connection cnn=DbConnection.getConnection();
		cnn.setAutoCommit(false);
		if(objController.saveItemDetails(objModel))
		{
			if(itemId > 0)
			{
				msg.showIndividualMessage("Item updated successfully", 0);
				
			}
			else
			{
				msg.showIndividualMessage("Item inserted successfully", 0);
			}
			cnn.commit();
			//clearAll();
			IWorkbenchPage wbp = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			try {
				if(form.equals("main"))
				{
					wbp.showView("com.tia.inven.stockList");
					StockListView.getInstance().refereshTable();;
				}
				else if(form.equals("stockpurchase"))
				{
					//wbp.showView("com.tia.inven.stockPurchasedEntry");
					StockPurchasedEntryView.getInstance().reloadItem();
				}
				
				shell.close();
				
		         } catch (PartInitException e1) {
				e1.printStackTrace();
			     }
		}
		else
		{
			msg.showIndividualMessage("Some error occured.Please try again", 1);
			cnn.rollback();
			cnn.setAutoCommit(true);
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
}
	

	public void loadData(int itemId)
	{
		this.itemId=itemId;
		ResultSet rs=objController.fetchItemDetails(itemId);
		
		try {
			if(rs.next())
			{
				txtName.setText(rs.getString("item_name"));
				txtCode.setText(rs.getString("item_code"));
				txtRackNo.setText(rs.getString("item_rack_no"));
				txtRemarks.setText(rs.getString("item_description"));
				bcmbCategory.setSelectedBoundValue(rs.getInt("cat_id"));
				txtMinStock.setText(String.valueOf(rs.getInt("item_min_stock")));
				txtSellingPrice.setText(String.valueOf(rs.getBigDecimal("item_selling_price")==null?"":rs.getBigDecimal("item_selling_price")));
				txtWeight.setText(String.valueOf(rs.getBigDecimal("unit_weight")==null?"":rs.getBigDecimal("unit_weight")));
				bcmbPurUnit.setSelectedBoundValue(rs.getInt("item_pur_unit_id"));
				bcmbSellingUnit.fillData(CmbQry.getCommonBaseTypeUnits(bcmbPurUnit.getSelectedBoundValue().toString()), CmbQry.getLabel(),
						CmbQry.getId(), DbConnection.getConnection());
				bcmbSellingUnit.setSelectedBoundValue(rs.getInt("item_sel_unit_id"));
				
				objModel.setUpdt_cnt(rs.getInt("updt_cnt"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
