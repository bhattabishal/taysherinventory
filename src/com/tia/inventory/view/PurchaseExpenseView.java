package com.tia.inventory.view;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.BeforeSaveMessages;
import com.tia.common.util.CmbQry;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.inventory.controller.CustomerSupplierController;
import com.tia.inventory.controller.PurchaseExpenseController;
import com.tia.inventory.controller.ServiceController;
import com.tia.inventory.model.CustomerModelClass;
import com.tia.inventory.model.PurchaseExpenseModel;
import com.tia.inventory.model.ServiceModel;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import com.tia.user.controller.encryptDectyptPassword;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.swt.widgets.Combo;

public class PurchaseExpenseView extends Dialog {
	private Text txtName;
	private Text txtRemarks;
	private UtilMethods objUtilMethods;
	private Composite comParent;
	private Composite comMainContainer;
	Object rtn;
	private SwtBndCombo bcmbCr;
	int id=0;
	String form="main";
	private BeforeSaveMessages msg;
	private PurchaseExpenseController control;
	private PurchaseExpenseModel model;
	private Shell shell;
	private SwtBndCombo bcmbDr;

	public PurchaseExpenseView(Shell parentShell,int id,String opnForm) {
		super(parentShell);
		objUtilMethods = new UtilMethods();
		this.id=id;
		this.form=opnForm;
		msg = new BeforeSaveMessages();
		control=new PurchaseExpenseController();
		model=new PurchaseExpenseModel();

	}

	public Object open() {
		Shell parent = getParent();
		shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setText("Purchase Expense Management");
		shell.setSize(590, 257);
		// Your code goes here (widget creation, set result, etc).
		createDialogArea(shell);
		shell.open();
		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		
		return rtn;
	}

	protected Control createDialogArea(Composite parent) {
		comParent = parent;// (Composite) super.createDialogArea(parent);
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("Purchase Expense Management", comParent,
				CustomerManagementView.class);

		// Main container that holds all other controls/widgets of the view
		comMainContainer = new Composite(comParent, SWT.NONE);
		comMainContainer.setLayout(new FormLayout());
		GridData gd_comMainContainer = new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1);
		gd_comMainContainer.widthHint = 983;
		gd_comMainContainer.heightHint = 216;
		comMainContainer.setLayoutData(gd_comMainContainer);

		Group grpPassword = new Group(comMainContainer, SWT.NONE);
		FormData fd_grpPassword = new FormData();
		fd_grpPassword.bottom = new FormAttachment(0, 180);
		fd_grpPassword.top = new FormAttachment(0);
		fd_grpPassword.left = new FormAttachment(0, 10);
		fd_grpPassword.right = new FormAttachment(0, 575);
		grpPassword.setLayoutData(fd_grpPassword);
		grpPassword.setText("Service Management");
		grpPassword.setBounds(206, 25, 388, 282);

		Button btnSave = new Button(grpPassword, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				beforeSave();
				if(msg.returnMessages())
				{
					model=new PurchaseExpenseModel();
					model.setExp_id(id);
					if(id <= 0)
					model.setCrtd_dt(JCalendarFunctions.currentDate());
					model.setName(txtName.getText());
					model.setExpense_cr(bcmbCr.getSelectedBoundValue().toString());
					model.setExpense_dr(bcmbDr.getSelectedBoundValue().toString());
					
					model.setRemarks(txtRemarks.getText());
					model.setCrtd_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
					
					Connection cnn=DbConnection.getConnection();
					try {
						cnn.setAutoCommit(false);
						if(control.saveDetails(model))
						{
							msg.showIndividualMessage("Successfully Saved/Updated.", 0);
							//clearAll();
							
							
							IWorkbenchPage wbp = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
							try {
								if(form.equals("main"))
								{
									wbp.showView("com.tia.inven.cusSuppList");
									CustomerSupplierListView.getInstance().refershTable();;
								}
								else if(form.equals("stocksales"))
								{
									StockSalesEntryView.getInstance().reloadService();
								}
								
								shell.close();
								
						         } catch (PartInitException e1) {
								e1.printStackTrace();
							     }
						}
						else
						{
							msg.showIndividualMessage("Some error occured while saving.Please try again.", 0);
						}
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		btnSave.setBounds(130, 143, 120, 25);
	
		btnSave.setText("Save");

		Button btnCancel = new Button(grpPassword, SWT.NONE);
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clearAll();
			}
		});
		btnCancel.setBounds(266, 143, 114, 25);
		
		btnCancel.setText("Clear");

		Label lblConformPassword = new Label(grpPassword, SWT.NONE);
		lblConformPassword.setBounds(295, 89, 114, 15);
		lblConformPassword.setText("Remarks:  ");
		lblConformPassword.setAlignment(SWT.RIGHT);

		txtRemarks = new Text(grpPassword, SWT.BORDER);
		txtRemarks.setBounds(415, 87, 136, 48);

		txtName = new Text(grpPassword, SWT.BORDER);
		txtName.setBounds(130, 49, 136, 21);

		Label lblCurrentPassword = new Label(grpPassword, SWT.NONE);
		lblCurrentPassword.setBounds(10, 52, 114, 15);
		lblCurrentPassword.setText("Name: ");
		lblCurrentPassword.setAlignment(SWT.RIGHT);
		lblCurrentPassword.setForeground(UtilMethods.getFontForegroundColorRed());
		
		Label lblCode = new Label(grpPassword, SWT.NONE);
		lblCode.setText("Dr Mapping: ");
		lblCode.setAlignment(SWT.RIGHT);
		lblCode.setBounds(295, 52, 114, 15);
		lblCode.setForeground(UtilMethods.getFontForegroundColorRed());
		
		Label lblType_1 = new Label(grpPassword, SWT.NONE);
		lblType_1.setText("Cr Mapping: ");
		lblType_1.setAlignment(SWT.RIGHT);
		lblType_1.setBounds(10, 89, 114, 15);
		lblType_1.setForeground(UtilMethods.getFontForegroundColorRed());
		
		bcmbCr = new SwtBndCombo(grpPassword, SWT.READ_ONLY);
		bcmbCr.setBounds(130, 87, 136, 21);
		
		bcmbDr = new SwtBndCombo(grpPassword, SWT.READ_ONLY);
		bcmbDr.setBounds(415, 49, 136, 23);
		bcmbCr.fillData(CmbQry.getCashBankMapAccCodeQuery(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		bcmbDr.fillData(CmbQry.getCashBankMapAccCodeQuery(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		

		grpPassword.setTabList(new Control[]{txtName, bcmbDr,bcmbCr,txtRemarks, btnSave, btnCancel});
		//loadData();

		return comParent;

		// TODO Auto-generated method stub

	}
	
//	private void loadData()
//	{
//		model=control.fetchItemDetails(id);
//		if(model !=null)
//		{
//			txtAddress.setText(model.getAddress());
//			bcmbType.setText(model.getContact_person());
//			txtEmail.setText(model.getEmail());
//			txtName.setText(model.getName());
//			txtRemarks.setText(model.getMobile());
//			txtCode.setText(model.getPhone());
//			txtRemarks.setText(model.getRemarks());
//			bcmbType.setSelectedBoundValue(model.getType());
//		}
//		
//	}
	private void clearAll()
	{
		//txtAddress.setText("");
		bcmbCr.setSelectedBoundValue("0");
		//txtEmail.setText("");
		txtName.setText("");
		txtRemarks.setText("");
		bcmbDr.setSelectedBoundValue("0");
		
		
	}
	
	private void beforeSave()
	{
		if(txtName.getText().equals(""))
		{
			msg.setMandatoryMessages("Expense Name is required");
		}
		else
		{
			if(id <= 0)
			{
				if(control.fetchCheckByCondition("name='"+txtName.getText()+"'"))
				{
					msg.setMandatoryMessages("Name you entered already exists");
				}
			}
			else
			{
				if(control.fetchCheckByCondition("name='"+txtName.getText()+"' AND exp_id !='"+id+"'"))
				{
					msg.setMandatoryMessages("Name you entered already exists");
				}
			}
		}
		if(bcmbDr.getSelectedBoundValue().toString().equals("0"))
		{
			msg.setMandatoryMessages("Please select Account dr.");
		}
		if(bcmbCr.getSelectedBoundValue().toString().equals("0"))
		{
			msg.setMandatoryMessages("Please select Account cr.");
		}
	}
}


