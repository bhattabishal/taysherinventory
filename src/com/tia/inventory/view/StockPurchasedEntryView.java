package com.tia.inventory.view;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.nebula.widgets.datechooser.DateChooserCombo;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.controller.JournalEntryController;
import com.tia.account.model.JVDetailsModel;
import com.tia.account.model.JVSummary;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.BeforeSaveMessages;
import com.tia.common.util.CmbQry;
import com.tia.common.util.DecimalTextPositiveOnly;
import com.tia.common.util.IntegerTextPositiveOnly;
import com.tia.common.util.SearchBox;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;

import com.tia.inventory.controller.StockItemController;
import com.tia.inventory.controller.StockPurchasedController;
import com.tia.inventory.model.DayBookDetailsModel;
import com.tia.inventory.model.DayBookModel;
import com.tia.inventory.model.StockPurchasedDetailsModel;
import com.tia.inventory.model.StockPurchasedModel;
import com.tia.inventory.model.StockPurchasedTrxnModel;
import com.tia.inventory.model.StockPurchseExpenseTrxnModel;
import com.tia.master.controller.UnitMapController;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import org.eclipse.swt.widgets.Combo;
import com.swtdesigner.SWTResourceManager;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;

public class StockPurchasedEntryView extends Dialog {
	private Composite comParent;
	private Composite comMainContainer;

	private JCalendarFunctions jcalFxn;

	private int purchasedId=0;
	double totalAmount=0;
	double itemEquivalence=0;
	int unit_id=0;
	String itemAction="add";
	String action="add";
	int jvNo=0;
	int expJvNo=0;
	StockPurchasedModel stockPurchasedModel=new StockPurchasedModel();
	StockPurchasedDetailsModel stockPurchasedDetailsModel=new StockPurchasedDetailsModel();
	StockPurchasedTrxnModel stockPurchasedTrxnModel=new StockPurchasedTrxnModel();
	ArrayList<StockPurchasedDetailsModel> itemsList=new ArrayList<StockPurchasedDetailsModel>();
	ArrayList<StockPurchseExpenseTrxnModel> expenseList=new ArrayList<StockPurchseExpenseTrxnModel>();
	ArrayList<StockPurchasedTrxnModel> sourceList=new ArrayList<StockPurchasedTrxnModel>();
	StockPurchasedController control=new StockPurchasedController();
	//JVDetailsModel objJVDetails = new JVDetailsModel();
	//JVSummary jvSummary = new JVSummary();
	//JVSummary jvSummaryExpense= new JVSummary();
	//ArrayList<JVDetailsModel> jvDetailList = new ArrayList<JVDetailsModel>();
	//ArrayList<JVDetailsModel> jvDetailExpenseList = new ArrayList<JVDetailsModel>();
	DayBookModel daybook=new DayBookModel();
	ArrayList<DayBookDetailsModel> dayBookDetailList = new ArrayList<DayBookDetailsModel>();

	Object rtn;
	private Text txtBillNo;
	private Text txtWeight;
	private Text txtTotalWeight;
	private Text txtUnit;
	private Text txtUnitQuantity;
	private Text txtRate;
	private Text txtActualRate;
	private Text txtTotal;
	private Table tblItem;
	private Text txtDueAmount;
	private Text txtRemarks;
	private Text txtCash;
	private Text txtBank;
	private Table tblSourceAmount;
	private SwtBndCombo bcmbSupplier;
	private SwtBndCombo bcmbBrand;
	private SwtBndCombo bcmbLocalBrandType;
	private SearchBox bcmbItem;
	private SwtBndCombo bcmbCash;
	private SwtBndCombo bcmbBank;
	private static StockPurchasedEntryView stockPurchaseEntry;
	private DateChooserCombo bcmbPurDate;
	private Text txtSellingPrice;
	private BeforeSaveMessages msg=new BeforeSaveMessages();
	private Button btnCash;
	private Button btnBank;
	private Text txtItemTotal;
	private Text txtPaidAmount;
	private Table tblExpense;
	private Text txtExpenseAmt;
	private Text txtTotExpense;
	private SwtBndCombo bcmbExpenses;

	public StockPurchasedEntryView(Shell parentShell, int purId) {
		super(parentShell);
		

		jcalFxn = new JCalendarFunctions();
		this.stockPurchaseEntry=this;
		this.purchasedId = purId;
		
	}

	/**
	 * @wbp.parser.constructor
	 */
	public StockPurchasedEntryView(Shell parentShell) {
		super(parentShell);
		

		jcalFxn = new JCalendarFunctions();
		
	}
	public static StockPurchasedEntryView getInstance() {
		return stockPurchaseEntry;
	}

	public Object open() {
		Shell parent = getParent();
		Shell shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setText("Stock Purchased Entry");
		shell.setSize(1000, 680);
		// Your code goes here (widget creation, set result, etc).
		createDialogArea(shell);
		shell.open();
		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		return rtn;
	}

	// @Override
	@SuppressWarnings("deprecation")
	protected Control createDialogArea(Composite parent) {
		comParent = parent;// (Composite) super.createDialogArea(parent);
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("Stock Purchased Entry", comParent,
				StockPurchasedEntryView.class);

		// Main container that holds all other controls/widgets of the view
		comMainContainer = new Composite(comParent, SWT.NONE);
		comMainContainer.setLayout(new FormLayout());
		GridData gd_comMainContainer = new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1);
		gd_comMainContainer.widthHint = 983;
		gd_comMainContainer.heightHint = 615;
		comMainContainer.setLayoutData(gd_comMainContainer);
		
		Group grpStockInformation = new Group(comMainContainer, SWT.NONE);
		grpStockInformation.setText("Stock Information");
		FormData fd_grpStockInformation = new FormData();
		fd_grpStockInformation.top = new FormAttachment(0);
		fd_grpStockInformation.left = new FormAttachment(0, 10);
		grpStockInformation.setLayoutData(fd_grpStockInformation);
		
		Label lblSupplier = new Label(grpStockInformation, SWT.NONE);
		lblSupplier.setAlignment(SWT.RIGHT);
		lblSupplier.setBounds(68, 51, 62, 15);
		lblSupplier.setText("Supplier: ");
		lblSupplier.setForeground(UtilMethods.getFontForegroundColorRed());
		
		bcmbSupplier = new SwtBndCombo(grpStockInformation, SWT.READ_ONLY);
		bcmbSupplier.setBounds(134, 46, 135, 23);
		bcmbSupplier.fillData(CmbQry.getAllSuppliers(2), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		
		Button btnSupplierAdd = new Button(grpStockInformation, SWT.NONE);
		btnSupplierAdd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String form="stockpurchase";
				CustomerManagementView hms = new CustomerManagementView(
						PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getShell(),0,form);
				hms.open();
			}
		});
		btnSupplierAdd.setBounds(270, 45, 38, 25);
		btnSupplierAdd.setText("add");
		
		Label lblBillNo = new Label(grpStockInformation, SWT.NONE);
		lblBillNo.setAlignment(SWT.RIGHT);
		lblBillNo.setBounds(69, 79, 62, 15);
		lblBillNo.setText("Bill No: ");
		lblBillNo.setForeground(UtilMethods.getFontForegroundColorRed());
		
		txtBillNo = new Text(grpStockInformation, SWT.BORDER);
		txtBillNo.setBounds(134, 77, 135, 21);
		
		Label lblPurchasedDate = new Label(grpStockInformation, SWT.NONE);
		lblPurchasedDate.setAlignment(SWT.RIGHT);
		lblPurchasedDate.setBounds(33, 20, 97, 15);
		lblPurchasedDate.setText("Purchased Date: ");
		lblPurchasedDate.setForeground(UtilMethods.getFontForegroundColorRed());
		
		bcmbPurDate = new DateChooserCombo(grpStockInformation, SWT.NONE);
		bcmbPurDate.setBounds(134, 20, 86, 17);
		bcmbPurDate.setValue(new Date());
		
		Label lblItemBrand = new Label(grpStockInformation, SWT.NONE);
		lblItemBrand.setAlignment(SWT.RIGHT);
		lblItemBrand.setBounds(28, 109, 105, 15);
		lblItemBrand.setText("Item Purchased: ");
		lblItemBrand.setForeground(UtilMethods.getFontForegroundColorRed());
		
		bcmbBrand = new SwtBndCombo(grpStockInformation, SWT.READ_ONLY);
		bcmbBrand.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(bcmbBrand.getSelectedBoundValue().toString().equals("2"))
				{
					bcmbLocalBrandType.setEnabled(false);
				}
				else
				{
					bcmbLocalBrandType.setEnabled(true);
				}
			}
		});
		bcmbBrand.setBounds(134, 106, 135, 23);
		bcmbBrand.fillData(CmbQry.getStockBrand(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		
		Group grpStockDetails = new Group(comMainContainer, SWT.NONE);
		fd_grpStockInformation.bottom = new FormAttachment(grpStockDetails, -6);
		grpStockDetails.setText("Stock Details");
		FormData fd_grpStockDetails = new FormData();
		fd_grpStockDetails.top = new FormAttachment(0, 196);
		fd_grpStockDetails.left = new FormAttachment(0, 10);
		fd_grpStockDetails.right = new FormAttachment(100, -10);
		
		Label lblLocalPurchasedType = new Label(grpStockInformation, SWT.NONE);
		lblLocalPurchasedType.setBounds(311, 109, 121, 15);
		lblLocalPurchasedType.setText("Local Purchased Type: ");
		
		bcmbLocalBrandType = new SwtBndCombo(grpStockInformation, SWT.READ_ONLY);
		bcmbLocalBrandType.setBounds(436, 106, 135, 23);
		grpStockDetails.setLayoutData(fd_grpStockDetails);
		bcmbLocalBrandType.fillData(CmbQry.getLocalBrand(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		
		Label lblItem = new Label(grpStockDetails, SWT.NONE);
		lblItem.setAlignment(SWT.RIGHT);
		lblItem.setBounds(10, 20, 65, 15);
		lblItem.setText("Item: ");
		lblItem.setForeground(UtilMethods.getFontForegroundColorRed());
		
		bcmbItem = new SearchBox(grpStockDetails, SWT.NONE);
		bcmbItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(!bcmbItem.getSelectedBoundValue().toString().equals("0"))
				{
					clearItemDetails();
					loadItemDetails(Integer.valueOf(bcmbItem.getSelectedBoundValue().toString()));
				}
				else
				{
					clearItemDetails();
				}
			}
		});
		bcmbItem.setBounds(76, 18, 132, 23);
		bcmbItem.fillData(CmbQry.getStockItems(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		
		Button btnItemAdd = new Button(grpStockDetails, SWT.NONE);
		btnItemAdd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String form="stockpurchase";
				StockEntryView hms = new StockEntryView(
						PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getShell(),0,form);
				hms.open();
			}
		});
		btnItemAdd.setText("add");
		btnItemAdd.setBounds(214, 17, 38, 25);
		
		Label lblUnit = new Label(grpStockDetails, SWT.NONE);
		lblUnit.setAlignment(SWT.RIGHT);
		lblUnit.setBounds(309, 22, 32, 15);
		lblUnit.setText("Unit: ");
		
		txtUnit = new Text(grpStockDetails, SWT.BORDER | SWT.READ_ONLY);
		txtUnit.setBounds(341, 20, 120, 21);
		
		Label lblUnitQuantity_1 = new Label(grpStockDetails, SWT.NONE);
		lblUnitQuantity_1.setAlignment(SWT.RIGHT);
		lblUnitQuantity_1.setBounds(651, 20, 82, 15);
		lblUnitQuantity_1.setText("Unit Quantity: ");
		lblUnitQuantity_1.setForeground(UtilMethods.getFontForegroundColorRed());
		
		txtUnitQuantity = new Text(grpStockDetails, SWT.BORDER);
		txtUnitQuantity.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calculateItemTotal();
			}
		});
		txtUnitQuantity.setBounds(733, 18, 76, 21);
		txtUnitQuantity.addListener(SWT.Verify, new IntegerTextPositiveOnly(
				txtUnitQuantity, 20));
		
		Label lblRate = new Label(grpStockDetails, SWT.NONE);
		lblRate.setAlignment(SWT.RIGHT);
		lblRate.setBounds(822, 20, 55, 15);
		lblRate.setText("Rate: ");
		lblRate.setForeground(UtilMethods.getFontForegroundColorRed());
		
		txtRate = new Text(grpStockDetails, SWT.BORDER);
		txtRate.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calculateItemTotal();
			}
		});
		txtRate.setBounds(877, 18, 76, 21);
		txtRate.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtRate, 20));
		
		Label lblActualRate = new Label(grpStockDetails, SWT.NONE);
		lblActualRate.setBounds(10, 52, 65, 15);
		lblActualRate.setText("Actual Rate: ");
		
		txtActualRate = new Text(grpStockDetails, SWT.BORDER | SWT.READ_ONLY);
		txtActualRate.setBounds(76, 50, 132, 21);
		
		Label lblTotal = new Label(grpStockDetails, SWT.NONE);
		lblTotal.setAlignment(SWT.RIGHT);
		lblTotal.setBounds(492, 49, 55, 15);
		lblTotal.setText("Total: ");
		
		txtTotal = new Text(grpStockDetails, SWT.BORDER | SWT.READ_ONLY);
		txtTotal.setBounds(547, 47, 76, 21);
		
		Group grpPurchaseExpenses = new Group(comMainContainer, SWT.NONE);
		fd_grpStockInformation.right = new FormAttachment(grpPurchaseExpenses, -6);
		grpPurchaseExpenses.setText("Purchase Expenses");
		FormData fd_grpPurchaseExpenses = new FormData();
		fd_grpPurchaseExpenses.bottom = new FormAttachment(grpStockDetails, -6);
		fd_grpPurchaseExpenses.top = new FormAttachment(0);
		fd_grpPurchaseExpenses.left = new FormAttachment(0, 599);
		fd_grpPurchaseExpenses.right = new FormAttachment(100, -10);
		grpPurchaseExpenses.setLayoutData(fd_grpPurchaseExpenses);
		
		Label Expenses = new Label(grpPurchaseExpenses, SWT.NONE);
		Expenses.setAlignment(SWT.RIGHT);
		Expenses.setBounds(10, 23, 54, 15);
		Expenses.setText("Expenses");
		
		bcmbExpenses = new SwtBndCombo(grpPurchaseExpenses, SWT.READ_ONLY);
		bcmbExpenses.setBounds(70, 20, 133, 23);
		bcmbExpenses.fillData(CmbQry.getAllExpenses(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		
		tblExpense = new Table(grpPurchaseExpenses, SWT.BORDER | SWT.FULL_SELECTION);
		tblExpense.setBounds(10, 49, 305, 112);
		tblExpense.setHeaderVisible(true);
		tblExpense.setLinesVisible(true);
		
		TableColumn tblclmnId = new TableColumn(tblExpense, SWT.NONE);
		tblclmnId.setWidth(0);
		tblclmnId.setText("S.N.");
		tblclmnId.setResizable(false);
		
		TableColumn tblclmnSn_2 = new TableColumn(tblExpense, SWT.NONE);
		tblclmnSn_2.setWidth(37);
		tblclmnSn_2.setText("S.N.");
		
		TableColumn tblclmnName = new TableColumn(tblExpense, SWT.NONE);
		tblclmnName.setWidth(168);
		tblclmnName.setText("Name");
		
		TableColumn tblclmnAmountrs = new TableColumn(tblExpense, SWT.NONE);
		tblclmnAmountrs.setWidth(94);
		tblclmnAmountrs.setText("Amount(Rs)");
		
		Label lblAmount = new Label(grpPurchaseExpenses, SWT.NONE);
		lblAmount.setAlignment(SWT.RIGHT);
		lblAmount.setBounds(228, 23, 54, 15);
		lblAmount.setText("Amount");
		
		txtExpenseAmt = new Text(grpPurchaseExpenses, SWT.BORDER);
		txtExpenseAmt.setBounds(288, 20, 76, 21);
		txtExpenseAmt.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtExpenseAmt, 20));
		
		Button btnAdd_1 = new Button(grpPurchaseExpenses, SWT.NONE);
		btnAdd_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(bcmbExpenses.getSelectedBoundValue().toString().equals("0"))
				{
					msg.setMandatoryMessages("Please select expense");
				}
				if(Double.parseDouble(txtExpenseAmt.getText().equals("")?"0":txtExpenseAmt.getText()) <= 0)
				{
					msg.setMandatoryMessages("Please enter expense amount");
				}
				if(msg.returnMessages())
				{
					addExpenseItem();
					calculateTotalExpenseAmount();
					clearExpense();
				}
			}
		});
		btnAdd_1.setBounds(321, 49, 43, 25);
		btnAdd_1.setText("Add");
		
		Button btnDelete_1 = new Button(grpPurchaseExpenses, SWT.NONE);
		btnDelete_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tblExpense.getSelectionIndex() < 0)
				{
					msg.showIndividualMessage("Please select the row to delete", 0);
				}
				else
				{
					if(msg.showIndividualMessage("Make sure before you delete the item", 2))
					{
						int selection=tblExpense.getSelectionIndex();
						tblExpense.remove(tblExpense.getSelectionIndex());
						
						if (tblExpense.getItemCount() > 0) {
							
							TableItem[] Item = tblExpense.getItems();
							for (int i = selection+1; i < tblExpense.getItemCount(); i++) {
								Item[i].setText(1, String.valueOf(tblExpense.getItemCount()));
								
							}
							
						}
						calculateTotalExpenseAmount();
					}
					
				}
			}
		});
		btnDelete_1.setBounds(321, 80, 43, 25);
		btnDelete_1.setText("Delete");
		
		Label lblTotal_2 = new Label(grpPurchaseExpenses, SWT.NONE);
		lblTotal_2.setAlignment(SWT.RIGHT);
		lblTotal_2.setBounds(165, 168, 55, 15);
		lblTotal_2.setText("Total");
		
		txtTotExpense = new Text(grpPurchaseExpenses, SWT.BORDER);
		txtTotExpense.setEditable(false);
		txtTotExpense.setBounds(222, 165, 93, 21);
		txtTotExpense.setForeground(UtilMethods.getFontForegroundColorRed());
		
		tblItem = new Table(comMainContainer, SWT.BORDER | SWT.FULL_SELECTION);
		fd_grpStockDetails.bottom = new FormAttachment(tblItem, -6);
		FormData fd_tblItem = new FormData();
		fd_tblItem.left = new FormAttachment(0, 10);
		fd_tblItem.right = new FormAttachment(100, -10);
		fd_tblItem.top = new FormAttachment(0, 284);
		
		Button btnAdd = new Button(grpStockDetails, SWT.NONE);
		btnAdd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				checkBeforeItemAdd();
				if(msg.returnMessages())
				{
					addItemInTable();
					calculateTotalAmount();
					clearItemDetails();
					bcmbItem.setSelectedBoundValue("0");
				}
			}
		});
		btnAdd.setBounds(720, 46, 75, 25);
		btnAdd.setText("Add");
		
		Button btnDelete = new Button(grpStockDetails, SWT.NONE);
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tblItem.getSelectionIndex() < 0)
				{
					msg.showIndividualMessage("Please seelct item to delete", 0);
				}
				else
				{
					if(msg.showIndividualMessage("Make sure before you delete the item", 2))
					{
						tblItem.remove(tblItem.getSelectionIndex());
						calculateTotalAmount();
					}
				}
			}
		});
		btnDelete.setBounds(878, 46, 75, 25);
		btnDelete.setText("Delete");
		
		Label lblSellingPrice = new Label(grpStockDetails, SWT.NONE);
		lblSellingPrice.setAlignment(SWT.RIGHT);
		lblSellingPrice.setBounds(262, 52, 76, 15);
		lblSellingPrice.setText("Selling Price: ");
		lblSellingPrice.setForeground(UtilMethods.getFontForegroundColorRed());
		
		txtSellingPrice = new Text(grpStockDetails, SWT.BORDER);
		txtSellingPrice.setBounds(341, 50, 120, 21);
		txtSellingPrice.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtSellingPrice, 20));
		
		Label lblUnitQuantity = new Label(grpStockDetails, SWT.NONE);
		lblUnitQuantity.setBounds(482, 22, 65, 15);
		lblUnitQuantity.setAlignment(SWT.RIGHT);
		lblUnitQuantity.setText("Weight(Kg): ");
		
		txtWeight = new Text(grpStockDetails, SWT.BORDER | SWT.READ_ONLY);
		txtWeight.setBounds(547, 18, 76, 21);
		txtWeight.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtWeight, 20));
		
		Button btnEdit = new Button(grpStockDetails, SWT.NONE);
		btnEdit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tblItem.getSelectionIndex() >= 0)
				{
					itemAction="edit";
					loadItemEditValues();
				}
				else
				{
					msg.showIndividualMessage("Please select item to edit", 0);
				}
			}
		});
		btnEdit.setBounds(801, 46, 75, 25);
		btnEdit.setText("Edit");
		
		Label lblRemarks = new Label(grpStockInformation, SWT.NONE);
		lblRemarks.setBounds(76, 141, 55, 15);
		lblRemarks.setAlignment(SWT.RIGHT);
		lblRemarks.setText("Remarks: ");
		
		txtRemarks = new Text(grpStockInformation, SWT.BORDER);
		txtRemarks.setBounds(134, 138, 437, 42);
		
		Label lblTotalWeightOf = new Label(grpStockInformation, SWT.NONE);
		lblTotalWeightOf.setBounds(320, 78, 112, 15);
		lblTotalWeightOf.setAlignment(SWT.RIGHT);
		lblTotalWeightOf.setText("Total Weight(Kg): ");
		
		txtTotalWeight = new Text(grpStockInformation, SWT.BORDER);
		txtTotalWeight.setBounds(435, 77, 136, 21);
		txtTotalWeight.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtTotalWeight, 20));
		tblItem.setLayoutData(fd_tblItem);
		tblItem.setHeaderVisible(true);
		tblItem.setLinesVisible(true);
		
		TableColumn tblclmniId = new TableColumn(tblItem, SWT.NONE);
		tblclmniId.setWidth(0);
		tblclmniId.setText("itemid.");
		tblclmniId.setResizable(false);
		
		TableColumn tblUnitId = new TableColumn(tblItem, SWT.NONE);
		tblUnitId.setWidth(0);
		tblUnitId.setText("unitid.");
		tblUnitId.setResizable(false);
		
		TableColumn tblclmnSn = new TableColumn(tblItem, SWT.NONE);
		tblclmnSn.setWidth(50);
		tblclmnSn.setText("S.N.");
		
		TableColumn tblclmnItemCode = new TableColumn(tblItem, SWT.NONE);
		tblclmnItemCode.setWidth(135);
		tblclmnItemCode.setText("Item Code");
		
		TableColumn tblclmnWeightkg = new TableColumn(tblItem, SWT.NONE);
		tblclmnWeightkg.setWidth(80);
		tblclmnWeightkg.setText("Weight(Kg)");
		
		TableColumn tblclmnUnit = new TableColumn(tblItem, SWT.NONE);
		tblclmnUnit.setWidth(142);
		tblclmnUnit.setText("Unit");
		
		TableColumn tblclmnUnitQuantity = new TableColumn(tblItem, SWT.NONE);
		tblclmnUnitQuantity.setWidth(100);
		tblclmnUnitQuantity.setText("Unit Quantity");
		
		TableColumn tblclmnRate = new TableColumn(tblItem, SWT.NONE);
		tblclmnRate.setWidth(106);
		tblclmnRate.setText("Rate(Rs)");
		
		TableColumn tblclmnActualRate = new TableColumn(tblItem, SWT.NONE);
		tblclmnActualRate.setWidth(110);
		tblclmnActualRate.setText("Actual Rate(Rs)");
		
		TableColumn tblclmnSellingPrice = new TableColumn(tblItem, SWT.NONE);
		tblclmnSellingPrice.setWidth(100);
		tblclmnSellingPrice.setText("Selling Price");
		
		TableColumn tblclmnTotal = new TableColumn(tblItem, SWT.NONE);
		tblclmnTotal.setWidth(134);
		tblclmnTotal.setText("Total(Rs)");
		
		TableColumn tblclmnEquivalence = new TableColumn(tblItem, SWT.NONE);
		tblclmnEquivalence.setWidth(0);
		tblclmnEquivalence.setText("Total(Rs)");
		tblclmnEquivalence.setResizable(false);
		
		TableColumn tblclmnUpdtcnt = new TableColumn(tblItem, SWT.NONE);
		tblclmnUpdtcnt.setWidth(0);
		tblclmnUpdtcnt.setText("Total(Rs)");
		tblclmnUpdtcnt.setResizable(false);
		
		Composite composite = new Composite(comMainContainer, SWT.NONE);
		fd_tblItem.bottom = new FormAttachment(composite, -6);
		FormData fd_composite = new FormData();
		fd_composite.top = new FormAttachment(0, 486);
		fd_composite.bottom = new FormAttachment(100);
		fd_composite.left = new FormAttachment(0, 10);
		fd_composite.right = new FormAttachment(100, -10);
		composite.setLayoutData(fd_composite);
		composite.setLayout(null);
		
		Label lblDueAmount = new Label(composite, SWT.RIGHT);
		lblDueAmount.setBounds(739, 65, 82, 15);
		lblDueAmount.setText("Due Amount");
		lblDueAmount.setFont(UtilMethods.getFontBold());
		
		txtDueAmount = new Text(composite, SWT.BORDER | SWT.READ_ONLY);
		txtDueAmount.setBounds(827, 62, 136, 20);
		txtDueAmount.setForeground(UtilMethods.getFontForegroundColorRed());
		
		Button btnSave = new Button(composite, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tblItem.getItemCount() <= 0)
				{
					msg.setMandatoryMessages("No items to save");
				}
				 if(bcmbSupplier.getSelectedBoundValue().toString().equals("0"))
				{
					msg.setMandatoryMessages("Please select supplier");
				}
				if(txtBillNo.getText().equals(""))
				{
					msg.setMandatoryMessages("Please enter bill no");
				}
				else
				{
					if(action.equals("add"))
					{
						if(UtilFxn.isNotUnique("select * from inven_item_purchase where bill_no='"+txtBillNo.getText()+"'"))
						{
							msg.setMandatoryMessages("Bill number you entered is already in use");
						}
					}
					else
					{
						if(UtilFxn.isNotUnique("select * from inven_item_purchase where bill_no='"+txtBillNo.getText()+"' and pur_id !='"+purchasedId+"'"))
						{
							msg.setMandatoryMessages("Bill number you entered is already in use");
						}
					}
				}
				if(bcmbPurDate.getValue()==null)
				{
					msg.setMandatoryMessages("Please select purchased date");
				}
				 if(bcmbBrand.getSelectedBoundValue().equals("0"))
				{
					msg.setMandatoryMessages("Please select item purchase type");
				}
				else if(bcmbBrand.getSelectedBoundValue().equals("1"))
				{
					if(bcmbLocalBrandType.getSelectedBoundValue().toString().equals("0"))
					{
						msg.setMandatoryMessages("Please select item local purchase type");
					}
				}
				if(checkTotalSourceAmount() > Double.parseDouble(txtItemTotal.getText().equals("")?"0":txtItemTotal.getText().replaceAll(",", "")))
				{
					msg.setMandatoryMessages("Your purchase cost exceeds the source paid cost");
				}
				 
				if(msg.returnMessages())
				{
					setSaveValues();
					
					
					try {
						Connection cnn=DbConnection.getConnection();
						cnn.setAutoCommit(false);
						if(control.saveAction(stockPurchasedModel, itemsList, sourceList,action,expenseList,daybook,dayBookDetailList))
						{
							
							clearAll();
							msg.showIndividualMessage("Items saved successfully.", 0);
							cnn.commit();
						}
						else
						{
							msg.showIndividualMessage("Some error occured while saving.Please try again.", 0);
							cnn.rollback();
							cnn.setAutoCommit(true);
						}
						
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						Connection cnn=DbConnection.getConnection();
						try {
							cnn.rollback();
							cnn.setAutoCommit(true);
						} catch (SQLException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
						
					}
				}
			}
		});
		btnSave.setBounds(600, 38, 72, 39);
		btnSave.setText("Save");
		
		Label lblTotal_1 = new Label(composite, SWT.RIGHT);
		lblTotal_1.setAlignment(SWT.RIGHT);
		lblTotal_1.setBounds(739, 6, 82, 15);
		lblTotal_1.setText("Total");
		lblTotal_1.setFont(UtilMethods.getFontBold());
		
		txtItemTotal = new Text(composite, SWT.BORDER);
		txtItemTotal.setEditable(false);
		txtItemTotal.setBounds(827, 4, 136, 20);
		txtItemTotal.setForeground(UtilMethods.getFontForegroundColorRed());
		
		Label lblPaidAmount = new Label(composite, SWT.RIGHT);
		lblPaidAmount.setBounds(739, 35, 82, 15);
		lblPaidAmount.setText("Paid Amount");
		lblPaidAmount.setFont(UtilMethods.getFontBold());
		
		txtPaidAmount = new Text(composite, SWT.BORDER);
		txtPaidAmount.setEditable(false);
		txtPaidAmount.setBounds(827, 33, 136, 20);
		txtPaidAmount.setForeground(UtilMethods.getFontForegroundColorRed());
		
		Group grpPaymentDetails = new Group(composite, SWT.NONE);
		grpPaymentDetails.setBounds(0, 0, 551, 118);
		grpPaymentDetails.setText("Payment Details");
		
		btnCash = new Button(grpPaymentDetails, SWT.RADIO);
		btnCash.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				enableDisableSource(false, true);
				clearSource();
			}
		});
		btnCash.setBounds(22, 20, 55, 16);
		btnCash.setText("Cash");
		
		btnBank = new Button(grpPaymentDetails, SWT.RADIO);
		btnBank.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				enableDisableSource(true, false);
				clearSource();
			}
		});
		btnBank.setBounds(22, 52, 55, 16);
		btnBank.setText("Bank");
		
		bcmbCash = new SwtBndCombo(grpPaymentDetails, SWT.READ_ONLY);
		bcmbCash.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(bcmbCash.getSelectedBoundValue().toString().equals("0"))
					clearSource();
				
			}
		});
		bcmbCash.setEnabled(false);
		bcmbCash.setBounds(79, 18, 102, 23);
		bcmbCash.fillData(CmbQry.getAllCash(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		
		bcmbBank = new SwtBndCombo(grpPaymentDetails, SWT.READ_ONLY);
		bcmbBank.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(bcmbBank.getSelectedBoundValue().toString().equals("0"))
					clearSource();
				
			}
		});
		bcmbBank.setEnabled(false);
		bcmbBank.setBounds(79, 50, 102, 23);
		
			bcmbBank.fillData(CmbQry.getAllBank(), CmbQry.getLabel(),
					CmbQry.getId(), DbConnection.getConnection(), true);
			
			txtCash = new Text(grpPaymentDetails, SWT.BORDER);
			txtCash.setEnabled(false);
			txtCash.setBounds(187, 18, 76, 21);
			txtCash.addListener(SWT.Verify, new DecimalTextPositiveOnly(
					txtCash, 20));
			
			txtBank = new Text(grpPaymentDetails, SWT.BORDER);
			txtBank.setEnabled(false);
			txtBank.setBounds(187, 50, 76, 21);
			txtBank.addListener(SWT.Verify, new DecimalTextPositiveOnly(
					txtBank, 20));
			
			Button btnPlus = new Button(grpPaymentDetails, SWT.NONE);
			btnPlus.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					beforeSourceAdd();
					if(msg.returnMessages())
					{
						addSourceItem();
						clearSource();
						calculateTotalAmount();
					}
				}
			});
			btnPlus.setFont(SWTResourceManager.getFont("Segoe UI", 13, SWT.BOLD));
			btnPlus.setBounds(275, 18, 32, 25);
			btnPlus.setText("+");
			
			tblSourceAmount = new Table(grpPaymentDetails, SWT.BORDER | SWT.FULL_SELECTION);
			tblSourceAmount.setBounds(313, 13, 232, 98);
			tblSourceAmount.setHeaderVisible(true);
			tblSourceAmount.setLinesVisible(true);
			
			TableColumn tblclmnAccCode = new TableColumn(tblSourceAmount, SWT.NONE);
			tblclmnAccCode.setWidth(0);
			tblclmnAccCode.setText("acccode");
			tblclmnAccCode.setResizable(false);
			
			TableColumn tblclmnSn_1 = new TableColumn(tblSourceAmount, SWT.NONE);
			tblclmnSn_1.setWidth(37);
			tblclmnSn_1.setText("S.N.");
			
			TableColumn tblclmnAmount = new TableColumn(tblSourceAmount, SWT.NONE);
			tblclmnAmount.setWidth(83);
			tblclmnAmount.setText("Amount(Rs)");
			
			TableColumn tblclmnSource = new TableColumn(tblSourceAmount, SWT.NONE);
			tblclmnSource.setWidth(107);
			tblclmnSource.setText("Source");
			
			TableColumn tblclmnTrxnId = new TableColumn(tblSourceAmount, SWT.NONE);
			tblclmnTrxnId.setWidth(0);
			tblclmnTrxnId.setText("TrxnId");
			tblclmnTrxnId.setResizable(false);
			
			Button btnX = new Button(grpPaymentDetails, SWT.NONE);
			btnX.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if(tblSourceAmount.getSelectionIndex() < 0)
					{
						msg.showIndividualMessage("Please select the row to delete", 0);
					}
					else
					{
						if(msg.showIndividualMessage("Make sure before you delete the item", 2))
						{
							int selection=tblSourceAmount.getSelectionIndex();
							tblSourceAmount.remove(tblSourceAmount.getSelectionIndex());
							
							if (tblSourceAmount.getItemCount() > 0) {
								
								TableItem[] Item = tblSourceAmount.getItems();
								for (int i = selection+1; i < tblSourceAmount.getItemCount(); i++) {
									Item[i].setText(1, String.valueOf(tblSourceAmount.getItemCount()));
									
								}
								
							}
							calculateTotalAmount();
						}
						
					}
				}
			});
			btnX.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
			btnX.setText("x");
			btnX.setBounds(275, 48, 32, 25);

		
		

		if(purchasedId > 0)
		{
			loadEditData(purchasedId);
		}
			
		return comParent;
	}
	private void clearItemDetails()
	{
		//bcmbItem.setSelectedBoundValue("0");
		txtWeight.setText("");
		txtUnit.setText("");
		txtUnitQuantity.setText("");
		txtRate.setText("");
		txtActualRate.setText("");
		txtTotal.setText("");
		txtSellingPrice.setText("");
		itemEquivalence=0;
		unit_id=0;
		itemAction="add";
	}
	private void loadItemDetails(int itemId)
	{
		StockItemController itemController=new StockItemController();
		ResultSet rs=itemController.fetchItemDetails(itemId);
		try {
		if(rs!=null && rs.next())
				{
					
						
				UnitMapController unitControl=new UnitMapController();
				ResultSet unitRs=unitControl.getUnitDetailsById(rs.getInt("item_pur_unit_id"));
				unit_id=rs.getInt("item_pur_unit_id");
				txtUnit.setText(unitRs.getString("unit_name"));
				txtWeight.setText(String.valueOf(rs.getDouble("unit_weight") <=0 ?"":rs.getBigDecimal("unit_weight")));
				itemEquivalence=unitRs.getDouble("conversion_ration");
				txtSellingPrice.setText(String.valueOf(rs.getDouble("item_selling_price") <=0 ?"0":rs.getBigDecimal("item_selling_price")));
			}
		else
		{
			clearItemDetails();
		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void calculateTotalAmount()
	{
		totalAmount=0;
		//txtTotal.setText(UtilFxn.getFormatedAmount(totalAmount));
		
		double totItemPrice=0;
		if (tblItem.getItemCount() > 0) {
			TableItem[] Item = tblItem.getItems();
			for (int i = 0; i < tblItem.getItemCount(); i++) {
				totItemPrice+=Double.parseDouble(Item[i].getText(10).replaceAll(",", ""));
			}
			
		}
		txtItemTotal.setText(UtilFxn.getFormatedAmount(totItemPrice));
		totalAmount=totItemPrice;		
		txtPaidAmount.setText(UtilFxn.getFormatedAmount(checkTotalSourceAmount()));
		txtDueAmount.setText(UtilFxn.getFormatedAmount(totalAmount-checkTotalSourceAmount()));
	}
	
	private void calculateItemTotal()
	{
		double rate=txtRate.getText().equals("")?0:Double.parseDouble(txtRate.getText().replaceAll(",", ""));
		double unitQuantity=txtUnitQuantity.getText().equals("")?0:Double.parseDouble(txtUnitQuantity.getText().replaceAll(",", ""));
		double totWeight=txtTotalWeight.getText().equals("")?0:Double.parseDouble(txtTotalWeight.getText().replaceAll(",", ""));
		double itemWeight=txtWeight.getText().equals("")?0:Double.parseDouble(txtWeight.getText().replaceAll(",", ""));
		
		txtTotal.setText(UtilFxn.getFormatedAmount((rate*unitQuantity)));
		
		if(totWeight > 0 && rate >0 && itemWeight > 0 && unitQuantity > 0)
		{
			double itemTotalWeight=itemEquivalence*itemWeight;
			double addedAmt=(calculateTotalExpenseAmount()/totWeight)*itemTotalWeight;
		
			txtActualRate.setText(UtilFxn.getFormatedAmount((rate+addedAmt)));
		}
		else
		{
			txtActualRate.setText(txtRate.getText());
		}
	}
	private void addItemInTable()
	{
		if(itemAction.equals("edit"))
		{
			TableItem[] Item = tblItem.getItems();
			for (int i = 0; i < tblItem.getItemCount(); i++) {
				String itemId=Item[i].getText(0);
				if(itemId.equals(bcmbItem.getSelectedBoundValue().toString()))
				{
					Item[i].setText(0, bcmbItem.getSelectedBoundValue().toString());
					Item[i].setText(1,String.valueOf(unit_id));
					Item[i].setText(2,String.valueOf(tblItem.getItemCount()));
					Item[i].setText(3,bcmbItem.getText());
					Item[i].setText(4,String.valueOf(txtWeight.getText()));
					Item[i].setText(5,txtUnit.getText());
					Item[i].setText(6,txtUnitQuantity.getText());
					Item[i].setText(7,txtRate.getText());
					Item[i].setText(8,txtActualRate.getText());
					Item[i].setText(9,txtSellingPrice.getText());
					Item[i].setText(10,txtTotal.getText());
					Item[i].setText(11,String.valueOf(itemEquivalence));
					Item[i].setText(12,String.valueOf(-1));
				}
			}
		}
		else
		{
			TableItem item=new TableItem(tblItem, SWT.NONE);
			item.setText(0, bcmbItem.getSelectedBoundValue().toString());
			item.setText(1,String.valueOf(unit_id));
			item.setText(2,String.valueOf(tblItem.getItemCount()));
			item.setText(3,bcmbItem.getText());
			item.setText(4,String.valueOf(txtWeight.getText()));
			item.setText(5,txtUnit.getText());
			item.setText(6,txtUnitQuantity.getText());
			item.setText(7,txtRate.getText());
			item.setText(8,txtActualRate.getText());
			item.setText(9,txtSellingPrice.getText());
			item.setText(10,txtTotal.getText());
			item.setText(11,String.valueOf(itemEquivalence));
			item.setText(12,String.valueOf(-1));
		}
		
		
	}
	
	private void loadItemEditValues()
	{
		bcmbItem.setSelectedBoundValue(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(0));
		itemEquivalence=Double.parseDouble(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(11));
		unit_id=Integer.valueOf(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(1));
		txtWeight.setText(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(4));
		txtUnit.setText(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(5));
		txtUnitQuantity.setText(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(6));
		txtRate.setText(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(7));
		txtActualRate.setText(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(8));
		txtSellingPrice.setText(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(9));
		txtTotal.setText(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(10));
	}
	
	private void checkBeforeItemAdd()
	{
		if(bcmbItem.getSelectedBoundValue().toString().equals("0"))
		{
			msg.setMandatoryMessages("Please select item.");
		}
		if(txtUnitQuantity.getText().equals(""))
		{
			msg.setMandatoryMessages("Please enter unit quantity purchased.");
		}
		else
		{
			if(Double.valueOf(txtUnitQuantity.getText().replaceAll(",", "")) <= 0)
			{
				msg.setMandatoryMessages("Please enter unit quantity purchased greater than 0.");
			}
		}
		if(txtRate.getText().equals(""))
		{
			msg.setMandatoryMessages("Please enter unit rate.");
		}
		else
		{
			if(Double.valueOf(txtRate.getText().replaceAll(",", "")) <= 0)
			{
				msg.setMandatoryMessages("Please enter unit rate greater than 0.");
			}
		}
		if(txtSellingPrice.getText().equals(""))
		{
			msg.setMandatoryMessages("Please enter unit selling price.");
		}
		else
		{
			if(Double.valueOf(txtSellingPrice.getText().replaceAll(",", "")) <= 0)
			{
				msg.setMandatoryMessages("Please enter unit selling price greater than 0.");
			}
		}
		
		//loop in the table to see if selected item is already added
		if (tblItem.getItemCount() > 0 && itemAction.equals("add")) {
			TableItem[] Item = tblItem.getItems();
			for (int i = 0; i < tblItem.getItemCount(); i++) {
				String itemId=Item[i].getText(0);
				if(itemId.equals(bcmbItem.getSelectedBoundValue().toString()))
				{
					msg.setMandatoryMessages("Selected item already added in the table.");
					break;
				}
			}
			
		}
	}
	
	private void enableDisableSource(boolean bank,boolean cash)
	{
		
		bcmbBank.setEnabled(bank);
		txtBank.setEnabled(bank);
		bcmbCash.setEnabled(cash);
		txtCash.setEnabled(cash);
		
	}
	
	private void clearSource()
	{
		
		
			bcmbBank.setSelectedBoundValue("0");
			txtBank.setText("");
			bcmbCash.setSelectedBoundValue("0");
			txtCash.setText("");
			
		
	}
	private void clearExpense()
	{
		txtExpenseAmt.setText("");
		bcmbExpenses.setSelectedBoundValue("0");
	}
	
	
	
	private void beforeSourceAdd()
	{
		if(btnCash.getSelection())
		{
			if(bcmbCash.getSelectedBoundValue().toString().equals("0"))
			{
				msg.setMandatoryMessages("Please select cash source");
			}
			else if(txtCash.getText().equals(""))
			{
				msg.setMandatoryMessages("Please enter cash amount");
			}
			else if(Integer.parseInt(txtCash.getText()) <= 0)
			{
				msg.setMandatoryMessages("Please enter cash amount greater than 0");
			}
			else if((checkTotalSourceAmount()+Double.parseDouble(txtCash.getText().replaceAll(",", ""))) > Double.parseDouble(txtItemTotal.getText().equals("")?"0":txtItemTotal.getText().replaceAll(",", "")))
			{
				msg.setMandatoryMessages("Your purchase cost exceeds the source paid cost");
			}
		}
		
		else if(btnBank.getSelection())
		{
			if(bcmbBank.getSelectedBoundValue().toString().equals("0"))
			{
				msg.setMandatoryMessages("Please select bank source");
			}
			else if(txtBank.getText().equals(""))
			{
				msg.setMandatoryMessages("Please enter bank amount");
			}
			else if(Integer.parseInt(txtBank.getText()) <= 0)
			{
				msg.setMandatoryMessages("Please enter bank amount greater than 0");
			}
			else if((checkTotalSourceAmount()+Double.parseDouble(txtBank.getText().replaceAll(",", ""))) > Double.parseDouble(txtItemTotal.getText().equals("")?"0":txtItemTotal.getText().replaceAll(",", "")))
			{
				msg.setMandatoryMessages("Your purchase cost exceeds the source paid cost");
			}
		}
		else
		{
			msg.setMandatoryMessages("Please select source");
		}
		
		
		
		
	}
	
	
	
	private double checkTotalSourceAmount()
	{
		double amount=0;
		if (tblSourceAmount.getItemCount() > 0) {
			TableItem[] Item = tblSourceAmount.getItems();
			for (int i = 0; i < tblSourceAmount.getItemCount(); i++) {
				String code=Item[i].getText(0);
				
					amount+=Double.parseDouble(Item[i].getText(2).replaceAll(",", ""));
				
			}
			
		}
		return amount;
	}
	
	private double calculateTotalExpenseAmount()
	{
		double amount=0;
		if (tblExpense.getItemCount() > 0) {
			TableItem[] Item = tblExpense.getItems();
			for (int i = 0; i < tblExpense.getItemCount(); i++) {
				String code=Item[i].getText(0);
				
					amount+=Double.parseDouble(Item[i].getText(3).replaceAll(",", ""));
				
			}
			
		}
		txtTotExpense.setText(UtilFxn.getFormatedAmount(amount));
		return amount;
	}
	
	
	private void setSaveValues()
	{
		stockPurchasedModel=new StockPurchasedModel();
		itemsList.clear();
		sourceList.clear();
		dayBookDetailList.clear();
		//set itempurchased
		if(action.equals("edit"))
		{
			stockPurchasedModel.setPur_id(purchasedId);
			stockPurchasedModel.setJv_no(jvNo);
		}
		
		stockPurchasedModel.setSupplier_id(Integer.valueOf(bcmbSupplier.getSelectedBoundValue().toString()));
		stockPurchasedModel.setBill_no(txtBillNo.getText());
		stockPurchasedModel.setTrxn_dt(bcmbPurDate.getText());
		stockPurchasedModel.setItem_purchased_type(Integer.valueOf(bcmbBrand.getSelectedBoundValue().toString()));
		stockPurchasedModel.setItem_local_purchased_type(Integer.valueOf(bcmbLocalBrandType.getSelectedBoundValue().toString()));
	
		//stockPurchasedModel.setLu_india_expense(txtLUIndia.getText().equals("")?"0":txtLUIndia.getText().replaceAll(",", ""));
		//stockPurchasedModel.setLu_border_expense(txtLUBorder.getText().equals("")?"0":txtLUBorder.getText().replaceAll(",", ""));
		//stockPurchasedModel.setLu_tax_clearence_expense(txtTaxClearence.getText().equals("")?"0":txtTaxClearence.getText().replaceAll(",", ""));
		//stockPurchasedModel.setLu_ktm_expense(txtLUKtm.getText().equals("")?"0":txtLUKtm.getText().replaceAll(",", ""));
		//stockPurchasedModel.setLu_shop_expense(txtLUShop.getText().equals("")?"0":txtLUShop.getText().replaceAll(",", ""));
		stockPurchasedModel.setOther_expense(txtTotExpense.getText().equals("")?"0":txtTotExpense.getText().replaceAll(",", ""));
		stockPurchasedModel.setTotal_weight(txtTotalWeight.getText().equals("")?"0":txtTotalWeight.getText().replaceAll(",", ""));
		stockPurchasedModel.setTotal_amount(txtItemTotal.getText().equals("")?"0":txtItemTotal.getText().replaceAll(",", ""));
		stockPurchasedModel.setPaid_amt(UtilFxn.getDoubleExpFormated(checkTotalSourceAmount()));
		stockPurchasedModel.setRemarks(txtRemarks.getText().replaceAll("'", "''"));
		stockPurchasedModel.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
		
		//daybook
		daybook.setDept_id(1);
		daybook.setExpense(Double.parseDouble(stockPurchasedModel.getPaid_amt()));
		daybook.setIncome(0);
		daybook.setRemarks(stockPurchasedModel.getRemarks());
		daybook.setTrxn_date_ad(stockPurchasedModel.getTrxn_dt());
		daybook.setTrxn_date_bs("0000-00-00");
		daybook.setTrxn_type(312);//312 for stock
		daybook.setCreated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		daybook.setCreated_dt(jcalFxn.currentDate());
		daybook.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
		daybook.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		daybook.setUpdt_cnt(0);
		daybook.setUpdt_dt(jcalFxn.currentDate());
		
		
		//items details
		if (tblItem.getItemCount() > 0) {
			TableItem[] Item = tblItem.getItems();
			for (int i = 0; i < tblItem.getItemCount(); i++) {
				StockPurchasedDetailsModel model=new StockPurchasedDetailsModel();
				model.setItem_id(Integer.valueOf(Item[i].getText(0)));
				model.setUnit_id(Integer.valueOf(Item[i].getText(1)));
				model.setUnit_weight(Item[i].getText(4).replaceAll(",", "")==""?"0":Item[i].getText(4).replaceAll(",", ""));
				model.setQuantity(Item[i].getText(6).replaceAll(",", ""));
				model.setRate(Item[i].getText(7).replaceAll(",", ""));
				model.setActual_rate(Item[i].getText(8).replaceAll(",", ""));
				model.setSelling_price(Item[i].getText(9).replaceAll(",", ""));
				model.setTotal(Item[i].getText(10).replaceAll(",", ""));
				model.setUnit_quantity(UtilFxn.getDoubleExpFormated(Double.valueOf(Item[i].getText(11).replaceAll(",", "")) * Double.valueOf(Item[i].getText(6).replaceAll(",", ""))));
				model.setCrtd_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
				model.setCrtd_dt(jcalFxn.currentDate());
				model.setUpdt_dt(jcalFxn.currentDate());
				model.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
				model.setUpdt_cnt(0);
				if(action.equals("edit"))
				{
					model.setUpdt_cnt(Integer.valueOf(Item[i].getText(12))+1);
				}
				itemsList.add(model);
					
				
			}
			
		}
		
		//payment details
		if (tblSourceAmount.getItemCount() > 0) {
			TableItem[] Item = tblSourceAmount.getItems();
			for (int i = 0; i < tblSourceAmount.getItemCount(); i++) {
				StockPurchasedTrxnModel model=new StockPurchasedTrxnModel();
				model.setAcc_code(Item[i].getText(0));
				model.setAmount(Item[i].getText(2).replaceAll(",", ""));
				model.setTrxn_source(Item[i].getText(3));
				if(action.equals("edit"))
				{
					model.setPur_trxn_id(Integer.valueOf(Item[i].getText(4)));
				}
				
				sourceList.add(model);
				
				
				//for daybook details
				DayBookDetailsModel model2=new DayBookDetailsModel();
				model2.setTrxn_with_id(1);
				model2.setTrxn_with_type_id(Integer.parseInt(model.getAcc_code()));
				model2.setIncome(0);
				model2.setExpense(Double.parseDouble(model.getAmount()));
				model2.setCreated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
				model2.setCreated_dt(jcalFxn.currentDate());
				//model2.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
				model2.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
				model2.setUpdt_cnt(0);
				model2.setUpdt_dt(jcalFxn.currentDate());
				
				dayBookDetailList.add(model2);
			}
		}
		
		//expense details
		expenseList.clear();
		if (tblExpense.getItemCount() > 0) {
			TableItem[] Item = tblExpense.getItems();
			for (int i = 0; i < tblExpense.getItemCount(); i++) {
				StockPurchseExpenseTrxnModel model=new StockPurchseExpenseTrxnModel();
				model.setExp_id(Integer.valueOf(Item[i].getText(0)));
				model.setAmount(Item[i].getText(3).replaceAll(",", ""));
				
//				if(action.equals("edit"))
//				{
//					model.setPur_trxn_id(Integer.valueOf(Item[i].getText(4)));311
//				}
				
				expenseList.add(model);
				
				//add daybook
				DayBookDetailsModel model2=new DayBookDetailsModel();
				model2.setTrxn_with_type_id(311);
				model2.setIncome(0);
				model2.setExpense(Double.parseDouble(model.getAmount()));
				model2.setCreated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
				model2.setCreated_dt(jcalFxn.currentDate());
				//model2.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
				model2.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
				model2.setUpdt_cnt(0);
				model2.setUpdt_dt(jcalFxn.currentDate());
				
				dayBookDetailList.add(model2);
			}
		}
		//set journal details
		
		
		
		
		
		
	}
	
	
	
	
	private void addSourceItem()
	{
		TableItem item = new TableItem(tblSourceAmount, SWT.NONE);
		if(btnCash.getSelection())
		{
			item.setText(0, bcmbCash.getSelectedBoundValue().toString());
			item.setText(2, txtCash.getText());
			item.setText(3,bcmbCash.getText());
			item.setText(4,"0");
		}
		if(btnBank.getSelection())
		{
			item.setText(0, bcmbBank.getSelectedBoundValue().toString());
			item.setText(2, txtBank.getText());
			item.setText(3,bcmbBank.getText());
			item.setText(4,"0");
		}
		item.setText(1, String.valueOf(tblSourceAmount.getItemCount()));
	}
	
	private void addExpenseItem()
	{
		TableItem item = new TableItem(tblExpense, SWT.NONE);
		item.setText(0, bcmbExpenses.getSelectedBoundValue().toString());
		item.setText(1, String.valueOf(tblExpense.getItemCount()));
		item.setText(2, bcmbExpenses.getText());
		item.setText(3, txtExpenseAmt.getText());
	}
	
	public void reloadSupplier()
	{
		bcmbSupplier.fillData(CmbQry.getAllSuppliers(2), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
	}
	
	public void reloadItem()
	{
		clearItemDetails();
		bcmbItem.fillData(CmbQry.getStockItems(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
	}
	
	
	//load edit datas
	private void loadEditData(int purid)
	{
		this.action="edit";
		
		ResultSet purchased=control.getPurchasedById(purid);
		ResultSet purchasedDetails=control.getPurchasedDetailsById(purid);
		ResultSet purchasedTrxn=control.getPurchasedSourceDetailsById(purid);
		ResultSet expTrxn=control.getPurchasedExpDetailsById(purid);
		
		//load purchased
		try {
			if(purchased.next())
			{
				bcmbSupplier.setSelectedBoundValue(purchased.getInt("supplier_id"));
				txtBillNo.setText(purchased.getString("bill_no"));
				bcmbPurDate.setValue(purchased.getDate("trxn_dt"));
				bcmbBrand.setSelectedBoundValue(purchased.getInt("item_purchased_type"));
				
				if(bcmbBrand.getSelectedBoundValue().toString().equals("2"))
				{
					bcmbLocalBrandType.setEnabled(false);
				}
				else
				{
					bcmbLocalBrandType.setEnabled(true);
				}
				
				bcmbLocalBrandType.setSelectedBoundValue(purchased.getInt("item_local_purchased_type"));
				txtRemarks.setText(purchased.getString("remarks"));
				//txtLUIndia.setText(UtilFxn.getFormatedAmount(purchased.getDouble("lu_india_expense")));
				//txtLUBorder.setText(UtilFxn.getFormatedAmount(purchased.getDouble("lu_border_expense")));
				//txtTaxClearence.setText(UtilFxn.getFormatedAmount(purchased.getDouble("lu_tax_clearence_expense")));
				//txtLUKtm.setText(UtilFxn.getFormatedAmount(purchased.getDouble("lu_ktm_expense")));
				//txtLUShop.setText(UtilFxn.getFormatedAmount(purchased.getDouble("lu_shop_expense")));
				//txtOtherExpense.setText(UtilFxn.getFormatedAmount(purchased.getDouble("other_expense")));
				txtTotalWeight.setText(UtilFxn.getFormatedAmount(purchased.getDouble("total_weight")));
				jvNo=purchased.getInt("jv_no");
				expJvNo=purchased.getInt("purchased_payment_id");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//load purchased details
		try {
			if(UtilFxn.getRowCount(purchasedDetails) > 0)
			{
				int i=1;
				while(purchasedDetails.next())
				{
					TableItem item=new TableItem(tblItem, SWT.NONE);
					item.setText(0,String.valueOf(purchasedDetails.getInt("item_id")));
					item.setText(1,String.valueOf(purchasedDetails.getInt("unit_id")));
					item.setText(2,String.valueOf(i));
					item.setText(3,purchasedDetails.getString("item_name"));
					item.setText(4,UtilFxn.getFormatedAmount(purchasedDetails.getDouble("unit_weight")));
					item.setText(5,purchasedDetails.getString("unit_name"));
					item.setText(6,UtilFxn.getFormatedAmount(purchasedDetails.getDouble("quantity")));
					item.setText(7,UtilFxn.getFormatedAmount(purchasedDetails.getDouble("rate")));
					item.setText(8,UtilFxn.getFormatedAmount(purchasedDetails.getDouble("actual_rate")));
					item.setText(9,UtilFxn.getFormatedAmount(purchasedDetails.getDouble("item_selling_price")));
					item.setText(10,UtilFxn.getFormatedAmount(purchasedDetails.getDouble("total")));
					item.setText(11,String.valueOf(purchasedDetails.getDouble("conversion_ration")));
					item.setText(12,String.valueOf(purchasedDetails.getInt("updt_cnt")));
					i++;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//load payment details
		try {
			if(UtilFxn.getRowCount(purchasedTrxn) > 0)
			{
				int i=1;
				while(purchasedTrxn.next())
				{
					TableItem item = new TableItem(tblSourceAmount, SWT.NONE);
					
						item.setText(0, purchasedTrxn.getString("acc_code"));
						item.setText(1, String.valueOf(i));
						item.setText(2, UtilFxn.getFormatedAmount(purchasedTrxn.getDouble("amount")));
						item.setText(3,purchasedTrxn.getString("cv_lbl"));					
					item.setText(4, String.valueOf(purchasedTrxn.getInt("pur_trxn_id")));
					i++;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			if(UtilFxn.getRowCount(expTrxn) > 0)
			{
				int i=1;
				while(expTrxn.next())
				{
					TableItem item = new TableItem(tblExpense, SWT.NONE);
					
						item.setText(0, String.valueOf(expTrxn.getInt("exp_id")));
						item.setText(1, String.valueOf(i));
						item.setText(2, expTrxn.getString("name"));
						item.setText(3,UtilFxn.getFormatedAmount(expTrxn.getDouble("amount")));					
					
					i++;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//cal total
		calculateTotalExpenseAmount();
		calculateTotalAmount();
	}
	
	private void clearAll()
	{
		
		bcmbSupplier.setSelectedBoundValue("0");
		txtBillNo.setText("");
		bcmbPurDate.setValue(new Date());
		bcmbBrand.setSelectedBoundValue("0");
		bcmbLocalBrandType.setSelectedBoundValue("0");
		txtRemarks.setText("");
		//txtLUBorder.setText("");
		//txtLUIndia.setText("");
		//txtLUKtm.setText("");
		//txtLUShop.setText("");
		//txtOtherExpense.setText("");
		//txtTaxClearence.setText("");
		txtTotalWeight.setText("");
		tblItem.removeAll();
		tblSourceAmount.removeAll();
		tblExpense.removeAll();
		txtTotal.setText("");
		//txtTotalExpense.setText("");
		txtItemTotal.setText("");
		txtDueAmount.setText("");
		txtPaidAmount.setText("");
		txtTotExpense.setText("");
		//txtTotalAmount.setText("");
		
	}
}

