package com.tia.inventory.view;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.controller.AccHeadController;
import com.tia.account.controller.LedgerController;
import com.tia.account.model.AccHeadModel;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.BeforeSaveMessages;
import com.tia.common.util.CmbQry;
import com.tia.common.util.DecimalTextPositiveOnly;
import com.tia.common.util.LoginCheck;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.inventory.controller.StockCategoryController;
import com.tia.inventory.controller.StockItemController;
import com.tia.inventory.model.StockCategoryModel;
import com.tia.plugins.DynaTree.controller.DynaTree;
import com.tia.plugins.DynaTree.model.TreeNode;
import com.tia.plugins.DynaTree.ui.TreeNodeLabelProvider;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

public class StockListView extends ViewPart {
	private Composite comMain;
	private Tree tree;
	private Tree rptTree;
	private DynaTree dynaTree;
	private String oldLogData = "";
	private Shell shell;
	private int groupId = 0;
	private int parentGroupId = 0;
	private int deep = 0;
	private String lineage = "";
	public static String action = "";

	protected TreeViewer treeViewer;
	protected TreeNode root;
	protected TreeNodeLabelProvider labelProvider;
	protected Action onlyBoardGamesAction, atLeatThreeItems;
	protected Action booksBoxesGamesAction, noArticleAction;
	protected Action addTreeNodeAction, removeTreeNodeAction, addLeafAction,
			removeLeafAction;
	protected ViewerFilter onlyBoardGamesFilter, atLeastThreeFilter;
	protected ViewerSorter booksBoxesGamesSorter, noArticleSorter;

	private static java.util.List<AccHeadModel> listHead;

	int maxSubAccId = 0;
	StockItemController control;
	
	BeforeSaveMessages msg = new BeforeSaveMessages();
	private Table table;
	private Text txtSearch;
	private static StockListView stockListView;
	private SwtBndCombo bcmbSearch;

	/**
	 * Default Constructor
	 */
	public StockListView() {
		control=new StockItemController();
		

		shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();

		setTitleImage(SWTResourceManager.getImage(StockListView.class,
				"/product16.png"));
		stockListView=this;
	}
	public static StockListView getInstance() {
		return stockListView;
	}

	@Override
	public void createPartControl(Composite parent) {
		// set the Gridlayout to the main form
		GridLayout gl_parent1 = new GridLayout(1, false);
		gl_parent1.marginHeight = 1;
		parent.setLayout(gl_parent1);
		Banner.getTitle("Stock Item Registration Management", parent, StockCategoryView.class);

		// Main composite holding other controls and set Grid Layout to it too
		comMain = new Composite(parent, SWT.NONE);
		GridData gd_comMain = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_comMain.heightHint = 613;
		gd_comMain.widthHint = 979;
		comMain.setBounds(5, 5, 970, 590);
		comMain.setLayout(null);
		comMain.setLayoutData(gd_comMain);
		
		table = new Table(comMain, SWT.BORDER | SWT.FULL_SELECTION);
		table.setBounds(5, 53, 974, 465);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		TableColumn tblclmnItemId = new TableColumn(table, SWT.NONE);
		tblclmnItemId.setWidth(0);
		tblclmnItemId.setText("id");
		tblclmnItemId.setResizable(false);
		
		TableColumn tblclmnSn = new TableColumn(table, SWT.NONE);
		tblclmnSn.setWidth(42);
		tblclmnSn.setText("S.N.");
		
		TableColumn tblclmnName = new TableColumn(table, SWT.NONE);
		tblclmnName.setWidth(115);
		tblclmnName.setText("Name");
		
		TableColumn tblclmnCode = new TableColumn(table, SWT.NONE);
		tblclmnCode.setText("Code");
		tblclmnCode.setWidth(115);
		
		TableColumn tblclmnSellingPrice = new TableColumn(table, SWT.NONE);
		tblclmnSellingPrice.setWidth(82);
		tblclmnSellingPrice.setText("Selling Price");
		
		TableColumn tblclmnRackno = new TableColumn(table, SWT.NONE);
		tblclmnRackno.setWidth(72);
		tblclmnRackno.setText("RackNo");
		
		TableColumn tblclmnBrand = new TableColumn(table, SWT.NONE);
		tblclmnBrand.setWidth(110);
		tblclmnBrand.setText("Brand");
		
		TableColumn tblclmnPurchased = new TableColumn(table, SWT.NONE);
		tblclmnPurchased.setWidth(100);
		tblclmnPurchased.setText("Purchase Unit");
		
		TableColumn tblclmnReamining = new TableColumn(table, SWT.NONE);
		tblclmnReamining.setWidth(119);
		tblclmnReamining.setText("Purchased Quantity");
		
		TableColumn tblclmnRemaining = new TableColumn(table, SWT.NONE);
		tblclmnRemaining.setWidth(100);
		tblclmnRemaining.setText("Selling Unit");
		
		TableColumn tblclmnRemainingQuantity = new TableColumn(table, SWT.NONE);
		tblclmnRemainingQuantity.setWidth(118);
		tblclmnRemainingQuantity.setText("Remaining Quantity");
		
		Group group = new Group(comMain, SWT.NONE);
		group.setBounds(579, 0, 400, 46);
		
		txtSearch = new Text(group, SWT.BORDER);
		txtSearch.setBounds(190, 17, 121, 21);
		
		Button btnSearch = new Button(group, SWT.NONE);
		btnSearch.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(txtSearch.getText().equals(""))
				{
					msg.showIndividualMessage("Please enter text to search", 0);
				}
				else
				{
					if(bcmbSearch.getSelectedBoundValue().equals("code"))
					{
						LoadData("and  item.item_code like '"+txtSearch.getText()+"%'");
					}
					else if(bcmbSearch.getSelectedBoundValue().equals("name"))
					{
						LoadData("and  item.item_name like '"+txtSearch.getText()+"%'");
					}
					else if(bcmbSearch.getSelectedBoundValue().equals("rackno"))
					{
						LoadData("and item.item_rack_no like '"+txtSearch.getText()+"%'");
					}
					else if(bcmbSearch.getSelectedBoundValue().equals("brand"))
					{
						LoadData("and cv.cv_lbl like '"+txtSearch.getText()+"%'");
					}
					else
					{
						LoadData("");
					}
				}
			}
		});
		btnSearch.setBounds(329, 15, 65, 25);
		btnSearch.setText("Search");
		
		bcmbSearch = new SwtBndCombo(group, SWT.READ_ONLY);
		bcmbSearch.setBounds(10, 17, 177, 23);
		bcmbSearch.add("Code", "code");
		bcmbSearch.add("Name", "name");
		bcmbSearch.add("Rack No", "rackno");
		bcmbSearch.add("Brand", "brand");
		bcmbSearch.select(0);
		
		Button btnNew = new Button(comMain, SWT.NONE);
		btnNew.setBounds(5, 22, 65, 25);
		btnNew.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				openView(0);
			}
		});
		btnNew.setText("New");
		
		Button btnEdit = new Button(comMain, SWT.NONE);
		btnEdit.setBounds(76, 22, 65, 25);
		btnEdit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(table.getSelectionIndex() < 0)
				{
					msg.showIndividualMessage("Please select the desired row to edit", 0);
				}
				else
				{
					String itemId = table.getSelection()[0].getText(0);
					openView(Integer.valueOf(itemId));
				}
				
			}
		});
		btnEdit.setText("Edit");
		
		Button btnDelete = new Button(comMain, SWT.NONE);
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(table.getSelectionIndex() < 0)
				{
					msg.showIndividualMessage("Please select the table row to delete", 0);
				}
				else
				{
					if(msg.showIndividualMessage("Make sure before you delete the selected record", 2))
					{
						int itemId = Integer.valueOf(table.getSelection()[0].getText(0));
						if(UtilFxn.isNotUnique("select * from inven_item_purchsed_details where item_id='"+itemId+"'"))
						{
							msg.showIndividualMessage("Selected record cannot be deleted.It is in use", 0);
						}
						else
						{
							if(control.deleteItem(itemId) )
							{
								LoadData("");
								msg.showIndividualMessage("Selected record has been successfully deleted", 0);
							}
							else
							{
								msg.showIndividualMessage("Some error occured while deleting record.Please try again", 0);
							}
						}
						
					}
					
					
				}
			}
		});
		btnDelete.setBounds(147, 22, 65, 25);
		btnDelete.setText("Delete");
		
		Button btnReferesh = new Button(comMain, SWT.NONE);
		btnReferesh.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				LoadData("");
			}
		});
		btnReferesh.setBounds(218, 22, 65, 25);
		btnReferesh.setText("Referesh");

		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		Label label = new Label(parent, SWT.NONE);
		label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false,
				1, 1));
		
		LoadData("");
	}// end of method createPartControl()

	
	public void openView(int itemId){
		IWorkbenchPage wbp = PlatformUI.getWorkbench()
		.getActiveWorkbenchWindow().getActivePage();

		try {
			//wbp.showView("com.tia.inven.stockEntry");
			String form="main";
			System.out.println("The itemid is :"+itemId);
			//StockEntryView.getInstance().loadData(itemId);
			StockEntryView hms = new StockEntryView(
					PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getShell(),itemId,form);
			hms.open();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		
	}
	
	
	private void LoadData(String cond) {
		table.removeAll();
		ResultSet rs;
		
		
			rs=control.getAllItems(cond);
		
		
		try {
			if(UtilFxn.getRowCount(rs) > 0)
			{
				int sno = 1;
				while(rs.next())
				{
					TableItem item=new TableItem(table,  SWT.NONE);
					item.setText(0,String.valueOf(rs.getInt("item_id")));
					item.setText(1,String.valueOf(sno));
					item.setText(2,rs.getString("item_name"));
					item.setText(3,rs.getString("item_code"));
					item.setText(4,String.valueOf(rs.getBigDecimal("item_selling_price")));
					item.setText(5,rs.getString("item_rack_no"));
					item.setText(6,rs.getString("cat_name"));
					item.setText(7,rs.getString("pur_unit_name"));
					item.setText(8,UtilFxn.getFormatedInt(rs.getDouble("pur_unit_total")));
					item.setText(9,rs.getString("sel_unit_name"));
					item.setText(10,UtilFxn.getFormatedInt(rs.getDouble("remaining_unit_total")));
					
					
					sno++;
				}
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void refereshTable()
	{
		LoadData("");
	}

	@Override
	public void setFocus() {

	}
}// end of class


