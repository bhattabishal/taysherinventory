package com.tia.inventory.view;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.nebula.widgets.datechooser.DateChooserCombo;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.controller.JournalEntryController;
import com.tia.account.model.JVDetailsModel;
import com.tia.account.model.JVSummary;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.BeforeSaveMessages;
import com.tia.common.util.CmbQry;
import com.tia.common.util.DecimalTextPositiveOnly;
import com.tia.common.util.IntegerTextPositiveOnly;
import com.tia.common.util.SearchBox;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;

import com.tia.inventory.controller.ServiceController;
import com.tia.inventory.controller.StockItemController;
import com.tia.inventory.controller.StockPurchasedController;
import com.tia.inventory.controller.StockSalesController;
import com.tia.inventory.model.DayBookDetailsModel;
import com.tia.inventory.model.DayBookModel;
import com.tia.inventory.model.SalesTrxnModel;
import com.tia.inventory.model.ServiceModel;
import com.tia.inventory.model.ServiceTrxnModel;
import com.tia.inventory.model.StockPurchasedDetailsModel;
import com.tia.inventory.model.StockPurchasedModel;
import com.tia.inventory.model.StockPurchasedTrxnModel;
import com.tia.inventory.model.StockSalesDetailsModel;
import com.tia.inventory.model.StockSalesModel;
import com.tia.inventory.model.StockSalesReportModel;
import com.tia.master.controller.UnitMapController;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import org.eclipse.swt.widgets.Combo;
import com.swtdesigner.SWTResourceManager;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;

public class StockSalesEntryView extends Dialog {
	private Composite comParent;
	private Composite comMainContainer;

	private JCalendarFunctions jcalFxn;

	private int salesId=0;
	private double equivalence=0;
	private String itemName="";
	private String serviceCode="";
	String itemAction="add";
	String action="add";
	String discount=returnVatDiscount("discount");
	
	StockItemController itemControl=new StockItemController();
	StockSalesModel stockSalesModel;
	StockSalesModel stockSalesOldModel=new StockSalesModel();
	StockSalesDetailsModel stockSalesDetailsModel=new StockSalesDetailsModel();
	SalesTrxnModel salesTrxnModel=new SalesTrxnModel();
	ServiceTrxnModel serviceTrxn=new ServiceTrxnModel();
	ArrayList<StockSalesDetailsModel> itemsList=new ArrayList<StockSalesDetailsModel>();
	ArrayList<ServiceTrxnModel> serviceList=new ArrayList<ServiceTrxnModel>();
	ArrayList<SalesTrxnModel> sourceList=new ArrayList<SalesTrxnModel>();
	ArrayList<StockSalesReportModel> reportList=new ArrayList<StockSalesReportModel>();
	StockSalesController control=new StockSalesController();
	ServiceController serviceControl=new ServiceController();
	DayBookModel daybook=new DayBookModel();
	ArrayList<DayBookDetailsModel> dayBookDetailList = new ArrayList<DayBookDetailsModel>();

	Object rtn;
	private Text txtBillNo;
	private Text txtAvailableQunatity;
	private SwtBndCombo bcmbUnit;
	private Text txtUnitQuantity;
	private Text txtRate;
	private Text txtTotal;
	private Table tblItem;
	private Text txtVat;
	private Text txtPaidAmount;
	private Text txtAmount;
	private Text txtBank;
	private Table tblSourceAmount;
	private SwtBndCombo bcmbCustomer;
	private SearchBox bcmbItem;
	private static StockSalesEntryView stockPurchaseEntry;
	private DateChooserCombo bcmbSelDate;
	private BeforeSaveMessages msg=new BeforeSaveMessages();
	private Text txtVoucherNo;
	private Button btnCash;
	private Button btnBank;
	private Text txtItemTotal;
	private Text txtItemDiscount;
	private Text txtGrandTotal;
	private SwtBndCombo bcmbServiceProvider;
	private Text txtSellingCost;
	private Text txtPercEarned;
	private SwtBndCombo bcmbServiceType;
	private Text txtPercPaid;
	private Text txtDueAmount;
	private Text txtRemarks;
	private Text txtRackNo;
	private Group grpSalesDetails;
	private Group grpServiceDetails;
	private Button btnSalesDetails;
	private Button btnServiceDetails;
	private SearchBox bcmbService;
	private Button btnServiceAdd;
	private Button btnServiceProviderAdd;
	private Text txtDiscountPercentage;
	private Text txtVatPercentage;
	private SwtBndCombo bcmbBillType;
	private Label lblServiceProvider;
	private Label lblearned;
	private Label lblpaid;
	private SwtBndCombo bcmbDeposite;

	public StockSalesEntryView(Shell parentShell, int purId) {
		super(parentShell);
		

		jcalFxn = new JCalendarFunctions();
		stockSalesModel=new StockSalesModel();
		stockSalesModel.setCrtd_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		stockSalesModel.setCrtd_dt(jcalFxn.currentDate());
		stockSalesModel.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		stockSalesModel.setUpdt_cnt(0);
		stockSalesModel.setUpdt_dt(jcalFxn.currentDate());
		stockSalesModel.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
		this.stockPurchaseEntry=this;
		this.salesId = purId;
		
	}

	/**
	 * @wbp.parser.constructor
	 */
	public StockSalesEntryView(Shell parentShell) {
		super(parentShell);
		

		jcalFxn = new JCalendarFunctions();
		
	}
	public static StockSalesEntryView getInstance() {
		return stockPurchaseEntry;
	}

	public Object open() {
		Shell parent = getParent();
		Shell shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setText("Stock Sales Entry");
		shell.setSize(1000, 700);
		// Your code goes here (widget creation, set result, etc).
		createDialogArea(shell);
		shell.open();
		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		return rtn;
	}

	// @Override
	@SuppressWarnings("deprecation")
	protected Control createDialogArea(Composite parent) {
		comParent = parent;// (Composite) super.createDialogArea(parent);
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("Stock Sales Entry", comParent,
				StockPurchasedEntryView.class);

		// Main container that holds all other controls/widgets of the view
		comMainContainer = new Composite(comParent, SWT.NONE);
		comMainContainer.setLayout(new FormLayout());
		GridData gd_comMainContainer = new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1);
		gd_comMainContainer.widthHint = 983;
		gd_comMainContainer.heightHint = 649;
		comMainContainer.setLayoutData(gd_comMainContainer);
		
		Group grpSalesInformation = new Group(comMainContainer, SWT.NONE);
		grpSalesInformation.setText("Sales Information");
		FormData fd_grpSalesInformation = new FormData();
		fd_grpSalesInformation.top = new FormAttachment(0);
		fd_grpSalesInformation.left = new FormAttachment(0, 10);
		fd_grpSalesInformation.right = new FormAttachment(0, 973);
		grpSalesInformation.setLayoutData(fd_grpSalesInformation);
		
		Label lblSupplier = new Label(grpSalesInformation, SWT.NONE);
		lblSupplier.setAlignment(SWT.RIGHT);
		lblSupplier.setBounds(10, 20, 66, 15);
		lblSupplier.setText("Customer: ");
		
		
		bcmbCustomer = new SwtBndCombo(grpSalesInformation, SWT.READ_ONLY);
		bcmbCustomer.setBounds(76, 18, 179, 23);
		bcmbCustomer.fillData(CmbQry.getAllSuppliers(1), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		
		Button btnSupplierAdd = new Button(grpSalesInformation, SWT.NONE);
		btnSupplierAdd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String form="stocksales";
				CustomerManagementView hms = new CustomerManagementView(
						PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getShell(),0,form);
				hms.open();
			}
		});
		btnSupplierAdd.setBounds(255, 17, 38, 25);
		btnSupplierAdd.setText("add");
		
		Label lblBillNo = new Label(grpSalesInformation, SWT.NONE);
		lblBillNo.setAlignment(SWT.RIGHT);
		lblBillNo.setBounds(334, 20, 86, 15);
		lblBillNo.setText("Bill No: ");
		//lblBillNo.setForeground(UtilMethods.getFontForegroundColorRed());
		
		txtBillNo = new Text(grpSalesInformation, SWT.BORDER);
		txtBillNo.setEditable(false);
		txtBillNo.setBounds(421, 18, 129, 21);
		txtBillNo.setText(String.valueOf(UtilMethods.getMaxId("inven_stock_out", "bill_no")));
		
		Label lblPurchasedDate = new Label(grpSalesInformation, SWT.NONE);
		lblPurchasedDate.setAlignment(SWT.RIGHT);
		lblPurchasedDate.setBounds(553, 20, 105, 15);
		lblPurchasedDate.setText("Sale Date: ");
		lblPurchasedDate.setForeground(UtilMethods.getFontForegroundColorRed());
		
		bcmbSelDate = new DateChooserCombo(grpSalesInformation, SWT.NONE);
		bcmbSelDate.setBounds(661, 18, 105, 17);
		bcmbSelDate.setValue(new Date());
		
		grpSalesDetails = new Group(comMainContainer, SWT.SHADOW_IN);
		grpSalesDetails.setText("Sales Details");
		FormData fd_grpSalesDetails = new FormData();
		fd_grpSalesDetails.top = new FormAttachment(grpSalesInformation, 2);
		fd_grpSalesDetails.left = new FormAttachment(grpSalesInformation, 0, SWT.LEFT);
		fd_grpSalesDetails.right = new FormAttachment(100, -10);
		grpSalesDetails.setLayoutData(fd_grpSalesDetails);
		
		Label lblItem = new Label(grpSalesDetails, SWT.NONE);
		lblItem.setAlignment(SWT.RIGHT);
		lblItem.setBounds(10, 20, 65, 15);
		lblItem.setText("Item: ");
		lblItem.setForeground(UtilMethods.getFontForegroundColorRed());
		
		bcmbItem = new SearchBox(grpSalesDetails, SWT.NONE);
		bcmbItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(!bcmbItem.getSelectedBoundValue().toString().equals("0"))
				{
					clearItemDetails();
					loadItemDetails(Integer.valueOf(bcmbItem.getSelectedBoundValue().toString()));
				}
				else
				{
					clearItemDetails();
				}
			}
		});
		
		bcmbItem.setBounds(76, 18, 179, 23);
		bcmbItem.fillData(CmbQry.getStockItems(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		
		Label lblUnit = new Label(grpSalesDetails, SWT.NONE);
		lblUnit.setAlignment(SWT.RIGHT);
		lblUnit.setBounds(576, 20, 32, 15);
		lblUnit.setText("Unit: ");
		
		bcmbUnit = new SwtBndCombo(grpSalesDetails, SWT.BORDER | SWT.READ_ONLY);
		bcmbUnit.setEnabled(false);
		bcmbUnit.setBounds(608, 17, 131, 21);
		bcmbUnit.fillData(CmbQry.getUnit(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection());
		
		Label lblUnitQuantity_1 = new Label(grpSalesDetails, SWT.NONE);
		lblUnitQuantity_1.setAlignment(SWT.RIGHT);
		lblUnitQuantity_1.setBounds(10, 49, 65, 15);
		lblUnitQuantity_1.setText("Quantity: ");
		lblUnitQuantity_1.setForeground(UtilMethods.getFontForegroundColorRed());
		
		txtUnitQuantity = new Text(grpSalesDetails, SWT.BORDER);
		txtUnitQuantity.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				double avail=Double.valueOf(txtAvailableQunatity.getText().equals("")?"0":txtAvailableQunatity.getText().replaceAll(",",""));
				double quan=Double.valueOf(txtUnitQuantity.getText().equals("")?"0":txtUnitQuantity.getText().replaceAll(",",""));
				double rate=Double.valueOf(txtRate.getText().equals("")?"0":txtRate.getText().replaceAll(",",""));
				if( avail > 0)
				{
					if(quan > avail)
					{
						msg.showIndividualMessage("Selling quantity is more than available quantity", 0);
						txtUnitQuantity.setText("0");
						txtTotal.setText("0");
					}
					else
					{
						txtTotal.setText(UtilFxn.getFormatedAmount(rate*quan));
					}
				}
				else
				{
					msg.showIndividualMessage("No available quantity", 0);
					txtTotal.setText("0");
					txtUnitQuantity.setText("0");
				}
			}
		});
		
		
		txtUnitQuantity.setBounds(76, 47, 162, 21);
		txtUnitQuantity.addListener(SWT.Verify, new IntegerTextPositiveOnly(
				txtUnitQuantity, 20));
		
		Label lblRate = new Label(grpSalesDetails, SWT.NONE);
		lblRate.setAlignment(SWT.RIGHT);
		lblRate.setBounds(275, 49, 83, 15);
		lblRate.setText("Rate: ");
		lblRate.setForeground(UtilMethods.getFontForegroundColorRed());
		
		txtRate = new Text(grpSalesDetails, SWT.BORDER | SWT.READ_ONLY);
		
		txtRate.setBounds(360, 47, 131, 21);
		txtRate.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtRate, 20));
		
		Label lblTotal = new Label(grpSalesDetails, SWT.NONE);
		lblTotal.setAlignment(SWT.RIGHT);
		lblTotal.setBounds(553, 49, 55, 15);
		lblTotal.setText("Total: ");
		
		txtTotal = new Text(grpSalesDetails, SWT.BORDER | SWT.READ_ONLY);
		txtTotal.setBounds(608, 47, 131, 21);
		fd_grpSalesInformation.bottom = new FormAttachment(100, -599);
		
		tblItem = new Table(comMainContainer, SWT.BORDER | SWT.FULL_SELECTION);
		FormData fd_tblItem = new FormData();
		fd_tblItem.left = new FormAttachment(grpSalesInformation, 0, SWT.LEFT);
		fd_tblItem.right = new FormAttachment(grpSalesInformation, 0, SWT.RIGHT);
		
		Label lblUnitQuantity = new Label(grpSalesDetails, SWT.NONE);
		lblUnitQuantity.setBounds(757, 20, 65, 15);
		lblUnitQuantity.setAlignment(SWT.RIGHT);
		lblUnitQuantity.setText("Available: ");
		
		txtAvailableQunatity = new Text(grpSalesDetails, SWT.BORDER | SWT.READ_ONLY);
		txtAvailableQunatity.setBounds(822, 18, 131, 21);
		txtAvailableQunatity.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtAvailableQunatity, 20));
		tblItem.setLayoutData(fd_tblItem);
		tblItem.setHeaderVisible(true);
		tblItem.setLinesVisible(true);
		
		TableColumn tblclmnId = new TableColumn(tblItem, SWT.NONE);
		tblclmnId.setWidth(0);
		tblclmnId.setText("itemid.");
		tblclmnId.setResizable(false);
		
		
		
		TableColumn tblitemName = new TableColumn(tblItem, SWT.NONE);
		tblitemName.setWidth(0);
		tblitemName.setText("Item Name.");
		tblitemName.setResizable(false);
		
		TableColumn tblclmnSn = new TableColumn(tblItem, SWT.NONE);
		tblclmnSn.setWidth(50);
		tblclmnSn.setText("S.N.");
		
		TableColumn tblclmnItemCode = new TableColumn(tblItem, SWT.NONE);
		tblclmnItemCode.setWidth(88);
		tblclmnItemCode.setText("Item Code");
		
		TableColumn tblclmnWeightkg = new TableColumn(tblItem, SWT.NONE);
		tblclmnWeightkg.setWidth(123);
		tblclmnWeightkg.setText("Item/Service Name: ");
		
		TableColumn tblclmnRackNo = new TableColumn(tblItem, SWT.NONE);
		tblclmnRackNo.setWidth(70);
		tblclmnRackNo.setText("Rack No");
		
		TableColumn tblclmnServiceProvider = new TableColumn(tblItem, SWT.NONE);
		tblclmnServiceProvider.setWidth(100);
		tblclmnServiceProvider.setText("Service Provider");
		
		TableColumn tblclmnTpe = new TableColumn(tblItem, SWT.NONE);
		tblclmnTpe.setWidth(58);
		tblclmnTpe.setText("T.P.E(%)");
		
		TableColumn tblclmnTpp = new TableColumn(tblItem, SWT.NONE);
		tblclmnTpp.setWidth(62);
		tblclmnTpp.setText("T.P.P(%)");
		
		TableColumn tblclmnUnit = new TableColumn(tblItem, SWT.NONE);
		tblclmnUnit.setWidth(78);
		tblclmnUnit.setText("Unit");
		
		TableColumn tblclmnAvailable = new TableColumn(tblItem, SWT.NONE);
		tblclmnAvailable.setWidth(62);
		tblclmnAvailable.setText("Available");
		
		TableColumn tblclmnUnitQuantity = new TableColumn(tblItem, SWT.NONE);
		tblclmnUnitQuantity.setWidth(86);
		tblclmnUnitQuantity.setText("Unit Quantity");
		
		TableColumn tblclmnRate = new TableColumn(tblItem, SWT.NONE);
		tblclmnRate.setWidth(84);
		tblclmnRate.setText("Rate(Rs)");
		
		TableColumn tblclmnTotal = new TableColumn(tblItem, SWT.NONE);
		tblclmnTotal.setWidth(134);
		tblclmnTotal.setText("Total(Rs)");
		
		
		
		TableColumn tblclmnUpdtcnt = new TableColumn(tblItem, SWT.NONE);
		tblclmnUpdtcnt.setWidth(0);
		tblclmnUpdtcnt.setText("Total(Rs)");
		tblclmnUpdtcnt.setResizable(false);
		
		TableColumn tblclmnType = new TableColumn(tblItem, SWT.NONE);
		tblclmnType.setWidth(0);
		tblclmnType.setText("Total(Rs)");
		tblclmnType.setResizable(false);
		
		TableColumn tblUnit = new TableColumn(tblItem, SWT.NONE);
		tblUnit.setWidth(0);
		tblUnit.setText("Total(Rs)");
		tblUnit.setResizable(false);
		
		TableColumn id = new TableColumn(tblItem, SWT.NONE);
		id.setWidth(0);
		id.setText("Total(Rs)");
		id.setResizable(false);
		
		Composite compPayment = new Composite(comMainContainer, SWT.NONE);
		fd_tblItem.bottom = new FormAttachment(100, -183);
		FormData fd_compPayment = new FormData();
		fd_compPayment.top = new FormAttachment(tblItem, 6);
		fd_compPayment.left = new FormAttachment(0, 10);
		fd_compPayment.bottom = new FormAttachment(100, -16);
		fd_compPayment.right = new FormAttachment(100, -10);
		compPayment.setLayoutData(fd_compPayment);
		compPayment.setLayout(null);
		
		Label lblTotalAmount = new Label(compPayment, SWT.RIGHT);
		lblTotalAmount.setBounds(739, 54, 82, 15);
		lblTotalAmount.setText("Vat: ");
		lblTotalAmount.setFont(UtilMethods.getFontBold());
		
		txtVat = new Text(compPayment, SWT.BORDER | SWT.READ_ONLY);
		txtVat.setBounds(876, 52, 87, 20);
		txtVat.setForeground(UtilMethods.getFontForegroundColorRed());
		
		Label lblDueAmount = new Label(compPayment, SWT.RIGHT);
		lblDueAmount.setBounds(713, 102, 108, 15);
		lblDueAmount.setText("Paid Amount: ");
		lblDueAmount.setFont(UtilMethods.getFontBold());
		
		txtPaidAmount = new Text(compPayment, SWT.BORDER | SWT.READ_ONLY);
		txtPaidAmount.setBounds(827, 100, 136, 20);
		txtPaidAmount.setForeground(UtilMethods.getFontForegroundColorRed());
		
		Button btnSave = new Button(compPayment, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tblSourceAmount.getItemCount() <= 0)
				{
					if(bcmbCustomer.getSelectedBoundValue().toString().equals("0"))
					{
						msg.setMandatoryMessages("Please select customer");
					}
				}
				if(txtBillNo.getText().equals(""))
				{
					msg.setMandatoryMessages("Please enter bill no");
				}
				if(bcmbSelDate.getValue()==null)
				{
					msg.setMandatoryMessages("Please select sales date");
				}
				if(tblItem.getItemCount() <= 0)
				{
					msg.setMandatoryMessages("No items to save");
				}
				if(Double.parseDouble(txtGrandTotal.getText().equals("")?"0":txtGrandTotal.getText().replaceAll(",", "")) <= 0)
				{
					msg.setMandatoryMessages("Grand total cannot be empty");
				}
				
				if((Double.parseDouble(txtGrandTotal.getText().equals("")?"0":txtGrandTotal.getText().replaceAll(",", ""))-Double.parseDouble(txtPaidAmount.getText().equals("")?"0":txtPaidAmount.getText().replaceAll(",", "")))!=Double.parseDouble(txtDueAmount.getText().equals("")?"0":txtDueAmount.getText().replaceAll(",", "")))
				{
					msg.setMandatoryMessages("Grand total should be equal to paid amount plus due amount ");
					System.out.println("grand total :" +Double.parseDouble(txtGrandTotal.getText()));
					System.out.println("paid :" +Double.parseDouble(txtPaidAmount.getText()));
					System.out.println("due :" +Double.parseDouble(txtDueAmount.getText()));
				}
				if(msg.returnMessages())
				{
					setSaveValues();
					
					//save action
					Connection cnn=DbConnection.getConnection();
					try {
						cnn.setAutoCommit(false);
						int savedId=control.saveAction(stockSalesModel,itemsList,serviceList,sourceList,reportList,daybook,dayBookDetailList);
						if( savedId > 0)
						{
							if(action.equals("add"))
							{
								clearAll();
							}
							//btnPrint.setEnabled(true);
							
							cnn.commit();
							cnn.setAutoCommit(true);
							if(msg.showIndividualMessage("Sales successfully saved.Do you want to print the bill?", 2))
							{
								StockSalesReportView hms = new StockSalesReportView(
										PlatformUI.getWorkbench()
												.getActiveWorkbenchWindow().getShell(),savedId,"sales");
								hms.open();
							}
						}
						else
						{
							//btnPrint.setEnabled(false);
							msg.showIndividualMessage("Some error occured while saving.Please try again", 0);
							cnn.rollback();
							cnn.setAutoCommit(true);
						}
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						try {
							cnn.rollback();
							cnn.setAutoCommit(true);
						} catch (SQLException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
						
					}
				}
			}
		});
		
		btnSave.setBounds(615, 54, 72, 39);
		btnSave.setText("Save");
		
		Label lblTotal_1 = new Label(compPayment, SWT.RIGHT);
		lblTotal_1.setAlignment(SWT.RIGHT);
		lblTotal_1.setBounds(739, 6, 82, 15);
		lblTotal_1.setText("Total: ");
		lblTotal_1.setFont(UtilMethods.getFontBold());
		
		txtItemTotal = new Text(compPayment, SWT.BORDER);
		txtItemTotal.setEditable(false);
		txtItemTotal.setBounds(827, 4, 136, 20);
		txtItemTotal.setForeground(UtilMethods.getFontForegroundColorRed());
		
		Label lblExpenses = new Label(compPayment, SWT.RIGHT);
		lblExpenses.setAlignment(SWT.RIGHT);
		lblExpenses.setBounds(749, 30, 72, 15);
		lblExpenses.setText("Discount: ");
		lblExpenses.setFont(UtilMethods.getFontBold());
		
		txtItemDiscount = new Text(compPayment, SWT.BORDER);
		txtItemDiscount.setEditable(false);
		txtItemDiscount.setBounds(876, 28, 87, 20);
		txtItemDiscount.setForeground(UtilMethods.getFontForegroundColorRed());
		
		Label lblPaidAmount = new Label(compPayment, SWT.RIGHT);
		lblPaidAmount.setBounds(739, 78, 82, 15);
		lblPaidAmount.setText("Grand Total: ");
		lblPaidAmount.setFont(UtilMethods.getFontBold());
		
		txtGrandTotal = new Text(compPayment, SWT.BORDER);
		txtGrandTotal.setEditable(false);
		txtGrandTotal.setBounds(827, 76, 136, 20);
		txtGrandTotal.setForeground(UtilMethods.getFontForegroundColorRed());
			
			grpServiceDetails = new Group(comMainContainer, SWT.NONE);
			grpServiceDetails.setText("Service Details");
			FormData fd_grpServiceDetails = new FormData();
			fd_grpServiceDetails.bottom = new FormAttachment(grpSalesDetails, 85, SWT.BOTTOM);
			fd_grpServiceDetails.top = new FormAttachment(grpSalesDetails, 3);
			fd_grpServiceDetails.right = new FormAttachment(grpSalesInformation, 0, SWT.RIGHT);
			fd_grpServiceDetails.left = new FormAttachment(grpSalesInformation, 0, SWT.LEFT);
			grpServiceDetails.setLayoutData(fd_grpServiceDetails);
			
			Label lblService = new Label(grpServiceDetails, SWT.NONE);
			lblService.setAlignment(SWT.RIGHT);
			lblService.setBounds(24, 22, 55, 15);
			lblService.setText("Service: ");
			lblService.setForeground(UtilMethods.getFontForegroundColorRed());
			
			bcmbService = new SearchBox(grpServiceDetails, SWT.NONE);
			bcmbService.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if(!bcmbService.getSelectedBoundValue().toString().equals("0"))
					{
						clearServiceDetails();
						loadServiceDetails(Integer.valueOf(bcmbService.getSelectedBoundValue().toString()));
					}
					else
					{
						clearServiceDetails();
					}
				}
			});
			bcmbService.setEnabled(false);
			bcmbService.setBounds(80, 18, 179, 23);
			bcmbService.fillData(CmbQry.getAllServices(), CmbQry.getLabel(),
					CmbQry.getId(), DbConnection.getConnection(), true);
			
			btnServiceAdd = new Button(grpServiceDetails, SWT.NONE);
			btnServiceAdd.setEnabled(false);
			btnServiceAdd.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					String form="stocksales";
					ServiceEntryForm hms = new ServiceEntryForm(
							PlatformUI.getWorkbench()
									.getActiveWorkbenchWindow().getShell(),0,form);
					hms.open();
				}
			});
			btnServiceAdd.setBounds(259, 17, 38, 25);
			btnServiceAdd.setText("add");
			
			lblServiceProvider = new Label(grpServiceDetails, SWT.NONE);
			lblServiceProvider.setBounds(570, 23, 89, 15);
			lblServiceProvider.setText("Service Provider: ");
			
			bcmbServiceProvider = new SwtBndCombo(grpServiceDetails, SWT.READ_ONLY);
			bcmbServiceProvider.setEnabled(false);
			bcmbServiceProvider.setBounds(661, 20, 179, 21);
			bcmbServiceProvider.fillData(CmbQry.getAllSuppliers(3), CmbQry.getLabel(),
					CmbQry.getId(), DbConnection.getConnection(), true);
			
			btnServiceProviderAdd = new Button(grpServiceDetails, SWT.NONE);
			btnServiceProviderAdd.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					String form="stocksalesservice";
					CustomerManagementView hms = new CustomerManagementView(
							PlatformUI.getWorkbench()
									.getActiveWorkbenchWindow().getShell(),0,form);
					hms.open();
				}
			});
			btnServiceProviderAdd.setEnabled(false);
			btnServiceProviderAdd.setBounds(840, 19, 38, 23);
			btnServiceProviderAdd.setText("add");
			
			Label lblCost = new Label(grpServiceDetails, SWT.NONE);
			lblCost.setBounds(11, 51, 67, 15);
			lblCost.setText("Selling Cost: ");
			lblCost.setForeground(UtilMethods.getFontForegroundColorRed());
			
			txtSellingCost = new Text(grpServiceDetails, SWT.BORDER);
			txtSellingCost.setEnabled(false);
			txtSellingCost.setBounds(80, 49, 179, 21);
			txtSellingCost.addListener(SWT.Verify, new DecimalTextPositiveOnly(
					txtSellingCost, 20));
			
			lblearned = new Label(grpServiceDetails, SWT.NONE);
			lblearned.setAlignment(SWT.RIGHT);
			lblearned.setBounds(353, 51, 67, 15);
			lblearned.setText("%earned: ");
			
			txtPercEarned = new Text(grpServiceDetails, SWT.BORDER);
			txtPercEarned.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					double earned=Double.valueOf(txtPercEarned.getText().equals("")?"0":txtPercEarned.getText());
					if(earned > 100)
					{
						msg.showIndividualMessage("Earned cannot exceeds 100 percentage", 0);
					}
					else
					{
						double paid=100-earned;
						txtPercPaid.setText(UtilFxn.getFormatedInt(paid));
					}
					
				}
			});
			txtPercEarned.setEnabled(false);
			txtPercEarned.setBounds(421, 49, 132, 21);
			txtPercEarned.addListener(SWT.Verify, new DecimalTextPositiveOnly(
					txtPercEarned, 20));
			
			Label lblServiceType = new Label(grpServiceDetails, SWT.NONE);
			lblServiceType.setBounds(348, 23, 73, 15);
			lblServiceType.setText("Service Type: ");
			
			bcmbServiceType = new SwtBndCombo(grpServiceDetails, SWT.READ_ONLY);
			bcmbServiceType.setEnabled(false);
			bcmbServiceType.setBounds(421, 20, 132, 21);
			bcmbServiceType.fillData(CmbQry.getServiceType(), CmbQry.getLabel(),
					CmbQry.getId(), DbConnection.getConnection(), true);
			
			Composite compItemAdd = new Composite(comMainContainer, SWT.NONE);
			fd_tblItem.top = new FormAttachment(compItemAdd, 6);
			fd_grpSalesDetails.bottom = new FormAttachment(100, -521);
			
			Label lblRackNo = new Label(grpSalesDetails, SWT.NONE);
			lblRackNo.setAlignment(SWT.RIGHT);
			lblRackNo.setBounds(303, 20, 55, 15);
			lblRackNo.setText("Rack No: ");
			
			txtRackNo = new Text(grpSalesDetails, SWT.BORDER | SWT.READ_ONLY);
			txtRackNo.setBounds(360, 18, 131, 21);
			
			btnSalesDetails = new Button(grpSalesDetails, SWT.RADIO);
			btnSalesDetails.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if(btnSalesDetails.getSelection())
					{
						enableDisableItems(true,false);
						btnServiceDetails.setSelection(false);
					}
					else
					{
						enableDisableItems(false,false);
					}
				}
			});
			btnSalesDetails.setSelection(true);
			btnSalesDetails.setBounds(10, 0, 90, 16);
			btnSalesDetails.setText("Sales Details");
			compItemAdd.setLayout(null);
			FormData fd_compItemAdd = new FormData();
			fd_compItemAdd.top = new FormAttachment(grpServiceDetails, 6);
			fd_compItemAdd.right = new FormAttachment(100, -10);
			fd_compItemAdd.bottom = new FormAttachment(100, -405);
			
			Group grpPaymentDetails = new Group(compPayment, SWT.NONE);
			grpPaymentDetails.setBounds(0, 0, 583, 156);
			grpPaymentDetails.setText("Payment Details");
			
			btnCash = new Button(grpPaymentDetails, SWT.RADIO);
			btnCash.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					clearCashBank();
					if(btnCash.getSelection())
					{
						toogleCashBank(true,false);
					}
				}
			});
			btnCash.setSelection(true);
			
			btnCash.setBounds(93, 20, 55, 16);
			btnCash.setText("Cash");
			
			btnBank = new Button(grpPaymentDetails, SWT.RADIO);
			btnBank.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					clearCashBank();
					if(btnBank.getSelection())
					{
						toogleCashBank(false,true);
					}
				}
			});
			
			btnBank.setBounds(168, 20, 55, 16);
			btnBank.setText("Bank");
			
			txtAmount = new Text(grpPaymentDetails, SWT.BORDER);
			txtAmount.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					
				}
			});
			txtAmount.setBounds(93, 42, 130, 21);
			txtAmount.addListener(SWT.Verify, new DecimalTextPositiveOnly(
					txtAmount, 20));
			
			txtBank = new Text(grpPaymentDetails, SWT.BORDER);
			txtBank.setEnabled(false);
			txtBank.setBounds(93, 69, 130, 21);
			
			
			Button btnPlus = new Button(grpPaymentDetails, SWT.NONE);
			btnPlus.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					beforeSourceAdd();
					if(msg.returnMessages())
					{
						addSourceItem();
						clearCashBank();
						calculateTotalAmount();
					}
				}
			});
			
			btnPlus.setFont(SWTResourceManager.getFont("Segoe UI", 13, SWT.BOLD));
			btnPlus.setBounds(310, 18, 32, 25);
			btnPlus.setText("+");
			
			tblSourceAmount = new Table(grpPaymentDetails, SWT.BORDER | SWT.FULL_SELECTION);
			tblSourceAmount.setBounds(345, 12, 232, 98);
			tblSourceAmount.setHeaderVisible(true);
			tblSourceAmount.setLinesVisible(true);
			
			TableColumn tblclmnAccCode = new TableColumn(tblSourceAmount, SWT.NONE);
			tblclmnAccCode.setWidth(0);
			tblclmnAccCode.setText("acccode");
			tblclmnAccCode.setResizable(false);
			
			TableColumn tblclmnSn_1 = new TableColumn(tblSourceAmount, SWT.NONE);
			tblclmnSn_1.setWidth(37);
			tblclmnSn_1.setText("S.N.");
			
			TableColumn tblclmnAmount = new TableColumn(tblSourceAmount, SWT.NONE);
			tblclmnAmount.setWidth(83);
			tblclmnAmount.setText("Amount(Rs)");
			
			TableColumn tblclmnSource = new TableColumn(tblSourceAmount, SWT.NONE);
			tblclmnSource.setWidth(107);
			tblclmnSource.setText("Source");
			
			TableColumn tblclmnTrxnId = new TableColumn(tblSourceAmount, SWT.NONE);
			tblclmnTrxnId.setWidth(0);
			tblclmnTrxnId.setText("TrxnId");
			tblclmnTrxnId.setResizable(false);
			
			TableColumn tblid = new TableColumn(tblSourceAmount, SWT.NONE);
			tblid.setWidth(0);
			tblid.setText("TrxnId");
			tblid.setResizable(false);
			
			Button btnX = new Button(grpPaymentDetails, SWT.NONE);
			btnX.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if(tblSourceAmount.getSelectionIndex() < 0)
					{
						msg.showIndividualMessage("Please select the row to delete", 0);
					}
					else
					{
						if(msg.showIndividualMessage("Make sure before you delete the item", 2))
						{
							int selection=tblSourceAmount.getSelectionIndex();
							tblSourceAmount.remove(tblSourceAmount.getSelectionIndex());
							
							if (tblSourceAmount.getItemCount() > 0) {
								
								TableItem[] Item = tblSourceAmount.getItems();
								for (int i = selection+1; i < tblSourceAmount.getItemCount(); i++) {
									Item[i].setText(1, String.valueOf(tblSourceAmount.getItemCount()));
									
								}
								
							}
							calculateTotalAmount();
						}
						
					}
				}
			});
			
			btnX.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
			btnX.setText("x");
			btnX.setBounds(310, 48, 32, 25);
			
			Label lblAvailableBalance = new Label(grpPaymentDetails, SWT.NONE);
			lblAvailableBalance.setBounds(50, 98, 40, 15);
			lblAvailableBalance.setText("v.n/c.n.");
			
			txtVoucherNo = new Text(grpPaymentDetails, SWT.BORDER);
			txtVoucherNo.setEnabled(false);
			txtVoucherNo.setBounds(93, 95, 130, 21);
			//txtVoucherNo.setForeground(UtilMethods.getFontForegroundColorRed());
			
			Label lblAmount = new Label(grpPaymentDetails, SWT.NONE);
			lblAmount.setAlignment(SWT.RIGHT);
			lblAmount.setBounds(7, 45, 88, 15);
			lblAmount.setText("Amount: ");
			
			Label lblBank = new Label(grpPaymentDetails, SWT.NONE);
			lblBank.setAlignment(SWT.RIGHT);
			lblBank.setBounds(7, 72, 88, 15);
			lblBank.setText("Bank: ");
			
			Label lblRemarks = new Label(grpPaymentDetails, SWT.NONE);
			lblRemarks.setBounds(287, 119, 55, 15);
			lblRemarks.setText("Remarks");
			
			txtRemarks = new Text(grpPaymentDetails, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
			txtRemarks.setBounds(345, 114, 232, 38);
			
			bcmbDeposite = new SwtBndCombo(grpPaymentDetails, SWT.READ_ONLY);
			bcmbDeposite.setBounds(93, 122, 130, 23);
			bcmbDeposite.fillData(CmbQry.getDepositeType(), CmbQry.getLabel(),
					CmbQry.getId(), DbConnection.getConnection(), true);
			
			Label lblAcDeposit = new Label(grpPaymentDetails, SWT.NONE);
			lblAcDeposit.setBounds(20, 125, 69, 15);
			lblAcDeposit.setText("A/C deposite");
			
			Label lblDueAmount_1 = new Label(compPayment, SWT.RIGHT);
			lblDueAmount_1.setText("Due Amount: ");
			lblDueAmount_1.setFont(SWTResourceManager.getFont("Arial", 10, SWT.BOLD));
			lblDueAmount_1.setBounds(739, 126, 82, 15);
			
			txtDueAmount = new Text(compPayment, SWT.BORDER | SWT.READ_ONLY);
			txtDueAmount.setForeground(SWTResourceManager.getColor(255, 0, 0));
			txtDueAmount.setBounds(827, 124, 136, 20);
			
			txtDiscountPercentage = new Text(compPayment, SWT.BORDER);
			txtDiscountPercentage.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					double dis=Double.valueOf(discount);
					double discountentered=Double.valueOf(txtDiscountPercentage.getText().equals("")?"0":txtDiscountPercentage.getText());
					if(discountentered > 0)
					{
						
						if(discountentered > dis )
						{
							msg.showIndividualMessage("Discount entered is greater than the defined discount amount", 0);
							txtDiscountPercentage.setText("");
						}
						
					}
					calculateTotalAmount();
				}
			});
			txtDiscountPercentage.setBounds(827, 28, 32, 20);
			//txtDiscountPercentage.setText(returnVatDiscount("discount"));
			txtDiscountPercentage.addListener(SWT.Verify, new DecimalTextPositiveOnly(
					txtDiscountPercentage, 20));
			
			Label label = new Label(compPayment, SWT.NONE);
			label.setBounds(861, 31, 12, 15);
			label.setText("%");
			
			txtVatPercentage = new Text(compPayment, SWT.BORDER);
			txtVatPercentage.setEditable(false);
			txtVatPercentage.setBounds(827, 52, 32, 20);
			txtVatPercentage.setText(returnVatDiscount("vat"));
			
			
			Label label_1 = new Label(compPayment, SWT.NONE);
			label_1.setText("%");
			label_1.setBounds(861, 55, 12, 15);
			fd_compItemAdd.left = new FormAttachment(0, 748);
			
			lblpaid = new Label(grpServiceDetails, SWT.NONE);
			lblpaid.setAlignment(SWT.RIGHT);
			lblpaid.setBounds(604, 51, 55, 15);
			lblpaid.setText("%paid: ");
			
			txtPercPaid = new Text(grpServiceDetails, SWT.BORDER);
			txtPercPaid.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					double paid=Double.valueOf(txtPercPaid.getText().equals("")?"0":txtPercPaid.getText());
					if(paid > 100)
					{
						msg.showIndividualMessage("Paid cannot exceeds 100 percentage", 0);
					}
					else
					{
						double earned=100-paid;
						txtPercEarned.setText(UtilFxn.getFormatedInt(earned));
					}
				}
			});
			txtPercPaid.setEnabled(false);
			txtPercPaid.setBounds(661, 49, 132, 21);
			txtPercPaid.addListener(SWT.Verify, new DecimalTextPositiveOnly(
					txtPercPaid, 20));
			
			btnServiceDetails = new Button(grpServiceDetails, SWT.RADIO);
			btnServiceDetails.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if(btnServiceDetails.getSelection())
					{
						enableDisableItems(false,true);
						btnSalesDetails.setSelection(false);
					}
					else
					{
						enableDisableItems(false,false);
					}
				}
			});
			btnServiceDetails.setBounds(10, 0, 96, 16);
			btnServiceDetails.setText("Service Details");
			
			Label lblBillingType = new Label(grpSalesInformation, SWT.NONE);
			lblBillingType.setBounds(792, 20, 68, 15);
			lblBillingType.setText("Billing Type: ");
			//lblBillingType.setForeground(UtilMethods.getFontForegroundColorRed());
			
			bcmbBillType = new SwtBndCombo(grpSalesInformation, SWT.READ_ONLY);
			bcmbBillType.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					//if(bcmbBillType.getSelectedBoundValue().toString().equals("2"))
					//{
						calculateTotalAmount();
					//}
				}
			});
			bcmbBillType.setBounds(862, 18, 91, 23);
			compItemAdd.setLayoutData(fd_compItemAdd);
			bcmbBillType.add("General", "1");
			bcmbBillType.add("Vat", "2");
			bcmbBillType.select(0);
			
			Button btnAdd = new Button(compItemAdd, SWT.NONE);
			btnAdd.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					checkBeforeItemAdd();
					if(msg.returnMessages())
					{
						loadItem();
						itemAction="add";
						clearItemDetails();
						clearServiceDetails();
						bcmbItem.setSelectedBoundValue("0");
						bcmbService.setSelectedBoundValue("0");
						calculateTotalAmount();
					}
				}
			});
			btnAdd.setBounds(57, 0, 52, 25);
			
			btnAdd.setText("Add");
			
			Button btnEdit = new Button(compItemAdd, SWT.NONE);
			btnEdit.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if(tblItem.getSelectionIndex() >= 0)
					{
						itemAction="edit";
						loadItemEditValues();
						
					}
					else
					{
						msg.showIndividualMessage("Please select item to edit", 0);
					}
				}
			});
			btnEdit.setBounds(115, 0, 52, 25);
			
			btnEdit.setText("Edit");
			
			Button btnDelete = new Button(compItemAdd, SWT.NONE);
			btnDelete.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if(tblItem.getSelectionIndex() < 0)
					{
						msg.showIndividualMessage("Please select item to delete", 0);
					}
					else
					{
						if(msg.showIndividualMessage("Make sure before you delete the item", 2))
						{
							tblItem.remove(tblItem.getSelectionIndex());
							
						}
						calculateTotalAmount();
					}
				}
			});
			btnDelete.setBounds(173, 0, 52, 25);
			
			btnDelete.setText("Delete");
			comMainContainer.setTabList(new Control[]{grpSalesInformation, grpSalesDetails, grpServiceDetails, compItemAdd, tblItem, compPayment});

		
		
			if(salesId > 0)
			{
				loadEditData(salesId);
			}
		
			
		return comParent;
	}
	
	private void clearItemDetails()
	{
		//bcmbItem.setSelectedBoundValue("0");
		txtRackNo.setText("");
		bcmbUnit.setSelectedBoundValue("0");
		txtAvailableQunatity.setText("");
		txtUnitQuantity.setText("");
		txtRate.setText("");
		txtTotal.setText("");
		itemName="";
		equivalence=0;
		
	}
	private void enableDisableItems(boolean item,boolean service)
	{
		
		clearItemDetails();
		bcmbItem.setSelectedBoundValue("0");
		bcmbItem.setEnabled(item);
		txtUnitQuantity.setEnabled(item);
		btnSalesDetails.setSelection(item);
		
		clearServiceDetails();
		bcmbService.setSelectedBoundValue("0");
		bcmbService.setEnabled(service);
		btnServiceAdd.setEnabled(service);
		bcmbServiceProvider.setEnabled(service);
		btnServiceProviderAdd.setEnabled(service);
		txtSellingCost.setEnabled(service);
		txtPercEarned.setEnabled(service);
		txtPercPaid.setEnabled(service);
		btnServiceDetails.setSelection(service);
		
	}
	private void clearServiceDetails()
	{
		bcmbServiceType.setSelectedBoundValue("0");
		bcmbServiceProvider.setSelectedBoundValue("0");
		txtSellingCost.setText("");
		txtPercEarned.setText("");
		txtPercPaid.setText("");
		serviceCode="";
	}
	
	
	private void loadItemDetails(int itemId)
	{
		ResultSet rs=itemControl.fetchItemRemainingDetailsById(itemId);
		try {
			if(UtilFxn.getRowCount(rs)> 0)
			{
				if(rs.next())
				{
					txtRackNo.setText(rs.getString("item_rack_no"));
					bcmbUnit.setSelectedBoundValue(rs.getInt("item_sel_unit_id"));
					equivalence=rs.getDouble("sel_unit_equ");
					double uniavail=rs.getDouble("remaining_unit_total");
					
					itemName=rs.getString("item_name");
					
					txtAvailableQunatity.setText(UtilFxn.getFormatedInt(uniavail/equivalence));
					txtRate.setText(UtilFxn.getFormatedAmount(rs.getDouble("item_selling_price")));
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void loadServiceDetails(int sid)
	{
		ResultSet rs=serviceControl.getServiceDetails(sid);
		
		try {
			if(UtilFxn.getRowCount(rs) > 0)
			{
				if(rs.next())
				{
					bcmbServiceType.setSelectedBoundValue(rs.getInt("service_type"));
					serviceCode=rs.getString("service_code")==null?"":rs.getString("service_code");
					
				}
				if(bcmbServiceType.getSelectedBoundValue().toString().equals("1"))//own service
				{
					bcmbServiceProvider.setEnabled(false);
					btnServiceProviderAdd.setEnabled(false);
					txtPercEarned.setEnabled(false);
					txtPercPaid.setEnabled(false);
					lblServiceProvider.setForeground(UtilMethods.getFontForegroundColorBlack());
					lblpaid.setForeground(UtilMethods.getFontForegroundColorBlack());
					lblearned.setForeground(UtilMethods.getFontForegroundColorBlack());
				}
				else
				{
					bcmbServiceProvider.setEnabled(true);
					btnServiceProviderAdd.setEnabled(true);
					lblServiceProvider.setForeground(UtilMethods.getFontForegroundColorRed());
					lblearned.setForeground(UtilMethods.getFontForegroundColorRed());
					lblpaid.setForeground(UtilMethods.getFontForegroundColorRed());
					txtPercEarned.setEnabled(true);
					txtPercPaid.setEnabled(true);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		public void reloadCustomer()
		{
			bcmbCustomer.fillData(CmbQry.getAllSuppliers(1), CmbQry.getLabel(),
					CmbQry.getId(), DbConnection.getConnection(), true);
		}
		
		public void reloadService()
		{
			bcmbService.fillData(CmbQry.getAllServices(), CmbQry.getLabel(),
					CmbQry.getId(), DbConnection.getConnection(), true);
		}
		
		public void reloadServiceProviders()
		{
			bcmbServiceProvider.fillData(CmbQry.getAllSuppliers(3), CmbQry.getLabel(),
					CmbQry.getId(), DbConnection.getConnection(), true);
		}
		
		private void checkBeforeItemAdd(){
			if(btnSalesDetails.getSelection())
			{
				if(bcmbItem.getSelectedBoundValue().toString().equals("0"))
				{
					msg.setMandatoryMessages("Item must be selected");
				}
				if(txtUnitQuantity.getText().equals(""))
				{
					msg.setMandatoryMessages("Please enter unit quantity");
				}
				else if(Integer.valueOf(txtUnitQuantity.getText())<= 0)
				{
					msg.setMandatoryMessages("Please enter unit quantity");
				}
				
				//loop in the table to see if selected item is already added
				if (tblItem.getItemCount() > 0 && itemAction.equals("add")) {
					TableItem[] Item = tblItem.getItems();
					for (int i = 0; i < tblItem.getItemCount(); i++) {
						String itemId=Item[i].getText(0);
						if(itemId.equals(bcmbItem.getSelectedBoundValue().toString()))
						{
							msg.setMandatoryMessages("Selected item already added in the table.");
							break;
						}
					}
					
				}
			}
			else if(btnServiceDetails.getSelection())
			{
				if(bcmbService.getSelectedBoundValue().toString().equals("0"))
				{
					msg.setMandatoryMessages("service must be selected");
				}
				if(txtSellingCost.getText().equals(""))
				{
					msg.setMandatoryMessages("Service selling cost is required");
				}
				else
				{
					if(Double.valueOf(txtSellingCost.getText()) <= 0)
					{
						msg.setMandatoryMessages("Service selling cost is required");
					}
				}
				if(bcmbServiceType.getSelectedBoundValue().toString().equals("2"))
				{
					if(bcmbServiceProvider.getSelectedBoundValue().toString().equals("0"))
					{
						msg.setMandatoryMessages("service provider must be selected");
					}
					if(txtPercEarned.getText().equals("") && txtPercPaid.getText().equals(""))
					{
						msg.setMandatoryMessages("Third party percentage earned and percentage paid is required");
					}
					else
					{
						if(Double.valueOf(txtPercEarned.getText()) <= 0 && Double.valueOf(txtPercPaid.getText()) <= 0)
						{
							msg.setMandatoryMessages("Third party percentage earned and percentage paid is required");
						}
					}
					
				}
				//loop in the table to see if selected item is already added
				if (tblItem.getItemCount() > 0 && itemAction.equals("add")) {
					TableItem[] Item = tblItem.getItems();
					for (int i = 0; i < tblItem.getItemCount(); i++) {
						String itemId=Item[i].getText(0);
						if(itemId.equals(bcmbService.getSelectedBoundValue().toString()))
						{
							msg.setMandatoryMessages("Selected service is already added in the table.");
							break;
						}
					}
					
				}
			}
			
			
			
		}
		
		private void loadItem()
		{
			
			if(btnSalesDetails.getSelection())
			{
				if(itemAction.equals("edit"))
				{
					TableItem[] Item = tblItem.getItems();
					for (int i = 0; i < tblItem.getItemCount(); i++) {
						String itemId=Item[i].getText(0);
						if(itemId.equals(bcmbItem.getSelectedBoundValue().toString()))
						{
							Item[i].setText(0, bcmbItem.getSelectedBoundValue().toString());
							Item[i].setText(1,String.valueOf(equivalence));
							Item[i].setText(2,String.valueOf(tblItem.getItemCount()));
							Item[i].setText(3,bcmbItem.getText());
							Item[i].setText(4,String.valueOf(itemName));
							Item[i].setText(5,txtRackNo.getText());
							Item[i].setText(6,"-");
							Item[i].setText(7,"-");
							Item[i].setText(8,"-");
							Item[i].setText(9,bcmbUnit.getText());
							Item[i].setText(10,txtAvailableQunatity.getText());
							Item[i].setText(11,txtUnitQuantity.getText());
							Item[i].setText(12,txtRate.getText());
							Item[i].setText(13,txtTotal.getText());
							//Item[i].setText(14,String.valueOf(-1));
							Item[i].setText(15,"sales");
							Item[i].setText(16,bcmbUnit.getSelectedBoundValue().toString());
						}
					}
				}
				else
				{
					TableItem item=new TableItem(tblItem, SWT.NONE);
					item.setText(0, bcmbItem.getSelectedBoundValue().toString());
					item.setText(1,String.valueOf(equivalence));
					item.setText(2,String.valueOf(tblItem.getItemCount()));
					item.setText(3,bcmbItem.getText());
					item.setText(4,String.valueOf(itemName));
					item.setText(5,txtRackNo.getText());
					item.setText(6,"-");
					item.setText(7,"-");
					item.setText(8,"-");
					item.setText(9,bcmbUnit.getText());
					item.setText(10,txtAvailableQunatity.getText());
					item.setText(11,txtUnitQuantity.getText());
					item.setText(12,txtRate.getText());
					item.setText(13,txtTotal.getText());
					//item.setText(14,String.valueOf(-1));
					item.setText(15,"sales");
					item.setText(16,bcmbUnit.getSelectedBoundValue().toString());
					item.setText(17,String.valueOf(0));
				}
				
			}
			else if(btnServiceDetails.getSelection())
			{
				if(itemAction.equals("edit"))
				{
					TableItem[] Item = tblItem.getItems();
					for (int i = 0; i < tblItem.getItemCount(); i++) {
						String itemId=Item[i].getText(0);
						if(itemId.equals(bcmbItem.getSelectedBoundValue().toString()))
						{
							Item[i].setText(0, bcmbService.getSelectedBoundValue().toString());
							Item[i].setText(1,String.valueOf(bcmbServiceProvider.getSelectedBoundValue()));
							Item[i].setText(2,String.valueOf(tblItem.getItemCount()));
							Item[i].setText(3,serviceCode.equals("")?"-":serviceCode);
							Item[i].setText(4,String.valueOf(bcmbService.getText()));
							Item[i].setText(5,"-");
							Item[i].setText(6,String.valueOf(bcmbServiceProvider.getText().equals("Select")?"-":bcmbServiceProvider.getText()));
							Item[i].setText(7,txtPercEarned.getText().equals("")?"0":txtPercEarned.getText());
							Item[i].setText(8,txtPercPaid.getText().equals("")?"0":txtPercPaid.getText());
							Item[i].setText(9,"-");
							Item[i].setText(10,"-");
							Item[i].setText(11,"1");
							Item[i].setText(12,txtSellingCost.getText());
							Item[i].setText(13,txtSellingCost.getText());
							//Item[i].setText(14,String.valueOf(-1));
							Item[i].setText(15,"service");
							Item[i].setText(16,bcmbServiceType.getSelectedBoundValue().toString());
						}
					}
				}
				else
				{
					TableItem item=new TableItem(tblItem, SWT.NONE);
					item.setText(0, bcmbService.getSelectedBoundValue().toString());
					item.setText(1,String.valueOf(bcmbServiceProvider.getSelectedBoundValue()));
					item.setText(2,String.valueOf(tblItem.getItemCount()));
					item.setText(3,serviceCode.equals("")?"-":serviceCode);
					item.setText(4,String.valueOf(bcmbService.getText()));
					item.setText(5,"-");
					item.setText(6,String.valueOf(bcmbServiceProvider.getText().equals("Select")?"-":bcmbServiceProvider.getText()));
					item.setText(7,txtPercEarned.getText().equals("")?"0":txtPercEarned.getText());
					item.setText(8,txtPercPaid.getText().equals("")?"0":txtPercPaid.getText());
					item.setText(9,"-");
					item.setText(10,"-");
					item.setText(11,"1");
					item.setText(12,txtSellingCost.getText());
					item.setText(13,txtSellingCost.getText());
					//item.setText(14,String.valueOf(-1));
					item.setText(15,"service");
					item.setText(16,bcmbServiceType.getSelectedBoundValue().toString());
					item.setText(17,String.valueOf(0));
				}
				
			}
			
		}
		
		private void loadItemEditValues()
		{
			if(tblItem.getItem(tblItem.getSelectionIndex()).getText(15).equals("sales"))
			{
				enableDisableItems(true, false);
				
				bcmbItem.setSelectedBoundValue(tblItem.getItem(
						tblItem.getSelectionIndex()).getText(0));
				equivalence=Double.parseDouble(tblItem.getItem(
						tblItem.getSelectionIndex()).getText(1));
				txtRackNo.setText(tblItem.getItem(
						tblItem.getSelectionIndex()).getText(5));
				bcmbUnit.setSelectedBoundValue(tblItem.getItem(
						tblItem.getSelectionIndex()).getText(16));
				txtAvailableQunatity.setText(tblItem.getItem(
						tblItem.getSelectionIndex()).getText(10));
				txtUnitQuantity.setText(tblItem.getItem(
						tblItem.getSelectionIndex()).getText(11));
				txtRate.setText(tblItem.getItem(
						tblItem.getSelectionIndex()).getText(12));
				txtTotal.setText(tblItem.getItem(
						tblItem.getSelectionIndex()).getText(13));
			}
			else if(tblItem.getItem(tblItem.getSelectionIndex()).getText(15).equals("service"))
			{
				enableDisableItems(false, true);
				bcmbService.setSelectedBoundValue(tblItem.getItem(
						tblItem.getSelectionIndex()).getText(0));
				bcmbServiceType.setSelectedBoundValue(tblItem.getItem(
						tblItem.getSelectionIndex()).getText(16));
				bcmbServiceProvider.setSelectedBoundValue(tblItem.getItem(
						tblItem.getSelectionIndex()).getText(1));
				txtSellingCost.setText(tblItem.getItem(
						tblItem.getSelectionIndex()).getText(12));
				txtPercEarned.setText(tblItem.getItem(
						tblItem.getSelectionIndex()).getText(7));
				txtPercPaid.setText(tblItem.getItem(
						tblItem.getSelectionIndex()).getText(8));
			}
			
		}
		
		private void toogleCashBank(boolean cash,boolean bank)
		{
			if(cash)
			{
				txtAmount.setEnabled(true);
				txtBank.setEnabled(false);
				txtVoucherNo.setEnabled(false);
				
			}
			else if(bank)
			{
				txtAmount.setEnabled(true);
				txtBank.setEnabled(true);
				txtVoucherNo.setEnabled(true);
				
			}
		}
		
		private void beforeSourceAdd()
		{
			if(txtAmount.getText().equals(""))
			{
				msg.setMandatoryMessages("Amount is required");
			}
			else if(Double.valueOf(txtAmount.getText()) <= 0)
			{
				msg.setMandatoryMessages("Amount is required");
			}
			
			
			double amount=Double.valueOf(txtAmount.getText().equals("")?"0":txtAmount.getText().replaceAll(",", ""));
			if((amount+checkTotalSourceAmount()) > Double.parseDouble(txtGrandTotal.getText().equals("")?"0":txtGrandTotal.getText().replaceAll(",", "")))
			{
				msg.setMandatoryMessages("Paid amount exceeds the grand total amount");
			}
			if(btnBank.getSelection())
			{
				if(txtBank.getText().equals(""))
				{
					msg.setMandatoryMessages("Bank name is required");
				}
				if(txtVoucherNo.getText().equals(""))
				{
					msg.setMandatoryMessages("Voucher No is required");
				}
			}
			if(bcmbDeposite.getSelectedBoundValue().equals("0"))
			{
				msg.setMandatoryMessages("Dr account is required");
			}
		}
		
		private void clearCashBank()
		{
			txtAmount.setText("");
			txtBank.setText("");
			txtVoucherNo.setText("");
			bcmbDeposite.setSelectedBoundValue("0");
		}
		private void addSourceItem()
		{
			TableItem item = new TableItem(tblSourceAmount, SWT.NONE);
			if(btnCash.getSelection())
			{
				item.setText(0,  bcmbDeposite.getSelectedBoundValue().toString());
				item.setText(2, txtAmount.getText());
				item.setText(3,"Cash");
				item.setText(4,"0");
			}
			else if(btnBank.getSelection())
			{
				item.setText(0,  bcmbDeposite.getSelectedBoundValue().toString());
				item.setText(2, txtAmount.getText());
				item.setText(3,txtBank.getText());
				item.setText(4,txtVoucherNo.getText());
			}
			item.setText(1, String.valueOf(tblSourceAmount.getItemCount()));
			item.setText(5,String.valueOf(0));
		}
		
		private String returnVatDiscount(String lvl)
		{
		
			String vat=UtilFxn.GetValueFromTable("select cv_code from code_value_mcg where cv_type='sales' and cv_lbl='vat (%)'");
			String discount=UtilFxn.GetValueFromTable("select cv_code from code_value_mcg where cv_type='sales' and cv_lbl='Discount (%)'");
			
			if(lvl.equals("vat"))
			{
				return vat;
			}
			else
				return discount;
		}
		
		private void calculateTotalAmount()
		{
			double totItemPrice=0;
			if (tblItem.getItemCount() > 0) {
				TableItem[] Item = tblItem.getItems();
				for (int i = 0; i < tblItem.getItemCount(); i++) {
					totItemPrice+=Double.parseDouble(Item[i].getText(13).replaceAll(",", ""));
				}
				
			}
			txtItemTotal.setText(UtilFxn.getFormatedAmount(totItemPrice));
			
			double discountPercentage=Double.valueOf(txtDiscountPercentage.getText().equals("")?"0":txtDiscountPercentage.getText());
			double discount=(discountPercentage/100)*totItemPrice;
			
			
			double vatpercentage=0;
			double vat=0;
			if(bcmbBillType.getSelectedBoundValue().toString().equals("2"))
			{
				vatpercentage=Double.valueOf(txtVatPercentage.getText().equals("")?"0":txtVatPercentage.getText());
				vat=(vatpercentage/100)*(totItemPrice-discount);
			}
			
			
			txtItemDiscount.setText(UtilFxn.getFormatedAmount(discount));
			txtVat.setText(UtilFxn.getFormatedAmount(vat));
			
			double grandTotal=(totItemPrice-discount)+vat;
			double paidAmt=checkTotalSourceAmount();
			txtGrandTotal.setText(UtilFxn.getFormatedAmount(grandTotal));
			txtPaidAmount.setText(UtilFxn.getFormatedAmount(paidAmt));
			txtDueAmount.setText(UtilFxn.getFormatedAmount(grandTotal-paidAmt));
		}
		private double checkTotalSourceAmount()
		{
			double amount=0;
			if (tblSourceAmount.getItemCount() > 0) {
				TableItem[] Item = tblSourceAmount.getItems();
				for (int i = 0; i < tblSourceAmount.getItemCount(); i++) {
					String code=Item[i].getText(0);
					
						amount+=Double.parseDouble(Item[i].getText(2).replaceAll(",", ""));
					
				}
				
			}
			return amount;
		}
		
		private void setSaveValues()
		{
			itemsList.clear();
			serviceList.clear();
			reportList.clear();
			sourceList.clear();
			
			dayBookDetailList.clear();
			stockSalesModel=new StockSalesModel();
			stockSalesModel.setCustomer_id(Integer.valueOf(bcmbCustomer.getSelectedBoundValue().toString()));
			stockSalesModel.setBill_no(Integer.valueOf(txtBillNo.getText()));
			stockSalesModel.setTrxn_dt(bcmbSelDate.getText());
			stockSalesModel.setBilling_type(Integer.valueOf(bcmbBillType.getSelectedBoundValue().toString()));
			stockSalesModel.setTotal_amt(UtilFxn.getDoubleExpFormated(txtGrandTotal.getText()));
			stockSalesModel.setPaid_amt(UtilFxn.getDoubleExpFormated(txtPaidAmount.getText()));
			stockSalesModel.setDiscount(UtilFxn.getDoubleExpFormated(txtItemDiscount.getText()));
			stockSalesModel.setVat(UtilFxn.getDoubleExpFormated(txtVat.getText()));
			stockSalesModel.setRemarks(txtRemarks.getText().replaceAll("'", "''"));
			stockSalesModel.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
			stockSalesModel.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
			stockSalesModel.setUpdt_dt(jcalFxn.currentDate());
			stockSalesModel.setDiscount_percentage(txtDiscountPercentage.getText().equals("")?"0":txtDiscountPercentage.getText().replaceAll(",", ""));
			stockSalesModel.setVat_perentage(txtVatPercentage.getText().equals("")?"":txtVatPercentage.getText().replaceAll(",", ""));
			if(action.equals("add"))
			{
				stockSalesModel.setCrtd_dt(jcalFxn.currentDate());
				stockSalesModel.setCrtd_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
			}
			else
			{
				stockSalesModel.setCrtd_dt(stockSalesOldModel.getCrtd_dt());
				stockSalesModel.setCrtd_by(stockSalesOldModel.getCrtd_by());
				stockSalesModel.setUpdt_cnt(stockSalesOldModel.getUpdt_cnt()+1);
				stockSalesModel.setStock_out_id(stockSalesOldModel.getStock_out_id());
				stockSalesModel.setSales_jv_no(stockSalesOldModel.getSales_jv_no());
			}
			
			//daybook
			daybook.setDept_id(1);
			daybook.setExpense(0);
			daybook.setIncome(Double.parseDouble(stockSalesModel.getPaid_amt()));
			daybook.setRemarks(stockSalesModel.getRemarks());
			daybook.setTrxn_date_ad(stockSalesModel.getTrxn_dt());
			daybook.setTrxn_date_bs("0000-00-00");
			daybook.setTrxn_type(317);//317 for sales
			daybook.setCreated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
			daybook.setCreated_dt(jcalFxn.currentDate());
			daybook.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
			daybook.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
			daybook.setUpdt_cnt(0);
			daybook.setUpdt_dt(jcalFxn.currentDate());
			
			
			//daybookdetails discount
			//for daybook details
			DayBookDetailsModel model2=new DayBookDetailsModel();
			model2.setTrxn_with_id(1);
			model2.setTrxn_with_type_id(315);
			model2.setIncome(0);
			model2.setExpense(Double.parseDouble(stockSalesModel.getDiscount()));
			model2.setCreated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
			model2.setCreated_dt(jcalFxn.currentDate());
			//model2.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
			model2.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
			model2.setUpdt_cnt(0);
			model2.setUpdt_dt(jcalFxn.currentDate());
			
			dayBookDetailList.add(model2);
			
			//daybookdetails vat
			model2=new DayBookDetailsModel();
			model2.setTrxn_with_id(1);
			model2.setTrxn_with_type_id(316);
			model2.setIncome(Double.parseDouble(stockSalesModel.getVat()));
			model2.setExpense(0);
			model2.setCreated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
			model2.setCreated_dt(jcalFxn.currentDate());
			//model2.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
			model2.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
			model2.setUpdt_cnt(0);
			model2.setUpdt_dt(jcalFxn.currentDate());
			
			dayBookDetailList.add(model2);
			
			StockSalesReportModel reportModal;
			double serviceEarnedTot=0;
			double servicePaidTot=0;
			if (tblItem.getItemCount() > 0) {
				TableItem[] Item = tblItem.getItems();
				int j=1;
				for (int i = 0; i < tblItem.getItemCount(); i++) {
					StockSalesDetailsModel stockModel=new StockSalesDetailsModel();
					ServiceTrxnModel serviceTrxn=new ServiceTrxnModel();
					
					//load sales
					if(Item[i].getText(15).equals("sales"))
					{
						stockModel.setItem_id(Integer.valueOf(Item[i].getText(0)));
						stockModel.setUnit_id(Integer.valueOf(Item[i].getText(16)));
						stockModel.setQuantity(UtilFxn.getDoubleExpFormated(Double.valueOf(Item[i].getText(11).replaceAll(",", ""))));
						stockModel.setUnit_quantity(UtilFxn.getDoubleExpFormated(Double.valueOf(Item[i].getText(11).replaceAll(",", "")) * Double.valueOf(Item[i].getText(1).replaceAll(",", ""))));
						stockModel.setRate(UtilFxn.getDoubleExpFormated(Double.valueOf(Item[i].getText(12).replaceAll(",", ""))));
						stockModel.setTotal(UtilFxn.getDoubleExpFormated(Double.valueOf(Item[i].getText(13).replaceAll(",", ""))));
						if(action.equals("add"))
						{
							stockModel.setCrtd_dt(jcalFxn.currentDate());
							stockModel.setCrtd_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
							stockModel.setUpdt_cnt(0);
						}
						else
						{
							stockModel.setStock_out_det_id(Integer.valueOf(Item[i].getText(17)));
							stockModel.setUpdt_cnt(Integer.valueOf(Item[i].getText(14))+1);
						}
						stockModel.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
						stockModel.setUpdt_dt(jcalFxn.currentDate());
						
						itemsList.add(stockModel);
						
					}
					//load service
					
					else if(Item[i].getText(15).equals("service"))
					{
						serviceTrxn.setService_id(Integer.valueOf(Item[i].getText(0)));
						serviceTrxn.setTrxn_dt(jcalFxn.currentDate());
						serviceTrxn.setSelling_cost(UtilFxn.getDoubleExpFormated(Item[i].getText(12)));
						serviceTrxn.setPerc_earned(UtilFxn.getDoubleExpFormated(Item[i].getText(7)));
						serviceTrxn.setPerc_paid(UtilFxn.getDoubleExpFormated(Item[i].getText(8)));
						serviceTrxn.setService_provider_id(Integer.valueOf(Item[i].getText(1)));
						serviceTrxn.setCrtd_dt(bcmbSelDate.getText());
						serviceTrxn.setService_trxn_id(Integer.valueOf(Item[i].getText(17)));
						serviceList.add(serviceTrxn);
						serviceEarnedTot=serviceEarnedTot+((Double.parseDouble(serviceTrxn.getPerc_earned())/100)*Double.parseDouble(serviceTrxn.getSelling_cost()));
						servicePaidTot=servicePaidTot+((Double.parseDouble(serviceTrxn.getPerc_paid())/100)*Double.parseDouble(serviceTrxn.getSelling_cost()));
					}
					
					//now for report
					if( i+1== tblItem.getItemCount())
					{
						
						reportModal=new StockSalesReportModel();
						reportModal.setName(Item[i].getText(4));
						reportModal.setQuantity(Item[i].getText(11));
						reportModal.setRate(Item[i].getText(12));
						reportModal.setRpt_order(String.valueOf(j));
						reportModal.setSales_id(0);
						reportModal.setTotal(Item[i].getText(13));
						reportModal.setTrxn_dt(bcmbSelDate.getText());
						reportModal.setUnit(Item[i].getText(9));
						reportModal.setBill_no(String.valueOf(stockSalesModel.getBill_no()));
						reportModal.setReport_for("sales");
						reportList.add(reportModal);
						//total
						reportModal=new StockSalesReportModel();
						reportModal.setName("");
						reportModal.setQuantity("");
						reportModal.setRate("Total");
						reportModal.setRpt_order("");
						reportModal.setSales_id(0);
						reportModal.setTotal(txtItemTotal.getText());
						reportModal.setTrxn_dt(bcmbSelDate.getText());
						reportModal.setUnit("");
						reportModal.setBill_no(String.valueOf(stockSalesModel.getBill_no()));
						reportModal.setReport_for("sales");
						reportList.add(reportModal);
						
						
						//discount
						if(Double.parseDouble(stockSalesModel.getDiscount()) > 0)
						{
							reportModal=new StockSalesReportModel();
							reportModal.setName("");
							reportModal.setQuantity("");
							reportModal.setRate("Discount("+txtDiscountPercentage.getText()+"%)");
							reportModal.setRpt_order("");
							reportModal.setSales_id(0);
							reportModal.setTotal(txtItemDiscount.getText());
							reportModal.setTrxn_dt(bcmbSelDate.getText());
							reportModal.setUnit("");
							reportModal.setBill_no(String.valueOf(stockSalesModel.getBill_no()));
							reportModal.setReport_for("sales");
							reportList.add(reportModal);
						}
						
						//vat
						if(Double.parseDouble(stockSalesModel.getVat()) > 0)
						{
							reportModal=new StockSalesReportModel();
							reportModal.setName("");
							reportModal.setQuantity("");
							reportModal.setRate("Vat("+txtVatPercentage.getText()+"%)");
							reportModal.setRpt_order("");
							reportModal.setSales_id(0);
							reportModal.setTotal(txtVat.getText());
							reportModal.setTrxn_dt(bcmbSelDate.getText());
							reportModal.setUnit("");
							reportModal.setBill_no(String.valueOf(stockSalesModel.getBill_no()));
							reportModal.setReport_for("sales");
							reportList.add(reportModal);
						}
						
						//grandTotal
						reportModal=new StockSalesReportModel();
						reportModal.setName("");
						reportModal.setQuantity("");
						reportModal.setRate("Grand Total");
						reportModal.setRpt_order("");
						reportModal.setSales_id(0);
						reportModal.setTotal(txtGrandTotal.getText());
						reportModal.setTrxn_dt(bcmbSelDate.getText());
						reportModal.setUnit("");
						reportModal.setBill_no(String.valueOf(stockSalesModel.getBill_no()));
						reportModal.setReport_for("sales");
						reportList.add(reportModal);
						
						
						//paid amount
						reportModal=new StockSalesReportModel();
						reportModal.setName("");
						reportModal.setQuantity("");
						reportModal.setRate("Paid Amount");
						reportModal.setRpt_order("");
						reportModal.setSales_id(0);
						reportModal.setTotal(txtPaidAmount.getText());
						reportModal.setTrxn_dt(bcmbSelDate.getText());
						reportModal.setUnit("");
						reportModal.setBill_no(String.valueOf(stockSalesModel.getBill_no()));
						reportModal.setReport_for("sales");
						reportList.add(reportModal);
						
						//deu amount
						reportModal=new StockSalesReportModel();
						reportModal.setName("");
						reportModal.setQuantity("");
						reportModal.setRate("Deu Amount");
						reportModal.setRpt_order("");
						reportModal.setSales_id(0);
						reportModal.setTotal(txtDueAmount.getText());
						reportModal.setTrxn_dt(bcmbSelDate.getText());
						reportModal.setUnit("");
						reportModal.setBill_no(String.valueOf(stockSalesModel.getBill_no()));
						reportModal.setReport_for("sales");
						reportList.add(reportModal);
					}
					else
					{
						reportModal=new StockSalesReportModel();
						reportModal.setName(Item[i].getText(4));
						reportModal.setQuantity(Item[i].getText(11));
						reportModal.setRate(Item[i].getText(12));
						reportModal.setRpt_order(String.valueOf(j));
						reportModal.setSales_id(0);
						reportModal.setTotal(Item[i].getText(13));
						reportModal.setTrxn_dt(bcmbSelDate.getText());
						reportModal.setUnit(Item[i].getText(9));
						reportModal.setBill_no(String.valueOf(stockSalesModel.getBill_no()));
						reportModal.setReport_for("sales");
						reportList.add(reportModal);
					}
					
					//report ends
					j++;
				}
				
				
				
				
			}
			
			stockSalesModel.setService_income(UtilFxn.getDoubleExpFormated(serviceEarnedTot));
			stockSalesModel.setService_expense(UtilFxn.getDoubleExpFormated(servicePaidTot));
			
			//for daybook details
			model2=new DayBookDetailsModel();
			model2.setTrxn_with_id(1);
			model2.setTrxn_with_type_id(313);
			model2.setIncome(serviceEarnedTot);
			model2.setExpense(0);
			model2.setCreated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
			model2.setCreated_dt(jcalFxn.currentDate());
			//model2.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
			model2.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
			model2.setUpdt_cnt(0);
			model2.setUpdt_dt(jcalFxn.currentDate());
			
			dayBookDetailList.add(model2);
			
			//for daybook details
			model2=new DayBookDetailsModel();
			model2.setTrxn_with_id(1);
			model2.setTrxn_with_type_id(314);
			model2.setIncome(0);
			model2.setExpense(servicePaidTot);
			model2.setCreated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
			model2.setCreated_dt(jcalFxn.currentDate());
			//model2.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
			model2.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
			model2.setUpdt_cnt(0);
			model2.setUpdt_dt(jcalFxn.currentDate());
			
			dayBookDetailList.add(model2);
			
			//for acc head payments
			if (tblSourceAmount.getItemCount() > 0) {
				TableItem[] Item = tblSourceAmount.getItems();
				for (int i = 0; i < tblSourceAmount.getItemCount(); i++) {
					SalesTrxnModel model=new SalesTrxnModel();
					model.setTrxn_amt(UtilFxn.getDoubleExpFormated(Item[i].getText(2)));
					model.setSource(Item[i].getText(3).equals("Cash")?"Cash":"Bank");
					model.setTrxn_dt(bcmbSelDate.getText());
					model.setBank_name(Item[i].getText(3));
					model.setVno(Item[i].getText(4));
					model.setBank_acc_code(Item[i].getText(0));
					model.setCrtd_dt(jcalFxn.currentDate());
					model.setSales_trxn_id(Integer.parseInt(Item[i].getText(5)));
					sourceList.add(model);
					
					//for daybook details
					model2=new DayBookDetailsModel();
					model2.setTrxn_with_id(1);
					model2.setTrxn_with_type_id(Integer.parseInt(model.getBank_acc_code()));
					model2.setIncome(Double.parseDouble(model.getTrxn_amt()));
					model2.setExpense(0);
					model2.setCreated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
					model2.setCreated_dt(jcalFxn.currentDate());
					//model2.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
					model2.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
					model2.setUpdt_cnt(0);
					model2.setUpdt_dt(jcalFxn.currentDate());
					
					dayBookDetailList.add(model2);
					
					
					
				}
			}
			
			//paisa ako sabai debit
			
		}
		
		private void clearAll()
		{
			clearCashBank();
			clearItemDetails();
			clearServiceDetails();
			tblItem.removeAll();
			tblSourceAmount.removeAll();
			bcmbCustomer.setSelectedBoundValue("0");
			txtBillNo.setText("");
			bcmbSelDate.setValue(new Date());
			bcmbBillType.select(0);
			bcmbDeposite.select(0);
			txtItemTotal.setText("");
			txtDiscountPercentage.setText("");
			txtDueAmount.setText("");
			txtItemDiscount.setText("");
			txtVat.setText("");
			txtVatPercentage.setText("");
			txtGrandTotal.setText("");
			txtPaidAmount.setText("");
			txtRemarks.setText("");
		}
		
		private void loadEditData(int id)
		{
			if(id > 0)
			{
				this.action="edit";
				
				loadStockDetails(id);
				loadServiceValues(id);
				loadTrxnDetails(id);
				loadStockInfo(id);
				calculateTotalAmount();
			}
			
		}
		
		//load stock info
		private void loadStockInfo(int id)
		{
			stockSalesOldModel=control.getSalesInfoById(id);
			if(stockSalesOldModel != null)
			{
				bcmbCustomer.setSelectedBoundValue(stockSalesOldModel.getCustomer_id());
				txtBillNo.setText(String.valueOf(stockSalesOldModel.getBill_no()));
				txtBillNo.setEnabled(false);
				 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				 try {
						bcmbSelDate.setValue(sdf.parse(stockSalesOldModel.getTrxn_dt()));
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				bcmbBillType.setSelectedBoundValue(stockSalesOldModel.getBilling_type());
				txtRemarks.setText(stockSalesOldModel.getRemarks());
				txtGrandTotal.setText(UtilFxn.getFormatedAmount(Double.valueOf(stockSalesOldModel.getTotal_amt())));
				
				
				if(Double.valueOf(stockSalesOldModel.getDiscount()) > 0)
				{
					txtItemDiscount.setText(UtilFxn.getFormatedAmount(Double.valueOf(stockSalesOldModel.getDiscount())));
					
					txtDiscountPercentage.setText(stockSalesOldModel.getDiscount_percentage());
					
				}
				if(Double.valueOf(stockSalesOldModel.getVat()) > 0)
				{
					txtVat.setText(UtilFxn.getFormatedAmount(Double.valueOf(stockSalesOldModel.getVat())));
					txtVatPercentage.setText(stockSalesOldModel.getVat_percentage());
				}
				txtPaidAmount.setText(UtilFxn.getFormatedAmount(Double.valueOf(stockSalesOldModel.getPaid_amt())));
				txtDueAmount.setText(UtilFxn.getFormatedAmount(Double.valueOf(stockSalesOldModel.getTotal_amt())-Double.valueOf(stockSalesOldModel.getPaid_amt())));
			}
		}
		//edit stock details values from db
		private void loadStockDetails(int id)
		{
			StockSalesDetailsModel model;
			ArrayList<StockSalesDetailsModel> list=control.getItemsList(id);
			for (int i = 0; i < list.size(); i++) {
				model=new StockSalesDetailsModel();
				model=list.get(i);
				TableItem item=new TableItem(tblItem, SWT.NONE);
				
				double equivalence=0;
				double uniavail=0;
				double avai=0;
				ResultSet rs=itemControl.fetchItemRemainingDetailsById(model.getItem_id());
				try {
					if(UtilFxn.getRowCount(rs)> 0)
					{
						if(rs.next())
						{
							
							equivalence=rs.getDouble("sel_unit_equ");
							uniavail=rs.getDouble("remaining_unit_total");							
							avai=uniavail/equivalence;
							
						}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				item.setText(0, String.valueOf(model.getItem_id()));
			
				item.setText(1,String.valueOf(equivalence));
				item.setText(2,String.valueOf(tblItem.getItemCount()));
				item.setText(3,String.valueOf(UtilFxn.GetValueFromTable("inven_item", "item_code", "item_id='"+model.getItem_id()+"'")));
				item.setText(4,String.valueOf(UtilFxn.GetValueFromTable("inven_item", "item_name", "item_id='"+model.getItem_id()+"'")));
				item.setText(5,String.valueOf(UtilFxn.GetValueFromTable("inven_item", "item_rack_no", "item_id='"+model.getItem_id()+"'")));
				item.setText(6,"-");
				item.setText(7,"-");
				item.setText(8,"-");
				item.setText(9,String.valueOf(UtilFxn.GetValueFromTable("inven_unit", "unit_name", "unit_id='"+model.getUnit_id()+"'")));
				item.setText(10,UtilFxn.getFormatedInt(avai));
				item.setText(11,model.getQuantity());
				item.setText(12,UtilFxn.getFormatedAmount(Double.parseDouble(model.getRate())));
				item.setText(13,UtilFxn.getFormatedAmount(Double.parseDouble(model.getTotal())));
				item.setText(14,String.valueOf(model.getUpdt_cnt()));
				item.setText(15,"sales");
				item.setText(16,String.valueOf(model.getUnit_id()));
				item.setText(17,String.valueOf(model.getStock_out_det_id()));
			}
		}
		
		
		//load edit service details from db
		private void loadServiceValues(int id)
		{
			ServiceTrxnModel model;
			ArrayList<ServiceTrxnModel> list=control.getServiceList(id);
			for (int i = 0; i < list.size(); i++) {
				model=new ServiceTrxnModel();
				model=list.get(i);
				TableItem item=new TableItem(tblItem, SWT.NONE);
				
				ResultSet rs=serviceControl.getServiceDetails(model.getService_id());
				String serviceCode="";
				int serviceType=0;
				try {
					if(UtilFxn.getRowCount(rs) > 0)
					{
						if(rs.next())
						{
							
							serviceCode=rs.getString("service_code")==null?"":rs.getString("service_code");
							serviceType=rs.getInt("service_type");
						}
		
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				item.setText(0, String.valueOf(model.getService_id()));
				item.setText(1,String.valueOf(model.getService_provider_id()));
				item.setText(2,String.valueOf(tblItem.getItemCount()));
				item.setText(3,serviceCode.equals("")?"-":serviceCode);
				item.setText(4,String.valueOf(UtilFxn.GetValueFromTable("inven_service", "service_name", "service_id='"+model.getService_id()+"'")));
				item.setText(5,"-");
				String service_provider=String.valueOf(UtilFxn.GetValueFromTable("inven_customer", "name", "customer_id='"+model.getService_provider_id()+"'"));
				item.setText(6,service_provider.equals("")?"-":service_provider);
				item.setText(7,UtilFxn.getFormatedAmount(Double.parseDouble(model.getPerc_earned())));
				item.setText(8,UtilFxn.getFormatedAmount(Double.parseDouble(model.getPerc_paid())));
				item.setText(9,"-");
				item.setText(10,"-");
				item.setText(11,"1");
				item.setText(12,UtilFxn.getFormatedAmount(Double.parseDouble(model.getSelling_cost())));
				item.setText(13,UtilFxn.getFormatedAmount(Double.parseDouble(model.getSelling_cost())));
				item.setText(14,String.valueOf(0));
				item.setText(15,"service");
				item.setText(16,String.valueOf(serviceType));
				item.setText(17,String.valueOf(model.getService_trxn_id()));
			}
		}
		
		private void loadTrxnDetails(int id)
		{
			ArrayList<SalesTrxnModel> list=control.getTrxnList(id);
			SalesTrxnModel model;
			for (int i = 0; i < list.size(); i++) {
				model=new SalesTrxnModel();
				model=list.get(i);
				TableItem item=new TableItem(tblSourceAmount, SWT.NONE);
				
				item.setText(0, model.getBank_acc_code());
				item.setText(1, String.valueOf(i+1));
				item.setText(2,UtilFxn.getFormatedAmount(Double.parseDouble(model.getTrxn_amt())));
				item.setText(3,model.getBank_name());
				item.setText(4,model.getVno());
				item.setText(5, String.valueOf(model.getSales_trxn_id()));
			}
		}
}

