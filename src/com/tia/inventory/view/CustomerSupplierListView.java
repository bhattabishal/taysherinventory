package com.tia.inventory.view;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.eclipse.osgi.framework.internal.core.Msg;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.util.Banner;
import com.tia.common.util.BeforeSaveMessages;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;

import com.tia.inventory.controller.CustomerSupplierController;
import com.tia.inventory.model.CustomerModelClass;

public class CustomerSupplierListView extends ViewPart {

	private Composite comParent;
	private Composite comMainContainer;
	private Table tblCustomerList;
	private TableColumn tblclmnSno;
	private TableColumn tblclmnCustomerid;
	private TableColumn tblclmnName;
	private TableColumn tblclmnPhone;
	private TableColumn tblclmnContactPerson;
	private TableColumn tblclmnCellNo;
	private TableColumn tblclmnEmail;
	private TableColumn tblclmnMobile;
	private TableColumn tblclmnEmail_1;
	private TableColumn tblclmnTypeid;
	private static CustomerSupplierListView listCusSupView;
private CustomerSupplierController
	 control=new CustomerSupplierController();
private BeforeSaveMessages msg=new BeforeSaveMessages();

	public CustomerSupplierListView() {
		setTitleImage(SWTResourceManager.getImage(StockPurchasedListView.class, "/customer16.png"));
listCusSupView=this;
	}
	public static CustomerSupplierListView getInstance() {
		return listCusSupView;
	}
	@Override
	public void createPartControl(Composite parent) {

		/*
		 * The top-most composite that holds title bar and the comMainContainer
		 * composite together.
		 */
		comParent = parent;
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("Customer/Supplier/Service Provider Management", comParent,
				StockPurchasedListView.class);

		// Main container that holds all other controls/widgets of the view
		comMainContainer = new Composite(comParent, SWT.NONE);
		comMainContainer.setLayout(new FormLayout());
		GridData gd_comMainContainer = new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1);
		gd_comMainContainer.widthHint = 983;
		gd_comMainContainer.heightHint = 561;
		comMainContainer.setLayoutData(gd_comMainContainer);

		// Table for displaying the current list of customers in the database
		tblCustomerList = new Table(comMainContainer, SWT.BORDER
				| SWT.FULL_SELECTION);
		tblCustomerList.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {

				TableItem[] items = tblCustomerList.getSelection();

				/*
				 * Get the customer id from the second column which is hidden.
				 */
				int custId = Integer.parseInt(items[0].getText(1));

				

			}

		});
		FormData fd_table = new FormData();
		fd_table.bottom = new FormAttachment(100, -30);
		fd_table.top = new FormAttachment(0, 43);
		fd_table.left = new FormAttachment(0, 10);
		fd_table.right = new FormAttachment(0, 973);
		tblCustomerList.setLayoutData(fd_table);
		tblCustomerList.setHeaderVisible(true);
		tblCustomerList.setLinesVisible(true);

		// Table column customerId
		tblclmnCustomerid = new TableColumn(tblCustomerList, SWT.NONE);
	
		tblclmnCustomerid.setText("customerID");
		tblclmnCustomerid.setWidth(0);
		tblclmnCustomerid.setResizable(false);
		
		tblclmnTypeid = new TableColumn(tblCustomerList, SWT.NONE);
		tblclmnTypeid.setText("type");
		tblclmnTypeid.setWidth(0);
		tblclmnTypeid.setResizable(false);
		
		// Table column S.No.
		tblclmnSno = new TableColumn(tblCustomerList, SWT.NONE);
		tblclmnSno.setWidth(46);
		tblclmnSno.setText("S.No.");

		

		// Table column Name
		tblclmnName = new TableColumn(tblCustomerList, SWT.NONE);
		tblclmnName.setWidth(124);
		tblclmnName.setText("Type");

		// Table column Phone
		tblclmnPhone = new TableColumn(tblCustomerList, SWT.NONE);
		tblclmnPhone.setWidth(120);
		tblclmnPhone.setText("Name");

		// Table column Contact Person
		tblclmnContactPerson = new TableColumn(tblCustomerList, SWT.NONE);
		tblclmnContactPerson.setWidth(204);
		tblclmnContactPerson.setText("Address");

		// Table column Cell No.
		tblclmnCellNo = new TableColumn(tblCustomerList, SWT.NONE);
		tblclmnCellNo.setWidth(137);
		tblclmnCellNo.setText("Phone");

		// Table column Email
		tblclmnEmail = new TableColumn(tblCustomerList, SWT.NONE);
		tblclmnEmail.setWidth(125);
		tblclmnEmail.setText("Contact Person");
		
		tblclmnMobile = new TableColumn(tblCustomerList, SWT.NONE);
		tblclmnMobile.setWidth(100);
		tblclmnMobile.setText("Mobile");
		
		tblclmnEmail_1 = new TableColumn(tblCustomerList, SWT.NONE);
		tblclmnEmail_1.setWidth(100);
		tblclmnEmail_1.setText("Email");
		
		Composite composite = new Composite(comMainContainer, SWT.NONE);
		FormData fd_composite = new FormData();
		fd_composite.top = new FormAttachment(0);
		fd_composite.left = new FormAttachment(0);
		fd_composite.right = new FormAttachment(0, 258);
		fd_composite.bottom = new FormAttachment(0, 37);
		composite.setLayoutData(fd_composite);
		
		Button btnNew = new Button(composite, SWT.NONE);
		btnNew.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				openView(0);
			}
		});
		btnNew.setBounds(10, 10, 75, 25);
		btnNew.setText("New");
		
		Button btnEdit = new Button(composite, SWT.NONE);
		btnEdit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tblCustomerList.getSelectionIndex() < 0)
				{
					msg.showIndividualMessage("No row selected to edit", 0);
				}
				else
				{
					String trxnId = tblCustomerList.getSelection()[0].getText(0);
					openView(Integer.valueOf(trxnId));
					return;
				}
				
			}
		});
		btnEdit.setBounds(91, 10, 75, 25);
		btnEdit.setText("Edit");
		
		Button btnDelete = new Button(composite, SWT.NONE);
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tblCustomerList.getSelectionIndex() < 0)
				{
					msg.showIndividualMessage("No row selected to delete", 0);
				}
				else
				{
					String trxnId = tblCustomerList.getSelection()[0].getText(0);
					if(control.remove(Integer.valueOf(trxnId)))
					{
						msg.showIndividualMessage("Selected item deleted successfully", 0);
					}
					else
					{
						msg.showIndividualMessage("Some error occured while deleting.Please try again", 0);
					}
				}
			}
		});
		btnDelete.setBounds(172, 10, 75, 25);
		btnDelete.setText("Delete");

		fillTable();

	}

	private void openView(int id)
	{
		String form="main";
		CustomerManagementView hms = new CustomerManagementView(
				PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell(),id,form);
		hms.open();
	}
	
	public void refershTable()
	{
		this.fillTable();
	}
	private void fillTable()
	{
		ArrayList<CustomerModelClass> list=control.getAll();
		tblCustomerList.removeAll();
		int i= 1;
		if(list != null)
		{
			for (Iterator<CustomerModelClass> iterator = list.iterator(); iterator
			.hasNext();) 
			{
				CustomerModelClass trxn = iterator.next();
				TableItem item = new TableItem(tblCustomerList, SWT.NONE);
				item.setText(0, String.valueOf(trxn.getCustomer_id()));
				item.setText(1,String.valueOf(trxn.getType()));
				item.setText(2,String.valueOf(i));
				item.setText(3,String.valueOf(UtilFxn.GetValueFromTable("code_value_mcg", "cv_lbl", "cv_type='cusuptype' AND cv_code='"+trxn.getType()+"'")));
				item.setText(4,String.valueOf(trxn.getName()));
				item.setText(5,trxn.getAddress());
				item.setText(6,trxn.getPhone());
				item.setText(7,trxn.getContact_person());
				item.setText(8,trxn.getMobile());
				item.setText(9,trxn.getEmail());
			}
		}
	}
	
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}
}

