package com.tia.inventory.view;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.controller.AccHeadController;
import com.tia.account.controller.LedgerController;
import com.tia.account.model.AccHeadModel;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.BeforeSaveMessages;
import com.tia.common.util.CmbQry;
import com.tia.common.util.DecimalTextPositiveOnly;
import com.tia.common.util.LoginCheck;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.inventory.controller.StockCategoryController;
import com.tia.inventory.model.StockCategoryModel;
import com.tia.plugins.DynaTree.controller.DynaTree;
import com.tia.plugins.DynaTree.model.TreeNode;
import com.tia.plugins.DynaTree.ui.TreeNodeLabelProvider;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;

import org.eclipse.swt.widgets.Combo;

public class StockCategoryView extends ViewPart {
	private Text txtParent;
	private Text txtRemark;
	private Text txtParentId;
	private Composite compositeTree;
	private Composite comMain;
	private Composite comBody;
	private Composite comButton;
	private Button cmdNew;
	private Button cmdSave;
	private Button cmdEdit;
	private Button cmdCancel;
	private Button cmdDelete;
	private Tree tree;
	private Tree rptTree;
	private DynaTree dynaTree;
	private String oldLogData = "";
	private Shell shell;
	private int groupId = 0;
	private int parentGroupId = 0;
	private int deep = 0;
	private String lineage = "";
	public static String action = "";

	protected TreeViewer treeViewer;
	protected TreeNode root;
	protected TreeNodeLabelProvider labelProvider;
	protected Action onlyBoardGamesAction, atLeatThreeItems;
	protected Action booksBoxesGamesAction, noArticleAction;
	protected Action addTreeNodeAction, removeTreeNodeAction, addLeafAction,
			removeLeafAction;
	protected ViewerFilter onlyBoardGamesFilter, atLeastThreeFilter;
	protected ViewerSorter booksBoxesGamesSorter, noArticleSorter;

	private static java.util.List<AccHeadModel> listHead;

	int maxSubAccId = 0;
	StockCategoryController objController;
	StockCategoryModel objModel;
	BeforeSaveMessages msg = new BeforeSaveMessages();
	private Text txtName;
	private SwtBndCombo bcmbIcomeAcc;
	private SwtBndCombo bcmbAssetsAcc;
	private SwtBndCombo bcmbStockType;
	private SwtBndCombo bcmbStatus;

	/**
	 * Default Constructor
	 */
	public StockCategoryView() {
		objController = new StockCategoryController();
		objModel = new StockCategoryModel();

		shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();

		setTitleImage(SWTResourceManager.getImage(StockCategoryView.class,
				"/user16.png"));

	}

	@Override
	public void createPartControl(Composite parent) {
		// set the Gridlayout to the main form
		GridLayout gl_parent1 = new GridLayout(1, false);
		gl_parent1.marginHeight = 1;
		parent.setLayout(gl_parent1);
		Banner.getTitle("Stock Category", parent, StockCategoryView.class);

		// Main composite holding other controls and set Grid Layout to it too
		comMain = new Composite(parent, SWT.NONE);
		GridData gd_comMain = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_comMain.heightHint = 613;
		gd_comMain.widthHint = 948;
		comMain.setBounds(5, 5, 970, 590);
		comMain.setLayout(null);
		comMain.setLayoutData(gd_comMain);

		// composite for holding the tree and set the Grid Layout
		compositeTree = new Group(comMain, SWT.NONE);
		compositeTree.setBounds(5, 10, 392, 503);

		// tree displaying the account heads
		treeViewer = new TreeViewer(compositeTree);
		tree = treeViewer.getTree();

		tree.setBounds(10, 18, 372, 474);

		/*
		 * Initially had a problem displaying the tree on the composite. This
		 * problem was solved by implementing the form layout for the Tree
		 * instance i.e. tree in this case Same method followed in other forms
		 * as well.
		 */
		// the compositeTree composite has Form layout set to it.
		FormData fd_tree = new FormData();
		fd_tree.top = new FormAttachment(0, 10);
		fd_tree.bottom = new FormAttachment(100, -10);
		fd_tree.right = new FormAttachment(100, -10);
		fd_tree.left = new FormAttachment(0, 10);
		tree.setLayoutData(fd_tree);

		rptTree = new Tree(compositeTree, SWT.BORDER | SWT.H_SCROLL
				| SWT.V_SCROLL);
		// treeJournalList.setFont(CommonFunctions.getFont());
		rptTree.setBounds(10, 20, 359, 435);
		rptTree.setHeaderVisible(true);
		rptTree.setLinesVisible(true);
		// treeJournalList.redraw();

		TreeColumn[] col = new TreeColumn[9];

		int[][] intAlign = { { 0, 40 }, { 0, 40 }, { 0, 80 }, { 0, 40 },
				{ 0, 80 } };
		String[] colNames = { "", "Account Code", "Account Alias",
				"Account Code", "Type" };
		for (int i = 0; i < colNames.length; i++) {
			if (intAlign[i][0] == 0)
				col[i] = new TreeColumn(rptTree, SWT.NONE);

			col[i].setResizable(true);
			col[i].setWidth(intAlign[i][1]);
			col[i].setText(colNames[i]);
		}
		// fillRptTree();//fill the tree just before printing report to make
		// initial loading time of view faster

		// now, create the dynamic tree
		createTree();

		// composite for the Body
		comBody = new Group(comMain, SWT.NONE);
		comBody.setBounds(415, 10, 496, 503);
		comBody.setLayout(null);
		Composite comForm = new Group(comBody, SWT.NONE);
		comForm.setBounds(24, 21, 451, 269);
		comForm.setLayout(null);

		// label for account code (AccCode)
		Label labAccCode = new Label(comForm, SWT.NONE);
		labAccCode.setAlignment(SWT.RIGHT);
		labAccCode.setBounds(19, 21, 92, 18);
		labAccCode.setText("Parent");

		// textbox and settings for account code
		txtParent = new Text(comForm, SWT.BORDER);
		txtParent.setBounds(117, 16, 314, 24);

		txtParent.setEditable(false);

		// label for parent account
		Label labParent = new Label(comForm, SWT.NONE);
		labParent.setAlignment(SWT.RIGHT);
		labParent.setBounds(6, 50, 105, 18);
		labParent.setText("Name");
		labParent.setForeground(UtilMethods.getFontForegroundColorRed());

		// label for alias
		Label lblAlias = new Label(comForm, SWT.NONE);
		lblAlias.setAlignment(SWT.RIGHT);
		lblAlias.setBounds(19, 76, 92, 18);
		lblAlias.setText("Stock Type");
		// lblAlias.setFont(CommonFunctions.getFontLabel());
		lblAlias.setForeground(UtilMethods.getFontForegroundColorRed());

		// label for account name
		Label lblAccName = new Label(comForm, SWT.NONE);
		lblAccName.setAlignment(SWT.RIGHT);
		lblAccName.setBounds(11, 165, 100, 18);
		lblAccName.setText("Status");
		// lblAccName.setFont(CommonFunctions.getFontLabel());
		lblAccName.setForeground(UtilMethods.getFontForegroundColorRed());

		// label for remarks
		Label lblRemark = new Label(comForm, SWT.NONE);
		lblRemark.setAlignment(SWT.RIGHT);
		lblRemark.setBounds(26, 202, 85, 18);
		lblRemark.setText("Remarks");

		// textbox for remarks
		txtRemark = new Text(comForm, SWT.BORDER | SWT.MULTI);
		txtRemark.setBounds(117, 191, 314, 59);

		txtName = new Text(comForm, SWT.BORDER);
		txtName.setBounds(117, 47, 314, 21);

		Label lblIncomeAccount = new Label(comForm, SWT.NONE);
		lblIncomeAccount.setAlignment(SWT.RIGHT);
		lblIncomeAccount.setBounds(19, 106, 92, 15);
		lblIncomeAccount.setText("Income Account");
		lblIncomeAccount.setForeground(UtilMethods.getFontForegroundColorRed());

		bcmbIcomeAcc = new SwtBndCombo(comForm, SWT.READ_ONLY);
		bcmbIcomeAcc.setBounds(117, 103, 314, 23);
		bcmbIcomeAcc.fillData(CmbQry.getAccHeads("3"), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection());

		Label lblAssetsAccount = new Label(comForm, SWT.NONE);
		lblAssetsAccount.setAlignment(SWT.RIGHT);
		lblAssetsAccount.setBounds(19, 135, 92, 15);
		lblAssetsAccount.setText("Assets Account");
		lblAssetsAccount.setForeground(UtilMethods.getFontForegroundColorRed());

		bcmbAssetsAcc = new SwtBndCombo(comForm, SWT.READ_ONLY);
		bcmbAssetsAcc.setBounds(117, 132, 314, 23);
		bcmbAssetsAcc.fillData(CmbQry.getAccHeads("1"), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection());

		bcmbStockType = new SwtBndCombo(comForm, SWT.READ_ONLY);
		bcmbStockType.setBounds(117, 74, 314, 23);
		bcmbStockType.fillData(CmbQry.getStockType(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), false);

		bcmbStatus = new SwtBndCombo(comForm, SWT.READ_ONLY);
		bcmbStatus.setBounds(117, 162, 314, 23);
		bcmbStatus.fillData(CmbQry.getStatus(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), false);

		// composite that holds the buttons with Grid Layout
		comButton = new Group(comBody, SWT.NONE);
		comButton.setBounds(24, 306, 451, 46);
		comButton.setLayout(null);

		cmdNew = new Button(comButton, SWT.NONE);
		cmdNew.setBounds(7, 13, 65, 25);

		// widget selected code for New button
		cmdNew.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (!LoginCheck.isLogIn()) {
					LoginCheck.showMessage();
					return;
				}
				if (ApplicationWorkbenchWindowAdvisor
						.hasAuthOr(new String[] { "cat_stock_add" })) {
					action = "add";
					cmdNewActionPerformed();
				}

			}
		});
		cmdNew.setText("New");

		// Save button
		cmdSave = new Button(comButton, SWT.NONE);
		cmdSave.setBounds(79, 13, 65, 25);

		// widget selected code for Save button
		cmdSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (!LoginCheck.isLogIn()) {
					LoginCheck.showMessage();
					return;
				}
				cmdSaveActionPerformed();
			}
		});
		cmdSave.setText("Save");

		cmdSave.setEnabled(false);

		// Edit button
		cmdEdit = new Button(comButton, SWT.NONE);
		cmdEdit.setBounds(151, 13, 65, 25);
		cmdEdit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (!LoginCheck.isLogIn()) {
					LoginCheck.showMessage();
					return;
				}
				if (ApplicationWorkbenchWindowAdvisor
						.hasAuthOr(new String[] { "cat_stock_update" })) {
					action = "edit";
					if (groupId > 0) {
						cmdSave.setEnabled(true);
						cmdEdit.setEnabled(false);
						cmdCancel.setEnabled(true);
					}else {
						
						msg.showIndividualMessage(
								"Please Select Category From The Tree Item To Edit",
								0);
					}
				}
				
			}
		});
		cmdEdit.setText("Edit");

		// Delete button
		cmdDelete = new Button(comButton, SWT.NONE);
		cmdDelete.setBounds(223, 13, 65, 25);

		// widget selected code for delete button
		cmdDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (!LoginCheck.isLogIn()) {
					LoginCheck.showMessage();
					return;
				}
				if (ApplicationWorkbenchWindowAdvisor
						.hasAuthOr(new String[] { "cat_stock_update" })) {
					if (groupId > 0) {
						if (msg.showIndividualMessage(
								"Are You Sure You Want To Delete The Selected Category?",
								2)) {
							if (objController.checkIfHasChild(groupId)) {
								msg.showIndividualMessage(
										"The Selected Category Cannot Be Deleted.It Has Child Category.",
										1);
							}
							else
							{
								if (objController.hasMember(groupId)) {
									msg.showIndividualMessage(
											"The Selected Category Cannot Be Deleted.It Has Stock Items.",
											1);
								}
								else
								{
									if (deleteAction()) {
										msg.showIndividualMessage(
												"Selected Record Has Been Successfully Deleted",
												0);
										dynaTree.removeSelected();
										groupId = 0;
										parentGroupId = 0;
									}
									else
									{
										msg.showIndividualMessage(
												"Some Error Occured While Deleting The Record.Please Try Again",
												1);
									}
								}
							}
							
						}
					}
					else
						msg.showIndividualMessage(
								"Please Select Category To Delete", 1);
				}
			}
		});
		cmdDelete.setText("Delete");

		// Cancel button
		cmdCancel = new Button(comButton, SWT.NONE);
		cmdCancel.setBounds(296, 13, 65, 25);
		cmdCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clearAllFields();
				oldLogData = "";

				cmdNew.setEnabled(true);
				cmdSave.setEnabled(false);
				cmdEdit.setEnabled(true);
				cmdCancel.setEnabled(false);
				

				treeViewer.setSelection(null);
			}
		});
		cmdCancel.setText("Cancel");

		// hidden txtParentId
		txtParentId = new Text(comBody, SWT.BORDER);
		txtParentId.setBounds(115, 438, 404, 24);

		txtParentId.setEnabled(false);
		txtParentId.setEditable(false);
		txtParentId.setVisible(false);

		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		Label label = new Label(parent, SWT.NONE);
		label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false,
				1, 1));
	}// end of method createPartControl()

	private void createTree() {
		// now, create the dynamic tree
		dynaTree = new DynaTree(
				DbConnection.getConnection(),
				"SELECT cat_id AS id, cat_name AS label, parent_id AS parent, CAST((REPLACE(lineage,'-','')) AS float) AS sort, "
						+ "lineage AS code " + "FROM inven_stock_category ",
				"parent_id", "sort");
		treeViewer = dynaTree.createTree(treeViewer);
		labelProvider = new TreeNodeLabelProvider();
		// widget selection listener for the dynamic tree
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				// if the selection is empty, clear all the control fields
				labelProvider = new TreeNodeLabelProvider();
				if (event.getSelection().isEmpty()) {

					txtParent.setText("");
					txtName.setText("");
					bcmbAssetsAcc.setSelectedItem("0");
					bcmbIcomeAcc.setSelectedItem("0");
					bcmbStatus.setSelectedItem("0");
					bcmbStockType.setSelectedItem("0");
					txtRemark.setText("");
					return;
				}
				if (event.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) event
							.getSelection();
					clearAllFields();
					/*
					 * Only three things acc_id, acc_code and acc_name are
					 * provided by the TreeNodeLabelProvider.
					 */
					// StringBuffer for id(acc_head_id)
					StringBuffer catId = new StringBuffer();

					// StringBuffer for acc_code
					StringBuffer lineage = new StringBuffer();

					// StringBuffer for acc_name
					StringBuffer catName = new StringBuffer();

					for (Iterator iterator = selection.iterator(); iterator
							.hasNext();) {
						Object domain = iterator.next();
						String id = labelProvider.getId(domain);
						String code = labelProvider.getCode(domain);
						String name = labelProvider.getText(domain);

						// set the StringBuffers to the tree node values
						catId.append(id);
						lineage.append(code);
						catName.append(name);
					}
					// working here today--06-FRIDAY
					/*
					 * // remove the trailing comma space pair if
					 * (toShow.length() > 0) { toShow.setLength(toShow.length()
					 * - 2); }
					 */
					txtName.setText(catName.toString());
					groupId = Integer.valueOf(catId.toString());
					// txtId.setText(accName.toString());

					/*
					 * Now use the acc_id (id) to fetch other details from the
					 * database and load the values into the form fields.
					 */
					LoadData(catId.toString());

					/*
					 * While selecting a new node, Cancel button must be
					 * disabled, while New, Edit and Delete buttons must be
					 * enabled.
					 */
					cmdNew.setEnabled(true);
					cmdSave.setEnabled(false);
					cmdEdit.setEnabled(true);
					cmdDelete.setEnabled(true);
					cmdCancel.setEnabled(true);

				}
			}
		});
	}

	private void clearAllFields() {
		
		txtParent.setText("");
		if (groupId > 0) {
			txtParent.setText(txtName.getText());
			parentGroupId = groupId;
		} else {
			txtParent.setText("Root");
		}
		txtName.setText("");
		bcmbAssetsAcc.setSelectedBoundValue("0");
		bcmbIcomeAcc.setSelectedBoundValue("0");
		bcmbStatus.setSelectedBoundValue("1");
		bcmbStockType.setSelectedBoundValue("1");
		txtRemark.setText("");
		
	}

	private void LoadData(String catId) {
		ResultSet rs = objController.getCategory(catId);
		if (rs != null) {

			try {
				parentGroupId = rs.getInt("parent_id");
				txtParent.setText((rs.getString("parent_name")) != null ? rs
						.getString("parent_name") : "Root");
				txtName.setText(rs.getString("cat_name"));
				bcmbAssetsAcc
						.setSelectedBoundValue(rs.getString("assets_acc") == (null) ? "0"
								: rs.getString("assets_acc").toString());
				bcmbIcomeAcc
						.setSelectedBoundValue(rs.getString("income_acc") == (null) ? "0"
								: rs.getString("income_acc").toString());
				bcmbStockType.setSelectedBoundValue(rs
						.getString("cat_stock_type") == (null) ? "0" : rs
						.getString("cat_stock_type").toString());
				bcmbStatus
						.setSelectedBoundValue(rs.getString("cat_status") == (null) ? "0"
								: rs.getString("cat_status").toString());
				txtRemark.setText(rs.getString("remarks") == null ? "" : rs
						.getString("remarks"));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void cmdNewActionPerformed() {

		if (groupId > 0) {
			parentGroupId = groupId;

		}
		clearAllFields();
		if (parentGroupId > 0) {

			objModel = objController.fetchDetails(parentGroupId);
			bcmbAssetsAcc.setSelectedBoundValue(objModel.getAssets_acc());
			bcmbIcomeAcc.setSelectedBoundValue(objModel.getIncome_acc());
			bcmbStatus.setSelectedBoundValue(objModel.getCat_status());
			bcmbStockType.setSelectedBoundValue(objModel.getCat_stock_type());

			bcmbAssetsAcc.setEnabled(false);
			bcmbIcomeAcc.setEnabled(false);
			bcmbStockType.setEnabled(false);

			cmdNew.setEnabled(false);
			cmdSave.setEnabled(true);
			cmdEdit.setEnabled(false);
			cmdCancel.setEnabled(true);
		} else {
			
			cmdNew.setEnabled(false);
			cmdSave.setEnabled(true);
			cmdEdit.setEnabled(false);
			cmdCancel.setEnabled(true);

			bcmbAssetsAcc.setEnabled(true);
			bcmbIcomeAcc.setEnabled(true);
			bcmbStockType.setEnabled(true);
		}
	}

	private void cmdSaveActionPerformed() {
		beforeSave();
		if (msg.returnMessages()) {
			if (cmdSaveAction()) {
				cmdNew.setEnabled(true);
				cmdSave.setEnabled(false);
				cmdEdit.setEnabled(true);
				if (action == "add") {
					// mb.setMessage(Messages
					// .getMessage("GroupHasBeenSuccessfullyInserted"));
					msg.showIndividualMessage(
							"Category Has Been Successfully Created",
							0);
					// again create the tree with the added items if
					// any
					dynaTree.addNewTreeNode(
							Integer.toString(objModel.getCat_id()),
							objModel.getCat_name(), objModel.getLineage());
					// btnGenerateMeetingDate.setEnabled(false);
				} else if (action == "edit") {
					// mb.setMessage(Messages
					// .getMessage("GroupHasBeenSuccessfullyUpdated"));
					msg.showIndividualMessage(
							"Category Has Been Successfully Updated",
							0);
					// btnGenerateMeetingDate.setEnabled(false);
				}
				// mb.open();

				groupId = 0;
				parentGroupId = 0;
				lineage = "";
				deep = 0;
				clearAllFields();
				tree.deselectAll();
				
				IWorkbenchPage wbp = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		final IViewReference[] viewRefs = wbp.getViewReferences();
		for (int i = 0; i < viewRefs.length; i++) {
			if (viewRefs[i].getId().equalsIgnoreCase(
					"com.tia.inven.stockCat")) {
				IViewReference ivr = viewRefs[i];
				// close view first and reopen
				wbp.hideView(ivr);
				// then open it again
			}
		}
		// now reopen the view
		try {
			wbp.showView("com.tia.inven.stockCat");
		} catch (PartInitException e1) {
			e1.printStackTrace();
		}
				

			}
			else {
				
				msg.showIndividualMessage(
						"Some Error Occured While Saving Record.Please Try Again",
						1);
			}
		}
	}
	private boolean cmdSaveAction() {
		boolean success = false;
		Connection cnn = DbConnection.getConnection();
		try {
			cnn.setAutoCommit(false);
			setData();
			if (objController.save(objModel, action, cnn)) {
				success = true;
				cnn.setAutoCommit(true);
			}
			else {
				cnn.rollback();
				success = false;
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				cnn.rollback();
				success = false;
				cnn.setAutoCommit(true);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		
		return success;
		
	}
	private void beforeSave() {
		if (StringUtils.isBlank(txtName.getText())) {
			msg.setMandatoryMessages("Name is required");

		}
		if (StringUtils.isNotBlank(txtName.getText())) {
			if (action.equals("add")
					&& objController.fetchCheckByCondition("cat_name='"
							+ txtName.getText() + "'")) {
				msg.setMandatoryMessages("The Name You Have Given Already Exists.Please Enter Another Unique Name");

			}
			if (action.equals("edit")
					&& objController.fetchCheckByCondition("cat_name='"
							+ txtName.getText() + "' AND cat_id != '" + groupId
							+ "'")) {
				msg.setMandatoryMessages("The Name You Have Given Already Exists.Please Enter Another Unique Name");
			}
		}
		if (Integer.valueOf(bcmbStockType.getSelectedBoundValue().toString()) <= 0) {
			msg.setMandatoryMessages("Please Select The Stock Type");

		}
		if(bcmbIcomeAcc.getSelectedBoundValue().equals("0"))
		{
			msg.setMandatoryMessages("Please Select The Income Account");
		}
		if(bcmbAssetsAcc.getSelectedBoundValue().equals("0"))
		{
			msg.setMandatoryMessages("Please Select The Assets Account");
		}
		if (Integer.valueOf(bcmbStatus.getSelectedBoundValue().toString()) <= 0) {
			msg.setMandatoryMessages("Please Select The Status");

		}

	}
	/*
	 * setData method sets the object's value for insert or update
	 */
	private void setData() {
		if (action == "edit") {

			objModel.setCat_id(groupId);
			objModel.setLineage(lineage);
			objModel.setDeep(deep);
			System.out.println("The selected item's group id is :" + groupId);
		} else if (action == "add") {

			if (parentGroupId > 0) {
				objModel.setLineage(objController.calculateLineageDeep(
						parentGroupId, "lineage"));

				objModel.setDeep(Integer.valueOf(objController
						.calculateLineageDeep(parentGroupId, "deep")));
			} else {
				objModel.setLineage(String.valueOf(UtilMethods.getMaxId(
						"inven_stock_category", "cat_id")));
				objModel.setDeep(0);
			}
			// objModel.setGroup_id(UtilFxn.getMaxId("mem_group_mcg",
			// "group_id"));

		}

		

		
		objModel.setParent_id(parentGroupId);
		objModel.setAssets_acc(bcmbAssetsAcc.getSelectedBoundValue().toString());
		objModel.setIncome_acc(bcmbIcomeAcc.getSelectedBoundValue().toString());
		objModel.setBr_id(1);
		objModel.setCat_name(txtName.getText());
		objModel.setCat_status(Integer.valueOf(bcmbStatus.getSelectedBoundValue().toString()));
		objModel.setCat_stock_type(Integer.valueOf(bcmbStockType.getSelectedBoundValue().toString()));
		objModel.setCr_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		objModel.setCr_dt(JCalendarFunctions.currentDate());
		objModel.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		objModel.setUpdt_dt(JCalendarFunctions.currentDate());
		objModel.setUpdt_cnt(objModel.getUpdt_cnt()+1);
		objModel.setRemarks(txtRemark.getText());
		

		
	}
	private boolean deleteAction() {
		boolean deleted = false;
		if (groupId <= 0) {
			deleted = false;
		} else if (groupId > 0) {
			if (objController.remove(groupId)) {
				deleted = true;
			} else {
				deleted = false;
			}
		}
		return deleted;
	}

	@Override
	public void setFocus() {

	}
}// end of class

