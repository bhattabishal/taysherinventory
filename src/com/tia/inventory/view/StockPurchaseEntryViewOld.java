package com.tia.inventory.view;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.nebula.widgets.datechooser.DateChooserCombo;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.controller.JournalEntryController;
import com.tia.account.model.JVDetailsModel;
import com.tia.account.model.JVSummary;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.BeforeSaveMessages;
import com.tia.common.util.CmbQry;
import com.tia.common.util.DecimalTextPositiveOnly;
import com.tia.common.util.IntegerTextPositiveOnly;
import com.tia.common.util.SearchBox;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;

import com.tia.inventory.controller.StockItemController;
import com.tia.inventory.controller.StockPurchasedController;
import com.tia.inventory.model.StockPurchasedDetailsModel;
import com.tia.inventory.model.StockPurchasedModel;
import com.tia.inventory.model.StockPurchasedTrxnModel;
import com.tia.master.controller.UnitMapController;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import org.eclipse.swt.widgets.Combo;
import com.swtdesigner.SWTResourceManager;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;

public class StockPurchaseEntryViewOld extends Dialog {
	private Composite comParent;
	private Composite comMainContainer;

	private JCalendarFunctions jcalFxn;

	private int purchasedId=0;
	double totalAmount=0;
	double totalExpense=0;
	double itemEquivalence=0;
	int unit_id=0;
	String itemAction="add";
	String action="add";
	int jvNo=0;
	StockPurchasedModel stockPurchasedModel=new StockPurchasedModel();
	StockPurchasedDetailsModel stockPurchasedDetailsModel=new StockPurchasedDetailsModel();
	StockPurchasedTrxnModel stockPurchasedTrxnModel=new StockPurchasedTrxnModel();
	ArrayList<StockPurchasedDetailsModel> itemsList=new ArrayList<StockPurchasedDetailsModel>();
	ArrayList<StockPurchasedTrxnModel> sourceList=new ArrayList<StockPurchasedTrxnModel>();
	StockPurchasedController control=new StockPurchasedController();
	JVDetailsModel objJVDetails = new JVDetailsModel();
	JVSummary jvSummary = new JVSummary();
	ArrayList<JVDetailsModel> jvDetailList = new ArrayList<JVDetailsModel>();

	Object rtn;
	private Text txtBillNo;
	private Text txtWeight;
	private Text txtLUIndia;
	private Text txtLUBorder;
	private Text txtTaxClearence;
	private Text txtLUKtm;
	private Text txtLUShop;
	private Text txtTotalWeight;
	private Text txtUnit;
	private Text txtUnitQuantity;
	private Text txtRate;
	private Text txtActualRate;
	private Text txtTotal;
	private Table tblItem;
	private Text txtTotalAmount;
	private Text txtDueAmount;
	private Text txtRemarks;
	private Text txtCash;
	private Text txtBank;
	private Table tblSourceAmount;
	private SwtBndCombo bcmbSupplier;
	private SwtBndCombo bcmbBrand;
	private SwtBndCombo bcmbLocalBrandType;
	private SearchBox bcmbItem;
	private SwtBndCombo bcmbCash;
	private SwtBndCombo bcmbBank;
	private static StockPurchaseEntryViewOld stockPurchaseEntry;
	private DateChooserCombo bcmbPurDate;
	private Text txtOtherExpense;
	private Text txtSellingPrice;
	private BeforeSaveMessages msg=new BeforeSaveMessages();
	private Label lblBalanceAvailable;
	private Button btnCash;
	private Button btnBank;
	private Text txtItemTotal;
	private Text txtTotalExpense;
	private Text txtPaidAmount;

	public StockPurchaseEntryViewOld(Shell parentShell, int purId) {
		super(parentShell);
		

		jcalFxn = new JCalendarFunctions();
		this.stockPurchaseEntry=this;
		this.purchasedId = purId;
		
	}

	/**
	 * @wbp.parser.constructor
	 */
	public StockPurchaseEntryViewOld(Shell parentShell) {
		super(parentShell);
		

		jcalFxn = new JCalendarFunctions();
		
	}
	public static StockPurchaseEntryViewOld getInstance() {
		return stockPurchaseEntry;
	}

	public Object open() {
		Shell parent = getParent();
		Shell shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setText("Stock Purchased Entry");
		shell.setSize(1000, 680);
		// Your code goes here (widget creation, set result, etc).
		createDialogArea(shell);
		shell.open();
		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		return rtn;
	}

	// @Override
	@SuppressWarnings("deprecation")
	protected Control createDialogArea(Composite parent) {
		comParent = parent;// (Composite) super.createDialogArea(parent);
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("Stock Purchased Entry", comParent,
				StockPurchasedEntryView.class);

		// Main container that holds all other controls/widgets of the view
		comMainContainer = new Composite(comParent, SWT.NONE);
		comMainContainer.setLayout(new FormLayout());
		GridData gd_comMainContainer = new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1);
		gd_comMainContainer.widthHint = 983;
		gd_comMainContainer.heightHint = 615;
		comMainContainer.setLayoutData(gd_comMainContainer);
		
		Group grpStockInformation = new Group(comMainContainer, SWT.NONE);
		grpStockInformation.setText("Stock Information");
		FormData fd_grpStockInformation = new FormData();
		fd_grpStockInformation.top = new FormAttachment(0);
		fd_grpStockInformation.left = new FormAttachment(0, 10);
		fd_grpStockInformation.right = new FormAttachment(0, 973);
		grpStockInformation.setLayoutData(fd_grpStockInformation);
		
		Label lblSupplier = new Label(grpStockInformation, SWT.NONE);
		lblSupplier.setAlignment(SWT.RIGHT);
		lblSupplier.setBounds(10, 20, 121, 15);
		lblSupplier.setText("Supplier: ");
		lblSupplier.setForeground(UtilMethods.getFontForegroundColorRed());
		
		bcmbSupplier = new SwtBndCombo(grpStockInformation, SWT.READ_ONLY);
		bcmbSupplier.setBounds(131, 18, 135, 23);
		bcmbSupplier.fillData(CmbQry.getAllSuppliers(2), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		
		Button btnSupplierAdd = new Button(grpStockInformation, SWT.NONE);
		btnSupplierAdd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String form="stockpurchase";
				CustomerManagementView hms = new CustomerManagementView(
						PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getShell(),0,form);
				hms.open();
			}
		});
		btnSupplierAdd.setBounds(266, 17, 38, 25);
		btnSupplierAdd.setText("add");
		
		Label lblBillNo = new Label(grpStockInformation, SWT.NONE);
		lblBillNo.setAlignment(SWT.RIGHT);
		lblBillNo.setBounds(310, 20, 86, 15);
		lblBillNo.setText("Bill No: ");
		lblBillNo.setForeground(UtilMethods.getFontForegroundColorRed());
		
		txtBillNo = new Text(grpStockInformation, SWT.BORDER);
		txtBillNo.setBounds(396, 18, 76, 21);
		
		Label lblPurchasedDate = new Label(grpStockInformation, SWT.NONE);
		lblPurchasedDate.setAlignment(SWT.RIGHT);
		lblPurchasedDate.setBounds(494, 20, 105, 15);
		lblPurchasedDate.setText("Purchased Date: ");
		lblPurchasedDate.setForeground(UtilMethods.getFontForegroundColorRed());
		
		bcmbPurDate = new DateChooserCombo(grpStockInformation, SWT.NONE);
		bcmbPurDate.setBounds(601, 19, 86, 17);
		bcmbPurDate.setValue(new Date());
		
		Label lblItemBrand = new Label(grpStockInformation, SWT.NONE);
		lblItemBrand.setAlignment(SWT.RIGHT);
		lblItemBrand.setBounds(709, 20, 105, 15);
		lblItemBrand.setText("Item Purchased: ");
		lblItemBrand.setForeground(UtilMethods.getFontForegroundColorRed());
		
		bcmbBrand = new SwtBndCombo(grpStockInformation, SWT.READ_ONLY);
		bcmbBrand.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(bcmbBrand.getSelectedBoundValue().toString().equals("2"))
				{
					bcmbLocalBrandType.setEnabled(false);
				}
				else
				{
					bcmbLocalBrandType.setEnabled(true);
				}
			}
		});
		bcmbBrand.setBounds(816, 18, 135, 23);
		bcmbBrand.fillData(CmbQry.getStockBrand(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		
		Group grpStockDetails = new Group(comMainContainer, SWT.NONE);
		grpStockDetails.setText("Stock Details");
		FormData fd_grpStockDetails = new FormData();
		fd_grpStockDetails.right = new FormAttachment(grpStockInformation, 0, SWT.RIGHT);
		fd_grpStockDetails.left = new FormAttachment(0, 10);
		
		Label lblLocalPurchasedType = new Label(grpStockInformation, SWT.NONE);
		lblLocalPurchasedType.setBounds(10, 52, 121, 15);
		lblLocalPurchasedType.setText("Local Purchased Type: ");
		
		bcmbLocalBrandType = new SwtBndCombo(grpStockInformation, SWT.READ_ONLY);
		bcmbLocalBrandType.setBounds(131, 50, 135, 23);
		grpStockDetails.setLayoutData(fd_grpStockDetails);
		bcmbLocalBrandType.fillData(CmbQry.getLocalBrand(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		
		Label lblItem = new Label(grpStockDetails, SWT.NONE);
		lblItem.setAlignment(SWT.RIGHT);
		lblItem.setBounds(10, 20, 65, 15);
		lblItem.setText("Item: ");
		lblItem.setForeground(UtilMethods.getFontForegroundColorRed());
		
		bcmbItem = new SearchBox(grpStockDetails, SWT.NONE);
		bcmbItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(!bcmbItem.getSelectedBoundValue().toString().equals("0"))
				{
					clearItemDetails();
					loadItemDetails(Integer.valueOf(bcmbItem.getSelectedBoundValue().toString()));
				}
				else
				{
					clearItemDetails();
				}
			}
		});
		bcmbItem.setBounds(76, 18, 132, 23);
		bcmbItem.fillData(CmbQry.getStockItems(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		
		Button btnItemAdd = new Button(grpStockDetails, SWT.NONE);
		btnItemAdd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String form="stockpurchase";
				StockEntryView hms = new StockEntryView(
						PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getShell(),0,form);
				hms.open();
			}
		});
		btnItemAdd.setText("add");
		btnItemAdd.setBounds(214, 17, 38, 25);
		
		Label lblUnit = new Label(grpStockDetails, SWT.NONE);
		lblUnit.setAlignment(SWT.RIGHT);
		lblUnit.setBounds(309, 22, 32, 15);
		lblUnit.setText("Unit: ");
		
		txtUnit = new Text(grpStockDetails, SWT.BORDER | SWT.READ_ONLY);
		txtUnit.setBounds(341, 20, 120, 21);
		
		Label lblUnitQuantity_1 = new Label(grpStockDetails, SWT.NONE);
		lblUnitQuantity_1.setAlignment(SWT.RIGHT);
		lblUnitQuantity_1.setBounds(651, 20, 82, 15);
		lblUnitQuantity_1.setText("Unit Quantity: ");
		lblUnitQuantity_1.setForeground(UtilMethods.getFontForegroundColorRed());
		
		txtUnitQuantity = new Text(grpStockDetails, SWT.BORDER);
		txtUnitQuantity.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calculateItemTotal();
			}
		});
		txtUnitQuantity.setBounds(733, 18, 76, 21);
		txtUnitQuantity.addListener(SWT.Verify, new IntegerTextPositiveOnly(
				txtUnitQuantity, 20));
		
		Label lblRate = new Label(grpStockDetails, SWT.NONE);
		lblRate.setAlignment(SWT.RIGHT);
		lblRate.setBounds(822, 20, 55, 15);
		lblRate.setText("Rate: ");
		lblRate.setForeground(UtilMethods.getFontForegroundColorRed());
		
		txtRate = new Text(grpStockDetails, SWT.BORDER);
		txtRate.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calculateItemTotal();
			}
		});
		txtRate.setBounds(877, 18, 76, 21);
		txtRate.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtRate, 20));
		
		Label lblActualRate = new Label(grpStockDetails, SWT.NONE);
		lblActualRate.setBounds(10, 52, 65, 15);
		lblActualRate.setText("Actual Rate: ");
		
		txtActualRate = new Text(grpStockDetails, SWT.BORDER | SWT.READ_ONLY);
		txtActualRate.setBounds(76, 50, 132, 21);
		
		Label lblTotal = new Label(grpStockDetails, SWT.NONE);
		lblTotal.setAlignment(SWT.RIGHT);
		lblTotal.setBounds(492, 49, 55, 15);
		lblTotal.setText("Total: ");
		
		txtTotal = new Text(grpStockDetails, SWT.BORDER | SWT.READ_ONLY);
		txtTotal.setBounds(547, 47, 76, 21);
		
		Group grpPurchaseExpenses = new Group(comMainContainer, SWT.NONE);
		fd_grpStockDetails.top = new FormAttachment(grpPurchaseExpenses, 6);
		fd_grpStockInformation.bottom = new FormAttachment(100, -533);
		grpPurchaseExpenses.setText("Purchase Expenses");
		FormData fd_grpPurchaseExpenses = new FormData();
		fd_grpPurchaseExpenses.top = new FormAttachment(grpStockInformation, 6);
		fd_grpPurchaseExpenses.left = new FormAttachment(0, 10);
		fd_grpPurchaseExpenses.bottom = new FormAttachment(100, -445);
		fd_grpPurchaseExpenses.right = new FormAttachment(100, -10);
		grpPurchaseExpenses.setLayoutData(fd_grpPurchaseExpenses);
		
		Label lblLoadunloadindia = new Label(grpPurchaseExpenses, SWT.NONE);
		lblLoadunloadindia.setAlignment(SWT.RIGHT);
		lblLoadunloadindia.setBounds(10, 20, 168, 15);
		lblLoadunloadindia.setText("Load/Unload(transport india): ");
		
		txtLUIndia = new Text(grpPurchaseExpenses, SWT.BORDER);
		txtLUIndia.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calculateTotalAmount();
			}
		});
		txtLUIndia.setBounds(180, 18, 76, 21);
		txtLUIndia.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtLUIndia, 20));
		
		Label lblLoadunloadtoBorder = new Label(grpPurchaseExpenses, SWT.NONE);
		lblLoadunloadtoBorder.setAlignment(SWT.RIGHT);
		lblLoadunloadtoBorder.setBounds(262, 20, 155, 15);
		lblLoadunloadtoBorder.setText("Load/Unload (to border): ");
		
		txtLUBorder = new Text(grpPurchaseExpenses, SWT.BORDER);
		txtLUBorder.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calculateTotalAmount();
			}
		});
		txtLUBorder.setBounds(417, 17, 76, 21);
		txtLUBorder.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtLUBorder, 20));
		
		Label lblTaxClearence = new Label(grpPurchaseExpenses, SWT.NONE);
		lblTaxClearence.setAlignment(SWT.RIGHT);
		lblTaxClearence.setBounds(518, 20, 112, 15);
		lblTaxClearence.setText("Tax Clearence: ");
		
		txtTaxClearence = new Text(grpPurchaseExpenses, SWT.BORDER);
		txtTaxClearence.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calculateTotalAmount();
			}
		});
		txtTaxClearence.setBounds(633, 17, 76, 21);
		txtTaxClearence.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtTaxClearence, 20));
		
		Label lblLoadunloadtoKtm = new Label(grpPurchaseExpenses, SWT.NONE);
		lblLoadunloadtoKtm.setAlignment(SWT.RIGHT);
		lblLoadunloadtoKtm.setBounds(753, 20, 118, 15);
		lblLoadunloadtoKtm.setText("Load/Unload(to ktm): ");
		
		txtLUKtm = new Text(grpPurchaseExpenses, SWT.BORDER);
		txtLUKtm.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calculateTotalAmount();
			}
		});
		txtLUKtm.setBounds(877, 17, 76, 21);
		txtLUKtm.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtLUKtm, 20));
		
		Label lblLoadunloadtoShop = new Label(grpPurchaseExpenses, SWT.NONE);
		lblLoadunloadtoShop.setAlignment(SWT.RIGHT);
		lblLoadunloadtoShop.setBounds(10, 50, 168, 15);
		lblLoadunloadtoShop.setText("Load/Unload(to shop): ");
		
		txtLUShop = new Text(grpPurchaseExpenses, SWT.BORDER);
		txtLUShop.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calculateTotalAmount();
			}
		});
		txtLUShop.setBounds(180, 47, 76, 21);
		txtLUShop.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtLUShop, 20));
		
		Label lblTotalWeightOf = new Label(grpPurchaseExpenses, SWT.NONE);
		lblTotalWeightOf.setAlignment(SWT.RIGHT);
		lblTotalWeightOf.setBounds(518, 50, 112, 15);
		lblTotalWeightOf.setText("Total Weight(Kg): ");
		
		txtTotalWeight = new Text(grpPurchaseExpenses, SWT.BORDER);
		txtTotalWeight.setBounds(633, 47, 76, 21);
		txtTotalWeight.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtTotalWeight, 20));
		
		Label lblOther = new Label(grpPurchaseExpenses, SWT.NONE);
		lblOther.setAlignment(SWT.RIGHT);
		lblOther.setBounds(359, 50, 55, 15);
		lblOther.setText("Other: ");
		
		txtOtherExpense = new Text(grpPurchaseExpenses, SWT.BORDER);
		txtOtherExpense.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calculateTotalAmount();
			}
		});
		txtOtherExpense.setBounds(417, 47, 76, 21);
		
		tblItem = new Table(comMainContainer, SWT.BORDER | SWT.FULL_SELECTION);
		fd_grpStockDetails.bottom = new FormAttachment(100, -357);
		FormData fd_tblItem = new FormData();
		fd_tblItem.top = new FormAttachment(grpStockDetails, 6);
		
		Button btnAdd = new Button(grpStockDetails, SWT.NONE);
		btnAdd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				checkBeforeItemAdd();
				if(msg.returnMessages())
				{
					addItemInTable();
					calculateTotalAmount();
					clearItemDetails();
					bcmbItem.setSelectedBoundValue("0");
				}
			}
		});
		btnAdd.setBounds(662, 47, 75, 25);
		btnAdd.setText("Add");
		
		Button btnDelete = new Button(grpStockDetails, SWT.NONE);
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tblItem.getSelectionIndex() < 0)
				{
					msg.showIndividualMessage("Please seelct item to delete", 0);
				}
				else
				{
					if(msg.showIndividualMessage("Make sure before you delete the item", 2))
					{
						tblItem.remove(tblItem.getSelectionIndex());
						calculateTotalAmount();
					}
				}
			}
		});
		btnDelete.setBounds(820, 47, 75, 25);
		btnDelete.setText("Delete");
		
		Label lblSellingPrice = new Label(grpStockDetails, SWT.NONE);
		lblSellingPrice.setAlignment(SWT.RIGHT);
		lblSellingPrice.setBounds(262, 52, 76, 15);
		lblSellingPrice.setText("Selling Price: ");
		lblSellingPrice.setForeground(UtilMethods.getFontForegroundColorRed());
		
		txtSellingPrice = new Text(grpStockDetails, SWT.BORDER);
		txtSellingPrice.setBounds(341, 50, 120, 21);
		txtSellingPrice.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtSellingPrice, 20));
		
		Label lblUnitQuantity = new Label(grpStockDetails, SWT.NONE);
		lblUnitQuantity.setBounds(482, 22, 65, 15);
		lblUnitQuantity.setAlignment(SWT.RIGHT);
		lblUnitQuantity.setText("Weight(Kg): ");
		
		txtWeight = new Text(grpStockDetails, SWT.BORDER | SWT.READ_ONLY);
		txtWeight.setBounds(547, 18, 76, 21);
		txtWeight.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtWeight, 20));
		
		Button btnEdit = new Button(grpStockDetails, SWT.NONE);
		btnEdit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tblItem.getSelectionIndex() >= 0)
				{
					itemAction="edit";
					loadItemEditValues();
				}
				else
				{
					msg.showIndividualMessage("Please select item to edit", 0);
				}
			}
		});
		btnEdit.setBounds(743, 46, 75, 25);
		btnEdit.setText("Edit");
		fd_tblItem.left = new FormAttachment(grpStockInformation, 0, SWT.LEFT);
		
		Label lblRemarks = new Label(grpStockInformation, SWT.NONE);
		lblRemarks.setBounds(341, 52, 55, 15);
		lblRemarks.setAlignment(SWT.RIGHT);
		lblRemarks.setText("Remarks: ");
		
		txtRemarks = new Text(grpStockInformation, SWT.BORDER);
		txtRemarks.setBounds(396, 49, 291, 21);
		fd_tblItem.right = new FormAttachment(100, -10);
		fd_tblItem.bottom = new FormAttachment(100, -135);
		tblItem.setLayoutData(fd_tblItem);
		tblItem.setHeaderVisible(true);
		tblItem.setLinesVisible(true);
		
		TableColumn tblclmnId = new TableColumn(tblItem, SWT.NONE);
		tblclmnId.setWidth(0);
		tblclmnId.setText("itemid.");
		tblclmnId.setResizable(false);
		
		TableColumn tblUnitId = new TableColumn(tblItem, SWT.NONE);
		tblUnitId.setWidth(0);
		tblUnitId.setText("unitid.");
		tblUnitId.setResizable(false);
		
		TableColumn tblclmnSn = new TableColumn(tblItem, SWT.NONE);
		tblclmnSn.setWidth(50);
		tblclmnSn.setText("S.N.");
		
		TableColumn tblclmnItemCode = new TableColumn(tblItem, SWT.NONE);
		tblclmnItemCode.setWidth(135);
		tblclmnItemCode.setText("Item Code");
		
		TableColumn tblclmnWeightkg = new TableColumn(tblItem, SWT.NONE);
		tblclmnWeightkg.setWidth(80);
		tblclmnWeightkg.setText("Weight(Kg)");
		
		TableColumn tblclmnUnit = new TableColumn(tblItem, SWT.NONE);
		tblclmnUnit.setWidth(142);
		tblclmnUnit.setText("Unit");
		
		TableColumn tblclmnUnitQuantity = new TableColumn(tblItem, SWT.NONE);
		tblclmnUnitQuantity.setWidth(100);
		tblclmnUnitQuantity.setText("Unit Quantity");
		
		TableColumn tblclmnRate = new TableColumn(tblItem, SWT.NONE);
		tblclmnRate.setWidth(106);
		tblclmnRate.setText("Rate(Rs)");
		
		TableColumn tblclmnActualRate = new TableColumn(tblItem, SWT.NONE);
		tblclmnActualRate.setWidth(110);
		tblclmnActualRate.setText("Actual Rate(Rs)");
		
		TableColumn tblclmnSellingPrice = new TableColumn(tblItem, SWT.NONE);
		tblclmnSellingPrice.setWidth(100);
		tblclmnSellingPrice.setText("Selling Price");
		
		TableColumn tblclmnTotal = new TableColumn(tblItem, SWT.NONE);
		tblclmnTotal.setWidth(134);
		tblclmnTotal.setText("Total(Rs)");
		
		TableColumn tblclmnEquivalence = new TableColumn(tblItem, SWT.NONE);
		tblclmnEquivalence.setWidth(0);
		tblclmnEquivalence.setText("Total(Rs)");
		tblclmnEquivalence.setResizable(false);
		
		TableColumn tblclmnUpdtcnt = new TableColumn(tblItem, SWT.NONE);
		tblclmnUpdtcnt.setWidth(0);
		tblclmnUpdtcnt.setText("Total(Rs)");
		tblclmnUpdtcnt.setResizable(false);
		
		Composite composite = new Composite(comMainContainer, SWT.NONE);
		FormData fd_composite = new FormData();
		fd_composite.top = new FormAttachment(tblItem, 6);
		fd_composite.left = new FormAttachment(0, 10);
		fd_composite.bottom = new FormAttachment(100);
		fd_composite.right = new FormAttachment(100, -10);
		composite.setLayoutData(fd_composite);
		composite.setLayout(null);
		
		Label lblTotalAmount = new Label(composite, SWT.RIGHT);
		lblTotalAmount.setBounds(739, 54, 82, 15);
		lblTotalAmount.setText("Grand Total");
		lblTotalAmount.setFont(UtilMethods.getFontBold());
		
		txtTotalAmount = new Text(composite, SWT.BORDER | SWT.READ_ONLY);
		txtTotalAmount.setBounds(827, 52, 136, 20);
		txtTotalAmount.setForeground(UtilMethods.getFontForegroundColorRed());
		
		Label lblDueAmount = new Label(composite, SWT.RIGHT);
		lblDueAmount.setBounds(739, 102, 82, 15);
		lblDueAmount.setText("Due Amount");
		lblDueAmount.setFont(UtilMethods.getFontBold());
		
		txtDueAmount = new Text(composite, SWT.BORDER | SWT.READ_ONLY);
		txtDueAmount.setBounds(827, 100, 136, 20);
		txtDueAmount.setForeground(UtilMethods.getFontForegroundColorRed());
		
		Button btnSave = new Button(composite, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tblItem.getItemCount() <= 0)
				{
					msg.setMandatoryMessages("No items to save");
				}
				 if(bcmbSupplier.getSelectedBoundValue().toString().equals("0"))
				{
					msg.setMandatoryMessages("Please select supplier");
				}
				if(txtBillNo.getText().equals(""))
				{
					msg.setMandatoryMessages("Please enter bill no");
				}
				else
				{
					if(action.equals("add"))
					{
						if(UtilFxn.isNotUnique("select * from inven_item_purchase where bill_no='"+txtBillNo.getText()+"'"))
						{
							msg.setMandatoryMessages("Bill number you entered is already in use");
						}
					}
					else
					{
						if(UtilFxn.isNotUnique("select * from inven_item_purchase where bill_no='"+txtBillNo.getText()+"' and pur_id !='"+purchasedId+"'"))
						{
							msg.setMandatoryMessages("Bill number you entered is already in use");
						}
					}
				}
				if(bcmbPurDate.getValue()==null)
				{
					msg.setMandatoryMessages("Please select purchased date");
				}
				 if(bcmbBrand.getSelectedBoundValue().equals("0"))
				{
					msg.setMandatoryMessages("Please select item purchase type");
				}
				else if(bcmbBrand.getSelectedBoundValue().equals("1"))
				{
					if(bcmbLocalBrandType.getSelectedBoundValue().toString().equals("0"))
					{
						msg.setMandatoryMessages("Please select item local purchase type");
					}
				}
				if(checkTotalSourceAmount() > Double.parseDouble(txtTotalAmount.getText().equals("")?"0":txtTotalAmount.getText().replaceAll(",", "")))
				{
					msg.setMandatoryMessages("Your purchase cost exceeds the source paid cost");
				}
				 
				if(msg.returnMessages())
				{
					setSaveValues();
					
					
					try {
						Connection cnn=DbConnection.getConnection();
						cnn.setAutoCommit(false);
						//if(control.saveAction(stockPurchasedModel, itemsList, sourceList,jvSummary,jvDetailList,action))
						{
							
							clearAll();
							msg.showIndividualMessage("Items saved successfully.", 0);
							cnn.commit();
						}
						//else
						{
							msg.showIndividualMessage("Some error occured while saving.Please try again.", 0);
							cnn.rollback();
							cnn.setAutoCommit(true);
						}
						
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						Connection cnn=DbConnection.getConnection();
						try {
							cnn.rollback();
							cnn.setAutoCommit(true);
						} catch (SQLException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
						
					}
				}
			}
		});
		btnSave.setBounds(600, 38, 72, 39);
		btnSave.setText("Save");
		
		Label lblTotal_1 = new Label(composite, SWT.RIGHT);
		lblTotal_1.setAlignment(SWT.RIGHT);
		lblTotal_1.setBounds(739, 6, 82, 15);
		lblTotal_1.setText("Total");
		lblTotal_1.setFont(UtilMethods.getFontBold());
		
		txtItemTotal = new Text(composite, SWT.BORDER);
		txtItemTotal.setEditable(false);
		txtItemTotal.setBounds(827, 4, 136, 20);
		txtItemTotal.setForeground(UtilMethods.getFontForegroundColorRed());
		
		Label lblExpenses = new Label(composite, SWT.RIGHT);
		lblExpenses.setAlignment(SWT.RIGHT);
		lblExpenses.setBounds(749, 30, 72, 15);
		lblExpenses.setText("Expenses ");
		lblExpenses.setFont(UtilMethods.getFontBold());
		
		txtTotalExpense = new Text(composite, SWT.BORDER);
		txtTotalExpense.setEditable(false);
		txtTotalExpense.setBounds(827, 28, 136, 20);
		txtTotalExpense.setForeground(UtilMethods.getFontForegroundColorRed());
		
		Label lblPaidAmount = new Label(composite, SWT.RIGHT);
		lblPaidAmount.setBounds(739, 78, 82, 15);
		lblPaidAmount.setText("Paid Amount");
		lblPaidAmount.setFont(UtilMethods.getFontBold());
		
		txtPaidAmount = new Text(composite, SWT.BORDER);
		txtPaidAmount.setEditable(false);
		txtPaidAmount.setBounds(827, 76, 136, 20);
		txtPaidAmount.setForeground(UtilMethods.getFontForegroundColorRed());
		
		Group grpPaymentDetails = new Group(composite, SWT.NONE);
		grpPaymentDetails.setBounds(0, 0, 551, 118);
		grpPaymentDetails.setText("Payment Details");
		
		btnCash = new Button(grpPaymentDetails, SWT.RADIO);
		btnCash.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				enableDisableSource(false, true);
				clearSource();
			}
		});
		btnCash.setBounds(22, 20, 55, 16);
		btnCash.setText("Cash");
		
		btnBank = new Button(grpPaymentDetails, SWT.RADIO);
		btnBank.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				enableDisableSource(true, false);
				clearSource();
			}
		});
		btnBank.setBounds(22, 52, 55, 16);
		btnBank.setText("Bank");
		
		bcmbCash = new SwtBndCombo(grpPaymentDetails, SWT.READ_ONLY);
		bcmbCash.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(!bcmbCash.getSelectedBoundValue().toString().equals("0"))
				{
					loadHeadBalance(true,false);
				}
				else
				{
					clearSource();
				}
			}
		});
		bcmbCash.setEnabled(false);
		bcmbCash.setBounds(79, 18, 102, 23);
		bcmbCash.fillData(CmbQry.getMapAccCodeQuery("Cash"), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		
		bcmbBank = new SwtBndCombo(grpPaymentDetails, SWT.READ_ONLY);
		bcmbBank.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(!bcmbBank.getSelectedBoundValue().toString().equals("0"))
				{
					loadHeadBalance(false,true);
				}
				else
				{
					clearSource();
				}
			}
		});
		bcmbBank.setEnabled(false);
		bcmbBank.setBounds(79, 50, 102, 23);
		
			bcmbBank.fillData(CmbQry.getMapAccCodeQuery("Bank"), CmbQry.getLabel(),
					CmbQry.getId(), DbConnection.getConnection(), true);
			
			txtCash = new Text(grpPaymentDetails, SWT.BORDER);
			txtCash.setEnabled(false);
			txtCash.setBounds(187, 18, 76, 21);
			txtCash.addListener(SWT.Verify, new DecimalTextPositiveOnly(
					txtCash, 20));
			
			txtBank = new Text(grpPaymentDetails, SWT.BORDER);
			txtBank.setEnabled(false);
			txtBank.setBounds(187, 50, 76, 21);
			txtBank.addListener(SWT.Verify, new DecimalTextPositiveOnly(
					txtBank, 20));
			
			Button btnPlus = new Button(grpPaymentDetails, SWT.NONE);
			btnPlus.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					beforeSourceAdd();
					if(msg.returnMessages())
					{
						addSourceItem();
						clearSource();
						calculateTotalAmount();
					}
				}
			});
			btnPlus.setFont(SWTResourceManager.getFont("Segoe UI", 13, SWT.BOLD));
			btnPlus.setBounds(275, 18, 32, 25);
			btnPlus.setText("+");
			
			tblSourceAmount = new Table(grpPaymentDetails, SWT.BORDER | SWT.FULL_SELECTION);
			tblSourceAmount.setBounds(313, 13, 232, 98);
			tblSourceAmount.setHeaderVisible(true);
			tblSourceAmount.setLinesVisible(true);
			
			TableColumn tblclmnAccCode = new TableColumn(tblSourceAmount, SWT.NONE);
			tblclmnAccCode.setWidth(0);
			tblclmnAccCode.setText("acccode");
			tblclmnAccCode.setResizable(false);
			
			TableColumn tblclmnSn_1 = new TableColumn(tblSourceAmount, SWT.NONE);
			tblclmnSn_1.setWidth(37);
			tblclmnSn_1.setText("S.N.");
			
			TableColumn tblclmnAmount = new TableColumn(tblSourceAmount, SWT.NONE);
			tblclmnAmount.setWidth(83);
			tblclmnAmount.setText("Amount(Rs)");
			
			TableColumn tblclmnSource = new TableColumn(tblSourceAmount, SWT.NONE);
			tblclmnSource.setWidth(107);
			tblclmnSource.setText("Source");
			
			TableColumn tblclmnTrxnId = new TableColumn(tblSourceAmount, SWT.NONE);
			tblclmnTrxnId.setWidth(0);
			tblclmnTrxnId.setText("TrxnId");
			tblclmnTrxnId.setResizable(false);
			
			Button btnX = new Button(grpPaymentDetails, SWT.NONE);
			btnX.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if(tblSourceAmount.getSelectionIndex() < 0)
					{
						msg.showIndividualMessage("Please select the row to delete", 0);
					}
					else
					{
						if(msg.showIndividualMessage("Make sure before you delete the item", 2))
						{
							int selection=tblSourceAmount.getSelectionIndex();
							tblSourceAmount.remove(tblSourceAmount.getSelectionIndex());
							
							if (tblSourceAmount.getItemCount() > 0) {
								
								TableItem[] Item = tblSourceAmount.getItems();
								for (int i = selection+1; i < tblSourceAmount.getItemCount(); i++) {
									Item[i].setText(1, String.valueOf(tblSourceAmount.getItemCount()));
									
								}
								
							}
							calculateTotalAmount();
						}
						
					}
				}
			});
			btnX.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
			btnX.setText("x");
			btnX.setBounds(275, 48, 32, 25);
			
			Label lblAvailableBalance = new Label(grpPaymentDetails, SWT.NONE);
			lblAvailableBalance.setBounds(42, 80, 97, 15);
			lblAvailableBalance.setText("Available Balance: ");
			
			lblBalanceAvailable = new Label(grpPaymentDetails, SWT.BORDER);
			lblBalanceAvailable.setBounds(145, 79, 162, 21);
			lblBalanceAvailable.setForeground(UtilMethods.getFontForegroundColorRed());

		
		

		if(purchasedId > 0)
		{
			loadEditData(purchasedId);
		}
			
		return comParent;
	}
	private void clearItemDetails()
	{
		//bcmbItem.setSelectedBoundValue("0");
		txtWeight.setText("");
		txtUnit.setText("");
		txtUnitQuantity.setText("");
		txtRate.setText("");
		txtActualRate.setText("");
		txtTotal.setText("");
		txtSellingPrice.setText("");
		itemEquivalence=0;
		unit_id=0;
		itemAction="add";
	}
	private void loadItemDetails(int itemId)
	{
		StockItemController itemController=new StockItemController();
		ResultSet rs=itemController.fetchItemDetails(itemId);
		try {
		if(rs!=null && rs.next())
				{
					
						
				UnitMapController unitControl=new UnitMapController();
				ResultSet unitRs=unitControl.getUnitDetailsById(rs.getInt("item_pur_unit_id"));
				unit_id=rs.getInt("item_pur_unit_id");
				txtUnit.setText(unitRs.getString("unit_name"));
				txtWeight.setText(String.valueOf(rs.getDouble("unit_weight") <=0 ?"":rs.getBigDecimal("unit_weight")));
				itemEquivalence=unitRs.getDouble("conversion_ration");
				txtSellingPrice.setText(String.valueOf(rs.getDouble("item_selling_price") <=0 ?"0":rs.getBigDecimal("item_selling_price")));
			}
		else
		{
			clearItemDetails();
		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void calculateTotalAmount()
	{
		totalAmount=0;
		totalExpense=0;
		double lui=txtLUIndia.getText().equals("")?0:Double.parseDouble(txtLUIndia.getText().replaceAll(",", ""));
		double lub=txtLUBorder.getText().equals("")?0:Double.parseDouble(txtLUBorder.getText().replaceAll(",", ""));
		double taxClearence=txtTaxClearence.getText().equals("")?0:Double.parseDouble(txtTaxClearence.getText().replaceAll(",", ""));
		double luk=txtLUKtm.getText().equals("")?0:Double.parseDouble(txtLUKtm.getText().replaceAll(",", ""));
		double lus=txtLUShop.getText().equals("")?0:Double.parseDouble(txtLUShop.getText().replaceAll(",", ""));
		double other=txtOtherExpense.getText().equals("")?0:Double.parseDouble(txtOtherExpense.getText().replaceAll(",", ""));
		
		totalExpense=lui+lub+taxClearence+luk+lus+other;
		
		txtTotalExpense.setText(UtilFxn.getFormatedAmount(totalExpense));
		
		
		//txtTotal.setText(UtilFxn.getFormatedAmount(totalAmount));
		
		double totItemPrice=0;
		if (tblItem.getItemCount() > 0) {
			TableItem[] Item = tblItem.getItems();
			for (int i = 0; i < tblItem.getItemCount(); i++) {
				totItemPrice+=Double.parseDouble(Item[i].getText(10).replaceAll(",", ""));
			}
			
		}
		txtItemTotal.setText(UtilFxn.getFormatedAmount(totItemPrice));
		totalAmount=totalExpense+totItemPrice;
		txtTotalAmount.setText(UtilFxn.getFormatedAmount(totalAmount));
		
		txtPaidAmount.setText(UtilFxn.getFormatedAmount(checkTotalSourceAmount()));
		txtDueAmount.setText(UtilFxn.getFormatedAmount(Double.valueOf(txtTotalAmount.getText().replaceAll(",", ""))-checkTotalSourceAmount()));
	}
	
	private void calculateItemTotal()
	{
		double rate=txtRate.getText().equals("")?0:Double.parseDouble(txtRate.getText().replaceAll(",", ""));
		double unitQuantity=txtUnitQuantity.getText().equals("")?0:Double.parseDouble(txtUnitQuantity.getText().replaceAll(",", ""));
		double totWeight=txtTotalWeight.getText().equals("")?0:Double.parseDouble(txtTotalWeight.getText().replaceAll(",", ""));
		double itemWeight=txtWeight.getText().equals("")?0:Double.parseDouble(txtWeight.getText().replaceAll(",", ""));
		
		txtTotal.setText(UtilFxn.getFormatedAmount((rate*unitQuantity)));
		
		if(totWeight > 0 && rate >0 && itemWeight > 0 && unitQuantity > 0)
		{
			double itemTotalWeight=itemEquivalence*itemWeight;
			double addedAmt=(totalExpense/totWeight)*itemTotalWeight;
		
			txtActualRate.setText(UtilFxn.getFormatedAmount((rate+addedAmt)));
		}
		else
		{
			txtActualRate.setText(txtRate.getText());
		}
	}
	private void addItemInTable()
	{
		if(itemAction.equals("edit"))
		{
			TableItem[] Item = tblItem.getItems();
			for (int i = 0; i < tblItem.getItemCount(); i++) {
				String itemId=Item[i].getText(0);
				if(itemId.equals(bcmbItem.getSelectedBoundValue().toString()))
				{
					Item[i].setText(0, bcmbItem.getSelectedBoundValue().toString());
					Item[i].setText(1,String.valueOf(unit_id));
					Item[i].setText(2,String.valueOf(tblItem.getItemCount()));
					Item[i].setText(3,bcmbItem.getText());
					Item[i].setText(4,String.valueOf(txtWeight.getText()));
					Item[i].setText(5,txtUnit.getText());
					Item[i].setText(6,txtUnitQuantity.getText());
					Item[i].setText(7,txtRate.getText());
					Item[i].setText(8,txtActualRate.getText());
					Item[i].setText(9,txtSellingPrice.getText());
					Item[i].setText(10,txtTotal.getText());
					Item[i].setText(11,String.valueOf(itemEquivalence));
					Item[i].setText(12,String.valueOf(-1));
				}
			}
		}
		else
		{
			TableItem item=new TableItem(tblItem, SWT.NONE);
			item.setText(0, bcmbItem.getSelectedBoundValue().toString());
			item.setText(1,String.valueOf(unit_id));
			item.setText(2,String.valueOf(tblItem.getItemCount()));
			item.setText(3,bcmbItem.getText());
			item.setText(4,String.valueOf(txtWeight.getText()));
			item.setText(5,txtUnit.getText());
			item.setText(6,txtUnitQuantity.getText());
			item.setText(7,txtRate.getText());
			item.setText(8,txtActualRate.getText());
			item.setText(9,txtSellingPrice.getText());
			item.setText(10,txtTotal.getText());
			item.setText(11,String.valueOf(itemEquivalence));
			item.setText(12,String.valueOf(-1));
		}
		
		
	}
	
	private void loadItemEditValues()
	{
		bcmbItem.setSelectedBoundValue(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(0));
		itemEquivalence=Double.parseDouble(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(11));
		unit_id=Integer.valueOf(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(1));
		txtWeight.setText(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(4));
		txtUnit.setText(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(5));
		txtUnitQuantity.setText(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(6));
		txtRate.setText(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(7));
		txtActualRate.setText(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(8));
		txtSellingPrice.setText(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(9));
		txtTotal.setText(tblItem.getItem(
				tblItem.getSelectionIndex()).getText(10));
	}
	
	private void checkBeforeItemAdd()
	{
		if(bcmbItem.getSelectedBoundValue().toString().equals("0"))
		{
			msg.setMandatoryMessages("Please select item.");
		}
		if(txtUnitQuantity.getText().equals(""))
		{
			msg.setMandatoryMessages("Please enter unit quantity purchased.");
		}
		else
		{
			if(Double.valueOf(txtUnitQuantity.getText().replaceAll(",", "")) <= 0)
			{
				msg.setMandatoryMessages("Please enter unit quantity purchased greater than 0.");
			}
		}
		if(txtRate.getText().equals(""))
		{
			msg.setMandatoryMessages("Please enter unit rate.");
		}
		else
		{
			if(Double.valueOf(txtRate.getText().replaceAll(",", "")) <= 0)
			{
				msg.setMandatoryMessages("Please enter unit rate greater than 0.");
			}
		}
		if(txtSellingPrice.getText().equals(""))
		{
			msg.setMandatoryMessages("Please enter unit selling price.");
		}
		else
		{
			if(Double.valueOf(txtSellingPrice.getText().replaceAll(",", "")) <= 0)
			{
				msg.setMandatoryMessages("Please enter unit selling price greater than 0.");
			}
		}
		
		//loop in the table to see if selected item is already added
		if (tblItem.getItemCount() > 0 && itemAction.equals("add")) {
			TableItem[] Item = tblItem.getItems();
			for (int i = 0; i < tblItem.getItemCount(); i++) {
				String itemId=Item[i].getText(0);
				if(itemId.equals(bcmbItem.getSelectedBoundValue().toString()))
				{
					msg.setMandatoryMessages("Selected item already added in the table.");
					break;
				}
			}
			
		}
	}
	
	private void enableDisableSource(boolean bank,boolean cash)
	{
		
		bcmbBank.setEnabled(bank);
		txtBank.setEnabled(bank);
		bcmbCash.setEnabled(cash);
		txtCash.setEnabled(cash);
		
	}
	
	private void clearSource()
	{
		
		
			bcmbBank.setSelectedBoundValue("0");
			txtBank.setText("");
			bcmbCash.setSelectedBoundValue("0");
			txtCash.setText("");
			lblBalanceAvailable.setText("");
		
	}
	
	private void loadHeadBalance(boolean isCash,boolean isBank)
	{
		double cashBal=0;
		if (isCash)
		{
		    cashBal = JournalEntryController.getInstance()
		    .getAccountBalance(
			    bcmbCash.getSelectedBoundValue()
			    .toString(), "1");
		    System.out.println("This is the cash balance"+cashBal);
		    lblBalanceAvailable.setText(String.valueOf(cashBal <= 0 ? 0 : UtilFxn.getFormatedAmount(cashBal)));
		}
		else if(isBank)
		{
		    cashBal = JournalEntryController.getInstance()
		    .getAccountBalance(
			    bcmbBank.getSelectedBoundValue()
			    .toString(), "1");
		    lblBalanceAvailable.setText(String.valueOf(cashBal <= 0 ? 0 : UtilFxn.getFormatedAmount
			    (cashBal)));
		}
	}
	
	private void beforeSourceAdd()
	{
		if(btnCash.getSelection())
		{
			if(bcmbCash.getSelectedBoundValue().toString().equals("0"))
			{
				msg.setMandatoryMessages("Please select cash source");
			}
			else if(txtCash.getText().equals(""))
			{
				msg.setMandatoryMessages("Please enter cash amount");
			}
			else if(Integer.parseInt(txtCash.getText()) <= 0)
			{
				msg.setMandatoryMessages("Please enter cash amount greater than 0");
			}
			else if(checkSufficientSourceAmount(bcmbCash.getSelectedBoundValue().toString()) < Double.parseDouble(txtCash.getText().replaceAll(",", "")))
			{
				msg.setMandatoryMessages("You do not have sufficient source amount in selected source.Please select another source");
			}
			else if((checkTotalSourceAmount()+Double.parseDouble(txtCash.getText().replaceAll(",", ""))) > Double.parseDouble(txtTotalAmount.getText().equals("")?"0":txtTotalAmount.getText().replaceAll(",", "")))
			{
				msg.setMandatoryMessages("Your purchase cost exceeds the source paid cost");
			}
		}
		
		else if(btnBank.getSelection())
		{
			if(bcmbBank.getSelectedBoundValue().toString().equals("0"))
			{
				msg.setMandatoryMessages("Please select bank source");
			}
			else if(txtBank.getText().equals(""))
			{
				msg.setMandatoryMessages("Please enter bank amount");
			}
			else if(Integer.parseInt(txtBank.getText()) <= 0)
			{
				msg.setMandatoryMessages("Please enter bank amount greater than 0");
			}
			else if(checkSufficientSourceAmount(bcmbBank.getSelectedBoundValue().toString()) < Double.parseDouble(txtBank.getText().replaceAll(",", "")))
			{
				msg.setMandatoryMessages("You do not have sufficient source amount in selected source.Please select another source");
			}
			else if((checkTotalSourceAmount()+Double.parseDouble(txtBank.getText().replaceAll(",", ""))) > Double.parseDouble(txtTotalAmount.getText().equals("")?"0":txtTotalAmount.getText().replaceAll(",", "")))
			{
				msg.setMandatoryMessages("Your purchase cost exceeds the source paid cost");
			}
		}
		else
		{
			msg.setMandatoryMessages("Please select source");
		}
		
		
		
		
	}
	
	private double checkSufficientSourceAmount(String accCode)
	{
		double amount=0;
		if (tblSourceAmount.getItemCount() > 0) {
			TableItem[] Item = tblSourceAmount.getItems();
			for (int i = 0; i < tblSourceAmount.getItemCount(); i++) {
				String code=Item[i].getText(0);
				if(code.equals(accCode))
				{
					amount+=Double.parseDouble(Item[i].getText(2).replaceAll(",", ""));
				}
			}
			
		}
		return Double.parseDouble(lblBalanceAvailable.getText().equals("")?"0":lblBalanceAvailable.getText().replaceAll(",", ""))-amount;
	}
	
	private double checkTotalSourceAmount()
	{
		double amount=0;
		if (tblSourceAmount.getItemCount() > 0) {
			TableItem[] Item = tblSourceAmount.getItems();
			for (int i = 0; i < tblSourceAmount.getItemCount(); i++) {
				String code=Item[i].getText(0);
				
					amount+=Double.parseDouble(Item[i].getText(2).replaceAll(",", ""));
				
			}
			
		}
		return amount;
	}
	
	
	private void setSaveValues()
	{
		stockPurchasedModel=new StockPurchasedModel();
		itemsList.clear();
		sourceList.clear();
		jvDetailList.clear();
		//set itempurchased
		if(action.equals("edit"))
		{
			stockPurchasedModel.setPur_id(purchasedId);
			stockPurchasedModel.setJv_no(jvNo);
		}
		
		stockPurchasedModel.setSupplier_id(Integer.valueOf(bcmbSupplier.getSelectedBoundValue().toString()));
		stockPurchasedModel.setBill_no(txtBillNo.getText());
		stockPurchasedModel.setTrxn_dt(bcmbPurDate.getText());
		stockPurchasedModel.setItem_purchased_type(Integer.valueOf(bcmbBrand.getSelectedBoundValue().toString()));
		stockPurchasedModel.setItem_local_purchased_type(Integer.valueOf(bcmbLocalBrandType.getSelectedBoundValue().toString()));
	
		stockPurchasedModel.setLu_india_expense(txtLUIndia.getText().equals("")?"0":txtLUIndia.getText().replaceAll(",", ""));
		stockPurchasedModel.setLu_border_expense(txtLUBorder.getText().equals("")?"0":txtLUBorder.getText().replaceAll(",", ""));
		stockPurchasedModel.setLu_tax_clearence_expense(txtTaxClearence.getText().equals("")?"0":txtTaxClearence.getText().replaceAll(",", ""));
		stockPurchasedModel.setLu_ktm_expense(txtLUKtm.getText().equals("")?"0":txtLUKtm.getText().replaceAll(",", ""));
		stockPurchasedModel.setLu_shop_expense(txtLUShop.getText().equals("")?"0":txtLUShop.getText().replaceAll(",", ""));
		stockPurchasedModel.setOther_expense(txtOtherExpense.getText().equals("")?"0":txtOtherExpense.getText().replaceAll(",", ""));
		stockPurchasedModel.setTotal_weight(txtTotalWeight.getText().equals("")?"0":txtTotalWeight.getText().replaceAll(",", ""));
		stockPurchasedModel.setTotal_amount(txtTotalAmount.getText().equals("")?"0":txtTotalAmount.getText().replaceAll(",", ""));
		stockPurchasedModel.setPaid_amt(UtilFxn.getDoubleExpFormated(checkTotalSourceAmount()));
		stockPurchasedModel.setRemarks(txtRemarks.getText().replaceAll("'", "''"));
		stockPurchasedModel.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
		
		
		//items details
		if (tblItem.getItemCount() > 0) {
			TableItem[] Item = tblItem.getItems();
			for (int i = 0; i < tblItem.getItemCount(); i++) {
				StockPurchasedDetailsModel model=new StockPurchasedDetailsModel();
				model.setItem_id(Integer.valueOf(Item[i].getText(0)));
				model.setUnit_id(Integer.valueOf(Item[i].getText(1)));
				model.setUnit_weight(Item[i].getText(4).replaceAll(",", ""));
				model.setQuantity(Item[i].getText(6).replaceAll(",", ""));
				model.setRate(Item[i].getText(7).replaceAll(",", ""));
				model.setActual_rate(Item[i].getText(8).replaceAll(",", ""));
				model.setSelling_price(Item[i].getText(9).replaceAll(",", ""));
				model.setTotal(Item[i].getText(10).replaceAll(",", ""));
				model.setUnit_quantity(UtilFxn.getDoubleExpFormated(Double.valueOf(Item[i].getText(11).replaceAll(",", "")) * Double.valueOf(Item[i].getText(6).replaceAll(",", ""))));
				model.setCrtd_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
				model.setCrtd_dt(jcalFxn.currentDate());
				model.setUpdt_dt(jcalFxn.currentDate());
				model.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
				model.setUpdt_cnt(0);
				if(action.equals("edit"))
				{
					model.setUpdt_cnt(Integer.valueOf(Item[i].getText(12))+1);
				}
				itemsList.add(model);
					
				
			}
			
		}
		
		//payment details
		if (tblSourceAmount.getItemCount() > 0) {
			TableItem[] Item = tblSourceAmount.getItems();
			for (int i = 0; i < tblSourceAmount.getItemCount(); i++) {
				StockPurchasedTrxnModel model=new StockPurchasedTrxnModel();
				model.setAcc_code(Item[i].getText(0));
				model.setAmount(Item[i].getText(2).replaceAll(",", ""));
				model.setTrxn_source(Item[i].getText(3));
				if(action.equals("edit"))
				{
					model.setPur_trxn_id(Integer.valueOf(Item[i].getText(4)));
				}
				
				sourceList.add(model);
			}
		}
		
		
		//set journal details
		jvSummary = new JVSummary();
		jvSummary.setJv_date(stockPurchasedModel.getTrxn_dt());
		jvSummary.setCreated_date(jcalFxn.currentDate());
		jvSummary.setUpdated_date(jcalFxn.currentDate());
		jvSummary.setCreated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		jvSummary.setUpdated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		jvSummary.setApproved_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		jvSummary.setPosted_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		jvSummary.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
		jvSummary.setBr_id(1);
		jvSummary.setStatus(1);
		jvSummary.setUpdate_count(1);
		jvSummary
		.setBill_date(stockPurchasedModel.getTrxn_dt());
		jvSummary.setTo_from_type("Purchase");
		
		
		//jvdetails
		jvDetailList = new ArrayList<JVDetailsModel>();
		
		
		
		//item purchased
		objJVDetails = new JVDetailsModel();
		objJVDetails.setAcc_code(control.getAccountCode("Debit", "item_purchased"));	
		objJVDetails.setDr_amt(Double.valueOf(txtTotalAmount.getText().replaceAll(",", "")));
		objJVDetails.setCr_amt(0);
		objJVDetails.setNarration("Item Purchased on " +stockPurchasedModel.getTrxn_dt() + "from" + bcmbSupplier.getText());
		jvDetailList.add(objJVDetails);
		
		
		//for expenses
		objJVDetails = new JVDetailsModel();
		objJVDetails.setAcc_code(control.getAccountCode("Credit", "item_purchased_expense"));	
		objJVDetails.setDr_amt(0);
		objJVDetails.setCr_amt(totExpenses());
		objJVDetails.setNarration("Expense for Item Purchased on " +stockPurchasedModel.getTrxn_dt() + "from" + bcmbSupplier.getText());
		jvDetailList.add(objJVDetails);
		
		//for paid
		for (int i = 0; i < this.tblSourceAmount.getItemCount(); i++) {
			objJVDetails = new JVDetailsModel();
			objJVDetails.setAcc_code(tblSourceAmount.getItem(i).getText(0).trim());
		
			objJVDetails.setDr_amt(0);
			objJVDetails.setCr_amt(Double.parseDouble(tblSourceAmount.getItem(i).getText(2).replaceAll(",", "")));
			objJVDetails.setNarration("Amount paid for Item Purchased on " +stockPurchasedModel.getTrxn_dt() + "from" + bcmbSupplier.getText());
			jvDetailList.add(objJVDetails);
		}
		
		//remaining on credit
		if(checkTotalSourceAmount() < Double.parseDouble(txtTotalAmount.getText().replaceAll(",", ""))  )
		{
			
			objJVDetails = new JVDetailsModel();
			objJVDetails.setAcc_code(control.getAccountCode("Credit", "item_purchased_credit"));	
			objJVDetails.setDr_amt(0);
			objJVDetails.setCr_amt(Double.parseDouble(txtTotalAmount.getText().replaceAll(",", ""))-totExpenses()-checkTotalSourceAmount());
			objJVDetails.setNarration("Credit Item Purchased on " +stockPurchasedModel.getTrxn_dt() + "from" + bcmbSupplier.getText());
			jvDetailList.add(objJVDetails);
		}
		
		
		
		
		
	}
	
	
	private double totExpenses()
	{
		double tot=0;
		tot=Double.valueOf((Double.valueOf(stockPurchasedModel.getLu_border_expense())
				+Double.valueOf(stockPurchasedModel.getLu_india_expense())+Double.valueOf(stockPurchasedModel.getLu_ktm_expense())+
				Double.valueOf(stockPurchasedModel.getLu_shop_expense())+
				Double.valueOf(stockPurchasedModel.getLu_tax_clearence_expense())+Double.valueOf(stockPurchasedModel.getOther_expense())));
		return tot;
	}
	
	private void addSourceItem()
	{
		TableItem item = new TableItem(tblSourceAmount, SWT.NONE);
		if(btnCash.getSelection())
		{
			item.setText(0, bcmbCash.getSelectedBoundValue().toString());
			item.setText(2, txtCash.getText());
			item.setText(3,bcmbCash.getText());
			item.setText(4,"0");
		}
		item.setText(1, String.valueOf(tblSourceAmount.getItemCount()));
	}
	
	public void reloadSupplier()
	{
		bcmbSupplier.fillData(CmbQry.getAllSuppliers(2), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
	}
	
	public void reloadItem()
	{
		clearItemDetails();
		bcmbItem.fillData(CmbQry.getStockItems(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
	}
	
	
	//load edit datas
	private void loadEditData(int purid)
	{
		this.action="edit";
		
		ResultSet purchased=control.getPurchasedById(purid);
		ResultSet purchasedDetails=control.getPurchasedDetailsById(purid);
		ResultSet purchasedTrxn=control.getPurchasedSourceDetailsById(purid);
		
		//load purchased
		try {
			if(purchased.next())
			{
				bcmbSupplier.setSelectedBoundValue(purchased.getInt("supplier_id"));
				txtBillNo.setText(purchased.getString("bill_no"));
				bcmbPurDate.setValue(purchased.getDate("trxn_dt"));
				bcmbBrand.setSelectedBoundValue(purchased.getInt("item_purchased_type"));
				
				if(bcmbBrand.getSelectedBoundValue().toString().equals("2"))
				{
					bcmbLocalBrandType.setEnabled(false);
				}
				else
				{
					bcmbLocalBrandType.setEnabled(true);
				}
				
				bcmbLocalBrandType.setSelectedBoundValue(purchased.getInt("item_local_purchased_type"));
				txtRemarks.setText(purchased.getString("remarks"));
				txtLUIndia.setText(UtilFxn.getFormatedAmount(purchased.getDouble("lu_india_expense")));
				txtLUBorder.setText(UtilFxn.getFormatedAmount(purchased.getDouble("lu_border_expense")));
				txtTaxClearence.setText(UtilFxn.getFormatedAmount(purchased.getDouble("lu_tax_clearence_expense")));
				txtLUKtm.setText(UtilFxn.getFormatedAmount(purchased.getDouble("lu_ktm_expense")));
				txtLUShop.setText(UtilFxn.getFormatedAmount(purchased.getDouble("lu_shop_expense")));
				txtOtherExpense.setText(UtilFxn.getFormatedAmount(purchased.getDouble("other_expense")));
				txtTotalWeight.setText(UtilFxn.getFormatedAmount(purchased.getDouble("total_weight")));
				jvNo=purchased.getInt("jv_no");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//load purchased details
		try {
			if(UtilFxn.getRowCount(purchasedDetails) > 0)
			{
				int i=1;
				while(purchasedDetails.next())
				{
					TableItem item=new TableItem(tblItem, SWT.NONE);
					item.setText(0,String.valueOf(purchasedDetails.getInt("item_id")));
					item.setText(1,String.valueOf(purchasedDetails.getInt("unit_id")));
					item.setText(2,String.valueOf(i));
					item.setText(3,purchasedDetails.getString("item_name"));
					item.setText(4,UtilFxn.getFormatedAmount(purchasedDetails.getDouble("unit_weight")));
					item.setText(5,purchasedDetails.getString("unit_name"));
					item.setText(6,UtilFxn.getFormatedAmount(purchasedDetails.getDouble("quantity")));
					item.setText(7,UtilFxn.getFormatedAmount(purchasedDetails.getDouble("rate")));
					item.setText(8,UtilFxn.getFormatedAmount(purchasedDetails.getDouble("actual_rate")));
					item.setText(9,UtilFxn.getFormatedAmount(purchasedDetails.getDouble("item_selling_price")));
					item.setText(10,UtilFxn.getFormatedAmount(purchasedDetails.getDouble("total")));
					item.setText(11,String.valueOf(purchasedDetails.getDouble("conversion_ration")));
					item.setText(12,String.valueOf(purchasedDetails.getInt("updt_cnt")));
					i++;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//load payment details
		try {
			if(UtilFxn.getRowCount(purchasedTrxn) > 0)
			{
				int i=1;
				while(purchasedTrxn.next())
				{
					TableItem item = new TableItem(tblSourceAmount, SWT.NONE);
					
						item.setText(0, purchasedTrxn.getString("acc_code"));
						item.setText(1, String.valueOf(i));
						item.setText(2, UtilFxn.getFormatedAmount(purchasedTrxn.getDouble("amount")));
						item.setText(3,purchasedTrxn.getString("acc_name"));					
					item.setText(4, String.valueOf(purchasedTrxn.getInt("pur_trxn_id")));
					i++;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//cal total
		calculateTotalAmount();
	}
	
	private void clearAll()
	{
		
		bcmbSupplier.setSelectedBoundValue("0");
		txtBillNo.setText("");
		bcmbPurDate.setValue(new Date());
		bcmbBrand.setSelectedBoundValue("0");
		bcmbLocalBrandType.setSelectedBoundValue("0");
		txtRemarks.setText("");
		txtLUBorder.setText("");
		txtLUIndia.setText("");
		txtLUKtm.setText("");
		txtLUShop.setText("");
		txtOtherExpense.setText("");
		txtTaxClearence.setText("");
		txtTotalWeight.setText("");
		tblItem.removeAll();
		tblSourceAmount.removeAll();
		txtTotal.setText("");
		txtTotalExpense.setText("");
		txtItemTotal.setText("");
		txtDueAmount.setText("");
		txtPaidAmount.setText("");
		txtTotalAmount.setText("");
	}
}

