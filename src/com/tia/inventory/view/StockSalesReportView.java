package com.tia.inventory.view;


import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.jasperassistant.designer.viewer.ViewerComposite;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.UtilMethods;

import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;

public class StockSalesReportView extends Dialog {

	private Composite comParent;
	private Composite comMainContainer;

	private SimpleDateFormat sdf;
	private UtilMethods objUtilMethods;
	private ViewerComposite viewerComposite;

	private int id=0;
	private String type="";

	Object rtn;

	public StockSalesReportView(Shell parentShell,int selId,String rptfor) {
		super(parentShell);
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		this.id=selId;
		this.type=rptfor;
		objUtilMethods = new UtilMethods();

	}

	public Object open() {
		Shell parent = getParent();
		Shell shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setText("Report Dialog");
		shell.setSize(1000, 650);
		// Your code goes here (widget creation, set result, etc).
		createDialogArea(shell);
		shell.open();
		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		return rtn;
	}

	protected Control createDialogArea(Composite parent) {

		/*
		 * The top-most composite that holds title bar and the comMainContainer
		 * composite together.
		 */
		comParent = parent;
		// comParent = new Composite(parent, SWT.NONE);
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("Report Dialog", comParent,
				StockSalesReportView.class);

		// Main container that holds all other controls/widgets of the view
		comMainContainer = new Composite(comParent, SWT.NONE);
		comMainContainer.setLayout(new FormLayout());
		GridData gd_comMainContainer = new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1);
		gd_comMainContainer.widthHint = 983;
		gd_comMainContainer.heightHint = 561;
		comMainContainer.setLayoutData(gd_comMainContainer);

		// creating the viewer composite to view the reports
		viewerComposite = new ViewerComposite(comMainContainer, SWT.BORDER);
		FormData fd_viewerComposite = new FormData();
		fd_viewerComposite.left = new FormAttachment(100, -973);
		fd_viewerComposite.right = new FormAttachment(100, -10);
		fd_viewerComposite.top = new FormAttachment(0, 10);
		fd_viewerComposite.bottom = new FormAttachment(0, 551);
		viewerComposite.setLayoutData(fd_viewerComposite);

		JasperPrint jprint = null;
		jprint = generateReport(id);

		// Loading the JasperPrint object in the viewerComposite
		viewerComposite.getReportViewer().setDocument(jprint);
		// viewerComposite.pack();
		viewerComposite.setEnabled(true);

		return comMainContainer;

	}

	/**
	 * generateReport() generates the final JPrint ready to be outputted and
	 * returns the same.
	 * 
	 */
	@SuppressWarnings("deprecation")
	public static JasperPrint generateReport(int id) {
		Connection conn = null;
		JasperPrint myJPrint = null;

		try {

			conn = DbConnection.getConnection();

			// Loading my jasper file
			JasperDesign jasperDesign = null;
			JasperReport jasperReport = null;

			jasperDesign = JasperManager.loadXmlDesign(StockSalesReportView.class
					.getClassLoader()
					.getResourceAsStream("stockout.jrxml"));

			jasperReport = JasperCompileManager.compileReport(jasperDesign);

			// Passing parameters to the report
			@SuppressWarnings("rawtypes")
			Map<String, Object> params = new HashMap();
			params.put(
					"REPORT_DIR",
					StockSalesReportView.class.getClassLoader()
							.getResource("stockout.jrxml").toString()
							.replace("stockout.jrxml", ""));
			params.put("sales_id", id);

			/*
			 * Filling the report with data from the database based on the
			 * parameters passed.
			 */
			myJPrint = JasperFillManager.fillReport(jasperReport, params, conn);
			params.clear();

		} catch (JRException ex) {
			ex.printStackTrace();
		}

		return myJPrint;

	}
}
