package com.tia.inventory.view;



import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.view.JournalVoucher;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.BeforeSaveMessages;
import com.tia.common.util.CmbQry;
import com.tia.common.util.DecimalTextPositiveOnly;
import com.tia.common.util.IntegerTextPositiveOnly;
import com.tia.common.util.SearchBox;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;

import com.tia.inventory.controller.DayBookController;
import com.tia.inventory.controller.StockItemController;
import com.tia.inventory.model.DayBookDetailsModel;
import com.tia.inventory.model.DayBookModel;
import com.tia.inventory.model.StockItemModel;
import com.tia.master.controller.UnitMapController;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.nebula.widgets.datechooser.DateChooserCombo;

public class DayBookEntryView extends Dialog {
	public static String StockEntry_ViewId = "tia.dayBook.entry";
	private Composite comParent;
	private Composite comMainContainer;
	private Text txtReceiptNo;
	private Text txtIncome;
	private Text txtExpense;
	private Label lblCellNo;
	private Label lblAddress;
	private Text txtRemarks;
	private Group grpCustomerDetails;
	private Label lblName;
	private Label lblPhone;
	private Label lblContactPerson;

	private DayBookController objController;
	private DayBookModel objModel;
	private DayBookDetailsModel objDetailModel;
	private SimpleDateFormat sdf;
	private UtilMethods objUtilMethods;
	private static DayBookEntryView stockEntryView;
	private boolean uglyFlag;
	private Button btnSave;
	private Button btnClear;
	private SwtBndCombo bcmbTrxnFor;
	private int base_pur_unitId=0;
	private int whole_pur_unitId=0;
	private int itemId=0;
	private String form="main";
	private BeforeSaveMessages msg=new BeforeSaveMessages();
	Object rtn;
	private Shell shell;
	private DateChooserCombo txtDate;
	
	public DayBookEntryView(Shell parentShell, int purId,String forms) {
		//setTitleImage(SWTResourceManager.getImage(StockEntryView.class, "/customer16.png"));
		super(parentShell);
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		objController = new DayBookController();
		objModel = new DayBookModel();
		objDetailModel=new DayBookDetailsModel();

		objUtilMethods = new UtilMethods();
		stockEntryView=this;
		this.itemId=purId;
		this.form=forms;

	}
//	public StockEntryView(Shell parentShell) {
//		super(parentShell);
//		
//		
//	}
	public static DayBookEntryView getInstance() {
		return stockEntryView;
	}
	
	public Object open() {
		Shell parent = getParent();
		shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setText("Day Book Entry");
		shell.setSize(1000, 296);
		// Your code goes here (widget creation, set result, etc).
		createDialogArea(shell);
		shell.open();
		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		return rtn;
	}
	// @Override
	@SuppressWarnings("deprecation")
	public Control createDialogArea(Composite parent) {

		/*
		 * The top-most composite that holds title bar and the comMainContainer
		 * composite together.
		 */
		comParent = parent;
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("Day Book Entry", comParent,
				StockEntryView.class);

		// Main container that holds all other controls/widgets of the view
		comMainContainer = new Composite(comParent, SWT.NONE);
		comMainContainer.setLayout(new FormLayout());
		GridData gd_comMainContainer = new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1);
		gd_comMainContainer.widthHint = 983;
		gd_comMainContainer.heightHint = 257;
		comMainContainer.setLayoutData(gd_comMainContainer);

		// Group holding widgets to enter the customer details
		grpCustomerDetails = new Group(comMainContainer, SWT.NONE);
		FormData fd_grpCustomerDetails = new FormData();
		fd_grpCustomerDetails.top = new FormAttachment(0, 10);
		fd_grpCustomerDetails.left = new FormAttachment(0, 10);
		fd_grpCustomerDetails.bottom = new FormAttachment(0, 208);
		fd_grpCustomerDetails.right = new FormAttachment(0, 973);
		grpCustomerDetails.setLayoutData(fd_grpCustomerDetails);
		grpCustomerDetails.setText("Item Details");

		// Name label
		lblName = new Label(grpCustomerDetails, SWT.NONE);
		lblName.setAlignment(SWT.RIGHT);
		lblName.setBounds(326, 28, 75, 18);
		lblName.setText("Receipt No:");
		lblName.setForeground(UtilMethods.getFontForegroundColorRed());

		// Phone label
		lblPhone = new Label(grpCustomerDetails, SWT.NONE);
		lblPhone.setAlignment(SWT.RIGHT);
		lblPhone.setBounds(673, 31, 55, 15);
		lblPhone.setText("Income:");
		lblPhone.setForeground(UtilMethods.getFontForegroundColorRed());

		// Contact Person label
		lblContactPerson = new Label(grpCustomerDetails, SWT.NONE);
		lblContactPerson.setAlignment(SWT.RIGHT);
		lblContactPerson.setBounds(10, 62, 75, 18);
		lblContactPerson.setText("Expense:");
		lblContactPerson.setForeground(UtilMethods.getFontForegroundColorRed());

		// Name text widget
		txtReceiptNo = new Text(grpCustomerDetails, SWT.BORDER);
		txtReceiptNo.setEditable(false);
		txtReceiptNo.setBounds(407, 25, 219, 21);
		txtReceiptNo.setText(String.valueOf(UtilMethods.getMaxId("day_book", "receipt_no")));

		// Phone text widget
		txtIncome = new Text(grpCustomerDetails, SWT.BORDER);
		txtIncome.setBounds(734, 28, 219, 21);
		txtIncome.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtIncome, 20));

		// Contact Person text widget
		txtExpense = new Text(grpCustomerDetails, SWT.BORDER);
		txtExpense.setBounds(91, 59, 219, 21);
		txtExpense.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtExpense, 20));

		// Cell No. label
		lblCellNo = new Label(grpCustomerDetails, SWT.NONE);
		lblCellNo.setAlignment(SWT.RIGHT);
		lblCellNo.setBounds(326, 60, 76, 15);
		lblCellNo.setText("Trxn For:");
		lblCellNo.setForeground(UtilMethods.getFontForegroundColorRed());

		// Address label
		lblAddress = new Label(grpCustomerDetails, SWT.NONE);
		lblAddress.setAlignment(SWT.RIGHT);
		lblAddress.setBounds(673, 65, 55, 15);
		lblAddress.setText("Remarks:");

		// Address text widget
		txtRemarks = new Text(grpCustomerDetails, SWT.BORDER | SWT.WRAP);
		txtRemarks.setBounds(734, 60, 219, 45);
		
		bcmbTrxnFor = new SwtBndCombo(grpCustomerDetails, SWT.READ_ONLY);
		bcmbTrxnFor.setBounds(407, 57, 219, 23);
		bcmbTrxnFor.fillData(CmbQry.getOtherTrxn(), CmbQry.getLabel(), CmbQry.getId(), DbConnection.getConnection());
		
		
		
		btnSave = new Button(grpCustomerDetails, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				beforeSave();
				
				if(msg.returnMessages())
				{
					saveAction();
				}
			}
		});
		btnSave.setBounds(410, 143, 75, 25);
		btnSave.setText("Save");
		
		btnClear = new Button(grpCustomerDetails, SWT.NONE);
		btnClear.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clearAll();
				
				
			}
		});
		btnClear.setBounds(500, 143, 75, 25);
		btnClear.setText("clear");
		
		txtDate = new DateChooserCombo(grpCustomerDetails, SWT.NONE);
		txtDate.setBounds(91, 28, 219, 17);
		txtDate.setValue(new Date());
		
		Label lblDate = new Label(grpCustomerDetails, SWT.NONE);
		lblDate.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		lblDate.setAlignment(SWT.RIGHT);
		lblDate.setBounds(30, 28, 55, 15);
		lblDate.setText("Date: ");

		// fill the table
		loadData(itemId);
		return comParent;
	}

	
private void beforeSave()
{
	if(txtDate.getValue()==null)
	{
		msg.setMandatoryMessages("Please select purchased date");
	}
	
	if(txtReceiptNo.getText().equals(""))
	{
		msg.setMandatoryMessages("Receipt No is required");
	}
	if(txtIncome.getText().equals("") && txtExpense.getText().equals("") )
	{
		msg.setMandatoryMessages("Income/Expense is required");
	}
	else
	{
		if(itemId <= 0)
		{
			if(UtilFxn.isNotUnique("select * from day_book where receipt_no='"+txtReceiptNo.getText()+"'"))
			{
				msg.setMandatoryMessages("Receipt No you entered is already in use");
			}
		}
		else
		{
			if(UtilFxn.isNotUnique("select * from day_book where receipt_no='"+txtReceiptNo.getText()+"' and day_book_id !='"+itemId+"'"))
			{
				msg.setMandatoryMessages("Receipt no you entered is already in use");
			}
		}
	}
	
	if(bcmbTrxnFor.getSelectedBoundValue().equals("0"))
	{
		msg.setMandatoryMessages("Trxn For is required");
	}
	
	
	
	
	
	
//	else
//	{
//		if(txtSellingPrice.getText().equals(""))
//		{
//			msg.setMandatoryMessages("Selling price is required");
//		}
//	}
	
	
		
	
	
}
	
private void clearAll()
{
	txtReceiptNo.setText("");
	txtIncome.setText("");
	txtExpense.setText("");
	bcmbTrxnFor.setSelectedBoundValue("0");
	
	
	txtRemarks.setText("");
	
	
}
	
private void saveAction()
{
	
	objModel.setDay_book_id(itemId);
	
	objDetailModel.setDay_book_id(itemId);
	
	objModel.setReceipt_no(Integer.valueOf(txtReceiptNo.getText()));
	objModel.setIncome(Double.parseDouble(txtIncome.getText().equals("")?"0":txtIncome.getText()));
	objDetailModel.setIncome(Double.parseDouble(txtIncome.getText().equals("")?"0":txtIncome.getText()));
	objModel.setExpense(Double.parseDouble(txtIncome.getText().equals("")?"0":txtExpense.getText()));
	objDetailModel.setExpense(Double.parseDouble(txtIncome.getText().equals("")?"0":txtExpense.getText()));
	objModel.setRemarks(txtRemarks.getText());
	
	objModel.setTrxn_type(Integer.valueOf(bcmbTrxnFor.getSelectedBoundValue().toString()));
	
	objModel.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
	objModel.setDept_id(0);
	objModel.setTrxn_other_id(Integer.valueOf(bcmbTrxnFor.getSelectedBoundValue().toString()));
	
	objDetailModel.setTrxn_with_id(0);
	objDetailModel.setTrxn_with_type_id(Integer.valueOf(bcmbTrxnFor.getSelectedBoundValue().toString()));
	objModel.setTrxn_date_ad(txtDate.getText());
	
	if(itemId <= 0)
	{
		objModel.setCreated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		objModel.setCreated_dt(JCalendarFunctions.currentDate());
		objModel.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		objModel.setUpdt_dt(JCalendarFunctions.currentDate());
		objModel.setUpdt_cnt(0);
		objModel.setTrxn_date_bs("0000-00-00");
		
		//objModel.setTrxn_date_ad(JCalendarFunctions.currentDate());
		
		objDetailModel.setCreated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		objDetailModel.setCreated_dt(JCalendarFunctions.currentDate());
		objDetailModel.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		objDetailModel.setUpdt_dt(JCalendarFunctions.currentDate());
		objDetailModel.setUpdt_cnt(0);
		
		
		
	}
	else
	{
		objModel.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		objModel.setUpdt_dt(JCalendarFunctions.currentDate());
		objModel.setUpdt_cnt(objModel.getUpdt_cnt()+1);
		
		objDetailModel.setDay_book_id(itemId);
		
		objDetailModel.setUpdt_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		objDetailModel.setUpdt_dt(JCalendarFunctions.currentDate());
		objDetailModel.setUpdt_cnt(objModel.getUpdt_cnt()+1);
	}
	
	
	
	
	try
	{
		Connection cnn=DbConnection.getConnection();
		cnn.setAutoCommit(false);
		if(objController.saveItemDetails(objModel,objDetailModel))
		{
			if(itemId > 0)
			{
				msg.showIndividualMessage("Day Book updated successfully", 0);
				
			}
			else
			{
				msg.showIndividualMessage("Day Book inserted successfully", 0);
			}
			cnn.commit();
			//clearAll();
			IWorkbenchPage wbp = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			try {
				if(form.equals("main"))
				{
					wbp.showView("com.tia.account.journalVoucher");
					JournalVoucher.getInstance().refereshTable();;
				}
				
				
				shell.close();
				
		         } catch (Exception e1) {
				e1.printStackTrace();
			     }
		}
		else
		{
			msg.showIndividualMessage("Some error occured.Please try again", 1);
			cnn.rollback();
			cnn.setAutoCommit(true);
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
}
	

	public void loadData(int itemId)
	{
		this.itemId=itemId;
		ResultSet rs=objController.fetchItemDetails(itemId);
		
		try {
			if(rs.next())
			{
				txtDate.setValue(rs.getDate("trxn_date_ad"));
				txtReceiptNo.setText(String.valueOf(rs.getInt("receipt_no")));
				txtIncome.setText(String.valueOf(rs.getBigDecimal("income")==null?"":rs.getBigDecimal("income")));
				txtExpense.setText(String.valueOf(rs.getBigDecimal("expense")==null?"":rs.getBigDecimal("expense")));
				txtRemarks.setText(rs.getString("remarks"));
				bcmbTrxnFor.setSelectedBoundValue(rs.getInt("trxn_type"));	
				objModel.setUpdt_cnt(rs.getInt("updt_cnt"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
