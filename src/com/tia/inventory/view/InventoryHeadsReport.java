package com.tia.inventory.view;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.controller.AccHeadController;
import com.tia.account.controller.LedgerController;
import com.tia.account.model.AccHeadModel;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.BeforeSaveMessages;
import com.tia.common.util.CmbQry;
import com.tia.common.util.DecimalTextPositiveOnly;
import com.tia.common.util.LoginCheck;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.inventory.controller.DayBookController;
import com.tia.inventory.controller.StockCategoryController;
import com.tia.inventory.controller.StockItemController;
import com.tia.inventory.controller.StockPurchasedController;
import com.tia.inventory.model.StockCategoryModel;
import com.tia.plugins.DynaTree.controller.DynaTree;
import com.tia.plugins.DynaTree.model.TreeNode;
import com.tia.plugins.DynaTree.ui.TreeNodeLabelProvider;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

public class InventoryHeadsReport extends ViewPart {
	private Composite comMain;
	

	protected TreeViewer treeViewer;
	protected TreeNode root;
	protected TreeNodeLabelProvider labelProvider;
	protected Action onlyBoardGamesAction, atLeatThreeItems;
	protected Action booksBoxesGamesAction, noArticleAction;
	protected Action addTreeNodeAction, removeTreeNodeAction, addLeafAction,
			removeLeafAction;
	protected ViewerFilter onlyBoardGamesFilter, atLeastThreeFilter;
	protected ViewerSorter booksBoxesGamesSorter, noArticleSorter;

	private static java.util.List<AccHeadModel> listHead;

	int maxSubAccId = 0;
	DayBookController control;
	private DecimalFormat df = new DecimalFormat("#,##0.00");
	
	BeforeSaveMessages msg = new BeforeSaveMessages();
	private Tree table;
	private static InventoryHeadsReport stockListView;


	private Shell shell;

	/**
	 * Default Constructor
	 */
	public InventoryHeadsReport() {
		control=new DayBookController();
		

		shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();

		setTitleImage(SWTResourceManager.getImage(StockListView.class,
				"/product16.png"));
		stockListView=this;
	}
	public static InventoryHeadsReport getInstance() {
		return stockListView;
	}

	@Override
	public void createPartControl(Composite parent) {
		// set the Gridlayout to the main form
		GridLayout gl_parent1 = new GridLayout(1, false);
		gl_parent1.marginHeight = 1;
		parent.setLayout(gl_parent1);
		Banner.getTitle("Inventory Transaction Summary", parent, StockCategoryView.class);

		// Main composite holding other controls and set Grid Layout to it too
		comMain = new Composite(parent, SWT.NONE);
		GridData gd_comMain = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_comMain.heightHint = 613;
		gd_comMain.widthHint = 979;
		comMain.setBounds(5, 5, 970, 590);
		comMain.setLayout(null);
		comMain.setLayoutData(gd_comMain);
		
		table = new Tree(comMain, SWT.BORDER | SWT.FULL_SELECTION);
		table.setBounds(5, 10, 974, 508);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		
		
		TreeColumn tblclmnSn = new TreeColumn(table, SWT.NONE);
		tblclmnSn.setWidth(42);
		tblclmnSn.setText("S.N.");
		
		TreeColumn tblclmnName = new TreeColumn(table, SWT.NONE);
		tblclmnName.setWidth(307);
		tblclmnName.setText("Name");
		
		TreeColumn tblclmnCode = new TreeColumn(table, SWT.NONE);
		tblclmnCode.setText("-");
		tblclmnCode.setWidth(130);
		
		TreeColumn tblclmnSellingPrice = new TreeColumn(table, SWT.RIGHT);
		tblclmnSellingPrice.setWidth(139);
		tblclmnSellingPrice.setText("Amount");

		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		Label label = new Label(parent, SWT.NONE);
		label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false,
				1, 1));
		
		LoadData("");
	}// end of method createPartControl()

	
	public void openView(int itemId){
		IWorkbenchPage wbp = PlatformUI.getWorkbench()
		.getActiveWorkbenchWindow().getActivePage();

		try {
			//wbp.showView("com.tia.inven.stockEntry");
			String form="main";
			System.out.println("The itemid is :"+itemId);
			//StockEntryView.getInstance().loadData(itemId);
			StockEntryView hms = new StockEntryView(
					PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getShell(),itemId,form);
			hms.open();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		
	}
	
	
	private void LoadData(String cond) {
		table.removeAll();
		
		//cash accounts
		ResultSet cashAcc=control.getCashBankAccounts("cash");
		ResultSet bankAcc=control.getCashBankAccounts("bank");
		ResultSet remaining=control.getPurchasedRemainingDetails();
		ResultSet remainingSold=control.getSoldRemainingDetails();
		
			TreeItem item=new TreeItem(table,  SWT.NONE);
			item.setText(0,"1");
			item.setText(1,"Cash");
			int id=0;
			double amount=0;
			//cash
			try {
				if(UtilFxn.getRowCount(cashAcc) > 0)
				{
					
					while(cashAcc.next())
					{
						TreeItem subItem = new TreeItem(item, SWT.NONE);
						subItem.setText(2,cashAcc.getString("cv_lbl"));
						id=cashAcc.getInt("cv_id");
						amount=control.getHeadAmounts(id);
						subItem.setText(3,df.format(amount));
						
					}
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//cash end
			
			
			TreeItem item2=new TreeItem(table,  SWT.NONE);
			item2.setText(0,"2");
			item2.setText(1,"Bank");
			//bank
			try {
				if(UtilFxn.getRowCount(bankAcc) > 0)
				{
					
					while(bankAcc.next())
					{
						TreeItem subItem = new TreeItem(item2, SWT.NONE);
						subItem.setText(2,bankAcc.getString("cv_lbl"));
						id=bankAcc.getInt("cv_id");
						amount=control.getHeadAmounts(id);
						subItem.setText(3,df.format(amount));
						
					}
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//bank end
			
			
			//remaining
			TreeItem item3=new TreeItem(table,  SWT.NONE);
			item3.setText(0,"3");
			item3.setText(1,"Amount Payable To Suppliers");
			
			try {
				if(UtilFxn.getRowCount(remaining) > 0)
				{
					
					while(remaining.next())
					{
						TreeItem subItem = new TreeItem(item3, SWT.NONE);
						subItem.setText(2,remaining.getString("name"));
						subItem.setText(3,df.format(remaining.getDouble("amount")));
						
					}
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//remaining end
			
			//remaining from customer
			TreeItem item4=new TreeItem(table,  SWT.NONE);
			item4.setText(0,"4");
			item4.setText(1,"Amount receivable from customer");
			
			try {
				if(UtilFxn.getRowCount(remainingSold) > 0)
				{
					
					while(remainingSold.next())
					{
						TreeItem subItem = new TreeItem(item4, SWT.NONE);
						subItem.setText(2,remainingSold.getString("name"));
						subItem.setText(3,df.format(remainingSold.getDouble("amount")));
						
					}
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//remaining customer end
			
		//third party earned
			ResultSet earned=control.getThirdPartyEarnedPaid("earned");
			TreeItem item5=new TreeItem(table,  SWT.NONE);
			item5.setText(0,"5");
			item5.setText(1,"Third Party Earned");
			
			try {
				if(UtilFxn.getRowCount(earned) > 0)
				{
					
					while(earned.next())
					{
						TreeItem subItem = new TreeItem(item5, SWT.NONE);
						subItem.setText(2,earned.getString("name"));
						subItem.setText(3,df.format(earned.getDouble("earned")));
						
					}
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//third party earned end
			
			//third party paid
			ResultSet paid=control.getThirdPartyEarnedPaid("paid");
			TreeItem item6=new TreeItem(table,  SWT.NONE);
			item6.setText(0,"6");
			item6.setText(1,"Third Party Paid");
			
			try {
				if(UtilFxn.getRowCount(paid) > 0)
				{
					
					while(paid.next())
					{
						TreeItem subItem = new TreeItem(item6, SWT.NONE);
						subItem.setText(2,paid.getString("name"));
						subItem.setText(3,df.format(paid.getDouble("paid")));
						
					}
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//third party earned end
			
		//discount given 
			TreeItem item7=new TreeItem(table,  SWT.NONE);
			item7.setText(0,"7");
			item7.setText(1,"Discount on sale");
			item7.setText(3,df.format(control.getDiscountEarned()));
		//end discount given
			
		//tax received
			TreeItem item8=new TreeItem(table,  SWT.NONE);
			item8.setText(0,"8");
			item8.setText(1,"Tax received on sale");
			item8.setText(3,df.format(control.getTaxEarned()));
		//tax received end
			
			//total stock amount
			TreeItem item9=new TreeItem(table,  SWT.NONE);
			item9.setText(0,"9");
			item9.setText(1,"Total stock purcased amount");
			item9.setText(3,df.format(control.getTotalStockAmount()));
			//end total stock amount
			
			//total stock amount
			TreeItem item10=new TreeItem(table,  SWT.NONE);
			item10.setText(0,"10");
			item10.setText(1,"Total sold amount");
			item10.setText(3,df.format(control.getTotalSoldAmount()));
			//end total stock amount
		
	}
	
	
	public void refereshTable()
	{
		LoadData("");
	}

	@Override
	public void setFocus() {

	}
}// end of class


