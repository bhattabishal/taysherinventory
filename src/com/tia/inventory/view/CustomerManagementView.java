package com.tia.inventory.view;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.BeforeSaveMessages;
import com.tia.common.util.CmbQry;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.inventory.controller.CustomerSupplierController;
import com.tia.inventory.model.CustomerModelClass;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import com.tia.user.controller.encryptDectyptPassword;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class CustomerManagementView extends Dialog {
	private Text txtFullName;
	private Text txtMobile;
	private UtilMethods objUtilMethods;
	private Composite comParent;
	private Composite comMainContainer;
	Object rtn;
	private Text txtPhone;
	private Text txtContactPerson;
	private Text txtEmail;
	private Text txtRemarks;
	private Text txtAddress;
	private SwtBndCombo bcmbType;
	int id=0;
	String form="main";
	private BeforeSaveMessages msg;
	private CustomerSupplierController control;
	private CustomerModelClass model;
	private Shell shell;

	public CustomerManagementView(Shell parentShell,int id,String opnForm) {
		super(parentShell);
		objUtilMethods = new UtilMethods();
		this.id=id;
		this.form=opnForm;
		msg = new BeforeSaveMessages();
		control=new CustomerSupplierController();
		model=new CustomerModelClass();

	}

	public Object open() {
		Shell parent = getParent();
		shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setText("Customer/Supplier/Service Provider Management");
		shell.setSize(590, 350);
		// Your code goes here (widget creation, set result, etc).
		createDialogArea(shell);
		shell.open();
		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		
		return rtn;
	}

	protected Control createDialogArea(Composite parent) {
		comParent = parent;// (Composite) super.createDialogArea(parent);
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("Customer/Supplier/Service Provider Management", comParent,
				CustomerManagementView.class);

		// Main container that holds all other controls/widgets of the view
		comMainContainer = new Composite(comParent, SWT.NONE);
		comMainContainer.setLayout(new FormLayout());
		GridData gd_comMainContainer = new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1);
		gd_comMainContainer.widthHint = 983;
		gd_comMainContainer.heightHint = 561;
		comMainContainer.setLayoutData(gd_comMainContainer);

		Group grpPassword = new Group(comMainContainer, SWT.NONE);
		FormData fd_grpPassword = new FormData();
		fd_grpPassword.bottom = new FormAttachment(0, 282);
		fd_grpPassword.top = new FormAttachment(0);
		fd_grpPassword.left = new FormAttachment(0, 10);
		fd_grpPassword.right = new FormAttachment(0, 575);
		grpPassword.setLayoutData(fd_grpPassword);
		grpPassword.setText("Customer/Supplier/Service Provider  Management");
		grpPassword.setBounds(206, 25, 388, 282);

		Button btnChangePassoword = new Button(grpPassword, SWT.NONE);
		btnChangePassoword.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				beforeSave();
				if(msg.returnMessages())
				{
					model=new CustomerModelClass();
					model.setCustomer_id(id);
					model.setAddress(txtAddress.getText());
					model.setContact_person(txtContactPerson.getText());
					if(id <= 0)
					model.setCreated_dt(JCalendarFunctions.currentDate());
					
					model.setEmail(txtEmail.getText());
					model.setMobile(txtMobile.getText());
					model.setName(txtFullName.getText());
					model.setPhone(txtPhone.getText());
					model.setRemarks(txtRemarks.getText());
					model.setType(Integer.valueOf(bcmbType.getSelectedBoundValue().toString()));
					
					Connection cnn=DbConnection.getConnection();
					try {
						cnn.setAutoCommit(false);
						if(control.saveDetails(model))
						{
							msg.showIndividualMessage("Successfully Saved/Updated.", 0);
							//clearAll();
							
							
							IWorkbenchPage wbp = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
							try {
								if(form.equals("main"))
								{
									wbp.showView("com.tia.inven.cusSuppList");
									CustomerSupplierListView.getInstance().refershTable();;
								}
								else if(form.equals("stockpurchase"))
								{
									//wbp.showView("com.tia.inven.stockPurchasedEntry");
									StockPurchasedEntryView.getInstance().reloadSupplier();
								}
								else if(form.equals("stocksales"))
								{
									StockSalesEntryView.getInstance().reloadCustomer();
								}
								else if(form.equals("stocksalesservice"))
								{
									StockSalesEntryView.getInstance().reloadServiceProviders();
								}
								
								shell.close();
								
						         } catch (PartInitException e1) {
								e1.printStackTrace();
							     }
						}
						else
						{
							msg.showIndividualMessage("Some error occured while saving.Please try again.", 0);
						}
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		btnChangePassoword.setBounds(130, 247, 120, 25);
	
		btnChangePassoword.setText("Save");

		Button btnCancel = new Button(grpPassword, SWT.NONE);
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clearAll();
			}
		});
		btnCancel.setBounds(266, 247, 114, 25);
		
		btnCancel.setText("Clear");

		Label lblConformPassword = new Label(grpPassword, SWT.NONE);
		lblConformPassword.setBounds(295, 89, 114, 15);
		lblConformPassword.setText("Mobile:  ");
		lblConformPassword.setAlignment(SWT.RIGHT);

		txtMobile = new Text(grpPassword, SWT.BORDER);
		txtMobile.setBounds(415, 87, 136, 21);

		txtFullName = new Text(grpPassword, SWT.BORDER);
		txtFullName.setBounds(130, 49, 136, 21);

		Label lblCurrentPassword = new Label(grpPassword, SWT.NONE);
		lblCurrentPassword.setBounds(10, 52, 114, 15);
		lblCurrentPassword.setText("Full Name: ");
		lblCurrentPassword.setAlignment(SWT.RIGHT);
		lblCurrentPassword.setForeground(UtilMethods.getFontForegroundColorRed());
		
		Label label = new Label(grpPassword, SWT.NONE);
		label.setText("Phone: ");
		label.setAlignment(SWT.RIGHT);
		label.setBounds(295, 52, 114, 15);
		
		txtPhone = new Text(grpPassword, SWT.BORDER);
		txtPhone.setBounds(415, 49, 136, 21);
		
		Label label_1 = new Label(grpPassword, SWT.NONE);
		label_1.setText("Contact Person: ");
		label_1.setAlignment(SWT.RIGHT);
		label_1.setBounds(10, 89, 114, 15);
		
		txtContactPerson = new Text(grpPassword, SWT.BORDER);
		txtContactPerson.setBounds(130, 87, 136, 21);
		
		Label lblEmail = new Label(grpPassword, SWT.NONE);
		lblEmail.setText("Email:  ");
		lblEmail.setAlignment(SWT.RIGHT);
		lblEmail.setBounds(10, 126, 114, 15);
		
		txtEmail = new Text(grpPassword, SWT.BORDER);
		txtEmail.setBounds(130, 124, 136, 21);
		
		Label lblAddress = new Label(grpPassword, SWT.NONE);
		lblAddress.setBounds(354, 161, 55, 15);
		lblAddress.setText("Remarks:  ");
		
		txtRemarks = new Text(grpPassword, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		txtRemarks.setBounds(415, 161, 136, 66);
		
		Label lblRemarks = new Label(grpPassword, SWT.NONE);
		lblRemarks.setText("Address:   ");
		lblRemarks.setAlignment(SWT.RIGHT);
		lblRemarks.setBounds(10, 161, 114, 15);
		
		txtAddress = new Text(grpPassword, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.PASSWORD | SWT.MULTI);
		txtAddress.setBounds(130, 161, 136, 66);
		
		Label lblType = new Label(grpPassword, SWT.NONE);
		lblType.setText("Type:  ");
		lblType.setAlignment(SWT.RIGHT);
		lblType.setBounds(295, 126, 114, 15);
		lblType.setForeground(UtilMethods.getFontForegroundColorRed());
		
		bcmbType = new SwtBndCombo(grpPassword, SWT.READ_ONLY);
		bcmbType.setBounds(415, 124, 136, 21);
		bcmbType.fillData(CmbQry.getCusSupType(), CmbQry.getLabel(),
				CmbQry.getId(), DbConnection.getConnection(), true);
		if(form.equals("stocksales"))
		{
			bcmbType.setSelectedBoundValue("1");
		}
		else if(form.equals("stockpurchase"))
		{
			bcmbType.setSelectedBoundValue("2");
		}
		else if(form.equals("stocksalesservice"))
		{
			bcmbType.setSelectedBoundValue("3");
		}

		grpPassword.setTabList(new Control[]{txtFullName, txtMobile, btnChangePassoword, btnCancel});
		loadData();

		return comParent;

		// TODO Auto-generated method stub

	}
	
	private void loadData()
	{
		model=control.fetchItemDetails(id);
		if(model !=null)
		{
			txtAddress.setText(model.getAddress());
			txtContactPerson.setText(model.getContact_person());
			txtEmail.setText(model.getEmail());
			txtFullName.setText(model.getName());
			txtMobile.setText(model.getMobile());
			txtPhone.setText(model.getPhone());
			txtRemarks.setText(model.getRemarks());
			bcmbType.setSelectedBoundValue(model.getType());
		}
		
	}
	private void clearAll()
	{
		txtAddress.setText("");
		txtContactPerson.setText("");
		txtEmail.setText("");
		txtFullName.setText("");
		txtMobile.setText("");
		txtPhone.setText("");
		txtRemarks.setText("");
		bcmbType.setSelectedBoundValue(0);
	}
	
	private void beforeSave()
	{
		if(txtFullName.getText().equals(""))
		{
			msg.setMandatoryMessages("Full Name is required");
		}
		else
		{
			if(id <= 0)
			{
				if(control.fetchCheckByCondition("name='"+txtFullName.getText()+"'"))
				{
					msg.setMandatoryMessages("Name you entered already exists");
				}
			}
			else
			{
				if(control.fetchCheckByCondition("name='"+txtFullName.getText()+"' AND customer_id !='"+id+"'"))
				{
					msg.setMandatoryMessages("Name you entered already exists");
				}
			}
		}
		if(bcmbType.getSelectedBoundValue().toString().equals("0"))
		{
			msg.setMandatoryMessages("Please select type as customer or supplier.");
		}
	}
}

