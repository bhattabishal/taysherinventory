package com.tia.inventory.view;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.GridLayout;

import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.db.DbConnection;
import com.tia.common.util.Banner;
import com.tia.common.util.BeforeSaveMessages;
import com.tia.common.util.DecimalTextPositiveOnly;
import com.tia.common.util.UtilMethods;
import com.tia.common.util.Validator;
import com.tia.inventory.controller.ServiceRecordController;
import com.tia.inventory.model.ServiceRecordModel;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;
import com.tia.user.controller.UserManagementController;
import com.tia.user.controller.encryptDectyptPassword;
import com.tia.user.model.UserManagementModel;
import org.eclipse.nebula.widgets.datechooser.DateChooserCombo;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;

public class ServiceRecordManagementView extends ViewPart {
	private Text txtCarNo;
	private Text txtServicingAt;
	private Text txtPhoneNo;
	private Text txtNextServicing;
	private Table tblDetails;
	private Text txtCustomer;
	private Button cmdNew;
	private Button cmdSave;
	private Button cmdCancel;
	private Button cmdEdit;
	private Button cmdDelete;
	private Composite comParent;
	Shell sh = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	ServiceRecordModel objUserManagemetnModel = new ServiceRecordModel();
	static Connection cnn = DbConnection.getConnection();
	Validator validator = new Validator();
	BeforeSaveMessages msg=new BeforeSaveMessages();
	ServiceRecordController objUserCntrl = new ServiceRecordController();
	JCalendarFunctions jcal=new JCalendarFunctions();
	private Text txtRemarks;
	private Text txtCarNoSearch;
	private Text txtCustomerSearch;
	private Text txtId;
	private DateChooserCombo bcmbDate;
	private Button btnCar;
	private Button btnCustomer;

	public ServiceRecordManagementView() {
		setTitleImage(SWTResourceManager.getImage(ServiceRecordManagementView.class, "/user16.png"));

	}

	@Override
	public void createPartControl(Composite parent) {

		comParent = new Composite(parent, SWT.NONE);
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("Service Record Book", comParent, ServiceRecordManagementView.class);

		Composite cmpMain = new Composite(comParent, SWT.NONE);
		Group grpPersonalInfo = new Group(cmpMain, SWT.NONE);
		grpPersonalInfo.setText("Personal Info");
		grpPersonalInfo.setBounds(10, 10, 972, 109);

		Label lblFullName = new Label(grpPersonalInfo, SWT.NONE);
		lblFullName.setAlignment(SWT.RIGHT);
		lblFullName.setBounds(13, 31, 85, 15);
		lblFullName.setText("Car No :");
		lblFullName.setForeground(UtilMethods.getFontForegroundColorRed());

		txtCarNo = new Text(grpPersonalInfo, SWT.BORDER);
		txtCarNo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(!txtCarNo.equals(""))
				{
					loadCarDetails(txtCarNo.getText());
				}
				else
				{
					txtServicingAt.setText("");
					txtCustomer.setText("");
					txtPhoneNo.setText("");
				}
				
			}
		});
		txtCarNo.setBounds(101, 28, 163, 21);
		txtCarNo.setEnabled(false);

		Label lblPhoneNo = new Label(grpPersonalInfo, SWT.NONE);
		lblPhoneNo.setText("Servicing At(km) :");
		lblPhoneNo.setAlignment(SWT.RIGHT);
		lblPhoneNo.setBounds(309, 31, 122, 15);
		lblPhoneNo.setForeground(UtilMethods.getFontForegroundColorRed());

		txtServicingAt = new Text(grpPersonalInfo, SWT.BORDER);
		txtServicingAt.setBounds(434, 28, 163, 21);
		txtServicingAt.setEnabled(false);
		txtServicingAt.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtServicingAt, 20));

		Label lblCellNo = new Label(grpPersonalInfo, SWT.NONE);
		lblCellNo.setText("Phone No :");
		lblCellNo.setAlignment(SWT.RIGHT);
		lblCellNo.setBounds(346, 60, 85, 15);

		txtPhoneNo = new Text(grpPersonalInfo, SWT.BORDER);
		txtPhoneNo.setBounds(434, 56, 163, 21);
		txtPhoneNo.setEnabled(false);

		Label lblAdress = new Label(grpPersonalInfo, SWT.NONE);
		lblAdress.setText("Next Servicing(km):");
		lblAdress.setAlignment(SWT.RIGHT);
		lblAdress.setBounds(615, 32, 176, 15);
		lblAdress.setForeground(UtilMethods.getFontForegroundColorRed());

		txtNextServicing = new Text(grpPersonalInfo, SWT.BORDER);
		txtNextServicing.setBounds(795, 28, 163, 21);
		txtNextServicing.setEnabled(false);
		txtNextServicing.addListener(SWT.Verify, new DecimalTextPositiveOnly(
				txtNextServicing, 20));

		Label lblEmail = new Label(grpPersonalInfo, SWT.NONE);
		lblEmail.setText("Customer Name:");
		lblEmail.setAlignment(SWT.RIGHT);
		lblEmail.setBounds(5, 58, 95, 15);
		lblEmail.setForeground(UtilMethods.getFontForegroundColorRed());

		txtCustomer = new Text(grpPersonalInfo, SWT.BORDER);
		txtCustomer.setBounds(101, 55, 163, 21);
		txtCustomer.setEnabled(false);
		
		txtRemarks = new Text(grpPersonalInfo, SWT.BORDER);
		txtRemarks.setBounds(795, 56, 163, 43);
		
		Label lblRemarks = new Label(grpPersonalInfo, SWT.NONE);
		lblRemarks.setAlignment(SWT.RIGHT);
		lblRemarks.setBounds(738, 59, 55, 15);
		lblRemarks.setText("Remarks: ");
		
		txtId = new Text(grpPersonalInfo, SWT.BORDER);
		txtId.setBounds(101, 82, 76, 21);
		txtId.setVisible(false);
		
		Label lblDate = new Label(grpPersonalInfo, SWT.RIGHT);
		lblDate.setBounds(40, 84, 55, 15);
		lblDate.setText("Date : ");
		
		bcmbDate = new DateChooserCombo(grpPersonalInfo, SWT.NONE);
		bcmbDate.setEnabled(false);
		bcmbDate.setBounds(101, 82, 163, 17);
		bcmbDate.setData(new Date());
		
		grpPersonalInfo.setTabList(new Control[] { txtCarNo, txtServicingAt,
				txtCustomer, txtPhoneNo, txtNextServicing });

		Group grpControls = new Group(cmpMain, SWT.NONE);
		grpControls.setBounds(692, 125, 290, 90);

		cmdNew = new Button(grpControls, SWT.NONE);
		cmdNew.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
					SetFieldDisable();
					SetFieldEnabled();
					cmdNew.setEnabled(false);
					cmdDelete.setEnabled(false);
					txtCarNo.forceFocus();
				 
			}
		});
		cmdNew.setBounds(21, 20, 75, 25);
		cmdNew.setText("New");

		cmdEdit = new Button(grpControls, SWT.NONE);
		cmdEdit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				
					SetFieldEnabled();
					cmdEdit.setEnabled(false);
					cmdNew.setEnabled(false);
				
			}
		});
		cmdEdit.setEnabled(false);
		cmdEdit.setText("Edit");
		cmdEdit.setBounds(108, 20, 75, 25);

		cmdDelete = new Button(grpControls, SWT.NONE);
		cmdDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				MessageBox quesBox = new MessageBox(sh, SWT.YES | SWT.NO);
				quesBox.setText("Confirm Delete");
				quesBox.setMessage("Are u sure u want to Delete?");

				if (quesBox.open() == SWT.YES) {

					int userId = Integer.parseInt(txtId.getText());
					if (objUserCntrl.Delete(userId))
					{
						String Message = "Sucessfully Deleted";
						MessageBoxInformation(Message);
						tblDetails.removeAll();
						FillTableDetails("");
						SetFieldDisable();
						cmdNew.setEnabled(true);
					}
				}
			}
		});
		cmdDelete.setEnabled(false);
		cmdDelete.setText("Delete");
		cmdDelete.setBounds(192, 20, 75, 25);

		cmdCancel = new Button(grpControls, SWT.NONE);
		cmdCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				SetFieldDisable();
				cmdNew.setEnabled(true);
			}
		});
		cmdCancel.setText("Cancel");
		cmdCancel.setBounds(155, 56, 75, 25);

		cmdSave = new Button(grpControls, SWT.NONE);
		cmdSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (checkSave()) {
					int role;
					MessageBox quesBox = new MessageBox(sh, SWT.YES | SWT.NO);
					quesBox.setMessage("Confirm Save");
					quesBox.setMessage("Are u sure u want to Save?");

					
					objUserManagemetnModel.setNext_servicing_at_km(txtNextServicing.getText());
					objUserManagemetnModel.setPhone_no(txtPhoneNo.getText());
					objUserManagemetnModel.setCustomer_name(txtCustomer.getText());
					objUserManagemetnModel.setServicing_at_km(txtServicingAt.getText());
					objUserManagemetnModel.setCar_no(txtCarNo.getText());
					objUserManagemetnModel.setRemarks(txtRemarks.getText());
					objUserManagemetnModel.setCrdt_dt(bcmbDate.getText());
					try {
						String strQry = "";
						java.sql.Statement st = cnn.createStatement(
								ResultSet.TYPE_SCROLL_SENSITIVE,
								ResultSet.CONCUR_UPDATABLE);
						if (!txtId.getText().equals("")) {
							
							objUserManagemetnModel.z_WhereClause
									.service_record_id(Integer.parseInt(txtId
											.getText()));
							strQry = objUserManagemetnModel.Update();
						} else {
							
							
							strQry = objUserManagemetnModel.Insert();
						}
						System.out.println(strQry);
						st.executeUpdate(strQry);
						String message = "sucessfully Saved/Updated";
						MessageBoxInformation(message);
						tblDetails.removeAll();
						FillTableDetails("");
						SetFieldDisable();
						cmdNew.setEnabled(true);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			}
		});
		cmdSave.setEnabled(false);
		cmdSave.setBounds(68, 56, 75, 25);
		cmdSave.setText("Save");

		tblDetails = new Table(cmpMain, SWT.BORDER | SWT.FULL_SELECTION);
		tblDetails.setLocation(10, 223);
		tblDetails.setSize(972, 236);
		tblDetails.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				cmdSave.setEnabled(false);
				SetFieldDisable();
				 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");;
				txtCarNo.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(2));
				txtServicingAt.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(5));
				txtPhoneNo.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(4));
				txtNextServicing.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(6));
				txtRemarks.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(7));
				txtCustomer.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(3));
				try {
					bcmbDate.setValue(sdf.parse(tblDetails.getItem(
							tblDetails.getSelectionIndex()).getText(8)));
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				txtId.setText(tblDetails.getItem(
						tblDetails.getSelectionIndex()).getText(0));
				cmdEdit.setEnabled(true);
			}
		});
		tblDetails.setHeaderVisible(true);
		tblDetails.setLinesVisible(true);
		TableColumn tableColumn = new TableColumn(tblDetails, SWT.NONE);
		tableColumn.setWidth(0);
		tableColumn.setText("UserId");
		TableColumn tblclmnUserId = new TableColumn(tblDetails, SWT.NONE);
		tblclmnUserId.setWidth(51);
		tblclmnUserId.setText("S. No.");

		TableColumn tblclmnLoginName = new TableColumn(tblDetails, SWT.NONE);
		tblclmnLoginName.setWidth(146);
		tblclmnLoginName.setText("Car No");

		TableColumn tblclmnFullName = new TableColumn(tblDetails, SWT.NONE);
		tblclmnFullName.setWidth(131);
		tblclmnFullName.setText("Customer");

		TableColumn tblclmnAdress = new TableColumn(tblDetails, SWT.NONE);
		tblclmnAdress.setWidth(139);
		tblclmnAdress.setText("Phone No");

		TableColumn tblclmnEmail = new TableColumn(tblDetails, SWT.NONE);
		tblclmnEmail.setWidth(126);
		tblclmnEmail.setText("Servicing At");

		TableColumn tblclmnRole = new TableColumn(tblDetails, SWT.NONE);
		tblclmnRole.setWidth(147);
		tblclmnRole.setText("Next Servicing");

		

		TableColumn tblclmnPhoneNo = new TableColumn(tblDetails, SWT.NONE);
		tblclmnPhoneNo.setWidth(87);
		tblclmnPhoneNo.setText("Remarks");

		TableColumn tblclmnCellNo = new TableColumn(tblDetails, SWT.NONE);
		tblclmnCellNo.setWidth(100);
		tblclmnCellNo.setText("Created Date");
		
		
		Group grpSearch = new Group(cmpMain, SWT.NONE);
		grpSearch.setText("Search");
		grpSearch.setBounds(10, 125, 371, 90);
		
		Label lblCarNo = new Label(grpSearch, SWT.NONE);
		lblCarNo.setBounds(38, 23, 56, 15);
		lblCarNo.setText("Car No: ");
		
		txtCarNoSearch = new Text(grpSearch, SWT.BORDER);
		txtCarNoSearch.setEnabled(false);
		txtCarNoSearch.setBounds(100, 20, 97, 21);
		
		Label lblCustomer = new Label(grpSearch, SWT.NONE);
		lblCustomer.setAlignment(SWT.RIGHT);
		lblCustomer.setBounds(38, 60, 58, 15);
		lblCustomer.setText("Customer: ");
		
		txtCustomerSearch = new Text(grpSearch, SWT.BORDER);
		txtCustomerSearch.setEnabled(false);
		txtCustomerSearch.setBounds(100, 57, 97, 21);
		
		Button btnSearch = new Button(grpSearch, SWT.NONE);
		btnSearch.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(txtCarNoSearch.getText().equals("") && txtCustomerSearch.getText().equals(""))
				{
					msg.showIndividualMessage("Please enter carno or customer name to search", 0);
				}
				else
				{
					if(!txtCarNoSearch.getText().equals(""))
					{
						FillTableDetails("where car_no like '"+txtCarNoSearch.getText()+"%'");
					}
					else
					{
						FillTableDetails("where customer_name like '"+txtCustomerSearch.getText()+"%'");
					}
				}
			}
		});
		btnSearch.setBounds(252, 55, 97, 25);
		btnSearch.setText("Search");
		
		btnCar = new Button(grpSearch, SWT.CHECK);
		btnCar.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(btnCar.getSelection())
				{
					txtCustomerSearch.setEnabled(false);
					txtCarNoSearch.setEnabled(true);
					txtCarNoSearch.setText("");
					txtCustomerSearch.setText("");
					btnCustomer.setSelection(false);
				}
				else
				{
					txtCustomerSearch.setEnabled(false);
					txtCarNoSearch.setEnabled(false);
					txtCarNoSearch.setText("");
					txtCustomerSearch.setText("");
					btnCustomer.setSelection(false);
				}
				
			}
		});
		btnCar.setBounds(23, 22, 13, 16);
		
		btnCustomer = new Button(grpSearch, SWT.CHECK);
		btnCustomer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(btnCustomer.getSelection())
				{
					txtCustomerSearch.setEnabled(true);
					txtCarNoSearch.setEnabled(false);
					txtCarNoSearch.setText("");
					txtCustomerSearch.setText("");
					btnCar.setSelection(false);
				}
				else
				{
					txtCustomerSearch.setEnabled(false);
					txtCarNoSearch.setEnabled(false);
					txtCarNoSearch.setText("");
					txtCustomerSearch.setText("");
					btnCar.setSelection(false);
				}
			}
		});
		btnCustomer.setBounds(23, 59, 13, 16);
		// TODO Auto-generated method stub
		FillTableDetails("");

	}

	protected void MessageBoxInformation(String message) {
		MessageBox mbInfo = new MessageBox(sh, SWT.ICON_INFORMATION);
		mbInfo.setText("Sucess");
		mbInfo.setMessage(message);
		mbInfo.open();

	}

	protected boolean checkSave() {
		@SuppressWarnings("unused")
		boolean check = true;

		String[] errors;
		errors = new String[22];
		int cnt = 0;

		if (txtCarNo.getText().equals("")) {
			check = false;
			errors[cnt] = "Please Enter Car  No";
			++cnt;
		}


		if (txtServicingAt.getText().equals("")) {

			check = false;
			errors[cnt] = "Please Enter Servicing at km.";
			
			++cnt;
		}

		if (txtNextServicing.getText().equals("")) {
			check = false;
			errors[cnt] = "Please enter next servicing at";			
			++cnt;
		}

		if (txtCustomer.getText().equals("")) {
			check = false;
			errors[cnt] = "Please enter customer";			
			++cnt;
		}
		if (bcmbDate.getText().equals("")) {
			check = false;
			errors[cnt] = "Please select date";			
			++cnt;
		}
		

		boolean hasError = false;
		String strError = "";
		for (int i = 0; i < errors.length; i++) {
			if (errors[i] != null) {
				strError += errors[i] + "\n";
				hasError = true;
			}

		}
		if (hasError) {
			MessageBox mb = new MessageBox(sh, SWT.ERROR);
			mb.setText("Errors");
			mb.setMessage(strError);
			mb.open();
			return false;
		} else {
			return true;
		}

	}

	protected void SetFieldEnabled() {
		txtNextServicing.setEnabled(true);
		txtPhoneNo.setEnabled(true);
		txtCustomer.setEnabled(true);
		txtCarNo.setEnabled(true);
		bcmbDate.setEnabled(true);
		txtServicingAt.setEnabled(true);
		txtRemarks.setEnabled(true);
		cmdSave.setEnabled(true);
		cmdDelete.setEnabled(true);
		
	}

	protected void SetFieldDisable() {
		
		txtNextServicing.setText("");
		txtPhoneNo.setText("");
		txtCustomer.setText("");
		txtCarNo.setText("");
		txtServicingAt.setText("");
		txtRemarks.setText("");
		txtId.setText("");
		bcmbDate.setValue(new Date());
		txtRemarks.setEnabled(false);
		txtNextServicing.setEnabled(false);
		txtPhoneNo.setEnabled(false);
		txtCustomer.setEnabled(false);
		bcmbDate.setEnabled(false);
		txtCarNo.setEnabled(false);
		txtServicingAt.setEnabled(false);
		cmdSave.setEnabled(false);
		cmdDelete.setEnabled(false);
		cmdEdit.setEnabled(false);

	}

	private void FillTableDetails(String cond) {

		ResultSet rsUserDetail = objUserCntrl.GetAll(cond);
		tblDetails.removeAll();
		try {
			if (rsUserDetail == null) {
				TableItem item = new TableItem(tblDetails, SWT.NONE);
				item.setText(0, "");
				item.setText(1, "");
				item.setText(2, "");
				item.setText(3, "");
				item.setText(4, "");
				item.setText(5, "");
				item.setText(6, "");
				item.setText(7, "");
				item.setText(8, "");

			} else
				do {
					TableItem item = new TableItem(tblDetails, SWT.NONE);
					item.setText(0, String.valueOf(rsUserDetail.getInt("service_record_id")));
					item.setText(1, String.valueOf(tblDetails.getItemCount()));

					item.setText(2, rsUserDetail.getString("car_no"));
					item.setText(3, rsUserDetail.getString("customer_name"));
						item.setText(4, rsUserDetail.getString("phone_no"));					
						item.setText(5, rsUserDetail.getString("servicing_at_km"));
					
						item.setText(6, rsUserDetail.getString("next_servicing_at_km"));
					
				
						item.setText(7, rsUserDetail.getString("remarks"));
						item.setText(8, rsUserDetail.getString("crdt_dt"));

				} while (rsUserDetail.next());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// public static String encryptPassword( String password )
	// {
	// String encrypted = "";
	// try
	// {
	// MessageDigest digest = MessageDigest.getInstance( "MD5" );
	// byte[] passwordBytes = password.getBytes( );
	// digest.reset( );
	// digest.update( passwordBytes );
	// byte[] message = digest.digest( );
	//
	// StringBuffer hexString = new StringBuffer();
	// for ( int i=0; i < message.length; i++)
	// {
	// hexString.append( Integer.toHexString(
	// 0xFF & message[ i ] ) );
	// }
	// encrypted = hexString.toString();
	// }
	// catch( Exception e ) { }
	// return encrypted;
	// }
	
	private void loadCarDetails(String carno)
	{
		ResultSet rs=objUserCntrl.GetLatestRecordByCarNo(carno);
		try {
			if(rs!=null)
			{
			
				txtServicingAt.setText(String.valueOf(rs.getDouble("next_servicing_at_km")));
				txtCustomer.setText(rs.getString("customer_name"));
				txtPhoneNo.setText(rs.getString("phone_no"));
			
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}
}
