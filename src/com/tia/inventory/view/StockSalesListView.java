package com.tia.inventory.view;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.swtdesigner.SWTResourceManager;
import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.util.Banner;
import com.tia.common.util.BeforeSaveMessages;
import com.tia.common.util.SwtBndCombo;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;

import com.tia.inventory.controller.StockPurchasedController;
import com.tia.inventory.controller.StockSalesController;
import com.tia.inventory.model.StockSalesModel;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Control;

public class StockSalesListView extends ViewPart {

	private Composite comParent;
	private Composite comMainContainer;
	private Table tblItemPurchased;
	private TableColumn tblclmnSno;
	private TableColumn tblclmnName;
	private TableColumn tblclmnPhone;
	private TableColumn tblclmnContactPerson;
	private TableColumn tblclmnCellNo;
	private TableColumn tblclmnEmail;
	private TableColumn tblclmnTotalExpense;
	private TableColumn tblclmnItemType;
	private TableColumn tblclmnItemLocalType;
	private StockSalesController control=new StockSalesController();
	private BeforeSaveMessages msg=new BeforeSaveMessages();
	private TableColumn tblclmnPurId;
	private SwtBndCombo cmbSearch;
	private Text txtSearch;
	private TableColumn tblclmnJVId;
	

	public StockSalesListView() {
		setTitleImage(SWTResourceManager.getImage(StockPurchasedListView.class, "/sales16.png"));

	}

	@Override
	public void createPartControl(Composite parent) {

		/*
		 * The top-most composite that holds title bar and the comMainContainer
		 * composite together.
		 */
		comParent = parent;
		GridLayout gl_comOuterMain = new GridLayout();
		gl_comOuterMain.marginHeight = 1;
		comParent.setLayout(gl_comOuterMain);

		// Title for the form/view
		Banner.getTitle("Stock Sales", comParent,
				StockPurchasedListView.class);

		// Main container that holds all other controls/widgets of the view
		comMainContainer = new Composite(comParent, SWT.NONE);
		comMainContainer.setLayout(new FormLayout());
		GridData gd_comMainContainer = new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1);
		gd_comMainContainer.widthHint = 983;
		gd_comMainContainer.heightHint = 561;
		comMainContainer.setLayoutData(gd_comMainContainer);

		// Table for displaying the current list of customers in the database
		tblItemPurchased = new Table(comMainContainer, SWT.BORDER
				| SWT.FULL_SELECTION);
		tblItemPurchased.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {

				TableItem[] items = tblItemPurchased.getSelection();

				/*
				 * Get the customer id from the second column which is hidden.
				 */
				int custId = Integer.parseInt(items[0].getText(1));

				

			}

		});
		FormData fd_tblItemPurchased = new FormData();
		fd_tblItemPurchased.bottom = new FormAttachment(100, -45);
		fd_tblItemPurchased.top = new FormAttachment(0, 43);
		fd_tblItemPurchased.left = new FormAttachment(0, 10);
		fd_tblItemPurchased.right = new FormAttachment(0, 973);
		tblItemPurchased.setLayoutData(fd_tblItemPurchased);
		tblItemPurchased.setHeaderVisible(true);
		tblItemPurchased.setLinesVisible(true);

		tblclmnPurId = new TableColumn(tblItemPurchased, SWT.NONE);
		tblclmnPurId.setWidth(0);
		tblclmnPurId.setText("id");
		tblclmnPurId.setResizable(false);
		
		tblclmnSno = new TableColumn(tblItemPurchased, SWT.NONE);
		tblclmnSno.setWidth(46);
		tblclmnSno.setText("S.No.");

		// Table column Name
		tblclmnName = new TableColumn(tblItemPurchased, SWT.NONE);
		tblclmnName.setWidth(104);
		tblclmnName.setText("Sold Date");

		// Table column Phone
		tblclmnPhone = new TableColumn(tblItemPurchased, SWT.NONE);
		tblclmnPhone.setWidth(151);
		tblclmnPhone.setText("Customer");

		// Table column Contact Person
		tblclmnContactPerson = new TableColumn(tblItemPurchased, SWT.NONE);
		tblclmnContactPerson.setWidth(83);
		tblclmnContactPerson.setText("Bill No");

		// Table column Cell No.
		tblclmnCellNo = new TableColumn(tblItemPurchased, SWT.NONE);
		tblclmnCellNo.setWidth(118);
		tblclmnCellNo.setText("Total Amount(Rs)");

		// Table column Email
		tblclmnEmail = new TableColumn(tblItemPurchased, SWT.NONE);
		tblclmnEmail.setWidth(108);
		tblclmnEmail.setText("Due Amount(Rs)");
		
		tblclmnTotalExpense = new TableColumn(tblItemPurchased, SWT.NONE);
		tblclmnTotalExpense.setWidth(107);
		tblclmnTotalExpense.setText("Discount(Rs)");
		
		tblclmnItemType = new TableColumn(tblItemPurchased, SWT.NONE);
		tblclmnItemType.setWidth(121);
		tblclmnItemType.setText("Vat(Rs)");
		
		tblclmnItemLocalType = new TableColumn(tblItemPurchased, SWT.NONE);
		tblclmnItemLocalType.setWidth(119);
		tblclmnItemLocalType.setText("Trxn No");
		
		tblclmnJVId = new TableColumn(tblItemPurchased, SWT.NONE);
		tblclmnJVId.setWidth(0);
		tblclmnJVId.setText("id");
		tblclmnJVId.setResizable(false);
		
		Composite composite = new Composite(comMainContainer, SWT.NONE);
		FormData fd_composite = new FormData();
		fd_composite.right = new FormAttachment(tblItemPurchased, 0, SWT.RIGHT);
		fd_composite.top = new FormAttachment(0);
		fd_composite.left = new FormAttachment(0);
		fd_composite.bottom = new FormAttachment(0, 37);
		composite.setLayoutData(fd_composite);
		
		Button btnNew = new Button(composite, SWT.NONE);
		btnNew.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				openView(0);
			}
		});
		btnNew.setBounds(10, 10, 64, 25);
		btnNew.setText("New");
		
		Button btnEdit = new Button(composite, SWT.NONE);
		btnEdit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tblItemPurchased.getSelectionIndex() < 0)
				{
					msg.showIndividualMessage("Please select the table row to edit", 0);
				}
				else
				{
					String itemId = tblItemPurchased.getSelection()[0].getText(0);
					openView(Integer.valueOf(itemId));
				}
			}
		});
		btnEdit.setBounds(80, 10, 75, 25);
		btnEdit.setText("Edit");
		
		Button btnDelete = new Button(composite, SWT.NONE);
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tblItemPurchased.getSelectionIndex() < 0)
				{
					msg.showIndividualMessage("Please select the table row to delete", 0);
				}
				else
				{
					if(msg.showIndividualMessage("Make sure before you delete the selected record", 2))
					{
						int itemId = Integer.valueOf(tblItemPurchased.getSelection()[0].getText(0));
						int jvno = Integer.valueOf(tblItemPurchased.getSelection()[0].getText(9));
						if(control.deleteOldJournals(jvno) && control.deleteSoldDetails(itemId) )
						{
							fillTable("");
							msg.showIndividualMessage("Selected record has been successfully deleted", 0);
						}
						else
						{
							msg.showIndividualMessage("Some error occured while deleting record.Please try again", 0);
						}
					}
					
					
				}
			}
		});
		btnDelete.setBounds(161, 10, 75, 25);
		btnDelete.setText("Delete");
		
		cmbSearch = new SwtBndCombo(composite, SWT.READ_ONLY);
		cmbSearch.setBounds(553, 12, 200, 23);
		//cmbSearch.add( "All","all");
		cmbSearch.add("Sold Date","date");
		cmbSearch.add("Customer", "customer");
		cmbSearch.add("Bill No", "bill_no");
		cmbSearch.select(0);
		
		Button btnSearch = new Button(composite, SWT.NONE);
		btnSearch.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(txtSearch.getText().equals(""))
				{
					msg.showIndividualMessage("Please enter text to search", 0);
				}
				else
				{
					if(cmbSearch.getSelectedBoundValue().equals("date"))
					{
						fillTable("where trxn_dt like '"+txtSearch.getText()+"%'");
					}
					else if(cmbSearch.getSelectedBoundValue().equals("customer"))
					{
						fillTable("inner join inven_customer on inven_stock_out.customer_id=inven_customer.customer_id where  inven_customer.name like '"+txtSearch.getText()+"%'");
					}
					else if(cmbSearch.getSelectedBoundValue().equals("bill_no"))
					{
						fillTable("where bill_no like '"+txtSearch.getText()+"%'");
					}
					else
					{
						fillTable("");
					}
				}
			}
		});
		btnSearch.setBounds(898, 11, 75, 25);
		btnSearch.setText("Search");
		
		Button btnReferesh = new Button(composite, SWT.NONE);
		btnReferesh.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				fillTable("");
				txtSearch.setText("");
				cmbSearch.select(0);
			}
		});
		btnReferesh.setBounds(242, 10, 75, 25);
		btnReferesh.setText("Referesh");
		
		txtSearch = new Text(composite, SWT.BORDER);
		txtSearch.setBounds(759, 13, 133, 21);
		
		Button btnPrintBill = new Button(composite, SWT.NONE);
		btnPrintBill.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tblItemPurchased.getSelectionIndex() < 0)
				{
					msg.showIndividualMessage("Please select the table row to print", 0);
				}
				else
				{
					
						int itemId = Integer.valueOf(tblItemPurchased.getSelection()[0].getText(0));
						
						StockSalesReportView hms = new StockSalesReportView(
								PlatformUI.getWorkbench()
										.getActiveWorkbenchWindow().getShell(),itemId,"sales");
						hms.open();
					
					
				}
			}
		});
		btnPrintBill.setBounds(323, 10, 75, 25);
		btnPrintBill.setText("Print Bill");
		composite.setTabList(new Control[]{btnNew, btnEdit, btnDelete, btnReferesh, btnPrintBill, cmbSearch, txtSearch, btnSearch});

		
		fillTable("");
	}
	
	private void fillTable(String cond)
	{
		tblItemPurchased.removeAll();
		ResultSet rs=control.getSoldListByCond(cond);
		try {
			if(UtilFxn.getRowCount(rs) > 0)
			{
				int sno = 1;
				while(rs.next())
				{
					TableItem item=new TableItem(tblItemPurchased, SWT.NONE);
					item.setText(0,String.valueOf(rs.getInt("stock_out_id")));
					item.setText(1,String.valueOf(sno));
					item.setText(2,String.valueOf(rs.getDate("trxn_dt")));
					item.setText(3,String.valueOf(UtilFxn.GetValueFromTable("inven_customer", "name", "customer_id='"+rs.getInt("customer_id")+"'")));
					item.setText(4,rs.getString("bill_no")==null?"":rs.getString("bill_no"));
					item.setText(5,UtilFxn.getFormatedAmount(rs.getDouble("total_amt")));
					item.setText(6,UtilFxn.getFormatedAmount(rs.getDouble("total_amt")-rs.getDouble("paid_amt")));
					item.setForeground(6, UtilMethods.getFontForegroundColorRed());
					item.setText(7,UtilFxn.getFormatedAmount(rs.getDouble("discount")));
					item.setText(8,UtilFxn.getFormatedAmount(rs.getDouble("vat")));
					String jvno="";
					if(rs.getInt("sales_jv_no") > 0)
					{
						jvno=String.valueOf(rs.getInt("sales_jv_no"));
					}
					if(rs.getInt("service_income_jv_no") > 0)
					{
						jvno=jvno+","+String.valueOf(rs.getInt("service_income_jv_no"));
					}
					if(rs.getInt("service_expense_jv_no") > 0)
					{
						jvno=jvno+","+String.valueOf(rs.getInt("service_expense_jv_no"));
					}
					if(rs.getInt("discount_expense_jv_no") > 0)
					{
						jvno=jvno+","+String.valueOf(rs.getInt("discount_expense_jv_no"));
					}
					if(rs.getInt("vat_expense_jv_no") > 0)
					{
						jvno=jvno+","+String.valueOf(rs.getInt("vat_expense_jv_no"));
					}
					
					item.setText(9,jvno);
					sno++;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void openView(int id)
	{
		StockSalesEntryView hms = new StockSalesEntryView(
				PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell(),id);
		hms.open();
	}
	
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}
}

