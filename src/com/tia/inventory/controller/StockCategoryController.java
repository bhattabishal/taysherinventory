package com.tia.inventory.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;


import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.inventory.model.StockCategoryModel;



public class StockCategoryController {
	private Connection cnn = DbConnection.getConnection();
	private UtilFxn utilFxn = new UtilFxn();
	private Statement st;
	public boolean fetchCheckByCondition(String cond) {
		boolean returnVal = false;
		try {
			this.st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			String strQry = "SELECT * FROM inven_stock_category WHERE " + cond;
			
			ResultSet rs = st.executeQuery(strQry);
			if (utilFxn.getRowCount(rs) > 0) {
				// cnn.close();
				returnVal = true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("SQLException: " + e.getMessage());
		}
		return returnVal;
	}
	public boolean hasMember(int groupId) {
		String strQry ;
		
			try
			{
			this.st=cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			
				strQry="SELECT count(*) FROM inven_item WHERE cat_id ='"+groupId+"'";
			
			
			ResultSet rs=st.executeQuery(strQry);
			if(rs.next())
			{
				if (rs.getInt(1) > 0)
				{
					return true;
				}
			}
			}
			catch(SQLException e)
			{
				return true;
			}
		
		return false;
	}
	public ResultSet getCategory(String catId) {
		ResultSet group = null;
		String strQry = "";
		// if (groupId.equals(""))
		// strQry = "SELECT *,(CASE WHEN parent_id IS NULL THEN '' ELSE "
		// +
		// "(SELECT name FROM mem_group_mcg WHERE group_id = A.parent_id) END) AS parent_name,"
		// +
		// "(SELECT COUNT(name) FROM mem_group_mcg WHERE parent_id = A.group_id) AS pcount"
		// + "FROM mem_group_mcg AS A WHERE parent_id=0";
		// else
		// strQry = "SELECT *,(CASE WHEN parent_id is NULL THEN '' ELSE"
		// +
		// "(SELECT name FROM mem_group_mcg where group_id = A.parent_id) END) AS parent_name,"
		// +
		// "(SELECT count(name) FROM mem_group_mcg WHERE parent_id = A.group_id) AS pcount"
		// + " FROM mem_group_mcg AS A WHERE parent_id='" + groupId
		// + "'";
		strQry = "SELECT a1.cat_id, a1.parent_id,a1.lineage,a1.deep,a1.br_id,a1.cat_name,a1.cat_stock_type,a1.cat_status,"
				+ "a1.income_acc,a1.assets_acc,a1.remarks,a1.cr_dt,a1.cr_by,a1.updt_by,a1.updt_dt,a1.updt_cnt,"
				+ "a2.cat_id,"
				+ "a2.cat_name AS [parent_name] "
				+ "FROM inven_stock_category AS a1 LEFT JOIN inven_stock_category AS a2 ON a1.parent_id = a2.cat_id "
				+ "WHERE a1.cat_id = '" + catId + "'";

		System.out.println("Query " + strQry);
		try {
			this.st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry);

			if (rs.next()) {
				group = rs;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return group;
	}
	public boolean isNodeLeaf(String lineage) {
		String strQry = "SELECT COUNT(*) AS children_count FROM inven_stock_category WHERE lineage LIKE '"
				+ lineage + "%'";

		try {
			this.st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry);

			while (rs.next()) {
				if (rs.getInt(1) == 1) {
					// then the selected account is definitely the leaf node
					return true;
				} else if (rs.getInt(1) > 1) {
					// then the selected account definitely has children
					return false;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public Integer getMaxid() {
		String strQry = "SELECT max(cat_id) as total FROM inven_stock_category";
		int count = 0;
		try {
			this.st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry);
			if (rs.next()) {
				count = rs.getInt("total");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return count;
	}
	public StockCategoryModel fetchDetails(int id) {
		StockCategoryModel catModel = new StockCategoryModel();
		// grpModel=null;
		String strQry = "SELECT * from inven_stock_category WHERE cat_id='" + id
				+ "'";
		System.out.println("fetchDetails query :" + strQry);
		try {
			this.st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry);
			if (rs.next()) {

				int cat_id=rs.getInt("cat_id");
				int parent_id=rs.getInt("parent_id");
				String lineage = rs.getString("lineage");
				int deep = rs.getInt("deep");
				int br_id = rs.getInt("br_id");
				String name = rs.getString("cat_name");				
				int entityType = rs.getInt("cat_stock_type");
				int status=rs.getInt("cat_status");
				String incomeAcc = rs.getString("income_acc");
				String assAcc = rs.getString("assets_acc");
				String remarks = rs.getString("remarks");
				
				Date cr_dt=rs.getDate("cr_dt");
				int cr_by = rs.getInt("cr_by");
				int updt_by = rs.getInt("updt_by");
				Date updt_dt=rs.getDate("updt_dt");
				int updt_cnt = rs.getInt("updt_cnt");
				
				catModel.setCat_id(cat_id);
				catModel.setParent_id(parent_id);
				catModel.setLineage(lineage);
				catModel.setDeep(deep);
				catModel.setBr_id(br_id);
				catModel.setCat_name(name);
				catModel.setCat_stock_type(entityType);
				catModel.setCat_status(status);
				catModel.setIncome_acc(incomeAcc);
				catModel.setAssets_acc(assAcc);
				catModel.setRemarks(remarks);
				catModel.setCr_dt(String.valueOf(cr_dt));
				catModel.setCr_by(cr_by);
				catModel.setUpdt_by(updt_by);
				catModel.setUpdt_dt(String.valueOf(updt_dt));
				catModel.setUpdt_cnt(updt_cnt);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return catModel;
	}
	public String calculateLineageDeep(Integer pid, String return_type) {
		int currentid = getMaxid() + 1;
		StockCategoryModel parentrow = fetchDetails(pid);
		// print_r($parentrow);
		int parentdeep = parentrow.getDeep();
		int deep = parentdeep + 1;
		String parentlineage = parentrow.getLineage();
		String lineage = parentlineage + "-" + currentid;
		String result = "";

		if (return_type == "lineage") {
			result = lineage;
		} else if (return_type == "deep") {
			result = String.valueOf(deep);
		}

		return result;
	}
	public boolean save(StockCategoryModel grpModel, String action,
			Connection con) {
		System.out.println("action selected is :" + action);
		boolean save = false;
		

		try {
			String strQry = "";
			this.cnn = con;
			this.st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			System.out.println("This is the action" + action);
			if (action == "add") {
				grpModel.setCat_id(UtilMethods.getMaxId("inven_stock_category",
				"cat_id"));
				
				strQry = grpModel.Insert();
			} else if (action == "edit") {
				System.out.println("Group Id to Edit record is :"
						+ grpModel.getCat_id());
				grpModel.z_WhereClause.cat_id(grpModel.getCat_id());
				grpModel.setUpdt_cnt(grpModel.getUpdt_cnt() + 1);
				grpModel.setUpdt_by(grpModel.getUpdt_by());
				grpModel.setUpdt_dt(grpModel.getUpdt_dt());
				strQry = grpModel.Update();
				System.out.println("This is the query to insert the values"
						+ strQry);
			}

			if (((st.executeUpdate(strQry) == 1))
					|| ((st.executeUpdate(strQry) >= 1))) {

				save = true;
			} else {
				this.cnn.rollback();
				save = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return save;
	}
	public boolean remove(int id) {
		boolean check = false;
		StockCategoryModel grpModel = new StockCategoryModel();
		grpModel.z_WhereClause.cat_id(id);
		String strQry = grpModel.Delete();
		try {
			this.cnn.setAutoCommit(false);
			this.st = cnn.createStatement();
			if (st.executeUpdate(strQry) == 1
					) {
				this.cnn.commit();
				check = true;
			} else {
				this.cnn.rollback();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return check;
	}
	public boolean checkIfHasChild(int id) {
		boolean check = false;
		String strQry = "Select * FROM inven_stock_category WHERE parent_id='" + id
				+ "'";
		try {
			this.st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(strQry);
			if (rs.next()) {
				check = true;
			} else {
				check = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return check;
	}
	
	public ResultSet getChildCategories(int id)
	{
		ResultSet rs=null;
		String strQry = "Select * FROM inven_stock_category WHERE parent_id='" + id
		+ "'";
		try {
			this.st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			rs = st.executeQuery(strQry);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	public int getLoopChildId(int id)
	{
		//int value=0;
		if(this.checkIfHasChild(id))
		{
			ResultSet child=this.getChildCategories(id);
			try {
				while(child.next())
				{
					int childId=child.getInt("cat_id");
					this.getLoopChildId(childId);
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return id;
	}
	
	public String getAllChildId(int id)
	{
		String value="";
		ResultSet child=null;
		if(this.checkIfHasChild(id))
		{
			child=this.getChildCategories(id);
			try {
				while(child.next())
				{
					int childId=child.getInt("cat_id");
					int lopval=this.getLoopChildId(childId);
					if(value.equals(""))
						value=String.valueOf(lopval);
					else
						value=value+","+String.valueOf(lopval);
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
				value=String.valueOf(id);
		}
		return value;
	}

}
