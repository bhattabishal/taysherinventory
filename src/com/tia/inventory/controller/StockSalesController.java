package com.tia.inventory.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;





import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.account.controller.JournalEntryController;
import com.tia.account.model.JVDetailsModel;
import com.tia.account.model.JVSummary;
import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.inventory.model.DayBookDetailsModel;
import com.tia.inventory.model.DayBookModel;
import com.tia.inventory.model.SalesTrxnModel;
import com.tia.inventory.model.ServiceTrxnModel;
import com.tia.inventory.model.StockPurchasedDetailsModel;
import com.tia.inventory.model.StockSalesDetailsModel;
import com.tia.inventory.model.StockSalesModel;
import com.tia.inventory.model.StockSalesReportModel;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;

public class StockSalesController {
	Connection cnn= DbConnection.getConnection();
	ArrayList<String> log= new ArrayList<String>();
	private JCalendarFunctions jcalFxn=new JCalendarFunctions();
	private StockPurchasedController purControl=new StockPurchasedController();
	StockSalesModel salesModel=new StockSalesModel();
	
	public int saveAction(StockSalesModel stockSalesModel,ArrayList<StockSalesDetailsModel> itemsList,ArrayList<ServiceTrxnModel> serviceList,ArrayList<SalesTrxnModel> sourceList,ArrayList<StockSalesReportModel> reportList,DayBookModel daybook,ArrayList<DayBookDetailsModel> daybookDetails)
	{
		
		boolean deleteold=true;
		if(stockSalesModel.getStock_out_id() > 0)
		{
			if(this.deleteOldJournals(stockSalesModel.getSales_jv_no()) && this.deleteSoldDetails(stockSalesModel.getStock_out_id()))
			{
				deleteold=true;
			}
			else
			{
				deleteold=false;
			}
			
		}
		
		int journalSave=1;
		if(Double.parseDouble(stockSalesModel.getPaid_amt()) > 0)
		{
			DayBookController dc=new DayBookController();
			journalSave=dc.saveJournals(daybook, daybookDetails);
			stockSalesModel.setSales_jv_no(journalSave);
			
		}
		else
		{
			stockSalesModel.setSales_jv_no(0);
		}
		
		//boolean savJournal=this.saveJournals();
		this.salesModel=stockSalesModel;
		int salesSave=this.saveSales();
		boolean saveItemsDetails=this.saveItemDetails(itemsList, salesSave);
		boolean saveTrxn=this.saveTrxnDetails(sourceList, salesSave);
		boolean saveService=this.saveServiceDetails(serviceList, salesSave);
		boolean reportSave=this.saveReport(reportList,salesSave);
		
		
		
		if(salesSave > 0 && deleteold && saveItemsDetails && saveTrxn && saveService && journalSave > 0 && reportSave)
		{
			return salesSave;
		}
		return 0;
	}
	
	
	public int saveSales()
	{
		String sql = "";
		if(salesModel.getStock_out_id() <= 0)
		{
		salesModel.setStock_out_id(UtilMethods.getMaxId("inven_stock_out", "stock_out_id"));
		salesModel.setBill_no(UtilMethods.getMaxId("inven_stock_out", "bill_no"));
			sql = salesModel.Insert();
			System.out.println("sales model:"+sql);
		}
		else
		{
			salesModel.z_WhereClause.stock_out_id(salesModel
					.getStock_out_id());
			sql = salesModel.Update();
		}
		try {

			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			// if the single user is inserted/update, its successful
			if (st.executeUpdate(sql) == 1)
				return salesModel.getStock_out_id();
			else
				return 0;

		} catch (SQLException e) {
			
			e.printStackTrace();
			return 0;
		}
		
	}
	
	//set item details
	private boolean saveItemDetails(ArrayList<StockSalesDetailsModel> itemsList,int selId)
	{
		ArrayList<StockSalesDetailsModel> oldList=this.getItemsList(selId);
		StockSalesDetailsModel model;
		String sql="";
		boolean itemSave=false;
		for (int i = 0; i < itemsList.size(); i++) {
			model=new StockSalesDetailsModel();
			model=itemsList.get(i);
			
			//if(model.getStock_out_det_id() > 0)
			//{
				//model.z_WhereClause.stock_out_det_id(model
				//		.getStock_out_det_id());
				//sql = model.Update();
			//}
			//else
			//{
				model.setStock_out_det_id(UtilMethods.getMaxId("inven_stock_out_details", "stock_out_det_id"));
				model.setStock_out_id(selId);
				sql=model.Insert();
			//}
			try {

				Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_UPDATABLE);

				// if the single user is inserted/update, its successful
				if (st.executeUpdate(sql) == 1)
				{
					itemSave=true;
				}
				else
				{
					itemSave=false;
				}

			} catch (SQLException e) {
				
				e.printStackTrace();
				itemSave=false;
			}
			if(!itemSave)
			{
				break;
				
			}
				
		}
		
		if(!itemSave)
		{
			return false;
		}
		//now compare with old ,if less record than delete it
//		for (int i = 0; i < oldList.size(); i++) {
//			model=new StockSalesDetailsModel();
//			model=oldList.get(i);
//			int id=model.getStock_out_det_id();
//			boolean found=false;
//			for (int j = 0; j < itemsList.size(); j++) {
//				model=new StockSalesDetailsModel();
//				model=oldList.get(i);
//				if(id==model.getStock_out_det_id())
//				{
//					found=true;
//				}
//			}
//			if(!found)
//			{
//				if(this.deleteItemDetailsById(id))
//				{
//					itemSave=true;
//				}
//				else
//				{
//					itemSave=false;
//				}
//			}
//		}
		
		return itemSave;
	}
	
	//set trxn details
	private boolean saveTrxnDetails(ArrayList<SalesTrxnModel> trxnList,int selId)
	{
		ArrayList<SalesTrxnModel> oldList=this.getTrxnList(selId);
		SalesTrxnModel model;
		String sql="";
		boolean itemSave=true;
		for (int i = 0; i < trxnList.size(); i++) {
			model=new SalesTrxnModel();
			model=trxnList.get(i);
			
//			if(model.getSales_trxn_id() > 0)
//			{
//				model.z_WhereClause.sales_trxn_id(model
//						.getSales_trxn_id());
//				sql = model.Update();
//			}
//			else
			//{
				model.setSales_trxn_id(UtilMethods.getMaxId("inven_sales_trxn", "sales_trxn_id"));
				model.setSales_id(selId);
				sql=model.Insert();
			//}
			try {

				Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_UPDATABLE);

				// if the single user is inserted/update, its successful
				if (st.executeUpdate(sql) == 1)
				{
					itemSave=true;
				}
				else
				{
					itemSave=false;
				}

			} catch (SQLException e) {
				
				e.printStackTrace();
				itemSave=false;
			}
			if(!itemSave)
			{
				break;
				
			}
				
		}
		
		if(!itemSave)
		{
			return false;
		}
		//now compare with old ,if less record than delete it
//		for (int i = 0; i < oldList.size(); i++) {
//			model=new SalesTrxnModel();
//			model=oldList.get(i);
//			int id=model.getSales_trxn_id();
//			boolean found=false;
//			for (int j = 0; j < trxnList.size(); j++) {
//				model=new SalesTrxnModel();
//				model=oldList.get(i);
//				if(id==model.getSales_trxn_id())
//				{
//					found=true;
//				}
//			}
//			if(!found)
//			{
//				if(this.deleteTrxnDetailsById(id))
//				{
//					itemSave=true;
//				}
//				else
//				{
//					itemSave=false;
//				}
//			}
//		}
		
		return itemSave;
	}
	
	//set service details
	private boolean saveServiceDetails(ArrayList<ServiceTrxnModel> serviceList,int selId)
	{
		ArrayList<ServiceTrxnModel> oldList=this.getServiceList(selId);
		ServiceTrxnModel model;
		String sql="";
		boolean itemSave=true;
		for (int i = 0; i < serviceList.size(); i++) {
			model=new ServiceTrxnModel();
			model=serviceList.get(i);
			
//			if(model.getService_trxn_id() > 0)
//			{
//				model.z_WhereClause.service_trxn_id(model
//						.getService_trxn_id());
//				sql = model.Update();
//			}
//			else
//			{
				model.setService_trxn_id(UtilMethods.getMaxId("inven_service_trxn", "service_trxn_id"));
				model.setSales_id(selId);
				sql=model.Insert();
			//}
			try {

				Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_UPDATABLE);

				// if the single user is inserted/update, its successful
				if (st.executeUpdate(sql) == 1)
				{
					itemSave=true;
				}
				else
				{
					itemSave=false;
				}

			} catch (SQLException e) {
				
				e.printStackTrace();
				itemSave=false;
			}
			if(!itemSave)
			{
				break;
				
			}
				
		}
		
		if(!itemSave)
		{
			return false;
		}
		//now compare with old ,if less record than delete it
//		for (int i = 0; i < oldList.size(); i++) {
//			model=new ServiceTrxnModel();
//			model=oldList.get(i);
//			int id=model.getService_trxn_id();
//			boolean found=false;
//			for (int j = 0; j < serviceList.size(); j++) {
//				model=new ServiceTrxnModel();
//				model=oldList.get(i);
//				if(id==model.getService_trxn_id())
//				{
//					found=true;
//				}
//			}
//			if(!found)
//			{
//				if(this.deleteServiceDetailsById(id))
//				{
//					itemSave=true;
//				}
//				else
//				{
//					itemSave=false;
//				}
//			}
//		}
		
		return itemSave;
	}
	
	//save for report
	private boolean saveReport(ArrayList<StockSalesReportModel> reportList,int selId)
	{
		//check for report and deleteold
		if(!this.deleteReport(selId))
		{
			return false;
		}
		
		StockSalesReportModel model;
		String sql="";
		boolean itemSave=false;
		for (int i = 0; i < reportList.size(); i++) {
			model=new StockSalesReportModel();
			model=reportList.get(i);
			
				model.setSales_id(selId);
				model.setBill_no(String.valueOf(salesModel.getBill_no()));
				sql=model.Insert();
			
			try {

				Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_UPDATABLE);

				// if the single user is inserted/update, its successful
				if (st.executeUpdate(sql) == 1)
				{
					itemSave=true;
				}
				else
				{
					itemSave=false;
				}

			} catch (SQLException e) {
				
				e.printStackTrace();
				itemSave=false;
			}
			if(!itemSave)
			{
				break;
				
			}
				
		}
		
		if(!itemSave)
		{
			return false;
		}
		
		
		return itemSave;
	}
	public ArrayList<StockSalesDetailsModel> getItemsList(int selId)
	{
		ArrayList<StockSalesDetailsModel> list=new ArrayList<StockSalesDetailsModel>();
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT *"
			+ " FROM inven_stock_out_details where stock_out_id='"+selId+"'";
		try {
			System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
			while(rs.next())
			{
				StockSalesDetailsModel model=new StockSalesDetailsModel();
				model.setStock_out_det_id(rs.getInt("stock_out_det_id"));
				model.setItem_id(rs.getInt("item_id"));
				model.setUnit_id(rs.getInt("unit_id"));
				model.setQuantity(String.valueOf(rs.getDouble("quantity")));
				model.setUnit_quantity(String.valueOf(rs.getDouble("unit_quantity")));
				model.setRate(String.valueOf(rs.getDouble("rate")));
				model.setTotal(String.valueOf(rs.getDouble("total")));
				model.setStock_out_id(rs.getInt("stock_out_id"));
				model.setCrtd_dt(rs.getString("crtd_dt"));
				model.setCrtd_by(rs.getInt("crtd_by"));
				model.setUpdt_by(rs.getInt("updt_by"));
				model.setUpdt_cnt(rs.getInt("updt_cnt"));
				model.setUpdt_dt(rs.getString("updt_dt"));
				list.add(model);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		return list;
	}
	public ArrayList<ServiceTrxnModel> getServiceList(int serId)
	{
		ArrayList<ServiceTrxnModel> list=new ArrayList<ServiceTrxnModel>();
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT *"
			+ " FROM inven_service_trxn where sales_id='"+serId+"'";
		try {
			System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
			while(rs.next())
			{
				ServiceTrxnModel model=new ServiceTrxnModel();
				model.setService_trxn_id(rs.getInt("service_trxn_id"));
				model.setService_id(rs.getInt("service_id"));
				model.setTrxn_dt(rs.getString("trxn_dt"));
				model.setSales_id(rs.getInt("sales_id"));
				model.setSelling_cost(String.valueOf(rs.getDouble("selling_cost")));
				model.setPerc_earned(String.valueOf(rs.getDouble("perc_earned")));
				model.setPerc_paid(String.valueOf(rs.getDouble("perc_paid")));
				model.setService_provider_id(rs.getInt("service_provider_id"));
				model.setCrtd_dt(rs.getString("crtd_dt"));
				
				list.add(model);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		return list;
	}
	
	public ArrayList<SalesTrxnModel> getTrxnList(int serId)
	{
		ArrayList<SalesTrxnModel> list=new ArrayList<SalesTrxnModel>();
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT *"
			+ " FROM inven_sales_trxn where sales_id='"+serId+"'";
		try {
			System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
			while(rs.next())
			{
				SalesTrxnModel model=new SalesTrxnModel();
				model.setSales_trxn_id(rs.getInt("sales_trxn_id"));
				model.setSales_id(rs.getInt("sales_id"));
				model.setTrxn_amt(String.valueOf(rs.getDouble("trxn_amt")));
				model.setSource(rs.getString("source"));
				model.setTrxn_dt(String.valueOf(rs.getDate("trxn_dt")));
				model.setBank_name(rs.getString("bank_name"));
				model.setVno(rs.getString("vno"));
				model.setBank_acc_code(rs.getString("bank_acc_code"));
				model.setCrtd_dt(rs.getString("crtd_dt"));
				
				list.add(model);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		return list;
	}
	
	public boolean deleteItemById(int detId)
	{
		
		String qry="DELETE from inven_stock_out WHERE stock_out_id='"+detId+"'";
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			st.execute(qry) ;
				

		} catch (Exception e) {
			System.out.println("The exception is " + e.toString());
			//e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean deleteItemDetailsById(int detId)
	{
		String qry="DELETE from inven_stock_out_details WHERE stock_out_det_id='"+detId+"'";
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			st.execute(qry);
				

		} catch (Exception e) {
			System.out.println("The exception is " + e.toString());
			//e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean deleteReport(int detId)
	{
		String qry="DELETE from inven_sales_report WHERE sales_id='"+detId+"'";
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			st.execute(qry);
				

		} catch (Exception e) {
			System.out.println("The exception is " + e.toString());
			//e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean deleteServiceDetailsById(int detId)
	{
		String qry="DELETE from inven_service_trxn WHERE service_trxn_id='"+detId+"'";
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			st.execute(qry);
				

		} catch (Exception e) {
			System.out.println("The exception is " + e.toString());
			//e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean deleteTrxnDetailsById(int detId)
	{
		String qry="DELETE from inven_sales_trxn WHERE sales_trxn_id='"+detId+"'";
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			st.execute(qry);
				

		} catch (Exception e) {
			System.out.println("The exception is " + e.toString());
			//e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean deleteSoldDetails(int id)
	{
		//String qry="DELETE from inven_stock_out WHERE stock_out_id='"+id+"'";
		String qry1="DELETE from inven_stock_out_details WHERE stock_out_id='"+id+"'";
		String qry2="DELETE from inven_sales_trxn WHERE sales_id='"+id+"'";
		String qry3="DELETE from inven_service_trxn WHERE sales_id='"+id+"'";
		String qry4="DELETE from inven_sales_report WHERE sales_id='"+id+"'";
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			//st.execute(qry);
			st.execute(qry1);
			st.execute(qry2);
			st.execute(qry3);
			st.execute(qry4);
				

		} catch (Exception e) {
			System.out.println("The exception is " + e.toString());
			//e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean deleteOldJournals(int salesid)
	{
			
		//get all vouchers having 
		
		if(deleteJournal(salesid))
		{
			return true;
		}
		return false;
	}
	
	private boolean deleteJournal(int jvno)
	{
		
		
		
		String qry1="DELETE from day_book WHERE day_book_id='"+jvno+"'";
		String qry2="DELETE from day_book_details where day_book_id='"+jvno+"'";
	
		
		
		
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			st.execute(qry1);
			st.execute(qry2);
			
				

		} catch (Exception e) {
			System.out.println("The exception is " + e.toString());
			//e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private boolean saveJournals()
	{
		
		
		
		
		
		DayBookController jvControl = new DayBookController();
		int itemSave=1;
		//int itemSave=jvControl.saveDayBook2(salesModel);
		salesModel.setSales_jv_no(itemSave);
		int servIncSave=1;
		salesModel.setService_income_jv_no(0);
		
		
		salesModel.setService_expense_jv_no(0);
		
		
		
		salesModel.setDiscount_expense_jv_no(0);
		
		
		salesModel.setVat_expense_jv_no(0);
		
		
		
		
			return true;
		
		
	}

	private JVSummary getJournalSummary(StockSalesModel stockSalesModel)
	{
		JVSummary jVitem =new JVSummary();
		jVitem.setApproved_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		jVitem.setAudited_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		jVitem.setBill_date(stockSalesModel.getTrxn_dt());
		jVitem.setBr_id(1);
		jVitem.setCreated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		jVitem.setCreated_date(jcalFxn.currentDate());
		jVitem.setFy_id(ApplicationWorkbenchWindowAdvisor.getFiscalId());
		jVitem.setJv_date(stockSalesModel.getTrxn_dt());
		jVitem.setNarration(stockSalesModel.getRemarks());
		jVitem.setPosted_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		jVitem.setRemarks(stockSalesModel.getRemarks());
		jVitem.setStatus(1);
		jVitem.setUpdate_count(0);
		jVitem.setUpdated_by(ApplicationWorkbenchWindowAdvisor.getLoginId());
		jVitem.setUpdated_date(jcalFxn.currentDate());
		jVitem.setTo_from_type("sales");
		jVitem.setTran_id("0");
		return jVitem;
	}
	public ArrayList<JVDetailsModel> getItemJournalDetails(ArrayList<SalesTrxnModel> sourceList,StockSalesModel stockSalesModel)
	{
		ArrayList<JVDetailsModel> jvDetail = new ArrayList<JVDetailsModel>();
		JVDetailsModel deatil = new JVDetailsModel();
		SalesTrxnModel model;
		//cr side
		deatil.setAcc_code(purControl.getAccountCode("Credit", "item_sold"));
		deatil.setDr_amt(0);
		deatil.setCr_amt(Double.parseDouble(stockSalesModel.getTotal_amt()));
		deatil.setNarration("Item sold");
		jvDetail.add(deatil);
		for (int i = 0; i < sourceList.size(); i++) {
			model=new SalesTrxnModel();
			model=sourceList.get(i);
			
			//dr side
			deatil = new JVDetailsModel();
			deatil.setAcc_code(model.getBank_acc_code());
			deatil.setDr_amt(Double.parseDouble(model.getTrxn_amt()));
			deatil.setCr_amt(0);
			deatil.setNarration("Item sold");
			jvDetail.add(deatil);
		
		}
		
		//for remaining
		double remaining=Double.parseDouble(stockSalesModel.getTotal_amt())-Double.parseDouble(stockSalesModel.getPaid_amt());
		if(remaining > 0)
		{
			deatil = new JVDetailsModel();
			deatil.setAcc_code(purControl.getAccountCode("Debit", "item_sold_credit"));
			deatil.setDr_amt(remaining);
			deatil.setCr_amt(0);
			deatil.setNarration("Item sold on credit");
			jvDetail.add(deatil);
		}
		
		
		return jvDetail;
	}
	
	
	//service earned
	private ArrayList<JVDetailsModel>getServiceEarnedDetails(ArrayList<ServiceTrxnModel> serviceList)
	{
		ArrayList<JVDetailsModel> jvDetail = new ArrayList<JVDetailsModel>();
		JVDetailsModel deatil = new JVDetailsModel();
		ServiceTrxnModel model;
		double tot=0;
		for (int i = 0; i < serviceList.size(); i++) {
			model=new ServiceTrxnModel();
			model=serviceList.get(i);
			double perce=Double.valueOf(model.getPerc_earned());
			if(perce > 0)
			{
				double amt=Double.parseDouble(model.getSelling_cost());
				double percEn=(perce/100)*amt;
				tot+=percEn;
			}
			else
			{
				tot+=Double.parseDouble(model.getSelling_cost());
			}
		}
		salesModel.setService_income(UtilFxn.getDoubleExpFormated(tot));
		deatil.setAcc_code(purControl.getAccountCode("Debit", "service_earned"));
		deatil.setDr_amt(tot);
		deatil.setCr_amt(0);
		deatil.setNarration("Service earned");
		jvDetail.add(deatil);
		
		deatil = new JVDetailsModel();
		deatil.setAcc_code(purControl.getAccountCode("Credit", "service_earned"));
		deatil.setDr_amt(0);
		deatil.setCr_amt(tot);
		deatil.setNarration("Service earned");
		jvDetail.add(deatil);
		
		return jvDetail;
	}
	//service expense
	private ArrayList<JVDetailsModel>getServiceExpenseDetails(ArrayList<ServiceTrxnModel> serviceList)
	{
		ArrayList<JVDetailsModel> jvDetail = new ArrayList<JVDetailsModel>();
		JVDetailsModel deatil = new JVDetailsModel();
		ServiceTrxnModel model;
		double tot=0;
		for (int i = 0; i < serviceList.size(); i++) {
			model=new ServiceTrxnModel();
			model=serviceList.get(i);
			double percp=Double.valueOf(model.getPerc_paid());
			if(percp > 0)
			{
				double amt=Double.parseDouble(model.getSelling_cost());
				double percEn=(percp/100)*amt;
				tot+=percEn;
			}
			
		}
		salesModel.setService_expense(UtilFxn.getDoubleExpFormated(tot));
		deatil.setAcc_code(purControl.getAccountCode("Debit", "service_expense"));
		deatil.setDr_amt(tot);
		deatil.setCr_amt(0);
		deatil.setNarration("Service Expense");
		jvDetail.add(deatil);
		
		deatil = new JVDetailsModel();
		deatil.setAcc_code(purControl.getAccountCode("Credit", "service_expense"));
		deatil.setDr_amt(0);
		deatil.setCr_amt(tot);
		deatil.setNarration("Service Expense");
		jvDetail.add(deatil);
		
		return jvDetail;
	}
	
	//discount expense
	private ArrayList<JVDetailsModel>getDiscountExpenseDetails(StockSalesModel stockSalesModel)
	{
		ArrayList<JVDetailsModel> jvDetail = new ArrayList<JVDetailsModel>();
		JVDetailsModel deatil = new JVDetailsModel();
		
		deatil.setAcc_code(purControl.getAccountCode("Debit", "item_sold_discount"));
		deatil.setDr_amt(Double.valueOf(stockSalesModel.getDiscount()));
		deatil.setCr_amt(0);
		deatil.setNarration("Discount");
		jvDetail.add(deatil);
		
		deatil = new JVDetailsModel();
		deatil.setAcc_code(purControl.getAccountCode("Credit", "item_sold_discount"));
		deatil.setDr_amt(0);
		deatil.setCr_amt(Double.valueOf(stockSalesModel.getDiscount()));
		deatil.setNarration("Discount");
		jvDetail.add(deatil);
		
		return jvDetail;
	}
	//vat expense
	private ArrayList<JVDetailsModel>getVatExpenseDetails(StockSalesModel stockSalesModel)
	{
		ArrayList<JVDetailsModel> jvDetail = new ArrayList<JVDetailsModel>();
		JVDetailsModel deatil = new JVDetailsModel();
		
		deatil.setAcc_code(purControl.getAccountCode("Debit", "item_sold_vat"));
		deatil.setDr_amt(Double.valueOf(stockSalesModel.getVat()));
		deatil.setCr_amt(0);
		deatil.setNarration("Vat");
		jvDetail.add(deatil);
		
		deatil = new JVDetailsModel();
		deatil.setAcc_code(purControl.getAccountCode("Credit", "item_sold_vat"));
		deatil.setDr_amt(0);
		deatil.setCr_amt(Double.valueOf(stockSalesModel.getVat()));
		deatil.setNarration("Vat");
		jvDetail.add(deatil);
		
		return jvDetail;
	}
	
	public ResultSet getSoldListByCond(String cond)
	{
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT *"
			+ " FROM inven_stock_out "+cond;
		try {
			System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return rs;
	}
	
	public StockSalesModel getSalesInfoById(int id)
	{
		StockSalesModel model=new StockSalesModel();
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT *"
			+ " FROM inven_stock_out where stock_out_id='"+id+"'";
		try {
			System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
			if(rs.next())
			{
				model.setBill_no(rs.getInt("bill_no"));
				model.setBilling_type(rs.getInt("billing_type"));
				model.setCrtd_by(rs.getInt("crtd_by"));
				model.setCrtd_dt(rs.getString("crtd_dt"));
				model.setCustomer_id(rs.getInt("customer_id"));
				model.setDiscount(String.valueOf(rs.getDouble("discount")));
				model.setDiscount_expense_jv_no(rs.getInt("discount_expense_jv_no"));
				model.setFy_id(rs.getInt("fy_id"));
				model.setPaid_amt(String.valueOf(rs.getDouble("paid_amt")));
				model.setRemarks(rs.getString("remarks")==null?"":rs.getString("remarks"));
				model.setSales_jv_no(rs.getInt("sales_jv_no"));
				model.setService_expense(String.valueOf(rs.getDouble("service_expense")));
				model.setService_expense_jv_no(rs.getInt("service_expense_jv_no"));
				model.setService_income(String.valueOf(rs.getDouble("service_income")));
				model.setService_income_jv_no(rs.getInt("service_income_jv_no"));
				model.setStock_out_id(rs.getInt("stock_out_id"));
				model.setTotal_amt(String.valueOf(rs.getDouble("total_amt")));
				model.setTrxn_dt(rs.getString("trxn_dt"));
				model.setUpdt_by(rs.getInt("updt_by"));
				model.setUpdt_cnt(rs.getInt("updt_cnt"));
				model.setUpdt_dt(rs.getString("updt_dt"));
				model.setVat(String.valueOf(rs.getDouble("vat")));
				model.setVat_expense_jv_no(rs.getInt("vat_expense_jv_no"));
				model.setDiscount_percentage(String.valueOf(rs.getDouble("discount_percentage")));
				model.setVat_perentage(String.valueOf(rs.getDouble("vat_percentage")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return model;
	}
}
