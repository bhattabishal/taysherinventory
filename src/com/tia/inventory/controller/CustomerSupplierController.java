package com.tia.inventory.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilFxn;

import com.tia.inventory.model.CustomerModelClass;
import com.tia.inventory.model.StockCategoryModel;
import com.tia.inventory.model.StockItemModel;

public class CustomerSupplierController {
	private Connection cnn = DbConnection.getConnection();
	private UtilFxn utilFxn = new UtilFxn();
	private Statement st;
	public boolean fetchCheckByCondition(String cond) {
		boolean returnVal = false;
		try {
			this.st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			String strQry = "SELECT * FROM inven_customer WHERE " + cond;
			
			ResultSet rs = st.executeQuery(strQry);
			if (utilFxn.getRowCount(rs) > 0) {
				// cnn.close();
				returnVal = true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("SQLException: " + e.getMessage());
		}
		return returnVal;
	}
	public ArrayList<CustomerModelClass> getAll()
	{
		ArrayList<CustomerModelClass> list=new ArrayList<CustomerModelClass>();
		try {
			this.st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			String strQry = "SELECT * FROM inven_customer";
			
			ResultSet rs = st.executeQuery(strQry);
			if (utilFxn.getRowCount(rs) > 0) {
				while(rs.next())
				{
					CustomerModelClass model=new CustomerModelClass();
					model.setAddress(rs.getString("address")==null?"":rs.getString("address"));
					model.setMobile(rs.getString("mobile")==null?"":rs.getString("mobile"));
					model.setContact_person(rs.getString("contact_person")==null?"":rs.getString("contact_person"));
					model.setCustomer_id(rs.getInt("customer_id"));
					model.setName(rs.getString("name")==null?"":rs.getString("name"));
					model.setEmail(rs.getString("email")==null?"":rs.getString("email"));
					model.setPhone(rs.getString("phone")==null?"":rs.getString("phone"));
					model.setRemarks(rs.getString("remarks")==null?"":rs.getString("remarks"));
					model.setType(rs.getInt("type"));
					model.setCreated_dt(rs.getString("created_dt"));
					list.add(model);
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("SQLException: " + e.getMessage());
			return null;
		}
		return list;
	}
	public CustomerModelClass fetchItemDetails(int itemId) {

		String sql = "SELECT * FROM inven_customer WHERE customer_id = " + itemId;
		CustomerModelClass model=new CustomerModelClass();
		ResultSet rs = null;

		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);
			if(rs.next())
			{
				model.setAddress(rs.getString("address")==null?"":rs.getString("address"));
				model.setMobile(rs.getString("mobile")==null?"":rs.getString("mobile"));
				model.setContact_person(rs.getString("contact_person")==null?"":rs.getString("contact_person"));
				model.setCustomer_id(rs.getInt("customer_id"));
				model.setName(rs.getString("name")==null?"":rs.getString("name"));
				model.setEmail(rs.getString("email")==null?"":rs.getString("email"));
				model.setPhone(rs.getString("phone")==null?"":rs.getString("phone"));
				model.setRemarks(rs.getString("remarks")==null?"":rs.getString("remarks"));
				model.setType(rs.getInt("type"));
				model.setCreated_dt(rs.getString("created_dt"));
			}
			else
			{
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return model;
	}
	public boolean saveDetails(CustomerModelClass objModel) {

		boolean retFlag = true;
		String sql = "";

		if (objModel.getCustomer_id() == 0) {
			// its the insert operation
			sql = objModel.Insert();
		} else {
			// its the update operation
			objModel.z_WhereClause.customer_id(objModel
					.getCustomer_id());
			// may need to be added
			/*
			 * objCustomerModel.z_WhereClause.br_id(
			 * ApplicationWorkbenchWindowAdvisor .getUserId());
			 */
		
			sql = objModel.Update();
		}
		System.out.println("The item query is :" + sql);
		try {

			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			// if the single user is inserted/update, its successful
			if (st.executeUpdate(sql) == 1)
				retFlag = true;
			else
				retFlag = false;

		} catch (SQLException e) {
			retFlag = false;
			e.printStackTrace();
		}
		return retFlag;
	}
	public boolean remove(int id) {
		boolean check = false;
		CustomerModelClass grpModel = new CustomerModelClass();
		grpModel.z_WhereClause.customer_id(id);
		String strQry = grpModel.Delete();
		try {
			this.cnn.setAutoCommit(false);
			this.st = cnn.createStatement();
			if (st.executeUpdate(strQry) == 1
					) {
				this.cnn.commit();
				check = true;
			} else {
				this.cnn.rollback();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return check;
	}
}
