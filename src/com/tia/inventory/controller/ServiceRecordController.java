package com.tia.inventory.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.tia.common.db.DbConnection;

public class ServiceRecordController {
	Connection cnn= DbConnection.getConnection();
	
	public ResultSet GetAll(String cond) {
		String strQry ="select * from inven_service_record "+cond;
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet rsUserDetail = st.executeQuery(strQry);
			if(rsUserDetail.next()){
			return rsUserDetail;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			return null;
		}
	public ResultSet GetLatestRecordByCarNo(String carno) {
		
	String strQry ="select top 1 * from inven_service_record where car_no='"+carno+"' order by service_record_id desc ";
	
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet rsUserDetail = st.executeQuery(strQry);
			if(rsUserDetail.next()){
			return rsUserDetail;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			return null;
		}
	public boolean Delete(int id){
		String strQry ="Delete  from inven_service_record where service_record_id='"+id+"'";
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			st.executeUpdate(strQry);				
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
		
	}
}
