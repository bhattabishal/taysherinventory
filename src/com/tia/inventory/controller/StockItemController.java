package com.tia.inventory.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.tia.common.db.DbConnection;

import com.tia.inventory.model.StockItemModel;

public class StockItemController {
	private Statement st;
	private Connection cnn;
	private ResultSet rs;
	
	public StockItemController() {
		cnn = DbConnection.getConnection();

	}
	
	public ResultSet fetchItemDetails(int itemId) {

		String sql = "SELECT * FROM inven_item WHERE item_id = " + itemId;

		rs = null;

		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return rs;
	}
	public ResultSet fetchItemRemainingDetailsById(int itemId) {

		String sql = "SELECT\n" +
		"item.item_id,\n" +
		"item.item_name,\n" +
		"item.item_code,\n" +
		"item.item_rack_no,\n" +
		"item.item_selling_price,\n" +
		"item.item_pur_unit_id,\n" +
		"item.item_sel_unit_id,\n" +
		"isnull((select sum(ip.unit_quantity) from dbo.inven_item_purchsed_details AS ip where ip.item_id=item.item_id),0) AS pur_unit_total,\n" +
		"isnull((select sum([is].unit_quantity) from dbo.inven_stock_out_details AS [is] where [is].item_id=item.item_id),0)AS sel_unit_total ,\n" +
		"(isnull((select sum(ip.unit_quantity) from dbo.inven_item_purchsed_details AS ip where ip.item_id=item.item_id),0)-isnull((select sum([is].unit_quantity) from dbo.inven_stock_out_details AS [is] where [is].item_id=item.item_id),0))as remaining_unit_total,\n" +
		"purunit.conversion_ration AS pur_unit_equ,\n" +
		"purunit.unit_name AS pur_unit_name,\n" +
		"selunit.conversion_ration AS sel_unit_equ,\n" +
		"selunit.unit_name AS sel_unit_name\n" +
		"FROM\n" +
		"dbo.inven_item AS item\n" +
		"LEFT JOIN dbo.inven_unit AS purunit ON item.item_pur_unit_id = purunit.unit_id\n" +
		"INNER JOIN dbo.inven_unit AS selunit ON item.item_sel_unit_id = selunit.unit_id\n" +
		"WHERE\n" +
		"item.item_id = '"+itemId+"' ";

		rs = null;

		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return rs;
	}
	
	public  ResultSet fetchItemOutOfStock() {

		String sql = "select t1.* from (SELECT\n" +
		"item.item_id,\n" +
		"item.item_name,\n" +
		"item.item_code,\n" +
		"item.item_min_stock,\n" +
		"item.cat_id,\n" +
		"cv.cv_lbl as cat_name,\n" +
		"item.item_rack_no,\n" +
		"item.item_selling_price,\n" +
		"item.item_pur_unit_id,\n" +
		"item.item_sel_unit_id,\n" +
		"isnull((select sum(ip.unit_quantity) from dbo.inven_item_purchsed_details AS ip where ip.item_id=item.item_id),0) AS pur_unit_total,\n" +
		"isnull((select sum([is].unit_quantity) from dbo.inven_stock_out_details AS [is] where [is].item_id=item.item_id),0)AS sel_unit_total ,\n" +
		"(isnull((select sum(ip.unit_quantity) from dbo.inven_item_purchsed_details AS ip where ip.item_id=item.item_id),0)-isnull((select sum([is].unit_quantity) from dbo.inven_stock_out_details AS [is] where [is].item_id=item.item_id),0))as remaining_unit_total,\n" +
		"purunit.conversion_ration AS pur_unit_equ,\n" +
		"purunit.unit_name AS pur_unit_name,\n" +
		"selunit.conversion_ration AS sel_unit_equ,\n" +
		"selunit.unit_name AS sel_unit_name\n" +
		"FROM\n" +
		"dbo.inven_item AS item\n" +
		"LEFT JOIN dbo.inven_unit AS purunit ON item.item_pur_unit_id = purunit.unit_id\n" +
		"INNER JOIN dbo.inven_unit AS selunit ON item.item_sel_unit_id = selunit.unit_id\n" +
		"LEFT JOIN dbo.code_value_mcg as cv on item.cat_id=cv.cv_code\n" +
		"WHERE\n" +
		"cv.cv_type='Item Type')t1\n" +
		"WHERE t1.remaining_unit_total <= t1.item_min_stock";

		rs = null;

		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return rs;
	}
	public ResultSet fetchItemByCatId(int catId) {
		
		//check if selected category has child
		StockCategoryController control=new StockCategoryController();
		String ids=control.getAllChildId(catId);

		String sql = "SELECT * FROM inven_item WHERE cat_id IN ("+ids+") ";
		System.out.println("query for items by cat id is :" +sql);

		rs = null;

		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rs;
	}
	public ResultSet getAllItems(String cond) {

		String sql = "SELECT\n" +
		"item.item_id,\n" +
		"item.item_name,\n" +
		"item.item_code,\n" +
		"item.cat_id,\n" +
		"cv.cv_lbl as cat_name,\n" +
		"item.item_rack_no,\n" +
		"item.item_selling_price,\n" +
		"item.item_pur_unit_id,\n" +
		"item.item_sel_unit_id,\n" +
		"isnull((select sum(ip.unit_quantity) from dbo.inven_item_purchsed_details AS ip where ip.item_id=item.item_id),0) AS pur_unit_total,\n" +
		"isnull((select sum([is].unit_quantity) from dbo.inven_stock_out_details AS [is] where [is].item_id=item.item_id),0)AS sel_unit_total ,\n" +
		"(isnull((select sum(ip.unit_quantity) from dbo.inven_item_purchsed_details AS ip where ip.item_id=item.item_id),0)-isnull((select sum([is].unit_quantity) from dbo.inven_stock_out_details AS [is] where [is].item_id=item.item_id),0))as remaining_unit_total,\n" +
		"purunit.conversion_ration AS pur_unit_equ,\n" +
		"purunit.unit_name AS pur_unit_name,\n" +
		"selunit.conversion_ration AS sel_unit_equ,\n" +
		"selunit.unit_name AS sel_unit_name\n" +
		"FROM\n" +
		"dbo.inven_item AS item\n" +
		"LEFT JOIN dbo.inven_unit AS purunit ON item.item_pur_unit_id = purunit.unit_id\n" +
		"INNER JOIN dbo.inven_unit AS selunit ON item.item_sel_unit_id = selunit.unit_id\n" +
		"LEFT JOIN dbo.code_value_mcg as cv on item.cat_id=cv.cv_code\n" +
		"WHERE\n" +
		"cv.cv_type='Item Type' "+cond;

		rs = null;

		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rs;
	}
	
	public boolean saveItemDetails(StockItemModel objModel) {

		boolean retFlag = true;
		String sql = "";

		if (objModel.getItem_id() == 0) {
			// its the insert operation
			sql = objModel.Insert();
		} else {
			// its the update operation
			objModel.z_WhereClause.item_id(objModel
					.getItem_id());
			// may need to be added
			/*
			 * objCustomerModel.z_WhereClause.br_id(
			 * ApplicationWorkbenchWindowAdvisor .getUserId());
			 */
		
			sql = objModel.Update();
		}
System.out.println("The item query is :" + sql);
		try {

			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			// if the single user is inserted/update, its successful
			if (st.executeUpdate(sql) == 1)
				retFlag = true;
			else
				retFlag = false;

		} catch (SQLException e) {
			retFlag = false;
			e.printStackTrace();
		}
		return retFlag;

	}
	public boolean deleteItem(int itemId) {
		boolean retFlag = false;

		StockItemModel objModel = new StockItemModel();
		objModel.z_WhereClause.item_id(itemId);

		String deleteSql = objModel.Delete();

		try {
			st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			if (st.executeUpdate(deleteSql) > 0) {
				retFlag = true;
			}

		} catch (Exception e) {
			System.out.println("The exception is " + e.toString());
			return false;
			//e.printStackTrace();
		}

		return retFlag;
	}
	
}
