package com.tia.inventory.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.tia.account.controller.JournalEntryController;
import com.tia.account.model.JVDetailsModel;
import com.tia.account.model.JVSummary;
import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.inventory.model.DayBookDetailsModel;
import com.tia.inventory.model.DayBookModel;
import com.tia.inventory.model.StockItemModel;
import com.tia.inventory.model.StockPurchasedDetailsModel;
import com.tia.inventory.model.StockPurchasedModel;
import com.tia.inventory.model.StockPurchasedTrxnModel;
import com.tia.inventory.model.StockPurchseExpenseTrxnModel;



public class StockPurchasedController {
	Connection cnn= DbConnection.getConnection();
	ArrayList<String> log= new ArrayList<String>();
	
	public boolean saveAction(StockPurchasedModel stockPurchasedModel,ArrayList<StockPurchasedDetailsModel> itemsList,
			ArrayList<StockPurchasedTrxnModel> sourceList
			,String action,ArrayList<StockPurchseExpenseTrxnModel> expenseList,DayBookModel daybook,ArrayList<DayBookDetailsModel> daybookDetails)
	{
		boolean save=false;
		int purId=0;
		boolean deleteOld=true;
		
		if(action.equals("add"))
		{
			purId = UtilMethods.getMaxId("inven_item_purchase", "pur_id");
			stockPurchasedModel.setPur_id(purId);
		}
		else
		{
			purId = stockPurchasedModel.getPur_id();
			//delete all old values
			deleteOld=deleteOldValues(stockPurchasedModel.getJv_no(),stockPurchasedModel.getPurchased_payment_id(),purId);
			
		}
		
		
		
		
		
		
		
		
		//save journals
		int journalSave=1;
		
		
		if(Double.parseDouble(stockPurchasedModel.getPaid_amt()) > 0)
		{
			DayBookController dc=new DayBookController();
			journalSave=dc.saveJournals(daybook, daybookDetails);
			stockPurchasedModel.setJv_no(journalSave);
			stockPurchasedModel.setPurchased_payment_id(journalSave);
		}
		else
		{
			stockPurchasedModel.setJv_no(0);
			stockPurchasedModel.setPurchased_payment_id(0);
		}
		
		
		
		
		//save item
		boolean saveItem=false;
		if(action.equals("add"))
		{
			saveItem=this.saveItem(stockPurchasedModel, "add");
		}
		else
		{
			saveItem=this.saveItem(stockPurchasedModel, "edit");
		}
		
		
		//save item details
		boolean saveItemDetails=this.saveItemDetails(itemsList, purId);
		
		//save trxn table
		boolean saveAcc=this.saveAccDetails(sourceList, purId);
		
		boolean saveExp=this.saveExpDetails(expenseList, purId);
		
		if(journalSave >  0 && saveItem && saveItemDetails && saveAcc  && saveExp && deleteOld)
		{
			save=true;
		}
		else
		{
			save=false;
		}
		
		
		return save;
	}
	
	//save items details
	private boolean saveItemDetails(ArrayList<StockPurchasedDetailsModel> itemsList,int purId)
	{
		StockPurchasedDetailsModel model = null;
		boolean retFlag=false;
		for (int i = 0; i < itemsList.size(); i++) {
			model=new StockPurchasedDetailsModel();
			model=itemsList.get(i);
			model.setPur_id(purId);
			model.setPur_det_id(UtilMethods.getMaxId("inven_item_purchsed_details", "pur_det_id"));
			String sql1 = model.Insert();
			StockItemModel itemModel=new StockItemModel();
			itemModel.setItem_selling_price(model.getSelling_price());
			itemModel.z_WhereClause.item_id(model
					.getItem_id());
			String sql2=itemModel.Update();
			try {

				Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_UPDATABLE);
				
				System.out.println(sql1);
				System.out.println(sql2);

				// if the single user is inserted/update, its successful
				if (st.executeUpdate(sql1) == 1 && st.executeUpdate(sql2) == 1)
				{
					retFlag = true;
				}
					
				else
				{
					retFlag = false;
					break;
				}
					

			} catch (SQLException e) {
				retFlag = false;
				e.printStackTrace();
			}
		}
		
		
		return retFlag;
	}
	
	//save trxn details
	private boolean saveAccDetails(ArrayList<StockPurchasedTrxnModel> sourceList,int purId)
	{
		StockPurchasedTrxnModel model = null;
		boolean retFlag=true;
		for (int i = 0; i < sourceList.size(); i++) {
			model=new StockPurchasedTrxnModel();
			model=sourceList.get(i);
			model.setPur_id(purId);
			model.setPur_trxn_id(UtilMethods.getMaxId("inven_item_pur_trxn", "pur_trxn_id"));
			String sql = model.Insert();
			try {

				Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_UPDATABLE);

				// if the single user is inserted/update, its successful
				if (st.executeUpdate(sql) == 1)
				{
					retFlag = true;
				}
					
				else
				{
					retFlag = false;
					break;
				}
					

			} catch (SQLException e) {
				retFlag = false;
				e.printStackTrace();
			}
		}
		
		
		return retFlag;
	}
	
	private boolean saveExpDetails(ArrayList<StockPurchseExpenseTrxnModel> sourceList,int purId)
	{
		StockPurchseExpenseTrxnModel model = null;
		boolean retFlag=true;
		for (int i = 0; i < sourceList.size(); i++) {
			model=new StockPurchseExpenseTrxnModel();
			model=sourceList.get(i);
			model.setPur_id(purId);
			//model.setExp_id(model.ge);
			//model.setPur_trxn_id(UtilMethods.getMaxId("inven_item_pur_trxn", "pur_trxn_id"));
			String sql = model.Insert();
			try {

				Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_UPDATABLE);

				// if the single user is inserted/update, its successful
				if (st.executeUpdate(sql) == 1)
				{
					retFlag = true;
				}
					
				else
				{
					retFlag = false;
					break;
				}
					

			} catch (SQLException e) {
				retFlag = false;
				e.printStackTrace();
			}
		}
		
		
		return retFlag;
	}
	//save item
	private boolean saveItem(StockPurchasedModel objModel,String action)
	{
		boolean retFlag = false;
		String sql = "";

		if (action.equals("add")) {
			// its the insert operation
			sql = objModel.Insert();
		} else {
			// its the update operation
			objModel.z_WhereClause.pur_id(objModel
					.getPur_id());
			// may need to be added
			/*
			 * objCustomerModel.z_WhereClause.br_id(
			 * ApplicationWorkbenchWindowAdvisor .getUserId());
			 */
		
			sql = objModel.Update();
		}
System.out.println("The item query is :" + sql);
		try {

			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			// if the single user is inserted/update, its successful
			if (st.executeUpdate(sql) <= 1)
				retFlag = true;
			else
				retFlag = false;

		} catch (SQLException e) {
			retFlag = false;
			e.printStackTrace();
		}
		return retFlag;

	}
	public static String getAccountCode( String strSide,
			String task) {
		String strQry = "";
		if (strSide.equalsIgnoreCase("Debit"))
			strQry = "SELECT "+task+"_dr"
			+ " FROM inven_acc_setting";
			
		else
			strQry = "SELECT "+task+"_cr"
			+ " FROM inven_acc_setting";
		try {
			System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);
			ResultSet rs = st.executeQuery(strQry);
			if (rs.next())
				return rs.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}
	public  ResultSet getPurchasedList(String cond) {
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT *"
			+ " FROM inven_item_purchase "+cond;
		try {
			System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return rs;
	}
	
	
	public ResultSet getPurchasedById(int purId)
	{
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT *"
			+ " FROM inven_item_purchase where pur_id='"+purId+"'";
		try {
			System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return rs;
	}
	public ResultSet getPurchasedDetailsById(int purId)
	{
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT\n" +
"pd.pur_det_id,\n" +
"pd.item_id,\n" +
"pd.unit_id,\n" +
"pd.quantity,\n" +
"pd.unit_quantity,\n" +
"pd.rate,\n" +
"pd.actual_rate,\n" +
"pd.unit_weight,\n" +
"pd.crtd_by,\n" +
"pd.crtd_dt,\n" +
"pd.updt_by,\n" +
"pd.updt_dt,\n" +
"pd.updt_cnt,\n" +
"pd.total,\n" +
"pd.pur_id,\n" +
"i.item_name,\n" +
"i.item_selling_price,\n" +
"u.conversion_ration,\n" +
"u.unit_name\n" +
"FROM\n" +
"dbo.inven_item_purchsed_details AS pd\n" +
"INNER JOIN dbo.inven_item AS i ON pd.item_id = i.item_id\n" +
"INNER JOIN dbo.inven_unit AS u ON pd.unit_id = u.unit_id where pd.pur_id='"+purId+"'";
		try {
			System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return rs;
	}
	public ResultSet getPurchasedSourceDetailsById(int purId)
	{
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT\n" +
"trxn.pur_trxn_id,\n" +
"trxn.acc_code,\n" +
"trxn.trxn_source,\n" +
"trxn.amount,\n" +
"trxn.jv_no,\n" +
"trxn.pur_id,\n" +
"ah.cv_lbl\n" +
"FROM\n" +
"dbo.inven_item_pur_trxn AS trxn\n" +
"INNER JOIN dbo.code_value_mcg AS ah ON trxn.acc_code = ah.cv_id where (ah.cv_type='cash' or ah.cv_type='bank')  and trxn.pur_id='"+purId+"'";
		try {
			System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return rs;
	}
	
	public ResultSet getPurchasedExpDetailsById(int purId)
	{
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT\n" +
			"dbo.code_value_mcg.cv_lbl as name,\n" +
			"dbo.inven_pur_exp_trxn.amount,\n" +
			"dbo.inven_pur_exp_trxn.exp_id,\n" +
			"dbo.inven_pur_exp_trxn.pur_id,\n" +
			"dbo.inven_pur_exp_trxn.exp_trxn_id\n" +
			"FROM\n" +
			"dbo.inven_pur_exp_trxn\n" +
			"INNER JOIN dbo.code_value_mcg ON dbo.inven_pur_exp_trxn.exp_id = dbo.code_value_mcg.cv_code\n" +
			"WHERE\n" +
			"dbo.inven_pur_exp_trxn.pur_id = '"+purId+"'\n" +
			"and dbo.code_value_mcg.cv_type='purexpense'";
		try {
			System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return rs;
	}
	
	public boolean deleteOldValues(int jvno,int expJvno,int purId)
	{
		//get all vouchers having 
		//System.out.println("the jvno is :" +jvno +"the purid is :" +purId +" exp jv no is :"+expJvno);
		String qry1="DELETE from day_book WHERE day_book_id ='"+jvno+"'";
		String qry2="DELETE from day_book_details WHERE day_book_id ='"+jvno+"'";
		
		
		//delete trxn details and item details
		String qry4="DELETE from inven_item_purchsed_details WHERE pur_id='"+purId+"'";
		String qry5="DELETE from inven_item_pur_trxn where pur_id='"+purId+"'";
		String qry6="DELETE from inven_pur_exp_trxn where pur_id='"+purId+"'";
		//String qry7="DELETE from inven_item_purchase where pur_id='"+purId+"'";
		
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			st.executeUpdate(qry1);
			st.executeUpdate(qry2);
			st.executeUpdate(qry4); 
			st.executeUpdate(qry5);
			st.executeUpdate(qry6);
			//st.executeUpdate(qry7); 
				
			

		} catch (Exception e) {
			System.out.println("The exception is " + e.toString());
			//e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
}