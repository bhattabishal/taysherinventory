package com.tia.inventory.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.tia.ApplicationWorkbenchWindowAdvisor;
import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilFxn;
import com.tia.common.util.UtilMethods;
import com.tia.inventory.model.DayBookDetailsModel;
import com.tia.inventory.model.DayBookModel;
import com.tia.inventory.model.StockItemModel;
import com.tia.inventory.model.StockPurchasedDetailsModel;
import com.tia.inventory.model.StockPurchasedModel;
import com.tia.inventory.model.StockSalesModel;
import com.tia.plugins.JnepCalendar.calendar.JCalendarFunctions;

public class DayBookController {
	Connection cnn= DbConnection.getConnection();
	ArrayList<String> log= new ArrayList<String>();
	private JCalendarFunctions jcalFxn = new JCalendarFunctions();
	private ResultSet rs;
	private Statement st;
	
	
	public int saveJournals(DayBookModel dayBookModel,ArrayList<DayBookDetailsModel> daybookDetails)
	{
		
		int maxId = UtilMethods.getMaxId("day_book", "day_book_id");
		dayBookModel.setDay_book_id(maxId);
		dayBookModel.setReceipt_no(UtilMethods.getMaxId("day_book", "receipt_no"));
		String sql=dayBookModel.Insert();
		try {

			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			// if the single user is inserted/update, its successful
			if (st.executeUpdate(sql) == 1)
			{
				DayBookDetailsModel  model = null;
				for (int i = 0; i < daybookDetails.size(); i++) {
					model=new DayBookDetailsModel();
					model=daybookDetails.get(i);
					model.setDay_book_details_id(UtilMethods.getMaxId("day_book_details", "day_book_details_id"));
					model.setDay_book_id(maxId);
					String sql1 = model.Insert();
					Statement st2 = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
							ResultSet.CONCUR_UPDATABLE);
					if (st2.executeUpdate(sql1) == 1)
							{
						
							}
					else
					{
						
						return 0;
						
					}
				}
			}
			else
			{
				return 0;
			}
			
			
				

		} catch (SQLException e) {
			
			e.printStackTrace();
			return 0;
		}
		return maxId;
	}
	
	
	
	
	
	
	
	
	public ResultSet getAllItems(String cond) {

		String sql = "SELECT\n" +
		"dbo.code_value_mcg.cv_lbl,\n" +
		"dbo.code_value_mcg.cv_type,\n" +
		"dbo.code_value_mcg.cv_id,\n" +
		"dbo.day_book.day_book_id,\n" +
		"dbo.day_book.receipt_no,\n" +
		"dbo.day_book.income,\n" +
		"dbo.day_book.expense,\n" +
		"dbo.day_book.trxn_type,\n" +
		"dbo.day_book.remarks,\n" +
		"dbo.day_book.trxn_date_ad\n" +
		"FROM\n" +
		"dbo.day_book\n" +
		"INNER JOIN dbo.code_value_mcg ON dbo.day_book.trxn_type = dbo.code_value_mcg.cv_id "+cond;

		rs = null;

		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rs;
	}
	
	public ResultSet fetchItemDetails(int itemId) {

		String sql = "SELECT * FROM day_book WHERE day_book_id = " + itemId;

		rs = null;

		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return rs;
	}
	public boolean deleteItem(int itemId) {
		boolean retFlag = false;
		
		ResultSet rs=this.fetchItemDetails(itemId);
		int type=0;
		try {
			if(rs.next())
			{
				type=rs.getInt("trxn_type");
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(type <= 2)
		{
			return false;
		}

		

		String qry1="DELETE from day_book WHERE day_book_id='"+itemId+"'";
		String qry2="DELETE from day_book_details where day_book_id='"+itemId+"'";
	
		
		
		
		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			st.execute(qry1);
			st.execute(qry2);
			
				

		} catch (Exception e) {
			System.out.println("The exception is " + e.toString());
			//e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean saveItemDetails(DayBookModel objModel,DayBookDetailsModel objDetail) {

		boolean retFlag = true;
		String sql = "";
		String sql2 = "";

		if (objModel.getDay_book_id() == 0) {
			objModel.setDay_book_id(UtilMethods.getMaxId("day_book", "day_book_id"));
			objDetail.setDay_book_id(objModel.getDay_book_id());
			objDetail.setDay_book_details_id(UtilMethods.getMaxId("day_book_details", "day_book_details_id"));
			// its the insert operation
			sql = objModel.Insert();
			sql2 = objDetail.Insert();
		} else {
			// its the update operation
			objModel.z_WhereClause.day_book_id(objModel
					.getDay_book_id());
			objDetail.z_WhereClause.day_book_id(objModel
					.getDay_book_id());
			// may need to be added
			/*
			 * objCustomerModel.z_WhereClause.br_id(
			 * ApplicationWorkbenchWindowAdvisor .getUserId());
			 */
		
			sql = objModel.Update();
			sql2 = objDetail.Update();
		}
System.out.println("The item query is :" + sql);
		try {

			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			// if the single user is inserted/update, its successful
			if (st.executeUpdate(sql) == 1 && st.executeUpdate(sql2)==1)
				retFlag = true;
			else
				retFlag = false;

		} catch (SQLException e) {
			retFlag = false;
			e.printStackTrace();
		}
		return retFlag;

	}
	
	public ResultSet getTotalIncome()
	{
		String sql = "select sum(income) as income,sum(expense)  as expense from day_book";

		ResultSet rs = null;

		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return rs;
	}
	
	public ResultSet getTotalStockItem()
	{
		String sql = "select sum(paid_amt) as item from inven_item_purchase";

		ResultSet rs = null;

		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return rs;
	}
	
	public ResultSet getTotalStockOut()
	{
		String sql = "select sum(total) as item  from inven_stock_out_details";

		ResultSet rs = null;

		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return rs;
	}
	
	public ResultSet getCashBankAccounts(String type)
	{
		String sql = "select *  from code_value_mcg where cv_type='"+type+"'";
		System.out.println("sql is :" + sql);
		ResultSet rs = null;

		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return rs;
	}
	
	public double getHeadAmounts(int id)
	{
		String sql = "select (sum(income)-sum(expense)) as amount  from day_book_details where trxn_with_type_id='"+id+"'";
		System.out.println("sql is :" + sql);
		ResultSet rs = null;

		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);
			
			if(UtilFxn.getRowCount(rs) > 0)
			{
				if(rs.next())
				{
					return rs.getDouble("amount");
				}
				
			}

		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

		return 0;
	}
	
	public ResultSet getPurchasedRemainingDetails()
	{
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT\n" +
"sum(dbo.inven_item_purchase.total_amount)-sum(dbo.inven_item_purchase.paid_amt) as amount,\n" +
"dbo.inven_customer.name\n" +
"FROM\n" +
"dbo.inven_item_purchase\n" +
"INNER JOIN dbo.inven_customer ON dbo.inven_item_purchase.supplier_id = dbo.inven_customer.customer_id\n" +
"GROUP BY dbo.inven_customer.name\n" +
"HAVING (sum(dbo.inven_item_purchase.total_amount)-sum(dbo.inven_item_purchase.paid_amt)) > 0";
		try {
			//System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return rs;
	}
	
	public ResultSet getSoldRemainingDetails()
	{
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT\n" +
			"sum(dbo.inven_stock_out.total_amt)-sum(dbo.inven_stock_out.paid_amt) as amount,\n" +
			"dbo.inven_customer.name\n" +
			"FROM\n" +
			"dbo.inven_stock_out\n" +
			"INNER JOIN dbo.inven_customer ON dbo.inven_stock_out.customer_id = dbo.inven_customer.customer_id\n" +
			"GROUP BY dbo.inven_customer.name\n" +
			"HAVING (sum(dbo.inven_stock_out.total_amt)-sum(dbo.inven_stock_out.paid_amt)) > 0";
		try {
			//System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return rs;
	}
	
	public ResultSet getThirdPartyEarnedPaid(String type)
	{
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT\n" +
			"dbo.inven_customer.name,\n" +
			"--dbo.inven_service_trxn.selling_cost,\n" +
			"sum(((dbo.inven_service_trxn.perc_earned/100)*dbo.inven_service_trxn.selling_cost)) as earned,\n" +
			"sum(((dbo.inven_service_trxn.perc_paid/100)*dbo.inven_service_trxn.selling_cost)) as paid\n" +
			"--dbo.inven_service_trxn.perc_earned,\n" +
			"--dbo.inven_service_trxn.perc_paid,\n" +
			"--dbo.inven_service_trxn.service_provider_id\n" +
			"FROM\n" +
			"dbo.inven_service_trxn \n" +
			"INNER JOIN dbo.inven_customer ON dbo.inven_service_trxn.service_provider_id = dbo.inven_customer.customer_id\n" +
			"GROUP BY dbo.inven_service_trxn.service_provider_id,dbo.inven_customer.name\n";
			if(type.equals("earned"))
			{
				strQry=strQry+" HAVING (sum(((dbo.inven_service_trxn.perc_earned/100)*dbo.inven_service_trxn.selling_cost))) > 0";
			}
			else
			{
				strQry=strQry+" HAVING (sum(((dbo.inven_service_trxn.perc_paid/100)*dbo.inven_service_trxn.selling_cost))) > 0";
			}
			
		try {
			System.out.println("The third party query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return rs;
	}
	
	
	public double getDiscountEarned()
	{
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT sum(discount) as amount from inven_stock_out";
			
		try {
			//System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
			if(UtilFxn.getRowCount(rs) > 0)
			{
				
				if(rs.next())
				{
					return rs.getDouble("amount");
					
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
		return 0;
	}
	
	public double getTaxEarned()
	{
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT sum(vat) as amount from inven_stock_out";
			
		try {
			//System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
			if(UtilFxn.getRowCount(rs) > 0)
			{
				
				if(rs.next())
				{
					return rs.getDouble("amount");
					
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
		return 0;
	}
	
	public double getTotalStockAmount()
	{
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT sum(total_amount) as amount from inven_item_purchase";
			
		try {
			//System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
			if(UtilFxn.getRowCount(rs) > 0)
			{
				
				if(rs.next())
				{
					return rs.getDouble("amount");
					
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
		return 0;
	}
	public double getTotalSoldAmount()
	{
		ResultSet rs=null;
		String strQry = "";
			strQry = "SELECT sum(total_amt) as amount from inven_stock_out";
			
		try {
			//System.out.println("The gdsgdsf query is " + strQry);
			Connection cnn = DbConnection.getConnection();
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(strQry);
			if(UtilFxn.getRowCount(rs) > 0)
			{
				
				if(rs.next())
				{
					return rs.getDouble("amount");
					
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
		return 0;
	}



}
