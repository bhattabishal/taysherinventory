package com.tia.inventory.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilFxn;
import com.tia.inventory.model.CustomerModelClass;
import com.tia.inventory.model.ServiceModel;

public class ServiceController {
	private Connection cnn = DbConnection.getConnection();
	private UtilFxn utilFxn = new UtilFxn();
	private Statement st;
	
	
	public boolean fetchCheckByCondition(String cond) {
		boolean returnVal = false;
		try {
			this.st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			String strQry = "SELECT * FROM inven_service WHERE " + cond;
			
			ResultSet rs = st.executeQuery(strQry);
			if (utilFxn.getRowCount(rs) > 0) {
				// cnn.close();
				returnVal = true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("SQLException: " + e.getMessage());
		}
		return returnVal;
	}
	
	public ResultSet getServiceDetails(int id)
	{
		ResultSet rs=null;
		try {
			this.st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			String strQry = "SELECT * FROM inven_service WHERE service_id='"+id+"'";
			
			rs = st.executeQuery(strQry);
			return rs;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("SQLException: " + e.getMessage());
			return null;
		}
		
	}
	
	public boolean saveDetails(ServiceModel objModel) {

		boolean retFlag = true;
		String sql = "";

		if (objModel.getService_id() == 0) {
			// its the insert operation
			sql = objModel.Insert();
		} else {
			// its the update operation
			objModel.z_WhereClause.service_id(objModel
					.getService_id());
			// may need to be added
			/*
			 * objCustomerModel.z_WhereClause.br_id(
			 * ApplicationWorkbenchWindowAdvisor .getUserId());
			 */
		
			sql = objModel.Update();
		}
		System.out.println("The item query is :" + sql);
		try {

			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			// if the single user is inserted/update, its successful
			if (st.executeUpdate(sql) == 1)
				retFlag = true;
			else
				retFlag = false;

		} catch (SQLException e) {
			retFlag = false;
			e.printStackTrace();
		}
		return retFlag;
	}

}
