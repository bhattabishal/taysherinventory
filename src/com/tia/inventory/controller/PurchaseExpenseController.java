package com.tia.inventory.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.tia.common.db.DbConnection;
import com.tia.common.util.UtilFxn;

import com.tia.inventory.model.CustomerModelClass;
import com.tia.inventory.model.PurchaseExpenseModel;
import com.tia.inventory.model.StockCategoryModel;
import com.tia.inventory.model.StockItemModel;

public class PurchaseExpenseController {
	private Connection cnn = DbConnection.getConnection();
	private UtilFxn utilFxn = new UtilFxn();
	private Statement st;
	public boolean fetchCheckByCondition(String cond) {
		boolean returnVal = false;
		try {
			this.st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			String strQry = "SELECT * FROM inven_pur_expense WHERE " + cond;
			
			ResultSet rs = st.executeQuery(strQry);
			if (utilFxn.getRowCount(rs) > 0) {
				// cnn.close();
				returnVal = true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("SQLException: " + e.getMessage());
		}
		return returnVal;
	}
	public ArrayList<PurchaseExpenseModel> getAll()
	{
		ArrayList<PurchaseExpenseModel> list=new ArrayList<PurchaseExpenseModel>();
		try {
			this.st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			String strQry = "SELECT * FROM inven_pur_expense";
			
			ResultSet rs = st.executeQuery(strQry);
			if (utilFxn.getRowCount(rs) > 0) {
				while(rs.next())
				{
					PurchaseExpenseModel model=new PurchaseExpenseModel();
					model.setName(rs.getString("name")==null?"":rs.getString("name"));
					model.setExpense_cr(rs.getString("expense_cr")==null?"":rs.getString("expense_cr"));
					model.setExpense_dr(rs.getString("expense_dr")==null?"":rs.getString("expense_dr"));
					model.setExp_id(rs.getInt("exp_id"));
					
					model.setRemarks(rs.getString("remarks")==null?"":rs.getString("remarks"));
					model.setCrtd_by(rs.getInt("crtd_by"));
					model.setCrtd_dt(rs.getString("crtd_dt"));
					list.add(model);
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("SQLException: " + e.getMessage());
			return null;
		}
		return list;
	}
	public PurchaseExpenseModel fetchItemDetails(int itemId) {

		String sql = "SELECT * FROM inven_pur_expense WHERE exp_id = " + itemId;
		PurchaseExpenseModel model=new PurchaseExpenseModel();
		ResultSet rs = null;

		try {
			Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = st.executeQuery(sql);
			if(rs.next())
			{
				model.setName(rs.getString("name")==null?"":rs.getString("name"));
				model.setExpense_cr(rs.getString("expense_cr")==null?"":rs.getString("expense_cr"));
				model.setExpense_dr(rs.getString("expense_dr")==null?"":rs.getString("expense_dr"));
				model.setExp_id(rs.getInt("exp_id"));
				
				model.setRemarks(rs.getString("remarks")==null?"":rs.getString("remarks"));
				model.setCrtd_by(rs.getInt("crtd_by"));
				model.setCrtd_dt(rs.getString("crtd_dt"));
			}
			else
			{
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return model;
	}
	public boolean saveDetails(PurchaseExpenseModel objModel) {

		boolean retFlag = true;
		String sql = "";

		if (objModel.getExp_id() == 0) {
			// its the insert operation
			sql = objModel.Insert();
		} else {
			// its the update operation
			objModel.z_WhereClause.exp_id(objModel
					.getExp_id());
			// may need to be added
			/*
			 * objCustomerModel.z_WhereClause.br_id(
			 * ApplicationWorkbenchWindowAdvisor .getUserId());
			 */
		
			sql = objModel.Update();
		}
		System.out.println("The item query is :" + sql);
		try {

			st = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			// if the single user is inserted/update, its successful
			if (st.executeUpdate(sql) == 1)
				retFlag = true;
			else
				retFlag = false;

		} catch (SQLException e) {
			retFlag = false;
			e.printStackTrace();
		}
		return retFlag;
	}
	public boolean remove(int id) {
		boolean check = false;
		PurchaseExpenseModel grpModel = new PurchaseExpenseModel();
		grpModel.z_WhereClause.exp_id(id);
		String strQry = grpModel.Delete();
		try {
			this.cnn.setAutoCommit(false);
			this.st = cnn.createStatement();
			if (st.executeUpdate(strQry) == 1
					) {
				this.cnn.commit();
				check = true;
			} else {
				this.cnn.rollback();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return check;
	}
}
